import Foundation  //*************** change 3.0.5 add file ***
import UIKit

let hasOnlyGarageDoors:Bool = true
let hasInternalApp:Bool = true

var singleBrand:Bool = false

let isInternalApp:Bool = false
let isHomeApp:Bool = false
let isGarageApp:Bool = false
let isGarageDev:Bool = false
let isAmarr:Bool = false
let isClopay:Bool = true
let isPrecision:Bool = false
let isHomeDev:Bool = false
let isInternalDev:Bool = false

var brand:String = "Precision"
var themeColor:UIColor = UIColor.init(red: 0.0117647058, green: 0.2039215686, blue: 0.15294117647, alpha: 1.0)
var titleColor:UIColor = UIColor.white
var revNotePref:String = "precision"

//------------ the apply trans (production/development) ------------------------
var isProduction:Bool = true

var baseUrl = "https://modifaiprod-dot-homes-196619.appspot.com"
var applytransBase = "http://ec2-44-238-132-205.us-west-2.compute.amazonaws.com:5083/applyTrans"
var jotFormLink = "https://form.jotform.com/201259212801040"
var paintUrl:URL = URL(string: "http://ec2-44-236-52-217.us-west-2.compute.amazonaws.com:5083/applyTrans")!

