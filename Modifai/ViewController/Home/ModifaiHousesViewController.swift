import UIKit

//------------------------------------------------------------------------------------------
protocol ModifaiHousesViewControllerDelegate: class
{
    func selectedPhoto(img:UIImage,imgName:String)
}

//------------------------------------------------------------------------------------------
class ModifaiHousesViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource
{
    let kHeaderSectionTag: Int = 8500

    var type:String = ""
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchHeight: NSLayoutConstraint!
   
    var
    agentHomesArray:NSMutableArray = NSMutableArray()
    var resultArray:NSMutableArray = NSMutableArray()
var openSections: Array<Int> = []
    var agentHomesData:NSDictionary!
  
    var titleArray: [String] = [String]()
    var imagesArray:[UIImage] =  [UIImage]()
       var imageNames:[String] = [String]()
   
    var selctedModifaiImage = -1
    var  flasTimer:Timer?
    weak var delegate: ModifaiHousesViewControllerDelegate?
    @IBOutlet weak var modifaiHousesTableView: UITableView!
    @IBOutlet weak var loadMoreButton: UIButton!

    @IBOutlet weak var titleText: UILabel!
    var maxCount = 0
    var weekKeys = [String]()
    var weekFormatDictionary = [String:[AgentList]]()

    //------------------------------------------------------------------------------------------
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        
        self.view.backgroundColor = UIColor.white
        modifaiHousesTableView.backgroundColor = UIColor.white
        modifaiHousesTableView.separatorColor = UIColor.clear
        self.searchBar.isHidden = true
         self.searchHeight.constant = 0
        
        if type == "agent"
        {
            titleText.text = "AGENT HOUSES"
            self.callAPIForAgentHome()
           
           
        }else if type == "agenthome"
        {
            titleText.text = "AGENT COMPLETED HOUSES"
            self.callAPIForAgentHome()
           
        }else if type == "agenthomedelete"
        {
            titleText.text = "AGENT DELETED HOUSES"
            self.callAPIForAgentHome()
           
        }
        else if type == "agent_create"
        {
            if let curhome = homeviewController
            {
                let urlsArray = curhome.getCreatedOrders()
                agentHomesArray.addObjects(from:urlsArray)
            }
            
        }else
        {
            loadMoreButton.isHidden = true
            titleText.text = "\(brand) HOUSES"
        }
        if hasOnlyGarageDoors
           {
               titleArray = ["House 1","House 2","House 3","House 4", "House 5","House 6","House 7","House 8"]

            imagesArray =  [Constants.ModiGallImages.Home.galleryImage1!,Constants.ModiGallImages.Home.galleryImage2!,Constants.ModiGallImages.Home.galleryImage4!,Constants.ModiGallImages.Home.galleryImage5!,Constants.ModiGallImages.Home.galleryImage7!,Constants.ModiGallImages.Home.galleryImage9!,Constants.ModiGallImages.Home.galleryImage11!,Constants.ModiGallImages.Home.galleryImage12!]

            imageNames = ["modiNewGall1.jpg","modiGall2.jpg","modiGall4.jpg","modiGall5.jpg","modiNewGall7.jpg","modiGall9.jpg","modiGall11.jpg","modiGall12.jpg"]
           }else
        {
             titleArray = ["House 1","House 2","House 3","House 4", "House 5","House 6","House 7","House 8","House 9", "House 10"]
            imagesArray =  [Constants.ModiGallImages.Home.galleryImage1!,Constants.ModiGallImages.Home.galleryImage2!,Constants.ModiGallImages.Home.galleryImage3!,Constants.ModiGallImages.Home.galleryImage4!,Constants.ModiGallImages.Home.galleryImage5!,Constants.ModiGallImages.Home.galleryImage7!,Constants.ModiGallImages.Home.galleryImage8!,Constants.ModiGallImages.Home.galleryImage9!,Constants.ModiGallImages.Home.galleryImage11!,Constants.ModiGallImages.Home.galleryImage12!]

            imageNames = ["modiNewGall1.jpg","modiGall2.jpg","modiGall3.jpg","modiGall4.jpg","modiGall5.jpg","modiNewGall7.jpg","modiGall8.jpg","modiGall9.jpg","modiGall11.jpg","modiGall12.jpg"]
        }
    }

    //------------------------------------------------------------------------------------------
    override func viewDidAppear(_ animated: Bool)
    {
        if type == "agent"
               {
                   self.searchBar.isHidden = false
                self.searchHeight.constant = 40
                  
                  
               }else if type == "agenthome"
               {
                   self.searchBar.isHidden = false
                 self.searchHeight.constant = 40
               }else if type == "agenthomedelete"
               {
                   self.searchBar.isHidden = false
                 self.searchHeight.constant = 40
                   
                  
               }
        flasTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true)
        { timer in
            self.modifaiHousesTableView.flashScrollIndicators()
        }
      DispatchQueue.main.asyncAfter(deadline: (.now() + .seconds(10))) {
        self.flasTimer!.invalidate()
      }
    }

    
    //-------------------------------
    
    @IBAction func loadMoreBtnAction(_ sender: UIButton) {
       
         if type == "agenthome"
        {
            DataManager.getCompletedAgentHomes(range: "0", limit: "\(self.maxCount)", completion:{
                           (error,data) in
                          self.loadDataWithResponse(data:data)
                           
                       })
        }else if type == "agent"
         {
       
            
            sender.isHidden = false
            DataManager.getAllAgentHomes(range: "0", limit: "\(self.maxCount)", completion:{
                (error,data) in
                self.loadDataWithResponse(data:data)
            })
        }else if type == "agenthomedelete"
          {
        
             
             sender.isHidden = false
             DataManager.getDeletedAgentHomes(range: "0", limit: "\(self.maxCount)", completion:{
                 (error,data) in
                 self.loadDataWithResponse(data:data)
             })
         }else
        {
            sender.isHidden = true
        }
    }
   
    func callAPIForAgentHome()
    {
        self.loadMoreButton.isHidden = true
          var file_name = "AllAgentHomesData.txt"
                if type == "agenthome"
                {
                    file_name = "CompletedHomesData.txt"
      
                }
        if type == "agenthomedelete"
                      {
                          file_name = "DeletedHomesData.txt"
            
                      }
    
                let fullsizedArray =  DataManager.loadPaintTextFile(fileName:file_name)
                var totalArray:[Any] = [Any]()
                var tempArray:[Any] = [Any]()
                totalArray.append(contentsOf:fullsizedArray)
                  for item in totalArray
                     {
                      if let itemDic = item as? NSDictionary
                      {
                       let imageURl = itemDic["image_url"] as? String ?? ""
                       if verifyUrl(urlString:imageURl)
                       {
                           tempArray.append(item)
                       }
                       }
                       
                   }
                   totalArray = tempArray
        if totalArray.count == 0
        {
            if type == "agent"
                   {
                      
                       DataManager.getAllAgentHomes(range: "0", limit: "200", completion:{
                           (error,data) in
                          self.loadDataWithResponse(data:data)
                           
                       })
                      
                   }else if type == "agenthome"
                   {
                     
                       DataManager.getCompletedAgentHomes(range: "0", limit: "200", completion:{
                                      (error,data) in
                                     self.loadDataWithResponse(data:data)
                                      
                                  })
                   }else if type == "agenthomedelete"
                   {
                     
                    DataManager.getDeletedAgentHomes(range: "0", limit: "200", completion:{
                                      (error,data) in
                                     self.loadDataWithResponse(data:data)
                                      
                                  })
                   }
        }else
        {
            
            self.agentHomesArray.addObjects(from:totalArray)
            resultArray = agentHomesArray
            divideItemsBasedOnCretaedDate()
            self.modifaiHousesTableView.reloadData()

        }
    }
    func divideItemsBasedOnCretaedDate(){
            
        weekFormatDictionary = [String:[AgentList]]()
        var tempWeekKeys = [Date?]()
        
             for item in self.agentHomesArray{
                
               //let tempData =  TimeLogSheet(dictionary:option as! [String : Any])
                
                let tempData:AgentList =  AgentList(item as? NSDictionary ?? [:])
                
                let curDate = tempData.capture.dateFromString()
                let curWeekStart = curDate?.startOfWeek?.stringFromDate()
                let curWeekEnd = curDate?.endOfWeek?.stringFromDate()
                tempWeekKeys.append(curWeekStart?.dateFromString() ?? nil)

               let curWeek = "\(curWeekStart ?? "") - \(curWeekEnd ?? "")"
                if weekFormatDictionary[curWeek] != nil {
                   
                    var tempMutArray = weekFormatDictionary[curWeek]
                    tempMutArray?.append(tempData)
                    weekFormatDictionary[curWeek] = tempMutArray
                }
                else {
                    var tempMut1Array:[AgentList] = [AgentList]()
                    tempMut1Array.append(tempData)
                    weekFormatDictionary[curWeek] = tempMut1Array
                }
            }
        
        weekKeys = weekFormatDictionary.map({ $0.key as String })
        let sortedKeys = weekKeys.sorted(by: { $0.dateFromWeekString().compare($1.dateFromWeekString()) == .orderedDescending })
        weekKeys = sortedKeys
           
        }
    func loadDataWithResponse(data:NSDictionary?)
    {
    if data != nil
    {
        
        self.agentHomesData = data
        self.maxCount = (self.agentHomesData["rowcount"] as? Int)!
        
        self.agentHomesArray.addObjects(from:(data!["data"] as? [Any] ?? []) )
        var file_name = "AllAgentHomesData.txt"
        if type == "agenthome"
        {
            file_name = "CompletedHomesData.txt"

        }
        if type == "agenthomedelete"
               {
                   file_name = "DeletedHomesData.txt"

               }
        let fullsizedArray =  DataManager.loadPaintTextFile(fileName:file_name)
        var totalArray:[Any] = [Any]()
        totalArray.append(contentsOf:fullsizedArray)
        for item in self.agentHomesArray
        {
            let itemTemp = item as? NSDictionary
            let name = itemTemp?["agentID"] as! String
          
            let namePredicate = NSPredicate(format: "agentID like %@",name)
            let filteredArray = fullsizedArray.filter { namePredicate.evaluate(with: $0) }

            if filteredArray.count > 0
            {
                
            }else
            {
                totalArray.append(item)
            }
        }
        var tempArray:[Any] = [Any]()
                         for item in totalArray
                            {
                             if let itemDic = item as? NSDictionary
                             {
                              let imageURl = itemDic["image_url"] as? String ?? ""
                              if verifyUrl(urlString:imageURl)
                              {
                                  tempArray.append(item)
                              }
                              }
                              
                          }
        totalArray = tempArray
        if totalArray.count > 0
        {
            let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            let filename = file_name
            let filenameTemppath =  paths[0].appendingPathComponent(filename)
            
           
                do {
                    
                    let jsonData: Data? = try? JSONSerialization.data(withJSONObject: totalArray)
                    try jsonData!.write(to: filenameTemppath)
                } catch {
                    //print("Couldn't write file")
                }
            
        }
        self.agentHomesArray.removeAllObjects()
        self.agentHomesArray.addObjects(from:totalArray)
        self.resultArray = agentHomesArray
        divideItemsBasedOnCretaedDate()
        self.modifaiHousesTableView.reloadData()
    }
    }
    
    //------------------------------------------------------------------------------------------
    @IBAction func btnUsePhotoClicked(_ sender: Any)
    {
        self.dismiss(animated: true, completion:
        {
//print("btnUse PhotoClicked library.")
            self.delegate?.selectedPhoto(img: self.imagesArray[self.selctedModifaiImage],imgName: self.imageNames[self.selctedModifaiImage])
        })
    }
    
    //------------------------------------------------------------------------------------------
    @IBAction func btnCancelClicked(_ sender: Any)
    {
       //self.navigationController?.popViewController(animated: true)

//print("btnCancelClicked library.")

        self.dismiss(animated: true, completion:
        {
        })
    }

    //MARK:- UITableview Delegate and Datasource methods
    //------------------------------------------------------------------------------------------
    func numberOfSections(in tableView: UITableView) -> Int {
        if type == "agent" || type == "agenthome" || type == "agenthomedelete"
               {
                return weekKeys.count
               }
        return 1
    }

    //------------------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if type == "agent" || type == "agenthome" || type == "agenthomedelete"
        {
            if  self.openSections.contains(section){
                       let agentCount = weekFormatDictionary[weekKeys[section]] ?? [AgentList]()
                       return agentCount.count
                   } else {
                       return 0;
                   }
           
        }
        if type == "agent_create"
        {
            return agentHomesArray.count
        }
        return titleArray.count;
        
    }
    //------------------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if type == "agent" || type == "agenthome" || type == "agenthomedelete"
        {
        return 50
        }
        return 0
    }
    
    //------------------------------------------------------------------------------------------

    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        //recast your view as a UITableViewHeaderFooterView
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        
        header.contentView.backgroundColor = UIColor(red: 66.0/255.0, green: 205.0/255.0, blue: 183.0/255.0, alpha: 1.0) //UIColor.colorWithHexString(hexStr: "#42CCBA")
        header.textLabel?.textColor = UIColor.white
        header.textLabel?.text = weekKeys[section]
        
        if let viewWithTag = self.view.viewWithTag(kHeaderSectionTag + section) {
                   viewWithTag.removeFromSuperview()
               }
        let headerFrame = self.view.frame.size
               let theImageView = UIImageView(frame: CGRect(x: headerFrame.width - 32, y: 13, width: 18, height: 18));
               theImageView.image = UIImage(named: "Chevron-Dn-Wht")
               theImageView.tag = kHeaderSectionTag + section
               header.addSubview(theImageView)
        // make headers touchable
        header.tag = section
        let headerTapGesture = UITapGestureRecognizer()
        headerTapGesture.addTarget(self, action: #selector(self.sectionHeaderWasTouched(_:)))
        header.addGestureRecognizer(headerTapGesture)
    }
   
    //------------------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100
        
    }
    
    

    //------------------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let modifaiHousesCell = tableView.dequeueReusableCell(withIdentifier: "ModifaiHousesCell") as? ModifaiHousesTableViewCell else
        {
            return UITableViewCell()
        }
        modifaiHousesCell.backgroundColor = UIColor.white
        modifaiHousesCell.feedButton.isHidden = true
        modifaiHousesCell.deleteButton.isHidden = true
        if type == "agent" || type == "agenthome" || type == "agenthomedelete"
        {
            let agentCount = weekFormatDictionary[weekKeys[indexPath.section]]
            modifaiHousesCell.deleteButton.isHidden = false
            if let agentItem:AgentList = agentCount?[indexPath.row]
            {
            modifaiHousesCell.imgView.sd_setImage(with: URL(string: agentItem.image_url), placeholderImage: UIImage(named: "loadingTemp"))
          
            modifaiHousesCell.titleLabel.text = "Order Number - \(agentItem.orderNumber)"
            modifaiHousesCell.deleteButton.addTarget(self, action: #selector(deleteCellFromServer), for: .touchUpInside)
            modifaiHousesCell.deleteButton.tag = indexPath.row
            modifaiHousesCell.deleteButton.accessibilityLabel = agentItem.agentID
            modifaiHousesCell.deleteButton.isHidden = false
                if type == "agenthomedelete"
                {
                    modifaiHousesCell.deleteButton.isHidden = true
                }
                if type == "agenthome"
                 {
                    modifaiHousesCell.feedButton.isSelected = false
                    if agentItem.feed == "1"
                    {
                        modifaiHousesCell.feedButton.isSelected = true
                        
                    }
                    modifaiHousesCell.feedButton.addTarget(self, action: #selector(updateFeedToServer(sender:)), for: .touchUpInside)
                              modifaiHousesCell.feedButton.tag = indexPath.row
                              modifaiHousesCell.feedButton.accessibilityLabel = agentItem.agentID
                              modifaiHousesCell.feedButton.isHidden = false
                }else
                {
                     modifaiHousesCell.feedButton.isHidden = true
                }
            }
            
        }else if type == "agent_create"
        {
            let imagefromDoc = UIImage(contentsOfFile: (self.agentHomesArray[indexPath.row] as! URL).path)
            modifaiHousesCell.imgView.image = imagefromDoc

                  
            modifaiHousesCell.titleLabel.text = "Order Number - \(indexPath.row+1)"
        }
        else
        {
        modifaiHousesCell.imgView.image = imagesArray[indexPath.row]

       
        modifaiHousesCell.titleLabel.text = titleArray[indexPath.row]
        }
        return modifaiHousesCell;
    }
    
    //MARK: feedUpdate
    @objc func updateFeedToServer(sender:UIButton)
    {
        if let agentID = sender.accessibilityLabel
        {
           var feed = "1"
            if sender.isSelected
            {
                feed = "0"
            }
            sender.isSelected = !sender.isSelected
            
                DataManager.agentHouseFeedUpdate(agentID:agentID, status: feed)
                let file_name = "CompletedHomesData.txt"
            
                for dict in self.resultArray
                {
                    let dic = (dict as? NSDictionary)?.mutableCopy() as? NSMutableDictionary
                    if let agenyNUm = dic?["agentID"] as? String
                    {
                        if agenyNUm.contains(agentID)
                        {
                            dic?["feed"] = feed
                            self.resultArray.add(dic?.copy() as? NSDictionary ?? [:])
                            self.resultArray.remove(dict)
                            break
                        }
                    }
                  
                }
                if self.resultArray.count > 0
                {
                    
                    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
                    let filename = file_name
                    let filenameTemppath =  paths[0].appendingPathComponent(filename)
                    
                    
                    do {
                        
                        let jsonData: Data? = try? JSONSerialization.data(withJSONObject: self.resultArray)
                        try jsonData!.write(to: filenameTemppath)
                    } catch {
                        //print("Couldn't write file")
                    }
                    
                }
                
//                self.agentHomesArray = self.resultArray
//                self.divideItemsBasedOnCretaedDate()
//                self.modifaiHousesTableView.reloadData()

        }
        
    }
//delete Action
    @objc func deleteCellFromServer(sender:UIButton)
    {
        if let agentID = sender.accessibilityLabel
        {
            let alertController = UIAlertController(title: "Alert",message: "Are you sure you want to delete agent home?",preferredStyle: UIAlertController.Style.alert)
            alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                DataManager.removeAgentHomeFromServer(agentID: agentID)
                var file_name = "AllAgentHomesData.txt"
                if self.type == "agenthome"
                {
                    file_name = "CompletedHomesData.txt"
                    
                }
                for dict in self.resultArray
                {
                    let dic = (dict as? NSDictionary) ?? [:]
                    if let agenyNUm = dic["agentID"] as? String
                    {
                        if agenyNUm.contains(agentID)
                        {
                            self.resultArray.remove(dict)
                            break
                        }
                    }
                }
                if self.resultArray.count > 0
                {
                    
                    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
                    let filename = file_name
                    let filenameTemppath =  paths[0].appendingPathComponent(filename)
                    
                    
                    do {
                        
                        let jsonData: Data? = try? JSONSerialization.data(withJSONObject: self.resultArray)
                        try jsonData!.write(to: filenameTemppath)
                    } catch {
                        //print("Couldn't write file")
                    }
                    
                }
                
                self.agentHomesArray = self.resultArray
                self.divideItemsBasedOnCretaedDate()
                self.modifaiHousesTableView.reloadData()
                
            }))
            alertController.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))

			if let popoverController = alertController.popoverPresentationController  //********** change 3.0.3 add if block ***
			{
			  popoverController.sourceView = self.view
			  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
			  popoverController.permittedArrowDirections = []
			}

            self.present(alertController, animated: true,completion: nil)
        }
    }
    
    
    //------------------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let cell = tableView.cellForRow(at: indexPath) as? ModifaiHousesTableViewCell

        self.selctedModifaiImage = indexPath.row
        if type == "agenthome"
        {
            let agentCount = weekFormatDictionary[weekKeys[indexPath.section]]
            if let agentItem:AgentList = agentCount?[indexPath.row]
            {
              //  let url = URL(string:agentItem.image_url)
                                 
              //  let imageLastname = url?.lastPathComponent ?? ""
                
                LoadingIndicatorView.show("Please Wait")
                
                DataManager.getAgentHouseList(orderNumber:agentItem.orderNumber, completion: { (error,data) in
                    if data != nil
                    {
                        DataManager.GetPaintColorsForAgentHomes(orderNumber: agentItem.orderNumber,completion:{
                            (error,data) in
                           
                            DispatchQueue.main.asyncAfter(deadline: .now() + 4, execute: {
                                // code to execute
                                LoadingIndicatorView.hide()
                                self.dismiss(animated: true, completion:
                                    {
                                       // self.delegate?.selectedPhoto(img: (cell?.imgView.image)!,imgName: imageLastname)
                                        LoadingIndicatorView.hide()
                                        if data != nil {
                                            
                                            if let home = homeviewController{
                                                home.loadDownloadedImage()
                                                return
                                            }
                                        }
                                })
                                
                                
                            })
                          
                            
                        })

                        if hasInternalApp
                        {
                        	DataManager.completeProject(agentID:agentItem.agentID,status:"working",completion:
                        	{(error,status) in
                            
                        	})
                        }
                        
                    } else {
                        LoadingIndicatorView.hide()
                        self.dismiss(animated: true, completion: nil)
                    }
                    
                })
                
            }else
            {
            
            self.dismiss(animated:true, completion: nil)
            }
            
            return
        }
        
       
        if type == "agenthomedelete"
        {

            let agentCount = weekFormatDictionary[weekKeys[indexPath.section]]
            if let agentItem:AgentList = agentCount?[indexPath.row]
            {
                
                LoadingIndicatorView.show("Please Wait")
//                let url = URL(string:agentItem.image_url)
//
//                let imageLastname = url?.lastPathComponent ?? ""
                
                DataManager.openCompleteProject(agentID:agentItem.agentID, completion: {
                    (err,status) in
                    
                    if status
                    {
                        self.dismiss(animated:true, completion: nil)
                        DataManager.getAgentHouseList(orderNumber:agentItem.orderNumber, completion: { (error,data) in
                            
                            if data != nil
                            {
                                DataManager.GetPaintColorsForAgentHomes(orderNumber: agentItem.orderNumber,completion:{
                                    (error,data) in
                                  
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 4, execute: {
                                        // code to execute
                                        LoadingIndicatorView.hide()
                                        self.dismiss(animated: true, completion:
                                        {
                                           // self.delegate?.selectedPhoto(img: (cell?.imgView.image)!,imgName: imageLastname)
                                             LoadingIndicatorView.hide()
                                            if data != nil {
                                                 LoadingIndicatorView.hide()
                                                if let home = homeviewController{
                                                    home.loadDownloadedImage()
                                                    return
                                                }
                                            }
                                        })
                                     
                                    })
                                  
                                    
                                })
                                
                                
                            }else
                            {
                                LoadingIndicatorView.hide()
                                AppDelegate.orderDownloaded()
                            }
                        })
                    }else
                    {
                        LoadingIndicatorView.hide()
                        
                    }
                    
                })
            }
            
           
            
            return
        }
       //self.modifaiHousesTableView.reloadData()
        if type == "agent" && !isHomeApp
               {

                let agentCount = weekFormatDictionary[weekKeys[indexPath.section]]
                if let agentItem:AgentList = agentCount?[indexPath.row]
                   {
                   

                   let url = URL(string:agentItem.image_url) //************* change add 2.0.6
                    let urlString = agentItem.image_url //************* change add 2.0.6
                   let imageLastname = url?.lastPathComponent ?? "" //************* change add 2.0.6
                DataManager.getAllAgentHomeStatus(agentID:agentItem.agentID, completion: {
                    (error,status) in
                    if status == false
                    {
                        self.dismiss(animated: true, completion:
                                       {
                                        let newImageName = "".randomString() //************* change add 2.0.6 adding new name
                                        let replaceString = urlString.replacingOccurrences(of: imageLastname, with: newImageName)
                                        DataManager.replaceURLInAgent(urlString:urlString, replaceStr: replaceString)
                                           self.delegate?.selectedPhoto(img: (cell?.imgView.image)!,imgName: newImageName)
                                       })
                    }else
                    {
                        AlertViewManager.shared.ShowOkAlert(title: "Opps..", message: "Someone working on this project", handler: nil)
                        
                        
                    }
                })
               
                }
               }else
        {
        self.dismiss(animated: true, completion:
        {
            if self.type == "agent_create"
            {
                let imagefromDoc = UIImage(contentsOfFile: (self.agentHomesArray[indexPath.row] as! URL).path)
                let url:URL = self.agentHomesArray[indexPath.row] as! URL
                
               var name = (url.absoluteString as NSString).lastPathComponent
                name = name.changeExtensions(name:name)
                if imagefromDoc != nil
                {
               self.delegate?.selectedPhoto(img: imagefromDoc!,imgName:name )
                }
            }else
            {
            self.delegate?.selectedPhoto(img: self.imagesArray[self.selctedModifaiImage],imgName: self.imageNames[self.selctedModifaiImage])
            }
        })
        }
    }
    // MARK: - Expand / Collapse Methods
       
       @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
           let headerView = sender.view as! UITableViewHeaderFooterView
           let section    = headerView.tag
           let eImageView = headerView.viewWithTag(kHeaderSectionTag + section) as? UIImageView

           if (self.openSections.count == 0) {
              // self.expandedSectionHeaderNumber = section
               self.openSections.insert(section, at: 0)
               tableViewExpandSection(section, imageView: eImageView!)
           } else {
               if self.openSections.contains(section) {
                   let index = openSections.firstIndex(of:section)
                   self.openSections.remove(at: index!)
                  tableViewCollapeSection(section, imageView: eImageView!)
               } else {
                 //  let cImageView = self.view.viewWithTag(kHeaderSectionTag + //self.expandedSectionHeaderNumber) as? UIImageView
                //   tableViewCollapeSection(self.expandedSectionHeaderNumber, imageView: cImageView!)
              //      self.expandedSectionHeaderNumber = section;
                       self.openSections.insert(section, at: 0)
                  tableViewExpandSection(section, imageView: eImageView!)
               }
           }
       }
       
       func tableViewCollapeSection(_ section: Int, imageView: UIImageView) {
          let sectionData = weekFormatDictionary[weekKeys[section]] ?? [AgentList]()

           
       //    self.expandedSectionHeaderNumber = -1;
        if (sectionData.count == 0) {
               return;
           } else {
             UIView.animate(withDuration: 0.4, animations: {
                            imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
                        })
            
               var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                   let index = IndexPath(row: i, section: section)
                   indexesPath.append(index)
               }
               self.modifaiHousesTableView!.beginUpdates()
               self.modifaiHousesTableView!.deleteRows(at: indexesPath, with: UITableView.RowAnimation.fade)
               self.modifaiHousesTableView!.endUpdates()
           }
       }
       
       func tableViewExpandSection(_ section: Int, imageView: UIImageView) {
           let sectionData = weekFormatDictionary[weekKeys[section]] ?? [AgentList]()

        if (sectionData.count == 0) {
             //  self.expandedSectionHeaderNumber = -1;
              
               return;
           } else {

            UIView.animate(withDuration: 0.4, animations: {
                          imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
                      })
            var indexesPath = [IndexPath]()
               for i in 0 ..< sectionData.count {
                   let index = IndexPath(row: i, section: section)
                   indexesPath.append(index)
               }
              // self.expandedSectionHeaderNumber = section
               self.modifaiHousesTableView!.beginUpdates()
               self.modifaiHousesTableView!.insertRows(at: indexesPath, with: UITableView.RowAnimation.fade)
               self.modifaiHousesTableView!.endUpdates()
           }
       }
}

extension ModifaiHousesViewController: UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        if searchText.isEmpty
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
//print("search cleared")
                searchBar.resignFirstResponder()
                self.agentHomesArray = self.resultArray
                self.divideItemsBasedOnCretaedDate()
                self.modifaiHousesTableView.reloadData()
                
            }
        } else {
//print("\(searchText)")
        let tempResult = NSMutableArray()

        for dict in resultArray
        {
            let dic = (dict as? NSDictionary) ?? [:]
            if let ordernumber = dic["orderNumber"] as? String
            {
                if ordernumber.lowercased().contains(searchText.lowercased())
                {
                    tempResult.add(dic)
                }
            }
        }
        agentHomesArray = tempResult
        divideItemsBasedOnCretaedDate()
        self.modifaiHousesTableView.reloadData()
        }
    }
    
   func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
   {
        searchBar.resignFirstResponder()
//print("\(searchBar.searchTextField.text ?? "")")
    }
}


