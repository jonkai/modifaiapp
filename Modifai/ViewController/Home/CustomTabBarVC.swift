
//  Created  on 27/11/18.


import UIKit

class CustomTabBarVC: UITabBarController,UITabBarControllerDelegate {
    @IBOutlet weak var customTabBar: UITabBar!
    override func viewDidLoad() {
        super.viewDidLoad()
         self.delegate = self
        self.customTabBar.unselectedItemTintColor = UIColor(red: 169/255.0, green: 185/255.0, blue: 238/255.0, alpha: 1.0)
        

        self.customTabBar.tintColor = themeColor

    }

    func showAlertForLogin()
    {
//print("showAlert For Login")
        let okay = UIAlertAction.init(title:"Login", style: .default, handler: {
            action in
           //calLgin
            
        })
        let cancel = UIAlertAction.init(title:"Cancal", style: .default, handler: {
            action in
            
        })
        AlertViewManager.shared.ShowAlert(title: "Alert!", message: "Please Login For Acess!", actions: [okay,cancel])
    }
    
    // called whenever a tab button is tapped
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if(item.tag == 1) {
            // Code for item 1
        } else if(item.tag == 2) {
            // Code for item 2
        }
    }
     func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController)  {
    
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
//        let navVC = viewController as! UINavigationController
//        let className = NSStringFromClass(navVC.visibleViewController!.classForCoder)
//        if(className == "ProposalsViewController") {
//            return false;
//        }
        return true
    }
    
    func tabBarController(tabBarController: UITabBarController,
                          didSelectViewController viewController: UIViewController) {
        
        if let vc = viewController as? UINavigationController {
            vc.popViewController(animated: false)
        }
    }
    
    // alternate method if you need the tab bar item


}


