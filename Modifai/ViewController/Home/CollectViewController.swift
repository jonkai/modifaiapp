import UIKit  //************** change 3.0.3 add file ***

//-----------------------------------------------------------------------
class CollectViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout
{
    private var collectionViewFlowLayout: UICollectionViewFlowLayout
	{
        return collectionViewLayout as! UICollectionViewFlowLayout
    }

	//-----------------------------------------------------------------------
    override func viewDidLoad()
	{
        super.viewDidLoad()
    }

	//------------------------------------------------------------------------------------
	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)

		self.collectionView?.reloadData();
	}

	//------------------------------------------------------------------------------------
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
	{
		if (hasOnlyGarageDoors)
		{
			return UIEdgeInsets(top: 12, left: 0, bottom: 0, right: 0)
		}

		return UIEdgeInsets(top: 58, left: 0, bottom: 0, right: 0)
    }

	//-----------------------------------------------------------------------
    override func viewDidLayoutSubviews()
	{
        super.viewDidLayoutSubviews()
//        configureCollectionViewLayoutItemSize()
    }



//func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
//{
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "customCell", for: indexPath) as! CustomCell
//            if let indexPaths = collectionView.indexPathsForSelectedItems, let indexP = indexPaths.first
//            {
//                cell.bgView.backgroundColor = indexP == indexPath ? .white : .lightGray
//            }
//            return cell
//}
//func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
//{
//            if let cell = collectionView.cellForItem(at: indexPath) as? CustomCell {
//                cell.bgView.backgroundColor = .white }
//}
//func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath)
//{
//        guard let cell = collectionView.cellForItem(at: indexPath) as? CustomCell else { return }
//        cell.bgView.backgroundColor = .lightGray
//}



//	//-----------------------------------------------------------------------
//	func calculateSectionInset() -> CGFloat
//	{
//		return 10.0
//	}

//	//-----------------------------------------------------------------------
//    private func configureCollectionViewLayoutItemSize()
//	{
//		if (collectionViewFlowLayout.scrollDirection == .horizontal)
//		{
////			let inset:CGFloat = calculateSectionInset()
////			collectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
//////print("configureCollect ionViewLayoutItemSize collectionViewFlowLayout.itemSize",collectionViewFlowLayout.itemSize);
//////print("configureCollect ionViewLayoutItemSize doorCellWidth",doorCellWidth);
////
////			collectionViewFlowLayout.itemSize = CGSize(width: doorCellWidth+20.0, height: collectionViewLayout.collectionView!.frame.size.height)
//////print("horz configureCollecti onViewLayoutItemSize collectionViewFlowLayout.itemSize",collectionViewFlowLayout.itemSize);
//        } else {
////			let inset:CGFloat = calculateSectionInset()
////			collectionViewFlowLayout.sectionInset = UIEdgeInsets(top: inset, left: 0, bottom: inset, right: 0)
////			collectionViewFlowLayout.itemSize = CGSize(width: collectionViewLayout.collectionView!.frame.size.width, height: 86 - (inset * 2))
//////print("vert configureCollecti onViewLayoutItemSize collectionViewFlowLayout.itemSize",collectionViewFlowLayout.itemSize);
//		}
//    }

//	//-----------------------------------------------------------------------
//    private func indexOfMajorCell() -> Int
//	{
//        let itemWidth = collectionViewFlowLayout.itemSize.width
//        let proportionalOffset = collectionViewLayout.collectionView!.contentOffset.x / itemWidth
//        let index = Int(round(proportionalOffset))
//        let numberOfItems = collectionView.numberOfItems(inSection: 0)
//        let safeIndex = max(0, min(numberOfItems - 1, index))
//        return safeIndex
//    }
//
//	//-----------------------------------------------------------------------
//    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
//	{
//        indexOfCellBeforeDragging = indexOfMajorCell()
//    }
//
//	//-----------------------------------------------------------------------
//    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>)
//	{
//    }
}
