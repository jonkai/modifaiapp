import AVFoundation
import Photos
import GLKit
import PECimage
var location0:CLLocation?

//---------------------------------------------------------------------------
class PhotoCaptureProcessor: NSObject
{
    private(set) var requestedPhotoSettings: AVCapturePhotoSettings
    private let willCapturePhotoAnimation: () -> Void
    private let livePhotoCaptureHandler: (Bool) -> Void
    private let completionHandler: (PhotoCaptureProcessor) -> Void
    
    var metadataPC:Dictionary = Dictionary<String, Any>()
    var assetPC: PHAsset?
    var photoData: Data?
    var livePhotoCompanionMovieURL: URL?
    
    let reviveAlbum = "Revive Originals"
    var assetCollection: PHAssetCollection!
    
    //---------------------------------------------------------------------------
    init(with requestedPhotoSettings: AVCapturePhotoSettings,
         willCapturePhotoAnimation: @escaping () -> Void,
         livePhotoCaptureHandler: @escaping (Bool) -> Void,
         completionHandler: @escaping (PhotoCaptureProcessor) -> Void)
    {
        self.requestedPhotoSettings = requestedPhotoSettings
        self.willCapturePhotoAnimation = willCapturePhotoAnimation
        self.livePhotoCaptureHandler = livePhotoCaptureHandler
        self.completionHandler = completionHandler
        
        super.init()
        
        if PHPhotoLibrary.authorizationStatus() == .authorized
        {
            if let assetCollection = fetchAssetCollectionForAlbum()
            {
                self.assetCollection = assetCollection
                //print("album already created self.assetCollection",self.assetCollection)
                return
            }
            
            //print("album not yet created before createAlbum")
            self.createAlbum()
        } else {
            //print("shouldn't get to this point unless purposefully did not authorize")
        }
    }
    
    //------------------------------------------------------------
    func createAlbum()
    {
        PHPhotoLibrary.shared().performChanges(
            {
                PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: self.reviveAlbum)
        }) { success, error in
            if success
            {
                self.assetCollection = self.fetchAssetCollectionForAlbum()
            } else {
                //print("createAlbum error \(String(describing: error))")
            }
        }
    }
    
    //------------------------------------------------------------
    func fetchAssetCollectionForAlbum() -> PHAssetCollection!
    {
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", reviveAlbum)
        let collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
        
        if let _: AnyObject = collection.firstObject
        {
            return collection.firstObject! as PHAssetCollection
        }
        return nil
    }
    
    //---------------------------------------------------------------------------
    private func didFinish()
    {
        completionHandler(self)
    }
    
}

//---------------------------------------------------------------------------
extension PhotoCaptureProcessor: AVCapturePhotoCaptureDelegate
{
    //---------------------------------------------------------------------------
    func photoOutput(_ output: AVCapturePhotoOutput, willBeginCaptureFor resolvedSettings: AVCaptureResolvedPhotoSettings)
    {
        //print("willBeginCaptureFor")
        //print("willBeginCaptureFor attitude.xy",attitude.x,attitude.y)
        //        pitch = attitude.y;
        //print("  ")
        //print("  ")
        //print("landscape or...   pitch:",pitch)
        //
        //        if (gravity.x > 0.0)
        //        {
        //            pitch = -pitch;
        //        }
        //print("  ")
        //print("  ")
        //print("landscape or...   pitch2:",pitch)
        
        
        
        
        
        //        if (UIDevice.current.orientation.isLandscape)
        //        {
        //print("  ")
        //print("  ")
        //print("landscape gravity.xy",gravity.x,gravity.y)
        //
        ////            pitch = attitude.y;
        ////print("landscape pitch",pitch)
        //
        //            let sideTiltVector = double3([gravity.x, gravity.y, 0.0])
        //            pitch = sideTiltVector.angleBetween(secondVector: refWallLandscape)
        //            let fullTiltAngle = gravity.angleBetween(secondVector: refWallLandscape)
        //print("landscape pitch",pitch)
        //print("landscape fullTiltAngle",fullTiltAngle)
        //
        //            if (fabs(fullTiltAngle - pitch) < 0.01)
        //            {
        //                pitch = fullTiltAngle;
        //            }
        //
        //            if (gravity.y < 0.0)
        //            {
        //                pitch = -pitch;
        //            }
        //
        //print("landscape 2 pitch",pitch)
        //            if (gravity.x < 0.0)
        //            {
        //                pitch = -pitch;
        //            }
        //print("landscape 3 pitch",pitch)
        //
        //        } else if (UIDevice.current.orientation.isPortrait) {
        //print("  ")
        //print("  ")
        //print("portrait gravity.xy",gravity.x,gravity.y)
        //
        ////            pitch = gravity.x;
        //
        //            let sideTiltVector = double3([gravity.x, gravity.y, 0.0])
        //            pitch = sideTiltVector.angleBetween(secondVector: refWallPortrait)
        //            let fullTiltAngle = gravity.angleBetween(secondVector: refWallPortrait)
        //print("portrait 1 pitch",pitch)
        //print("portrait fullTiltAngle",fullTiltAngle)
        //
        //            if (fabs(fullTiltAngle - pitch) < 0.01)
        //            {
        //                pitch = fullTiltAngle;
        //            }
        //
        //            if (gravity.x < 0.0)
        //            {
        //                pitch = -pitch;
        //            }
        //print("portrait 2 pitch",pitch)
        //
        ////print("portrait gravity.y",gravity.y)
        //            if (gravity.y > 0.0)
        //            {
        //                pitch = -pitch;
        //            }
        //print("portrait 3 pitch",pitch)
        //        }
        
        //print("  ")
        //print("  ")
        //print("  ")
        
        
        
        
    }
    
    //---------------------------------------------------------------------------
    func photoOutput(_ output: AVCapturePhotoOutput, willCapturePhotoFor resolvedSettings: AVCaptureResolvedPhotoSettings)
    {
        rotationMatrix0 = rotationMatrix
        //print("willCapturePhotoFor")
        willCapturePhotoAnimation()
    }
    
    // OSType of pixelBuffer raw format
    //---------------------------------------------------------------------------
    func str4 (_ n: Int) -> String
    {
        var s: String = String (describing: UnicodeScalar((n >> 24) & 255)!)
        s.append(String(describing: UnicodeScalar((n >> 16) & 255)!))
        s.append(String(describing: UnicodeScalar((n >> 8) & 255)!))
        s.append(String(describing: UnicodeScalar(n & 255)!))
        return s
    }
    
    //-----------------------------------------------------------------------------
    func linePlaneInterConeX(theline: simd_float3, thenorm: simd_float3, planePt: simd_float3) -> simd_float3
    {
        var intpoint = simd_float3(0.0,0.0,0.0);
        var limitBelow = Eight_EPSILON;
        
        let above:Float = (thenorm.x*planePt.x) + (thenorm.y*planePt.y) + (thenorm.z*planePt.z);
        let below:Float = (thenorm.x*theline.x) + (thenorm.y*theline.y) + (thenorm.z*theline.z);
        
        let flen:Float = (theline.x * theline.x) + (theline.y * theline.y) + (theline.z * theline.z);
        if (flen < One_EPSILON) {limitBelow = flen * 0.1;}
        
        //NSLog(@"  ");
        //NSLog(@"  ");
        //NSLog(@"flen: %f ",flen);
        //NSLog(@"below: %f ",below);
        //NSLog(@"limitBelow: %f ",limitBelow);
        
        if (fabsf(below) < limitBelow) {return intpoint;}
        let t1:Float = above / below;
        //NSLog(@"t1: %f ",t1);
        
        intpoint.x = t1 * theline.x;
        intpoint.y = t1 * theline.y;
        intpoint.z = t1 * theline.z;
        
        return intpoint;
    }
    
    //---------------------------------------------------------------------------
    func bisect(_ v1: CIVector, _ v2: CIVector) -> CIVector
    {
        let intpoint:CIVector = CIVector(x:((v2.x - v1.x) * 0.5) + v1.x, y:((v2.y - v1.y) * 0.5) + v1.y);
        return intpoint;
    }
    
    //---------------------------------------------------------------------------
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?)
    {
        //print("didFinishProcessingPhoto")
        if (error == nil)
        {
            var metadata:Dictionary = photo.metadata as Dictionary<String, Any>
            //      var focalLength:Double = 0.0;
            
            if let exifData = metadata["{Exif}"] as? [String: Any]
            {
                lensModel = "none"
                if let lmodel =  exifData[kCGImagePropertyExifLensModel as String] as? String
                {
                    lensModel = lmodel;
                }
                
                //                if let focalLength00 = exifData[kCGImagePropertyExifFocalLength as String] as? Double
                //                {
                //                    //print("didFinishProc essingPhoto focalLength00:",focalLength00)
                //                    focalLength = focalLength00;
                //                }
                
                //                let focalLenIn35mmFilm = exifData[kCGImagePropertyExifFocalLenIn35mmFilm as String]
                //                let digitalZoom = exifData[kCGImagePropertyExifDigitalZoomRatio as String]
                //print("didFinishProc essingPhoto focalLength:",focalLength)
                //print("didFinishProc essingPhoto lensModel:",lensModel)
                //print("didFinishProc essingPhoto focalLenIn35mmFilm:",focalLenIn35mmFilm as Any)
                //print("didFinishProc essingPhoto digitalZoom:",digitalZoom as Any)
                //print(" ")
                //print(" ")
            }
            
            if let location = locatManager.location
            {
                let locationMetadata = NSMutableDictionary()
                location0 = location;
                
                let altitude : CLLocationDistance = location.altitude;
                var latitude : CLLocationDegrees = location.coordinate.latitude
                var latitudeRef : NSString!
                if (latitude < 0.0)
                {
                    latitude *= -1.0
                    latitudeRef = "S"
                } else {
                    latitudeRef = "N"
                }
                
                var longitude : CLLocationDegrees = location.coordinate.longitude
                var longitudeRef : NSString!
                if (longitude < 0.0)
                {
                    longitude *= -1.0
                    longitudeRef = "W"
                } else {
                    longitudeRef = "E"
                }
                
                let date : NSDate! = location.timestamp as NSDate
                let dateString = getUTCFormattedDate(localDate: date)
                let timeString = getUTCFormattedTime(localDate: date)
                locationMetadata.setObject(timeString, forKey: "TimeStamp" as NSCopying)
                locationMetadata.setObject(dateString, forKey: "DateStamp" as NSCopying)
                
                let latNum: NSNumber = NSNumber(value:latitude)
                locationMetadata.setObject(latNum, forKey: "Latitude" as NSCopying)
				locationMetadata.setObject(latitudeRef as Any, forKey: "LatitudeRef" as NSCopying)
                
                let lngNum: NSNumber = NSNumber(value:longitude)
                locationMetadata.setObject(lngNum, forKey: "Longitude" as NSCopying)
				locationMetadata.setObject(longitudeRef as Any, forKey:"LongitudeRef" as NSCopying)
                
                let hztNum: NSNumber = NSNumber(value:location.horizontalAccuracy)
                locationMetadata.setObject(hztNum, forKey: "DOP" as NSCopying)
                
                let altNum: NSNumber = NSNumber(value:altitude)
                locationMetadata.setObject(altNum, forKey: "Altitude" as NSCopying)
                
                if let heading = locatManager.heading
                {
                    let trueNum: NSNumber = NSNumber(value:heading.trueHeading)
                    locationMetadata.setObject("T", forKey: "ImgDirectionRef" as NSCopying)
                    locationMetadata.setObject(trueNum, forKey: "ImgDirection" as NSCopying)
                }
                
                metadata[kCGImagePropertyGPSDictionary as String] = locationMetadata;
            }
            
            if let photoData = photo.fileDataRepresentation()
            {
                if let originalImage = UIImage(data: photoData)
                {
                    var imageWidth0:CGFloat = originalImage.size.width //just for finding FOV
                    var imageWidth:CGFloat  = originalImage.size.width
                    var imageHeight:CGFloat = originalImage.size.height
                    //print(" ")
                    //print("imageWidth           :",imageWidth)
                    //print("imageHeight          :",imageHeight)
                    
                    if (originalImage.size.height > imageWidth0)
                    {
                        imageWidth0  = originalImage.size.height
                    }
                    //print(" ")
                    //print("imageWidth0     :",imageWidth0)
                    
                    focalLengthInPixels = (0.5 * Float(imageWidth0)) / (tan(QQ * (FOV * 0.5)));
                    //print("0 focalLengthInPixels :",focalLengthInPixels)
                    focalLengthInPixels = focalLengthInPixels * Float(lastZoomFactor);
                    //print("1 focalLengthInPixels :",focalLengthInPixels)
                    
                    let deviceOrientation:UIDeviceOrientation = UIDevice.current.orientation;
                    
                    if (UIDevice.current.orientation.isLandscape)
                    {
                        imageHeight = originalImage.size.width
                        imageWidth  = originalImage.size.height
                        //print(" ")
                        //print("imageWidth is landscape :",imageWidth)
                        //print("imageHeight             :",imageHeight)
                    }
                    
                    let halfWidth:Float  = Float(imageWidth) * 0.5
                    let halfHeight:Float = Float(imageHeight) * 0.5
//print(" ")
//print("FOV:",FOV)
//print("originalI mage.size  :",origina lImage.size)
//print("halfWidth           :",halfWidth)
//print("halfHeight          :",halfHeight)
//print("focalLengthInPixels :",focalLengthInPixels)
//print(" ")
                    mainPlate.removeAll()
                    mainPlate.append(simd_float4( 0.0,  0.0, -focalLengthInPixels, 1.0))
                    mainPlate.append(simd_float4(-halfWidth, -halfHeight, -focalLengthInPixels, 1.0))
                    mainPlate.append(simd_float4( halfWidth, -halfHeight, -focalLengthInPixels, 1.0))
                    mainPlate.append(simd_float4( halfWidth,  halfHeight, -focalLengthInPixels, 1.0))
                    mainPlate.append(simd_float4(-halfWidth,  halfHeight, -focalLengthInPixels, 1.0))
                    
                    let simdRotation = simd_float4x4(matrixCMR: rotationMatrix0)
//print(" ")
//print("mainPlate:",mainPlate)
//print(" ")
//print(" ")
//print(" ")
//print(" ")
//print("simdRotation",simdRotation)
//print(" ")
//print(" ")
                    
                    var attitudePlate:[simd_float4] = [simd_float4]();
                    
                    //print(" ")
                    //print(" ")
                    for i in 0..<mainPlate.count
                    {
                        attitudePlate.append(simdRotation * mainPlate[i]);
//print("attitudePlate[i]",attitudePlate[i])
                    }
                    //print(" ")
                    //print(" ")
                    
                    var centerPt:simd_float4 = attitudePlate[0]
                    centerPt.z = 0.0;
                    
                    let hyp = centerPt.magnitude();
                    let opi = centerPt.x;
                    let opOverHyp = opi / hyp;
                    
                    var angle:Float = asin(opOverHyp);
                    
                    //print("centerPt",centerPt)
                    //print("hyp",hyp)
                    //print("opi",opi)
                    //print("opOverHyp",opOverHyp)
                    //print("angle",angle)
                    
                    var dir:Float = .pi;
                    
                    if (angle < 0.0)
                    {
                        dir = -dir;
                    }
                    
                    if (centerPt.y < 0.0)
                    {
                        angle = dir - angle;
                    }
                    //print("angle2",angle)
                    
                    var justifiedPlate:[simd_float4] = [simd_float4]();
                    let rotateMatrix4 = simd_float4x4.makeRotate(radians:angle, 0.0, 0.0, 1.0)
                    
                    //print(" ")
                    //print(" ")
                    for i in 0..<attitudePlate.count
                    {
                        justifiedPlate.append(rotateMatrix4 * attitudePlate[i]);
                        //print("justifiedPlate[i]",justifiedPlate[i])
                    }
                    //print(" ")
                    //print(" ")
                    
                    let thenorm = simd_float3(0.0,-1.0,0.0);
                    let planePt = simd_float3(0.0,justifiedPlate[0].y,justifiedPlate[0].z);
                    
                    let theline1 = simd_float3(justifiedPlate[1].x,justifiedPlate[1].y,justifiedPlate[1].z);
                    let theline2 = simd_float3(justifiedPlate[2].x,justifiedPlate[2].y,justifiedPlate[2].z);
                    let theline3 = simd_float3(justifiedPlate[3].x,justifiedPlate[3].y,justifiedPlate[3].z);
                    let theline4 = simd_float3(justifiedPlate[4].x,justifiedPlate[4].y,justifiedPlate[4].z);
                    
                    // corners of image after auto leveling in a 3D manor
                    let cornerPt1 = self.linePlaneInterConeX(theline: theline1, thenorm: thenorm, planePt: planePt)
                    let cornerPt2 = self.linePlaneInterConeX(theline: theline2, thenorm: thenorm, planePt: planePt)
                    let cornerPt3 = self.linePlaneInterConeX(theline: theline3, thenorm: thenorm, planePt: planePt)
                    let cornerPt4 = self.linePlaneInterConeX(theline: theline4, thenorm: thenorm, planePt: planePt)
                    
                    //find the minimum X and "Y" values to move image to 0,0
                    var minX = cornerPt1.x;
                    //                    var maxX = cornerPt1.x;
                    var minZ = cornerPt1.z;
                    var maxZ = cornerPt1.z;
                    
                    if (cornerPt2.x < minX) {minX = cornerPt2.x}
                    if (cornerPt3.x < minX) {minX = cornerPt3.x}
                    if (cornerPt4.x < minX) {minX = cornerPt4.x}
                    
                    //                    if (cornerPt2.x > maxX) {maxX = cornerPt2.x;}
                    //                    if (cornerPt3.x > maxX) {maxX = cornerPt3.x;}
                    //                    if (cornerPt4.x > maxX) {maxX = cornerPt4.x;}
                    
                    if (cornerPt2.z < minZ) {minZ = cornerPt2.z}
                    if (cornerPt3.z < minZ) {minZ = cornerPt3.z}
                    if (cornerPt4.z < minZ) {minZ = cornerPt4.z}
                    
                    if (cornerPt2.z > maxZ) {maxZ = cornerPt2.z}
                    if (cornerPt3.z > maxZ) {maxZ = cornerPt3.z}
                    if (cornerPt4.z > maxZ) {maxZ = cornerPt4.z}
                    
                    //                    let totwidth  = maxX - minX;
                    //                    let totHeight = maxZ - minZ;
                    
                    minX = -minX;
                    minZ = -minZ;
                    
                    //needed a confersion to CIVector to use with CIFilter and with crop, CIFilter used for warping image
                    let v1:CIVector = CIVector(x:CGFloat(cornerPt1.x+minX), y:CGFloat(cornerPt1.z+minZ));
                    let v2:CIVector = CIVector(x:CGFloat(cornerPt2.x+minX), y:CGFloat(cornerPt2.z+minZ));
                    let v3:CIVector = CIVector(x:CGFloat(cornerPt3.x+minX), y:CGFloat(cornerPt3.z+minZ));
                    let v4:CIVector = CIVector(x:CGFloat(cornerPt4.x+minX), y:CGFloat(cornerPt4.z+minZ));
                    
                    //mid pts used with crop
                    let midPtTop:CIVector = bisect(v2,v3)
                    let midPtBot:CIVector = bisect(v1,v4)
                    let midPtLft:CIVector = bisect(v4,v3)
                    let midPtRht:CIVector = bisect(v1,v2)
                    
                    var ptTop:CGFloat = max(v1.y,v2.y)
                    var ptBot:CGFloat = min(v3.y,v4.y)
                    var ptLft:CGFloat = max(v4.x,v1.x)
                    var ptRht:CGFloat = min(v2.x,v3.x)
                    
                    //print(" ")
                    //print("cornerPt1",cornerPt1)
                    //print("cornerPt2",cornerPt2)
                    //print("cornerPt3",cornerPt3)
                    //print("cornerPt4",cornerPt4)
                    //print(" ")
                    //print(" ")
                    //print(" ")
                    //print("v1",v1)
                    //print("v2",v2)
                    //print("v3",v3)
                    //print("v4",v4)
                    //print(" ")
                    //print(" ")
                    //print("ptTop",ptTop)
                    //print("ptBot",ptBot)
                    //print("ptLft",ptLft)
                    //print("ptRht",ptRht)
                    //print(" ")
                    //print("minX",minX)
                    //print("minZ",minZ)
                    //print("maxZ",maxZ)
                    ////print("totHeight",totHeight)
                    //print(" ")
                    //print(" ")
                    //print(" ")
                    //print("midPtTop",midPtTop)
                    //print("midPtBot",midPtBot)
                    //print("midPtLft",midPtLft)
                    //print("midPtRht",midPtRht)
                    print(" ")
                    
                    //correct for lens distortion
                    let filteredImage = originalImage;
                    //                    if (focalLength > 4.2)
                    //                    {
                    //                        //print("focalLength > 4.2")
                    //                        //                        forcedIm ageSize = Size(width: Float(origin alImage.size.width), height: Float(origina lImage.size.height))
                    //                        //                        let lensCorrFilter = PECLensDistortCorr(lens:lensModel)
                    //                        //                        filteredImage = origin alImage.filterWithOperation(lensCorrFilter)
                    //                    } else {
                    //                        let imageSize = Size(width: Float(imageWidth), height: Float(imageHeight))
                    //                        let lensCorrFilter = PECLensDistortCorr(lens:lensModel, imageSize:imageSize)
                    //                        filteredImage = origina lImage.filterWithOperation(lensCorrFilter)
                    //                    }
                    
                    let context:CIContext = CIContext(options: nil);
                    
                    //3D auto level warping of image using CIFilter
                    if let perpFilter = CIFilter(name: "CIPerspectiveTransform")
                    {
                        let beginImage = CIImage(image: filteredImage)
                        perpFilter.setValue(beginImage, forKey:kCIInputImageKey)
                        
                        perpFilter.setValue(v3, forKey:"inputTopLeft");
                        perpFilter.setValue(v2, forKey:"inputTopRight");
                        perpFilter.setValue(v1, forKey:"inputBottomRight");
                        perpFilter.setValue(v4, forKey:"inputBottomLeft");
                        
                        
                        
                        if (deviceOrientation == UIDeviceOrientation.landscapeLeft)
                        {
                            //print("landscapeLeft")
                            ptBot = min(v3.y,v2.y)
                            ptTop = max(v1.y,v4.y)
                            ptLft = max(v4.x,v3.x)
                            ptRht = min(v2.x,v1.x)
                        } else if (deviceOrientation == UIDeviceOrientation.landscapeRight) {
                            //print("landscapeRight")
                            ptTop = max(v3.y,v2.y)
                            ptBot = min(v1.y,v4.y)
                            ptRht = min(v4.x,v3.x)
                            ptLft = max(v2.x,v1.x)
                        }
                        
                        
                        //print(" ")
                        //print("ptTop",ptTop)
                        //print("ptBot",ptBot)
                        //print("ptLft",ptLft)
                        //print("ptRht",ptRht)
                        //print(" ")
                        
                        if let output = perpFilter.outputImage
                        {
                            if let cgimg = context.createCGImage(output, from: output.extent)
                            {
                                let processedImage = UIImage(cgImage: cgimg)
                                
                                //                                if var exifData = metadata["{Exif}"] as? [String: Any]
                                //                                {
                                //                                    exifData[kCGImagePropertyExifPixelXDimension as String] = Int(proces sedImage.size.width)// Int();
                                //                                    exifData[kCGImagePropertyExifPixelYDimension as String] = Int(proces sedImage.size.height) //Int();
                                //                                    metadata[kCGImagePropertyExifDictionary as String] = exifData
                                //                                }
                                //
                                //                                metadata[kCGImagePropertyOrientation as String] = 0;
                                //                                self.metadataPC = metadata;
                                //print(" ")
                                //print("forcedIm ageSize  :",forcedIm ageSize)
                                //print(" ")
                                //print("filteredImage.size  :",filteredImage.size)
                                //print(" ")
                                //print(" ")
                                //print("output.extent  :",output.extent)
                                //print(" ")
                                //print(" ")
                                //print("process edImage.size  :",proces sedImage.size)
                                //print(" ")
                                //print(" ")
                                //print(" ")
                                //print("self.metadataPC  :",self.metadataPC)
                                //print(" ")
                                // find new size of image and crop the image so not so much white space
                                let newSize = CGSize(width: abs(ptRht - ptLft), height: abs(ptBot - ptTop))
                                
                                //print(" ")
                                //print("newSize",newSize)
                                
                                //print(" ")
                                //print("midPtLft.y:",midPtLft.y)
                                //print("midPtRht.y:",midPtRht.y)
                                //print(" ")
                                //                                var distFromTop:Float = Float(max(midPtLft.y,midPtRht.y))
                                
                                
                                //                                if (UIDevice.current.orientation.isLandscape)
                                //                                {
                                //                                    newSize = CGSize(width: abs(ptBot - ptTop), height: abs(ptRht - ptLft))
                                //
                                //                                    //print(" ")
                                //                                    //print("midPtT op.y:",midPtT op.y)
                                //                                    //print("midPtBot.y:",midPtBot.y)
                                //                                    //print(" ")
                                //                                }
                                //
                                //print(" ")
                                //print("newSize",newSize)
                                
                                var centerImagePt:CIVector = CIVector(x:((midPtRht.x - midPtLft.x) * 0.5) + midPtLft.x, y:((midPtTop.y - midPtBot.y) * 0.5) + midPtBot.y);
                                eyeLevelFromTop = (Float(newSize.height) * 0.5) + (Float(centerImagePt.y) - minZ)
                                //print(" ")
                                //print("0 centerImagePt:",centerImagePt)
                                //print("0 eyeLevelF romTop from top:",eyeLevelFromTop)
                                
                                centerImagePt = CIVector(x:centerImagePt.x, y:processedImage.size.height - centerImagePt.y);
                                //print("1 centerImagePt:",centerImagePt)
                                //print("newSize:",newSize)
                                //print(" ")
                                //                                let ratioWidth = newSize.width/proces sedImage.size.width;
                                //print("ratioWidth:",ratioWidth)
                                
                                
                                if var exifData = metadata["{Exif}"] as? [String: Any]
                                {
                                    exifData[kCGImagePropertyExifPixelXDimension as String] = Int(processedImage.size.width)// Int();
                                    exifData[kCGImagePropertyExifPixelYDimension as String] = Int(processedImage.size.height) //Int();
                                    
                                    var userN = "wasblank"
                                    if let user = UserDefaults.standard.string(forKey: "username")
                                    {
                                        userN = user;
                                    }
                                    
                                    exifData[kCGImagePropertyExifUserComment as String] = "userName_"+userN+" eyeLevel:"+String(eyeLevelFromTop);
                                    metadata[kCGImagePropertyExifDictionary as String] = exifData
                                }
                                
                                metadata[kCGImagePropertyOrientation as String] = 0;
                                self.metadataPC = metadata;
                                
                                //                                // try to leave no white space at all
                                //                                if (ratioWidth > cropRatio)
                                //                                {
                                //                                    let ratio = cropRatio / ratioWidth;   
                                //                                    //print("ratio:",ratio)
                                //                                    newSize.width = newSize.width * ratio;
                                //                                    newSize.height = newSize.height * ratio;
                                //                                }
                                
                                // crop image using new size
                                if let cropImage = processedImage.crop(centerPt: centerImagePt, newSize:newSize)
                                {
                                    if let imageData = cropImage.jpegData(compressionQuality: 1.0)
                                    {
                                        // put back in metadata that was lost in rotation and crop
                                        let dict = metadata as NSDictionary
                                        self.photoData = mergeImageData(imageData:imageData, with: dict)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    //-----------------------------------------------------------------------------
    func mergeImageData(imageData: Data, with metadata: NSDictionary) -> Data
    {
        let source: CGImageSource = CGImageSourceCreateWithData(imageData as NSData, nil)!
        let UTI: CFString = CGImageSourceGetType(source)!
        let newImageData =  NSMutableData()
        
        let cgImage = UIImage(data: imageData)!.cgImage
        let imageDestination: CGImageDestination = CGImageDestinationCreateWithData((newImageData as CFMutableData), UTI, 1, nil)!
        CGImageDestinationAddImage(imageDestination, cgImage!, metadata as CFDictionary)
        CGImageDestinationFinalize(imageDestination)
        
        return newImageData as Data
    }
    
    //------------------------------------------------------------------
    func getUTCFormattedDate(localDate : NSDate!) -> NSString
    {
        let dateFormatter = DateFormatter()
        let timeZone = TimeZone(secondsFromGMT:0)
        dateFormatter.timeZone = timeZone;
        dateFormatter.dateFormat = "yyyy:MM:dd"
        let dateString = dateFormatter.string(from: localDate as Date) as NSString
        return dateString;
    }
    
    //------------------------------------------------------------------
    func getUTCFormattedTime(localDate : NSDate!) -> NSString
    {
        let dateFormatter = DateFormatter()
        let timeZone = TimeZone(secondsFromGMT:0)
        dateFormatter.timeZone = timeZone;
        dateFormatter.dateFormat = "HH:mm:ss"
        let dateString = dateFormatter.string(from: localDate as Date) as NSString
        return dateString;
    }
    
    //---------------------------------------------------------------------------
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishCaptureFor resolvedSettings: AVCaptureResolvedPhotoSettings, error: Error?)
    {
        //print("didFinish Capture For")
        if (error != nil)
        {
            //print("Error capturing photo: \(error)")
            didFinish()
            return
        }
        
        guard let photoData = self.photoData else
        {
            //print("No photo data resource")
            didFinish()
            return
        }
        
        PHPhotoLibrary.requestAuthorization
            { status in
                if status == .authorized
                {
                    if self.assetCollection == nil
                    {
                        //print("asset collection is nil? find out why.")
                        return
                    }
                    
                    var imageIdentifier: String?
                    
                    PHPhotoLibrary.shared().performChanges(
                        {
                            let options = PHAssetResourceCreationOptions()
                            let creationRequest = PHAssetCreationRequest.forAsset()
                            options.uniformTypeIdentifier = self.requestedPhotoSettings.processedFileType.map { $0.rawValue }
                            creationRequest.addResource(with: .photo, data: photoData, options: options)
                            
                            let assetPlaceholder = creationRequest.placeholderForCreatedAsset
                            let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
                            let fastEnumeration = NSArray(array: [assetPlaceholder!] as [PHObjectPlaceholder])
                            albumChangeRequest?.addAssets(fastEnumeration)
                            
                            imageIdentifier = assetPlaceholder?.localIdentifier
                            //print("imageIdentifier:",imageIdentifier as Any)
                            
                    }, completionHandler: { _, error in
                        
                        //        if let error = error
                        
                        if (error == nil)
                        {
                            //print("PHPhotoLibrary Error occurred while saving photo to photo library: \(error)")
                            //    } else {
                            
                            if let imageIdentifier0 = imageIdentifier
                            {
                                if let asset = PHAsset.fetchAssets(withLocalIdentifiers: [imageIdentifier0], options: .none).firstObject
                                {
                                    self.assetPC = asset;
                                    //print("asset:",asset)
                                    //                                   let date = asset.creationDate ?? Date()
                                    //print("Creation date: \(date)")
                                    //print("asset.originalFilename:",asset.originalFilename as Any)
                                }
                            }
                        }
                        
                        //print("options.originalFilename  imageIdentifier:",imageIdentifier as Any)
                        
                        self.didFinish()
                    })
                } else {
                    //print("PHPhotoLibrary not authorized")
                    self.didFinish()
                }
        }
    }
    
    //-----------------------------------------------------------------------------
    func saveImageData(data: Data, metadata: NSDictionary? = nil, album: PHAssetCollection, completion:((PHAsset?)->())? = nil)
    {
        var placeholder: PHObjectPlaceholder?
        
        PHPhotoLibrary.shared().performChanges(
            {
                var changeRequest: PHAssetChangeRequest
                
                if let metadata = metadata
                {
                    let newImageData = self.mergeImageData(imageData: data, with: metadata)
                    changeRequest = PHAssetCreationRequest.forAsset()
                    (changeRequest as! PHAssetCreationRequest).addResource(with: .photo, data: newImageData as Data, options: nil)
                } else {
                    changeRequest = PHAssetChangeRequest.creationRequestForAsset(from: UIImage(data: data)!)
                }
                
                guard let albumChangeRequest = PHAssetCollectionChangeRequest(for: album),
                    let photoPlaceholder = changeRequest.placeholderForCreatedAsset else { return }
                
                placeholder = photoPlaceholder
                let fastEnumeration = NSArray(array: [photoPlaceholder] as [PHObjectPlaceholder])
                albumChangeRequest.addAssets(fastEnumeration)
        }, completionHandler: { success, error in
            guard let placeholder = placeholder else
            {
                completion?(nil)
                return
            }
            
            if success
            {
                let assets:PHFetchResult<PHAsset> =  PHAsset.fetchAssets(withLocalIdentifiers: [placeholder.localIdentifier], options: nil)
                let asset:PHAsset? = assets.firstObject
                completion?(asset)
            } else {
                completion?(nil)
            }
        })
    }
}

