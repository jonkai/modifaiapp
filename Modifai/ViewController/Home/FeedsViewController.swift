
import Foundation
import UIKit

//------------------------------------------------------------------------------------------
protocol FeedsViewControllerDelegate: class
{
    func selectedPhoto(img:UIImage,imgName:String)
}

//------------------------------------------------------------------------------------------
class FeedsViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource
{
    let kHeaderSectionTag: Int = 8500
    
    var type:String = ""
    var titleArray = [String]()
    var agentHomesArray:NSMutableArray = NSMutableArray()
    var resultArray:NSMutableArray = NSMutableArray()
    var agentHomesData:NSDictionary!
    var imagesArray:[UIImage] =  [UIImage]()
    
    var imageNames:[String] = [String]()
    var selctedModifaiImage = -1
    var  flasTimer:Timer?
    weak var delegate: ModifaiHousesViewControllerDelegate?
    @IBOutlet weak var modifaiHousesTableView: UITableView!
    var maxCount = 0
    
    //------------------------------------------------------------------------------------------
    override func viewDidLoad()
    {
        super.viewDidLoad()
       // self.navigationController?.navigationBar.isHidden = true
         AppUtility.lockOrientation(.portrait)
        self.view.backgroundColor = UIColor.white
        modifaiHousesTableView.backgroundColor = UIColor.white
       // modifaiHousesTableView.separatorColor = UIColor.clear
        
        
        self.navigationItem.title = "\(brand) Homes"
       
        
    }
    //----------------------------------------------------------------
       override var prefersStatusBarHidden: Bool
       {
           return true
       }
    //------------------------------------------------------------------------------------------
    override func viewDidAppear(_ animated: Bool)
    {
        
         self.callAPIForAgentHome()
        flasTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true)
        { timer in
            self.modifaiHousesTableView.flashScrollIndicators()
        }
        DispatchQueue.main.asyncAfter(deadline: (.now() + .seconds(10))) {
            self.flasTimer!.invalidate()
        }
    }
    
    
    
    
    func callAPIForAgentHome() //*********** Change 2.1.6 to 200 or is it 1000?  //.......... this is inconsistant?
    {
        
          LoadingIndicatorView.show("Please Wait")
            DataManager.getCompletedAgentHomes(range: "0", limit: "200", completion:{
                (error,data) in
                LoadingIndicatorView.hide()
                self.loadDataWithResponse(data:data)
                
            })
            
       
    }

    func loadDataWithResponse(data:NSDictionary?)
    {
        if data != nil
        {
            
            self.agentHomesData = data
            self.maxCount = (self.agentHomesData["rowcount"] as? Int)!
            
            self.agentHomesArray.addObjects(from:(data!["data"] as? [Any] ?? []) )
            
             var totalArray:[Any] = [Any]()
                  totalArray.append(contentsOf:(data!["data"] as? [Any] ?? []) )
       
            
            var tempArray:[Any] = [Any]()
            for item in totalArray
            {
                if let itemDic = item as? NSDictionary
                {
                    let imageURl = itemDic["image_url"] as? String ?? ""
                    let feedBool = itemDic["feed"] as? Bool ?? false
                    if verifyUrl(urlString:imageURl) && feedBool
                    {
                        tempArray.append(item)
                    }
                }
                
            }
            totalArray = tempArray
            if totalArray.count > 0
            {
                let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
                let filenameTemppath =  paths[0].appendingPathComponent("CompletedHomesData.txt")
                
                
                do {
                    
                    let jsonData: Data? = try? JSONSerialization.data(withJSONObject: totalArray)
                    try jsonData!.write(to: filenameTemppath)
                } catch {
                    //print("Couldn't write file")
                }
                
            }
            self.agentHomesArray.removeAllObjects()
            self.agentHomesArray.addObjects(from:totalArray)
            self.resultArray = agentHomesArray
            self.modifaiHousesTableView.reloadData()
        }
    }
    
    
    
    //------------------------------------------------------------------------------------------
   
    
    //MARK:- UITableview Delegate and Datasource methods
    //------------------------------------------------------------------------------------------
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    //------------------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return self.agentHomesArray.count
        
        
    }
    
    //------------------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 220
        
    }
    
    
    
    //------------------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let modifaiHousesCell = tableView.dequeueReusableCell(withIdentifier: "ModifaiOrdersCell") as? ModifaiHousesTableViewCell else
        {
            return UITableViewCell()
        }
        modifaiHousesCell.backgroundColor = UIColor.white

        
        if  let agentCount = self.agentHomesArray[indexPath.row] as? NSDictionary
        {
            let agentItem:AgentList =  AgentList(agentCount)
        
            modifaiHousesCell.imgView.sd_setImage(with: URL(string: agentItem.image_url), placeholderImage: UIImage(named: "loadingTemp"))
                       
        }
        return modifaiHousesCell;
    }
    //delete Action
    @objc func deleteCellFromServer(sender:UIButton)
    {
        if let agentID = sender.accessibilityLabel
        {
            let alertController = UIAlertController(title: "Alert",message: "Are you sure you want to delete agent home?",preferredStyle: UIAlertController.Style.alert)
            alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                DataManager.removeAgentHomeFromServer(agentID: agentID)
                let file_name = "CompletedHomesData.txt"
                
                for dict in self.resultArray
                {
                    let dic = (dict as? NSDictionary) ?? [:]
                    if let agenyNUm = dic["agentID"] as? String
                    {
                        if agenyNUm.contains(agentID)
                        {
                            self.resultArray.remove(dict)
                            break
                        }
                    }
                }
                if self.resultArray.count > 0
                {
                    
                    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
                    let filename = file_name
                    let filenameTemppath =  paths[0].appendingPathComponent(filename)
                    
                    
                    do {
                        
                        let jsonData: Data? = try? JSONSerialization.data(withJSONObject: self.resultArray)
                        try jsonData!.write(to: filenameTemppath)
                    } catch {
                        //print("Couldn't write file")
                    }
                    
                }
                
                self.agentHomesArray = self.resultArray
                self.modifaiHousesTableView.reloadData()
                
            }))
            alertController.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))

			if let popoverController = alertController.popoverPresentationController  //********** change 3.0.3 add if block ***
			{
			  popoverController.sourceView = self.view
			  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
			  popoverController.permittedArrowDirections = []
			}

            self.present(alertController, animated: true,completion: nil)
		}
    }
    
    
    //------------------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        self.selctedModifaiImage = indexPath.row
        
        if  let agentCount = self.agentHomesArray[indexPath.row] as? NSDictionary
               {
                
                
            let agentItem:AgentList =  AgentList(agentCount)
            LoadingIndicatorView.show("Please Wait")
            
            DataManager.getAgentHouseList(orderNumber:agentItem.orderNumber, completion: { (error,data) in
                if data != nil
                {
                    DataManager.GetPaintColorsForAgentHomes(orderNumber: agentItem.orderNumber,completion:{
                        (error,data) in
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 4, execute: {
                            // code to execute
                           
                            
                                   
                                    if data != nil {
                                        
                                       
                                           
                                            DispatchQueue.main.asyncAfter(deadline: .now() + 4, execute: {
                                             LoadingIndicatorView.hide()
                                                comingFromfeed = true
                                            let tabBarViewController = UIApplication.shared.windows.first!.rootViewController as!   UITabBarController
                                                tabBarViewController.selectedIndex = 0
                                                
                                            })
                                        
                                        }else
                                    {
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 4, execute: {
                                         LoadingIndicatorView.hide()
                                            comingFromfeed = true
                                        let tabBarViewController = UIApplication.shared.windows.first!.rootViewController as!   UITabBarController
                                            tabBarViewController.selectedIndex = 0
                                            
                                        })

                            }
                                    
                                                    
                        })
                        
                        
                    })
                 
                    
                }else
                {
                      LoadingIndicatorView.hide()
                   
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 4, execute: {
//                        LoadingIndicatorView.hide()
//                        let tabBarViewController = UIApplication.shared.windows.first!.rootViewController as!   UITabBarController
//                                                               comingFromfeed = true
//                                                                   tabBarViewController.selectedIndex = 0
//                    })
                }
                
            })
            
        }
        
    }
    
}



