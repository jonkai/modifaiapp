import UIKit //************** change 3.0.6 change whole file ***

//-----------------------------------------------------------------------
class VertHorzCollectVC: CollectViewController
{
	//-----------------------------------------------------------------------
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if (homeviewController.btnGarageClicked)
        {
			if (isTwoCar)
			{
				return homeviewController.homeObj?.garages.count ?? 0
			} else if (isOneCar) {
				return homeviewController.homeObj?.singleGarage.count ?? 0
			}
		} else if (homeviewController.btnDoorClicked) {

			if (isDouble)
			{
				return homeviewController.doorsDoubleArray.count
			} else if (isDoor) {
				return homeviewController.doorsArray.count
			}
		} else if (homeviewController.btnPaintListClicked && !hasOnlyGarageDoors) {
			return curProjectChoiceList.count
		}

        return homeviewController.homeObj?.garages.count ?? 0
    }

	//-----------------------------------------------------------------------
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "paintDoorCell", for: indexPath) as? PaintCollectionCell else
		{
			return PaintCollectionCell()
		}

		let subviewTp = cell.subviews[0]
		let subview = subviewTp.subviews[0]
		let doorCellV = subview.subviews[0]
//print("doorCellV:",doorCellV)

		cell.cellPaintView.isHidden = true;

		if let doorInfo = doorCellV.subviews[1] as? UIButton
		{
//print("doorInfo:",doorInfo)
			doorInfo.tag = indexPath.row
			doorInfo.removeTarget(homeviewController, action: nil, for: .touchUpInside)

			if let doorImageV = doorCellV.subviews[0] as? UIImageView
			{
        		if (homeviewController.btnGarageClicked)
        		{
					if (isTwoCar)
					{
						if let kount = homeviewController.homeObj?.garages.count
						{
							if (indexPath.row < kount)
							{
								if let door = homeviewController.homeObj?.garages[indexPath.row]
								{
									doorImageV.sd_setImage(with: URL(string: door.image), placeholderImage: UIImage(named: "loadingTemp"))
									doorInfo.addTarget(homeviewController, action: #selector(homeviewController.garageListClickAction(sender:)), for:.touchUpInside)
								}
							}
						}
					} else if (isOneCar) {
						if let kount = homeviewController.homeObj?.singleGarage.count
						{
							if (indexPath.row < kount)
							{
								if let door = homeviewController.homeObj?.singleGarage[indexPath.row]
								{
									doorImageV.sd_setImage(with: URL(string: door.image), placeholderImage: UIImage(named: "loadingTemp"))
									doorInfo.addTarget(homeviewController, action: #selector(homeviewController.garageListClickAction(sender:)), for:.touchUpInside)
								}
							}
						}
					}
				} else if (homeviewController.btnDoorClicked) {
//print("homeviewController.btnDoorClicked isDoor doorInfo:",isDoor, doorInfo)

					if (isDouble)
					{
						if (indexPath.row < homeviewController.doorsDoubleArray.count)
						{
							let door = homeviewController.doorsDoubleArray[indexPath.row]
							doorImageV.sd_setImage(with: URL(string: door.image), placeholderImage: UIImage(named: "loadingTemp"))
							doorInfo.addTarget(homeviewController, action: #selector(homeviewController.doorListClickAction(sender:)), for:.touchUpInside)
						}
					} else if (isDoor) {
						if (indexPath.row < homeviewController.doorsArray.count)
						{
							let door = homeviewController.doorsArray[indexPath.row]
							doorImageV.sd_setImage(with: URL(string: door.image), placeholderImage: UIImage(named: "loadingTemp"))
							doorInfo.addTarget(homeviewController, action: #selector(homeviewController.doorListClickAction(sender:)), for:.touchUpInside)
						}
					}
				} else if (homeviewController.btnPaintListClicked && !hasOnlyGarageDoors) {

					cell.cellPaintView.isHidden = false;
                	cell.color1View.isHidden = true
                	cell.color2View.isHidden = true
                	cell.color3View.isHidden = true
                	cell.color1Lbl.isHidden = true
                	cell.originalPaint.isHidden = true   //********** change 3.0.7 add ***


            		let proj = homeviewController.getMaskColors(index:indexPath.row)

//print("  ")
//print(" indexPath.row",indexPath.row)
//print(" proj",proj as Any)
//print("proj?.mask0:",proj?.mask0 as Any)
//print("proj?.mask1:",proj?.mask1 as Any)
//print("proj?.mask2:",proj?.mask2 as Any)

					if (indexPath.row > 0)
					{
						if proj?.mask0 != nil
						{
							cell.maskView1?.setImageColor(color:UIColor.color(data:proj?.defaultLineColor0d))
							cell.color1View.isHidden = false
						}

						if proj?.mask1 != nil
						{
							cell.maskView2?.setImageColor(color:UIColor.color(data:proj?.defaultLineColor1d))
							cell.color1Lbl.isHidden = false

							cell.color2View.isHidden = false
						}

						if proj?.mask2 != nil
						{
							cell.maskView3?.setImageColor(color:UIColor.color(data:proj?.defaultLineColor2d))
							cell.color3View.isHidden = false
						}
					} else {   //********** change 3.0.7 add ***
                		cell.originalPaint.isHidden = false   //********** change 3.0.7 add ***
					}
				}
			}
		}

        return cell
    }

	//-----------------------------------------------------------------------
	override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
	{
	//		var cell : UICollectionViewCell = collectionView.cellForItem(at: indexPath)!

		if (homeviewController.btnGarageClicked)
		{
			if (isTwoCar || isOneCar)
			{
				homeviewController.garageMaskView.image = nil;
				maskedDoorImage  = nil;

				if let paintCont = currentPaintImageController
				{
					paintCont.scrollView.garageMaskView.image = nil;
				}

				homeviewController.selectedimage = indexPath.row
				homeviewController.garageScrollIndex = indexPath.row+1

				guard let s = homeviewController.selectedReferenceItem else { return }

				if (isTwoCar)
				{
					s.replacementGarageNames = [homeviewController.homeObj?.garages[indexPath.row].image] as? [String]
					guard let rep = s.replacementGarageNames else { return }

					if let garage = homeviewController.homeObj?.garages[indexPath.row]
					{
						homeviewController.cur2Garage = garage;
					}

					homeviewController.garageCatID = (homeviewController.homeObj?.garages[indexPath.row].catalogID)!
					homeviewController.garageCatName = (homeviewController.homeObj?.garages[indexPath.row].title)!
					let currentPage = rep.count - 1

					garimageID = ""


					if let house0Object = houseObject
					{
						for resp in house0Object.response
						{
							for respItem in resp
							{
								if (respItem.labels.count > 0)
								{
									let lbl = respItem.labels[0]
									if lbl.contains("two-car")
									{
										let imgId = respItem.imageID_db
										if garimageID.count > 0
										{
											garimageID = garimageID+"||"+imgId
										} else {
											garimageID = imgId
										}
									}
								}
							}
						}
					}

					homeviewController.checkFavStatus(indexs: homeviewController.selectedimage)
					homeviewController.replaceObjectAt(index: currentPage,type: "garage")
				} else {
					s.replacementGarageNames = [homeviewController.homeObj?.singleGarage[indexPath.row].image] as? [String]
					homeviewController.garageCatID = (homeviewController.homeObj?.singleGarage[indexPath.row].catalogID)!
					homeviewController.garageCatName = (homeviewController.homeObj?.singleGarage[indexPath.row].catalogID)!
					guard let rep = s.replacementGarageNames else { return }

					garimageID = ""

					if let garage = homeviewController.homeObj?.singleGarage[indexPath.row]
					{
						homeviewController.cur1Garage = garage;
					}

					if let house0Object = houseObject
					{
						for resp in house0Object.response
						{
							for respItem in resp
							{
								if (respItem.labels.count > 0)
								{
									let lbl = respItem.labels[0]
									if lbl.contains("one-car") || lbl.contains("two-car")
									{
										let imgId = respItem.imageID_db
										if garimageID.count > 0
										{
											garimageID = garimageID+"||"+imgId
										} else {
											garimageID = imgId
										}
									}
								}
							}
						}
					}

					let currentPage = rep.count - 1
					homeviewController.checkFavStatus(indexs: homeviewController.selectedimage)
					homeviewController.replaceObjectAt(index: currentPage,type: "garage")
				}
			}

		} else if (homeviewController.btnDoorClicked) {

			homeviewController.frontDrMaskView.image = nil;
			maskedFrontDrImage  = nil;

			if let paintCont = currentPaintImageController
			{
				paintCont.scrollView.frontDrMaskView.image = nil;
			}

			homeviewController.selectedimage = indexPath.row
			homeviewController.doorScrollIndex = indexPath.row+1

			 guard let s = homeviewController.selectedReferenceItem else { return }
			let garage:Door =  isDouble ?  homeviewController.doorsDoubleArray[indexPath.row] : homeviewController.doorsArray[indexPath.row]

			s.replacementGarageNames = [garage.image]
			homeviewController.doorCatID = garage.catalogID

			if (isDouble)
			{
				homeviewController.cur2Door = garage;
			} else {
				homeviewController.cur1Door = garage;
			}

			if let house0Object = houseObject
			{
				for resp in house0Object.response
				{
					for respItem in resp
					{
						if (respItem.labels.count > 0)
						{
							let lbl = respItem.labels[0]
							if lbl.contains("door")
							{
								doorimageID = respItem.imageID_db
							}
						}
					}
				}
			}
			guard let rep = s.replacementGarageNames else { return }
			let currentPage = rep.count - 1

			homeviewController.checkFavStatus(indexs: homeviewController.selectedimage)
			homeviewController.replaceObjectAt(index: currentPage,type: "door")

		} else if (homeviewController.btnPaintListClicked && !hasOnlyGarageDoors) {

			homeviewController.paintScrollend(index:indexPath.row)
		}
	}
}
