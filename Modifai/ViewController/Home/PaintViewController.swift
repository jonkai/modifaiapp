import Foundation
import UIKit
import Photos
import PECimage
import ImageIO
import MobileCoreServices

//-------------------------------------------
public struct PECpixel: Equatable, Hashable
{
    public var h: Int = 0
    public var lu: Int = 0
    public var hue: CGFloat = 0.0
    public var s: CGFloat = 0.0
    public var l: CGFloat = 0.0

    public var slmin: CGFloat = 0.0
    public var slmax: CGFloat = 0.0

    public var smin: CGFloat = 0.0
    public var smax: CGFloat = 0.0
    public var lmin: CGFloat = 0.0
    public var lmax: CGFloat = 0.0

    public var r: CGFloat = 0.0
    public var g: CGFloat = 0.0
    public var b: CGFloat = 0.0

    public var hl: CGFloat = 0.0
    public var k: Int = 0
    public var diffH: CGFloat = 0.0

    //-------------------------------------------
    public init()
    {
       self.r = 0.0
       self.g = 0.0
       self.b = 0.0

       self.h = 0
       self.lu = 0
       self.hue = 0.0
       self.s = 0.0
       self.l = 0.0

       self.slmin = 0.0
       self.slmax = 0.0

       self.smin = 0.0
       self.smax = 0.0
       self.lmin = 0.0
       self.lmax = 0.0

       self.hl = 0.0
       self.k = 0
       self.diffH = 0.0
    }

    //-------------------------------------------
    public init(h:Int)
    {
       self.r = 0.0
       self.g = 0.0
       self.b = 0.0

       self.h = h
       self.lu = 0
       self.hue = CGFloat(h) * 0.01
       self.s = 0.0
       self.l = 0.0

       self.slmin = 0.0
       self.slmax = 0.0

       self.smin = 0.0
       self.smax = 0.0
       self.lmin = 0.0
       self.lmax = 0.0

       self.hl = 0.0
       self.k = 0
       self.diffH = 0.0
    }

    //-------------------------------------------
    public init(lu:Int)
    {
       self.r = 0.0
       self.g = 0.0
       self.b = 0.0

       self.h = 0
       self.lu = lu
       self.hue = 0.0
       self.s = 0.0
       self.l = CGFloat(lu) * 0.01

       self.slmin = 0.0
       self.slmax = 0.0

       self.smin = 0.0
       self.smax = 0.0
       self.lmin = 0.0
       self.lmax = 0.0

       self.hl = 0.0
       self.k = 0
       self.diffH = 0.0
    }
}

var sidings:[NSArray] = [NSArray]();
var houses:[NSArray] = [NSArray]();
var roofs:[NSArray] = [NSArray]();
var windows:[NSArray] = [NSArray]();
var trims:[NSArray] = [NSArray]();

var currProject:Project? = nil
var currProjectThumb:ProjectThumb? = nil
var currGallThumb:ProjectThumb? = nil    //*************** change 1.1.0 add ***
var currGallID:String? = nil;   //*************** change 1.1.0 add ***
var curProjectChoiceList:[ProjectThumb] = [ProjectThumb]();   //*************** change 1.1.4 add, needs to be universal, changed only when project changes ***

//var garageImageView0:UIImage?;
var garageMaskImage:UIImage?;
var garageSingleMaskImage:UIImage?;
var frontDrMaskImage:UIImage?;

var maskedSidingImage:UIImage?;
var maskedRoofImage:UIImage?;
var houseMask:UIImage?;


var maskedHouseImage:UIImage?;
var maskedHouseImage0:UIImage?;
var maskedHouseImage1:UIImage?;
var maskedHouseImage2:UIImage?;
var maskedHouseImage3:UIImage?;
var maskedHouseImage4:UIImage?;

var oneCarImgae:UIImage?
var twoCarImgae:UIImage?
var oneDoorImgae:UIImage?
var twoDoorImgae:UIImage?

var maskedHouseReady0:Bool = false;
var maskedHouseReady1:Bool = false;
var maskTargetSize:CGSize = CGSize(width: 1.0, height: 1.0);

var hasBeenSaved:Bool = false;
var scrollEndDoor:Bool    = false;   //*************** change 1.1.0 add ***

var currentButtonToDisplay: Int = 0;
var currentWellNum: Int = 0;

var resolutionOnorOff = true
var paintOnorOff = false
var masksOnorOff = true
var proOnorOff = false
var isSecretTurnOn = false

var includeGray:Bool = false;
var outRightWhite:Bool = false;
var outRightGray:CGFloat = 1.0;

var pullBarDist:CGFloat = 50.0;
var touchPadCenterX:CGFloat = 346.0;
var touchPadLimit:CGFloat = 340.0;


var containWidthImage:CGFloat  = 1.0;
var containHeightImage:CGFloat = 1.0;

var viewTransition = false;
var phonePortrait:Bool = true;
var lastPhonePortrait:Bool = true;
var skipConfigure:Bool = false;
var curZoom:CGFloat = 1.0;

var curOffset:CGPoint = CGPoint();
var curSize:CGSize = CGSize();


var currentPaintImageController:PaintViewController!;
var wasIpad:Bool = false;
var isThisForPaint:Bool = false;
var wasGoodLayer:Bool = false;


var appDelegate:AppDelegate!
var isBump = false

var alreadyDone:Bool = false;
var shouldWriteToDisk:Bool = false;
var hidingViewWasPreferenceContoller:Bool = false;
var commingFromProjectList:Bool = false;
var isaMaskPush:Bool = false;
var wasAuserName:Bool = false;

var shouldSave:Bool = false;



var defaultLineColor:UIColor = UIColor.clear;
var origStartColor:UIColor = UIColor.clear;

var defaultLineColor0:UIColor = UIColor.clear;
var origStartColor0:UIColor = UIColor.clear;

var defaultLineColor1:UIColor = UIColor.clear;
var origStartColor1:UIColor = UIColor.clear;

var defaultLineColor2:UIColor = UIColor.clear;
var origStartColor2:UIColor = UIColor.clear;

var defaultLineColor3:UIColor = UIColor.clear;
var origStartColor3:UIColor = UIColor.clear;

var defaultLineColor4:UIColor = UIColor.clear;
var origStartColor4:UIColor = UIColor.clear;



//var defaultLineGarage:UIColor = UIColor.clear;  //*************** change 1.1.0 remove ***
//var origStartGarage:UIColor = UIColor.clear;  //*************** change 1.1.0 remove ***
//
//var defaultLineFrontDr:UIColor = UIColor.clear;  //*************** change 1.1.0 remove ***
//var origStartFrontDr:UIColor = UIColor.clear;  //*************** change 1.1.0 remove ***


var currentToolIndex:Int = 0;
var cropButtonSize:CGFloat =  35.0
var cornerButtonSize:CGFloat =  55.0

var currSelectedPt:CGPoint = CGPoint.zero;

var hueNormal00:CGFloat =  0.0
var sat00:CGFloat =  0.0
var lum00:CGFloat =  0.0
var delta00:CGFloat =  0.0

var userNameWas = "paintApp"
var imageNameString = "imageXYZ.jpg"

var twasForModifai = true; //true; false
var shouldAddToAlbum:Bool  = true;

var screenScale:CGFloat = 1.0;

var choiceIsOriginal:Bool = false;




//------------ old stuff --------------------------------
var maskedDoorImage:UIImage?;
//var maskedDo orImage1:UIImage?; //*************** change 1.1.0 remove ***

var maskedFrontDrImage:UIImage?;
//var maskedFr ontDrImage1:UIImage?; //*************** change 1.1.0 remove ***

var theDoorsImage:UIImage?;  //*************** change 1.1.0 add ***

var defaultDoorColor0:UIColor = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.0); //*************** change 1.1.0 change ***
var doorStartColor0:UIColor = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.0); //*************** change 1.1.0 change ***

//var defaultDoorColor1:UIColor = UIColor.clear; //*************** change 1.1.0 remove ***
//var doorStartColor1:UIColor = UIColor.clear; //*************** change 1.1.0 remove ***


var defaultFrontDrColor0:UIColor = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.0); //*************** change 1.1.0 change ***
var frontDrStartColor0:UIColor = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.0); //*************** change 1.1.0 change ***

//var defaultFrontDrColor1:UIColor = UIColor.clear; //*************** change 1.1.0 remove ***
//var frontDrStartColor1:UIColor = UIColor.clear; //*************** change 1.1.0 remove ***


//--------------------------------------------------------------------------------------
class PaintViewController: UIViewController, UIGestureRecognizerDelegate,SignInClickDelegate,UITextViewDelegate
{
    @IBOutlet weak var mainBaseView: UIView!

    @IBOutlet weak var scrollView: PaintScrollView!
    @IBOutlet weak var dot1: UIImageView!
    @IBOutlet weak var dot2: UIImageView!
    @IBOutlet weak var dot3: UIImageView!

    @IBOutlet weak var processingView: UIView!
    @IBOutlet weak var animaView: UIImageView!




    //------------- overlap with Paint Overlay view-----------------
    @IBOutlet weak var cornerButton: PECButton!
    @IBOutlet weak var cutButton: UIButton!
    @IBOutlet weak var deleteDrButton: UIButton!
    //------------- end overlap with Paint Overlay view-----------------




    @IBOutlet weak var readyView: UIView!

    @IBOutlet weak var satLumView: UIView!
    @IBOutlet weak var gradientViewSaturation: ColorPickGradientView!
    @IBOutlet weak var gradientViewLuminosity: ColorPickGradientView!


    //------------- wells Paint Overlay view-----------------
//    @IBOutlet weak var colorWel lView0: UIView!
//    @IBOutlet weak var colorWel lView1: UIView!

    @IBOutlet weak var indButton0: UIButton!
    @IBOutlet weak var indButton1: UIButton!
    @IBOutlet weak var indButton2: UIButton!
    @IBOutlet weak var indButton3: UIButton!
    @IBOutlet weak var indButton4: UIButton!

    @IBOutlet weak var indicatLabel0: UILabel!
    @IBOutlet weak var indicatLabel1: UILabel!
    @IBOutlet weak var indicatLabel2: UILabel!

    @IBOutlet weak var colorWellButtonDr: UIButton!
    @IBOutlet weak var colorWellButton0: UIButton!
    @IBOutlet weak var colorWellButton1: UIButton!
    @IBOutlet weak var colorWellButton2: UIButton!
    @IBOutlet weak var colorWellButton3: UIButton!
    @IBOutlet weak var colorWellButton4: UIButton!

    @IBOutlet weak var bumpButton1: UIButton!
    @IBOutlet weak var bumpButton2: UIButton!
    @IBOutlet weak var bumpButton3: UIButton!
    @IBOutlet weak var bumpButton4: UIButton!
    //------------- end wells Paint Overlay view-----------------


    //------------- edit Paint Overlay view-----------------
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var galleryButton: UIButton!

    @IBOutlet weak var proSwitch: UISwitch!
    @IBOutlet weak var proLabel: UILabel!

    @IBOutlet weak var checkPaintButton: UIButton!
    @IBOutlet weak var editPaintButton: UIButton!
    @IBOutlet weak var cutPaintButton: UIButton!
    @IBOutlet weak var copyPaintButton: UIButton!


    @IBOutlet weak var eraserButton: UIButton!
    @IBOutlet weak var penButton: UIButton!
    @IBOutlet weak var xactoButton: UIButton!
    @IBOutlet weak var ptToptButton: UIButton!

    @IBOutlet weak var d4Button: UIButton!
    @IBOutlet weak var toolSizeSlider: UISlider!
    //------------- end edit Paint Overlay view-----------------


	@IBOutlet weak var codeNum: UITextField!    //************** change 3.0.0 add *** also added to storyboard
	@IBOutlet weak var colorName: UITextField!  //************** change 3.0.0 add *** also added to storyboard





//    var projectLi st00:[Proje ctThumb] = [Proje ctThumb]()
//    var curProjectCho iceList:[ProjectThumb] = [ProjectThumb]()

    var theButtonV0:PECButton?

    var lastInputMode: String? = "random";
    var isDictationRunning: Bool = false
    var lastKeybrdHeight: CGFloat = 0.0;

    var metadata: [String: Any] = [:]
    var asset: PHAsset?


    //------- processing screen effects -------
    let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular) //prominent,regular,extraLight, light, dark
    var blurEffectView = UIVisualEffectView()
    var dotLayers = [CALayer]()
    var dotsScale = 1.5
    var dotsCount: Int = 3

    //------- interaction with server -------
    var timer:Timer?
    var timeLeft = 20

    let paintPath:UIBezierPath = UIBezierPath.init();
    let paintLayer:CAShapeLayer = CAShapeLayer.init();

    var paintObject: HouseObject?

    var maskOfDoor = false
    var maskOfFrontDoor = false

    var isSaveDefaultImage = false
    var lastChangedImageUrl:String = ""
    var cameraImageSelected:String = ""

    var isComingFromDone = false

    var fullsize = false;
    var isPaintChanged = false
//    var thePlace:Range = 0..<1


    var isPaintView:Bool = false;
    var isEditView:Bool = false;
    var isHomeView:Bool = true;

    var assetCollection: PHAssetCollection!
//    var shouldAd dToAlbum:Bool  = true;

    var respToSize:Response?;

    var hasGoodLayer = false;

    let reviveAlbum = "Modifai Images"



    //------- paint overlay -------
    var wellBorder0:CAShapeLayer = CAShapeLayer();
    var wellBorder1:CAShapeLayer = CAShapeLayer();
    var wellBorder2:CAShapeLayer = CAShapeLayer();
    var wellBorder3:CAShapeLayer = CAShapeLayer();
    var wellBorder4:CAShapeLayer = CAShapeLayer();

    var currentWell: UIButton!;
    var currentBump: UIButton!;
    var currentOverlay: UIImageView!;
    var currentOverlay1: UIImageView!;



    var pixelArray:[PECpixel] = [PECpixel]()
    var penTag:Int = 9;


    var upperLDev:CGFloat = 1.0;
    var upperSDev:CGFloat = 1.0;
    var lowerSDev:CGFloat = 0.0;

    var slopef:CGFloat = 0.0;
    var interceptYaxis:CGFloat = 0.65;

    var slopef3:CGFloat = 0.0;
    var interceptYaxis3:CGFloat = 0.65;

    var slopefLow:CGFloat = 0.0;
    var interceptYaxisLow:CGFloat = 0.65;
    //------- end paint overlay -------




    //------------ old stuff ------------------------------
    var paintListPopUP:PaintPopupScreen!
    var editSelectedImage:Bool!
    var selectedImageName:String = ""

//    var paintIm ageName:String!

    var sourceSet = Set([garDoorSourceDdr,garDoorSourceSdr,frntDoorSourceDdr,frntDoorSourceSdr,windowSource])
    var sourceArray  = [[UIImage]]()


//    var proces sedImage: UIImage!


//    var hous eObject: Hous eObject?
    var jsonError:NSError?

    var wasChoosen:Bool    = false;
    var doorWasPicked: Bool = false;
    var whichDoor: Int = 0;


    var tempResponseArray:NSArray = NSArray()

//    var maskedHouseI1Temp:UIImage?;  //******** change 1.1.0 remove ***
//
//    var theImageOverlay0: UIImageView!  //******** change 1.1.0 remove ***
//    var theImageOverlay1: UIImageView! //******** change 1.1.0 remove ***
//    var theImageOverlay2: UIImageView! //******** change 1.1.0 remove ***


    //*************** change 0.0.30 add several ***
    var wellName0 = "_wasWell0Sel";

    var paintName0 = "_paint0";
    var paintName1 = "_paint1";
    var paintName2 = "_paint2";

    var maskName0 = "_paintMask0";
    var maskName1 = "_paintMask1";
    var maskName2 = "_paintMask2";

    var startName0 = "_startColor0";
    var startName1 = "_startColor1";
    var startName2 = "_startColor2";

    var defaultName0 = "_defaultColor0";
    var defaultName1 = "_defaultColor1";
    var defaultName2 = "_defaultColor2";


//    var maskedHouseITemp:UIImage?;   //******** change 1.1.0 remove move to here***
//    var defaultLineColor0Temp:UIColor = UIColor.clear;  //******** change 1.1.0 remove ***
//    var origStartColor0Temp:UIColor = UIColor.clear;  //******** change 1.1.0 remove ***

//    var defaultLineColor1Temp:UIColor = UIColor.clear; //******** change 1.1.0 remove ***
//    var origStartColor1Temp:UIColor = UIColor.clear; //******** change 1.1.0 remove ***
//
//    var defaultLineColor2Temp:UIColor = UIColor.clear; //******** change 1.1.0 remove ***
//    var origStartColor2Temp:UIColor = UIColor.clear; //******** change 1.1.0 remove ***

    var wasGoodLTemp:Bool = false;
    var gallIndex:Int = 0; //******** change 1.1.0 add ***





    // MARK: - -------------- load methods ---------------------------------------------
    //-------------------------------------------------------------------------------------
    override var prefersStatusBarHidden: Bool
    {
        return true
    }

//    //----------------------------------------------------------------------
//    override var prefersHomeIndicatorAutoHidden: Bool  //********** if I want delay,  can't hide this ***
//    {
//       return true
//    }

    //------------------------------------------------------------------------------------
    override var preferredScreenEdgesDeferringSystemGestures: UIRectEdge
    {
        return [.all]; //[.all];
    }


    //------------------------------------------------------------------------------------
    override func viewDidLoad()
    {
        currentPaintImageController = self;
        isThisForPaint = true
        saveButton.layer.masksToBounds = true
        saveButton.layer.cornerRadius = 20
        doneButton.layer.masksToBounds = true
        doneButton.layer.cornerRadius = 20

        super.viewDidLoad()
    }

    //------------------------------------------------------------------------------------
    override func viewWillAppear(_ animated: Bool)
    {
//print(" ")
//print("paint viewWillAppear")
        hidingViewWasPreferenceContoller = false;
        editSelectedImage = false;
        comingFromPaintTutorial = false;

        super.viewWillAppear(animated)
        resolutionOnorOff = true
        paintOnorOff = true
        navigationController?.setNavigationBarHidden(true, animated: true)

        self.setupDotLayers()
        self.processingView.isHidden = false;

        isThisForPaint = true;
        self.theButtonV0 = nil;
        hasBeenSaved = false;

        showWndwButton = false;
        currentPaintImageController = self;


        NotificationCenter.default.addObserver(self, selector: #selector(PaintViewController.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PaintViewController.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PaintViewController.currentInputModeDidChange), name: UITextInputMode.currentInputModeDidChangeNotification, object: nil)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false;


        self.mainBaseView.isUserInteractionEnabled = false;
        self.pixelArray.removeAll()

        self.scrollView.imageView.image = originalHomeImage;


        if UIDevice.current.userInterfaceIdiom == .phone
        {
            wasIpad = false;
        } else {
            wasIpad = true;
        }

		self.codeNum.layer.borderWidth = 1.0 //************** change 3.0.0 add ***
		self.colorName.layer.borderWidth = 1.0//************** change 3.0.0 add ***
		self.codeNum.layer.borderColor = UIColor.white.cgColor   //************** change 3.0.0 add ***
		self.colorName.layer.borderColor = UIColor.white.cgColor //************** change 3.0.0 add ***

        if (proOnorOff)
        {
            self.proSwitch.isOn = true;
            self.scrollView.pullBarMax = 170.0;
            self.scrollView.touchPadView.colorWheelChip.isHidden = true;
            self.satLumView.isHidden = false;
            self.scrollView.touchPadView.indicator1.isHidden = false;
            self.scrollView.touchPadView.indicator2SpinnerView.isHidden = true;
            self.scrollView.touchPadView.satLayer.isHidden = false;

            pullBarDist = 143.0;
            touchPadCenterX = 252.0;
            touchPadLimit = 256.0;

            self.scrollView.touchPadView.satLayer.frame  =  CGRect(x:84.0, y:84.0, width:512.0,
                                                                                    height:512.0);
            self.scrollView.touchPadView.satLayer.cornerRadius = 256.0;
        } else {
            self.proSwitch.isOn = false;
            self.scrollView.pullBarMax = 170.0+74.0;  //104.0
            self.scrollView.touchPadView.colorWheelChip.isHidden = false;
            self.satLumView.isHidden = false;   //true;
            self.scrollView.touchPadView.indicator1.isHidden = true;
            self.scrollView.touchPadView.indicator2SpinnerView.isHidden = false;
            self.scrollView.touchPadView.satLayer.isHidden = true;  //true;

            pullBarDist = 143.0;  // 50.0
            touchPadCenterX = 346.0;
            touchPadLimit = 340.0;

            self.scrollView.touchPadView.satLayer.frame  =  CGRect(x:0.0, y:0.0, width:680.0,
                                                                                height:680.0);
            self.scrollView.touchPadView.satLayer.cornerRadius = 340.0;
        }

        let trans:CGAffineTransform = CGAffineTransform(rotationAngle: -PIdivTwo)
        self.toolSizeSlider.transform = trans;


        self.jsonError = nil;
        currentPaintImageController = self;
        currentController = "paint"

        currentWell = colorWellButton0;
        currentBump = bumpButton1;

        isThisForPaint = true;

        navigationController?.setNavigationBarHidden(true, animated: false)

        self.configureColorWell();
//print("paint viewWil lAppear after configureColorWell currentTo olIndex:",currentTo olIndex)


        screenScale = UIScreen.main.scale
        if (originalHomeImage.size.width > 1048) || (originalHomeImage.size.height > 1048)
        {
            screenScale = 1.0;
        }


        self.scrollView.garageMaskView.image = nil;   //************** change 1.1.0 add ***
        self.scrollView.frontDrMaskView.image = nil;   //************** change 1.1.0 add ***
        self.scrollView.garageMaskView.highlightedImage = nil;   //************** change 1.1.0 add ***
        self.scrollView.frontDrMaskView.highlightedImage = nil;   //************** change 1.1.0 add ***

//print("paint viewWil lAppear self.maskO fDoor,self.maskOfFr ontDoor:",self.maskOfDoor,self.maskOfFrontDoor)

        if (self.maskOfDoor)  //************** change 1.1.0 add if block ***
        {
            defaultLineColor = defaultDoorColor0;
        } else if (self.maskOfFrontDoor) {
            defaultLineColor = defaultFrontDrColor0;
        }

//print("paint viewWil lAppear defaultDo orColor0 :",defaultDoorColor0 as Any)
//print("paint viewWil lAppear defaultL ineColor :",defaultLineColor as Any)

        if let curHome = homeviewController
        {
            self.scrollView.garageImageView.image = curHome.garageImageView.image;
            theDoorsImage = curHome.garageImageView.image;        //************** change 1.1.0 add  ***

//print("paint viewWil lAppear theDoorsImage :",theDoorsImage as Any)

//            if (self.maskOfDo or)  //************** change 1.1.0 remove ***
//            {
//                self.scrollView.frontDrMa skView.image = curHome.frontDrMa skView.image;
//
//                if (curHome.garageMa skView.image != nil)
//                {
//                    self.scrollView.garageMas kView.highlightedImage = curHome.garageM askView.image;
////print("paint viewWillAppear curHome.garageMa skView.image:",curHome.garageM askView.image as Any)
//                } else {
////print("paint viewWillAppear maskedDo orImage:",maskedDo orImage as Any)
//        //            garageMa skImage = maskedDo orImage;
////print("paint viewWillAppear garageMa skImage:",garageMa skImage as Any)
//                }
//            } else if (self.maskOfFro ntDoor) {
//
//                self.scrollView.garageMa skView.image = curHome.garageMa skView.image;
//
//                if (curHome.frontDrM askView.image != nil)
//                {
//                    self.scrollView.frontDrMa skView.image = curHome.frontDrM askView.image;
//                } else {
//                    self.scrollView.frontDrM askView.image = maskedFr ontDrImage;
//                }
//            } else {
//                self.scrollView.garageMa skView.image = curHome.garageMa skView.image;
//                self.scrollView.frontDrMas kView.image = curHome.frontDrMask View.image;
//            }

            curHome.tabBarController?.tabBar.isHidden = true
        }


        self.scrollView.contentSize = self.scrollView.containerView.frame.size
        self.scrollView.delegate = self.scrollView;
//print("0 self.scrollView.contain erView.frame:",self.scrollView.containerView.frame);

        self.scrollView.panGestureRecognizer.delaysTouchesBegan = false;
        self.scrollView.panGestureRecognizer.minimumNumberOfTouches = 2;

        self.scrollView.imageView.frame = self.view.frame;
        self.scrollView.imageContainView.frame = self.view.frame;
        self.scrollView.containerView.frame = self.view.frame;

        self.scrollView.paintImageOverlay0.frame = self.view.frame;
        self.scrollView.paintImageOverlay1.frame = self.view.frame;
        self.scrollView.paintImageOverlay2.frame = self.view.frame;
        self.scrollView.paintImageOverlay3.frame = self.view.frame;
        self.scrollView.paintImageOverlay4.frame = self.view.frame;  //++++++++++++ should this have been automatic? +++

        self.scrollView.garageImageView.frame = self.view.frame;
        self.scrollView.garageMaskView.frame = self.view.frame;
        self.scrollView.frontDrMaskView.frame = self.view.frame;

        self.scrollView.imageViewOverlay.frame = self.view.frame;

        alreadyDone = false;
        isBump = false;

        currentToolIndex = -1;
        currentButtonToDisplay = 0;
        currentWellNum = 0;
        self.currentWell = self.colorWellButton0;

    //    var tempPaintIndex = "_original"

        self.wellName0 = "_wasWell0Sel";

        self.paintName0 = "_paint0";
        self.paintName1 = "_paint1";

        self.maskName0 = "_paintMask0";
        self.maskName1 = "_paintMask1";

        self.startName0 = "_startColor0";
        self.startName1 = "_startColor1";

        self.defaultName0 = "_defaultColor0";
        self.defaultName1 = "_defaultColor1";



        if (self.maskOfDoor)
        {
//            self.maskedHo useITemp  = maskedHou seImage0;
//            self.maskedH ouseI1Temp = maskedHou seImage1;
//
//            maskedHo useImage0  = maskedDo orImage;
//            maskedHou seImage1 = maskedDo orImage1;


            self.wasGoodLTemp = wasGoodLayer;
            wasGoodLayer = true;

//            self.theImageOverlay0 = self.scrollView.garageMaskView  //******** change 1.1.0 remove ***

//            self.bumpBut ton1.isHidden = true;
//            self.saveButton.isHidden = true;
//            self.galleryButton.isHidden  = true;
//
//            self.checkPaintButton.isHidden = true;
//            self.editPaintButton.isHidden = true;
//            self.cutPaintButton.isHidden = true;
//            self.copyPaintButton.isHidden = true;


            self.wellName0 = "_doorWell0Sel";

            self.paintName0 = "_door0";
            self.paintName1 = "_door1";

            self.maskName0 = "_doorMask0";
            self.maskName1 = "_doorMask1";

            self.startName0 = "_startDrColor0";
            self.startName1 = "_startDrColor1";

            self.defaultName0 = "_defaultDrColor0";
            self.defaultName1 = "_defaultDrColor1";


//            self.defaultLin eColor0Temp  = defaultLi neColor0;
//            self.origStar tColor0Temp = origSta rtColor0;
//
//            self.defaultLineCo lor1Temp  = defaultL ineColor1;
//            self.origStartCo lor1Temp = origStartC olor1;
//
//
//            defaultLi neColor0 = defa ultDoorColor0;
//            origSt artColor0 = doorSta rtColor0;
//
//            defaultL ineColor1 = default DoorColor1;
//            origStar tColor1 = doorSta rtColor1;

            //self.colorWellBut ton0.isSelected = true;

//            if let sublayers = self.colorWellBut ton0.layer.sublayers
//            {
//                if let currentBorder = sublayers.last as? CAShapeLayer
//                {
//                    currentBorder.strokeColor = UIColor.green.cgColor;
//                }
//            }
//
//            let paintWell0Sel = currentImageName.replacingOccurrences(of:".jpg", with: "_wasWell0Sel\(tempPaintIndex).txt")
//            let paintDicWell = self.checkIfWellNumExists(filename:paintWell0Sel)
//
//            if paintDicWell["wasWell0Sel"] != nil
//            {
//                if (paintDicWell["wasWell0Sel"] as? Int == 1)
//                {
//                    self.scrollView.paintImag eOverlay0.isHidden = false;
//                }
//            }
//
//            if paintDicWell["wasWell1Sel"] != nil
//            {
//                if (paintDicWell["wasWell1Sel"] as? Int == 1)
//                {
//                    self.scrollView.paintImag eOverlay1.isHidden = false;
//                }
//            }
        } else if (self.maskOfFrontDoor) {
//print("self.maskOfFro ntDoor :",self.maskOfFr ontDoor)

//            self.maskedHo useITemp  = maskedHou seImage0;
//            self.maskedHo useI1Temp = maskedHou seImage1;
//
//            maskedHou seImage0  = maskedFro ntDrImage;
//            maskedHou seImage1 = maskedFron tDrImage1;


            self.wasGoodLTemp = wasGoodLayer;
            wasGoodLayer = true;

//            self.theImageOverlay0 = self.scrollView.frontDrMaskView  //******** change 1.1.0 remove ***

//            self.bumpBut ton1.isHidden = true;
//            self.saveButton.isHidden = true;
//            self.galleryButton.isHidden  = true;
//
//            self.checkPaintButton.isHidden = true;
//            self.editPaintButton.isHidden = true;
//            self.cutPaintButton.isHidden = true;
//            self.copyPaintButton.isHidden = true;


            self.wellName0 = "_frontDrWell0Sel";

            self.paintName0 = "_frontDr0";
            self.paintName1 = "_frontDr1";

            self.maskName0 = "_frontDrMask0";
            self.maskName1 = "_frontDrMask1";

            self.startName0 = "_startFrontDrColor0";
            self.startName1 = "_startFrontDrColor1";

            self.defaultName0 = "_defaultFrontDrColor0";
            self.defaultName1 = "_defaultFrontDrColor1";



        }


        self.imageSetup();

//print("paint viewWil lAppear after imag eSetup currentToolIndex:",currentTo olIndex)



//print("paint viewWil lAppear after changeToP aintChoice  curProjec tChoiceList.count:",curProject ChoiceList.count)

        phonePortrait = self.view.bounds.size.height > self.view.bounds.size.width;
        lastPhonePortrait = phonePortrait;
        skipConfigure = false;

        self.paintLayer.backgroundColor = UIColor.clear.cgColor;
        self.paintLayer.fillColor = UIColor.clear.cgColor;
        self.paintLayer.strokeColor = UIColor(displayP3Red: 0.0, green: 0.5, blue: 0.5, alpha: 1.0).cgColor;
        self.paintLayer.lineWidth = 20.0;

        self.scrollView.imageViewOverlay.layer.addSublayer(self.paintLayer);

        self.scrollView.touchPadView.hueAdjustFilter = CIFilter(name: "CIHueAdjust");
        self.scrollView.touchPadView.saturationFilter = CIFilter(name: "CIColorControls");

        self.scrollView.touchPadView.protractorImage.layer.addSublayer(self.scrollView.touchPadView.satLayer);

        let size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height);
        self.viewSettings(size);

//print("paint viewWil lAppear after viewSettings currentToolIndex:",currentToolIndex)


        self.gradientViewSaturation.backgroundColor = UIColor.clear;
        self.gradientViewSaturation.layer.masksToBounds = true;
        self.gradientViewSaturation.layer.cornerRadius = 5.0;
        self.gradientViewSaturation.layer.borderColor = UIColor.gray.cgColor;
        self.gradientViewSaturation.layer.borderWidth = 2.0;

        self.gradientViewLuminosity.backgroundColor = UIColor.clear;
        self.gradientViewLuminosity.layer.masksToBounds = true;
        self.gradientViewLuminosity.layer.cornerRadius = 5.0;
        self.gradientViewLuminosity.layer.borderColor = UIColor.gray.cgColor;
        self.gradientViewLuminosity.layer.borderWidth = 2.0;


//print("paint viewWil lAppear commingFromProje ctList:",commingFromProjectList)
//print("paint viewWil lAppear currPr oject:",currProject as Any)
//print("paint viewWil lAppear noProces sing:",noProcessing)

//commingFromProjectList = false;  //+++++++++++ take out :for testing +++
//noProcessing = true;  //++++++++++++ take out :for testing +++


//********** change 1.1.2 replace everthing from here down to end of function ***
        if (commingFromProjectList)
        {
//print("paint viewWil lAppear currPr oject?.wasProcessed:",currProject?.wasProcessed as Any)
            var firstMask:UIImage? = nil;
            if let data = currProject?.mask0 { firstMask = UIImage(data:data); }
//            let curProje ctChoiceList:[ProjectThumb] = self.loadThu mbChoices(currProjectThumb?.projectID)  //********** change 1.1.4 remove,  needs to be universal ***
//print("paint viewWil lAppear curProje ctChoiceList:",curProjectCho iceList as Any)

            var numthischoice = 0
            for choice in curProjectChoiceList
            {
//print("0paint choice.gal lIndex,self.gal lIndex:",choice.gallIndex,self.gallIndex)
                if (choice.gallIndex == self.gallIndex)
                {
                    numthischoice += 1;
                }
            }

//print("paint viewWil lAppear firstMask as Any,curProjectCho iceList.count:",firstMask as Any,curProjectCho iceList.count)
//print("0paint viewWil lAppear numthischoice:",numthischoice)

            if (firstMask != nil || curProjectChoiceList.count > 1)
            {
                shouldSave = false;

//print("  ")
//print("  ")
//print("1paint viewWil lAppear currGal lThumb:",currGallThumb as Any)
//print("1paint viewWil lAppear curProjectCho iceList.count:",curProjectChoiceList.count)
//print("1paint viewWil lAppear defaultDoorColor0:",defaultDoorColor0)
//print("1paint viewWil lAppear doorStartColor0:",doorStartColor0)
//print("1paint viewWil lAppear defaultLineColor:",defaultLineColor)
//print("1paint viewWil lAppear origStartColor:",origStartColor)
//print("1paint viewWil lAppear scrollEndDoor:",scrollEndDoor)
//print("1paint viewWil lAppear choiceIsOriginal:",choiceIsOriginal)
//print("1paint viewWil lAppear shouldSave:",shouldSave)
//print("  ")
//print("  ")

                if (currGallThumb != nil)
                {
                    self.changeToPaintChoice(currGallThumb);
                } else if (hasOnlyGarageDoors && numthischoice > 0) {
                    scrollEndDoor = false;
                    self.galleryClickAction(galleryButton as Any)
                } else if (curProjectChoiceList.count > 1) {
                    let projectThumb:ProjectThumb = curProjectChoiceList[curProjectChoiceList.count-1]
                    self.changeToPaintChoice(projectThumb);
                } else if (hasOnlyGarageDoors && curProjectChoiceList.count == 1) {
                    let projectThumb:ProjectThumb = curProjectChoiceList[curProjectChoiceList.count-1]
                    self.changeToPaintChoice(projectThumb);
                } else {
                    currentToolIndex = 8668;
                }

                if (currentToolIndex != 8668) {self.turnItAllOn()}
                self.finishProgressViewMain()
            } else {
                if (noProcessing || isHomeApp)   //++++++++++++ put back in :taken out for testing +++
                {
                    shouldSave = false;

//print("2paint viewWil lAppear currGal lThumb:",currGallThumb as Any)
//print("2paint viewWil lAppear curProjectCho iceList.count:",curProjectChoiceList.count)

                    if (currGallThumb != nil)
                    {
                        self.changeToPaintChoice(currGallThumb);
                    } else if (hasOnlyGarageDoors && numthischoice > 0) {
                        scrollEndDoor = false;
                        self.galleryClickAction(galleryButton as Any)
                    } else if (curProjectChoiceList.count > 1) {
                        let projectThumb:ProjectThumb = curProjectChoiceList[curProjectChoiceList.count-1]
                        self.changeToPaintChoice(projectThumb);
                    } else if (hasOnlyGarageDoors && curProjectChoiceList.count == 1) {
                        let projectThumb:ProjectThumb = curProjectChoiceList[curProjectChoiceList.count-1]
                        self.changeToPaintChoice(projectThumb);
                    } else {
                        currentToolIndex = 8668;
                    }

                    if (currentToolIndex != 8668) {self.turnItAllOn()}
                    self.finishProgressViewMain()
                } else {

                    self.fetchPaintObject
                    { (paint0Object) in
                    }
                }
            }

        } else {

            if (noProcessing || isHomeApp)  //++++++++++++ put back in :taken out for testing +++
            {
//                let curProjectCh oiceList:[ProjectThumb] = self.loadThu mbChoices(currProjectThumb?.projectID)  //********** change 1.1.4 remove,  needs to be universal ***
                shouldSave = false;

                var numthischoice = 0
                for choice in curProjectChoiceList
                {
//print("3paint choice.gal lIndex,self.gal lIndex:",choice.gallIndex,self.gallIndex)
                    if (choice.gallIndex == self.gallIndex)
                    {
                        numthischoice += 1;
                    }
                }

//print("3paint viewWil lAppear currGal lThumb:",currGal lThumb as Any)
//print("3paint viewWil lAppear curProj ectChoiceList.count:",curProjectCh oiceList.count)
//print("3paint viewWil lAppear numthischoice:",numthischoice)

                if (currGallThumb != nil)
                {
                    self.changeToPaintChoice(currGallThumb);
                } else if (hasOnlyGarageDoors && numthischoice > 0) {
                    scrollEndDoor = false;
                    self.galleryClickAction(galleryButton as Any)
                } else if (curProjectChoiceList.count > 1) {
                    let projectThumb:ProjectThumb = curProjectChoiceList[curProjectChoiceList.count-1]
                    self.changeToPaintChoice(projectThumb);
                } else if (hasOnlyGarageDoors && curProjectChoiceList.count == 1) {
                    let projectThumb:ProjectThumb = curProjectChoiceList[curProjectChoiceList.count-1]
                    self.changeToPaintChoice(projectThumb);
                } else {
                    currentToolIndex = 8668;
                }

                if (currentToolIndex != 8668) {self.turnItAllOn()}
                self.finishProgressViewMain()
            } else {

                self.fetchPaintObject
                { (paint0Object) in
                }
            }

            commingFromProjectList = true;
        }

        if isHomeApp
        {
            let editTutorial =  UserDefaults.standard.bool(forKey: "paintTutorial")
            if editTutorial
            {
                self.galleryClickAction(galleryButton as Any)
            }
        }
    }

    //------------------------------------------------------------------------------------
    override func viewDidAppear(_ animated: Bool)
    {
//print("paint view Did Appear")
        super.viewDidAppear(animated)
    }

    //------------------------------------------------------------------------------------
    override func viewWillDisappear(_ animated: Bool)
    {
//print("viewWill Disappear")

        var num0:Int = 0;
        var num1:Int = 0;

        if (self.colorWellButton0.isSelected)
        {
            num0 = 1;
        }

        if (self.colorWellButton1.isSelected)
        {
            num1 = 1;
        }


        currentMainImageController = nil;

        let dic:[String:Any] = ["wasWell0Sel" : num0,"wasWell1Sel" : num1,"wellNum":currentWellNum,"wellNumTot":currentButtonToDisplay]



        //---------------- save to default --------------------------
        // self.saveNumToDocumentDirectory(dic: dic, type:"\(self.wellName0).txt")

//        if (currentWellNum == 0) //*************** change 1.1.0 remove ***
//        {
//            defaultLi neColor0 = defaultLi neColor;
//        } else if (currentWellNum == 1) {
//            defaultLi neColor1 = defaultLi neColor;
//        } else if (currentWellNum == 2) {
//            defaultLi neColor2 = defaultLi neColor;
//        }

//print("viewWill Disappear homeviewController: ",homeviewController as Any)
        if let curHome = homeviewController
        {

            let paintMasksal0 = curHome.loadImgTypeGallery(type:self.paintName0,filename:currentImageName, ext: "png")
//print("viewWill Disappear paintMas ksal0: ",paintMasksal0)

            if paintMasksal0.count == 0
            {
                //  self.saveBut tonAction(doneBut ton)
            }

            //---------------- save at paint gallery number too --------------------------
            if paintMasksal0.count > 1
            {
                self.saveNumToDocumentDirectory(dic: dic, type:self.wellName0+selectedImageName+".txt")
            }
        }



//print("disappear self.maskO f Door :",self.maskO f Door)
//        if (self.maskOfDoor)  //*************** change 1.1.0 remove  if block ***
//        {
//    //        maskedDo orImage  = maskedHo useImage0;
//    //        maskedDo orImage1 = maskedHou seImage1; //*************** change 1.1.0 remove ***
//
//            maskedHou seImage0  = self.maskedHo useITemp;
//    //        maskedHou seImage1 = self.maskedHous eI1Temp; //*************** change 1.1.0 remove ***
//
//            wasGoodLayer = self.wasGoodLTemp;
//
//            defaultDo orColor0 = defaultL ineColor0;
//    //        doorSta rtColor0 = origStar tColor0;  //*************** change 1.1.0 remove ***
//
////            defaultD oorColor1 = defaultLi neColor1;
////            doorStar tColor1 = origSta rtColor1;
//
//
//            defaultLi neColor0 = self.defaul tLineC olor0Temp;
//            origStar tColor0 = self.origStartC olor0Temp;
//
// //............. needs looked at ...
//        } else if (self.maskOfFrontDoor) {   //*************** change 1.1.0 remove if block ***
//
////            maskedFro ntDrImage  = maskedHou seImage0;
////            maskedFro ntDrImage1 = maskedHo useImage1;
////
//            maskedHo useImage0  = self.maskedH ouseITemp;
////            maskedHou seImage1 = self.maskedH ouseI1Temp;
////
////            wasGoodLayer = self.wasGoodLTemp;
////
//            defaultFro ntDrColor0 = defaultLi neColor0;
////            frontDrSt artColor0 = origSta rtColor0;
////
////            defaultFro ntDrColor1 = defaultL ineColor1;
////            frontDrSta rtColor1 = origSt artColor1;
////
////
//            defaultLin eColor0 = self.defaultLin eColor0Temp;
//            origStart Color0 = self.origStartCol or0Temp;
////
////            defaultLi neColor1 = self.defaultLin eColor1Temp;
////            origStart Color1 = self.origStart Color1Temp;
//        }

        if self.isMovingFromParent
        {
            self.view.isHidden = true
        }

        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UITextInputMode.currentInputModeDidChangeNotification, object: nil)

        super.viewWillDisappear(animated)
    }

    //------------------------------------------------------------------------------------
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    //-------------------------------------------------------------------------------
    override var shouldAutorotate: Bool
    {
        if (dontRotate) { return false }
        return true
    }

    //-------------------------------------------------------------------------------
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        super.viewWillTransition(to: size, with: coordinator)
        viewTransition = true;

        coordinator.animate(alongsideTransition:
        { context in
            self.viewSettings(size);
        }, completion:
        { context in

        })



//        navigationController?.setNavigationBarHidden(true, animated: false)
//
//        if let curHome = homeviewController
//        {
//            curHome.tabBarController?.tabBar.isHidden = true
//        }
//
//        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
//
//        coordinator.animate(alongsideTransition:
//            { context in
//
//                var left:CGFloat = keyWindow?.safeAreaInsets.left ?? 0.0;
//                var rght:CGFloat = keyWindow?.safeAreaInsets.right ?? 0.0;
//                var top:CGFloat = keyWindow?.safeAreaInsets.top ?? 0.0;
//                var bot:CGFloat = keyWindow?.safeAreaInsets.bottom ?? 0.0;
//
//                let deviceOrientation:UIDeviceOrientation = UIDevice.current.orientation;
//
//                if (deviceOrientation == .landscapeRight)
//                {
//                    left = 0.0;
//                    if (rght > 0.0) {rght = 30.0;}
//                    top = 0.0;
//                    bot = 0.0;
//                }
//
//                if (deviceOrientation == .landscapeLeft)
//                {
//                    rght = 0.0;
//                    if (left > 0.0) {left = 30.0;}
//                    top = 0.0;
//                    bot = 0.0;
//                }
//
//                if (deviceOrientation == .portrait)
//                {
//                    top = 0.0;
//                }
//
//                if (deviceOrientation == .portraitUpsideDown)
//                {
//                    bot = 0.0;
//                }
//
//                self.mainBaseView.frame = CGRect(x:left, y:top,
//                                              width:self.view.frame.size.width  - (left + rght),
//                                              height:self.view.frame.size.height - (bot + top));
//
//        }, completion:
//            { context in
//
//        })
    }

    //-------------------------------------------------------------------------------
    func viewSettings(_ size:CGSize)
    {
        phonePortrait = size.height > size.width;
//print("phonePortrait:",phonePortrait);

//        self.vertChangeDoors.isHidden = true;
//        self.vertFrntDoors.isHidden   = true;
////        self.horzChangeDoors.isHidden = true;
////        self.horzFrntDoors.isHidden   = true;
//        self.horzWndws.isHidden      = true;
//        self.vertWndws.isHidden      = true;
//
//        self.barView.isHidden        = true;
//        self.galleryView.isHidden    = true;
//        self.optionsMiddleView.isHidden = true;
//        self.optionsBottomView.isHidden = true;
//        self.shareButtonsView.isHidden = true;
//        self.scrollView.logoContainer.isHidden = true;


        if (isPaintView || isEditView)
        {
//            self.vertContainer.isHidden  = true;
//            self.horizContainer.isHidden = true;
        } else {
            if (phonePortrait && !wasIpad)
            {
//                self.vertContainer.isHidden = true;
//                self.horizContainer.isHidden = false;
//                self.barView.isHidden        = false;
//                self.optionsMiddleView.isHidden = false;


//                if (showGarButton)
//                {
//                    self.horzChangeDoors.isHidden = false;  //++++++++ not right +++
//                }
//                if (showFrntButton)
//                {
//                    self.horzFrntDoors.isHidden   = false;
//                }
//                if (showWndwButton)
//                {
//                    self.horzWndws.isHidden         = false;
//                }

            } else {
//                self.vertContainer.isHidden  = false;
//                self.horizContainer.isHidden = true;
//                self.optionsBottomView.isHidden = false;
//                self.vertChangeDoors.isHidden = false;
//                self.vertFrntDoors.isHidden   = false;

//                if (showGarButton)
//                {
//                    self.vertChangeDoors.isHidden = false;
//                }
//                if (showFrntButton)
//                {
//                    self.vertFrntDoors.isHidden   = false;
//                }
//                if (showWndwButton)
//                {
//                    self.vertWndws.isHidden   = false;
//                }
            }

//            if (phonePortrait != lastPhonePortrait)
//            {
//                self.hhvc?.collectionView?.reloadData();
//                self.vvvc?.collectionView?.reloadData();
//            }
        }

        lastPhonePortrait = phonePortrait;

        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

        var left:CGFloat = keyWindow?.safeAreaInsets.left ?? 0.0;
        var rght:CGFloat = keyWindow?.safeAreaInsets.right ?? 0.0;
        var top:CGFloat = keyWindow?.safeAreaInsets.top ?? 0.0;
        var bot:CGFloat = keyWindow?.safeAreaInsets.bottom ?? 0.0;

        let deviceOrientation:UIDeviceOrientation = UIDevice.current.orientation;
//print(" ")
//print("UIDevice.current.orientation:",UIDevice.current.orientation)
//print("left,rght,top,bot:",left,rght,top,bot)
//print("self.view.frame:",self.view.frame)

        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.tabBarController?.tabBar.isHidden = true

        self.extendedLayoutIncludesOpaqueBars = true;
        self.edgesForExtendedLayout = [.top,.bottom];

//print("phonePortrait || wasIpad:",phonePortrait,wasIpad)
        if (phonePortrait || wasIpad)
        {
            if (deviceOrientation == .portraitUpsideDown)
            {
//print("viewSettings deviceOrientation == .portraitUpsideDown")
//                bot = 0.0;

//                if (self.isHomeView)
//                {
//                    top = 0.0;
//                    bot = 0.0;
//                    self.navigationController?.setNavigationBarHidden(false, animated: false)
//                    self.tabBarController?.tabBar.isHidden = false
//
//                    self.extendedLayoutIncludesOpaqueBars = false;
//                    self.edgesForExtendedLayout = [];
//                }
            } else {
//print("viewSettings deviceOrientation == .portrait  self.scrollView.frame:",self.scrollView.frame)
//                top = 0.0;

//                if (self.isHomeView)
//                {
//                    top = 0.0;
//                    bot = 0.0;
//                    self.navigationController?.setNavigationBarHidden(false, animated: false)
//                    self.tabBarController?.tabBar.isHidden = false
//
//                    self.extendedLayoutIncludesOpaqueBars = false;
//                    self.edgesForExtendedLayout = [];
//                }
            }

        } else {

            if (deviceOrientation == .landscapeRight)
            {
//print("viewSettings deviceOrientation == .landscapeRight")
                left = 0.0;
                if (rght > 0.0) {rght = 30.0;}
                top = 0.0;
                bot = 0.0;
            } else {
//print("viewSettings deviceOrientation == .landscapeLeft")
                rght = 0.0;
                if (left > 0.0) {left = 30.0;}
                top = 0.0;
                bot = 0.0;
            }
        }

//print("2 left,rght,top,bot:",left,rght,top,bot)

        self.mainBaseView.frame = CGRect(x:left, y:top,
                              width:self.view.frame.size.width  - (left + rght),
                             height:self.view.frame.size.height - (bot + top));

//print("2 self.mainBaseView.frame:",self.mainBaseView.frame)
//print("2 self.scrollView.frame  :",self.scrollView.frame)
    }



    // MARK: - configure
    //----------------------------------------------------------------------------------------------------------
    func configureColorWell()
    {
//print("configure Color Well");
        let colorWellBorderDr:CAShapeLayer = CAShapeLayer.init()
        let colorWellPathDr:CGPath = CGPath(rect: self.colorWellButtonDr.bounds,transform: nil);
        colorWellBorderDr.backgroundColor = UIColor.clear.cgColor;
        colorWellBorderDr.fillColor = UIColor.clear.cgColor;
        colorWellBorderDr.strokeColor = UIColor.green.cgColor;
        colorWellBorderDr.lineWidth = 2.2;
        colorWellBorderDr.path = colorWellPathDr;

        self.colorWellButtonDr.layer.addSublayer(colorWellBorderDr)

        let colorWellBorder:CAShapeLayer = CAShapeLayer.init()
        let colorWellPath:CGPath = CGPath(rect: self.colorWellButton0.bounds,transform: nil);
        colorWellBorder.backgroundColor = UIColor.clear.cgColor;
        colorWellBorder.fillColor = UIColor.clear.cgColor;
        colorWellBorder.strokeColor = UIColor.clear.cgColor;
        colorWellBorder.lineWidth = 2.2;
        colorWellBorder.path = colorWellPath;

        self.colorWellButton0.layer.addSublayer(colorWellBorder)

        let colorWellBorder1:CAShapeLayer = CAShapeLayer.init()
        let colorWellPath1:CGPath = CGPath(rect: self.colorWellButton1.bounds,transform: nil);
        colorWellBorder1.backgroundColor = UIColor.clear.cgColor;
        colorWellBorder1.fillColor = UIColor.clear.cgColor;
        colorWellBorder1.strokeColor = UIColor.clear.cgColor;
        colorWellBorder1.lineWidth = 2.2;
        colorWellBorder1.path = colorWellPath1;

        self.colorWellButton1.layer.addSublayer(colorWellBorder1)

        let colorWellBorder2:CAShapeLayer = CAShapeLayer.init()
        let colorWellPath2:CGPath = CGPath(rect: self.colorWellButton2.bounds,transform: nil);
        colorWellBorder2.backgroundColor = UIColor.clear.cgColor;
        colorWellBorder2.fillColor = UIColor.clear.cgColor;
        colorWellBorder2.strokeColor = UIColor.clear.cgColor;
        colorWellBorder2.lineWidth = 2.2;
        colorWellBorder2.path = colorWellPath2;

        self.colorWellButton2.layer.addSublayer(colorWellBorder2)

        let colorWellBorder3:CAShapeLayer = CAShapeLayer.init()
        let colorWellPath3:CGPath = CGPath(rect: self.colorWellButton3.bounds,transform: nil);
        colorWellBorder3.backgroundColor = UIColor.clear.cgColor;
        colorWellBorder3.fillColor = UIColor.clear.cgColor;
        colorWellBorder3.strokeColor = UIColor.clear.cgColor;
        colorWellBorder3.lineWidth = 2.2;
        colorWellBorder3.path = colorWellPath3;

        self.colorWellButton3.layer.addSublayer(colorWellBorder3)

        let colorWellBorder4:CAShapeLayer = CAShapeLayer.init()
        let colorWellPath4:CGPath = CGPath(rect: self.colorWellButton4.bounds,transform: nil);
        colorWellBorder4.backgroundColor = UIColor.clear.cgColor;
        colorWellBorder4.fillColor = UIColor.clear.cgColor;
        colorWellBorder4.strokeColor = UIColor.clear.cgColor;
        colorWellBorder4.lineWidth = 2.2;
        colorWellBorder4.path = colorWellPath4;

        self.colorWellButton4.layer.addSublayer(colorWellBorder4)

        if let sublayers = self.colorWellButton0.layer.sublayers
        {
            if let currentBorder = sublayers.last as? CAShapeLayer
            {
                self.wellBorder0 = currentBorder;
                self.wellBorder0.strokeColor = UIColor.clear.cgColor;
            }
        }

        if let sublayers = self.colorWellButton1.layer.sublayers
        {
            if let currentBorder = sublayers.last as? CAShapeLayer
            {
                self.wellBorder1 = currentBorder;
                self.wellBorder1.strokeColor = UIColor.clear.cgColor;
            }
        }

        if let sublayers = self.colorWellButton2.layer.sublayers
        {
            if let currentBorder = sublayers.last as? CAShapeLayer
            {
                self.wellBorder2 = currentBorder;
                self.wellBorder2.strokeColor = UIColor.clear.cgColor;
            }
        }

        if let sublayers = self.colorWellButton3.layer.sublayers
        {
            if let currentBorder = sublayers.last as? CAShapeLayer
            {
                self.wellBorder3 = currentBorder;
                self.wellBorder3.strokeColor = UIColor.clear.cgColor;
            }
        }

        if let sublayers = self.colorWellButton4.layer.sublayers
        {
            if let currentBorder = sublayers.last as? CAShapeLayer
            {
                self.wellBorder4 = currentBorder;
                self.wellBorder4.strokeColor = UIColor.clear.cgColor;
            }
        }

        self.turnItAllAllOff()
        currentButtonToDisplay = 0;
        currentWellNum = 0;

        if (self.maskOfDoor)  //*************** change 1.1.0 add if block ***
        {
            self.currentOverlay  = self.scrollView.garageMaskView;
            maskedHouseImage = maskedDoorImage;
            self.currentWell = self.colorWellButtonDr;
            self.currentBump = self.bumpButton1;
            self.colorWellButtonDr.isSelected = true;
        } else if (self.maskOfFrontDoor) {
            self.currentOverlay  = self.scrollView.frontDrMaskView;
            maskedHouseImage = maskedFrontDrImage;
            self.currentWell = self.colorWellButtonDr;
            self.currentBump = self.bumpButton1;
            self.colorWellButtonDr.isSelected = true;
        } else {
            self.currentOverlay  = self.scrollView.paintImageOverlay0;
            self.currentOverlay1 = self.scrollView.paintImageOverlay1;
            maskedHouseImage = maskedHouseImage0;

            self.currentWell = self.colorWellButton0;
            self.currentBump = self.bumpButton1;
            self.colorWellButton0.isSelected = true;
        }

        self.gradientViewSaturation.backgroundColor = UIColor.clear;
        self.gradientViewSaturation.layer.masksToBounds = true;
        self.gradientViewSaturation.layer.cornerRadius = 5.0;
        self.gradientViewSaturation.layer.borderColor = UIColor.gray.cgColor;
        self.gradientViewSaturation.layer.borderWidth = 2.0;

        self.gradientViewLuminosity.backgroundColor = UIColor.clear;
        self.gradientViewLuminosity.layer.masksToBounds = true;
        self.gradientViewLuminosity.layer.cornerRadius = 5.0;
        self.gradientViewLuminosity.layer.borderColor = UIColor.gray.cgColor;
        self.gradientViewLuminosity.layer.borderWidth = 2.0;
    }


    //----------------------------------------------------------------------------------------------------------
    func configurePaintOverlay()
    {
        self.scrollView.setUpColorWheelImage();

//print("  configurePaintOverlay origStartCo lor0:",origSt artColor0)
//print("  configurePaintOverlay defaultLin eColor0:",defaul tLineColor0)
//print("  configurePaintOverlay origStartColor1:",origStartColor1)
//print("  configurePaintOverlay defaultLi neColor1:",default LineColor1)
//print("  configurePaintOverlay garageIm ageView0:",garageImageView0 as Any)
    }



    // MARK: - Paint View Overlay - actions
    //----------------------------------------------------------------------------------------------------------
    func turnItAllOff()
    {
        self.indicatLabel0.isHidden = true;
        self.indicatLabel1.isHidden = true;
        self.indicatLabel2.isHidden = true;

        self.indButton0.isHidden = true;
        self.indButton1.isHidden = true;
        self.indButton2.isHidden = true;
        self.indButton3.isHidden = true;
        self.indButton4.isHidden = true;

        self.indButton0.isSelected = false;
        self.indButton1.isSelected = false;
        self.indButton2.isSelected = false;
        self.indButton3.isSelected = false;
        self.indButton4.isSelected = false;

        self.colorWellButtonDr.isHidden = true;
        self.colorWellButton0.isHidden = true;
        self.colorWellButton1.isHidden = true;
        self.colorWellButton2.isHidden = true;
        self.colorWellButton3.isHidden = true;
        self.colorWellButton4.isHidden = true;

        self.bumpButton1.isHidden = true;
        self.bumpButton2.isHidden = true;
        self.bumpButton3.isHidden = true;
        self.bumpButton4.isHidden = true;

        self.scrollView.paintImageOverlay0.isHidden = true;
        self.scrollView.paintImageOverlay1.isHidden = true;
        self.scrollView.paintImageOverlay2.isHidden = true;
        self.scrollView.paintImageOverlay3.isHidden = true;
        self.scrollView.paintImageOverlay4.isHidden = true;

        self.scrollView.garageImageView.isHidden = true;
        self.scrollView.garageMaskView.isHidden = true;
        self.scrollView.frontDrMaskView.isHidden = true;

        self.wellBorder0.strokeColor = UIColor.clear.cgColor;
        self.wellBorder1.strokeColor = UIColor.clear.cgColor;
        self.wellBorder2.strokeColor = UIColor.clear.cgColor;
        self.wellBorder3.strokeColor = UIColor.clear.cgColor;
        self.wellBorder4.strokeColor = UIColor.clear.cgColor;
    }


    //----------------------------------------------------------------------------------------------------------
    func turnItAllAllOff()
    {
//print("turnItAllAl lOff")
        self.turnItAllOff()

        self.readyView.isHidden = true;
//        self.homeButton.isHidden = true;

//        self.backGroundBtn.isHidden = true;
//        self.spitOutMaskBtn.isHidden = true;
//        self.combineMaskBtn.isHidden = true;

//        self.scrollView.stopButton.isHidden = true;
        self.scrollView.pullBarButton.isHidden = true;
        self.scrollView.layrBarButton.isHidden = true;
        self.scrollView.layrControlView.isHidden = true;
        self.scrollView.touchPadView.isHidden = true;
        self.satLumView.isHidden = true;

        self.codeNum.isHidden = true; //********** change 3.0.7 add ***
        self.colorName.isHidden = true; //********** change 3.0.7 add ***

        self.galleryButton.isHidden = true;
        self.saveButton.isHidden = true;
        self.doneButton.isHidden = true;
        self.proSwitch.isHidden  = true;
        self.proLabel.isHidden  = true;

        self.checkPaintButton.isHidden = true;
        self.editPaintButton.isHidden = true;
        self.cutPaintButton.isHidden = true;
        self.copyPaintButton.isHidden = true;
    }

    //----------------------------------------------------------------------------------------------------------
    func turnItAllOn()
    {
//print("turnItAl lOn")
        currentToolIndex = -1;

        self.stopAnimatingDots()
        self.processingView.isHidden = true;
        self.readyView.isHidden = true;

//        self.backGroundBtn.isHidden = false;
//        self.spitOutMaskBtn.isHidden = false;
//        self.combineMaskBtn.isHidden = false;

        self.scrollView.pullBarButton.isHidden = false;
        self.scrollView.touchPadView.isHidden = false;
        self.codeNum.isHidden = false; //********** change 3.0.7 add ***
        self.colorName.isHidden = false; //********** change 3.0.7 add ***

        if (isSecretTurnOn)
        {
            self.scrollView.layrBarButton.isHidden = false; //false; true
            self.scrollView.layrControlView.isHidden = false; //false; true
        }


        self.galleryButton.isHidden = false;
        self.saveButton.isHidden = false;
        self.doneButton.isHidden = false;
        self.proSwitch.isHidden  = false;
        self.proLabel.isHidden  = false;

        self.checkPaintButton.isHidden = true;
        self.editPaintButton.isHidden = false;
        self.cutPaintButton.isHidden = false;
        self.copyPaintButton.isHidden = true;  //********** hidden Peter request #1 *** was false;

        self.editPaintButton.isSelected = false;
        self.cutPaintButton.isSelected = false;
        self.copyPaintButton.isSelected = false;

        self.eraserButton.isSelected = false;
        self.penButton.isSelected = false;
        self.xactoButton.isSelected = false;
        self.ptToptButton.isSelected = false;


//        if (proOnorOff)  //********** exposed Peter request for beginner mode #0 ***
//        {
            self.satLumView.isHidden = false;
//        }

        self.turnOnButtons()
    }

    //----------------------------------------------------------------------------------------------------------
    func turnOnButtons()
    {
//print("turnOnBut tons")
        self.bumpButton1.isHidden = false;
        self.colorWellButton0.isHidden = false;
        self.indButton0.isHidden = false;

        self.colorWellButton0.backgroundColor = defaultLineColor0;
        self.colorWellButton1.backgroundColor = defaultLineColor1;
        self.colorWellButton2.backgroundColor = defaultLineColor2;
        self.colorWellButton3.backgroundColor = defaultLineColor3;
        self.colorWellButton4.backgroundColor = defaultLineColor4;

            //------------ change way app works for modafai ----------
            if (twasForModifai)
            {
//                self.backGroundBtn.isHidden = true;
//                self.spitOutMaskBtn.isHidden = true;
//                self.combineMaskBtn.isHidden = true;

//                if (currentButtonToDisplay > 0)    //********** hidden Peter request #1 ***
//                {
//                    self.copyPaintButton.isHidden = true;
//                } else {
//                    self.copyPaintButton.isHidden = false;
//                }
            }

            if (currentButtonToDisplay > 3)
            {
                self.copyPaintButton.isHidden = true;
            }

            if (currentButtonToDisplay > 0)
            {
//                if (twasForModifai)     //********** hidden Peter request #2 to add third layer, keep if go back to two Paint layers ***
//                {
//                    self.bumpButton2.isHidden = true;
//                } else {
//                    self.bumpButton2.isHidden = false;
//                }

                self.bumpButton2.isHidden = false;
                self.colorWellButton1.isHidden = false;
                self.indButton1.isHidden = false;
            }

            if (currentButtonToDisplay > 1)
            {
                if (twasForModifai)
                {
                    self.bumpButton3.isHidden = true;
                } else {
                    self.bumpButton3.isHidden = false;
                }

                self.colorWellButton2.isHidden = false;
                self.indButton2.isHidden = false;
            }
            //------------ end change way app works for modafai ----------

        if (currentButtonToDisplay > 2)
        {
            self.bumpButton4.isHidden = false;
            self.colorWellButton3.isHidden = false;
            self.indButton3.isHidden = false;
        }

        if (currentButtonToDisplay > 3)
        {
            self.colorWellButton4.isHidden = false;
            self.indButton4.isHidden = false;
        }


        if (currentButtonToDisplay == 3)
        {
            self.colorWellButton4.isSelected = false;
            self.colorWellButton4.isHidden = true;
            self.indButton4.isHidden = true;
            self.scrollView.paintImageOverlay4.isHidden = true;
            self.scrollView.paintImageOverlay4.image = nil;
            self.scrollView.paintImageOverlay4.highlightedImage = nil;
            maskedHouseImage4 = nil;
        } else if (currentButtonToDisplay == 2) {

            self.colorWellButton4.isSelected = false;
            self.colorWellButton4.isHidden = true;
            self.indButton4.isHidden = true;
            self.scrollView.paintImageOverlay4.isHidden = true;
            self.scrollView.paintImageOverlay4.image = nil;
            self.scrollView.paintImageOverlay4.highlightedImage = nil;
            maskedHouseImage4 = nil;

            self.colorWellButton3.isSelected = false;
            self.colorWellButton3.isHidden = true;
            self.indButton3.isHidden = true;
            self.scrollView.paintImageOverlay3.isHidden = true;
            self.scrollView.paintImageOverlay3.image = nil;
            self.scrollView.paintImageOverlay3.highlightedImage = nil;
            maskedHouseImage3 = nil;
        } else if (currentButtonToDisplay == 1) {

            self.colorWellButton4.isSelected = false;
            self.colorWellButton4.isHidden = true;
            self.indButton4.isHidden = true;
            self.scrollView.paintImageOverlay4.isHidden = true;
            self.scrollView.paintImageOverlay4.image = nil;
            self.scrollView.paintImageOverlay4.highlightedImage = nil;
            maskedHouseImage4 = nil;

            self.colorWellButton3.isSelected = false;
            self.colorWellButton3.isHidden = true;
            self.indButton3.isHidden = true;
            self.scrollView.paintImageOverlay3.isHidden = true;
            self.scrollView.paintImageOverlay3.image = nil;
            self.scrollView.paintImageOverlay3.highlightedImage = nil;
            maskedHouseImage3 = nil;

            self.colorWellButton2.isSelected = false;
            self.colorWellButton2.isHidden = true;
            self.indButton2.isHidden = true;
            self.indicatLabel2.isHidden = true;
            self.scrollView.paintImageOverlay2.isHidden = true;
            self.scrollView.paintImageOverlay2.image = nil;
            self.scrollView.paintImageOverlay2.highlightedImage = nil;
            maskedHouseImage2 = nil;
        } else if (currentButtonToDisplay == 0) {

            self.colorWellButton4.isSelected = false;
            self.colorWellButton4.isHidden = true;
            self.indButton4.isHidden = true;
            self.scrollView.paintImageOverlay4.isHidden = true;
            self.scrollView.paintImageOverlay4.image = nil;
            self.scrollView.paintImageOverlay4.highlightedImage = nil;
            maskedHouseImage4 = nil;

            self.colorWellButton3.isSelected = false;
            self.colorWellButton3.isHidden = true;
            self.indButton3.isHidden = true;
            self.scrollView.paintImageOverlay3.isHidden = true;
            self.scrollView.paintImageOverlay3.image = nil;
            self.scrollView.paintImageOverlay3.highlightedImage = nil;
            maskedHouseImage3 = nil;

            self.colorWellButton2.isSelected = false;
            self.colorWellButton2.isHidden = true;
            self.indButton2.isHidden = true;
            self.indicatLabel2.isHidden = true;
            self.scrollView.paintImageOverlay2.isHidden = true;
            self.scrollView.paintImageOverlay2.image = nil;
            self.scrollView.paintImageOverlay2.highlightedImage = nil;
            maskedHouseImage2 = nil;

            self.colorWellButton1.isSelected = false;
            self.colorWellButton1.isHidden = true;
            self.indButton1.isHidden = true;
            self.indicatLabel1.isHidden = true;
            self.scrollView.paintImageOverlay1.isHidden = true;
            self.scrollView.paintImageOverlay1.image = nil;
            self.scrollView.paintImageOverlay1.highlightedImage = nil;
            maskedHouseImage1 = nil;
        }


        if (currentWellNum == 0)
        {
            self.indButton0.isSelected = true;
            self.indicatLabel0.isHidden = false;
            self.colorWellButton0.isSelected = true;
            self.currentWell = self.colorWellButton0
            self.currentOverlay = self.scrollView.paintImageOverlay0;

        } else if (currentWellNum == 1) {
            self.indButton1.isSelected = true;
            self.indicatLabel1.isHidden = false;
            self.colorWellButton1.isSelected = true;
            self.currentWell = self.colorWellButton1
            self.currentOverlay = self.scrollView.paintImageOverlay1;

        } else if (currentWellNum == 2) {
            self.indButton2.isSelected = true;
            self.indicatLabel2.isHidden = false;
            self.colorWellButton2.isSelected = true;
            self.currentWell = self.colorWellButton2
            self.currentOverlay = self.scrollView.paintImageOverlay2;

        } else if (currentWellNum == 3) {
            self.indButton3.isSelected = true;
            self.colorWellButton3.isSelected = true;
            self.currentWell = self.colorWellButton3
            self.currentOverlay = self.scrollView.paintImageOverlay3;

        } else if (currentWellNum == 4) {
            self.indButton4.isSelected = true;
            self.colorWellButton4.isSelected = true;
            self.currentWell = self.colorWellButton4
            self.currentOverlay = self.scrollView.paintImageOverlay4;
        }

        self.scrollView.garageImageView.isHidden = false;
        self.scrollView.garageMaskView.isHidden = false;  //************** change 1.1.0 *** put back in
        self.scrollView.frontDrMaskView.isHidden = false;  //************** change 1.1.0 *** put back in
//print("turnOnButtons currentoverlay done");
//print("turnOnButtons currentoverlay self.scrollView.garageIma geView.image:",self.scrollView.garageIma geView.image as Any);

        if (self.colorWellButton0.isSelected)
        {
            self.scrollView.paintImageOverlay0.isHidden = false;
            self.wellBorder0.strokeColor = UIColor.green.cgColor;
        }
        if (self.colorWellButton1.isSelected)
        {
            self.scrollView.paintImageOverlay1.isHidden = false;
            self.wellBorder1.strokeColor = UIColor.green.cgColor;
        }
        if (self.colorWellButton2.isSelected)
        {
            self.scrollView.paintImageOverlay2.isHidden = false;
            self.wellBorder2.strokeColor = UIColor.green.cgColor;
        }
        if (self.colorWellButton3.isSelected)
        {
            self.scrollView.paintImageOverlay3.isHidden = false;
            self.wellBorder3.strokeColor = UIColor.green.cgColor;
        }
        if (self.colorWellButton4.isSelected)
        {
            self.scrollView.paintImageOverlay4.isHidden = false;
            self.wellBorder4.strokeColor = UIColor.green.cgColor;
        }

        if (self.maskOfDoor) || (self.maskOfFrontDoor)
        {
            self.scrollView.layrBarButton.isHidden = true;
            self.scrollView.layrControlView.isHidden = true;

            self.checkPaintButton.isHidden = true;
            self.editPaintButton.isHidden = true;
            self.cutPaintButton.isHidden = true;
            self.copyPaintButton.isHidden = true;


            self.indicatLabel0.isHidden = true;
            self.indicatLabel1.isHidden = true;
            self.indicatLabel2.isHidden = true;

            self.indButton0.isHidden = true;
            self.indButton1.isHidden = true;
            self.indButton2.isHidden = true;
            self.indButton3.isHidden = true;
            self.indButton4.isHidden = true;

            self.colorWellButton0.isHidden = true;
            self.colorWellButton1.isHidden = true;
            self.colorWellButton2.isHidden = true;
            self.colorWellButton3.isHidden = true;
            self.colorWellButton4.isHidden = true;

            self.bumpButton1.isHidden = true;
            self.bumpButton2.isHidden = true;
            self.bumpButton3.isHidden = true;
            self.bumpButton4.isHidden = true;

            self.colorWellButtonDr.isHidden = false;
            self.colorWellButtonDr.isSelected = true;
            self.colorWellButtonDr.backgroundColor = defaultDoorColor0;

            self.currentWell = self.colorWellButtonDr
            self.currentOverlay = self.scrollView.garageMaskView;
            currentButtonToDisplay = 0;   //************** change 1.1.0 add ***
            currentWellNum = 0;   //************** change 1.1.0 add ***

            maskedHouseImage = maskedDoorImage;   //************** change 1.1.0 change ***
            defaultLineColor = defaultDoorColor0;
            origStartColor = doorStartColor0
//print("turnOnBut tons defaultDo orColor0 :",defaultDoorColor0 as Any)
//print("turnOnBut tons defau ltLineColor :",defaultLineColor as Any)

            if (self.maskOfFrontDoor)
            {
                self.colorWellButtonDr.backgroundColor = defaultFrontDrColor0;
                self.currentOverlay = self.scrollView.frontDrMaskView;

                maskedHouseImage = maskedFrontDrImage;   //************** change 1.1.0 change ***
                defaultLineColor = defaultFrontDrColor0;
                origStartColor = frontDrStartColor0
            }
        }


        if (hasOnlyGarageDoors) //************** change 1.1.0 add if block ***
        {
            self.scrollView.paintImageOverlay0.isHidden = true;
            self.scrollView.paintImageOverlay1.isHidden = true;
            self.scrollView.paintImageOverlay2.isHidden = true;
            self.scrollView.paintImageOverlay3.isHidden = true;
            self.scrollView.paintImageOverlay4.isHidden = true;
        }
    }

    //----------------------------------------------------------------------------------------------------------
    func createOverlay()
    {
//print("creat eOver lay");
        DispatchQueue.main.async
        {
            var targetImageRect:CGRect = CGRect(x:0.0, y:0.0, width:1.0, height:1.0);

            //-----------  create overlay -----------------
            if let origImage = self.scrollView.imageView.image
            {
                targetImageRect = CGRect(x:0.0, y:0.0, width:origImage.size.width, height:origImage.size.height);

                UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);

                    origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)

                    if let mask = maskedHouseImage  //************** change 1.1.0 rename ***
                    {
                        mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0) //************** change 1.1.0 rename ***
                    }

                    self.currentOverlay.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                    self.currentOverlay.image = self.currentOverlay.highlightedImage;
//print("creat eOver lay self.currentOverlay:",self.currentOverlay as Any);
//print("creat eOver lay self.currentOverlay.image:",self.currentOverlay.image as Any);

                UIGraphicsEndImageContext();

                self.scrollView.touchPadView.updateData();
            }
        }
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func combineMask(_ sender: UIButton?)
    {
//print("combi neMask")
        if (currentButtonToDisplay >= 4) {return;}

        var tempSplash:UIImage?
//        var tempDefault:UIColor = UIColor.clear
//        var tempStart:UIColor = UIColor.clear

//        if (twasForModifai)
//        {
//            self.lineU pMask()
//
////            self.indButton0.isSelected = true;
////            self.indicatLabel0.isHidden = false;
////            self.colorWellButton0.isSelected = true;
////            self.scrollView.paintImageOverlay0.isHidden = false;
////            self.wellBorder0.strokeColor = UIColor.green.cgColor;
//
//            tempSplash = maskedHou seImage0
//            tempDefault = defaultL ineColor0
//            tempStart = origStar tColor0
//
//        } else {
//            if (currentBut tonToDisplay < 1) {return;}
//            if (currentWellNum < 1) {return;}

//            self.lineU pMask()

//            self.indButton0.isSelected = false;
//            self.indicatLabel0.isHidden = true;
//            self.colorWellButton0.isSelected = false;
//            self.scrollView.paintImageOverlay0.isHidden = true;
//            self.wellBorder0.strokeColor = UIColor.clear.cgColor;
            if (currentWellNum == 0)
            {
                tempSplash = maskedHouseImage0
//                tempDefault = defaultL ineColor0
//                tempStart = origStart Color0

            } else if (currentWellNum == 1) {
                tempSplash = maskedHouseImage1
//                tempDefault = defaultL ineColor1
//                tempStart = origStartColor1
//                self.indButton1.isSelected = false;
//                self.indicatLabel1.isHidden = true;
//                self.colorWellBut ton1.isSelected = false;
//                self.scrollView.paintImageOverlay1.isHidden = true;
//                self.wellBorder1.strokeColor = UIColor.clear.cgColor;
            } else if (currentWellNum == 2) {
                tempSplash = maskedHouseImage2
//                tempDefault = defaultL ineColor2
//                tempStart = origStartColor2
//                self.indButton2.isSelected = false;
//                self.indicatLabel2.isHidden = true;
//                self.colorWellButton2.isSelected = false;
//                self.scrollView.paintImageOverlay2.isHidden = true;
//                self.wellBorder2.strokeColor = UIColor.clear.cgColor;
            } else if (currentWellNum == 3) {
                tempSplash = maskedHouseImage3
//                tempDefault = defaultLi neColor3
//                tempStart = origStartColor3
//                self.indButton3.isSelected = false;
//                self.colorWellButton3.isSelected = false;
//                self.scrollView.paintImageOverlay3.isHidden = true;
//                self.wellBorder3.strokeColor = UIColor.clear.cgColor;
            } else if (currentWellNum == 4) {
                tempSplash = maskedHouseImage4
//                tempDefault = defaultL ineColor4
//                tempStart = origStartColor4
            }

//            currentBut tonToDisplay = currentBut tonToDisplay + 1;
//        }

        var targetImageRect:CGRect = CGRect(x:0.0, y:0.0, width:1.0, height:1.0);
        var maskCombinedExpanded:UIImage?

        if let origImage = self.scrollView.imageView.image
        {
            if let tempSplash0 = tempSplash
            {
                targetImageRect = CGRect(x:0.0, y:0.0, width:origImage.size.width, height:origImage.size.height);

                //-----------  combine the masks where they agree -----------------
                autoreleasepool
                {
                    //-----------  modify the house Mask -----------------
//print("combine Mask  house Mask:",houseMask as Any)
                    if let tempMaskHI00 = houseMask
                    {
                        //-----------  expanded combined the masks where they agree -----------------
                        UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);

                            tempSplash0.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
                            tempMaskHI00.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                            maskCombinedExpanded = UIGraphicsGetImageFromCurrentImageContext();

                        UIGraphicsEndImageContext();

                        var imageOverlayMask:UIImage?

                        //-----------  create mask -----------------
                        autoreleasepool
                        {
                            UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                                //-----------  create overlay -----------------
                                origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)

                                if let mask = maskCombinedExpanded
                                {
                                    mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                                }

                                imageOverlayMask = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage();

                            UIGraphicsEndImageContext();
                        }

                        if (currentButtonToDisplay == 0)
                        {
                            self.colorWellButton0.isSelected = true;
                            self.scrollView.paintImageOverlay0.image = imageOverlayMask
                            self.scrollView.paintImageOverlay0.highlightedImage = imageOverlayMask
                            maskedHouseImage0 = maskCombinedExpanded

                            currentWellNum = 0
                            maskedHouseImage = maskedHouseImage0;
                        } else if (currentButtonToDisplay == 1) {

                            self.colorWellButton1.isSelected = true;
                            self.scrollView.paintImageOverlay1.image = imageOverlayMask
                            self.scrollView.paintImageOverlay1.highlightedImage = imageOverlayMask
//                            origStart Color1 = tempStart;
//                            defaultLi neColor1 = tempDefault;
                            maskedHouseImage1 = maskCombinedExpanded

                            currentWellNum = 1
                            maskedHouseImage = maskedHouseImage1;
//                            default LineColor = defaultLineColor1;
//                            origSta rtColor = origStartColor1;

                        } else if (currentButtonToDisplay == 2) {

                            self.colorWellButton2.isSelected = true;
                            self.scrollView.paintImageOverlay2.image = imageOverlayMask
                            self.scrollView.paintImageOverlay2.highlightedImage = imageOverlayMask
//                            origStartColor2 = tempStart;
//                            defaultLineColor2 = tempDefault;
                            maskedHouseImage2 = maskCombinedExpanded

                            currentWellNum = 2
                            maskedHouseImage = maskedHouseImage2;
//                            defaultLi neColor = defaultLineColor2;
//                            origStart Color = origStartColor2;

                        } else if (currentButtonToDisplay == 3) {
                            self.colorWellButton3.isSelected = true;
                            self.scrollView.paintImageOverlay3.image = imageOverlayMask
                            self.scrollView.paintImageOverlay3.highlightedImage = imageOverlayMask
//                            origSta rtColor3 = tempStart;
//                            defaultL ineColor3 = tempDefault;
                            maskedHouseImage3 = maskCombinedExpanded

                            currentWellNum = 3
                            maskedHouseImage = maskedHouseImage3;
//                            defaultL ineColor = defaultLineColor3;
//                            origSta rtColor = origStartColor3;

                        } else if (currentButtonToDisplay == 4) {

                            self.colorWellButton4.isSelected = true;
                            self.scrollView.paintImageOverlay4.image = imageOverlayMask
                            self.scrollView.paintImageOverlay4.highlightedImage = imageOverlayMask
//                            origStartColor4 = tempStart;
//                            defaultLi neColor4 = tempDefault;
                            maskedHouseImage4 = maskCombinedExpanded

                            currentWellNum = 4
                            maskedHouseImage = maskedHouseImage4;
//                            defaultL ineColor = defaultLineColor4;
//                            origStar tColor = origStartColor4;
                        }

                        imageOverlayMask = nil;
                        maskCombinedExpanded = nil;
                        tempSplash = nil;

                        DispatchQueue.main.async
                        {
                            self.turnOnButtons()

                            isBump = true;
                            self.scrollView.setUpColorWheelImage()
                            self.scrollView.touchPadView.snapToOriginalColor()
                            self.valuesChanged();
                        }
                    }
                }
            }
        }
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func hideSelection(_ sender0: UIButton?)
    {
//print("12 hideS election self.editPaintBut ton.isSelected:",self.editPaintButton.isSelected)
        self.turnItAllOn()
        dragSquare = false;
        self.scrollView.deActivateAllScrollView()

        self.paintPath.removeAllPoints();
        self.paintLayer.backgroundColor = UIColor.clear.cgColor;
        self.paintLayer.fillColor = UIColor.clear.cgColor;
        self.paintLayer.path = nil;
        self.paintObject = nil;
        self.theButtonV0 = nil;

        self.scrollView.curvePts.removeAll()

        self.d4Button.isHidden = true
        self.toolSizeSlider.isHidden = true

        self.scrollView.d1Button.isHidden = true;
        self.scrollView.d2Button.isHidden = true;
        self.scrollView.deleteDimButton.isHidden =  true;
        self.scrollView.editDimButton.isHidden =  true;

        for subView in self.scrollView.imageViewOverlay.subviews
        {
            if let button:UIButton = subView as? UIButton
            {
                button.removeFromSuperview();
            }
        }
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func penSelection(_ sender0: UIButton?)
    {
        isPaintChanged = true
//print("penSele ction self.hous eObject:",houseObject as Any)
        guard let sender = sender0 else {return}

        targetMaskRect = CGRect(x:0.0, y:0.0, width:1.0, height:1.0);

        if let origImage = self.scrollView.imageView?.image
        {
            targetMaskRect = CGRect(x:0.0, y:0.0, width:origImage.size.width, height:origImage.size.height);
        } else {
            return;
        }

        wasGoodLayer = true;
        shouldSave = true;

//print("penSele ction self.penTag:",self.penTag)

        //-----------  fill with mask -----------------
        if (self.penTag == 9)
        {
            //-----------  create mask -----------------
            UIGraphicsBeginImageContextWithOptions(targetMaskRect.size, false, 1.0);
                if let context = UIGraphicsGetCurrentContext()
                {
                    self.paintLayer.fillColor = UIColor.black.cgColor;
                    self.paintLayer.strokeColor = UIColor.clear.cgColor;

                    context.setBlendMode(.normal);

                    if let masked0 = maskedHouseImage
                    {
                        masked0.draw(in: targetMaskRect)
                    }

                    self.paintLayer.render(in: context);
                    maskedHouseImage = UIGraphicsGetImageFromCurrentImageContext();
                }

                //-----------  create overlay -----------------
                if let pecImage = self.scrollView.imageView.image
                {
                    pecImage.draw(in: targetMaskRect, blendMode: .normal, alpha: 1.0)
                }

                if let masked0 = maskedHouseImage
                {
                    masked0.draw(in: targetMaskRect, blendMode: .destinationIn, alpha: 1.0)
                }

                self.currentOverlay.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                self.currentOverlay.image = self.currentOverlay.highlightedImage;
            UIGraphicsEndImageContext();

        } else if (self.penTag == 8) {

            //-----------  clear with mask -----------------
            UIGraphicsBeginImageContextWithOptions(targetMaskRect.size, false, 1.0);
                if let context = UIGraphicsGetCurrentContext()
                {
                    self.paintLayer.fillColor = UIColor.black.cgColor;
                    self.paintLayer.strokeColor = UIColor.clear.cgColor;

                    context.setBlendMode(.normal);

                    if let masked0 = maskedHouseImage
                    {
                        masked0.draw(in: targetMaskRect)
                    }

                    context.setBlendMode(.clear);
                    self.paintLayer.render(in: context);
                    maskedHouseImage = UIGraphicsGetImageFromCurrentImageContext();
                }

                //-----------  create overlay -----------------
                if let pecImage = self.scrollView.imageView.image
                {
                    pecImage.draw(in: targetMaskRect, blendMode: .normal, alpha: 1.0)
                }

                if let masked0 = maskedHouseImage
                {
                    masked0.draw(in: targetMaskRect, blendMode: .destinationIn, alpha: 1.0)
                }

                self.currentOverlay.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                self.currentOverlay.image = self.currentOverlay.highlightedImage;
            UIGraphicsEndImageContext();

        } else if (self.penTag == 7) {

            if (currentWellNum > 3) {return;}

            //-----------  create mask -----------------
            var maskedImage:UIImage? = nil;
            UIGraphicsBeginImageContextWithOptions(targetMaskRect.size, false, 1.0);
                if let context = UIGraphicsGetCurrentContext()
                {
                    context.setBlendMode(.normal);

                    self.paintLayer.fillColor = UIColor.black.cgColor;
                    self.paintLayer.strokeColor = UIColor.clear.cgColor;

                    self.paintLayer.render(in: context);
                    maskedImage = UIGraphicsGetImageFromCurrentImageContext();
                }
            UIGraphicsEndImageContext();

            UIGraphicsBeginImageContextWithOptions(targetMaskRect.size, false, 1.0);

                if let maskedHouse0 = maskedHouseImage
                {
                    maskedHouse0.draw(in: targetMaskRect, blendMode: .normal, alpha: 1.0)
                }

                if let masked0 = maskedImage
                {
                    masked0.draw(in: targetMaskRect, blendMode: .destinationIn, alpha: 1.0)
                }

                if (currentWellNum == 0)
                {
                    self.showColorWell(self.bumpButton1)
                } else if (currentWellNum == 1) {
                    self.showColorWell(self.bumpButton2)
                } else if (currentWellNum == 2) {
                    self.showColorWell(self.bumpButton3)
                } else if (currentWellNum == 3) {
                    self.showColorWell(self.bumpButton4)
                }

                maskedHouseImage = UIGraphicsGetImageFromCurrentImageContext();

                //-----------  create overlay -----------------
                if let pecImage = self.scrollView.imageView.image
                {
                    pecImage.draw(in: targetMaskRect, blendMode: .normal, alpha: 1.0)
                }

                if let masked0 = maskedHouseImage
                {
                    masked0.draw(in: targetMaskRect, blendMode: .destinationIn, alpha: 1.0)
                }

                self.readyView.isHidden = true;
                currentToolIndex = -1;

                self.currentOverlay.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                self.currentOverlay.image = self.currentOverlay.highlightedImage;
            UIGraphicsEndImageContext();

            if (currentWellNum == 0)
            {
            } else if (currentWellNum == 1) {
                origStartColor1 = origStartColor0;
                defaultLineColor1 = defaultLineColor0;
                origStartColor = origStartColor1;
                defaultLineColor = defaultLineColor1;
            } else if (currentWellNum == 2) {
                origStartColor2 = origStartColor1;
                defaultLineColor2 = defaultLineColor1;
                origStartColor = origStartColor2;
                defaultLineColor = defaultLineColor2;
            } else if (currentWellNum == 3) {
                origStartColor3 = origStartColor2;
                defaultLineColor3 = defaultLineColor2;
                origStartColor = origStartColor3;
                defaultLineColor = defaultLineColor3;
            } else if (currentWellNum == 4) {
                origStartColor4 = origStartColor3;
                defaultLineColor4 = defaultLineColor3;
                origStartColor = origStartColor4;
                defaultLineColor = defaultLineColor4;
            }
        }

        //------- reset the button and paths -------------
        isBump = true;
        self.scrollView.touchPadView.updateData();
        self.valuesChanged();
        self.scrollView.touchPadView.runThroughHueFilters();

        self.hideSelection(sender)
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func editPaintAction(_ sender: UIButton?)
    {
        guard let button = sender else {return}

        button.isSelected = !button.isSelected;
        self.lineUpMask()

//print("editPaintAction button.isSelected:",button.isSelected)
        if (button.isSelected)
        {
            self.turnItAllAllOff()

            self.scrollView.garageImageView.isHidden = false;
            self.scrollView.garageMaskView.isHidden = false;  //************** change 1.1.0 *** put back in
            self.scrollView.frontDrMaskView.isHidden = false;  //************** change 1.1.0 *** put back in

            if (self.colorWellButton0.isSelected)
            {
                self.scrollView.paintImageOverlay0.isHidden = false;
            }
            if (self.colorWellButton1.isSelected)
            {
                self.scrollView.paintImageOverlay1.isHidden = false;
            }
            if (self.colorWellButton2.isSelected)
            {
                self.scrollView.paintImageOverlay2.isHidden = false;
            }
            if (self.colorWellButton3.isSelected)
            {
                self.scrollView.paintImageOverlay3.isHidden = false;
            }
            if (self.colorWellButton4.isSelected)
            {
                self.scrollView.paintImageOverlay4.isHidden = false;
            }

            self.paintPath.removeAllPoints();
            self.paintLayer.path = nil;
            self.paintObject = nil;

            self.scrollView.wasStart = false;
            currentToolIndex = 5
            dragSquare = true;
            self.scrollView.editDimButton.tag = 0;
//print("1 editPaintAction self.scrollView.wasSt art:",self.scrollView.wasStart)


            if (button.tag == 1)
            {
                self.penTag = 9;
            } else if (button.tag == 2) {
                self.penTag = 8;
            } else if (button.tag == 3) {
                self.penTag = 7;
            }

//print("editPaintAction self.penTag:",self.penTag)

            self.paintLayer.backgroundColor = UIColor.clear.cgColor;
            self.paintLayer.fillColor = UIColor.clear.cgColor;
            self.paintLayer.strokeColor = UIColor(displayP3Red: 0.0, green: 0.5, blue: 0.5, alpha: 1.0).cgColor;
            self.paintLayer.lineJoin = CAShapeLayerLineJoin.round
            self.paintLayer.lineCap = CAShapeLayerLineCap.round

            let path:String! = Bundle.main.path(forResource: "editingRect", ofType: "json")!;

            DispatchQueue.main.asyncAfter(deadline: (.now() + .milliseconds(175)))
            {
                do
                {
                    let responseData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe);
                    let json = String(data: responseData, encoding: String.Encoding.utf8)
//print("editPaintAction json:",json as Any)
//print("editPaintAction dragSquare:",dragSquare)

                    if let j = json
                    {
                        if (dragSquare)
                        {
                            button.isHidden = false;
                            self.checkPaintButton.isHidden = false;
                            self.paintObject = HouseObject(jsonString:j)
                            self.setUpPaintPath();
                            self.scrollView.drawPaintCorners();
                        }
                    }
                } catch {
                    return
                }
            }

        } else {
            self.penSelection(button)
        }
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func hideUpSelection(_ sender: UIButton)
    {
//print("hideUpSel ection");
        if (sender.tag == 7)
        {
            var targetImageRect:CGRect = CGRect(x:0.0, y:0.0, width:1.0, height:1.0);

            if let origImage = self.scrollView.imageView.image
            {
                targetImageRect = CGRect(x:0.0, y:0.0, width:origImage.size.width, height:origImage.size.height);
            }

            //-----------  create mask -----------------
            UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                if let context = UIGraphicsGetCurrentContext()
                {
                    context.setBlendMode(.normal);

                    if let mask = maskedHouseImage
                    {
                        mask.draw(in: targetImageRect)
                    }

                    self.paintLayer.fillColor = UIColor.clear.cgColor;
                    self.paintLayer.strokeColor = UIColor.black.cgColor;

                    context.setBlendMode(.normal);
                    self.paintLayer.render(in: context);
                    maskedHouseImage = UIGraphicsGetImageFromCurrentImageContext();
                }

                //-----------  create overlay -----------------
                if let pecImage = self.scrollView.imageView.image
                {
                    pecImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
                }

                if let mask = maskedHouseImage
                {
                    mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                }

                self.currentOverlay.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                self.currentOverlay.image = self.currentOverlay.highlightedImage;
            UIGraphicsEndImageContext();

            isBump = true;
            self.scrollView.setUpColorWheelImage()
            self.scrollView.touchPadView.snapToOriginalColor()
            self.valuesChanged();

        } else if (sender.tag == 9) {
            var targetImageRect:CGRect = CGRect(x:0.0, y:0.0, width:1.0, height:1.0);
            shouldSave = true;

            if let origImage = self.scrollView.imageView.image
            {
                targetImageRect = CGRect(x:0.0, y:0.0, width:origImage.size.width, height:origImage.size.height);
            }

            //-----------  create mask -----------------
            UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                if let context = UIGraphicsGetCurrentContext()
                {
                    context.setBlendMode(.normal);

                    if let maskedHouseImage0 = maskedHouseImage
                    {
                        maskedHouseImage0.draw(in: targetImageRect)
                    }

                    self.paintLayer.fillColor = UIColor.black.cgColor;
                    self.paintLayer.strokeColor = UIColor.clear.cgColor;

                    context.setBlendMode(.normal);
                    self.paintLayer.render(in: context);
                    maskedHouseImage = UIGraphicsGetImageFromCurrentImageContext();
                }

                //-----------  create overlay -----------------
                if let pecImage = self.scrollView.imageView.image
                {
                    pecImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
                }

                if let maskedHouseImage0 = maskedHouseImage
                {
                    maskedHouseImage0.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                }

                self.currentOverlay.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                self.currentOverlay.image = self.currentOverlay.highlightedImage;
            UIGraphicsEndImageContext();

//print("hideSel ection  9 ?");

            isBump = true;
            self.scrollView.setUpColorWheelImage()
            self.scrollView.touchPadView.snapToOriginalColor()
            self.valuesChanged();
        }

        self.turnItAllOn()
        dragSquare = false;
        self.scrollView.deActivateAllScrollView()

        self.paintPath.removeAllPoints();
        self.paintLayer.backgroundColor = UIColor.clear.cgColor;
        self.paintLayer.fillColor = UIColor.clear.cgColor;
        self.paintLayer.path = nil;
        self.paintObject = nil;
        self.theButtonV0 = nil;

        self.scrollView.curvePts.removeAll()

        self.d4Button.isHidden = true
        self.toolSizeSlider.isHidden = true

        self.scrollView.d1Button.isHidden = true;
        self.scrollView.d2Button.isHidden = true;
        self.scrollView.deleteDimButton.isHidden =  true;
        self.scrollView.editDimButton.isHidden =  true;

        for subView in self.scrollView.imageViewOverlay.subviews
        {
            if let button:UIButton = subView as? UIButton
            {
                button.removeFromSuperview();
            }
        }
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func penUpSelection(_ sender: UIButton)
    {
//print("penUpSel ection");
        var targetImageRect:CGRect = CGRect(x:0.0, y:0.0, width:1.0, height:1.0);
        shouldSave = true;

        if let origImage = self.scrollView.imageView.image
        {
            targetImageRect = CGRect(x:0.0, y:0.0, width:origImage.size.width, height:origImage.size.height);
        }

        if (sender.tag == 6)
        {
            //-----------  create mask -----------------
            UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                if let context = UIGraphicsGetCurrentContext()
                {
                    context.setBlendMode(.normal);

                    if let mask = maskedHouseImage
                    {
                        mask.draw(in: targetImageRect)
                    }

                    self.paintLayer.fillColor = UIColor.clear.cgColor;
                    self.paintLayer.strokeColor = UIColor.black.cgColor;

                    context.setBlendMode(.clear);
                    self.paintLayer.render(in: context);
                    maskedHouseImage = UIGraphicsGetImageFromCurrentImageContext();
                }

                //-----------  create overlay -----------------
                if let pecImage = self.scrollView.imageView.image
                {
                    pecImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
                }

                if let mask = maskedHouseImage
                {
                    mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                }

                self.currentOverlay.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                self.currentOverlay.image = self.currentOverlay.highlightedImage;
            UIGraphicsEndImageContext();

        } else if (sender.tag == 7) {
            //-----------  create mask -----------------
            UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                if let context = UIGraphicsGetCurrentContext()
                {
                    context.setBlendMode(.normal);

                    if let mask = maskedHouseImage
                    {
                        mask.draw(in: targetImageRect)
                    }

                    self.paintLayer.fillColor = UIColor.clear.cgColor;
                    self.paintLayer.strokeColor = UIColor.black.cgColor;

                    context.setBlendMode(.normal);
                    self.paintLayer.render(in: context);
                    maskedHouseImage = UIGraphicsGetImageFromCurrentImageContext();
                }

                //-----------  create overlay -----------------
                if let pecImage = self.scrollView.imageView.image
                {
                    pecImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
                }

                if let mask = maskedHouseImage
                {
                    mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                }

                self.currentOverlay.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                self.currentOverlay.image = self.currentOverlay.highlightedImage;
            UIGraphicsEndImageContext();

        } else if (sender.tag == 8) {
            //-----------  create mask -----------------
            UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                if let context = UIGraphicsGetCurrentContext()
                {
                    context.setBlendMode(.normal);

                    if let mask = maskedHouseImage
                    {
                        mask.draw(in: targetImageRect)
                    }

                    self.paintLayer.fillColor = UIColor.black.cgColor;
                    self.paintLayer.strokeColor = UIColor.clear.cgColor;

                    context.setBlendMode(.clear);
                    self.paintLayer.render(in: context);
                    maskedHouseImage = UIGraphicsGetImageFromCurrentImageContext();
                }

                //-----------  create overlay -----------------
                if let pecImage = self.scrollView.imageView.image
                {
                    pecImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
                }

                if let mask = maskedHouseImage
                {
                    mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                }

                self.currentOverlay.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                self.currentOverlay.image = self.currentOverlay.highlightedImage;
            UIGraphicsEndImageContext();
        }

        isBump = true;
        self.scrollView.touchPadView.updateData();
        self.valuesChanged();
        self.scrollView.touchPadView.runThroughHueFilters();

        self.hideUpSelection(sender)
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func fillSelection(_ sender: UIButton)
    {
//print("fillSel ection");
        var targetImageRect:CGRect = CGRect(x:0.0, y:0.0, width:1.0, height:1.0);

        if let origImage = self.scrollView.imageView.image
        {
            targetImageRect = CGRect(x:0.0, y:0.0, width:origImage.size.width, height:origImage.size.height);
        }

        if (sender.tag == 13)
        {
            //-----------  create mask -----------------
            UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                if let context = UIGraphicsGetCurrentContext()
                {
                    self.paintLayer.fillColor = UIColor.black.cgColor;
                    self.paintLayer.strokeColor = UIColor.clear.cgColor;

                    context.setBlendMode(.normal);

                    if let maskedHouseImage0 = maskedHouseImage
                    {
                        maskedHouseImage0.draw(in: targetImageRect)
                    }
                    self.paintLayer.render(in: context);
                    maskedHouseImage = UIGraphicsGetImageFromCurrentImageContext();
                }

                //-----------  create overlay -----------------
                if let pecImage = self.scrollView.imageView.image
                {
                    pecImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
                }

                if let maskedHouseImage0 = maskedHouseImage
                {
                    maskedHouseImage0.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                }

                self.currentOverlay.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                self.currentOverlay.image = self.currentOverlay.highlightedImage;
            UIGraphicsEndImageContext();

            isBump = true;
            self.scrollView.setUpColorWheelImage()
            self.scrollView.touchPadView.snapToOriginalColor()
            self.valuesChanged();

        } else if (sender.tag == 12) {

            //-----------  create mask -----------------
            UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                if let context = UIGraphicsGetCurrentContext()
                {
                    self.paintLayer.fillColor = UIColor.black.cgColor;
                    self.paintLayer.strokeColor = UIColor.clear.cgColor;

                    context.setBlendMode(.normal);

                    if let maskedHouseImage0 = maskedHouseImage
                    {
                        maskedHouseImage0.draw(in: targetImageRect)
                    }

                    context.setBlendMode(.clear);
                    self.paintLayer.render(in: context);
                    maskedHouseImage = UIGraphicsGetImageFromCurrentImageContext();
                }

                //-----------  create overlay -----------------
                if let pecImage = self.scrollView.imageView.image
                {
                    pecImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
                }

                if let maskedHouseImage0 = maskedHouseImage
                {
                    maskedHouseImage0.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                }

                self.currentOverlay.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                self.currentOverlay.image = self.currentOverlay.highlightedImage;
            UIGraphicsEndImageContext();


            isBump = true;
            self.scrollView.setUpColorWheelImage()
            self.scrollView.touchPadView.snapToOriginalColor()
            self.valuesChanged();
        }

        //------- reset the button and paths -------------
        self.hideUpSelection(sender)
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func eraserAction(_ sender: UIButton)
    {
//print("eraser Action  self.eraserBut ton.isSelected:",self.eraserButton.isSelected);
        self.eraserButton.isSelected = !self.eraserButton.isSelected;
        self.scrollView.deActivateAllScrollView()
        self.scrollView.curvePts.removeAll()
        self.paintLayer.path = nil;

        if (self.eraserButton.isSelected)
        {
//            self.homeButton.isHidden = true;
//            self.scrollView.stopButton.isHidden = true; //false;
            self.checkPaintButton.isHidden = false;
            self.scrollView.layrBarButton.isHidden = true;
            self.scrollView.layrControlView.isHidden = true;

//            self.backGroundBtn.isHidden = true;
//            self.spitOutMaskBtn.isHidden = true;
//            self.combineMaskBtn.isHidden = true;

            self.galleryButton.isHidden = true;
            self.saveButton.isHidden = true;
            self.doneButton.isHidden = true;
            self.proSwitch.isHidden  = true;
            self.proLabel.isHidden  = true;

            self.editPaintButton.isHidden = true;
            self.cutPaintButton.isHidden = true;
            self.copyPaintButton.isHidden = true;


            self.scrollView.deleteDimButton.tag = 5;
            self.scrollView.deleteDimButton.setBackgroundImage(UIImage(named: "deleteDim.png"), for: .normal);

            self.scrollView.editDimButton.tag = 6;
            self.scrollView.editDimButton.setBackgroundImage(UIImage(named: "clearArea.png"), for: .normal);

            self.paintLayer.backgroundColor = UIColor.clear.cgColor;
            self.paintLayer.fillColor = UIColor.clear.cgColor;
            self.paintLayer.strokeColor = UIColor.white.cgColor;
            self.paintLayer.lineJoin = CAShapeLayerLineJoin.round
            self.paintLayer.lineCap = CAShapeLayerLineCap.butt

            currentToolIndex = 2
            self.scrollView.reUpSlider()
        } else {
            self.hideUpSelection(sender)
        }
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func penAction(_ sender: UIButton)
    {
//print("pen Action");
        self.penButton.isSelected = !self.penButton.isSelected;
        self.scrollView.deActivateAllScrollView()
        self.scrollView.curvePts.removeAll()
        self.paintLayer.path = nil;

        if (self.penButton.isSelected)
        {
//            self.homeButton.isHidden = true;
//            self.scrollView.stopButton.isHidden = false;
            self.checkPaintButton.isHidden = false;
            self.scrollView.layrBarButton.isHidden = true;
            self.scrollView.layrControlView.isHidden = true;

//            self.backGroundBtn.isHidden = true;
//            self.spitOutMaskBtn.isHidden = true;
//            self.combineMaskBtn.isHidden = true;

            self.galleryButton.isHidden = true;
            self.saveButton.isHidden = true;
            self.doneButton.isHidden = true;
            self.proSwitch.isHidden  = true;
            self.proLabel.isHidden  = true;

            self.editPaintButton.isHidden = true;
            self.cutPaintButton.isHidden = true;
            self.copyPaintButton.isHidden = true;

            self.scrollView.deleteDimButton.tag = 5;
            self.scrollView.deleteDimButton.setBackgroundImage(UIImage(named: "deleteDim.png"), for: .normal);

            self.scrollView.editDimButton.tag = 7;
            self.scrollView.editDimButton.setBackgroundImage(UIImage(named: "penMarker.png"), for: .normal);

            self.paintLayer.backgroundColor = UIColor.clear.cgColor;
            self.paintLayer.fillColor = UIColor.clear.cgColor;
            self.paintLayer.strokeColor = UIColor.black.cgColor;
            self.paintLayer.lineJoin = CAShapeLayerLineJoin.round
            self.paintLayer.lineCap = CAShapeLayerLineCap.butt

            currentToolIndex = 3
            self.scrollView.reUpSlider()
        } else {
            self.hideUpSelection(sender)
        }
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func xactoAction(_ sender: UIButton)
    {
//print("xacto Action  self.xactoButton.isSelected:",self.xactoButton.isSelected);
        self.xactoButton.isSelected = !self.xactoButton.isSelected;
        self.scrollView.deActivateAllScrollView()
        self.scrollView.curvePts.removeAll()
        self.paintLayer.path = nil;

        if (self.xactoButton.isSelected)
        {
//            self.homeButton.isHidden = true;
//            self.scrollView.stopButton.isHidden = true; //false;
            self.checkPaintButton.isHidden = false;
            self.scrollView.layrBarButton.isHidden = true;
            self.scrollView.layrControlView.isHidden = true;

//            self.backGroundBtn.isHidden = true;
//            self.spitOutMaskBtn.isHidden = true;
//            self.combineMaskBtn.isHidden = true;

            self.galleryButton.isHidden = true;
            self.saveButton.isHidden = true;
            self.doneButton.isHidden = true;
            self.proSwitch.isHidden  = true;
            self.proLabel.isHidden  = true;

            self.editPaintButton.isHidden = true;
            self.cutPaintButton.isHidden = true;
            self.copyPaintButton.isHidden = true;

            self.scrollView.deleteDimButton.tag = 9;
            self.scrollView.deleteDimButton.setBackgroundImage(UIImage(named: "bucketArea.png"), for: .normal);

            self.scrollView.editDimButton.tag = 8;
            self.scrollView.editDimButton.setBackgroundImage(UIImage(named: "clearArea.png"), for: .normal);

            self.paintLayer.backgroundColor = UIColor.clear.cgColor;
            self.paintLayer.fillColor = UIColor.clear.cgColor;
            self.paintLayer.strokeColor = UIColor(displayP3Red: 0.0, green: 0.5, blue: 0.5, alpha: 1.0).cgColor;
            self.paintLayer.lineJoin = CAShapeLayerLineJoin.round
            self.paintLayer.lineCap = CAShapeLayerLineCap.round

            hostRectPEC = self.scrollView.imageView.frame

            if (hostRectPEC.size.width >= hostRectPEC.size.height)
            {
                theImageScale = min(hostRectPEC.size.width / 4096.0, 1.0);
            } else {
                theImageScale = min(hostRectPEC.size.height / 4096.0, 1.0);
            }

            let lineWidth = 10.0 * min(theImageScale*1.25,1.0);

            if (self.scrollView.reverseZoomFactor > 5.5)
            {
                self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,28.0*theImageScale);
            } else if (self.scrollView.reverseZoomFactor > 3.75) {
                self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,20.0*theImageScale);
            } else if (self.scrollView.reverseZoomFactor > 2.5) {
                self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,15.0*theImageScale);
            } else if (self.scrollView.reverseZoomFactor > 1.0) {
                self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,10.0*theImageScale);
            } else {
                self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,6.0*theImageScale);
            }

            currentToolIndex = 4

        } else {
            self.hideUpSelection(sender)
        }
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func ptToptAction(_ sender: UIButton)
    {
//print("ptTop tAction");
        self.ptToptButton.isSelected = !self.ptToptButton.isSelected;
        self.scrollView.deActivateAllScrollView()
        self.scrollView.curvePts.removeAll()
        self.paintLayer.path = nil;
        dragSquare = false;

        if (self.ptToptButton.isSelected)
        {
            self.scrollView.wasStart = true;

//            self.homeButton.isHidden = true;
//            self.scrollView.stopButton.isHidden = true; //false;
            self.checkPaintButton.isHidden = false;
            self.scrollView.layrBarButton.isHidden = true;
            self.scrollView.layrControlView.isHidden = true;

//            self.backGroundBtn.isHidden = true;
//            self.spitOutMaskBtn.isHidden = true;
//            self.combineMaskBtn.isHidden = true;

            self.galleryButton.isHidden = true;
            self.saveButton.isHidden = true;
            self.doneButton.isHidden = true;
            self.proSwitch.isHidden  = true;
            self.proLabel.isHidden  = true;

            self.editPaintButton.isHidden = true;
            self.cutPaintButton.isHidden = true;
            self.copyPaintButton.isHidden = true;

            self.scrollView.deleteDimButton.tag = 7;
            self.scrollView.deleteDimButton.setBackgroundImage(UIImage(named: "bucketArea.png"), for: .normal);

            self.scrollView.editDimButton.tag = 6;
            self.scrollView.editDimButton.setBackgroundImage(UIImage(named: "clearArea.png"), for: .normal);

            self.paintLayer.backgroundColor = UIColor.clear.cgColor;
            self.paintLayer.fillColor = UIColor.clear.cgColor;
            self.paintLayer.strokeColor = UIColor.white.cgColor;
            self.paintLayer.lineJoin = CAShapeLayerLineJoin.miter
            self.paintLayer.lineCap = CAShapeLayerLineCap.butt

            self.paintPath.removeAllPoints();

            currentToolIndex = 11
            self.scrollView.reUpSlider()
        } else {
            self.hideUpSelection(sender)
        }
    }

    // MARK: - bump well
    //--------------------------------------------------------------------------------------------------------
    @IBAction func currentIndicateWell(_ sender: UIButton)
    {
//print("0 current Indicate Well currentWel lNum:",currentWellNum);
        self.lineUpMask()
        shouldSave = true;   //********** change 0.0.45 add ***

        self.indButton0.isSelected = false;
        self.indButton1.isSelected = false;
        self.indButton2.isSelected = false;
        self.indButton3.isSelected = false;
        self.indButton4.isSelected = false;

        self.indicatLabel0.isHidden = true;
        self.indicatLabel1.isHidden = true;
        self.indicatLabel2.isHidden = true;

        currentWellNum = sender.tag
        sender.isSelected = true;
        self.currentWell = nil;

        if (currentWellNum == 0)
        {
            self.indicatLabel0.isHidden = false;
            self.currentOverlay = self.scrollView.paintImageOverlay0;
            maskedHouseImage = maskedHouseImage0;
            self.colorWellButton0.isSelected = true;
            self.currentWell = self.colorWellButton0;
            defaultLineColor = defaultLineColor0;
            origStartColor = origStartColor0
            self.scrollView.paintImageOverlay0.isHidden = false;
            self.wellBorder0.strokeColor = UIColor.green.cgColor;
        } else if (currentWellNum == 1) {
            self.indicatLabel1.isHidden = false;
            self.currentOverlay = self.scrollView.paintImageOverlay1;
            maskedHouseImage = maskedHouseImage1;
            self.colorWellButton1.isSelected = true;
            self.currentWell = self.colorWellButton1;
            defaultLineColor = defaultLineColor1;
            origStartColor = origStartColor1;
            self.scrollView.paintImageOverlay1.isHidden = false;
            self.wellBorder1.strokeColor = UIColor.green.cgColor;
        } else if (currentWellNum == 2) {
            self.indicatLabel2.isHidden = false;
            self.currentOverlay = self.scrollView.paintImageOverlay2;
            maskedHouseImage = maskedHouseImage2;
            self.colorWellButton2.isSelected = true;
            self.currentWell = self.colorWellButton2;
            defaultLineColor = defaultLineColor2;
            origStartColor = origStartColor2;
            self.scrollView.paintImageOverlay2.isHidden = false;
            self.wellBorder2.strokeColor = UIColor.green.cgColor;
        } else if (currentWellNum == 3) {
            self.currentOverlay = self.scrollView.paintImageOverlay3;
            maskedHouseImage = maskedHouseImage3;
            self.colorWellButton3.isSelected = true;
            self.currentWell = self.colorWellButton3;
            defaultLineColor = defaultLineColor3;
            origStartColor = origStartColor3;
            self.scrollView.paintImageOverlay3.isHidden = false;
            self.wellBorder3.strokeColor = UIColor.green.cgColor;
        } else if (currentWellNum == 4) {
            self.currentOverlay = self.scrollView.paintImageOverlay4;
            maskedHouseImage = maskedHouseImage4;
            self.colorWellButton4.isSelected = true;
            self.currentWell = self.colorWellButton4;
            defaultLineColor = defaultLineColor4;
            origStartColor = origStartColor4;
            self.scrollView.paintImageOverlay4.isHidden = false;
            self.wellBorder4.strokeColor = UIColor.green.cgColor;
        }

        self.scrollView.setUpColorWheelImage()
        self.scrollView.touchPadView.snapToDefaultColor()
    }

    //--------------------------------------------------------------------------------------------------------
    @IBAction func currentColorWell(_ sender: UIButton)
    {
//print("current Color Well");
        let wellTag = sender.tag;
        let oldWellNum = currentWellNum;
        shouldSave = true;   //********** change 0.0.45 add ***

        if (sender.isSelected)
        {
            if (wellTag == 0)
            {
                self.indButton0.isSelected = false;
                self.indicatLabel0.isHidden = true;
                self.scrollView.paintImageOverlay0.isHidden = true;
                self.wellBorder0.strokeColor = UIColor.clear.cgColor;
            } else if (wellTag == 1) {
                self.indButton1.isSelected = false;
                self.indicatLabel1.isHidden = true;
                self.scrollView.paintImageOverlay1.isHidden = true;
                self.wellBorder1.strokeColor = UIColor.clear.cgColor;
            } else if (wellTag == 2) {
                self.indButton2.isSelected = false;
                self.indicatLabel2.isHidden = true;
                self.scrollView.paintImageOverlay2.isHidden = true;
                self.wellBorder2.strokeColor = UIColor.clear.cgColor;
            } else if (wellTag == 3) {
                self.indButton3.isSelected = false;
                self.scrollView.paintImageOverlay3.isHidden = true;
                self.wellBorder3.strokeColor = UIColor.clear.cgColor;
            } else if (wellTag == 4) {
                self.indButton4.isSelected = false;
                self.scrollView.paintImageOverlay4.isHidden = true;
                self.wellBorder4.strokeColor = UIColor.clear.cgColor;
            }

            sender.isSelected = false;

            if (currentWellNum == wellTag)
            {
                self.lineUpMask()

                currentWellNum = -1;
                self.currentOverlay = nil;
                self.currentWell = nil;

                if (self.colorWellButton4.isSelected)
                {
                    self.currentWell = self.colorWellButton4;
                    currentWellNum = 4;

                    self.currentOverlay = self.scrollView.paintImageOverlay4;
                    maskedHouseImage = maskedHouseImage4;
                    defaultLineColor = defaultLineColor4;
                    origStartColor = origStartColor4

                    self.indButton4.isSelected = true;

                } else if (self.colorWellButton3.isSelected) {
                    self.currentWell = self.colorWellButton3;
                    currentWellNum = 3;
                    self.currentOverlay = self.scrollView.paintImageOverlay3;
                    maskedHouseImage = maskedHouseImage3;
                    self.indButton3.isSelected = true;

                    defaultLineColor = defaultLineColor3;
                    origStartColor = origStartColor3

                } else if (self.colorWellButton2.isSelected) {
                    self.currentWell = self.colorWellButton2;
                    currentWellNum = 2;
                    self.currentOverlay = self.scrollView.paintImageOverlay2;
                    maskedHouseImage = maskedHouseImage2;
                    self.indButton2.isSelected = true;
                    self.indicatLabel2.isHidden = false;

                    defaultLineColor = defaultLineColor2;
                    origStartColor = origStartColor2

                } else if (self.colorWellButton1.isSelected) {
                    self.currentWell = self.colorWellButton1;
                    currentWellNum = 1;
                    self.currentOverlay = self.scrollView.paintImageOverlay1;
                    maskedHouseImage = maskedHouseImage1;
                    self.indButton1.isSelected = true;
                    self.indicatLabel1.isHidden = false;

                    defaultLineColor = defaultLineColor1;
                    origStartColor = origStartColor1

                } else if (self.colorWellButton0.isSelected) {
                    self.currentWell = self.colorWellButton0;
                    currentWellNum = 0;
                    self.currentOverlay = self.scrollView.paintImageOverlay0;
                    maskedHouseImage = maskedHouseImage0;
                    self.indButton0.isSelected = true;
                    self.indicatLabel0.isHidden = false;

                    defaultLineColor = defaultLineColor0;
                    origStartColor = origStartColor0
                }
            }

            if (currentWellNum < 0) {return;}

        } else {

            if (wellTag == 0)
            {
                self.colorWellButton0.isSelected = true;
                self.scrollView.paintImageOverlay0.isHidden = false;
                self.wellBorder0.strokeColor = UIColor.green.cgColor;

                if (currentWellNum < 0)
                {
                    self.indButton0.isSelected = true;
                    self.indicatLabel0.isHidden = false;
                    currentWellNum = 0;
                    self.currentOverlay = self.scrollView.paintImageOverlay0;
                    maskedHouseImage = maskedHouseImage0;
                    self.currentWell = self.colorWellButton0;
                    defaultLineColor = defaultLineColor0;
                    origStartColor = origStartColor0
                }

            } else if (wellTag == 1) {

                self.colorWellButton1.isSelected = true;
                self.scrollView.paintImageOverlay1.isHidden = false;
                self.wellBorder1.strokeColor = UIColor.green.cgColor;

                if (currentWellNum < 0)
                {
                    self.indButton1.isSelected = true;
                    self.indicatLabel1.isHidden = false;
                    currentWellNum = 1;
                    self.currentOverlay = self.scrollView.paintImageOverlay1;
                    maskedHouseImage = maskedHouseImage1;
                    self.currentWell = self.colorWellButton1;
                    defaultLineColor = defaultLineColor1;
                    origStartColor = origStartColor1;
                }
            } else if (wellTag == 2) {
                self.colorWellButton2.isSelected = true;
                self.scrollView.paintImageOverlay2.isHidden = false;
                self.wellBorder2.strokeColor = UIColor.green.cgColor;

                if (currentWellNum < 0)
                {
                    self.indButton2.isSelected = true;
                    self.indicatLabel2.isHidden = false;
                    currentWellNum = 2;
                    self.currentOverlay = self.scrollView.paintImageOverlay2;
                    maskedHouseImage = maskedHouseImage2;
                    self.currentWell = self.colorWellButton2;
                    defaultLineColor = defaultLineColor2;
                    origStartColor = origStartColor2;
                }
            } else if (wellTag == 3) {
                self.colorWellButton3.isSelected = true;
                self.scrollView.paintImageOverlay3.isHidden = false;
                self.wellBorder3.strokeColor = UIColor.green.cgColor;

                if (currentWellNum < 0)
                {
                    self.indButton3.isSelected = true;
                    currentWellNum = 3;
                    self.currentOverlay = self.scrollView.paintImageOverlay3;
                    maskedHouseImage = maskedHouseImage3;
                    self.currentWell = self.colorWellButton3;
                    defaultLineColor = defaultLineColor3;
                    origStartColor = origStartColor3;
                }
            } else if (wellTag == 4) {
                self.colorWellButton4.isSelected = true;
                self.scrollView.paintImageOverlay4.isHidden = false;
                self.wellBorder4.strokeColor = UIColor.green.cgColor;

                if (currentWellNum < 0)
                {
                    self.indButton4.isSelected = true;
                    currentWellNum = 4;
                    self.currentOverlay = self.scrollView.paintImageOverlay4;
                    maskedHouseImage = maskedHouseImage4;
                    self.currentWell = self.colorWellButton4;
                    defaultLineColor = defaultLineColor4;
                    origStartColor = origStartColor4;
                }
            }
        }

        if (self.currentWell != nil)
        {
            if (oldWellNum != currentWellNum)
            {
                self.scrollView.setUpColorWheelImage()
                self.scrollView.touchPadView.snapToDefaultColor()
            }
        }
    }

    //--------------------------------------------------------------------------------------------------------
    @IBAction func showColorWell(_ sender: UIButton?)
    {
//print("show Color Well")
        self.lineUpMask()
        self.turnItAllAllOff()
        shouldSave = true;   //********** change 0.0.45 add ***

        self.readyView.isHidden = false;
        currentToolIndex = 8668;
    //    commingFromProjectList = false;
//print("paint showColorWell commingFro mProjectList  :",commingFromProjectList)

        if let sender0 = sender
        {
            currentButtonToDisplay = sender0.tag;
        } else {
            currentButtonToDisplay = 0
        }

        currentWellNum = currentButtonToDisplay;
        self.currentWell = self.colorWellButton0;
        self.scrollView.touchPadView.pecImage = nil;

        if (currentWellNum == 0)
        {
            self.currentWell = self.colorWellButton0;

            self.colorWellButton0.isSelected = true;
            self.indButton0.isSelected = true;
            self.scrollView.paintImageOverlay0.image = nil;
            self.scrollView.paintImageOverlay0.highlightedImage = nil;  //........... keep original?  can't decide on that.
            self.currentOverlay = self.scrollView.paintImageOverlay0;
            maskedHouseImage = maskedHouseImage0;
            self.wellBorder0.strokeColor = UIColor.green.cgColor;

            origStartColor0 = UIColor.clear;
            defaultLineColor0 = UIColor.clear;

            if (houseMask == nil)
            {
                houseMask = maskedHouseImage0  //............. new test ...
            }

        } else if (currentWellNum == 1) {
            self.currentWell = self.colorWellButton1;

            self.colorWellButton1.isSelected = true;
            self.indButton1.isSelected = true;
            self.scrollView.paintImageOverlay1.image = nil;
            self.scrollView.paintImageOverlay1.highlightedImage = nil;
            self.currentOverlay = self.scrollView.paintImageOverlay1;
            maskedHouseImage = maskedHouseImage1;
            self.wellBorder1.strokeColor = UIColor.green.cgColor;

            origStartColor1 = UIColor.clear;
            defaultLineColor1 = UIColor.clear;

        } else if (currentWellNum == 2) {
            self.currentWell = self.colorWellButton2;

            self.colorWellButton2.isSelected = true;
            self.indButton2.isSelected = true;
            self.scrollView.paintImageOverlay2.image = nil;
            self.scrollView.paintImageOverlay2.highlightedImage = nil;
            self.currentOverlay = self.scrollView.paintImageOverlay2;
            maskedHouseImage = maskedHouseImage2;
            self.wellBorder2.strokeColor = UIColor.green.cgColor;

            origStartColor2 = UIColor.clear;
            defaultLineColor2 = UIColor.clear;
        } else if (currentWellNum == 3) {
            self.currentWell = self.colorWellButton3;

            self.colorWellButton3.isSelected = true;
            self.indButton3.isSelected = true;
            self.scrollView.paintImageOverlay3.image = nil;
            self.scrollView.paintImageOverlay3.highlightedImage = nil;
            self.currentOverlay = self.scrollView.paintImageOverlay3;
            maskedHouseImage = maskedHouseImage3;
            self.wellBorder3.strokeColor = UIColor.green.cgColor;

            origStartColor3 = UIColor.clear;
            defaultLineColor3 = UIColor.clear;
        } else if (currentWellNum == 4) {
            self.currentWell = self.colorWellButton4;

            self.colorWellButton4.isSelected = true;
            self.indButton4.isSelected = true;
            self.scrollView.paintImageOverlay4.image = nil;
            self.scrollView.paintImageOverlay4.highlightedImage = nil;
            self.currentOverlay = self.scrollView.paintImageOverlay4;
            maskedHouseImage = maskedHouseImage4;
            self.wellBorder4.strokeColor = UIColor.green.cgColor;

            origStartColor4 = UIColor.clear;
            defaultLineColor4 = UIColor.clear;
        }
    }

    // MARK: - well gestures
    //----------------------------------------------------------------------------------------------------------
    @IBAction func longTapWell0Action(_ recognizer: UILongPressGestureRecognizer)
    {
        switch (recognizer.state)
        {
            case UIGestureRecognizer.State.possible:
            break;
            case UIGestureRecognizer.State.began:

                //-------------- delete a paint layer,  move rest of layers down -----------------------------
                if (currentButtonToDisplay < 1) {return;}

                self.lineUpMask()
                currentButtonToDisplay = currentButtonToDisplay - 1;
                currentWellNum = currentWellNum - 1

                self.colorWellButton0.isSelected = self.colorWellButton1.isSelected;
                self.scrollView.paintImageOverlay0.image = self.scrollView.paintImageOverlay1.image;
                self.scrollView.paintImageOverlay0.highlightedImage = self.scrollView.paintImageOverlay1.highlightedImage;
                maskedHouseImage0 = maskedHouseImage1;
                defaultLineColor0 = defaultLineColor1;
                origStartColor0 = origStartColor1


                if (currentButtonToDisplay > 0)
                {
                    self.colorWellButton1.isSelected = self.colorWellButton2.isSelected;
                    self.scrollView.paintImageOverlay1.image    = self.scrollView.paintImageOverlay2.image;
                    self.scrollView.paintImageOverlay1.highlightedImage = self.scrollView.paintImageOverlay2.highlightedImage;
                    maskedHouseImage1 = maskedHouseImage2;
                    defaultLineColor1 = defaultLineColor2;
                    origStartColor1 = origStartColor2

                    if (currentWellNum == 1)
                    {
                        maskedHouseImage = maskedHouseImage1;
                        defaultLineColor = defaultLineColor1;
                        origStartColor = origStartColor1
                    }

                    if (currentButtonToDisplay > 1)
                    {
                        self.colorWellButton2.isSelected = self.colorWellButton3.isSelected;
                        self.scrollView.paintImageOverlay2.image    = self.scrollView.paintImageOverlay3.image;
                        self.scrollView.paintImageOverlay2.highlightedImage = self.scrollView.paintImageOverlay3.highlightedImage;
                        maskedHouseImage2 = maskedHouseImage3;
                        defaultLineColor2 = defaultLineColor3;
                        origStartColor2 = origStartColor3

                        if (currentWellNum == 2)
                        {
                            maskedHouseImage = maskedHouseImage2;
                            defaultLineColor = defaultLineColor2;
                            origStartColor = origStartColor2
                        }
                    }

                    if (currentButtonToDisplay > 2)
                    {
                        self.colorWellButton3.isSelected = self.colorWellButton4.isSelected;
                        self.scrollView.paintImageOverlay3.image    = self.scrollView.paintImageOverlay4.image;
                        self.scrollView.paintImageOverlay3.highlightedImage = self.scrollView.paintImageOverlay4.highlightedImage;
                        maskedHouseImage3 = maskedHouseImage4;
                        defaultLineColor3 = defaultLineColor4;
                        origStartColor3 = origStartColor4

                        if (currentWellNum == 3)
                        {
                            maskedHouseImage = maskedHouseImage3;
                            defaultLineColor = defaultLineColor3;
                            origStartColor = origStartColor3
                        }
                    }
                }

                if (currentWellNum == 0)
                {
                    maskedHouseImage = maskedHouseImage0;
                    defaultLineColor = defaultLineColor0;
                    origStartColor = origStartColor0
                } else if (currentWellNum < 0) {

                    self.currentOverlay = nil;
                    self.currentWell = nil;
                    maskedHouseImage = nil;

                    if (self.colorWellButton3.isSelected)
                    {
                        currentWellNum = 3;
                        maskedHouseImage = maskedHouseImage3;
                        defaultLineColor = defaultLineColor3;
                        origStartColor = origStartColor3

                    } else if (self.colorWellButton2.isSelected) {
                        currentWellNum = 2;
                        maskedHouseImage = maskedHouseImage2;
                        defaultLineColor = defaultLineColor2;
                        origStartColor = origStartColor2

                    } else if (self.colorWellButton1.isSelected) {
                        currentWellNum = 1;
                        maskedHouseImage = maskedHouseImage1;
                        defaultLineColor = defaultLineColor1;
                        origStartColor = origStartColor1

                    } else if (self.colorWellButton0.isSelected) {
                        currentWellNum = 0;
                        maskedHouseImage = maskedHouseImage0;
                        defaultLineColor = defaultLineColor0;
                        origStartColor = origStartColor0
                    }
                }

                self.turnItAllOff()
                self.turnOnButtons()

                self.scrollView.setUpColorWheelImage()
                self.scrollView.touchPadView.snapToDefaultColor()

            break;
            case UIGestureRecognizer.State.changed:
            break;
            case UIGestureRecognizer.State.ended:
            break;
            case UIGestureRecognizer.State.failed:
            break;
            case UIGestureRecognizer.State.cancelled:
            break;
            default:
            break;
        }
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func longTapWell1Action(_ recognizer: UILongPressGestureRecognizer)
    {
        switch (recognizer.state)
        {
            case UIGestureRecognizer.State.possible:
            break;
            case UIGestureRecognizer.State.began:

                //-------------- delete a paint layer,  move rest of layers down -----------------------------
                if (currentButtonToDisplay < 1) {return;}

                self.lineUpMask()
                currentButtonToDisplay = currentButtonToDisplay - 1;

                self.colorWellButton1.isSelected = self.colorWellButton2.isSelected;
                self.scrollView.paintImageOverlay1.image    = self.scrollView.paintImageOverlay2.image;
                self.scrollView.paintImageOverlay1.highlightedImage = self.scrollView.paintImageOverlay2.highlightedImage;
                maskedHouseImage1 = maskedHouseImage2;
                defaultLineColor1 = defaultLineColor2;
                origStartColor1 = origStartColor2

                if (currentWellNum == 0)
                {
                } else if (currentWellNum == 1) {
                    currentWellNum = -1
                } else {
                    currentWellNum = currentWellNum - 1
                }

                if (currentButtonToDisplay > 1)
                {
                    self.colorWellButton2.isSelected = self.colorWellButton3.isSelected;
                    self.scrollView.paintImageOverlay2.image    = self.scrollView.paintImageOverlay3.image;
                    self.scrollView.paintImageOverlay2.highlightedImage = self.scrollView.paintImageOverlay3.highlightedImage;
                    maskedHouseImage2 = maskedHouseImage3;
                    defaultLineColor2 = defaultLineColor3;
                    origStartColor2 = origStartColor3

                    if (currentWellNum == 2)
                    {
                        maskedHouseImage = maskedHouseImage2;
                        defaultLineColor = defaultLineColor2;
                        origStartColor = origStartColor2
                    }

                    if (currentButtonToDisplay > 2)
                    {
                        self.colorWellButton3.isSelected = self.colorWellButton4.isSelected;
                        self.scrollView.paintImageOverlay3.image    = self.scrollView.paintImageOverlay4.image;
                        self.scrollView.paintImageOverlay3.highlightedImage = self.scrollView.paintImageOverlay4.highlightedImage;
                        maskedHouseImage3 = maskedHouseImage4;
                        defaultLineColor3 = defaultLineColor4;
                        origStartColor3 = origStartColor4

                        if (currentWellNum == 3)
                        {
                            maskedHouseImage = maskedHouseImage3;
                            defaultLineColor = defaultLineColor3;
                            origStartColor = origStartColor3
                        }
                    }
                }

                if (currentWellNum < 0)
                {
                    self.currentOverlay = nil;
                    self.currentWell = nil;
                    maskedHouseImage = nil;

                    if (self.colorWellButton3.isSelected)
                    {
                        currentWellNum = 3;
                        maskedHouseImage = maskedHouseImage3;
                        defaultLineColor = defaultLineColor3;
                        origStartColor = origStartColor3

                    } else if (self.colorWellButton2.isSelected) {
                        currentWellNum = 2;
                        maskedHouseImage = maskedHouseImage2;
                        defaultLineColor = defaultLineColor2;
                        origStartColor = origStartColor2

                    } else if (self.colorWellButton1.isSelected) {
                        currentWellNum = 1;
                        maskedHouseImage = maskedHouseImage1;
                        defaultLineColor = defaultLineColor1;
                        origStartColor = origStartColor1

                    } else if (self.colorWellButton0.isSelected) {
                        currentWellNum = 0;
                        maskedHouseImage = maskedHouseImage0;
                        defaultLineColor = defaultLineColor0;
                        origStartColor = origStartColor0
                    }
                }

                self.turnItAllOff()
                self.turnOnButtons()

                self.scrollView.setUpColorWheelImage()
                self.scrollView.touchPadView.snapToDefaultColor()

            break;
            case UIGestureRecognizer.State.changed:
            break;
            case UIGestureRecognizer.State.ended:
            break;
            case UIGestureRecognizer.State.failed:
            break;
            case UIGestureRecognizer.State.cancelled:
            break;
            default:
            break;
        }
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func longTapWell2Action(_ recognizer: UILongPressGestureRecognizer)
    {
        switch (recognizer.state)
        {
            case UIGestureRecognizer.State.possible:
            break;
            case UIGestureRecognizer.State.began:

                //-------------- delete a paint layer,  move rest of layers down -----------------------------
                if (currentButtonToDisplay < 1) {return;}

                self.lineUpMask()
                currentButtonToDisplay = currentButtonToDisplay - 1;

                self.colorWellButton2.isSelected = self.colorWellButton3.isSelected;
                self.scrollView.paintImageOverlay2.image    = self.scrollView.paintImageOverlay3.image;
                self.scrollView.paintImageOverlay2.highlightedImage = self.scrollView.paintImageOverlay3.highlightedImage;
                maskedHouseImage2 = maskedHouseImage3;
                defaultLineColor2 = defaultLineColor3;
                origStartColor2 = origStartColor3

                if (currentWellNum < 2)
                {
                } else if (currentWellNum == 2) {
                    currentWellNum = -1
                } else {
                    currentWellNum = currentWellNum - 1
                }

                if (currentWellNum == 2)
                {
                    maskedHouseImage = maskedHouseImage2;
                    defaultLineColor = defaultLineColor2;
                    origStartColor = origStartColor2
                }

                if (currentButtonToDisplay > 2)
                {
                    self.colorWellButton3.isSelected = self.colorWellButton4.isSelected;
                    self.scrollView.paintImageOverlay3.image    = self.scrollView.paintImageOverlay4.image;
                    self.scrollView.paintImageOverlay3.highlightedImage = self.scrollView.paintImageOverlay4.highlightedImage;
                    maskedHouseImage3 = maskedHouseImage4;
                    defaultLineColor3 = defaultLineColor4;
                    origStartColor3 = origStartColor4

                    if (currentWellNum == 3)
                    {
                        maskedHouseImage = maskedHouseImage3;
                        defaultLineColor = defaultLineColor3;
                        origStartColor = origStartColor3
                    }
                }

                if (currentWellNum < 0)
                {
                    self.currentOverlay = nil;
                    self.currentWell = nil;
                    maskedHouseImage = nil;

                    if (self.colorWellButton3.isSelected)
                    {
                        currentWellNum = 3;
                        maskedHouseImage = maskedHouseImage3;
                        defaultLineColor = defaultLineColor3;
                        origStartColor = origStartColor3

                    } else if (self.colorWellButton2.isSelected) {
                        currentWellNum = 2;
                        maskedHouseImage = maskedHouseImage2;
                        defaultLineColor = defaultLineColor2;
                        origStartColor = origStartColor2

                    } else if (self.colorWellButton1.isSelected) {
                        currentWellNum = 1;
                        maskedHouseImage = maskedHouseImage1;
                        defaultLineColor = defaultLineColor1;
                        origStartColor = origStartColor1

                    } else if (self.colorWellButton0.isSelected) {
                        currentWellNum = 0;
                        maskedHouseImage = maskedHouseImage0;
                        defaultLineColor = defaultLineColor0;
                        origStartColor = origStartColor0
                    }
                }

                self.turnItAllOff()
                self.turnOnButtons()

                self.scrollView.setUpColorWheelImage()
                self.scrollView.touchPadView.snapToDefaultColor()

            break;
            case UIGestureRecognizer.State.changed:
            break;
            case UIGestureRecognizer.State.ended:
            break;
            case UIGestureRecognizer.State.failed:
            break;
            case UIGestureRecognizer.State.cancelled:
            break;
            default:
            break;
        }
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func longTapWell3Action(_ recognizer: UILongPressGestureRecognizer)
    {
        switch (recognizer.state)
        {
            case UIGestureRecognizer.State.possible:
            break;
            case UIGestureRecognizer.State.began:

                //-------------- delete a paint layer,  move rest of layers down -----------------------------
                if (currentButtonToDisplay < 1) {return;}

                self.lineUpMask()
                currentButtonToDisplay = currentButtonToDisplay - 1;

                self.colorWellButton3.isSelected = self.colorWellButton4.isSelected;
                self.scrollView.paintImageOverlay3.image    = self.scrollView.paintImageOverlay4.image;
                self.scrollView.paintImageOverlay3.highlightedImage = self.scrollView.paintImageOverlay4.highlightedImage;
                maskedHouseImage3 = maskedHouseImage4;
                defaultLineColor3 = defaultLineColor4;
                origStartColor3 = origStartColor4

                if (currentWellNum < 3)
                {
                } else if (currentWellNum == 3) {
                    currentWellNum = -1
                } else {
                    currentWellNum = currentWellNum - 1
                }

                if (currentWellNum == 3)
                {
                    maskedHouseImage = maskedHouseImage3;
                    defaultLineColor = defaultLineColor3;
                    origStartColor = origStartColor3
                }

                if (currentWellNum < 0)
                {
                    self.currentOverlay = nil;
                    self.currentWell = nil;
                    maskedHouseImage = nil;

                    if (self.colorWellButton3.isSelected)
                    {
                        currentWellNum = 3;
                        maskedHouseImage = maskedHouseImage3;
                        defaultLineColor = defaultLineColor3;
                        origStartColor = origStartColor3

                    } else if (self.colorWellButton2.isSelected) {
                        currentWellNum = 2;
                        maskedHouseImage = maskedHouseImage2;
                        defaultLineColor = defaultLineColor2;
                        origStartColor = origStartColor2

                    } else if (self.colorWellButton1.isSelected) {
                        currentWellNum = 1;
                        maskedHouseImage = maskedHouseImage1;
                        defaultLineColor = defaultLineColor1;
                        origStartColor = origStartColor1

                    } else if (self.colorWellButton0.isSelected) {
                        currentWellNum = 0;
                        maskedHouseImage = maskedHouseImage0;
                        defaultLineColor = defaultLineColor0;
                        origStartColor = origStartColor0
                    }
                }

                self.turnItAllOff()
                self.turnOnButtons()

                self.scrollView.setUpColorWheelImage()
                self.scrollView.touchPadView.snapToDefaultColor()

            break;
            case UIGestureRecognizer.State.changed:
            break;
            case UIGestureRecognizer.State.ended:
            break;
            case UIGestureRecognizer.State.failed:
            break;
            case UIGestureRecognizer.State.cancelled:
            break;
            default:
            break;
        }
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func longTapWell4Action(_ recognizer: UILongPressGestureRecognizer)
    {
        switch (recognizer.state)
        {
            case UIGestureRecognizer.State.possible:
            break;
            case UIGestureRecognizer.State.began:

                //-------------- delete a paint layer,  move rest of layers down -----------------------------
                if (currentButtonToDisplay < 1) {return;}

                self.lineUpMask()
                currentButtonToDisplay = currentButtonToDisplay - 1;

                if (currentWellNum == 4)
                {
                    currentWellNum = -1
                    self.currentOverlay = nil;
                    self.currentWell = nil;
                    maskedHouseImage = nil;

                    if (self.colorWellButton3.isSelected)
                    {
                        currentWellNum = 3;
                        maskedHouseImage = maskedHouseImage3;
                        defaultLineColor = defaultLineColor3;
                        origStartColor = origStartColor3

                    } else if (self.colorWellButton2.isSelected) {
                        currentWellNum = 2;
                        maskedHouseImage = maskedHouseImage2;
                        defaultLineColor = defaultLineColor2;
                        origStartColor = origStartColor2

                    } else if (self.colorWellButton1.isSelected) {
                        currentWellNum = 1;
                        maskedHouseImage = maskedHouseImage1;
                        defaultLineColor = defaultLineColor1;
                        origStartColor = origStartColor1

                    } else if (self.colorWellButton0.isSelected) {
                        currentWellNum = 0;
                        maskedHouseImage = maskedHouseImage0;
                        defaultLineColor = defaultLineColor0;
                        origStartColor = origStartColor0
                    }
                }

                self.turnItAllOff()
                self.turnOnButtons()

                self.scrollView.setUpColorWheelImage()
                self.scrollView.touchPadView.snapToDefaultColor()

            break;
            case UIGestureRecognizer.State.changed:
            break;
            case UIGestureRecognizer.State.ended:
            break;
            case UIGestureRecognizer.State.failed:
            break;
            case UIGestureRecognizer.State.cancelled:
            break;
            default:
            break;
        }
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func doubleTapWell1Action(_ recognizer: UITapGestureRecognizer)
    {
        //-------------- merge paint layer to layer below -----------------------------
        if (currentButtonToDisplay < 1) {return;}

        self.lineUpMask()
        currentButtonToDisplay = currentButtonToDisplay - 1;

        var targetImageRect:CGRect = CGRect(x:0.0, y:0.0, width:1.0, height:1.0);

        if let origImage = self.scrollView.imageView.image
        {
            targetImageRect = CGRect(x:0.0, y:0.0, width:origImage.size.width, height:origImage.size.height);

        //    var imageOverlayMask:UIImage?

            //-----------  create mask -----------------
            autoreleasepool
            {
                UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                    if let mask = maskedHouseImage0
                    {
                        mask.draw(in: targetImageRect)
                    }

                    if let mask = maskedHouseImage1
                    {
                        mask.draw(in: targetImageRect)
                    }

                    maskedHouseImage0 = UIGraphicsGetImageFromCurrentImageContext();

                    //-----------  create overlay -----------------
                    origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)

                    if let mask = maskedHouseImage0
                    {
                        mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                    }

                    self.scrollView.paintImageOverlay0.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
            }

            self.scrollView.paintImageOverlay0.image = self.scrollView.paintImageOverlay0.highlightedImage;

            self.colorWellButton1.isSelected = self.colorWellButton2.isSelected;
            self.scrollView.paintImageOverlay1.image    = self.scrollView.paintImageOverlay2.image;
            self.scrollView.paintImageOverlay1.highlightedImage = self.scrollView.paintImageOverlay2.highlightedImage;
            maskedHouseImage1 = maskedHouseImage2;
            defaultLineColor1 = defaultLineColor2;
            origStartColor1 = origStartColor2

            currentWellNum = 0;
            maskedHouseImage = maskedHouseImage0;
            defaultLineColor = defaultLineColor0;
            origStartColor = origStartColor0;

            if (currentButtonToDisplay > 1)
            {
                self.colorWellButton2.isSelected = self.colorWellButton3.isSelected;
                self.scrollView.paintImageOverlay2.image    = self.scrollView.paintImageOverlay3.image;
                self.scrollView.paintImageOverlay2.highlightedImage = self.scrollView.paintImageOverlay3.highlightedImage;
                maskedHouseImage2 = maskedHouseImage3;
                defaultLineColor2 = defaultLineColor3;
                origStartColor2 = origStartColor3
            }

            if (currentButtonToDisplay > 2)
            {
                self.colorWellButton3.isSelected = self.colorWellButton4.isSelected;
                self.scrollView.paintImageOverlay3.image    = self.scrollView.paintImageOverlay4.image;
                self.scrollView.paintImageOverlay3.highlightedImage = self.scrollView.paintImageOverlay4.highlightedImage;
                maskedHouseImage3 = maskedHouseImage4;
                defaultLineColor3 = defaultLineColor4;
                origStartColor3 = origStartColor4
            }

            self.turnItAllOff()
            self.turnOnButtons()

//print("doubleTa pWell1Action")

            isBump = true;
            self.scrollView.setUpColorWheelImage()
            self.scrollView.touchPadView.snapToOriginalColor()
            self.valuesChanged();
        }
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func doubleTapWell2Action(_ recognizer: UITapGestureRecognizer)
    {
        //-------------- merge paint layer to layer below -----------------------------
        if (currentButtonToDisplay < 2) {return;}

        self.lineUpMask()
        currentButtonToDisplay = currentButtonToDisplay - 1;

        var targetImageRect:CGRect = CGRect(x:0.0, y:0.0, width:1.0, height:1.0);

        if let origImage = self.scrollView.imageView.image
        {
            targetImageRect = CGRect(x:0.0, y:0.0, width:origImage.size.width, height:origImage.size.height);

    //        var imageOverlayMask:UIImage?

            //-----------  create mask -----------------
            autoreleasepool
            {
                UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                    if let mask = maskedHouseImage1
                    {
                        mask.draw(in: targetImageRect)
                    }

                    if let mask = maskedHouseImage2
                    {
                        mask.draw(in: targetImageRect)
                    }

                    maskedHouseImage1 = UIGraphicsGetImageFromCurrentImageContext();

                    //-----------  create overlay -----------------
                    origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)

                    if let mask = maskedHouseImage1
                    {
                        mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                    }

                    self.scrollView.paintImageOverlay1.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
            }

            self.scrollView.paintImageOverlay1.image = self.scrollView.paintImageOverlay1.highlightedImage;

            self.colorWellButton2.isSelected = self.colorWellButton3.isSelected;
            self.scrollView.paintImageOverlay2.image    = self.scrollView.paintImageOverlay3.image;
            self.scrollView.paintImageOverlay2.highlightedImage = self.scrollView.paintImageOverlay3.highlightedImage;
            maskedHouseImage2 = maskedHouseImage3;
            defaultLineColor2 = defaultLineColor3;
            origStartColor2 = origStartColor3

            currentWellNum = 1;
            maskedHouseImage = maskedHouseImage1;
            defaultLineColor = defaultLineColor1;
            origStartColor = origStartColor1

            if (currentButtonToDisplay > 2)
            {
                self.colorWellButton3.isSelected = self.colorWellButton4.isSelected;
                self.scrollView.paintImageOverlay3.image    = self.scrollView.paintImageOverlay4.image;
                self.scrollView.paintImageOverlay3.highlightedImage = self.scrollView.paintImageOverlay4.highlightedImage;
                maskedHouseImage3 = maskedHouseImage4;
                defaultLineColor3 = defaultLineColor4;
                origStartColor3 = origStartColor4
            }

            self.turnItAllOff()
            self.turnOnButtons()

//print("doubleTa pWell2Action")

            isBump = true;
            self.scrollView.setUpColorWheelImage()
            self.scrollView.touchPadView.snapToOriginalColor()
            self.valuesChanged();
        }
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func doubleTapWell3Action(_ recognizer: UITapGestureRecognizer)
    {
        //-------------- merge paint layer to layer below -----------------------------
        if (currentButtonToDisplay < 3) {return;}

        self.lineUpMask()
        currentButtonToDisplay = currentButtonToDisplay - 1;

        var targetImageRect:CGRect = CGRect(x:0.0, y:0.0, width:1.0, height:1.0);

        if let origImage = self.scrollView.imageView.image
        {
            targetImageRect = CGRect(x:0.0, y:0.0, width:origImage.size.width, height:origImage.size.height);

    //        var imageOverlayMask:UIImage?

            //-----------  create mask -----------------
            autoreleasepool
            {
                UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                    if let mask = maskedHouseImage2
                    {
                        mask.draw(in: targetImageRect)
                    }

                    if let mask = maskedHouseImage3
                    {
                        mask.draw(in: targetImageRect)
                    }

                    maskedHouseImage2 = UIGraphicsGetImageFromCurrentImageContext();

                    //-----------  create overlay -----------------
                    origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)

                    if let mask = maskedHouseImage2
                    {
                        mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                    }

                    self.scrollView.paintImageOverlay2.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
            }

            self.scrollView.paintImageOverlay2.image = self.scrollView.paintImageOverlay2.highlightedImage;

            self.colorWellButton3.isSelected = self.colorWellButton4.isSelected;
            self.scrollView.paintImageOverlay3.image    = self.scrollView.paintImageOverlay4.image;
            self.scrollView.paintImageOverlay3.highlightedImage = self.scrollView.paintImageOverlay4.highlightedImage;
            maskedHouseImage3 = maskedHouseImage4;
            defaultLineColor3 = defaultLineColor4;
            origStartColor3 = origStartColor4

            currentWellNum = 2;
            maskedHouseImage = maskedHouseImage2;
            defaultLineColor = defaultLineColor2;
            origStartColor = origStartColor2

            self.turnItAllOff()
            self.turnOnButtons()

//print("doubleTa pWell3Action")

            isBump = true;
            self.scrollView.setUpColorWheelImage()
            self.scrollView.touchPadView.snapToOriginalColor()
            self.valuesChanged();
        }
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func doubleTapWell4Action(_ recognizer: UITapGestureRecognizer)
    {
        //-------------- merge paint layer to layer below -----------------------------
        if (currentButtonToDisplay < 4) {return;}

        self.lineUpMask()
        currentButtonToDisplay = currentButtonToDisplay - 1;

        var targetImageRect:CGRect = CGRect(x:0.0, y:0.0, width:1.0, height:1.0);

        if let origImage = self.scrollView.imageView.image
        {
            targetImageRect = CGRect(x:0.0, y:0.0, width:origImage.size.width, height:origImage.size.height);

    //        var imageOverlayMask:UIImage?

            //-----------  create mask -----------------
            autoreleasepool
            {
                UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                    if let mask = maskedHouseImage3
                    {
                        mask.draw(in: targetImageRect)
                    }

                    if let mask = maskedHouseImage4
                    {
                        mask.draw(in: targetImageRect)
                    }

                    maskedHouseImage3 = UIGraphicsGetImageFromCurrentImageContext();

                    //-----------  create overlay -----------------
                    origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)

                    if let mask = maskedHouseImage3
                    {
                        mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                    }

                    self.scrollView.paintImageOverlay3.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
            }

            self.scrollView.paintImageOverlay3.image = self.scrollView.paintImageOverlay3.highlightedImage;

            currentWellNum = 3;
            maskedHouseImage = maskedHouseImage3;
            defaultLineColor = defaultLineColor3;
            origStartColor = origStartColor3

            self.turnItAllOff()
            self.turnOnButtons()

//print("doubleTa pWell4Action")

            isBump = true;
            self.scrollView.setUpColorWheelImage()
            self.scrollView.touchPadView.snapToOriginalColor()
            self.valuesChanged();
        }
    }


    // MARK: - edit gestures
    //----------------------------------------------------------------------------------------------------------
    @IBAction func longTapShowWell1(_ recognizer: UILongPressGestureRecognizer)
    {
        switch (recognizer.state)
        {
            case UIGestureRecognizer.State.possible:
            break;
            case UIGestureRecognizer.State.began:

                //-------------- new clear layer -----------------
                self.showColorWell(self.bumpButton1)
                currentToolIndex = 8669;

            break;
            case UIGestureRecognizer.State.changed:
            break;
            case UIGestureRecognizer.State.ended:
            break;
            case UIGestureRecognizer.State.failed:
            break;
            case UIGestureRecognizer.State.cancelled:
            break;
            default:
            break;
        }
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func longTapShowWell2(_ recognizer: UILongPressGestureRecognizer)
    {
        switch (recognizer.state)
        {
            case UIGestureRecognizer.State.possible:
            break;
            case UIGestureRecognizer.State.began:

                //-------------- new clear layer -----------------
                self.showColorWell(self.bumpButton2)
                currentToolIndex = 8669;

            break;
            case UIGestureRecognizer.State.changed:
            break;
            case UIGestureRecognizer.State.ended:
            break;
            case UIGestureRecognizer.State.failed:
            break;
            case UIGestureRecognizer.State.cancelled:
            break;
            default:
            break;
        }
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func longTapSecretOn(_ recognizer: UILongPressGestureRecognizer)
    {
        switch (recognizer.state)
        {
            case UIGestureRecognizer.State.possible:
            break;
            case UIGestureRecognizer.State.began:

                //-------------- Turn On  Layer Bar Pull -----------------
                isSecretTurnOn = true;
                UserDefaults.standard.set(isSecretTurnOn, forKey: "isSecretTurnOn")
                UserDefaults.standard.synchronize();

                self.scrollView.layrBarButton.isHidden = false;
                self.scrollView.layrControlView.isHidden = false;

            break;
            case UIGestureRecognizer.State.changed:
            break;
            case UIGestureRecognizer.State.ended:
            break;
            case UIGestureRecognizer.State.failed:
            break;
            case UIGestureRecognizer.State.cancelled:
            break;
            default:
            break;
        }
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func doubleTapPaintEdit(_ recognizer: UITapGestureRecognizer?)
    {
//print("00 doubleTapPa intEdit self.penTag:",self.penTag)
        self.hideSelection(nil)
        self.turnItAllAllOff()


        if let view = recognizer?.view
        {
//print("01 doubleTapPai ntEdit view:",view)
            if  (view == self.editPaintButton)
            {
                self.penTag = 9
            } else if (view == self.cutPaintButton) {
                self.penTag = 8
            } else if (view == self.copyPaintButton) {
                self.penTag = 7
            }
        }
//print("01 doubleTapPai ntEdit self.penTag:",self.penTag)


        self.scrollView.garageImageView.isHidden = false;
        self.scrollView.garageMaskView.isHidden = false;  //************** change 1.1.0 *** put back in
        self.scrollView.frontDrMaskView.isHidden = false;  //************** change 1.1.0 *** put back in

        if (self.colorWellButton0.isSelected)
        {
            self.scrollView.paintImageOverlay0.isHidden = false;
        }
        if (self.colorWellButton1.isSelected)
        {
            self.scrollView.paintImageOverlay1.isHidden = false;
        }
        if (self.colorWellButton2.isSelected)
        {
            self.scrollView.paintImageOverlay2.isHidden = false;
        }
        if (self.colorWellButton3.isSelected)
        {
            self.scrollView.paintImageOverlay3.isHidden = false;
        }
        if (self.colorWellButton4.isSelected)
        {
            self.scrollView.paintImageOverlay4.isHidden = false;
        }


        self.checkPaintButton.isHidden = false;
        dragSquare = false;
        self.scrollView.wasStart = true;
        currentToolIndex = 5
        self.scrollView.editDimButton.tag = 0;

//print("01 doubleTapPai ntEdit self.scrollView.wasS tart:",self.scrollView.wasStart)

//print("doubleTapPai ntEdit self.penTag:",self.penTag)

        if (self.penTag == 9)
        {
            self.editPaintButton.isSelected = true;
            self.editPaintButton.isHidden = false;
        } else if (self.penTag == 8) {
            self.cutPaintButton.isSelected = true;
            self.cutPaintButton.isHidden = false;
        } else  if (self.penTag == 7) {
            self.copyPaintButton.isSelected = true;
            self.copyPaintButton.isHidden = false;
        }


        self.paintLayer.backgroundColor = UIColor.clear.cgColor;
        self.paintLayer.fillColor = UIColor.clear.cgColor;
        self.paintLayer.strokeColor = UIColor(displayP3Red: 0.0, green: 0.5, blue: 0.5, alpha: 1.0).cgColor;
        self.paintLayer.lineJoin = CAShapeLayerLineJoin.round
        self.paintLayer.lineCap = CAShapeLayerLineCap.round

        hostRectPEC = self.scrollView.imageView.frame


        if (hostRectPEC.size.width >= hostRectPEC.size.height)
        {
            theImageScale = min(hostRectPEC.size.width / 4096.0, 1.0);
        } else {
            theImageScale = min(hostRectPEC.size.height / 4096.0, 1.0);
        }

        let lineWidth = 10.0 * min(theImageScale*1.25,1.0);
        let lineScale = min(theImageScale*2.0,1.0);

        if (self.scrollView.reverseZoomFactor > 5.5)
        {
            self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,14.0*lineScale);
        } else if (self.scrollView.reverseZoomFactor > 3.75) {
            self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,10.0*lineScale);
        } else if (self.scrollView.reverseZoomFactor > 2.5) {
            self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,8.0*lineScale);
        } else if (self.scrollView.reverseZoomFactor > 1.0) {
            self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,5.0*lineScale);
        } else if (self.scrollView.reverseZoomFactor > 0.5) {
            self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,3.0*lineScale);
        } else {
            self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,1.5*lineScale);
        }
    }







    // MARK: - actions
    //----------------------------------------------------------------------------------------------------------
    @IBAction func proSwitchTapped(_ sender: UISwitch)
    {
//print("proSwitchTapped sender.isOn:",sender.isOn)
        if (sender.isOn)
        {
            proOnorOff = true;

            self.scrollView.pullBarMax = 170.0;
            self.scrollView.touchPadView.colorWheelChip.isHidden = true;
            self.satLumView.isHidden = false;
            self.scrollView.touchPadView.indicator1.isHidden = false;
            self.scrollView.touchPadView.indicator2SpinnerView.isHidden = true;
            self.scrollView.touchPadView.satLayer.isHidden = false;

            pullBarDist = 143.0;
            touchPadCenterX = 252.0;
            touchPadLimit = 256.0;

            self.scrollView.touchPadView.satLayer.frame  =  CGRect(x:84.0, y:84.0, width:512.0,
                                                                                    height:512.0);
            self.scrollView.touchPadView.satLayer.cornerRadius = 256.0;
        } else {
            proOnorOff = false;

            self.scrollView.pullBarMax = 170.0+74.0;
            self.scrollView.touchPadView.colorWheelChip.isHidden = false;
            self.satLumView.isHidden = false;   //true;
            self.scrollView.touchPadView.indicator1.isHidden = true;
            self.scrollView.touchPadView.indicator2SpinnerView.isHidden = false;
            self.scrollView.touchPadView.satLayer.isHidden = true;  //true;

            pullBarDist = 143.0;  // 50.0
            touchPadCenterX = 346.0;
            touchPadLimit = 340.0;

            self.scrollView.touchPadView.satLayer.frame  =  CGRect(x:0.0, y:0.0, width:680.0,
                                                                                height:680.0);
            self.scrollView.touchPadView.satLayer.cornerRadius = 340.0;
        }

        let pullBarLimit = max(min(self.scrollView.pullBarButton.center.x, self.scrollView.pullBarMax),self.scrollView.pullBarMin);
        let pullBarCurrentOffset = pullBarLimit - self.scrollView.pullBarButton.center.x;

        self.scrollView.pullBarButton.center = CGPoint(x:self.scrollView.pullBarButton.center.x + pullBarCurrentOffset, y:self.mainBaseView.frame.height - pullBarDist);
        self.scrollView.touchPadView.center     = CGPoint(x:self.scrollView.pullBarButton.center.x - touchPadCenterX      , y:self.scrollView.touchPadView.center.y);

//print("proSwitchTapped proOnorOff:",proOnorOff)
        UserDefaults.standard.set(proOnorOff, forKey: "proOnorOff")
    }


    //----------------------------------------------------
    @IBAction func doneButtonTapped(_ sender: UIButton)
    {
        if self.isPaintChanged == true && self.editSelectedImage == false
        {
            isComingFromDone = true
        }


        if self.isPaintChanged == true
        {
            isComingFromDone = true

            let alertSuccess = UIAlertController(title: "", message: "Do you want to save your changes?", preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction (title: "Yes", style: UIAlertAction.Style.default, handler:
            { _ in

//print(" doneBut tonTapped  saveBut tonAction")
             //  self.saveProjectList()
                self.saveButtonAction(self.doneButton)
                self.backToHome()
                return
            })



            alertSuccess.addAction(okAction)

            let cancelAction = UIAlertAction (title: "No", style: UIAlertAction.Style.cancel, handler:
            { _ in
                self.backToHome()
                currGallThumb = nil; //********** change 1.1.2 change ***
//print("doneBut tonTapped cancelAction      self.currGal lThumb = nil")
                scrollEndDoor = true; //********** change 1.1.2 change ***
                return
            })
            alertSuccess.addAction(cancelAction)

			if let popoverController = alertSuccess.popoverPresentationController  //********** change 3.0.3 add if block ***
			{
			  popoverController.sourceView = self.view
			  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
			  popoverController.permittedArrowDirections = []
			}

            self.present(alertSuccess, animated: true, completion: nil)

            } else {

                self.backToHome()
                scrollEndDoor = true; //********** change 1.1.2 change ***
            }
//        else {
//            if self.cameraImageSelected == "defaultImage"
//            {
//                self.updateLastIndexImageToView()
//            }
//        }
    }

    //----------------------------------------------------
    func backToHome()
    {
        self.hideSelection(doneButton)

        self.paintLayer.path = nil;
        self.paintPath.removeAllPoints();

        wasAddDoor = false;
        scrollEndDoor = false; //******** change 1.1.0 add ***

        self.lineUpMask() //******** change 1.1.0 add ***

//print("  ")
//print("  ")
//print("backToHome  garimageID,garimageID.count,currGal lID:",garimageID,garimageID.count,currGallID as Any)

//        if let gallID = currGal lID, garimageID.count > 0
//        {
//            // for single garage to create favorite creating all combination of imageID and catID
//            garimageID = garimageID+"||"+gallID
//        }
//print("2 backToHome  garimageID:",garimageID as Any)
//print("  ")
//print("  ")


        sidings.removeAll();
        houses.removeAll();
        roofs.removeAll();
        windows.removeAll();
        trims.removeAll();

        maskedHouseImage = nil;

        self.paintLayer.path = nil;
        self.paintPath.removeAllPoints();

        for subView in self.scrollView.imageViewOverlay.subviews
        {
            if let button:UIButton = subView as? UIButton
            {
                button.removeFromSuperview();
            }
        }

        isHomeView = true;
        isEditView = false;
        isPaintView = false;

        self.navigationController?.popViewController(animated:true)
    }

    //-----------------------------------------------------
    @IBAction func galleryClickAction(_ sender: Any)
    {
        guard let projectThumb = currProjectThumb else {return;}

        let paintChoicesStoryboard = UIStoryboard(name: "PaintChoicesScreen", bundle: nil)
        let paintChoicesCtlr = paintChoicesStoryboard.instantiateInitialViewController() as! PaintChoicesCtlr

        paintChoicesCtlr.projectID = projectThumb.projectID
        paintChoicesCtlr.view.frame = self.mainBaseView.frame
        paintChoicesCtlr.modalPresentationStyle = .overCurrentContext
        paintChoicesCtlr.modalPresentationCapturesStatusBarAppearance = true

        self.present(paintChoicesCtlr, animated: true, completion: nil)
    }

    //------------------------------------------------------------------------------------
    func changeToPaintChoice( _ projectThumb0:ProjectThumb?)
    {
//print(" changeToPai ntChoice");
        if let projectThumb = projectThumb0
        {
            currGallThumb = projectThumb; //*************** change 1.1.0 add ***
            if let projectId = projectThumb.projectID
            {
                currGallID = projectId;   //*************** change 1.1.0 add ***
                if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                {
                    let fileURL = dir.appendingPathComponent("projectChoices_"+projectId+".txt")
                    do
                    {
                        let rawdata = try Data(contentsOf: fileURL)
                        NSKeyedUnarchiver.setClass(Project.self, forClassName: "Project")

                        if let project0 = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? Project
                        {
                            self.projectChoiceSetUp(project0)

//                            isBump = true;
//                            self.scrollView.setUpColorWheelImage()
//                            self.scrollView.touchPadView.runThroughHueFilters();
                        }
                    } catch {
  print(" ---------------------- changeToPa intChoice Couldn't read project -----------------------------")
                    }
                }
            }
        }
    }

    //-------------------------------------------------------------------------------
    func onSignInCompletionClick(sender: String)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if !isHomeApp
        {
            appDelegate.getPreviousHouseListAPI()
            DataManager.GetFavoriteScenes()
            DataManager.GetPaintColors()
            if let curHome = homeviewController
            {
                curHome.uploadFilesToCloud()
            }
        }
        
        //self.savePaintsToServer(count:selectedImageName)
    }

    //-------------------------------------------------------------------------------
    func showLoginController()
    {
        // loginStarted = true
        let signUpOverVC:SignupPopUp = UIStoryboard(name: "CustomTabBar", bundle: nil).instantiateViewController(withIdentifier: "SignupPopUp") as! SignupPopUp
        signUpOverVC.view.frame = self.view.frame
        signUpOverVC.modalPresentationStyle = .overCurrentContext  //********** change 3.0.3 change  ***
        signUpOverVC.fromOnboard = false
        signUpOverVC.delegate = self
        signUpOverVC.modalPresentationCapturesStatusBarAppearance = true

        self.present(signUpOverVC, animated: true, completion: nil)
    }

    //----------------------------------------------------------
    @IBAction func saveButtonAction(_ sender: UIButton?)
    {
//print("saveBut tonAction")
      //   self.saveProjectList()
        self.isPaintChanged = false
        guard let image = self.scrollView.imageView.image else {return;}
        var targetSize:CGSize = CGSize(width:image.size.width*0.35, height:image.size.height*0.35);
        if (image.size.width < 2000.0) || (image.size.height < 2000.0)
        {
            targetSize = CGSize(width:image.size.width*0.75, height:image.size.height*0.75);
        }
        self.lineUpMask()
        shouldSave = false

        let project:Project = Project()
        let projectThumb:ProjectThumb = ProjectThumb.init()
            currGallThumb = projectThumb; //*************** change 1.1.0 add ***

//print("saveBut tonAction: self.gal lIndex",self.gallIndex)
        project.gallIndex = self.gallIndex //*************** change 1.1.0 add ***
        projectThumb.gallIndex = self.gallIndex //*************** change 1.1.0 add ***

        project.well0 = self.colorWellButton0.isSelected
        project.well1 = self.colorWellButton1.isSelected
        project.well2 = self.colorWellButton2.isSelected
        project.well3 = self.colorWellButton3.isSelected
        project.well4 = self.colorWellButton4.isSelected

        var over00:UIImage? = nil
        var over10:UIImage? = nil
        var over20:UIImage? = nil
        var over30:UIImage? = nil
        var over40:UIImage? = nil

        if (hasOnlyGarageDoors) //************** change 1.1.0 add if block ***
        {
        } else {
            over00 = self.scrollView.paintImageOverlay0.image
            over10 = self.scrollView.paintImageOverlay1.image
            over20 = self.scrollView.paintImageOverlay2.image
            over30 = self.scrollView.paintImageOverlay3.image
            over40 = self.scrollView.paintImageOverlay4.image
        }
//self.scrollView.garageMa skView.image = nil;    //************** change 1.1.0 *** take out
//self.scrollView.frontDrMa skView.image = nil;    //************** change 1.1.0 *** take out

        let over50 = self.scrollView.garageImageView.image
        let over60 = self.scrollView.garageMaskView.image
        let over70 = self.scrollView.frontDrMaskView.image

        if let appDelegate0 = (UIApplication.shared.delegate as? AppDelegate)
        {
            DispatchQueue.global().async
            {
                appDelegate0.backgroundTaskID = UIApplication.shared.beginBackgroundTask (withName: "Finish saving Choices data")
                {
                    // End the task if time expires.
                    UIApplication.shared.endBackgroundTask(appDelegate0.backgroundTaskID!)
                    appDelegate0.backgroundTaskID = UIBackgroundTaskIdentifier.invalid
                }

                autoreleasepool
                {
                    var thumbImage:UIImage? = nil;

                    UIGraphicsBeginImageContextWithOptions(targetSize, false, 1.0);
                        image.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));

                        if let over0 = over00
                        {
                            if (project.well0)
                            {
                                over0.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
                            }
                        }
                        if let over1 = over10
                        {
                            if (project.well1)
                            {
                                over1.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
                            }
                        }
                        if let over2 = over20
                        {
                            if (project.well2)
                            {
                                over2.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
                            }
                        }
                        if let over3 = over30
                        {
                            if (project.well3)
                            {
                                over3.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
                            }
                        }
                        if let over4 = over40
                        {
                            if (project.well4)
                            {
                                over4.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
                            }
                        }

                        if let over5 = over50
                        {
                                over5.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
                        }
                        if let over6 = over60
                        {
                                over6.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
                        }
                        if let over7 = over70
                        {
                                over7.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
                        }

                        thumbImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();

                    project.thumbnailData = thumbImage?.jpegData(compressionQuality: 0.50)
                    projectThumb.thumbnailData = project.thumbnailData
                    let nameTemp = currentImageName.replacingOccurrences(of:".jpg", with:"")

                    let paintmaskName = nameTemp + "_projectChoices"+"_\(projectThumb.projectID ?? "")"+".txt"
                  //  let paintmaskName = currentImageName.replacingOccurrences(of:".jpg", with: "") + "_paintMask0"+"_\(projectThumb.proj ectID ?? "1")"
                    projectThumb.thumbnailName = paintmaskName
                    thumbImage = nil;
                }

                project.imageData = nil
            //    project.wasProces sed = false;

                project.defaultLineColor0d = defaultLineColor0.encode()
                project.origStartColor0d   = origStartColor0.encode()

                project.defaultLineColor1d = defaultLineColor1.encode()
                project.origStartColor1d   = origStartColor1.encode()

                project.defaultLineColor2d = defaultLineColor2.encode()
                project.origStartColor2d   = origStartColor2.encode()

                project.defaultLineColor3d = defaultLineColor3.encode()
                project.origStartColor3d   = origStartColor3.encode()

                project.defaultLineColor4d = defaultLineColor4.encode()
                project.origStartColor4d   = origStartColor4.encode()


                project.defaultLineGaraged = defaultDoorColor0.encode()
                project.origStartGaraged   = doorStartColor0.encode()
//print("saveButto nAction defaultDo orColor0 :",defaultDoorColor0 as Any)

                project.defaultLineFrontDrd = defaultFrontDrColor0.encode()
                project.origStartFrontDrd   = frontDrStartColor0.encode()

                project.house0 = nil;

//print(" saveButto nAction maskedHouse Image0:",maskedHo useImage0 as Any)

                project.mask0 = maskedHouseImage0?.pngData()
                project.mask1 = maskedHouseImage1?.pngData()
                project.mask2 = maskedHouseImage2?.pngData()
                project.mask3 = maskedHouseImage3?.pngData()
                project.mask4 = maskedHouseImage4?.pngData()

                project.garageMask  = maskedDoorImage?.pngData()    //************** change 1.1.0 *** put back in,  but also change
                project.garageSMask = theDoorsImage?.pngData()   //************** change 1.1.0 *** put back in,  but also change
                project.frontDrMask = maskedFrontDrImage?.pngData()   //************** change 1.1.0 *** put back in,  but also change

                project.siding0 = nil;
                project.roof0 = nil;

                project.currentWellNum = currentWellNum
                project.currentButtonToDisplay = currentButtonToDisplay
//print("  ")
//print(" saveButto nAction proje ct.currentWellNum:",pro ject.currentWellNum)
//print(" saveButt onAction proj ect.currentButtonToDisplay:",pr oject.currentButtonToDisplay)
//print("  ")

        //        curProjectCho iceList = self.loadThu mbChoices(currProjectThumb?.projectID) //********** change 1.1.4 remove,  needs to be universal ***
                curProjectChoiceList.append(projectThumb)  //********** change 1.1.4 change,  needs to be universal ***
               
                if ( !self.maskOfDoor && !self.maskOfFrontDoor && hasInternalApp)
                {
                    self.savePaintsToServer(count:"\(projectThumb.projectID ?? "1")",proj:project)
                }

//print("saveButt onAction curProje ctChoiceList:",curProj ectChoiceList.count);
//print("  ")
//print(" saveButt onAction curProje ctChoiceList:",curProj ectChoiceList)
//print("  ")
//print("  ")
//print(" saveButt onAction self.paintProjec tThumb.projectID:",self.paintPr ojectThumb.projectID as Any)
//print("  ")

                if let projectId = currProjectThumb?.projectID
                {
                    do
                    {
                        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                        {
                            let fileURL = dir.appendingPathComponent("projectThumbs_"+projectId+".txt")
//print("saveButt onAction DocumentDirectory fileURL:",fileURL)
                            let data = try NSKeyedArchiver.archivedData(withRootObject: curProjectChoiceList, requiringSecureCoding: false)  //********** change 1.1.4 change,  needs to be universal ***
                            try data.write(to: fileURL)
//print("saveButt onAction    hasBe enSaved")
                        }

                    } catch {
  print(" ---------------------- saveBut tonAction projec tThumbs_    error:",error)
                    }

                    if let thumbProjectId = projectThumb.projectID
                    {
                        currGallID = thumbProjectId;   //*************** change 1.1.0 add ***

                        do
                        {
                            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                            {
                                let fileURL = dir.appendingPathComponent("projectChoices_"+thumbProjectId+".txt")
//print("saveBut tonAction DocumentDirectory fileURL:",fileURL)
                                NSKeyedArchiver.setClassName("Project", for: Project.self)

                                let data = try NSKeyedArchiver.archivedData(withRootObject: project, requiringSecureCoding: false)
                                try data.write(to: fileURL)
//print("saveBut tonAction projectChoices_ has Been Saved")
                            }

                        } catch {
  print(" ---------------------- saveButto nAction    projec tChoices_      error:",error)
                        }
                    }
                }
               
                UIApplication.shared.endBackgroundTask(appDelegate0.backgroundTaskID!)
                appDelegate0.backgroundTaskID = UIBackgroundTaskIdentifier.invalid
            
            }
           
                           let alert = UIAlertController(title: "", message: "Paint changes saved successfully", preferredStyle: .alert)

							if let popoverController = alert.popoverPresentationController  //********** change 3.0.3 add if block ***
							{
							  popoverController.sourceView = self.view
							  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
							  popoverController.permittedArrowDirections = []
							}

                           self.present(alert, animated: true, completion: nil)
                           let when = DispatchTime.now() + 2
                           DispatchQueue.main.asyncAfter(deadline: when)
                           {
                               // code with delay
                               alert.dismiss(animated: true, completion: nil)
                               self.isPaintChanged = false
                               // 03/16
                               if self.isComingFromDone == true
                               {
                                   self.isComingFromDone = false
                                   self.navigationController?.popViewController(animated:true)
                               }
                               //end
                           }
                       
        }

        
    }



    //----------------------------------------------------------
    func savePaintsToServer(count:String,proj:Project)
    {
        if UserDefaults.standard.object(forKey: "UserID") == nil && isSaveDefaultImage
        {
            self.showLoginController()

        } else {
           
            let nameTemp = currentImageName.replacingOccurrences(of:".jpg", with:"")
            let fullImgID = UserDefaults.standard.value(forKey:String(format:"%@_fullID",nameTemp)) ?? ""
            let agentId = DataManager.checkAgentFileUrlWithName(imageName:currentImageName)
            DataManager.uploadPaintImageAndColors(fullImgID: fullImgID as! String, fileName: nameTemp,wellSel:count, agentID: agentId,project:proj, completion:{(error,response) in })

            editSelectedImage = false
            LoadingIndicatorView.hide()

            // 03/15 changed

        }
        //changes end
    }


    // MARK: - well gestures


    // MARK: - tut
    //----------------------------------------------------------------------------------------------------------
    func paintTutorial()
    {
        AppUtility.lockOrientation(.portrait)

        let storyboard = UIStoryboard(name: "CustomTabBar", bundle: nil)
        let onboradingVC = storyboard.instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
        onboradingVC.isPaintIntro = true
        onboradingVC.isPaintTour = true
        onboradingVC.hidesBottomBarWhenPushed = true
        onboradingVC.modalPresentationStyle = .overCurrentContext
        self.present(onboradingVC, animated: true, completion: nil)
    }










    // MARK: - gesture
    //----------------------------------------------------------------------------------------------------------
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool
    {
        return true;
    }

    // MARK: - image
    //-------------------------------------------------------------------------------
    func imageSetup()
    {
//print("imageSetup  hasInternalApp:",hasInternalApp)
        if (hasOnlyGarageDoors) //*************** change 1.1.0 add if block ***
        {
            noProcessing = true;
        } else if (hasInternalApp) {
            self.addBlurEffect()
            self.processingView.isHidden = false;
            self.startPaintAnimating()
        }
    }

    // MARK: - effects
    //------------------------------------------------------------------------------------
    func addBlurEffect()
    {
        self.blurEffectView = UIVisualEffectView(effect: self.blurEffect)
        self.blurEffectView.frame = self.view.bounds

        self.blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.mainBaseView.addSubview(self.blurEffectView)
    }

    //------------------------------------------------------------------------------------
    func removeBlurEffect()
    {
        blurEffectView.removeFromSuperview()
    }

    //-----------------------------------------------------
    func startPaintAnimating()
    {
        self.animaView.isHidden = false;
        self.animaView.fromGif(resourceName: "paintAnim1")
        self.animaView.animationDuration = 12.0;
        self.animaView.animationRepeatCount = 0;
        self.animaView.startAnimating()
//print("startPaintAni mating self.processi ngView.isHidden:",self.processingView.isHidden)
    }

    //-----------------------------------------------------
    func stopPaintAnimating()
    {
        dotLayers.forEach { $0.removeAllAnimations() }
        self.animaView.stopAnimating()
        self.animaView.animationImages = nil;
    }

//    //-----------------------------------------------------
//    func startAnimating()
//    {
//        if hasInternalApp
//        {
////            self.anim aView.isHidden = false;
////            self.animaView.fromGif(resourceName: "paintAnim1")
////            self.animaView.animationDuration = 12.0;
////            self.animaView.animationRepeatCount = 0;
////            self.animaView.startAnim ating()
//
//            var offset :TimeInterval = 0.0
//            dotLayers.forEach
//            {
//                $0.removeAllAnimations()
//                $0.add(scaleAnimation(offset), forKey: "scaleAnima")
//                offset = offset + 0.25
//            }
//        }
//    }

    //-----------------------------------------------------
    func startAnimatingDots()
    {
        if hasInternalApp
        {
            self.animaView.isHidden = true;
            var offset :TimeInterval = 0.0
            dotLayers.forEach
            {
                    $0.removeAllAnimations()
                    $0.add(scaleAnimation(offset), forKey: "scaleAnima")
                    offset = offset + 0.25
            }
        }
    }

    //-----------------------------------------------------
    func stopAnimatingDots()
    {
        dotLayers.forEach { $0.removeAllAnimations() }
        self.animaView.isHidden = false;
    }

//    //-----------------------------------------------------
//    func stopAn imating()
//    {
//        dotLayers.forEach { $0.removeAllAnimations() }
//        self.animaView.stopAni mating()
//        self.animaView.animationImages = nil;
//        //        self.animaView.layer.removeAllAnimations()
////        self.anim aView.isHidden = true;
//    }

    //-----------------------------------------------------
    func setupDotLayers()
    {
        dotLayers.append(dot1.layer)
        dotLayers.append(dot2.layer)
        dotLayers.append(dot3.layer)
    }

    //-----------------------------------------------------
    func scaleAnimation(_ after: TimeInterval = 0) -> CAAnimationGroup
    {
        let scaleUp = CABasicAnimation(keyPath: "transform.scale")
        scaleUp.beginTime = after
        scaleUp.fromValue = 1
        scaleUp.toValue = dotsScale
        scaleUp.duration = 0.3
        scaleUp.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)

        let scaleDown = CABasicAnimation(keyPath: "transform.scale")
        scaleDown.beginTime = after+scaleUp.duration
        scaleDown.fromValue = dotsScale
        scaleDown.toValue = 1.0
        scaleDown.duration = 0.2
        scaleDown.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)

        let group = CAAnimationGroup()
        group.animations = [scaleUp, scaleDown]
        group.repeatCount = Float.infinity

        let sum = CGFloat(dotsCount)*0.2 + CGFloat(0.4)
        group.duration = CFTimeInterval(sum)

        return group
    }

    // MARK: - process
    //---------------------------------------------------
    func saveNumToDocumentDirectory(dic:[String:Any], type:String )
    {
        var filename = currentImageName;

        do
        {
            filename = currentImageName.replacingOccurrences(of:".jpg", with: type)
//print("saveNum ToDocument Directory filename:",filename)
            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
            {
                let fileURL = dir.appendingPathComponent(filename)
//print("saveNum ToDocument Directory fileURL:",fileURL)
                let jsonData: Data? = try? JSONSerialization.data(withJSONObject: dic)

//print("saveNum ToDocument Directory data:",data)
                try jsonData!.write(to: fileURL)
            }
        } catch {
  print("Couldn't write num")
        }
    }

    //---------------------------------------------------------------------
    func resizeImage(image: UIImage, newSize: CGSize) -> UIImage
    {
        var newImage: UIImage

        let renderFormat = UIGraphicsImageRendererFormat.default()
        renderFormat.scale = 1.0;
        renderFormat.opaque = true
        let renderer = UIGraphicsImageRenderer(size: newSize, format: renderFormat)
        newImage = renderer.image
            {
                (context) in
                image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        }

        return newImage
    }

    //---------------------------------------------------------------------
    @objc func onTimerFired()
    {
        self.timeLeft -= 1
//print("paint controller onTim erFired:  \(timeLeft) seconds left  canFin dPaths,noPa int:",canFindPaths,noP aint)

        if (canFindPaths)
        {
            self.timer?.invalidate()
            self.timer = nil
            self.findPaths()
        } else if (noPaint) {
//print("onTime rFired noPa int")
            self.timer?.invalidate()
            self.timer = nil
            self.finishProgressViewMain()
        } else if (self.timeLeft == 25) {
            self.animaView.fromGif(resourceName: "paintAnim2")
            self.animaView.animationDuration = 12.0;
            self.animaView.animationRepeatCount = 0;
            self.animaView.startAnimating()
        }

        if (self.timeLeft <= 0)
        {
//print("self.timer?.invalidate")
            self.timer?.invalidate()
            self.timer = nil
            self.finishProgressViewMain()
        }
    }

    //---------------------------------------------------------------------
    func processError(inError: NSError?, message: String)
    {
        DispatchQueue.main.async(execute:
            {
                ////print("inside PrEr finishProgres sViewMain")
                if let error = inError
                {
                    //            self.finishProgres sViewMain()
                    let errorMessage1 = " ---- Description of the problem ---- "
                    let errorCodeString:String = "           code:"+String(error.code)
                    let errorMessage012 = errorMessage1+error.localizedDescription+errorCodeString;
                    let message0 = NSLocalizedString(errorMessage012, comment: "unsuccessful")
                    let alertController = UIAlertController(title: "Processing this image failed:", message: message0, preferredStyle: .alert)

                    alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "OK button"),
                                                            style: .cancel,
                                                            handler:
                        { _ in
                    }))

					if let popoverController = alertController.popoverPresentationController  //********** change 3.0.3 add if block ***
					{
					  popoverController.sourceView = self.view
					  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
					  popoverController.permittedArrowDirections = []
					}

                    self.present(alertController, animated: true, completion:
					{
                    })
                } else {

                    let errorMessage1 = " try taking the photo again zoomed in a little closer or at a different angle."
                    let message0 = NSLocalizedString(errorMessage1, comment: "unsuccessful")
                    let alertController = UIAlertController(title: "We couldn't find that type of Door", message: message0, preferredStyle: .alert)

                    alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "OK button"),
                                                            style: .cancel,
                                                            handler:
                        { _ in
                    }))

					if let popoverController = alertController.popoverPresentationController  //********** change 3.0.3 add if block ***
					{
					  popoverController.sourceView = self.view
					  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
					  popoverController.permittedArrowDirections = []
					}

                    self.present(alertController, animated: true, completion:
					{
                    })
                }
        })
    }


    // MARK: - interaction with server
    //---------------------------------------------------------------------
    func fetchPaintObject(completion: @escaping (HouseObject?) -> Void)
    {
//print("Paint fetch House Object")
        currentToolIndex = 8668;

//print("fetch canFi ndPaths",canFindPaths)
        if (canFindPaths)
        {
            self.findPaths()
            completion(nil)
        } else if (noPaint) {
            self.finishProgressViewMain()
            completion(nil)
        } else {
//print("fetch homeviewController",homeviewController as Any)
//print("fetch currentToolIndex",currentToolIndex)
                self.timeLeft = paintTimeLeft
//print("fetch self.timeLeft:",self.timeLeft)
//print("fetch paintMaxTime:",paintMaxTime)

            if (self.timeLeft == paintMaxTime)
            {
//print("fetch (self.timeLeft == 55)")
                self.didNotRun()
                completion(nil)

            } else {
                self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.onTimerFired), userInfo: nil, repeats: true)

//print("fetch self.timer",self.timer as Any)
//print("fetch self.timeLeft",self.timeLeft as Any)

                if (self.timeLeft <= 0)
                {
//print("(self.timeLeft <= 0)")
                    self.finishProgressViewMain()
                    completion(nil)
                }
            }
        }
    }

    //---------------------------------------------------------------------
    func didNotRun()
    {
//print("didNotRun")
        guard let refImageOrig = originalHomeImage else { return }

        maskTargetSize = refImageOrig.size;

        maskedHouseReady0 = false;
        maskedHouseReady1 = false;

        paintImageWidth  = 2048.0;
        paintImageHeight = 1536.0;

        if (maskTargetSize.height > maskTargetSize.width)
        {
            let tempWidth:CGFloat = paintImageWidth;
            paintImageWidth  = paintImageHeight;
            paintImageHeight = tempWidth;
        }

        let size = CGSize(width:CGFloat(paintImageWidth), height:CGFloat(paintImageHeight));

        let refImage = self.resizeImage(image: refImageOrig, newSize: size)

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd_HH-mm-ss"

        var userN = "paintApp"

        userN = userN+" lat="

        image_ID = "image"
        var fileName = "housephoto.jpg"
        image_lat = "0"
        image_lng = "0"
        image_date = "none"

        if let assetLocal = self.asset
        {
            image_ID = assetLocal.originalFilename ?? "housephoto.jpg";
            let date = assetLocal.creationDate ?? Date()
            image_date = formatter.string(from: date)
            fileName = image_ID+" date="+image_date+" username="

            if let location = assetLocal.location
            {
                image_lat = String(location.coordinate.latitude)
                image_lng = String(location.coordinate.longitude)
            }
        } else {
            image_ID = "housephoto.jpg";
            let date = Date()
            image_date = formatter.string(from: date)
            fileName = image_ID+" date="+image_date+" username="
        }

        userN = userN+image_lat+" lng="
        userN = userN+image_lng

        fileName = fileName+userN;

        DispatchQueue.main.async
        {
            self.timer = nil
            self.timeLeft = 75;
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.onTimerFired), userInfo: nil, repeats: true)

            self.getPaintJson(paramName: "image", fileName: fileName, image: refImage)
            { (error, json) in

                self.timer?.invalidate()
                self.timer = nil;
                if let e = error
                {
                    DispatchQueue.main.async(execute:
                    {
//print("didNotRun error:",e)
                //        self.doPaintRe ctInstead()
                        self.finishProgressViewMain()
                        self.processError(inError: e, message: "Error processing photo")
                    })
                    return
                } else {
//print("didNotRun self.findPaths currentToolIndex:",currentToolIndex)
                    self.findPaths()
                }
            }
        }
    }


    //---------------------------------------------------------------------
    func getPaintJson(paramName: String, fileName: String, image: UIImage, completion: @escaping (NSError?, String?) -> Void)
    {
//print("Paint getPai ntJson fileName:",fileName)
        sidings.removeAll();
        houses.removeAll();
        roofs.removeAll();
        windows.removeAll();
        trims.removeAll();

        guard let imageData = image.jpegData(compressionQuality: 1.0) else
        {
            let error = NSError(domain:"com.modifai", code:1008, userInfo:[ NSLocalizedDescriptionKey: "bad Image data, camera (or photo from library) working?"])
            completion(error, nil)
            return
        }

        // generate boundary string using a unique per-app string
        let boundary = UUID().uuidString

        let session = URLSession.shared

        // Set the URLRequest to POST and to the specified URL
        var urlRequest = URLRequest(url: paintUrl)
        urlRequest.httpMethod = "POST"
        urlRequest.timeoutInterval = 75.0

        // Set Content-Type Header to multipart/form-data, this is equivalent to submitting form data with file upload in a web browser
        // And the boundary is also set here
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        var data = Data()

        // Add the image data to the raw http request data
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"\(paramName)\"; filename=\"\(fileName)\"\r\n".data(using: .utf8)!)
        data.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
        data.append(imageData)

        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)

//print("   ");
//print("   ");
//print(" fileName: ",fileName);

        // Send a POST request to the URL, with the data we created earlier
        session.uploadTask(with: urlRequest, from: data, completionHandler:
        { responseData, response, error0 in

//print("  ")
//print("  ")
//print("getPa intJson error0aa: \(error0?.localizedDescription ?? "no error")")
//print("  ")
//print("  ")
//print("responseData:",responseData as Any)
//print("response:",response as Any)
//print("error0:",error0 as Any)

            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 500
            {
                let error = NSError(domain:"com.modifai", code:500, userInfo:[ NSLocalizedDescriptionKey: "the server may not be responding."])
//print("error:",error as Any)
                completion(error as NSError, nil)
                return
            } else if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                let error = NSError(domain:"com.modifai", code:httpStatus.statusCode, userInfo: [ NSLocalizedDescriptionKey: "unknown status code, something with the server side"])
                completion(error as NSError, nil)
                return
            }


            if error0 == nil
            {
                if let r = responseData
                {
                     do
                    {
                        if let jsonDict:Dictionary = try JSONSerialization.jsonObject(with: r, options: .allowFragments) as? Dictionary<String, Any>
                        {
//print(" jsonDict : ",jsonDict);
                            if let responseDict = jsonDict["response"] as? NSArray
                            {
                                for innerResponse in responseDict
                                {
                                    if let responseAr = innerResponse as? NSArray
                                    {
                                        for responseArDict in responseAr
                                        {
                                            if let responseArInDict = responseArDict as? NSDictionary
                                            {
////print(" responseArInDict : ",responseArInDict);
////                                                if (paintOnorOff)
////                                                {
                                                    if let ptArrayTop = responseArInDict.value(forKey: "paintableMasks") as? NSArray
                                                    {
//print(" type(of: ptArrayTop : ",type(of: ptArrayTop));
                                                        houses.append(ptArrayTop);
                                                        sidings.append(ptArrayTop);
                                                    }
////                                                } else {
                                                    if let ptArrayTop = responseArInDict.value(forKey: "houseMasks") as? NSArray
                                                    {
//print(" type(of: ptArrayTop : ",type(of: ptArrayTop));
                                                        houses.append(ptArrayTop);
                                                    }
//                                                }

                                                if let ptArrayTop = responseArInDict.value(forKey: "roofMasks") as? NSArray
                                                {
//print(" type(of: ptArrayTop : ",type(of: ptArrayTop));
                                                    roofs.append(ptArrayTop);
                                                }

                                                if let ptArrayTop = responseArInDict.value(forKey: "windowMasks") as? NSArray
                                                {
//print("PaintViewC  windowMasks type(of: ptArrayTop : ",type(of: ptArrayTop));
                                                    roofs.append(ptArrayTop);
                                                }

                                                if let ptArrayTop = responseArInDict.value(forKey: "trimMasks") as? NSArray
                                                {
//print("PaintViewC  trimMasks type(of: ptArrayTop : ",type(of: ptArrayTop));
                                                    trims.append(ptArrayTop);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

//print("   ");
//print("   ");
//print(" hous es.count :",hou ses.count);
//print(" roo  fs.count :",roofs.count);
//print(" windo ws.count:",windows.count);
//print(" tri ms.count:",tri ms.count);
//print("   ");
//print("   ");

//print(" hous es:",hou ses);
//print(" hous es:",hou ses);
//print(" windo ws:",windows);
                        completion(nil, nil)

                    } catch {
//print(" json error");
                        let error00 = NSError(domain:"com.modifai", code:1001, userInfo:[ NSLocalizedDescriptionKey: error0?.localizedDescription ?? "no contents or json data returned incorrectly"])
                        completion(error00 as NSError, nil)
                    }
                } else {
                    let error = NSError(domain:"com.modifai", code:1006, userInfo:[ NSLocalizedDescriptionKey: "is cell or wifi slow or none existent?"])
                    completion(error as NSError, nil)
                }
            } else {
                let error00 = NSError(domain:"com.modifai", code:1001, userInfo:[ NSLocalizedDescriptionKey: error0?.localizedDescription ?? "not actually error:1001, not a known error"])
                completion(error00 as NSError, nil)
            }
        }).resume()
    }


//    //---------------------------------------------------------------------
//    func getPain tJson(paramName: String, fileName: String, image: UIImage, completion: @escaping (NSError?, String?) -> Void)
//    {
//        houses.removeAll();
//        roofs.removeAll();
//        windows.removeAll();
//
//        //this was already checked before coming to this function???
//
//        let txtfileNameCheck = fileName.replacingOccurrences(of:".jpg", with: "_paint.txt")   //******** change 0.1.0 ***
//        let paintExistingArray = DataManager.loadPaintTextFile(fileName:txtfileNameCheck)
//
//        if paintExistingArray.count>0
//        {
//            self.processResponse(responseDict:paintExistingArray)
//
//            completion(nil, nil)
//            return
//        }
//
//        guard let imageData = image.jpegData(compressionQuality: 1.0) else
//        {
//            let error = NSError(domain:"com.modifai", code:1008, userInfo:[ NSLocalizedDescriptionKey: "bad Image data, camera (or photo from library) working?"])
//            completion(error, nil)
//            return
//        }
//
//        // generate boundary string using a unique per-app string
//        let boundary = UUID().uuidString
//
//        let session = URLSession.shared
//
//        // Set the URLRequest to POST and to the specified URL
//        var urlRequest = URLRequest(url: paintUrl)
//        urlRequest.httpMethod = "POST"
//        urlRequest.timeoutInterval = Double(paintMaxTime)
//
//        // Set Content-Type Header to multipart/form-data, this is equivalent to submitting form data with file upload in a web browser
//        // And the boundary is also set here
//        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
//
//        var data = Data()
//
//        // Add the image data to the raw http request data
//        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
//        data.append("Content-Disposition: form-data; name=\"\(paramName)\"; filename=\"\(fileName)\"\r\n".data(using: .utf8)!)
//        data.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
//        data.append(imageData)
//
//        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
//
//        // Send a POST request to the URL, with the data we created earlier
//        session.uploadTask(with: urlRequest, from: data, completionHandler:
//        { responseData, response, error0 in
//
//            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 500
//            {
//                let error = NSError(domain:"com.modifai", code:500, userInfo:[ NSLocalizedDescriptionKey: "the server may not be responding."])
//                completion(error as NSError, nil)
//                return
//            } else if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
//                let error = NSError(domain:"com.modifai", code:httpStatus.statusCode, userInfo: [ NSLocalizedDescriptionKey: "unknown status code, something with the server side"])
//                completion(error as NSError, nil)
//                return
//            }
//
//            if error0 == nil
//            {
//                if let r = responseData
//                {
//                    do
//                    {
//                        if let jsonDict:Dictionary = try JSONSerialization.jsonObject(with: r, options: .allowFragments) as? Dictionary<String, Any>
//                        {
////print(" jsonDict : ",jsonDict);
//                            if let responseDict = jsonDict["response"] as? NSArray
//                            {
//
//
//                                let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)   //******** change 0.1.0 *** ??? what? why?
//                                let jsonData: Data? = try? JSONSerialization.data(withJSONObject: responseDict)
//                                let jsonString = String(data: jsonData!, encoding: .utf8)
//
//
//                                let txtfileName = fileName.replacingOccurrences(of:".jpg", with: "_paint.txt")
//
//
//                                let filenameTemp =  paths[0].appendingPathComponent(txtfileName)
//                                if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
//                                {
//                                    let fileURL = dir.appendingPathComponent(txtfileName)
//
//                                    if FileManager.default.fileExists(atPath:fileURL.path)
//                                    {
////print("FILE AVAILABLE")
//                                    } else {
//  print("FILE NOT AVAILABLE")
//                                        do {
//                                            try jsonString!.write(to: filenameTemp, atomically: true, encoding: String.Encoding.utf8)
////print("Done Converting")
//                                        } catch {
//  print("Error Converting")
//                                        }
//                                    }
//                                    // self.uploadFilesToCloud()
//
//
//
//
//
//                                    //                                for innerResponse in responseDict
//                                    //                                {
//                                    //                                    if let responseAr = innerResponse as? NSArray
//                                    //                                    {
//                                    //                                        for responseArDict in responseAr
//                                    //                                        {
//                                    //                                            if let responseArInDict = responseArDict as? NSDictionary
//                                    //                                            {
//                                    ////print(" responseArInDict : ",responseArInDict);
//                                    //                                                if (paintOnorOff)
//                                    //                                                {
//                                    //                                                    if let ptArrayTop = responseArInDict.value(forKey: "paintableMasks") as? NSArray
//                                    //                                                    {
//                                    ////print(" type(of: ptArrayTop : ",type(of: ptArrayTop));
//                                    //                                                        houses.append(ptArrayTop);
//                                    //                                                    }
//                                    //                                                } else {
//                                    //                                                    if let ptArrayTop = responseArInDict.value(forKey: "house Masks") as? NSArray
//                                    //                                                    {
//                                    ////print(" type(of: ptArrayTop : ",type(of: ptArrayTop));
//                                    //                                                        houses.append(ptArrayTop);
//                                    //                                                    }
//                                    //                                                }
//                                    //
//                                    //                                                if let ptArrayTop = responseArInDict.value(forKey: "trimMasks") as? NSArray
//                                    //                                                {
//                                    ////print(" trimMasks: ptArrayTop : ",type(of: ptArrayTop));
//                                    //                                                    roofs.append(ptArrayTop);
//                                    //                                                }
//                                    //
//                                    //                                                if let ptArrayTop = responseArInDict.value(forKey: "roofMasks") as? NSArray
//                                    //                                                {
//                                    ////print(" type(of: ptArrayTop : ",type(of: ptArrayTop));
//                                    //                                                    roofs.append(ptArrayTop);
//                                    //                                                }
//                                    //
//                                    //                                                if let ptArrayTop = responseArInDict.value(forKey: "windowMasks") as? NSArray
//                                    //                                                {
//                                    ////print(" type(of: ptArrayTop : ",type(of: ptArrayTop));
//                                    //                                                    windows.append(ptArrayTop);
//                                    //                                                }
//                                    //                                            }
//                                    //                                        }
//                                    //                                    }
//                                    //                                }
//
//                                }
//                                self.processResponse(responseDict:responseDict)
//                            }
//                        }
//
////print(" hous es:",hous es);
////print(" hous es:",hous es);
////print(" windo ws:",windo ws);
//                        completion(nil, nil)
//
//                    } catch {
////print(" json error");
//                        let error00 = NSError(domain:"com.modifai", code:1001, userInfo:[ NSLocalizedDescriptionKey: error0?.localizedDescription ?? "no contents or json data returned incorrectly"])
//                        completion(error00 as NSError, nil)
//                    }
//                } else {
//                    let error = NSError(domain:"com.modifai", code:1006, userInfo:[ NSLocalizedDescriptionKey: "is cell or wifi slow or none existent?"])
//                    completion(error as NSError, nil)
//                }
//            } else {
//                let error00 = NSError(domain:"com.modifai", code:1001, userInfo:[ NSLocalizedDescriptionKey: error0?.localizedDescription ?? "not actually error:1001, not a known error"])
//                completion(error00 as NSError, nil)
//            }
//        }).resume()
//    }


//    //----------------------------------------------------------
//    func processResponse(responseDict:NSArray)  //******** change 0.1.0 *** what?  why??
//    {
//        for innerResponse in responseDict
//        {
//            if let responseAr = innerResponse as? NSArray
//            {
//                for responseArDict in responseAr
//                {
//                    if let responseArInDict = responseArDict as? NSDictionary
//                    {
//////print(" responseArInDict : ",responseArInDict);
//////                                                if (paintOnorOff)
//////                                                {
//                            if let ptArrayTop = responseArInDict.value(forKey: "paintableMasks") as? NSArray
//                            {
////print(" type(of: ptArrayTop : ",type(of: ptArrayTop));
//                                houses.append(ptArrayTop);
//                                sidings.append(ptArrayTop);
//                            }
//////                                                } else {
//                            if let ptArrayTop = responseArInDict.value(forKey: "house Masks") as? NSArray
//                            {
////print(" type(of: ptArrayTop : ",type(of: ptArrayTop));
//                                houses.append(ptArrayTop);
//                            }
////                                                }
//
//                        if let ptArrayTop = responseArInDict.value(forKey: "roofMasks") as? NSArray
//                        {
////print(" type(of: ptArrayTop : ",type(of: ptArrayTop));
//                            roofs.append(ptArrayTop);
//                        }
//
//                        if let ptArrayTop = responseArInDict.value(forKey: "windowMasks") as? NSArray
//                        {
////print("PaintViewC  windowMasks type(of: ptArrayTop : ",type(of: ptArrayTop));
//                            roofs.append(ptArrayTop);
//                        }
//
//                        if let ptArrayTop = responseArInDict.value(forKey: "trimMasks") as? NSArray
//                        {
////print("PaintViewC  trimMasks type(of: ptArrayTop : ",type(of: ptArrayTop));
//                            trims.append(ptArrayTop);
//                        }
//                    }
//                }
//            }
//        }
//    }

    //----------------------------------------------------------------------------
    func finishProgressViewMain()
    {
//print("finishProgres sViewMain")
        self.mainBaseView.isUserInteractionEnabled = true;

        self.stopPaintAnimating()
        self.stopAnimatingDots()
        self.removeBlurEffect()

        self.processingView.isHidden = true;
        dontRotate = false;
        self.scrollView.zoomScale = 1.0;

        let size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height);
        self.viewSettings(size);

        if (currentToolIndex == 8668)
        {
            self.readyView.isHidden = false;
        }

        self.scrollView.setUpColorWheelImage();
        self.scrollView.touchPadView.snapToDefaultColor();

        if let image = originalHomeImage
        {
            containWidthImage  = image.size.width;
            containHeightImage = image.size.height;
//print("0 finishProgres sViewMain  containWidthImage,containHeightImage:",containWidthImage,containHeightImage);

            let imageXpos:CGFloat = (containWidthImage  / 2.0) - (image.size.width  / 2.0);
            let imageYpos:CGFloat = (containHeightImage / 2.0) - (image.size.height / 2.0);

            self.scrollView.containerView.frame = CGRect(x:0, y:0, width:containWidthImage, height:containHeightImage);
//print("0 finishProgres sViewMain  self.scrollView.contain erView.frame:",self.scrollView.containerView.frame);

            self.scrollView.imageView.frame = CGRect(x:imageXpos, y:imageYpos, width:image.size.width, height:image.size.height);
//print("0 finishProgres sViewMain  self.scrollView.imageView.frame:",self.scrollView.imageView.frame);
            self.scrollView.imageContainView.frame = self.scrollView.imageView.frame;

            self.scrollView.paintImageOverlay0.frame = self.scrollView.imageView.frame;
            self.scrollView.paintImageOverlay1.frame = self.scrollView.imageView.frame;
            self.scrollView.paintImageOverlay2.frame = self.scrollView.imageView.frame;
            self.scrollView.paintImageOverlay3.frame = self.scrollView.imageView.frame;
            self.scrollView.paintImageOverlay4.frame = self.scrollView.imageView.frame;

            self.scrollView.garageImageView.frame = self.scrollView.imageView.frame;
            self.scrollView.garageMaskView.frame = self.scrollView.imageView.frame;
            self.scrollView.frontDrMaskView.frame = self.scrollView.imageView.frame;

            self.scrollView.imageViewOverlay.frame = self.scrollView.imageView.frame;

            if (containWidthImage  < 0.001) {return;}
            if (containHeightImage < 0.001) {return;}

            self.scrollView.displayImageWithSize();
        }
    }

    // MARK: -  backend methods
    //------------------------------------------------------------
    func findPaths()
    {
//print("findP aths: sidings.count",sidings.count)
//print("findP aths: houses.count",houses.count)
        //---------- doing siding surfaces -------------------
        var sidingPathsArray:[UIBezierPath] = [UIBezierPath]();

        for ptArray in sidings
        {
            if (ptArray.count > 2)
            {
                let sidingPath:UIBezierPath = UIBezierPath();
                var firstin = true;

                for pt00 in ptArray
                {
                    if let pt0 = pt00 as? NSArray
                    {
                        if (pt0.count > 1)
                        {
                            if let ptXNumber = pt0.object(at: 0) as? NSNumber
                            {
//print("ptXString:",ptXString);
                                let xValue:CGFloat = CGFloat(ptXNumber.floatValue);
//print("xValue:",xValue);

                                if let ptYNumber = pt0.object(at: 1) as? NSNumber
                                {
//print("ptYNumber:",ptYNumber);
                                    let yValue:CGFloat = CGFloat(ptYNumber.floatValue);
//print("yValue:",yValue);
                                    let basePtV:CIVector = CIVector(x:xValue, y:yValue);

                                    if (firstin)
                                    {
                                        firstin = false;
                                        sidingPath.move(to: basePtV.cgPointValue);
                                    } else {
                                        sidingPath.addLine(to: basePtV.cgPointValue);
                                    }
                                }
                            }
                        }
                    }
                }

                if ( !sidingPath.isEmpty)
                {
                    sidingPath.close();
                    sidingPathsArray.append(sidingPath);
                }
            }
        }

        //---------- doing house and Paint surfaces -------------------
        var housePathsArray:[UIBezierPath] = [UIBezierPath]();

        for ptArray in houses
        {
            if (ptArray.count > 2)
            {
                let housePath:UIBezierPath = UIBezierPath();
                var firstin = true;

                for pt00 in ptArray
                {
                    if let pt0 = pt00 as? NSArray
                    {
                        if (pt0.count > 1)
                        {
                            if let ptXNumber = pt0.object(at: 0) as? NSNumber
                            {
//print("ptXString:",ptXString);
                                let xValue:CGFloat = CGFloat(ptXNumber.floatValue);
//print("xValue:",xValue);

                                if let ptYNumber = pt0.object(at: 1) as? NSNumber
                                {
//print("ptYNumber:",ptYNumber);
                                    let yValue:CGFloat = CGFloat(ptYNumber.floatValue);
//print("yValue:",yValue);
                                    let basePtV:CIVector = CIVector(x:xValue, y:yValue);

                                    if (firstin)
                                    {
                                        firstin = false;
                                        housePath.move(to: basePtV.cgPointValue);
                                    } else {
                                        housePath.addLine(to: basePtV.cgPointValue);
                                    }
                                }
                            }
                        }
                    }
                }

                if ( !housePath.isEmpty)
                {
                    housePath.close();
                    housePathsArray.append(housePath);
                }
            }
        }
//print("   ");
//print("   ");
//print(" housePat hsArray.count :",housePat hsArray.count);
//print("   ");

        //---------- doing roofs -------------------
        var roofPathsArray:[UIBezierPath] = [UIBezierPath]();

        for ptArray in roofs
        {
            if (ptArray.count > 2)
            {
                let roofPath:UIBezierPath = UIBezierPath();
                var firstin = true;

                for pt00 in ptArray
                {
                    if let pt0 = pt00 as? NSArray
                    {
                        if (pt0.count > 1)
                        {
                            if let ptXNumber = pt0.object(at: 0) as? NSNumber
                            {
                                let xValue:CGFloat = CGFloat(ptXNumber.floatValue);

                                if let ptYNumber = pt0.object(at: 1) as? NSNumber
                                {
//print("ptYNumber:",ptYNumber);
                                    let yValue:CGFloat = CGFloat(ptYNumber.floatValue);
//print("yValue:",yValue);
                                    let basePtV:CIVector = CIVector(x:xValue, y:yValue);

                                    if (firstin)
                                    {
                                        firstin = false;
                                        roofPath.move(to: basePtV.cgPointValue);
                                    } else {
                                        roofPath.addLine(to: basePtV.cgPointValue);
                                    }
                                }
                            }
                        }
                    }
                }

                if ( !roofPath.isEmpty)
                {
                    roofPath.close();
                    roofPathsArray.append(roofPath);
                }
            }
        }

//print("   ");
//print("   ");
//print(" roofPathsArray.count :",roofPathsArray.count);
//print("   ");

//        //---------- doing windows -------------------
//        var windowPathsArray:[UIBezierPath] = [UIBezierPath]();
//
//        for ptArray in windows
//        {
//            if (ptArray.count > 2)
//            {
//                let windowPath:UIBezierPath = UIBezierPath();
//                var firstin = true;
//
//                for pt00 in ptArray
//                {
//                    if let pt0 = pt00 as? NSArray
//                    {
//                        if (pt0.count > 1)
//                        {
//                            if let ptXNumber = pt0.object(at: 0) as? NSNumber
//                            {
//                                let xValue:CGFloat = CGFloat(ptXNumber.floatValue);
//
//                                if let ptYNumber = pt0.object(at: 1) as? NSNumber
//                                {
//                                    let yValue:CGFloat = CGFloat(ptYNumber.floatValue);
//                                    let basePtV:CIVector = CIVector(x:xValue, y:yValue);
//
//                                    if (firstin)
//                                    {
//                                        firstin = false;
//                                        windowPath.move(to: basePtV.cgPointValue);
//                                    } else {
//                                        windowPath.addLine(to: basePtV.cgPointValue);
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//
//                if ( !windowPath.isEmpty)
//                {
//                    windowPath.clo se();
//                    windowPathsArray.append(windowPath);
//                }
//            }
//        }

//print("   ");
//print("   ");
//print(" windowPathsArray.count :",windowPathsArray.count);
//print("   ");


        //---------- doing trim -------------------
        var trimPathsArray:[UIBezierPath] = [UIBezierPath]();

        for ptArray in trims
        {
            if (ptArray.count > 2)
            {
                let trimPath:UIBezierPath = UIBezierPath();
                var firstin = true;

                for pt00 in ptArray
                {
                    if let pt0 = pt00 as? NSArray
                    {
                        if (pt0.count > 1)
                        {
                            if let ptXNumber = pt0.object(at: 0) as? NSNumber
                            {
                                let xValue:CGFloat = CGFloat(ptXNumber.floatValue);

                                if let ptYNumber = pt0.object(at: 1) as? NSNumber
                                {
                                    let yValue:CGFloat = CGFloat(ptYNumber.floatValue);
                                    let basePtV:CIVector = CIVector(x:xValue, y:yValue);

                                    if (firstin)
                                    {
                                        firstin = false;
                                        trimPath.move(to: basePtV.cgPointValue);
                                    } else {
                                        trimPath.addLine(to: basePtV.cgPointValue);
                                    }
                                }
                            }
                        }
                    }
                }

                if ( !trimPath.isEmpty)
                {
                    trimPath.close();
                    trimPathsArray.append(trimPath);
                }
            }
        }

        self.maskLayer(housePathsArray, sidingArray0:sidingPathsArray, roofArray0:roofPathsArray, trimArray0:trimPathsArray);
    }

    //------------------------------------------------------------
    func maskLayer(_ houseArray0:[UIBezierPath]?, sidingArray0:[UIBezierPath]?, roofArray0:[UIBezierPath]?, trimArray0:[UIBezierPath]?)
    {
//print("maskLayer")
//        var maskView:PECLayerView = self.scrollView.zoomView.subviews.object(at:2);
        let targetImageRect:CGRect = CGRect(x:0.0, y:0.0, width:maskTargetSize.width, height:maskTargetSize.height);
//print("  ")
//print("  ")
//print("mask Layer maskTa rgetSize:",maskTargetSize)
//print("mask Layer paintIm ageWidth,paintIm ageHeight:",paintImageWidth,paintImageHeight)

        let scaleX:CGFloat = maskTargetSize.width / paintImageWidth;
        let scaleY:CGFloat = maskTargetSize.height / paintImageHeight;

//print("mask Layer scaleX,scaleY:",scaleX,scaleY)
//        let scaleRevX:CGFloat = 1.0 / scaleX;
//print("mask Layer scaleRevX:",scaleRevX)
        var lineSize:CGFloat = 100.0 / scaleX;  //200.0
        var roofLineSize:CGFloat = 3.0 / scaleX;  //23.0  13.0
        var sidingLineSize:CGFloat = 66.0 / scaleX;
//print("mask Layer lineSize    :",lineSize)
//print("mask Layer roofLineSize:",roofLineSize)

        if (scaleX < 1.0)
        {
            lineSize = 100.0 * scaleX    //200.0
            roofLineSize = 13.0 * scaleX    //23.0
            sidingLineSize = 66.0 * scaleX
        }

        maskedSidingImage = nil;
        maskedRoofImage = nil;
        houseMask = nil;
//print("sidingArray0.count  :", sidingArray0?.count as Any);

        autoreleasepool
        {
            if let sidingArray = sidingArray0
            {
                if (sidingArray.count > 0)
                {
                    //-----------  create mask -----------------
                    UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                        if let context = UIGraphicsGetCurrentContext()
                        {
                            context.scaleBy(x: scaleX, y: scaleY)

                            for sidingPath in sidingArray
                            {
                                context.setBlendMode(.normal);

                                let sidingLayer:CAShapeLayer = CAShapeLayer();
                                sidingLayer.lineJoin = CAShapeLayerLineJoin.round;
                                sidingLayer.lineCap = CAShapeLayerLineCap.round;
                                sidingLayer.path = sidingPath.cgPath;

                                sidingLayer.fillColor = UIColor.black.cgColor;
                                sidingLayer.strokeColor = UIColor.clear.cgColor;
                                sidingLayer.lineWidth = 2.0;
                                sidingLayer.fillRule = CAShapeLayerFillRule.evenOdd;
                                sidingLayer.render(in: context);

                                context.setBlendMode(.destinationOut);

                                sidingLayer.fillColor = UIColor.clear.cgColor;
                                sidingLayer.strokeColor = UIColor.black.cgColor;
                                sidingLayer.lineWidth = sidingLineSize;
                                sidingLayer.fillRule = CAShapeLayerFillRule.evenOdd;
                                sidingLayer.render(in: context);
                            }

                            maskedSidingImage = UIGraphicsGetImageFromCurrentImageContext();
                        }
                    UIGraphicsEndImageContext();
                }
            }
        }

        var maskedTrimImage:UIImage? = nil;
        var tempRoofImage:UIImage? = nil;
        var tempTrimImage:UIImage? = nil;
//print("  ")
//print("  ")
//print("masklayer maskedSid ingImage  :", maskedSid ingImage as Any);
//print("  ")
//print("  ")

//print("trimArray0.count  :", trimArray0?.count as Any);
        autoreleasepool
        {
            if let trimArray = trimArray0
            {
                if (trimArray.count > 0)
                {
                    //-----------  create mask -----------------
                    UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                        if let context = UIGraphicsGetCurrentContext()
                        {
                            context.scaleBy(x: scaleX, y: scaleY)
                            context.setBlendMode(.normal);

                            for trimPath in trimArray
                            {
                                let trimLayer:CAShapeLayer = CAShapeLayer();
                                trimLayer.lineJoin = CAShapeLayerLineJoin.round;
                                trimLayer.lineCap = CAShapeLayerLineCap.round;
                                trimLayer.path = trimPath.cgPath;

                                trimLayer.fillColor = UIColor.black.cgColor;
                                trimLayer.strokeColor = UIColor.black.cgColor;
                                trimLayer.lineWidth = 2.0;
                                trimLayer.fillRule = CAShapeLayerFillRule.evenOdd;
                                trimLayer.render(in: context);
                            }

                            maskedTrimImage = UIGraphicsGetImageFromCurrentImageContext();
                        }
                    UIGraphicsEndImageContext();


                    //-----------  create mask -----------------
                    UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                        if let context = UIGraphicsGetCurrentContext()
                        {
                            context.scaleBy(x: scaleX, y: scaleY)
                            context.setBlendMode(.normal);

                            for trimPath in trimArray
                            {
                                let trimLayer:CAShapeLayer = CAShapeLayer();
                                trimLayer.lineJoin = CAShapeLayerLineJoin.round;
                                trimLayer.lineCap = CAShapeLayerLineCap.round;
                                trimLayer.path = trimPath.cgPath;

                                trimLayer.fillColor = UIColor.black.cgColor;
                                trimLayer.strokeColor = UIColor.black.cgColor;
                                trimLayer.lineWidth = sidingLineSize;
                                trimLayer.fillRule = CAShapeLayerFillRule.evenOdd;
                                trimLayer.render(in: context);
                            }

                            tempTrimImage = UIGraphicsGetImageFromCurrentImageContext();
                        }
                    UIGraphicsEndImageContext();
                }
            }
        }


        autoreleasepool
        {
            if let houseArray = houseArray0
            {
                if (houseArray.count > 0)
                {
                    //-----------  create mask -----------------
                    UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                        if let context = UIGraphicsGetCurrentContext()
                        {
                            context.scaleBy(x: scaleX, y: scaleY)
                            context.setBlendMode(.normal);

                            for housePath in houseArray
                            {
                                let houseLayer:CAShapeLayer = CAShapeLayer();
                                houseLayer.lineJoin = CAShapeLayerLineJoin.round;
                                houseLayer.lineCap = CAShapeLayerLineCap.round;
                                houseLayer.path = housePath.cgPath;

                                houseLayer.fillColor = UIColor.black.cgColor;
                                houseLayer.strokeColor = UIColor.black.cgColor;
                                houseLayer.lineWidth = lineSize;
                                houseLayer.fillRule = CAShapeLayerFillRule.evenOdd;
                                houseLayer.render(in: context);
                            }

                            houseMask = UIGraphicsGetImageFromCurrentImageContext();
                        }
                    UIGraphicsEndImageContext();
                } else {
                    UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                        if let context = UIGraphicsGetCurrentContext()
                        {
                            context.setBlendMode(.normal);
                            context.setFillColor(UIColor.black.cgColor);
                            context.fill(targetImageRect);
                            houseMask = UIGraphicsGetImageFromCurrentImageContext();
                        }
                    UIGraphicsEndImageContext();
                }
            }

            var roofArray:[UIBezierPath]! = [UIBezierPath]()
//            var windowArray:[UIBezierPath]! = [UIBezierPath]()

            if let roofArray00 = roofArray0
            {
                roofArray = roofArray00;
            }

//            if let windowArray00 = windowArray0
//            {
//                windowArray = windowArray00;
//            }

//print("roofArray.count  :", roofArray.count);
//print("windowArray.count:", windowArray.count);
//            if ((roofArray.count > 0) || (windowArray.count > 0))
            if (roofArray.count > 0)
            {
                //-----------  create mask -----------------
                UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                    if let context = UIGraphicsGetCurrentContext()
                    {
                        context.scaleBy(x: scaleX, y: scaleY)

                        for roofPath in roofArray
                        {
                            context.setBlendMode(.normal);

                            let roofLayer:CAShapeLayer = CAShapeLayer();
                            roofLayer.lineJoin = CAShapeLayerLineJoin.round;
                            roofLayer.lineCap = CAShapeLayerLineCap.round;
                            roofLayer.path = roofPath.cgPath;

                            roofLayer.fillColor = UIColor.black.cgColor;
                            roofLayer.strokeColor = UIColor.clear.cgColor;
                            roofLayer.lineWidth = 2.0;
                            roofLayer.fillRule = CAShapeLayerFillRule.evenOdd;
                            roofLayer.render(in: context);


                            context.setBlendMode(.destinationOut);

                            roofLayer.fillColor = UIColor.clear.cgColor;
                            roofLayer.strokeColor = UIColor.black.cgColor;
                            roofLayer.lineWidth = roofLineSize;
                            roofLayer.fillRule = CAShapeLayerFillRule.evenOdd;
                            roofLayer.render(in: context);
                        }

//                        for windowPath in windowArray
//                        {
//                            let windowLayer:CAShapeLayer = CAShapeLayer();
//                            windowLayer.lineJoin = CAShapeLayerLineJoin.round;
//                            windowLayer.lineCap = CAShapeLayerLineCap.round;
//                            windowLayer.path = windowPath.cgPath;
//
//                            windowLayer.fillColor = UIColor.black.cgColor;
//                            windowLayer.strokeColor = UIColor.black.cgColor;
//                            windowLayer.lineWidth = 2.0;
//                            windowLayer.fillRule = CAShapeLayerFillRule.evenOdd;
//                            windowLayer.render(in: context);
//                        }

                        maskedRoofImage = UIGraphicsGetImageFromCurrentImageContext();
                    }
                UIGraphicsEndImageContext();

                //-----------  create mask -----------------
                UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                    if let context = UIGraphicsGetCurrentContext()
                    {
                        context.scaleBy(x: scaleX, y: scaleY)
                        context.setBlendMode(.normal);

                        for roofPath in roofArray
                        {
                            let roofLayer:CAShapeLayer = CAShapeLayer();
                            roofLayer.lineJoin = CAShapeLayerLineJoin.round;
                            roofLayer.lineCap = CAShapeLayerLineCap.round;
                            roofLayer.path = roofPath.cgPath;

                            roofLayer.fillColor = UIColor.black.cgColor;
                            roofLayer.strokeColor = UIColor.clear.cgColor;
                            roofLayer.lineWidth = sidingLineSize;
                            roofLayer.fillRule = CAShapeLayerFillRule.evenOdd;
                            roofLayer.render(in: context);
                        }

                        tempRoofImage = UIGraphicsGetImageFromCurrentImageContext();
                    }
                UIGraphicsEndImageContext();

                autoreleasepool
                {
                    if let masked = houseMask
                    {
                        UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                            masked.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)

                            if let maskedRoof0 = maskedRoofImage
                            {
                                maskedRoof0.draw(in: targetImageRect, blendMode: .destinationOut, alpha: 1.0)
                            }

                            if let maskedTrim0 = maskedTrimImage
                            {
                                maskedTrim0.draw(in: targetImageRect, blendMode: .destinationOut, alpha: 1.0)
                            }

                            houseMask = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                    }

                    if let masked = maskedSidingImage
                    {
                        UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                            masked.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)

                            if let maskedRoof0 = tempRoofImage
                            {
                                maskedRoof0.draw(in: targetImageRect, blendMode: .destinationOut, alpha: 1.0)
                            }

                            if let maskedTrim0 = tempTrimImage
                            {
                                maskedTrim0.draw(in: targetImageRect, blendMode: .destinationOut, alpha: 1.0)
                            }

                            maskedSidingImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                    }
                }
            }
        }

        maskedTrimImage = nil;
        tempRoofImage = nil;

        DispatchQueue.main.async
        {
            if let masked = houseMask
            {
                if ( !twasForModifai)
                {
                    autoreleasepool
                    {
                        UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);

                            if let pecImage = self.scrollView.imageView.image
                            {
                                pecImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)

                                masked.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)

                                self.currentOverlay.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                                self.currentOverlay.image = self.currentOverlay.highlightedImage;

                                pecImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)

                                if let maskedSidingImage0 = maskedSidingImage
                                {
                                    maskedSidingImage0.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                                }

                                self.currentOverlay1.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                                self.currentOverlay1.image = self.currentOverlay1.highlightedImage;
                            }

                        UIGraphicsEndImageContext();
                    }
                }

                isaMaskPush = true;
//print("maskLayer isaMaskPush:",isaMaskPush)
                self.finishProgressViewMain()
            } else {
                self.finishProgressViewMain()
            }
        }
    }


    // MARK: -  change methods
    //----------------------------------------------------------------------------------------------------------
    func lineUpMask()
    {
//print("0 lineU pMask self.currentOverlay:",self.currentOverlay as Any)
        if (self.isEditView) {return;}
        if (self.currentOverlay == nil) {return;}
//print("0 lineU pMask self.currentOverlay.image:",self.currentOverlay.image as Any)

        if (self.maskOfDoor)  //*************** change 1.1.0 change if block ***
        {
            defaultDoorColor0 = defaultLineColor;
//print("lineU pMask defaultDo orColor0 :",defaultDoorColor0 as Any)
//            self.scrollView.garageMas kView.image = self.currentOv erlay.image;
//print("1 lineU pMask self.currentOverlay.image:",self.currentOverlay.image as Any)
        } else if (self.maskOfFrontDoor) {   //*************** change 1.1.0 change if block ***
            defaultFrontDrColor0 = defaultLineColor;
//            self.scrollView.frontDrMa skView.image = self.currentOv erlay.image;
        } else if (currentWellNum == 0) {
            defaultLineColor0 = defaultLineColor;
            maskedHouseImage0 = maskedHouseImage;
            self.scrollView.paintImageOverlay0.image = self.currentOverlay.image;
            self.scrollView.paintImageOverlay0.highlightedImage = self.currentOverlay.highlightedImage;
        } else if (currentWellNum == 1) {
            defaultLineColor1 = defaultLineColor;
            maskedHouseImage1 = maskedHouseImage;
            self.scrollView.paintImageOverlay1.image = self.currentOverlay.image;
            self.scrollView.paintImageOverlay1.highlightedImage = self.currentOverlay.highlightedImage;
        } else if (currentWellNum == 2) {
            defaultLineColor2 = defaultLineColor;
            maskedHouseImage2 = maskedHouseImage;
            self.scrollView.paintImageOverlay2.image = self.currentOverlay.image;
            self.scrollView.paintImageOverlay2.highlightedImage = self.currentOverlay.highlightedImage;
        } else if (currentWellNum == 3) {
            defaultLineColor3 = defaultLineColor;
            maskedHouseImage3 = maskedHouseImage;
            self.scrollView.paintImageOverlay3.image = self.currentOverlay.image;
            self.scrollView.paintImageOverlay3.highlightedImage = self.currentOverlay.highlightedImage;
        } else if (currentWellNum == 4) {
            defaultLineColor4 = defaultLineColor;
            maskedHouseImage4 = maskedHouseImage;
            self.scrollView.paintImageOverlay4.image = self.currentOverlay.image;
            self.scrollView.paintImageOverlay4.highlightedImage = self.currentOverlay.highlightedImage;
        }
    }


    //--------------------------------------------------------------------------------------------------
    func valuesChanged()
    {
        var oppositeHue:CGFloat = self.scrollView.touchPadView.hue;
        if (oppositeHue < 0.0)
        {
            oppositeHue = (CGFloat.pi*2) + oppositeHue;
        }

        let normalizedHue:CGFloat = oppositeHue / (CGFloat.pi * 2.0);
        let sat:CGFloat = self.scrollView.touchPadView.sat;
        let lum:CGFloat = self.scrollView.touchPadView.lum;
//print("normalizedHue   : ",normalizedHue);
//print("values Changed sat:",sat);
//print("values Changed lum:",lum);

		var redf1: CGFloat = 0.0;    //************** change 3.0.0 add several lines ***
		var greenf1: CGFloat = 0.0;
		var bluef1: CGFloat = 0.0;
		var alphaf1: CGFloat = 1.0;

		defaultLineColor.getRed(&redf1, green: &greenf1, blue: &bluef1, alpha: &alphaf1)

		let red1:Int = Int(redf1 * 255.0)
		let grn1:Int = Int(greenf1 * 255.0)
		let blu1:Int = Int(bluef1 * 255.0)

		self.convertColor(red:red1,grn:grn1,blu:blu1)  //************** end change 3.0.0 add several lines ***


        self.gradientViewSaturation.color1 = UIColor(hue:normalizedHue, saturation:0.0, brightness:1.0, alpha:1.0);
        self.gradientViewSaturation.color2 = UIColor(hue:normalizedHue, saturation:1.0, brightness:1.0, alpha:1.0);
        self.gradientViewSaturation.satValue = self.scrollView.touchPadView.sat;
        self.gradientViewSaturation.reloadGradient();
        self.gradientViewSaturation.setNeedsDisplay();


        self.scrollView.touchPadView.satLayer.backgroundColor = UIColor(hue:normalizedHue, saturation:sat, brightness:lum, alpha:1.0-lum).cgColor;

        self.gradientViewLuminosity.color1 = UIColor(hue:normalizedHue, saturation:sat, brightness:0.0, alpha:1.0);
        self.gradientViewLuminosity.color2 = UIColor(hue:normalizedHue, saturation:sat, brightness:1.0, alpha:1.0);
        self.gradientViewLuminosity.lumValue = self.scrollView.touchPadView.lum;
        self.gradientViewLuminosity.reloadGradient();
        self.gradientViewLuminosity.setNeedsDisplay();
    }

    //------------------------------------
    func projectChoiceSetUp(_ project0:Project?)
    {
//print("projectCho iceSetUp")
        self.isPaintChanged = false
        guard let project = project0 else {return;}
        self.lineUpMask()
        isaMaskPush = false;
        currentToolIndex = -1;

        if let orig = originalHomeImage
        {
            if orig.size.width < 4.0
            {
                if let data = project.imageData { originalHomeImage = UIImage(data:data); }
            }
        } else {
                if let data = project.imageData { originalHomeImage = UIImage(data:data); }
        }

        forcedImageSize = Size(width: Float(originalHomeImage.size.width),
                              height:Float(originalHomeImage.size.height));

        defaultLineColor0 = UIColor.color(data:project.defaultLineColor0d)
        origStartColor0   = UIColor.color(data:project.origStartColor0d)

        defaultLineColor1 = UIColor.color(data:project.defaultLineColor1d)
        origStartColor1   = UIColor.color(data:project.origStartColor1d)

        defaultLineColor2 = UIColor.color(data:project.defaultLineColor2d)
        origStartColor2   = UIColor.color(data:project.origStartColor2d)

        defaultLineColor3 = UIColor.color(data:project.defaultLineColor3d)
        origStartColor3   = UIColor.color(data:project.origStartColor3d)

        defaultLineColor4 = UIColor.color(data:project.defaultLineColor4d)
        origStartColor4   = UIColor.color(data:project.origStartColor4d)

//print("1 projectCho iceSetUp defaultDo orColor0:",defaultDoorColor0 as Any)
//print("1 projectCho iceSetUp scrollEndDoor:",scrollEndDoor)
//print("1 projectCho iceSetUp project.gal lIndex:",project.gal lIndex)


        if (scrollEndDoor)   //*************** change 1.1.0 add if block ***
        {
            scrollEndDoor = false;
        } else {
            defaultDoorColor0 = UIColor.color(data:project.defaultLineGaraged)
            doorStartColor0   = UIColor.color(data:project.origStartGaraged)

            defaultFrontDrColor0 = UIColor.color(data:project.defaultLineFrontDrd)
            frontDrStartColor0   = UIColor.color(data:project.origStartFrontDrd)

            //*************** change 1.1.0 move these to here ***
            if !imageNames.contains(currentImageName) || hasOnlyGarageDoors //***** addif for examples garage misplace issue 2.1.3
            {
            	if let data = project.garageMask  { maskedDoorImage = UIImage(data:data); }    //*************** change 1.1.0 put back in ***, also change
            	if let data = project.garageSMask { theDoorsImage = UIImage(data:data); }     //************** change 1.1.0 *** put back in ***, also change
            	if let data = project.frontDrMask { maskedFrontDrImage = UIImage(data:data); }   //*************** change 1.1.0 put back in ***, also change
            }
            self.scrollView.garageImageView.image = theDoorsImage;  //*************** change 1.1.0 add ***
            self.gallIndex = project.gallIndex //*************** change 1.1.0 add ***
//print("1b projectCho iceSetUp project.gal lIndex:",project.gallIndex as Any)
        }
//print("2 projectCho iceSetUp defaultDo orColor0:",defaultDoorColor0 as Any)

        if let data = project.mask0 { maskedHouseImage0 = UIImage(data:data); }
        if let data = project.mask1 { maskedHouseImage1 = UIImage(data:data); }
        if let data = project.mask2 { maskedHouseImage2 = UIImage(data:data); }
        if let data = project.mask3 { maskedHouseImage3 = UIImage(data:data); }
        if let data = project.mask4 { maskedHouseImage4 = UIImage(data:data); }



//        if let data = project.houseMask0 { houseMask = UIImage(data:data); }
//        if let data = project.siding0 { maskedSidingImage = UIImage(data:data); }
//        if let data = project.roof0 { maskedRoofImage = UIImage(data:data); }

        currentWellNum = project.currentWellNum;
        currentButtonToDisplay = project.currentButtonToDisplay;

        self.colorWellButton0.isSelected = project.well0;
        self.colorWellButton1.isSelected = project.well1;
        self.colorWellButton2.isSelected = project.well2;
        self.colorWellButton3.isSelected = project.well3;
        self.colorWellButton4.isSelected = project.well4;

//print("3 projectCho iceSetUp defaultDo orColor0 :",defaultDoorColor0 as Any)

        if (self.maskOfDoor) //******** change 1.1.0 add if block ***
        {
            currentWellNum = 0;
            currentButtonToDisplay = 0;
            self.currentOverlay = self.scrollView.garageMaskView;
            maskedHouseImage = maskedDoorImage;
            origStartColor = doorStartColor0;
            defaultLineColor = defaultDoorColor0;
//print("4 projectCho iceSetUp defaultLi neColor :",defaultLineColor as Any)

        } else if (self.maskOfFrontDoor) { //******** change 1.1.0 add if block ***
            currentWellNum = 0;
            currentButtonToDisplay = 0;
            self.currentOverlay = self.scrollView.frontDrMaskView;
            maskedHouseImage = maskedFrontDrImage;
            origStartColor = frontDrStartColor0;
            defaultLineColor = defaultFrontDrColor0;

        } else if (currentWellNum == 0) {
            self.currentOverlay = self.scrollView.paintImageOverlay0;
            maskedHouseImage = maskedHouseImage0;
            origStartColor = origStartColor0;
            defaultLineColor = defaultLineColor0;
//print("5 projectCho iceSetUp maskedHouseImage :",maskedHouseImage as Any)
//print("5 projectCho iceSetUp defaultLi neColor :",defaultLineColor as Any)

        } else if (currentWellNum == 1) {
            self.currentOverlay = self.scrollView.paintImageOverlay1;
            maskedHouseImage = maskedHouseImage1;
            origStartColor = origStartColor1;
            defaultLineColor = defaultLineColor1;

        } else if (currentWellNum == 2) {
            self.currentOverlay = self.scrollView.paintImageOverlay2;
            maskedHouseImage = maskedHouseImage2;
            origStartColor = origStartColor2;
            defaultLineColor = defaultLineColor2;

        } else if (currentWellNum == 3) {
            self.currentOverlay = self.scrollView.paintImageOverlay3;
            maskedHouseImage = maskedHouseImage3;
            origStartColor = origStartColor3;
            defaultLineColor = defaultLineColor3;

        } else if (currentWellNum == 4) {
            self.currentOverlay = self.scrollView.paintImageOverlay4;
            maskedHouseImage = maskedHouseImage4;
            origStartColor = origStartColor4;
            defaultLineColor = defaultLineColor4;
        }

        self.turnItAllOff()
    //    self.turnItAllOn()

        var targetImageRect:CGRect = CGRect(x:0.0, y:0.0, width:1.0, height:1.0);

        //-----------  create overlay -----------------
        if let origImage = originalHomeImage
        {
            targetImageRect = CGRect(x:0.0, y:0.0, width:origImage.size.width, height:origImage.size.height);

            autoreleasepool
            {
                if let mask = maskedHouseImage0
                {
                    if defaultLineColor0 != UIColor.black
                    {
                        UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                            origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
                            mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                            self.scrollView.paintImageOverlay0.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();

                        let brightHueFilter = PECbrightHue(origStartColor0, endColor:defaultLineColor0)
                        self.scrollView.paintImageOverlay0.image = self.scrollView.paintImageOverlay0.highlightedImage?.filterWithOperation(brightHueFilter)
                    } else {
                        self.scrollView.paintImageOverlay0.image = maskedHouseImage0
                    }
                }

                if let mask = maskedHouseImage1
                {
                    if defaultLineColor1 != UIColor.black
                    {
                        UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                            origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
                            mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                            self.scrollView.paintImageOverlay1.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();

                        let brightHueFilter = PECbrightHue(origStartColor1, endColor:defaultLineColor1)
                        self.scrollView.paintImageOverlay1.image = self.scrollView.paintImageOverlay1.highlightedImage?.filterWithOperation(brightHueFilter)
                    } else {
                        self.scrollView.paintImageOverlay1.image = maskedHouseImage1
                    }
                }

                if let mask = maskedHouseImage2
                {
                    if defaultLineColor2 != UIColor.black
                    {
                        UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                            origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
                            mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                            self.scrollView.paintImageOverlay2.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();

                        let brightHueFilter = PECbrightHue(origStartColor2, endColor:defaultLineColor2)
                        self.scrollView.paintImageOverlay2.image = self.scrollView.paintImageOverlay2.highlightedImage?.filterWithOperation(brightHueFilter)
                    } else {
                        self.scrollView.paintImageOverlay2.image = maskedHouseImage2
                    }
                }

                if let mask = maskedHouseImage3
                {
                    if defaultLineColor3 != UIColor.black
                    {
                        UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                            origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
                            mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                            self.scrollView.paintImageOverlay3.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();

                        let brightHueFilter = PECbrightHue(origStartColor3, endColor:defaultLineColor3)
                        self.scrollView.paintImageOverlay3.image = self.scrollView.paintImageOverlay3.highlightedImage?.filterWithOperation(brightHueFilter)
                    } else {
                        self.scrollView.paintImageOverlay3.image = maskedHouseImage3
                    }
                }

                if let mask = maskedHouseImage4
                {
                    if defaultLineColor4 != UIColor.black
                    {
                        UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                            origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
                            mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                            self.scrollView.paintImageOverlay4.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();

                        let brightHueFilter = PECbrightHue(origStartColor4, endColor:defaultLineColor4)
                        self.scrollView.paintImageOverlay4.image = self.scrollView.paintImageOverlay4.highlightedImage?.filterWithOperation(brightHueFilter)
                    } else {
                        self.scrollView.paintImageOverlay4.image = maskedHouseImage4
                    }
                }

            //    self.scrollView.garageIma geView.image = garageImageView0
//print("0 projectCho iceSetUp  self.scrollView.garageIm ageView.image:",self.scrollView.garageIma geView.image as Any)

//                if let mask = garageSingleMaskImage    //************** change 1.1.0 *** remove but keep for possible other uses
//                {
//                        UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
//                            origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
//                            mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
//                            self.scrollView.garageMaskView.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
//                        UIGraphicsEndImageContext();
//
//                        let brightHueFilter = PECbrightHue(doorStartColor0, endColor:defaultDo orColor0)
//                        self.scrollView.garageMa skView.image = self.scrollView.garageMaskView.highlightedImage?.filterWithOperation(brightHueFilter)
//                }

//print("0 projectCho iceSetUp  garageMa skImage:",garageMa skImage as Any)
//print("0 projectCho iceSetUp  maskedDo orImage:",maskedDo orImage as Any)
                if (self.maskOfDoor)   //************** change 1.1.0 add if block ***
                {
//                    if let mask = maskedDoorImage    //************** change 1.1.0 *** remove, also change
//                    {
//                        UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
//                            origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
//                            mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                            self.scrollView.garageMaskView.highlightedImage = maskedDoorImage; //************** change 1.1.0 change ***
//print("0 projectCho iceSetUp  self.scrollView.garageMaskView.highlightedImage:",self.scrollView.garageMaskView.highlightedImage as Any)
//                        UIGraphicsEndImageContext();

//print("5 projectCho iceSetUp doorStar  tColor0 :",doorStartColor0 as Any)
//print("5 projectCho iceSetUp defaultDo orColor0 :",defaultDoorColor0 as Any)
                        let brightHueFilter = PECbrightHue(doorStartColor0, endColor:defaultDoorColor0)
                        self.scrollView.garageMaskView.image = self.scrollView.garageMaskView.highlightedImage?.filterWithOperation(brightHueFilter)
    //print("0 projectCho iceSetUp  self.scrollView.garageMas kView.image:",self.scrollView.garageMa skView.image as Any)
//                    }
                }

//print("0 projectCho iceSetUp  maskedFro ntDrImage:",maskedFro ntDrImage as Any)
                if (self.maskOfFrontDoor)   //************** change 1.1.0 add if block ***
                {
//                    if let mask = maskedFrontDrImage    //************** change 1.1.0 *** remove, also change
//                    {
//                            UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
//                                origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
//                                mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                                self.scrollView.frontDrMaskView.highlightedImage = maskedFrontDrImage;  //************** change 1.1.0 change ***
//                            UIGraphicsEndImageContext();

                            let brightHueFilter = PECbrightHue(frontDrStartColor0, endColor:defaultFrontDrColor0)
                            self.scrollView.frontDrMaskView.image = self.scrollView.frontDrMaskView.highlightedImage?.filterWithOperation(brightHueFilter)
//                    }
                }
            }

            self.mainBaseView.isUserInteractionEnabled = true;
        }
    }

    // MARK: - project to disk and back
    //------------------------------------
    func loadProjects() -> [ProjectThumb]
    {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        {
            let fileURL = dir.appendingPathComponent("projectThumbs.txt")

            do
            {
//print("che ckHouse ProjectExist houseURL:",houseURL)
                let rawdata = try Data(contentsOf: fileURL)
//print("che ckHouse ProjectExist rawdata.count:",rawdata.count)
                if let projectList = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? [ProjectThumb]
                {
//print("che ckHouse ProjectExist projectList:",projectList)
                    return projectList
                } else {
                    return [ProjectThumb]()
                }
            } catch {
  print(" ---------------------- load Projects ProjectExist Couldn't read projectList (probably new project) ---------------------- ")
                return [ProjectThumb]()
            }
        }

        return [ProjectThumb]()
    }

//    //------------------------------------
//    func loadThumbForProject(_ projectID0:String?) -> ProjectThumb
//    {
////print("loadThum bForProject")
//        guard let projectId = projectID0 else {return ProjectThumb.init()}
//
//        let projectList = self.loadProjects()
//
//        for projectThumb in projectList
//        {
//            if (projectThumb.projectID == projectId)
//            {
//                return projectThumb;
//            }
//        }
//
//        return ProjectThumb.init()
//    }

      //********** change 1.1.4 remove function, no longer needed,  needs to change when project is changed in home view,
      //********** but needs to update when saving new gallery images handled already there ***
//    //------------------------------------
//    func loadThu mbChoices(_ projectID0:String?) -> [ProjectThumb]
//    {
//        guard let projectId = projectID0 else {return [ProjectThumb]()}
//
//        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
//        {
//            let fileURL = dir.appendingPathComponent("projectTh umbs_"+projectId+".txt")
////print("loadPro jectChoices  fileURL:",fileURL)
//
//            do
//            {
////print("che ckHouse  houseURL:",houseURL)
//                let rawdata = try Data(contentsOf: fileURL)
////print("che ckHouse  rawdata.count:",rawdata.count)
//                if let projectList = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? [ProjectThumb]
//                {
////print("che ckHouse  projectList:",projectList)
//                    return projectList
//                } else {
//                    return [ProjectThumb]()
//                }
//            } catch {
//  print(" ---------------------- loadThumb Choices  Couldn't read projectList, (probably new project) ---------------------- ")
//                return [ProjectThumb]()
//            }
//        }
//
//        return [ProjectThumb]()
//    }

      //---------------------------------------------------------------------
   // func saveProjectList()
//    {
////print("saveProj ectList")
//        guard let image = originalHomeImage else {return;}
//        var targetSize:CGSize = CGSize(width:image.size.width*0.35, height:image.size.height*0.35);
//        if (image.size.width < 2000.0) || (image.size.height < 2000.0)
//        {
//            targetSize = CGSize(width:image.size.width*0.75, height:image.size.height*0.75);
//        }
//        self.lineUpMask()
//        shouldSave = true
//
//        currentImageName = currentImageName.changeExtensions(name:currentImageName)
//
//        guard let project = currProject else {return;}
//        guard let projectThumb = currProje ctThumb else {return;}
//        let choiceThumb:ProjectThumb = ProjectThumb.init()
//
//        project.well0 = self.colorWellButton0.isSelected
//        project.well1 = self.colorWellButton1.isSelected
//        project.well2 = self.colorWellButton2.isSelected
//        project.well3 = self.colorWellButton3.isSelected
//        project.well4 = self.colorWellButton4.isSelected
//
//        let over00 = self.scrollView.paintImageOverlay0.image
//        let over10 = self.scrollView.paintImageOverlay1.image
//        let over20 = self.scrollView.paintImageOverlay2.image
//        let over30 = self.scrollView.paintImageOverlay3.image
//        let over40 = self.scrollView.paintImageOverlay4.image
//
//        let over50 = self.scrollView.garageIma geView.image
//        let over60 = self.scrollView.garage MaskView.image
//        let over70 = self.scrollView.frontDrMa skView.image
//
//
//        if let appDelegate0 = (UIApplication.shared.delegate as? AppDelegate)
//        {
//            DispatchQueue.global().async
//            {
//                appDelegate0.backgroundTaskID = UIApplication.shared.beginBackgroundTask (withName: "Finish saving data")
//                {
//                    // End the task if time expires.
//                    UIApplication.shared.endBackgroundTask(appDelegate0.backgroundTaskID!)
//                    appDelegate0.backgroundTaskID = UIBackgroundTaskIdentifier.invalid
//                }
//
//                autoreleasepool
//                {
//                    var thumbImage:UIImage? = nil;
//
//                    UIGraphicsBeginImageContextWithOptions(targetSize, false, 1.0);
//                        image.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
//
//                        if let over0 = over00
//                        {
//                            if (project.well0)
//                            {
//                                over0.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
//                            }
//                        }
//                        if let over1 = over10
//                        {
//                            if (project.well1)
//                            {
//                                over1.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
//                            }
//                        }
//                        if let over2 = over20
//                        {
//                            if (project.well2)
//                            {
//                                over2.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
//                            }
//                        }
//                        if let over3 = over30
//                        {
//                            if (project.well3)
//                            {
//                                over3.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
//                            }
//                        }
//                        if let over4 = over40
//                        {
//                            if (project.well4)
//                            {
//                                over4.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
//                            }
//                        }
//                        if let over5 = over50
//                        {
//                                over5.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
//                        }
//                        if let over6 = over60
//                        {
//                                over6.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
//                        }
//                        if let over7 = over70
//                        {
//                                over7.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
//                        }
//
//                        thumbImage = UIGraphicsGetImageFromCurrentImageContext();
//                    UIGraphicsEndImageContext();
//
//                    project.thumbnailData = thumbImage?.jpegData(compressionQuality: 0.50)
//                    projectThumb.thumbnailData = project.thumbnailData
//                    choiceThumb.thumbnailData = project.thumbnailData;
//                    thumbImage = nil;
//                }
//
//                project.imageData = image.jpegData(compressionQuality: 1.0)
//                project.imageName = currentImageName;
//
////print(" saveProje ctList project.wasProcessed:",project.wasProcessed);
//            //    project.wasProcessed = false;
//
//                project.defaultL ineColor0d = defaultLineColor0.encode()
//                project.origSta rtColor0d   = origStartColor0.encode()
//
//                project.defaultLi neColor1d = defaultLineColor1.encode()
//                project.origStartC olor1d   = origStartColor1.encode()
//
//                project.defaultLineColor2d = defaultLineColor2.encode()
//                project.origStartColor2d   = origStartColor2.encode()
//
//                project.defaultLineColor3d = defaultLineColor3.encode()
//                project.origStartColor3d   = origStartColor3.encode()
//
//                project.defaultLineColor4d = defaultLineColor4.encode()
//                project.origStartColor4d   = origStartColor4.encode()
//
//
//                project.defaultLineGaraged = defaultLineGarage.encode()
//                project.origStartGaraged   = origStartGarage.encode()
//
//                project.defaultLineFrontDrd = defaultLineFrontDr.encode()
//                project.origStartFrontDrd   = origStartFrontDr.encode()
//
//                let encoder = JSONEncoder()
//                project.house0 = try? encoder.encode(houseObject)
//
////print(" save project list maskedHo useImage0:",maskedHou seImage0 as Any)
////print("save project list currPr oject?.projectID:",currProject?.projectID as Any)
////print("save project list proje ct?.projectID:",project.projectID as Any)
//
//                project.mask0 = maskedHouseImage0?.pngData()
//                project.mask1 = maskedHouseImage1?.pngData()
//                project.mask2 = maskedHouseImage2?.pngData()
//                project.mask3 = maskedHouseImage3?.pngData()
//                project.mask4 = maskedHouseImage4?.pngData()
//
////                project.garageMask  = garageMa skImage?.pngData()  // temporary until door paint is working again ...
////                project.garageSMask = garageSi ngleMaskImage?.pngData()  // temporary until door paint is working again ...
////                project.frontDrMask = frontDrMa skImage?.pngData()        // temporary until door paint is working again ...
//
////print("save project  house Mask:",houseMask as Any)
//
//                project.houseMask0 = houseMask?.pngData()
//                project.siding0 = maskedSidingImage?.pngData()
//                project.roof0 = maskedRoofImage?.pngData()
//
//                project.currentWellNum = currentWellNum
//                project.currentButt onToDisplay = currentButt onToDisplay
//
//                projectList00 = self.loadProjects()
//
////                if (shouldAddToAlbum)
////                {
////                    projectList00.app end(projectThumb)
////                } else {
//
//                var k:Int = 0;
//                for thumb in projectList00
//                {
//                    if (thumb.projectID == project.projectID)
//                    {
//                        break;
//                    }
//                    k += 1;
//                }
////print("save project currProjec tThumb?.projectID:",currProje ctThumb?.projectID as Any)
////print("save project k:",k)
////print("save project projectList00.count:",projectList00.count)
//
//
//                if (k < projectList00.count)
//                {
//                    let place:Range = k..<k+1
//                    projectList00.replaceSubrange(place, with: [projectThumb])
//
//                    //---------------- save main project thumbnails to list ------------------
//                    do
//                    {
//                        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
//                        {
//                            let fileURL = dir.appendingPathComponent("projectThumbs.txt")
////print("saveHouseTo DocumentDirectory fileURL:",fileURL)
//                            let data = try NSKeyedArchiver.archivedData(withRootObject: projectList00, requiringSecureCoding: false)
//                            try data.write(to: fileURL)
//                            hasBeenSaved = true;
////print("projectTh umbs    hasBe enSaved:",hasBeenSaved)
//                        }
//
//                    } catch {
//  print(" ---------------------- savePaintObject projectTh umbs    error:",error)
//                    }
//                }
//
//
//                if let projectId = projectThumb.projectID
//                {
//                    do
//                    {
//                        //---------------- save main project  ------------------
//                        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
//                        {
//                            let fileURL = dir.appendingPathComponent("project_"+projectId+".txt")
//                            let currProjectURL = dir.appendingPathComponent("currProject.txt")
//                            let data = try NSKeyedArchiver.archivedData(withRootObject: project, requiringSecureCoding: false)
//                            try data.write(to: fileURL)
//                            try data.write(to: currProjectURL)
////print("saveProje ctList project has Been Saved")
//                        }
//
//                    } catch {
//  print(" ---------------------- savePaintObject    project_  error:",error)
//                    }
//
////print("save project list shouldSave:",shouldSave);
//                    if (shouldSave)
//                    {
//                        var thumbCh oiceList:[ProjectThumb] = self.loadThu mbChoices(projectThumb.projectID)
//                        thumbChoic eList.appe nd(choi ceThumb)
////print("save project list thumbChoiceList:",thumbChoiceList.count);
//
//                        do
//                        {
//                            //---------------- save choices thumbnails to choices list  ------------------
//                            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
//                            {
//                                let fileURL = dir.appendingPathComponent("projectTh umbs_"+projectId+".txt")
////print("saveBut tonAction DocumentDirectory fileURL:",fileURL)
//                                let data = try NSKeyedArchiver.archivedData(withRootObject: thumbChoiceList, requiringSecureCoding: false)
//                                try data.write(to: fileURL)
////print("saveBut tonAction    hasBe enSaved")
//                            }
//
//                        } catch {
//  print(" ---------------------- saveBut tonAction projec tThumbs_   error:",error)
//                        }
//                    }
//                }
//
//                if (shouldSave)
//                {
//                    project.imageData = nil;
//                    project.siding0 = nil;
//                    project.roof0 = nil;
//
//                    if let thumbProjectId = choiceThumb.projectID
//                    {
//                        do
//                        {
//                            //---------------- save current choice as a project  ------------------
//                            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
//                            {
//                                let fileURL = dir.appendingPathComponent("projectChoices_"+thumbProjectId+".txt")
////print("saveBut tonAction DocumentDirectory fileURL:",fileURL)
//                                let data = try NSKeyedArchiver.archivedData(withRootObject: project, requiringSecureCoding: false)
//                                try data.write(to: fileURL)
////print("saveBut tonAction projectChoices_ has Been Saved")
//                            }
//
//                        } catch {
//  print(" ---------------------- saveBut tonAction    projectChoices  error:",error)
//                        }
//                    }
//                }
//
//                UIApplication.shared.endBackgroundTask(appDelegate0.backgroundTaskID!)
//                appDelegate0.backgroundTaskID = UIBackgroundTaskIdentifier.invalid
//            }
//            if ( !self.maskOfDo or && !self.maskOfFro ntDoor && hasInternalApp)
//                                                                          {
//                                                                              let thumbChoiceList:[ProjectThumb] = self.loadThu mbChoices(currPro jectThumb?.projectID)
//
//                                                                              self.savePaintsToServer(count:"\(thumbChoiceList.count)",project:project)
//                                                                              //self.savePaints()
//                                                                          }
//        }
//    }





    // MARK: - layer methods
    //----------------------------------------------------------------------------------
    func convertPoint(point:CGPoint, fromRect:CGRect, toRect:CGRect) -> CGPoint
    {
        return  CGPoint(x:(toRect.size.width/fromRect.size.width) * point.x,
                        y:(toRect.size.height/fromRect.size.height) * point.y);
    }

    //----------------------------------------------------------------------------------
    func lineFitLow(pixelArray:[PECpixel], lowSpot:Int, percDeviat:CGFloat)
    {
//print(" ")
//print("------------- line Fit low ----------------")
//print(" ")
        var havg:CGFloat = 0.0;
        var lavg:CGFloat = 0.0;

        var sumx:CGFloat = 0.0;
        var sumxsq:CGFloat = 0.0;
    //    var sumysq:CGFloat = 0.0;
        var sumy:CGFloat = 0.0;
        var sumxy:CGFloat = 0.0;

        var realKount:Int = 0;

//        let startSpot:Int = 6;
//        let endSpot:Int = max(min(lowSpot+8,92),28);

        let startSpot:Int = max(lowSpot-10,1);
        var endSpot:Int = min(startSpot+20,92);

//print("lowSpot: ",lowSpot);
//print("startSpot: ",startSpot);
//print("endSpot: ",endSpot);
//print(" ")
//print(" ")
        if (startSpot >= endSpot)
        {
            endSpot = pixelArray.count;
        }
//print("startSpot: ",startSpot);
//print("endSpot: ",endSpot);
//print(" ")
//print(" ")

        for index in (startSpot..<endSpot)
        {
            let pixel = pixelArray[index]
            if (pixel.k > 1)
            {
                havg += pixel.hue
                lavg += pixel.l
                realKount += 1;
//print("index,havg,lavg,   pixel: ",index,havg,lavg,   pixel);
            } else {
//print(" --------------------------------- index,pixel: ",index,pixel);
            }
        }

//print(" ")
//print(" ")
//print("realKount: ",realKount);
//print(" ")
//print(" ")

        let x0:CGFloat = CGFloat(lowSpot) * 0.01;
        self.slopefLow = 0.0
        self.interceptYaxisLow = x0

        if (realKount > 0)
        {
            havg = havg / CGFloat(realKount)
            lavg = lavg / CGFloat(realKount)

//print("havg,lavg: ",havg,lavg);
//print(" ")
//print(" ")
            for index in (startSpot..<endSpot)
            {
                let pixel = pixelArray[index]
                if (pixel.k > 1)
                {
                    sumx = pixel.hue - havg;
                    sumy = pixel.l - lavg;

                    sumxy  = sumx*sumy;
                    sumxsq = sumx*sumx;
//print("index,sumxy,sumxsq,   pixel: ",index,sumxy,sumxsq,   pixel);
                } else {
//print(" --------------------------------- index,pixel: ",index,pixel);
                }
            }

//print("sumxy,sumxsq: ",sumxy,sumxsq);

            if (sumxsq != 0.0)
            {
                self.slopefLow = sumxy / sumxsq
                self.interceptYaxisLow = (lavg - (self.slopefLow * havg))
//print("self.interceptYaxisLow: ",self.interceptYaxisLow);
//print("percDeviat: ",percDeviat);
                self.interceptYaxisLow *= percDeviat //percDeviat;

//                let y0 = (x0  * self.slopefLow) + self.interceptYaxisLow   //************* keep for testing ***
//                let y  = (1.0 * self.slopefLow) + self.interceptYaxisLow   //************* keep for testing ***

//print("self.slopefLow: ",self.slopefLow);
//print("self.interceptYaxisLow: ",self.interceptYaxisLow);
//print("  ");
//print("x0: ",x0);
//print("y0: ",y0);
//print("y : ",y);
//print("  ");
//print("  ");
//print("  ");
            }
        }
    }

    //----------------------------------------------------------------------------------
    func lineFit(pixelArray:[PECpixel], highSpot:Int, highSpot3:Int, percDeviat:CGFloat)
    {
//print(" ")
//print("------------- line Fit high ----------------")
//print(" ")
        var havg:CGFloat = 0.0;
        var lavg:CGFloat = 0.0;

        var sumx:CGFloat = 0.0;
        var sumxsq:CGFloat = 0.0;
    //    var sumysq:CGFloat = 0.0;
        var sumy:CGFloat = 0.0;
        var sumxy:CGFloat = 0.0;

        var realKount:Int = 0;

        var startSpot:Int = max(highSpot-10,0);   //8
        var endSpot:Int = min(startSpot+20,pixelArray.count-6);  //30 8
//print("highSpot: ",highSpot);
//print("startSpot: ",startSpot);
//print("endSpot: ",endSpot);
//print(" ")
//print(" ")
        if (startSpot >= endSpot)
        {
            endSpot = pixelArray.count-3;
            startSpot = max(endSpot-15,0);
        }
//print("startSpot: ",startSpot);
//print("endSpot: ",endSpot);
//print(" ")
//print(" ")


        for index in (startSpot..<endSpot)
        {
            let pixel = pixelArray[index]
            if (pixel.k > 1)
            {
                havg += pixel.hue
                lavg += pixel.l
                realKount += 1;
            } else {
//print(" --------------------------------- index,pixel: ",index,pixel);
            }
        }

//print(" ")
//print(" ")
//print("realKount: ",realKount);
//print(" ")
//print(" ")

        let x0:CGFloat = CGFloat(highSpot) * 0.01;
        self.slopef = 0.0
        self.interceptYaxis = x0

        if (realKount > 0)
        {
            havg = havg / CGFloat(realKount)
            lavg = lavg / CGFloat(realKount)

//print("havg,lavg: ",havg,lavg);
//print(" ")
//print(" ")
            for index in (startSpot..<endSpot)
            {
                let pixel = pixelArray[index]
                if (pixel.k > 1)
                {
                    sumx = pixel.hue - havg;
                    sumy = pixel.l - lavg;

//                    sumxy  = sumx*sumy;
//                    sumxsq = sumx*sumx;
                } else {
//print(" --------------------------------- index,pixel: ",index,pixel);
                }
            }

//print("sumxy,sumxsq: ",sumxy,sumxsq);

            if (sumxsq != 0.0)
            {
                self.slopef = sumxy / sumxsq
                self.interceptYaxis = (lavg - (self.slopef * havg)) * 1.20 //1.20 1.40

//                let y0 = (x0  * self.slopef) + self.interceptYaxis   //************* keep for testing ***
//                let y  = (1.0 * self.slopef) + self.interceptYaxis   //************* keep for testing ***

//print("self.slopef: ",self.slopef);
//print("self.interceptYaxis: ",self.interceptYaxis);
//print("  ");
//print("x0: ",x0);
//print("y0: ",y0);
//print("y : ",y);
//print("  ");
//print("  ");
//print("  ");
            }
        }


//print(" ")
//print(" ")
//print(" ")
//print("------------- line Fit high 3 ----------------")
//print(" ")
        havg = 0.0;
        lavg = 0.0;

        sumx = 0.0;
        sumxsq = 0.0;
    //    var sumysq:CGFloat = 0.0;
        sumy = 0.0;
        sumxy = 0.0;

        realKount = 0;

        var startSpot3:Int = max(highSpot3-10,0);   //8
        var endSpot3:Int = min(startSpot3+20,pixelArray.count-6);  //30 8
//print("highSpot3: ",highSpot3);
//print("startSpot3: ",startSpot3);
//print("endSpot3: ",endSpot3);
//print(" ")
//print(" ")
        if (startSpot3 >= endSpot3)
        {
            endSpot3 = pixelArray.count-3;
            startSpot3 = max(endSpot3-15,0);
        }
//print("startSpot3: ",startSpot3);
//print("endSpot3: ",endSpot3);
//print(" ")
//print(" ")

        if (endSpot3 - startSpot3 < 11)
        {
            self.slopef3 = self.slopef
            self.interceptYaxis3 = self.interceptYaxis
            return;
        }

        for index in (startSpot3..<endSpot3)
        {
            let pixel = pixelArray[index]
            if (pixel.k > 1)
            {
                havg += pixel.hue
                lavg += pixel.l
                realKount += 1;
            } else {
//print(" --------------------------------- index,pixel: ",index,pixel);
            }
        }

//print(" ")
//print(" ")
//print("realKount: ",realKount);
//print(" ")
//print(" ")

        let x03:CGFloat = CGFloat(highSpot3) * 0.01;
        self.slopef3 = 0.0
        self.interceptYaxis3 = x03

        if (realKount > 0)
        {
            havg = havg / CGFloat(realKount)
            lavg = lavg / CGFloat(realKount)

//print("havg,lavg: ",havg,lavg);
//print(" ")
//print(" ")
            for index in (startSpot3..<endSpot3)
            {
                let pixel = pixelArray[index]
                if (pixel.k > 1)
                {
                    sumx = pixel.hue - havg;
                    sumy = pixel.l - lavg;

                    sumxy  = sumx*sumy;
                    sumxsq = sumx*sumx;
                } else {
//print(" --------------------------------- index,pixel: ",index,pixel);
                }
            }

//print("sumxy,sumxsq: ",sumxy,sumxsq);

            if (sumxsq != 0.0)
            {
                self.slopef3 = sumxy / sumxsq
                self.interceptYaxis3 = (lavg - (self.slopef3 * havg)) * 1.30 //1.20 1.40

//                let y0 = (x03  * self.slopef3) + self.interceptYaxis3  //************* keep for testing ***
//                let y  = (1.0 * self.slopef3) + self.interceptYaxis3    //************* keep for testing ***

//print("self.slopef3: ",self.slopef3);
//print("self.interceptYaxis3: ",self.interceptYaxis3);
//print("  ");
//print("x03: ",x03);
//print("y0: ",y0);
//print("y : ",y);
//print("  ");
//print("  ");
//print("  ");
            }
        }
    }

    //----------------------------------------------------------------------------------
    func smoothLine(pixelArray:[PECpixel], highSpot:Int)
    {
//print(" ")
//print("------------- leading 5 average smooth Line ----------------")
//print(" ")
        var y0:CGFloat = 0.0;
    //    var firsty0:CGFloat = 0.0;
        let highSpot0:Int = 10;
        let startSpot:Int = max(highSpot0-5,0);
        var id0:Int = 0;
        var pixelArray0:[PECpixel] = [PECpixel]()
//print("highSpot: ",highSpot);
//print("startSpot: ",startSpot);
//print(" ")
//print(" ")
        var pixel1:PECpixel = pixelArray[startSpot+1]
        var pixel2:PECpixel = pixelArray[startSpot+2]
        var pixel3:PECpixel = pixelArray[startSpot+3]
        var pixel4:PECpixel = pixelArray[startSpot+4]
        var pixel5:PECpixel = pixelArray[startSpot+5]

        y0 += pixel1.l
        y0 += pixel2.l
        y0 += pixel3.l
        y0 += pixel4.l

        id0 = 4;

        for index in (highSpot0..<pixelArray.count)
        {
            id0 += 1;
            var pixel = pixelArray[index]
            y0 += pixel.l

//print("index,id0,y0: ",index,id0,y0);

            if (id0 == 1)
            {
                pixel.l = y0 / 5.0
                pixel.lu = Int(roundf(Float(pixel.l) * 100.0))
                pixelArray0.append(pixel)
                y0 -= pixel2.l;
                pixel1 = pixel;
            }

            if (id0 == 2)
            {
                pixel.l = y0 / 5.0
                pixel.lu = Int(roundf(Float(pixel.l) * 100.0))
                pixelArray0.append(pixel)
                y0 -= pixel3.l;
                pixel2 = pixel;
            }

            if (id0 == 3)
            {
                pixel.l = y0 / 5.0
                pixel.lu = Int(roundf(Float(pixel.l) * 100.0))
                pixelArray0.append(pixel)
                y0 -= pixel4.l;
                pixel3 = pixel;
            }

            if (id0 == 4)
            {
                pixel.l = y0 / 5.0
                pixel.lu = Int(roundf(Float(pixel.l) * 100.0))
                pixelArray0.append(pixel)
                y0 -= pixel5.l;
                pixel4 = pixel;
            }

            if (id0 == 5)
            {
                pixel.l = y0 / 5.0
                pixel.lu = Int(roundf(Float(pixel.l) * 100.0))
                pixelArray0.append(pixel)
                id0 = 0;
                y0 -= pixel1.l;
                pixel5 = pixel;
            }
        }

//print(" ")
//print(" ")
//for pixel in pixelArray0
//{
//print("smooth pixel: ",pixel);
//}
//print(" ")
//print(" ")
    }


    // MARK: - touches tools
	//************** change 3.0.0 add function ***
	//----------------------------------------------------------------------------------
	func convertColor(red:Int,grn:Int,blu:Int)
	{
//print("  ");
//print("  ");
//print(" convertC olor red,grn,blu:",red,grn,blu);


//        let redf0 = CGFloat(red)/255.0;
//        let grnf0 = CGFloat(grn)/255.0;
//        let bluf0 = CGFloat(blu)/255.0;
//
////print("  ");
////print("redf0, greenf0, bluef0:",redf0, greenf0, bluef0);
//
//        let tmax:CGFloat = max(max(redf0, grnf0), bluf0);
//        let tmin:CGFloat = min(min(redf0, grnf0), bluf0);
//
//        let delta:CGFloat = tmax - tmin;
//
//        var sat:CGFloat = 0.0;
//        var hue:CGFloat = 0.0;
//
//        if (tmax > 0.0)
//        {
//            sat = delta / tmax;
//        }
//
//        if (delta > 0.0)
//        {
//            var divisor = 6.0
//
//            if ((redf0 > grnf0) && (redf0 > bluf0))
//            {
//                hue = CGFloat(modf(Double(grnf0 - bluf0) / Double(delta), &divisor)) * 0.6;
//            } else if ((grnf0 >= redf0) && (grnf0 > bluf0)) {
//                hue = (((bluf0 - redf0) / delta) + 2.0) * 0.6;
//            } else {
//                hue = (((redf0 - grnf0) / delta) + 4.0) * 0.6;
//            }
//
//            if (hue < 0.0)
//            {
//                hue = 3.6 + hue;
//            }
//        }
//
//        let hue10 = hue / 3.6;


//print(" convertC olor: hue10,sat,tmax:",hue10,sat,tmax);
		var sherwinColors0 = sherwinColors.copy() //as! [SherwinObject]

//print(" sherwinColors0:",sherwinColors0 as Any);
//print("  ");
//print("  ");

		var sherwinSorted:[SherwinObject]!
		var closestColor:SherwinObject!

		let currColor:SherwinObject = SherwinObject(code:8000,name:"curColor",red:Int(red),grn:Int(grn),blu:Int(blu))
		sherwinColors0.append(currColor)

//print(" currColor:",currColor as Any);
//print("  ");
//print("  ");

		sherwinSorted = sherwinColors0.sorted(by: { $0.hueSatLumKey < $1.hueSatLumKey })



//		if (red >= grn) && (red >= blu)
//		{
//			if (grn >= blu)
//			{
//				sherwinSorted = sherwinColors0.sorted(by: { $0.redGrnKey < $1.redGrnKey })
//			} else {
//				sherwinSorted = sherwinColors0.sorted(by: { $0.redBluKey < $1.redBluKey })
//			}
//
//		} else if (grn >= blu) && (grn >= red) {
//
//			if (blu >= red)
//			{
//				sherwinSorted = sherwinColors0.sorted(by: { $0.grnBluKey < $1.grnBluKey })
//			} else {
//				sherwinSorted = sherwinColors0.sorted(by: { $0.grnRedKey < $1.grnRedKey })
//			}
//
//		} else {
//
//			if (red >= grn)
//			{
//				sherwinSorted = sherwinColors0.sorted(by: { $0.bluRedKey < $1.bluRedKey })
//			} else {
//				sherwinSorted = sherwinColors0.sorted(by: { $0.bluGrnKey < $1.bluGrnKey })
//			}
//		}

//print(" sherwinSorted:",sherwinSorted as Any);
//print("  ");
//print("  ");

		var index00a:Int = 0;

		if let index00 = sherwinSorted.firstIndex(of: currColor)
		{
			index00a = index00;

//print(" index00a:",index00a);
//print("  ");
//print("  ");
			let startIndex = index00a-20
			let endIndex = index00a+20
	//		var sherwinHues:[SherwinObject] = [SherwinObject]()


			if (startIndex >= 0) && (endIndex <= sherwinSorted.count - 1)
			{
				var smallDist:CGFloat = 9999999.0

				for i in startIndex...endIndex
				{
					let testColor = sherwinSorted[i]
					if (testColor.code == currColor.code) {continue}

				//	let dist = sqrt(pow(((currColor.redf0-testColor.redf0)*0.3),2.0) + pow(((currColor.grnf0-testColor.grnf0)*0.59),2.0) + pow(((currColor.bluf0-testColor.bluf0)*0.11),2.0))
					let dist = sqrt(pow((currColor.redf0-testColor.redf0),2.0) + pow((currColor.grnf0-testColor.grnf0),2.0) + pow((currColor.bluf0-testColor.bluf0),2.0))
					if dist < smallDist
					{
						smallDist = dist
						closestColor = testColor
					}
				}

//				let sherwinHuesSorted = sherwinHues.sorted(by: { $0.grnRedKey < $1.grnRedKey })

//print(" closestColor:",closestColor as Any);
//print("  ");
//print("  ");


				self.codeNum.text = String(closestColor.code)
				self.colorName.text = closestColor.name

				let backColor = UIColor(red: closestColor.redf0, green: closestColor.grnf0, blue: closestColor.bluf0, alpha: 1.0)
				self.codeNum.backgroundColor   = backColor
				self.colorName.backgroundColor = backColor

//				if let index0 = sherwinHuesSorted.firstIndex(of: currColor)
//				{
//
//print(" index0:",index0);
//print("  ");
//print("  ");
//
////d = sqrt(((r2-r1)*0.3)^2 + ((g2-g1)*0.59)^2 + ((b2-b1)*0.11)^2)
//
//
//					if (index0 > 0) && (index0 < sherwinHuesSorted.count - 1)
//					{
//
//						let beforeColor = sherwinHuesSorted[index0-1]
//						let afterColor = sherwinHuesSorted[index0+1]
//
//print(" beforeColor:",beforeColor);
//print(" afterColor :",afterColor);
//print("  ");
//print("  ");
//
//						let diffBefore = abs(currColor.grnRedKey - beforeColor.grnRedKey)
//						let diffAfter  = abs(currColor.grnRedKey - afterColor.grnRedKey)
//
//print(" diffBefore,diffAfter:",diffBefore,diffAfter);
//print("  ");
//print("  ");
//						if (diffBefore < diffAfter)
//						{
//							closestColor = beforeColor
//						} else {
//							closestColor = afterColor
//						}
//
//						self.codeNum.text = String(closestColor.code)
//						self.colorName.text = closestColor.name
//
//						let backColor = UIColor(red: closestColor.redf0, green: closestColor.grnf0, blue: closestColor.bluf0, alpha: 1.0)
//						self.codeNum.backgroundColor   = backColor
//						self.colorName.backgroundColor = backColor
//					}
//				}
//			} else if (index == 0) {
//
//				let beforeColor = sherwinSorted.last!
//				let afterColor = sherwinSorted[index+1]
//
//print(" beforeColor:",beforeColor);
//print(" afterColor:",afterColor);
//print("  ");
//print("  ");
//
//				let diffBefore = abs(currColor.hueSatLumKey - beforeColor.hueSatLumKey)
//				let diffAfter  = abs(currColor.hueSatLumKey - afterColor.hueSatLumKey)
//
//print(" diffBefore,diffAfter:",diffBefore,diffAfter);
//print("  ");
//print("  ");
//				if (diffBefore < diffAfter)
//				{
//					closestColor = beforeColor
//				} else {
//					closestColor = afterColor
//				}
//
//				self.codeNum.text = String(closestColor.code)
//				self.colorName.text = closestColor.name
//
//				let backColor = UIColor(red: closestColor.redf0, green: closestColor.grnf0, blue: closestColor.bluf0, alpha: 1.0)
//				self.codeNum.backgroundColor   = backColor
//				self.colorName.backgroundColor = backColor
//			} else if (index == sherwinSorted.count - 1) {
//
//				let beforeColor = sherwinSorted[index-1]
//				let afterColor = sherwinSorted.first!
//
//print(" beforeColor:",beforeColor);
//print(" afterColor:",afterColor);
//print("  ");
//print("  ");
//
//				let diffBefore = abs(currColor.hueSatLumKey - beforeColor.hueSatLumKey)
//				let diffAfter  = abs(currColor.hueSatLumKey - afterColor.hueSatLumKey)
//
//print(" diffBefore,diffAfter:",diffBefore,diffAfter);
//print("  ");
//print("  ");
//				if (diffBefore < diffAfter)
//				{
//					closestColor = beforeColor
//				} else {
//					closestColor = afterColor
//				}
//
//				self.codeNum.text = String(closestColor.code)
//				self.colorName.text = closestColor.name
//
//				let backColor = UIColor(red: closestColor.redf0, green: closestColor.grnf0, blue: closestColor.bluf0, alpha: 1.0)
//				self.codeNum.backgroundColor   = backColor
//				self.colorName.backgroundColor = backColor
			}
		}

//print(" closestColor:",closestColor as Any);
//print("  ");
//print("  ");
	}

	//----------------------------------------------------------------------------------
    func findColorMask(_ touch:CGPoint)
    {
//print("findColo rMask")
        self.pixelArray.removeAll()

        if (includeGray) {return;}

        if (currentButtonToDisplay >= 4) {return;}
    //    if (currentWellNum < 0) {return;}

//maskedSid ingImage = maskedHou seImage1
//print("maskedSidi ngImage: ",maskedSid ingImage as Any);

        guard let pecImage = self.scrollView.imageView.image else {return;}
        guard let sideIamge = maskedSidingImage else {return;}

        let targetImageRect:CGRect = CGRect(x:0.0, y:0.0, width:sideIamge.size.width, height:sideIamge.size.height);

//print("  ")
//print("targetImageRect: ",targetImageRect);
//print("  ")
//print("  ")
//print("maskedSidi ngImage: ",maskedSidingI mage as Any);
//print("  ")
//print("  ")
        var overlay:UIImage? = nil

        //--------- make fresh paint overlay if paint mask exists ----------------
        if let maskedSidingImage0 = maskedSidingImage
        {
            autoreleasepool
            {
                UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, 1.0);

                    pecImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
                    maskedSidingImage0.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                    overlay = UIGraphicsGetImageFromCurrentImageContext();

                UIGraphicsEndImageContext();
            }
        }

//print("  ")
//print("  ")
//print("overlay: ",overlay as Any);

        //--------- run through paint mask if it exists ----------------
        if let filtImage0 = overlay
        {
            forcedImageSize = Size(width: Float(filtImage0.size.width), height: Float(filtImage0.size.height))
//print("self.currentOv erlay.ima ge.size:",self.currentOv erlay.ima ge?.size as Any)
//print("self.scrollView.imag eView.ima ge?.size:",self.scrol lView.imag eView.ima ge?.size as Any)
//print("forcedIm ageSize:",forcedImageSize)
            let posterFilter = Posterize() //Posterize()  ToonFilter()

//print("  ")
//print("  ")
//print("filtImage0:",filtImage0)
//print("  ")

            let pictureInput = PictureInput(image:filtImage0)
            let pictureOut   = PictureOutput()

            pictureOut.bufferAvailableCallback =
            { byteBuffer0 in
//print("extractStartpts FromImage 1 ");

                let imageByteSize:Int = Int(forcedImageSize.width) * Int(forcedImageSize.height) * 4;
//                let imageWidthSpan:Int = Int(forcedImageSize.width * 4.0);   //************* keep for testing ***
                var currentByte:Int = 0;
//print("  ");
//print("imageByteSize ",imageByteSize);
//print("imageWidthSpan ",imageWidthSpan);
//print("  ");
//print("  ");
//print("touch ",touch);
//print("self.scrollView.imageView.frame ",self.scrollView.imageView.frame);

//                var onImage:Bool = false;
//                var localPoint1:CGPoint = touch;
//
//                if let theView1 = self.scrollView.imageView
//                {
//                    localPoint1 = self.convertPoint(point:touch, fromRect:theView1.frame, toRect:targetImageRect);
//                }
//
//
//                let touchByte:Int = (Int(localPoint1.y) * imageWidthSpan) + (Int(localPoint1.x) * 4);
//
//print("touchByte,Int(touch.x) * Int(touch.y) * 4 ",touchByte,Int(touch.x) * Int(touch.y) * 4);
//print("  ");
//print("  ");


                //------------ calculate minimums -------------
                let dmaxRatio: CGFloat = 0.4
                let dminRatio: CGFloat = 0.4
                let cmaxRatio: CGFloat = 0.32
                let cminRatio: CGFloat = 0.32

                var hueNormalRatio = hueNormal00

                if hueNormalRatio > 0.5
                {
                    hueNormalRatio = 1.0 - hueNormalRatio
                }

                var dmaxAmount = max(hueNormalRatio * dmaxRatio, 0.04166)  //0.042
                var dminAmount = max(hueNormalRatio * dminRatio, 0.04166)
                var cmaxAmount = max(hueNormalRatio * cmaxRatio, 0.02942)
                var cminAmount = max(hueNormalRatio * cminRatio, 0.02942)

                if hueNormalRatio < 0.035
                {
                    dmaxAmount = 0.0833  //0.085
                    dminAmount = 0.0833
                    cmaxAmount = 0.065
                    cminAmount = 0.065
                }

                let dmin = (hueNormal00 - dminAmount);
                let dmax = (hueNormal00 + dmaxAmount);
                let cmin = (hueNormal00 - cminAmount);
                let cmax = (hueNormal00 + cmaxAmount);

                let emin = dmin * 0.85;

                let ddelta = dmax - dmin;
        //        let angle:CGFloat = (mpi2 * hueNormal00);   //************* keep for testing ***
                var doffi:Int = 0;

                if (dmin < 0.0)
                {
                    let dminI = Int(roundf(Float(dmin*100.0)));
                    doffi = abs(dminI)
                    doffi += 1;
                }


//print("  ");
//print("dminAmount,dmaxAmount: ",dminAmount,dmaxAmount);
//print("dmin,dmax,ddelta: ",dmin,dmax,ddelta);
//print("emin: ",emin);
//print("flippedBot: ",flippedBot);
//print("flippedTop: ",flippedTop);
//print("doffi: ",doffi);
//print("hueNormal00: ",hueNormal00);
//print("  ");
//print("angle  : ",angle);
//print("  ");

                //------------ set up hue array -------------
                var pixelArrayT:[PECpixel] = [PECpixel]()
                for i:Int in 0...110
                {
                    let pixel:PECpixel = PECpixel.init(h: i)
                    pixelArrayT.append(pixel)
                }

                //------------ set up lum array -------------
                var pixelArrayL:[PECpixel] = [PECpixel]()
                for i:Int in 0...100
                {
                    let pixel:PECpixel = PECpixel.init(lu: i)
                    pixelArrayL.append(pixel)
                }

                //------------ set up sat array -------------
                var pixelArrayS:[PECpixel] = [PECpixel]()
                for i:Int in 0...100
                {
                    let pixel:PECpixel = PECpixel.init(h: i)
                    pixelArrayS.append(pixel)
                }


                var index:Int = 0;
                var indLum:Int = 0;
                var indSat:Int = 0;

                //------------ roll though paint mask -------------
                while (currentByte < imageByteSize)
                {
                    let alphaByte = byteBuffer0[currentByte+3];

                    //------------ check if paint at pixel -------------
                    if (alphaByte > 254)
                    {
                        index += 1;
                        let redByte   = byteBuffer0[currentByte+0];
                        let greenByte = byteBuffer0[currentByte+1];
                        let blueByte  = byteBuffer0[currentByte+2];

//                        if (currentByte == touchByte)
//                        {
//                            onImage = true;
//                        }

                        let redf0:CGFloat   = CGFloat(redByte)*0.00392156862745;
                        let greenf0:CGFloat = CGFloat(greenByte)*0.00392156862745;
                        let bluef0:CGFloat  = CGFloat(blueByte)*0.00392156862745;


                        let tmax:CGFloat = max(max(redf0, greenf0), bluef0);
                        let tmin:CGFloat = min(min(redf0, greenf0), bluef0);

                        let delta:CGFloat = tmax - tmin;

                        var sat:CGFloat = 0.0;
                        var hue:CGFloat = 0.0;

                        if (tmax > 0.0)
                        {
                            sat = delta / tmax;
                        }

                        if (delta > 0.0)
                        {
                            var divisor = 6.0

                            if ((redf0 > greenf0) && (redf0 > bluef0))
                            {
                                hue = CGFloat(modf(Double(greenf0 - bluef0) / Double(delta), &divisor)) * 0.6;
                            } else if ((greenf0 >= redf0) && (greenf0 > bluef0)) {
                                hue = (((bluef0 - redf0) / delta) + 2.0) * 0.6;
                            } else {
                                hue = (((redf0 - greenf0) / delta) + 4.0) * 0.6;
                            }

//                            if (hue < 0.0)
//                            {
//                                hue = 3.6 + hue;
//                            }
                        }

                        hue = hue * 0.2777778;  //-----  / 3.6  ----


                        //------------ load Saturation -----------
                        let satI:Int = Int(roundf(Float(sat) * 100.0));
                        var pixelsu:PECpixel = pixelArrayS[satI];


                        if (hue > dmin) && (hue < dmax)
                        {
                            indSat += 1;
                            pixelsu.k = pixelsu.k + 1;
                            pixelsu.l = pixelsu.l + tmax;
                        }

                        if (hue > dmin) && (hue < dmax)
                        {
                            if (tmax < 0.38)
                            {
                                pixelsu.smax += 1.0;
                                if (tmax < 0.28)
                                {
                                    pixelsu.lmin += 1.0;
                                    if (tmax < 0.18)
                                    {
                                        pixelsu.lmax += 1.0;
                                    }
                                }
                            }

                            if (tmax >= 0.42)
                            {
                                pixelsu.r += 1.0;
                                if (tmax >= 0.52)
                                {
                                    pixelsu.g += 1.0;
                                    if (tmax >= 0.62)
                                    {
                                        pixelsu.b += 1.0;
                                        if (tmax >= 0.72)
                                        {
                                            pixelsu.hl += 1.0;
                                        }
                                    }
                                }
                            }
                        }

                        let placesu:Range = satI..<satI+1
                        pixelArrayS.replaceSubrange(placesu, with: [pixelsu])


                        //------------ load luminence by kount -----------
                        let tmaxI:Int = Int(roundf(Float(tmax) * 100.0));
                        var pixelu:PECpixel = pixelArrayL[tmaxI];

                        if (hue > dmin) && (hue < dmax)
                        {
                            indLum += 1;
                            pixelu.k = pixelu.k + 1;
                            pixelu.s = pixelu.s + sat;
                            pixelu.hue = pixelu.hue + hue;
                        }

                        let placelu:Range = tmaxI..<tmaxI+1
                        pixelArrayL.replaceSubrange(placelu, with: [pixelu])


                        //------------ load hue by kount -----------
                        let hue0:Int = (Int(roundf(Float(hue) * 100.0)));

                        if (doffi > 0)
                        {
                            let hueOffset:Int = hue0 + doffi;

                            if (hueOffset >= 0) && (hueOffset < pixelArrayT.count)
                            {
                                var pixel:PECpixel = pixelArrayT[hueOffset];
                                pixel.k = pixel.k + 1;

                                pixel.h   = hue0;
                                pixel.hue = CGFloat(hue0) * 0.01;

                                pixel.s = pixel.s + sat;
                                pixel.l = pixel.l + tmax;

                                let place:Range = hueOffset..<hueOffset+1
                                pixelArrayT.replaceSubrange(place, with: [pixel])
                            }

                        } else {

                            if (hue0 >= 0) && (hue0 < pixelArrayT.count)
                            {
                                var pixel:PECpixel = pixelArrayT[hue0];
                                pixel.k = pixel.k + 1;

                                pixel.s = pixel.s + sat;
                                pixel.l = pixel.l + tmax;

                                let place:Range = hue0..<hue0+1
                                pixelArrayT.replaceSubrange(place, with: [pixel])
                            }
                        }
                    }

                    currentByte = currentByte + 4;
                }

//print("  ");
//print("  ");
//print("------------------ onImage -----------------------",onImage);
//                //--------- touch was not on paint mask ------------
//                if ( !onImage) {return;}


                //----------- clear low kount lums --------------
                let lumCut = Int(CGFloat(indLum) * 0.00125)  //0.005
                let pixellastLu = pixelArrayL.last;
                var lastLu:CGFloat = pixellastLu?.l ?? 0.0;

//lumCut:  1524 purple 1443
//print("  ");
//print("  ");
//print("index: ",index);
//print("indLum: ",indLum);
//print("lumCut: ",lumCut);
//print("pixelArrayL.count: ",pixelArrayL.count);
//print("  ");
                var pixelArrayL0:[PECpixel] = [PECpixel]()
                var highCutLu: CGFloat = 1.0
                var firstLu: Bool = true;
                var lowSat: CGFloat = 1.0
                var higSat: CGFloat = 0.0

                for var pixel in pixelArrayL.reversed()
                {
                    if (pixel.l > 0.001) && (pixel.l < 0.999)
                    {
                        if (pixel.k > lumCut)
                        {
                            if (pixel.k > 0)
                            {
                                pixel.s = pixel.s / CGFloat(pixel.k)
                                pixel.hue = pixel.hue / CGFloat(pixel.k)

                                if (pixel.hue > emin) && (pixel.hue < dmax) && (pixel.s > 0.001)
                                {
                                    if (pixel.s < lowSat) {lowSat = pixel.s}
                                    if (pixel.s > higSat) {higSat = pixel.s}
                                }
                            }
//print("lum pixel: ",  pixel);

                            if (firstLu)
                            {
                                firstLu = false;
                                highCutLu = pixel.l;
//print("lum pixel: ", pixel);
                            }
                            pixel.diffH = lastLu - pixel.l;
                            lastLu = pixel.l;
//print("lum pixel: ",pixel);
                            pixelArrayL0.append(pixel)
                        }
                    }
                }
//print("  ");
//print("  ");
//print("pixelArrayL0.count: ",pixelArrayL0.count);
//print("  ");
//print("lum00,highCutLu: ",lum00,highCutLu);
//print("higSat: ",higSat);
//print("lowSat: ",lowSat);
//print("  ");

//print("  ");
//print("  ");
//print("pixelAr rayL0.count: ",pixelAr rayL0.count);
//for pixel in pixelAr rayL0
//{
//print("lumPrecut pixel: ",pixel);
//}
//print("  ");


                    let cutOffLu: CGFloat = 0.05

                    //----------- now find gaps caused by low kount lum removal to find high Lum --------------
                    for pixel in pixelArrayL0
                    {
                        if (pixel.diffH >= cutOffLu)
                        {
//print("pixel.l,lum00: ",pixel.l,lum00);
                            if (pixel.l > lum00)
                            {
                                highCutLu = pixel.l;
                            }
                        }
                    }

                    pixelArrayL.removeAll();
                    pixelArrayL0.removeAll();
//print("  ");
//print("highCutLu:",highCutLu);
//print("  ");
//print("  ");

                let satCut = CGFloat(indSat) * 0.00125  //0.005

//print("  ");
//print("sat indSat: ",indSat);
//print("sat satCut: ",satCut);
//print("  ");

                    //------------- start saturation calculations ---------------
                    var pixelArrayS0:[PECpixel] = [PECpixel]()
                    for var pixel in pixelArrayS
                    {
                        if (pixel.k > 0)
                        {
                            pixel.l = pixel.l / CGFloat(pixel.k)
                        }
                        pixel.lu = Int(pixel.l * 100.0)

//print("sat pixel: ",pixel);

                        pixelArrayS0.append(pixel)
                    }



//print("  ");
//print("  ");
                for pixel in pixelArrayS
                {
                        if (pixel.smax > satCut)
                        {
//print("sat smax pixel: ",pixel);
                        }
                }

//print("  ");
//print("  ");



//print("  ");
//print("  ");
                for pixel in pixelArrayS
                {
                        if (pixel.lmin > satCut)
                        {
//print("sat lmin pixel: ",pixel);
                        }
                }

//print("  ");
//print("  ");


//print("  ");
//print("  ");
                for pixel in pixelArrayS
                {
                        if (pixel.lmax > satCut)
                        {
//print("sat lmax pixel: ",pixel);
                        }
                }

//print("  ");
//print("  ");




                    pixelArrayS.removeAll();



                    //------------- start hue calculations ---------------
                    let kSmall:Int = Int(CGFloat(index) * 0.00025)

//print("  ");
//print("  ");
//print("kSm all ",kSmall);
//print("  ");

//print("  ");
//print("  ");
//print("pixelArr ayT.count: ",pixelArrayT.count);
//                    for pixel in pixelArrayT   //************* keep for testing ***
//                    {
////print("range pixel: ",pixel);
//                    }

//print("  ");
//print("  ");

                    //---------- find hue count deviations ------------
                    var pixelfrst = pixelArrayT.first;
                    var lastH:CGFloat = pixelfrst?.hue ?? 0.0001;

                    if (doffi > 0)
                    {
                        if (lastH == 0.0)
                        {
                            pixelArrayT.removeFirst();
                            pixelfrst = pixelArrayT.first;
                            lastH = pixelfrst?.hue ?? 0.0;
                        }
                    }

                    for var pixel in pixelArrayT
                    {
                        if (pixel.hue >= cmin) && (pixel.hue <= cmax)
                        {
                            if (pixel.k < 1)
                            {
                                pixel.k = 1
                            }
                            pixel.diffH = pixel.hue - lastH;
                            lastH = pixel.hue;

                            pixel.s = pixel.s / CGFloat(pixel.k)
                            pixel.l = pixel.l / CGFloat(pixel.k)

                            self.pixelArray.append(pixel)
                        } else {
                            if (pixel.k > kSmall)
                            {
                                pixel.diffH = pixel.hue - lastH;
                                lastH = pixel.hue;

                                pixel.s = pixel.s / CGFloat(pixel.k)
                                pixel.l = pixel.l / CGFloat(pixel.k)

                                self.pixelArray.append(pixel)
                            }
                        }
                    }

                    pixelArrayT.removeAll();



//            //        var kAvg:Float = 0.0;
//                    var kKount:Int = 0;
//                    var pixelArr ayT0:[PECpi xel] = [PECpi xel]()
//
//                    for pixel in pixelAr rayT
//                    {
//                        if (pixel.k > kSmall)
//                        {
//                //            kAvg += Float(pixel.k);
//                            kKount += 1
//                        }
//                    }
//
//print("  ");
//print("kKount: ",kKount);
//print("  ");
//print("  ");

//                    let sortByk = self.pix elArray.sorted(by: { $0.k > $1.k })
//
//                    for pixel in sortByk
//                    {
//print("sorted pixel: ",pixel);
//                    }

//print("  ");
//print("  ");
//
//print("self.pixelAr ray.count: ",self.pixelAr ray.count);
//print("  ");
//print("  ");


//                    if (self.pi xelArray.count > 30)
//                    {
//                        let sortByk = self.pix elArray.sorted(by: { $0.k > $1.k })
//
//                        let pixel30 = sortByk[30];
//                        kSmall = pixel30.k
//print("  ");
//print("2 kSm all ",kSmall);
//print("  ");
//
//                        pixelfrst = self.pixelAr ray.first;
//                        lastH = pixelfrst?.hue ?? 0.0;
//                        var pixelAr rayT0:[PECpi xel] = [PECpi xel]()
//                        pixelAr rayT0.append(contentsOf: self.pix elArray)
//                        self.pixelAr ray.removeAll();
//
//                        for var pixel in pixelAr rayT0
//                        {
//                            if (pixel.k > kSmall)
//                            {
//                                pixel.diffH = pixel.hue - lastH;
//                                lastH = pixel.hue;
//
//                                self.pixel Array.append(pixel)
//                            }
//                        }
//
//                        pixelAr rayT0.removeAll();
//                    }


//print("  ");
//print("  ");



//print("  ");
//print("  ");
//print("self.pixelAr ray.count: ",self.pixelArray.count);
//for pixel in self.pixelArray  //************* keep for testing ***
//{
////print("before any pixel: ",pixel);
//}
//print("  ");
//print("  ");
//print("  ");
//print("  ");



                    var lowCut: CGFloat = -2.0
                    var highCut: CGFloat = 2.0

                    var lowDone: Bool = false
                    var cutOff: CGFloat = 0.019

                    for pixel in self.pixelArray.reversed()
                    {
                        if (pixel.h >= 35)
                        {
                            cutOff = 0.049
                        } else if (pixel.h >= 30) {
                            cutOff = 0.039
                        } else if (pixel.h >= 20) {
                            cutOff = 0.029
                        } else if (pixel.h >= 10) {
                            cutOff = 0.025
                        } else if (pixel.h < 0) {
                            cutOff = 2.0
                        } else {
                            cutOff = 0.019
                        }

                        if (pixel.diffH >= cutOff)
                        {
//print("pixel.h,hueNormal00: ",pixel.h,hueNormal00);
                            if (pixel.hue - 0.015 <= hueNormal00)
                            {
                                if ( !lowDone)
                                {
                                    lowCut = pixel.hue;
                                    lowDone = true;
                                }
                            } else {
                                highCut = pixel.hue;
                            }
                        }
                    }


//print("  ");
//print("  lowCut,highCut :",lowCut,highCut);


                    for indx in (0..<self.pixelArray.count).reversed()
                    {
                        let pixel = self.pixelArray[indx]

                        if (pixel.hue >= dmin) && (pixel.hue <= dmax)
                        {
                        } else {
                            if (pixel.hue < lowCut)
                            {
                                self.pixelArray.remove(at: indx)
                            } else if (pixel.hue >= highCut) {
                                self.pixelArray.remove(at: indx)
                            }
                        }
                    }

//print("  ");
//print("  self.pixelAr ray.count:",self.pixelArray.count);


//for pixel in self.pixelArray  //************* keep for testing ***
//{
////print("before new pixel: ",pixel);
//}
//print("  ");
//print("  ");



                    //---------- find saturation standard deviation ------------
                    var sAvg:CGFloat = 0.0;
                    self.upperSDev = 1.0;
                    self.lowerSDev = 0.0;
                    var pixelArray00:[PECpixel] = [PECpixel]()

                    for pixel in self.pixelArray
                    {
                        sAvg += pixel.s;
                    }

                    if (self.pixelArray.count > 0)
                    {
                        sAvg = sAvg / CGFloat(self.pixelArray.count);
//print("sAvg: ",sAvg);

                        var sVariance:Float = 0.0;

                        for pixel in self.pixelArray
                        {
                            let sv = (pixel.s - sAvg);
                            sVariance += Float(sv*sv);
                        }

                        sVariance = sVariance / Float(self.pixelArray.count);
                        let sDeviation:Float = sqrtf(sVariance);

//print("sVariance,sDeviation: ",sVariance,sDeviation);
//print("  ");
//print("  ");

                        var upperExtra:CGFloat = 1.20 //1.10 //1.20
                        var lowerExtra:CGFloat = 0.833 //0.91 //0.833

                        let upperSDev0 = (sAvg + CGFloat(sDeviation));
                        let lowerSDev0 = (sAvg - CGFloat(sDeviation));
//print("upperSDev0: ",upperSDev0);
//print("lowerSDev0: ",lowerSDev0);
//print("  ");






//                    let per cDeviat:CGFloat = 1.20;
//                //        per cDeviat = 1.0 + (CGFloat(sDeviation) / sAvg)
//
//                    self.up perSDev = upperSDev0 * upperExtra;
//                    self.lo werSDev = min(lowerSDev0 * lowerSx,0.1125);
//
//
//print("self.uppe rSDev: ",self.upp erSDev);
//print("self.lowe rSDev: ",self.low erSDev);
//print("  ");
//
//                    let hig hSpot:Int = Int(roundf(Float(self.uppe rSDev * 100.0)));
//                    self.lineF it(pixelAr ray:pixelAr rayS0, highSpot:highSpot, percDev iat:percD eviat)
//
//                    let low Spot:Int = Int(roundf(Float(self.lowe rSDev * 100.0)));
//                    self.lineFi tLow(pixelAr ray:pixelAr rayS0, lowSpot:lowSpot, percDe viat:percD eviat)





                        if (sAvg < 0.18)
                        {
                            let amount:CGFloat = 1.0 + ((0.18 - sAvg) * 2.7);
                            upperExtra *= amount;
                            lowerExtra  = 1.0/upperExtra;

//print("amount,upperExtra,lowerExtra: ",amount,upperExtra,lowerExtra);
//print("  ");
//print("  ");
                        }


                        let upperSD  = upperSDev0 * upperExtra;
                        let lowerSD  = lowerSDev0 * lowerExtra;

//                        let upperSD2  = upperSDev0 * 1.25;
//                        let lowerSD2  = lowerSDev0 * 0.80;

                        let upperSD3  = upperSDev0 * 1.33;
                        let lowerSD3  = lowerSDev0 * 0.75;

//print("upperSDev,lowerSDev: ",(sAvg + CGFloat(sDeviation)),(sAvg - CGFloat(sDeviation)));
//print("upperSD,lowerSD    : ",upperSD,lowerSD);
//print("upperSD2,lowerSD2  : ",upperSD2,lowerSD2);
//print("upperSD3,lowerSD3  : ",upperSD3,lowerSD3);
//print("  ");
//print("  ");

                        pixelfrst = self.pixelArray.first;
                        lastH = pixelfrst?.hue ?? 0.0;

                        //---------- remove deviations of saturation ------------
                        for var pixel in self.pixelArray
                        {
                            if (pixel.hue >= dmin) && (pixel.hue <= dmax)
                            {
                                if (pixel.s > lowerSD3) && (pixel.s < upperSD3)
                                {
                                } else {
                                    pixel.s = -1.0;
                                }
                                pixel.diffH = pixel.hue - lastH;
                                lastH = pixel.hue;
                                pixelArray00.append(pixel)
                            } else if (pixel.s > lowerSD) && (pixel.s < upperSD) {
                                pixel.diffH = pixel.hue - lastH;
                                lastH = pixel.hue;
                                pixelArray00.append(pixel)
                            }
                        }
                    }

//print(" pixelArray00 ");
//                    for pixel in pixelArray00     //************* keep for testing ***
//                    {
////print("pixel: ",pixel);
//                    }
//print("  ");
//print("  ");


                    lowCut = -2.0
                    highCut = 2.0

                    lowDone = false
                    cutOff = 0.02

                    //---------- find low and high cutoff ------------
                    for pixel in pixelArray00.reversed()
                    {
                        if (pixel.h >= 35)
                        {
                            cutOff = 0.049
                        } else if (pixel.h >= 30) {
                            cutOff = 0.039
                        } else if (pixel.h >= 20) {
                            cutOff = 0.029
                        } else if (pixel.h >= 10) {
                            cutOff = 0.025
                        } else if (pixel.h < 0) {
                            cutOff = 2.0
                        } else {
                            cutOff = 0.019
                        }

                        if (pixel.diffH >= cutOff)
                        {
//print("pixel.h,hueNormal00: ",pixel.h,hueNormal00);
                            if (pixel.hue - 0.015 <= hueNormal00)
                            {
                                if ( !lowDone)
                                {
                                    lowCut = pixel.hue;
                                    lowDone = true;
                                }
                            } else {
                                highCut = pixel.hue;
                            }
                        }
                    }

//print("  ");
//print("  lowCut,highCut:",lowCut,highCut);
//print("  ");
//print("  ");
                    self.pixelArray.removeAll()

                    //---------- remove deviations of place skips in hue ------------
                    for pixel in pixelArray00
                    {
                        if (pixel.hue >= dmin) && (pixel.hue <= dmax)
                        {
                            self.pixelArray.append(pixel)
                        } else if (pixel.hue >= lowCut) && (pixel.hue < highCut) {
                            self.pixelArray.append(pixel)
                        }
                    }


//print(" self.pixelArray.count: ",self.pixelArray.count);
//                    for pixel in self.pixelArray    //************* keep for testing ***
//                    {
////print("pixel: ",pixel);
//                    }
//print("  ");
//print("  ");

                    //------------- remove first pixel and last pixel if sat is out of range ---------
                    if (self.pixelArray.count > 4)
                    {
                        let pixel0 = self.pixelArray[0]
                        let pixel1 = self.pixelArray[1]
                        let pixel2 = self.pixelArray[2]
                        let pixel3 = self.pixelArray[3]

                        if (pixel0.s < -0.5)
                        {
                //            self.pixelArray.removeFirst()
                        } else {

                            var satAvg3:CGFloat = 0.0;
                            var numSatAvg3:CGFloat = 0.0;

                            if (pixel1.s > 0.0) {satAvg3 += pixel1.s; numSatAvg3 += 1.0}
                            if (pixel2.s > 0.0) {satAvg3 += pixel2.s; numSatAvg3 += 1.0}
                            if (pixel3.s > 0.0) {satAvg3 += pixel3.s; numSatAvg3 += 1.0}

                            if (numSatAvg3 > 0.0)
                            {
                                satAvg3 = satAvg3 / numSatAvg3;
                                if (satAvg3 > 0.0)
                                {
                                    let satP   = pixel0.s / satAvg3;
//print("satAvg3,satP: ",satAvg3,satP);

                                    if (pixel0.hue >= dmin) && (pixel0.hue <= dmax)
                                    {
                                    } else if (satP < 0.42) || (satP > 1.75) {
                                        self.pixelArray.removeFirst()
                                    }
                                }
                            } else {
                                self.pixelArray.removeFirst()
                            }
                        }

//                        if let pixelLt = self.pixelArray.last
//                        {
//                            if (pixelLt.s < -0.5)
//                            {
//                                self.pixelArray.removeLast()
//                            }
//                        }


//print("was a first remove?  self.pixelArray.count: ",self.pixelArray.count);
//for pixel in self.pixelArray   //************* keep for testing ***
//{
////print("pixel: ",pixel);
//}
//print("  ");
//print("  ");
                    }


//print("  ");
//print("  ");
//print("ind exes?  self.pixelArray.count: ",self.pixelArray.count);
//for ind ex in (0..<self.pixelArray.count).reversed()
//{
//print("inde x: ",ind ex);
//    let pixel = self.pixelArray[inde x]
//print("pixel: ",pixel);
//}
//print("  ");
//print("  ");




                    let pixelLow  = self.pixelArray.first
                    let pixelHigh = self.pixelArray.last

                    let lowHue = pixelLow?.hue ?? 0.0;
                    let highHue = pixelHigh?.hue ?? 1.0;
                    let deltaHue = highHue - lowHue;


//print("  ");
//print("0 lowHue,highHue,deltaHue,(ddelta*1.05): ",lowHue,highHue,deltaHue,(ddelta*1.05));
//print("  ");
//                    if (self.flipped)
//                    {
//
//                    } else {

                        let deltaLow = hueNormal00 - lowHue;
                        let deltaHig = highHue - hueNormal00;
                        var ddeltaP:CGFloat = 1.05;
                        var deltaDistP:CGFloat = 1.0;

                        if (deltaHig > 0.0)
                        {
                            deltaDistP = deltaLow / deltaHig;
                            if (deltaLow > deltaHig)
                            {
                                deltaDistP = 1.0/deltaDistP
                            }
                        }
//print("  ");
//print("deltaLow,deltaHig,ddeltaP: ",deltaLow,deltaHig,ddeltaP);
//print("deltaDistP: ",deltaDistP);
//print("hueNormal00: ",hueNormal00);
//print("dmin,dmax: ",dmin,dmax);
//print("  ");
//print("  ");

                        if (self.pixelArray.count > 18)
                        {
                        } else {
                            ddeltaP = 1.20;
                        }

                        if (hueNormal00 < 0.0625)
                        {
//print("(hueNormal00 < 0.0625)");
                            for indx in (0..<self.pixelArray.count).reversed()
                            {
                                let pixel = self.pixelArray[indx]

                                if (pixel.hue < dmin-0.005)
                                {
                                    self.pixelArray.remove(at: indx)
                                }
                            }

                            for indx in (0..<self.pixelArray.count).reversed()
                            {
                                let pixel = self.pixelArray[indx]

                                if (pixel.hue > dmax+0.005)
                                {
                                    self.pixelArray.remove(at: indx)
                                }
                            }
                        }
                        else if (deltaHue > (ddelta*ddeltaP))
                        {
                            if (hueNormal00 < 0.125)
                            {
//print("(hueNormal00 < 0.125)");
                                for indx in (0..<self.pixelArray.count).reversed()
                                {
                                    let pixel = self.pixelArray[indx]

                                    if (pixel.hue > dmax)
                                    {
                                        self.pixelArray.remove(at: indx)
                                    }
                                }

                                if (deltaLow > (dminAmount*ddeltaP))
                                {
//print("(hueNormal00 < 0.125) deltaLow > dminAmount");
                                    self.pixelArray.removeFirst()
                                }
                            }
                            else if (hueNormal00 < 0.25)
                            {
//print("(hueNormal00 < 0.25)");
                                for indx in (0..<self.pixelArray.count).reversed()
                                {
                                    let pixel = self.pixelArray[indx]

                                    if (pixel.hue < dmin)
                                    {
                                        self.pixelArray.remove(at: indx)
                                    }
                                }

                                if (deltaHig > (dmaxAmount*ddeltaP))
                                {
//print("(hueNormal00 < 0.25) deltaHig > dmaxAmount");
                                    self.pixelArray.removeLast()
                                }
                            }
                            else if (hueNormal00 > 0.875)
                            {
//print("(hueNormal00 > 0.875)");
                                for indx in (0..<self.pixelArray.count).reversed()
                                {
                                    let pixel = self.pixelArray[indx]

                                    if (pixel.hue < dmin)
                                    {
                                        self.pixelArray.remove(at: indx)
                                    }
                                }

                                if (deltaHig > (dmaxAmount*ddeltaP))
                                {
//print("(hueNormal00 > 0.875) deltaHig > dmaxAmount");
                                    self.pixelArray.removeLast()
                                }
                            }
                            else if (hueNormal00 > 0.75)
                            {
//print("(hueNormal00 > 0.75)");
                                for indx in (0..<self.pixelArray.count).reversed()
                                {
                                    let pixel = self.pixelArray[indx]

                                    if (pixel.hue > dmax)
                                    {
                                        self.pixelArray.remove(at: indx)
                                    }
                                }

                                if (deltaLow > (dminAmount*ddeltaP))
                                {
//print("(hueNormal00 > 0.75) deltaLow > dminAmount");
                                    self.pixelArray.removeFirst()
                                }
                            }
                            else if (deltaDistP >= 0.725)
                            {
//print("deltaDistP >= 0.725");
                                var deltaDiff:CGFloat = 1.0 // ((ddelta*ddeltaP) * 0.5) * 1.625;

                                if (deltaHue > 0.0)
                                {
                                    deltaDiff = ddelta / deltaHue // ((ddelta*ddeltaP) * 0.5) * 1.625;
//print("ddelta / deltaHue: ",ddelta / deltaHue);
                                }
                                deltaDiff *= 1.75


                                var deltaLowDiff = (hueNormal00 - (deltaLow*deltaDiff)) - 0.05;
                                let deltaHigDiff = (hueNormal00 + (deltaHig*deltaDiff)) + 0.05;

//print("deltaLow,deltaHig: ",(deltaLow*deltaDiff),(deltaHig*deltaDiff));
//print("deltaDiff,deltaLowDiff,deltaHigDiff: ",deltaDiff,deltaLowDiff,deltaHigDiff);
//print(" ");

                                if (hueNormal00 > 0.125) && (deltaLowDiff < 0.10)
                                {
                                    let altDiff = min(hueNormal00 - 0.10,hueNormal00 - dmin)
                                    deltaDiff = altDiff / deltaLow;

                                    deltaLowDiff = (hueNormal00 - (deltaLow*deltaDiff))
                            //        deltaHigDiff = (hueNormal00 + (deltaHig*deltaDiff))
//print("altDiff,deltaDiff,deltaLowDiff,deltaHigDiff: ",altDiff,deltaDiff,deltaLowDiff,deltaHigDiff);
//print(" ");
                                }


                                for indx in (0..<self.pixelArray.count).reversed()
                                {
                                    let pixel = self.pixelArray[indx]

                                    if (pixel.hue < deltaLowDiff)
                                    {
                                        self.pixelArray.remove(at: indx)
                                    }
                                }

                                for indx in (0..<self.pixelArray.count).reversed()
                                {
                                    let pixel = self.pixelArray[indx]

                                    if (pixel.hue > deltaHigDiff)
                                    {
                                        self.pixelArray.remove(at: indx)
                                    }
                                }

                            } else if (deltaLow >= deltaHig) {
//print("deltaLow >= deltaHig");
                                for indx in (0..<self.pixelArray.count).reversed()
                                {
                                    let pixel = self.pixelArray[indx]

                                    if (pixel.hue < (dmin/ddeltaP))
                                    {
                                        self.pixelArray.remove(at: indx)
                                    }
                                }

                                if (deltaHig > (dmaxAmount*ddeltaP))
                                {
//print("deltaHig > dmaxAmount");
                                    self.pixelArray.removeLast()
                                }

                            } else {
//print("deltaHig > deltaLow");
                                var deltaDiff:CGFloat = 1.0

                                if (deltaHue > 0.0)
                                {
                                    deltaDiff = ddelta / deltaHue
//print("ddelta / deltaHue: ",ddelta / deltaHue);
                                }
                                deltaDiff *= 1.75


                        //        var deltaLowDiff = (hueNormal00 - (deltaLow*deltaDiff)) - 0.05;
                                let deltaHigDiff = (hueNormal00 + (deltaHig*deltaDiff)) + 0.05;

//print("deltaHig: ",(deltaHig*deltaDiff));
//print("deltaDiff,deltaHigDiff: ",deltaDiff,deltaHigDiff);
//print(" ");


                                for indx in (0..<self.pixelArray.count).reversed()
                                {
                                    let pixel = self.pixelArray[indx]

                                    if (pixel.hue > deltaHigDiff) //(dmax*ddeltaP))
                                    {
                                        self.pixelArray.remove(at: indx)
                                    }
                                }

                                if (deltaLow > (dminAmount*ddeltaP))
                                {
//print("deltaLow > dminAmount");
                                    self.pixelArray.removeFirst()
                                }
                            }
                        }
//                    }


//print("  ");
//print("  ");
//print("reset  self.pixelArray.count: ",self.pixelArray.count);
//for pixel in self.pixelArray    //************* keep for testing ***
//{
////print("pixel: ",pixel);
//}
//print("  ");
//print("  ");



                    //---------- find saturation standard deviation Final version ------------
                    sAvg = 0.0;
                    var numsAvg = 0;
                    let percDeviat:CGFloat = 1.00;
                    self.upperSDev = 1.0;
                    self.lowerSDev = 0.0;
                //        percDeviat = 1.0 + (CGFloat(sDeviation) / sA vg)

                    for pixel in self.pixelArray
                    {
                        if (pixel.s > 0.0)
                        {
                            sAvg += pixel.s;
                            numsAvg += 1;
                        }
                    }

                    if (numsAvg > 0)
                    {
                        sAvg = sAvg / CGFloat(numsAvg);
                        var sVarianceF:Float = 0.0;
//print("sAvg final: ",sAvg);


                        for pixel in self.pixelArray
                        {
                            if (pixel.s > 0.0)
                            {
                                let sv = (pixel.s - sAvg);
                                sVarianceF += Float(sv*sv);
                            }
                        }

                        sVarianceF = sVarianceF / Float(numsAvg);
                        let sDeviationFinal:Float = sqrtf(sVarianceF);

//print("sVari anceF,sDeviationFinal: ",sVarianceF,sDeviationFinal);
//print("  ");
//print("  ");

                        let upperSx:CGFloat = 1.20 //1.10 //1.20  1.40
                        let lowerSx:CGFloat = 0.80 //0.91 //0.833  0.78  0.714

                        let upperSDevFinal = (sAvg + CGFloat(sDeviationFinal));
                        let lowerSDevFinal = (sAvg - CGFloat(sDeviationFinal));
//print("upperSD evFinal: ",upperSDevFinal);
//print("lowerSD evFinal: ",lowerSDevFinal);
//print("  ");

                        self.upperSDev = upperSDevFinal * upperSx;
                        self.lowerSDev = min(lowerSDevFinal * lowerSx,0.1125);

//print("     lowe rSDev: ",lowerSDevFinal * lowerSx);
                    }


//print("  ");
//print("self.upperSDev: ",self.upperSDev);
//print("self.lowerSDev: ",self.lowerSDev);
//print("  ");
//print("  ");
//print("  ");
//print("  ");


                    //---------- find luminance standard deviation ------------
                    self.upperLDev = 1.0;
                    var lAvg:CGFloat = 0.0;

                    for pixel in self.pixelArray
                    {
                        lAvg += pixel.l;
                    }

                    if (self.pixelArray.count > 0)
                    {
                        lAvg = lAvg / CGFloat(self.pixelArray.count);
//print("lAvg: ",lAvg);

                        var lVariance:Float = 0.0;

                        for pixel in self.pixelArray
                        {
                            let lv = (pixel.l - lAvg);
                            lVariance += Float(lv*lv);
                        }


                        lVariance = lVariance / Float(self.pixelArray.count);
                        let lDeviation:Float = sqrtf(lVariance);

//print("lVariance,lDeviation: ",lVariance,lDeviation);
//print("  ");
//print("  ");

                        let upperLDev1 = (lAvg + CGFloat(lDeviation)) * 1.20;
                //        let upperLDev0 = (lAvg + CGFloat(lDeviation)) * 1.25;    //************* keep for testing ***
                        let lowerLDev  = (lAvg - CGFloat(lDeviation)) * 0.80;

//print("upperLDev,lowerLDev : ",(lAvg + CGFloat(lDeviation)),(lAvg - CGFloat(lDeviation)));
//print("upperLDev1,upperLDev0,lowerLDev: ",upperLDev1,upperLDev0,lowerLDev);
//print("  ");
//print("  ");
                        if (highCutLu > 0.989)
                        {
//print("> 0.989 highCutLu: ",highCutLu);
                            self.upperLDev = upperLDev1;

                        } else {
                            let lDevp = CGFloat(lDeviation) / lAvg;
//print("highCutLu: ",highCutLu);
//print("lDevp: ",lDevp);
                            let cutp = 1.00 + ((1.04 - highCutLu) * lDevp);  //1.0
//print("cutp: ",cutp);
                            self.upperLDev = cutp * highCutLu;
                        }

//print("self.upperLDev: ",self.upperLDev);
//print("  ");
//print("  ");


                        //---------- find saturation standard deviation for low end ------------
                        var sAvgHig:CGFloat = 0.0;
                        var higKount:CGFloat = 0.0;

                        var sAvgLow:CGFloat = 0.0;
                        var lowKount:CGFloat = 0.0;

                        for pixel in pixelArrayS0
                        {
                            if (lowerLDev < 0.18)
                            {
                                if (pixel.lmax > 10.0)
                                {
                                    sAvgHig  += (pixel.hue * pixel.lmax);
                                    higKount += pixel.lmax;
                                }
                            } else if (lowerLDev < 0.28) {
                                if (pixel.lmin > 10.0)
                                {
                                    sAvgHig  += (pixel.hue * pixel.lmin);
                                    higKount += pixel.lmin;
                                }
                            } else if (lowerLDev < 0.38) {
                                if (pixel.smax > 10.0)
                                {
                                    sAvgHig  += (pixel.hue * pixel.smax);
                                    higKount += pixel.smax;
                                }
                            }


                            if (upperLDev1 >= 0.72)
                            {
                                if (pixel.hl > 10.0)
                                {
                                    sAvgLow  += (pixel.hue * pixel.hl);
                                    lowKount += pixel.hl;
                                }
                            } else if (upperLDev1 >= 0.62) {
                                if (pixel.b > 10.0)
                                {
                                    sAvgLow  += (pixel.hue * pixel.b);
                                    lowKount += pixel.b;
                                }
                            } else if (upperLDev1 >= 0.52) {
                                if (pixel.g > 10.0)
                                {
                                    sAvgLow  += (pixel.hue * pixel.g);
                                    lowKount += pixel.g;
                                }
                            } else if (upperLDev1 >= 0.42) {
                                if (pixel.r > 10.0)
                                {
                                    sAvgLow  += (pixel.hue * pixel.r);
                                    lowKount += pixel.r;
                                }
                            }
                        }

                        if (lowKount > 0) && (higKount > 0)
                        {
                            sAvgHig = sAvgHig / higKount;
                            sAvgLow = sAvgLow / lowKount;
//print("  ");
//print("  ");
//print("sAvgHig,higKount: ",sAvgHig,higKount);
//print("sAvgLow,lowKount: ",sAvgLow,lowKount);

                            var sVarHig:CGFloat = 0.0;
                            var sVarHigKount:CGFloat = 0.0;

                            var sVarLow:CGFloat = 0.0;
                            var sVarlowKount:CGFloat = 0.0;

                            for pixel in pixelArrayS0
                            {
                                if (lowerLDev < 0.18)
                                {
                                    if (pixel.lmax > 10.0)
                                    {
                                        let sv = (pixel.hue - sAvgHig);
                                        sVarHig += CGFloat(sv*sv);
                                        sVarHigKount += 1.0
                                    }
                                } else if (lowerLDev < 0.28) {
                                    if (pixel.lmin > 10.0)
                                    {
                                        let sv = (pixel.hue - sAvgHig);
                                        sVarHig += CGFloat(sv*sv);
                                        sVarHigKount += 1.0
                                    }
                                } else if (lowerLDev < 0.38) {
                                    if (pixel.smax > 10.0)
                                    {
                                        let sv = (pixel.hue - sAvgHig);
                                        sVarHig += CGFloat(sv*sv);
                                        sVarHigKount += 1.0
                                    }
                                }


                                if (upperLDev1 >= 0.72)
                                {
                                    if (pixel.hl > 10.0)
                                    {
                                        let sv = (pixel.hue - sAvgLow);
                                        sVarLow += CGFloat(sv*sv);
                                        sVarlowKount += 1.0
                                    }
                                } else if (upperLDev1 >= 0.62) {
                                    if (pixel.b > 10.0)
                                    {
                                        let sv = (pixel.hue - sAvgLow);
                                        sVarLow += CGFloat(sv*sv);
                                        sVarlowKount += 1.0
                                    }
                                } else if (upperLDev1 >= 0.52) {
                                    if (pixel.g > 10.0)
                                    {
                                        let sv = (pixel.hue - sAvgLow);
                                        sVarLow += CGFloat(sv*sv);
                                        sVarlowKount += 1.0
                                    }
                                } else if (upperLDev1 >= 0.42) {
                                    if (pixel.r > 10.0)
                                    {
                                        let sv = (pixel.hue - sAvgLow);
                                        sVarLow += CGFloat(sv*sv);
                                        sVarlowKount += 1.0
                                    }
                                }
                            }

                            sVarHig = sVarHig / sVarHigKount;
                //            let sDevHig:CGFloat = sqrt(sVarHig);   //************* keep for testing ***

                            sVarLow = sVarLow / sVarlowKount;
                            let sDevLow:CGFloat = sqrt(sVarLow);


//print("  ");
//print("  ");
//print("sVarHig,sDevHig: ",sVarHig,sDevHig);
//print("sVarHigKount: ",sVarHigKount);
//print("  ");
//print("  ");
//print("sVarLow,sDevLow: ",sVarLow,sDevLow);
//print("sVarlowKount: ",sVarlowKount);
//print("  ");
//print("  ");

                    //        let upperSDevHig = (sAvgHig + sDevHig);    //************* keep for testing ***
                            let lowerSDevLow = (sAvgLow - (sDevLow * 0.88));
//print("upperSDevHig: ",upperSDevHig);
//print("s Avg Low - sDev Low, lowerSDevLow, : ",sAvgLow - sDevLow,lowerSDevLow);
//print("  ");

                            self.lowerSDev = lowerSDevLow;
                        }

//print("  ");
//print("  ");
//print("  ");
//print(" ------------------------------------ ");
//print("includ eGray   : ",includeGray);
//print("upperLDev1    : ",upperLDev1);
//print("self.upperLDev: ",self.upperLDev);
//print("  ");
//print("self.upperSDev: ",self.upperSDev);
//print("self.lowerSDev: ",self.lowerSDev);
//print(" ------------------------------------ ");
//print("  ");







                    let upperSDev1:CGFloat = self.upperSDev*1.10
                    let upperSDev3:CGFloat = self.upperSDev*1.30
//print("  ");
//print("upperSDev1   : ",upperSDev1);
//print("upperSDev3   : ",upperSDev3);
//print("  ");

                    let highSpot:Int  = Int(roundf(Float(upperSDev1 * 100.0)));
                    let highSpot3:Int = Int(roundf(Float(upperSDev3 * 100.0)));
                    self.lineFit(pixelArray:pixelArrayS0, highSpot:highSpot, highSpot3:highSpot3, percDeviat:percDeviat)

                    let lowSpot:Int = Int(roundf(Float(self.lowerSDev * 100.0)));
                    self.lineFitLow(pixelArray:pixelArrayS0, lowSpot:lowSpot, percDeviat:percDeviat)





//print("  ");
//print(" ------------------------------------ ");
//print("includeGray   : ",includeGray);
//print("self.upperLDev: ",self.upperLDev);
//print("  ");
//print("self.upperSDev: ",self.upperSDev);
//print("self.lowerSDev: ",self.lowerSDev);
//print(" ------------------------------------ ");
//print("  ");



                    }
            }

            pictureInput --> posterFilter --> pictureOut
            pictureInput.processImage(synchronously:true)
        }
    }

    //----------------------------------------------------------------------------------
    func touchesOnColor(_ touch:CGPoint)
    {
//print("touchesO nColor")
        var red1:UInt8 = 0;
        var green1:UInt8 = 0;
        var blue1:UInt8 = 0;

        var alpha:UInt8 = 255;
        var alphaPlace:Int  = 3;
        var point:CGPoint = CGPoint.zero;

        var cgAverageImage1:CGImage? = nil;

        var notOnImage = true;
        //var imageType:UInt32 = 1;

        autoreleasepool
        {
            var ciImage1:CIImage! = CIImage();

            var image0 = self.scrollView.imageView.image

            if (self.maskOfDoor) //******** change 1.1.0 add if block ***
            {
            } else if (self.maskOfFrontDoor) {
            } else if (currentWellNum == 0) {
                image0 = self.scrollView.paintImageOverlay0.image;
            } else if (currentWellNum == 1) {
                image0 = self.scrollView.paintImageOverlay1.image;
            } else if (currentWellNum == 2) {
                image0 = self.scrollView.paintImageOverlay2.image;
            } else if (currentWellNum == 3) {
                image0 = self.scrollView.paintImageOverlay3.image;
            } else if (currentWellNum == 4) {
                image0 = self.scrollView.paintImageOverlay4.image;
            } else {

            }


            if let image = image0
            {
                if let ciImage10 = CIImage(image:image, options:nil)
                {
                    ciImage1 = ciImage10;
                }
            }

            point = touch;
            let context1:CIContext = CIContext(options: nil);
            var localPoint1:CGPoint = point;

            if let theView1 = self.scrollView.imageView
            {
                localPoint1 = self.convertPoint(point:point, fromRect:theView1.frame, toRect:ciImage1.extent);
            }

//print("  ")
//print("  ")
//print("  ")
//print("currentWellNum:",currentWellNum)
//print("point:",point)
//print("localPoint1:",localPoint1)
//print("  ")
////print("  ")
////print("self.scrollVi ew.imag eView.image",self.scrollV iew.imag eView.image as Any)
////print("  ")
//print("  ")
//print("ciImage1",ciImage1 as Any)

            let areaVector1:CIVector = CIVector(x:localPoint1.x - 2.0, y:((ciImage1.extent.size.height - (localPoint1.y + 2.0)) - 1.0), z:5.0, w:5.0);
        //    let areaVector1:CIVector = CIVector(x:localPoint1.x - 0.0, y:((ciImage1.extent.size.height - (localPoint1.y + 0.0)) - 1.0), z:2.0, w:2.0);

//print("areaVector1",areaVector1)


            if let averageFilter1 = CIFilter(name:"CIAreaAverage")
            {
                averageFilter1.setValue(ciImage1, forKey:kCIInputImageKey)
                averageFilter1.setValue(areaVector1, forKey:"inputExtent")
                if let output = averageFilter1.outputImage
                {
                    if let cgimg = context1.createCGImage(output, from: CGRect(x:0, y:0, width:1, height:1))
                    {
                        cgAverageImage1 = cgimg;
                    }
                }
            }

//print(" ");
//print(" ");
//print("cgAverageImage1    : ",cgAverageImage1 as Any);

            var redPlace:Int   = 1;
            var goodAlpha = true;

            if let cgAverageImageX = cgAverageImage1
            {
                //imageType = cgAverageImageX.alphaInfo.rawValue;
                alphaPlace  = 3;
//print(" ");
//print(" ");
//print("cgAverageImageX.alphaInfo:",cgAverageImageX.alphaInfo);

                switch (cgAverageImageX.alphaInfo)
                {
                    case .none:                 redPlace = 0; alphaPlace = -1; print("cgAverageImageX.alphaInfo: none"); break;
                    case .premultipliedLast:    redPlace = 0; break;
                    case .premultipliedFirst:    redPlace = 1; alphaPlace =  0; break;
                    case .last:                    redPlace = 0; break;
                    case .first:                redPlace = 1; alphaPlace =  0; break;
                    case .noneSkipLast:            redPlace = 0; alphaPlace = -1; print("cgAverageImageX.alphaInfo: noneSkipLast"); break;
                    case .noneSkipFirst:        redPlace = 1; alphaPlace = -1; print("cgAverageImageX.alphaInfo: noneSkipFirst"); break;
                    case .alphaOnly:            goodAlpha = false; break;

                    default: redPlace = 1; break;
                }
//print("cgAverageImageX.alphaInfo, redPlace alphaPlace, goodAlpha:", cgAverageImageX.alphaInfo, redPlace,alphaPlace,goodAlpha); //imageType,

                if (goodAlpha)
                {
                    //---------------------------- average  ----------------------------------------------
                //    pixelDataAverage1 = CGDataProviderCopyData(CGImageGetDataProvider(cgAverageImageX));
                //    if let pixelDataAverage1 = CGDataProviderCopyData(cgAverageImageX.dataProvider);
                    if let pixelDataAverage1 = cgAverageImageX.dataProvider
                    {
                //        let dataAverage0 = CFDataGetBytePtr(pixelDataAverage1.data);
                        notOnImage = false;

                        if let dataAverage = CFDataGetBytePtr(pixelDataAverage1.data)
                        {
                            red1 = dataAverage[redPlace];
                            green1 = dataAverage[(redPlace + 1)];
                            blue1 = dataAverage[redPlace + 2];
                            alpha = 255;
                            if (alphaPlace >= 0) {alpha = dataAverage[alphaPlace];}
//print("red1, green1, blue1:",red1, green1, blue1);
                        }
                    }
                }
            }


            ciImage1 = nil;
        }

        cgAverageImage1 = nil;

//print("notOnImage, alphaPlace, alpha:",notOnImage, alphaPlace, alpha);

        if (notOnImage) {return;}

        if (alphaPlace < 0)
        {
            if ( red1 + green1 + blue1 == 0) {return;}
        } else {
            if (alpha == 0) {return;}
        }

        currSelectedPt = point;

        let redf0  :CGFloat = CGFloat(red1)/255.0;
        let greenf0:CGFloat = CGFloat(green1)/255.0;
        let bluef0 :CGFloat = CGFloat(blue1)/255.0;

//print("  ");
//print("redf0, greenf0, bluef0:",redf0, greenf0, bluef0);

        let tmax:CGFloat = max(max(redf0, greenf0), bluef0);
        let tmin:CGFloat = min(min(redf0, greenf0), bluef0);

        let delta:CGFloat = tmax - tmin;

        var sat:CGFloat = 0.0;
        var hue:CGFloat = 0.0;

        if (tmax > 0.0)
        {
            sat = delta / tmax;
        }

        if (delta > 0.0)
        {
            var divisor = 6.0

            if ((redf0 > greenf0) && (redf0 > bluef0))
            {
                hue = CGFloat(modf(Double(greenf0 - bluef0) / Double(delta), &divisor)) * 0.6;
//let angle0:CGFloat = (mpi2 * hue);
//print("red   dominate  hue,hue/3.6,delta,(greenf0 - bluef0),angle0: ",hue,hue/3.6,delta,(greenf0 - bluef0),angle0);
//print("  ");
            } else if ((greenf0 >= redf0) && (greenf0 > bluef0)) {
                hue = (((bluef0 - redf0) / delta) + 2.0) * 0.6;
//let angle0:CGFloat = (mpi2 * hue);
//print("green dominate  hue,hue/3.6,delta,(greenf0 - bluef0),angle0: ",hue,hue/3.6,delta,(greenf0 - bluef0),angle0);
//print("  ");
            } else {
                hue = (((redf0 - greenf0) / delta) + 4.0) * 0.6;
//let angle0:CGFloat = (mpi2 * hue);
//print("blue  dominate  hue,hue/3.6,delta,(greenf0 - bluef0),angle0: ",hue,hue/3.6,delta,(greenf0 - bluef0),angle0);
//print("  ");
            }

//            if (hue < 0.0)
//            {
//                hue = 3.6 + hue;
//            }
        }


        let hue11 = hue / 3.6;    //************* keep for testing ***
        let sat11 = sat;          //************* keep for testing ***
        let lum11 = tmax;         //************* keep for testing ***


  print(" ");
  print("red1, green1, blue1:",red1, green1, blue1);
  print(" ");
  print(" ");
  print("redf,greenf,bluef:      tmax,tmin,delta: ",redf0, greenf0,bluef0,     tmax,tmin,delta);
  print("h,s,l: ",hue11,sat11,lum11);
  print(" ");
  print(" ");
    }

    //----------------------------------------------------------------------------------
    func touchesOnSpecific(_ touch:CGPoint)
    {
//print("touchesO nSpecific")
        var red1:UInt8 = 0;
        var green1:UInt8 = 0;
        var blue1:UInt8 = 0;

        var redL:UInt8 = 0;
        var greenL:UInt8 = 0;
        var blueL:UInt8 = 0;

        var redR:UInt8 = 0;
        var greenR:UInt8 = 0;
        var blueR:UInt8 = 0;

        var redT:UInt8 = 0;
        var greenT:UInt8 = 0;
        var blueT:UInt8 = 0;

        var redB:UInt8 = 0;
        var greenB:UInt8 = 0;
        var blueB:UInt8 = 0;

        var redLi:UInt8 = 0;
        var greenLi:UInt8 = 0;
        var blueLi:UInt8 = 0;

        var redRi:UInt8 = 0;
        var greenRi:UInt8 = 0;
        var blueRi:UInt8 = 0;

        var redTi:UInt8 = 0;
        var greenTi:UInt8 = 0;
        var blueTi:UInt8 = 0;

        var redBi:UInt8 = 0;
        var greenBi:UInt8 = 0;
        var blueBi:UInt8 = 0;


        var alpha:UInt8 = 255;
        var alphaPlace:Int  = 3;
        var point:CGPoint = CGPoint.zero;

        var cgAverageImage1:CGImage? = nil;

        var cgAverageImageL:CGImage? = nil;
        var cgAverageImageR:CGImage? = nil;
        var cgAverageImageT:CGImage? = nil;
        var cgAverageImageB:CGImage? = nil;

        var cgAverageImageLi:CGImage? = nil;
        var cgAverageImageRi:CGImage? = nil;
        var cgAverageImageTi:CGImage? = nil;
        var cgAverageImageBi:CGImage? = nil;

        var notOnImage = true;
        //var imageType:UInt32 = 1;

        autoreleasepool
        {
            var ciImage1:CIImage! = CIImage();

            if let image = self.scrollView.imageView.image
            {
                if let ciImage10 = CIImage(image:image, options:nil)
                {
                    ciImage1 = ciImage10;
                }
            }

            point = touch;
            let context1:CIContext = CIContext(options: nil);
            var localPoint1:CGPoint = point;

            if let theView1 = self.scrollView.imageView
            {
                localPoint1 = self.convertPoint(point:point, fromRect:theView1.frame, toRect:ciImage1.extent);
            }

//print("  ")
//print("  ")
//print("  ")
//print("point",point)
//print("localPoint1",localPoint1)
//print("ciImage1",ciImage1 as Any)

            let areaVector1:CIVector = CIVector(x:localPoint1.x - 2, y:((ciImage1.extent.size.height - (localPoint1.y + 2)) - 1), z:5, w:5);


            let areaVectorL:CIVector = CIVector(x:localPoint1.x - 12, y:((ciImage1.extent.size.height - (localPoint1.y - 12)) - 1), z:5, w:5);
            let areaVectorR:CIVector = CIVector(x:localPoint1.x +  8, y:((ciImage1.extent.size.height - (localPoint1.y + 16)) - 1), z:5, w:5);
            let areaVectorT:CIVector = CIVector(x:localPoint1.x + 12, y:((ciImage1.extent.size.height - (localPoint1.y -  8)) - 1), z:5, w:5);
            let areaVectorB:CIVector = CIVector(x:localPoint1.x - 16, y:((ciImage1.extent.size.height - (localPoint1.y + 12)) - 1), z:5, w:5);

            let areaVectorLi:CIVector = CIVector(x:localPoint1.x -  9, y:((ciImage1.extent.size.height - (localPoint1.y -  3)) - 1), z:5, w:5);
            let areaVectorRi:CIVector = CIVector(x:localPoint1.x +  5, y:((ciImage1.extent.size.height - (localPoint1.y +  7)) - 1), z:5, w:5);
            let areaVectorTi:CIVector = CIVector(x:localPoint1.x +  3, y:((ciImage1.extent.size.height - (localPoint1.y -  5)) - 1), z:5, w:5);
            let areaVectorBi:CIVector = CIVector(x:localPoint1.x -  7, y:((ciImage1.extent.size.height - (localPoint1.y +  9)) - 1), z:5, w:5);


//print(" ");
//print(" ");
//print("point.x,y         : ",point.x,point.y);
//print("localPoint1.x,y   : ",localPoint1.x,localPoint1.y);
//print("ciImage1.extent.size.width    : ",ciImage1.extent.size.width);
//print("ciImage1.extent.size.height   : ",ciImage1.extent.size.height);
//print(" ");
//print("areaVector1.x,y   : ",areaVector1.x,areaVector1.y);
//print(" ");
//print("areaVectorL.x,y   : ",areaVectorL.x,areaVectorL.y);
//print("areaVectorR.x,y   : ",areaVectorR.x,areaVectorR.y);
//print("areaVectorT.x,y   : ",areaVectorT.x,areaVectorT.y);
//print("areaVectorB.x,y   : ",areaVectorB.x,areaVectorB.y);
//print(" ");
//print("areaVect orLi.x,y   : ",areaVect orLi.x,areaVect orLi.y);
//print("areaVectorRi.x,y   : ",areaVectorRi.x,areaVectorRi.y);
//print("areaVectorTi.x,y   : ",areaVectorTi.x,areaVectorTi.y);
//print("areaVectorBi.x,y   : ",areaVectorBi.x,areaVectorBi.y);
//print(" ");

            if let averageFilter1 = CIFilter(name:"CIAreaAverage")
            {
                averageFilter1.setValue(ciImage1, forKey:kCIInputImageKey)
                averageFilter1.setValue(areaVector1, forKey:"inputExtent")
                if let output = averageFilter1.outputImage
                {
                    if let cgimg = context1.createCGImage(output, from: CGRect(x:0, y:0, width:1, height:1))
                    {
                        cgAverageImage1 = cgimg;
                    }
                }

                averageFilter1.setValue(areaVectorL, forKey:"inputExtent")
                if let output = averageFilter1.outputImage
                {
                    if let cgimg = context1.createCGImage(output, from: CGRect(x:0, y:0, width:1, height:1))
                    {
                        cgAverageImageL = cgimg;
                    }
                }

                averageFilter1.setValue(areaVectorR, forKey:"inputExtent")
                if let output = averageFilter1.outputImage
                {
                    if let cgimg = context1.createCGImage(output, from: CGRect(x:0, y:0, width:1, height:1))
                    {
                        cgAverageImageR = cgimg;
                    }
                }

                averageFilter1.setValue(areaVectorT, forKey:"inputExtent")
                if let output = averageFilter1.outputImage
                {
                    if let cgimg = context1.createCGImage(output, from: CGRect(x:0, y:0, width:1, height:1))
                    {
                        cgAverageImageT = cgimg;
                    }
                }

                averageFilter1.setValue(areaVectorB, forKey:"inputExtent")
                if let output = averageFilter1.outputImage
                {
                    if let cgimg = context1.createCGImage(output, from: CGRect(x:0, y:0, width:1, height:1))
                    {
                        cgAverageImageB = cgimg;
                    }
                }

                averageFilter1.setValue(areaVectorLi, forKey:"inputExtent")
                if let output = averageFilter1.outputImage
                {
                    if let cgimg = context1.createCGImage(output, from: CGRect(x:0, y:0, width:1, height:1))
                    {
                        cgAverageImageLi = cgimg;
                    }
                }

                averageFilter1.setValue(areaVectorRi, forKey:"inputExtent")
                if let output = averageFilter1.outputImage
                {
                    if let cgimg = context1.createCGImage(output, from: CGRect(x:0, y:0, width:1, height:1))
                    {
                        cgAverageImageRi = cgimg;
                    }
                }

                averageFilter1.setValue(areaVectorTi, forKey:"inputExtent")
                if let output = averageFilter1.outputImage
                {
                    if let cgimg = context1.createCGImage(output, from: CGRect(x:0, y:0, width:1, height:1))
                    {
                        cgAverageImageTi = cgimg;
                    }
                }

                averageFilter1.setValue(areaVectorBi, forKey:"inputExtent")
                if let output = averageFilter1.outputImage
                {
                    if let cgimg = context1.createCGImage(output, from: CGRect(x:0, y:0, width:1, height:1))
                    {
                        cgAverageImageBi = cgimg;
                    }
                }
            }

//print(" ");
//print(" ");
//print("cgAverageImage1    : ",cgAverageImage1 as Any);
//print("cgAverageImageL    : ",cgAverageImageL as Any);
//print("cgAverageImageR    : ",cgAverageImageR as Any);
//print("cgAverageImageT    : ",cgAverageImageT as Any);
//print("cgAverageImageB    : ",cgAverageImageB as Any);
//print("cgAverageImageLi   : ",cgAverageImageLi as Any);
//print("cgAverageImageRi   : ",cgAverageImageRi as Any);
//print("cgAverageImageTi   : ",cgAverageImageTi as Any);
//print("cgAverageImageBi   : ",cgAverageImageBi as Any);

    //        context1 = nil;

            var redPlace:Int   = 1;
            var goodAlpha = true;

            if let cgAverageImageX = cgAverageImage1
            {
                //imageType = cgAverageImageX.alphaInfo.rawValue;
                alphaPlace  = 3;
//print(" ");
//print(" ");
//print("cgAverageImageX.alphaInfo:",cgAverageImageX.alphaInfo);

                switch (cgAverageImageX.alphaInfo)
                {
                    case .none:                 redPlace = 0; alphaPlace = -1; break;
                    case .premultipliedLast:    redPlace = 0; break;
                    case .premultipliedFirst:    redPlace = 1; alphaPlace =  0; break;
                    case .last:                    redPlace = 0; break;
                    case .first:                redPlace = 1; alphaPlace =  0; break;
                    case .noneSkipLast:            redPlace = 0; alphaPlace = -1; break;
                    case .noneSkipFirst:        redPlace = 1; alphaPlace = -1; break;
                    case .alphaOnly:            goodAlpha = false; break;

                    default: redPlace = 1; break;
                }
//print("cgAverageImageX.alphaInfo, redPlace alphaPlace, goodAlpha:", cgAverageImageX.alphaInfo, redPlace,alphaPlace,goodAlpha); //imageType,

                if (goodAlpha)
                {
                    //---------------------------- average  ----------------------------------------------
                //    pixelDataAverage1 = CGDataProviderCopyData(CGImageGetDataProvider(cgAverageImageX));
                //    if let pixelDataAverage1 = CGDataProviderCopyData(cgAverageImageX.dataProvider);
                    if let pixelDataAverage1 = cgAverageImageX.dataProvider
                    {
                //        let dataAverage0 = CFDataGetBytePtr(pixelDataAverage1.data);
                        notOnImage = false;

                        if let dataAverage = CFDataGetBytePtr(pixelDataAverage1.data)
                        {
                            red1 = dataAverage[redPlace];
                            green1 = dataAverage[(redPlace + 1)];
                            blue1 = dataAverage[redPlace + 2];
                            alpha = 255;
                            if (alphaPlace >= 0) {alpha = dataAverage[alphaPlace];}
//print("red1, green1, blue1:",red1, green1, blue1);
                        }
                    }
                }
            }

            if (goodAlpha)
            {
                if let cgAverageImageX = cgAverageImageL
                {
                    if let pixelDataAverage1 = cgAverageImageX.dataProvider
                    {
                        //---------------------------- average  ----------------------------------------------
                        if let dataAverage = CFDataGetBytePtr(pixelDataAverage1.data)
                        {
                            redL = dataAverage[redPlace];
                            greenL = dataAverage[(redPlace + 1)];
                            blueL = dataAverage[redPlace + 2];
//print("redL, greenL, blueL:",redL, greenL, blueL);
                        }
                    }
                }

                if let cgAverageImageX = cgAverageImageR
                {
                    if let pixelDataAverage1 = cgAverageImageX.dataProvider
                    {
                        //---------------------------- average  ----------------------------------------------
                        if let dataAverage = CFDataGetBytePtr(pixelDataAverage1.data)
                        {
                            redR = dataAverage[redPlace];
                            greenR = dataAverage[(redPlace + 1)];
                            blueR = dataAverage[redPlace + 2];
//print("redR, greenR, blueR:",redR, greenR, blueR);
                        }
                    }
                }

                if let cgAverageImageX = cgAverageImageT
                {
                    if let pixelDataAverage1 = cgAverageImageX.dataProvider
                    {
                        //---------------------------- average  ----------------------------------------------
                        if let dataAverage = CFDataGetBytePtr(pixelDataAverage1.data)
                        {
                            redT = dataAverage[redPlace];
                            greenT = dataAverage[(redPlace + 1)];
                            blueT = dataAverage[redPlace + 2];
                        }
                    }
                }

                if let cgAverageImageX = cgAverageImageB
                {
                    if let pixelDataAverage1 = cgAverageImageX.dataProvider
                    {
                        //---------------------------- average  ----------------------------------------------
                        if let dataAverage = CFDataGetBytePtr(pixelDataAverage1.data)
                        {
                            redB = dataAverage[redPlace];
                            greenB = dataAverage[(redPlace + 1)];
                            blueB = dataAverage[redPlace + 2];
                        }
                    }
                }





                if let cgAverageImageX = cgAverageImageLi
                {
                    if let pixelDataAverage1 = cgAverageImageX.dataProvider
                    {
                        //---------------------------- average  ----------------------------------------------
                        if let dataAverage = CFDataGetBytePtr(pixelDataAverage1.data)
                        {
                            redLi = dataAverage[redPlace];
                            greenLi = dataAverage[(redPlace + 1)];
                            blueLi = dataAverage[redPlace + 2];
                        }
                    }
                }

                if let cgAverageImageX = cgAverageImageRi
                {
                    if let pixelDataAverage1 = cgAverageImageX.dataProvider
                    {
                        //---------------------------- average  ----------------------------------------------
                        if let dataAverage = CFDataGetBytePtr(pixelDataAverage1.data)
                        {
                            redRi = dataAverage[redPlace];
                            greenRi = dataAverage[(redPlace + 1)];
                            blueRi = dataAverage[redPlace + 2];
                        }
                    }
                }

                if let cgAverageImageX = cgAverageImageTi
                {
                    if let pixelDataAverage1 = cgAverageImageX.dataProvider
                    {
                        //---------------------------- average  ----------------------------------------------
                        if let dataAverage = CFDataGetBytePtr(pixelDataAverage1.data)
                        {
                            redTi = dataAverage[redPlace];
                            greenTi = dataAverage[(redPlace + 1)];
                            blueTi = dataAverage[redPlace + 2];
                        }
                    }
                }

                if let cgAverageImageX = cgAverageImageBi
                {
                    if let pixelDataAverage1 = cgAverageImageX.dataProvider
                    {
                        //---------------------------- average  ----------------------------------------------
                        if let dataAverage = CFDataGetBytePtr(pixelDataAverage1.data)
                        {
                            redBi = dataAverage[redPlace];
                            greenBi = dataAverage[(redPlace + 1)];
                            blueBi = dataAverage[redPlace + 2];
                        }
                    }
                }
            }

            ciImage1 = nil;
//            averageFilter1 = nil;
        }

//        if (pixelDataAverage1) {CFRelease(pixelDataAverage1);}
//        if (cgAverageImage1)   {CGImageRelease(cgAverageImage1);}
//
//        if (pixelDataAverageL) {CFRelease(pixelDataAverageL);}
//        if (cgAverageImageL)   {CGImageRelease(cgAverageImageL);}
//
//        if (pixelDataAverageR) {CFRelease(pixelDataAverageR);}
//        if (cgAverageImageR)   {CGImageRelease(cgAverageImageR);}
//
//        if (pixelDataAverageT) {CFRelease(pixelDataAverageT);}
//        if (cgAverageImageT)   {CGImageRelease(cgAverageImageT);}
//
//        if (pixelDataAverageB) {CFRelease(pixelDataAverageB);}
//        if (cgAverageImageB)   {CGImageRelease(cgAverageImageB);}
//
//
//        if (pixelDataAverageLi) {CFRelease(pixelDataAverageLi);}
//        if (cgAverageImageLi)   {CGImageRelease(cgAverageImageLi);}
//
//        if (pixelDataAverageRi) {CFRelease(pixelDataAverageRi);}
//        if (cgAverageImageRi)   {CGImageRelease(cgAverageImageRi);}
//
//        if (pixelDataAverageTi) {CFRelease(pixelDataAverageTi);}
//        if (cgAverageImageTi)   {CGImageRelease(cgAverageImageTi);}
//
//        if (pixelDataAverageBi) {CFRelease(pixelDataAverageBi);}
//        if (cgAverageImageBi)   {CGImageRelease(cgAverageImageBi);}

        cgAverageImage1 = nil;

        cgAverageImageL = nil;
        cgAverageImageR = nil;
        cgAverageImageT = nil;
        cgAverageImageB = nil;

        cgAverageImageLi = nil;
        cgAverageImageRi = nil;
        cgAverageImageTi = nil;
        cgAverageImageBi = nil;

//print("red1, green1, blue1:",red1, green1, blue1);
//print("notOnImage, alpha, alphaPlace:",notOnImage, alpha, alphaPlace);

        if (notOnImage) {return;}

        if (alphaPlace < 0)
        {
            if ( red1 + green1 + blue1 == 0) {return;}
        } else {
            if (alpha == 0) {return;}
        }

//        [self.layerController addNewMaskLayer:theView1.layerImageView.image maskImage:nil
//                                                          imageRect:theView1.pecLayer.imageRect transform:theView1.pecLayer.transform isMask:false
//                                                          isVisible:false isPhoto:true blendMode:kCGBlendModeNormal layer:lastLayer];
//
//        self.project.currentLayer = lastLayer+1;
        currSelectedPt = point;

        var redf0  :CGFloat = CGFloat(red1)/255.0;
        var greenf0:CGFloat = CGFloat(green1)/255.0;
        var bluef0 :CGFloat = CGFloat(blue1)/255.0;
        let alphaf:CGFloat = CGFloat(alpha)/255.0;

        let redf1  :CGFloat = CGFloat(red1)/255.0;
        let greenf1:CGFloat = CGFloat(green1)/255.0;
        let bluef1 :CGFloat = CGFloat(blue1)/255.0;


        let redfL  :CGFloat = CGFloat(redL)/255.0;
        let greenfL:CGFloat = CGFloat(greenL)/255.0;
        let bluefL :CGFloat = CGFloat(blueL)/255.0;

        let redfR  :CGFloat = CGFloat(redR)/255.0;
        let greenfR:CGFloat = CGFloat(greenR)/255.0;
        let bluefR :CGFloat = CGFloat(blueR)/255.0;

        let redfT  :CGFloat = CGFloat(redT)/255.0;
        let greenfT:CGFloat = CGFloat(greenT)/255.0;
        let bluefT :CGFloat = CGFloat(blueT)/255.0;

        let redfB  :CGFloat = CGFloat(redB)/255.0;
        let greenfB:CGFloat = CGFloat(greenB)/255.0;
        let bluefB :CGFloat = CGFloat(blueB)/255.0;

        let redfLi  :CGFloat = CGFloat(redLi)/255.0;
        let greenfLi:CGFloat = CGFloat(greenLi)/255.0;
        let bluefLi :CGFloat = CGFloat(blueLi)/255.0;

        let redfRi  :CGFloat = CGFloat(redRi)/255.0;
        let greenfRi:CGFloat = CGFloat(greenRi)/255.0;
        let bluefRi :CGFloat = CGFloat(blueRi)/255.0;

        let redfTi  :CGFloat = CGFloat(redTi)/255.0;
        let greenfTi:CGFloat = CGFloat(greenTi)/255.0;
        let bluefTi :CGFloat = CGFloat(blueTi)/255.0;

        let redfBi  :CGFloat = CGFloat(redBi)/255.0;
        let greenfBi:CGFloat = CGFloat(greenBi)/255.0;
        let bluefBi :CGFloat = CGFloat(blueBi)/255.0;


        let   tmax1 = max(max(redf1, greenf1), bluef1);

        let   tmaxL = max(max(redfL, greenfL), bluefL);
        let   tmaxR = max(max(redfR, greenfR), bluefR);
        let   tmaxT = max(max(redfT, greenfT), bluefT);
        let   tmaxB = max(max(redfB, greenfB), bluefB);

        let   tmaxLi = max(max(redfLi, greenfLi), bluefLi);
        let   tmaxRi = max(max(redfRi, greenfRi), bluefRi);
        let   tmaxTi = max(max(redfTi, greenfTi), bluefTi);
        let   tmaxBi = max(max(redfBi, greenfBi), bluefBi);

        let   tmin1 = min(min(redf1, greenf1), bluef1);

        let   tminL = min(min(redfL, greenfL), bluefL);
        let   tminR = min(min(redfR, greenfR), bluefR);
        let   tminT = min(min(redfT, greenfT), bluefT);
        let   tminB = min(min(redfB, greenfB), bluefB);

        let   tminLi = min(min(redfLi, greenfLi), bluefLi);
        let   tminRi = min(min(redfRi, greenfRi), bluefRi);
        let   tminTi = min(min(redfTi, greenfTi), bluefTi);
        let   tminBi = min(min(redfBi, greenfBi), bluefBi);


//print(" ");
//print("redf ,greenf, bluef :tmax1,tmin1: ",redf1,greenf1,bluef1,tmax1,tmin1);

//print("redfL,greenfL,bluefL:tmaxL,tminL: ",redfL,greenfL,bluefL,tmaxL,tminL);
//print("redfR,greenfR,bluefR:tmaxR,tminR: ",redfR,greenfR,bluefR,tmaxR,tminR);
//print("redfT,greenfT,bluefT:tmaxT,tminT: ",redfT,greenfT,bluefT,tmaxT,tminT);
//print("redfB,greenfB,bluefB:tmaxB,tminB: ",redfB,greenfB,bluefB,tmaxB,tminB);
//print(" ");
//print("redfLi,greenfLi,bluefLi:tmaxL,tminL: ",redfLi,greenfLi,bluefLi,tmaxLi,tminLi);
//print("redfRi,greenfRi,bluefRi:tmaxR,tminR: ",redfRi,greenfRi,bluefRi,tmaxRi,tminRi);
//print("redfTi,greenfTi,bluefTi:tmaxT,tminT: ",redfTi,greenfTi,bluefTi,tmaxTi,tminTi);
//print("redfBi,greenfBi,bluefBi:tmaxB,tminB: ",redfBi,greenfBi,bluefBi,tmaxBi,tminBi);
//print(" ");

        let colorf1:CIVector = CIVector(x:tmin1, y:tmax1-tmin1, z:tmax1+tmin1, w:tmax1);
        let colorfL:CIVector = CIVector(x:tminL, y:tmaxL-tminL, z:tmaxL+tminL, w:tmaxL);
        let colorfR:CIVector = CIVector(x:tminR, y:tmaxR-tminR, z:tmaxR+tminR, w:tmaxR);
        let colorfT:CIVector = CIVector(x:tminT, y:tmaxT-tminT, z:tmaxT+tminT, w:tmaxT);
        let colorfB:CIVector = CIVector(x:tminB, y:tmaxB-tminB, z:tmaxB+tminB, w:tmaxB);

        let colorfLi:CIVector = CIVector(x:tminLi, y:tmaxLi-tminLi, z:tmaxLi+tminLi, w:tmaxLi);
        let colorfRi:CIVector = CIVector(x:tminRi, y:tmaxRi-tminRi, z:tmaxRi+tminRi, w:tmaxRi);
        let colorfTi:CIVector = CIVector(x:tminTi, y:tmaxTi-tminTi, z:tmaxTi+tminTi, w:tmaxTi);
        let colorfBi:CIVector = CIVector(x:tminBi, y:tmaxBi-tminBi, z:tmaxBi+tminBi, w:tmaxBi);


//print(" ");
//print(" ");
//print("colorf1: ",colorf1);

//print("colorfL: ",colorfL);
//print("colorfR: ",colorfR);
//print("colorfT: ",colorfT);
//print("colorfB: ",colorfB);

//print("colorfLi: ",colorfLi);
//print("colorfRi: ",colorfRi);
//print("colorfTi: ",colorfTi);
//print("colorfBi: ",colorfBi);


        let tmaxList:[CIVector] = [colorf1, colorfL, colorfR,  colorfT,  colorfB,
                                   colorfLi, colorfRi, colorfTi, colorfBi];

        var sortLum = tmaxList.sorted(by: { $0.z > $1.z })

//print("  ");
//print("tma xList: ",tma xList);
//print("sortLum: ",sortLum);
//print("  ");

        sortLum.removeLast();
        sortLum.removeLast();
        sortLum.removeFirst();
    //    sortLum.remove(at:0);

//print("sortLum: ",sortLum);
//print("  ");
//print("  ");

        var sortLum2 = sortLum.sorted(by:
        {
            var satA:CGFloat = 0.0;
            var satB:CGFloat = 0.0;

            if ($0.w > 0.0) {satA = $0.y / $0.w;}
            if ($1.w > 0.0) {satB = $1.y / $1.w;}
//print("satA,satB: ",satA,satB);

             return satA < satB
        })

//print("sortLum2: ",sortLum2);
//print("  ");
//print("  ");

        sortLum2.removeLast();
        sortLum2.removeLast();
        sortLum2.removeFirst();

//print("sortLum2: ",sortLum2);
//print("  ");
//print("  ");

        let sortLum3 = sortLum2.sorted(by: { $0.z > $1.z })
        let colorf0 = sortLum3[0];

//print("sortLum3: ",sortLum3);
//print("colorf0: ",colorf0);

        if (colorf0 == colorfL)
        {
            redf0   = redfL;
            greenf0 = greenfL;
            bluef0  = bluefL;
        } else if (colorf0 == colorfR) {
            redf0   = redfR;
            greenf0 = greenfR;
            bluef0  = bluefR;
        } else if (colorf0 == colorfT) {
            redf0   = redfT;
            greenf0 = greenfT;
            bluef0  = bluefT;
        } else if (colorf0 == colorfB) {
            redf0   = redfB;
            greenf0 = greenfB;
            bluef0  = bluefB;
        } else if (colorf0 == colorfLi) {
            redf0   = redfLi;
            greenf0 = greenfLi;
            bluef0  = bluefLi;
        } else if (colorf0 == colorfRi) {
            redf0   = redfRi;
            greenf0 = greenfRi;
            bluef0  = bluefRi;
        } else if (colorf0 == colorfTi) {
            redf0   = redfTi;
            greenf0 = greenfTi;
            bluef0  = bluefTi;
        } else if (colorf0 == colorfBi) {
            redf0   = redfBi;
            greenf0 = greenfBi;
            bluef0  = bluefBi;
        }

//print(" ");
//print(" ");
//print("redf0,greenf0,bluef0: ",redf0,greenf0,bluef0);
//print(" ");

//redf0 = 0.2
//greenf0 = 0.2
//bluef0 = 0.2


        let tmax:CGFloat = max(max(redf0, greenf0), bluef0);
        let tmin:CGFloat = min(min(redf0, greenf0), bluef0);
        var tmid:CGFloat = greenf0;

//        var redislead:Bool = false;
        var grnislead:Bool = false;
        var bluislead:Bool = true;

//        var redistail:Bool = true;
        var grnistail:Bool = false;
        var bluistail:Bool = false;

        let delta:CGFloat = tmax - tmin;

        var sat:CGFloat = 0.0;
        var hue:CGFloat = 0.0;

        if (tmax > 0.0)
        {
            sat = delta / tmax;
        }

        if (delta > 0.0)
        {
            var divisor = 6.0

            if ((redf0 > greenf0) && (redf0 > bluef0))
            {
                hue = CGFloat(modf(Double(greenf0 - bluef0) / Double(delta), &divisor)) * 0.6;
//                redislead = true;
                bluislead = false;
//print("  ");
//let angle0:CGFloat = (mpi2 * hue);
//print("red   dominate  hue,hue/3.6,delta,(greenf0 - bluef0),angle0: ",hue,hue/3.6,delta,(greenf0 - bluef0),angle0);
//print("  ");
            } else if ((greenf0 >= redf0) && (greenf0 > bluef0)) {
                hue = (((bluef0 - redf0) / delta) + 2.0) * 0.6;
                grnislead = true;
                bluislead = false;
                tmid = bluef0;

//print("  ");
//let angle0:CGFloat = (mpi2 * hue);
//print("green dominate  hue,hue/3.6,delta,(greenf0 - bluef0),angle0: ",hue,hue/3.6,delta,(greenf0 - bluef0),angle0);
//print("  ");
            } else {
                hue = (((redf0 - greenf0) / delta) + 4.0) * 0.6;
//print("  ");
//let angle0:CGFloat = (mpi2 * hue);
//print("blue  dominate  hue,hue/3.6,delta,(greenf0 - bluef0),angle0: ",hue,hue/3.6,delta,(greenf0 - bluef0),angle0);
//print("  ");
            }

            if ((greenf0 < redf0) && (greenf0 <= bluef0))
            {
//                redistail = false;
                grnistail = true;
                tmid = bluef0;
            } else if ((bluef0 < greenf0) && (bluef0 < redf0)) {
//                redistail = false;
                bluistail = true;
            }

//print(" ");
//print("redislead,grnislead,bluislead : ",redislead,grnislead,bluislead);
//print("redistail,grnistail,bluistail : ",redistail,grnistail,bluistail);

            if (grnislead || grnistail)
            {
                if (bluislead || bluistail)
                {
                    tmid = redf0;
                }
            }




//            if (hue < 0.0)
//            {
//                hue = 3.6 + hue;
//            }
        }

        hueNormal00 = hue / 3.6;
        sat00 = sat;
        lum00 = tmax;
        delta00 = delta;

//print(" ");
//print(" ");
//print("redf,greenf,bluef:tmax,tmin,delta:",redf0, greenf0,bluef0,tmax,tmin,delta);
//print("h,s,l:",hueNormal00,sat00,lum00);
//print(" ");
//print("delta: ",delta);
//print("tmid : ",tmid);

        includeGray = false;
        outRightGray = 0.0;
        outRightWhite = false;

        if (delta00 < 0.0625) // && (sat00 < 0.0625)  //0.0625  0.075
        {
            includeGray = true;
//            var kwhite:Int = 0
//            if (redf0 > 0.745) {kwhite += 1}
//            if (greenf0 > 0.745) {kwhite += 1}
//            if (bluef0 > 0.745) {kwhite += 1}
//
//            if (kwhite > 1) {outRightWhite = true;}
        }

        if (delta00 < 0.0625) && (sat00 < 0.0625)
        {
            var kwhite:Int = 0
            if (redf0 > 0.745) {kwhite += 1}
            if (greenf0 > 0.745) {kwhite += 1}
            if (bluef0 > 0.745) {kwhite += 1}

            if (kwhite > 1) {outRightWhite = true;}
        }

        let ratioTail = tmid / tmin
        let ratioLead = tmax / tmid
//print(" ");
//print("ratioTail,ratioLead : ",ratioTail,ratioLead);

        if (ratioTail < 1.035) && (ratioLead < 1.035)
        {
            outRightGray = 1.0;
        }

//print(" ");
//print("includ eGray: ",includeGray);
//print("outRightGray: ",outRightGray);
//print("outRightWhite: ",outRightWhite);
//print(" ");


        defaultLineColor = UIColor(red:redf0, green:greenf0, blue:bluef0, alpha:alphaf);
        origStartColor   = UIColor(red:redf0, green:greenf0, blue:bluef0, alpha:alphaf);

//print(" origStartColor,defaultLi neColor: ",origStartColor,defaultLineColor)
//print(" ");
//print(" ");

        self.scrollView.touchPadView.setHueSatLumFromRed()
    }

    //----------------------------------------------------------------------------------
    func touchesDealWithSpecificFill()
    {
//print(" ")
//print(" ")
//print("touchesDealWi thSpecificFill")
        guard let image = self.scrollView.imageView.image else {return;}

        forcedImageSize = Size(width: Float(image.size.width), height:Float(image.size.height));
        var countoursGroup:OperationGroup!

        if (includeGray)
        {
            let lowHue:CGFloat  = 0.0;
            let highHue:CGFloat = 1.0;

            countoursGroup = PECgrayGroup( lowHue: lowHue, highHue: highHue,
                                            color: defaultLineColor, hue: hueNormal00,
                                            white: outRightWhite, outRightGray: outRightGray)
        } else {
            countoursGroup = PECcontoursGroup(color: defaultLineColor, hue: hueNormal00, white:outRightWhite)
        }

//print("  ");
//print("self.pixelArray.count: ",self.pixelArray.count);

        if (self.pixelArray.count > 7)
        {
            let pixelLow  = self.pixelArray.first
            let pixelHigh = self.pixelArray.last

            let lowHue = pixelLow?.hue ?? 0.0;
            let highHue = pixelHigh?.hue ?? 1.0;

//print("  ");
//print("lowHue,highHue: ",lowHue,highHue);
//print("  ");
//        sat00 = sat;
//        lum00 = tmax;

            if (lum00 > 0.78) && (sat00 > 0.185)
            {
                countoursGroup = PEChigLumGroup(lowHue: lowHue, highHue: highHue,
                                                 color: defaultLineColor,
                                                   hue: hueNormal00,
                                                 white: outRightWhite)
            } else {
                countoursGroup = PECarrayTouchGroup(includeGray:includeGray,
                                                       lowHue: lowHue, highHue: highHue,
                                                    upperLDev:self.upperLDev,
                                                    upperSDev:self.upperSDev,
                                                    lowerSDev:self.lowerSDev,
                                                       slopef:self.slopef, interceptYaxis:self.interceptYaxis,
                                                      slopef3:self.slopef3, interceptYaxis3:self.interceptYaxis3,
                                                    slopefLow:self.slopefLow, interceptYaxisLow:self.interceptYaxisLow)
            }

            let pictureInput = PictureInput(image:image)
            let pictureOutput = PictureOutput()

            pictureOutput.imageAvailableCallback =
            { imageOut in
                maskedHouseImage = imageOut;

                if (currentWellNum == 0)
                {
                    maskedHouseImage0 = maskedHouseImage;
                    origStartColor0 = origStartColor;
                    defaultLineColor0 = defaultLineColor;
                } else if (currentWellNum == 1) {
                    maskedHouseImage1 = maskedHouseImage;
                    origStartColor1 = origStartColor;
                    defaultLineColor1 = defaultLineColor;
                } else if (currentWellNum == 2) {
                    maskedHouseImage2 = maskedHouseImage;
                    origStartColor2 = origStartColor;
                    defaultLineColor2 = defaultLineColor;
                } else if (currentWellNum == 3) {
                    maskedHouseImage3 = maskedHouseImage;
                    origStartColor3 = origStartColor;
                    defaultLineColor3 = defaultLineColor;
                } else if (currentWellNum == 4) {
                    maskedHouseImage4 = maskedHouseImage;
                    origStartColor4 = origStartColor;
                    defaultLineColor4 = defaultLineColor;
                }

                self.createOverlay()
            }

            pictureInput --> countoursGroup --> pictureOutput
            pictureInput.processImage(synchronously:true)

            return;
        }

        let pictureInput = PictureInput(image:image)
        let pictureOutput = PictureOutput()

        pictureOutput.imageAvailableCallback =
        { imageOut in
            maskedHouseImage = imageOut;

            if (currentWellNum == 0)
            {
                maskedHouseImage0 = maskedHouseImage;
                origStartColor0 = origStartColor;
                defaultLineColor0 = defaultLineColor;
            } else if (currentWellNum == 1) {
                maskedHouseImage1 = maskedHouseImage;
                origStartColor1 = origStartColor;
                defaultLineColor1 = defaultLineColor;
            } else if (currentWellNum == 2) {
                maskedHouseImage2 = maskedHouseImage;
                origStartColor2 = origStartColor;
                defaultLineColor2 = defaultLineColor;
            } else if (currentWellNum == 3) {
                maskedHouseImage3 = maskedHouseImage;
                origStartColor3 = origStartColor;
                defaultLineColor3 = defaultLineColor;
            } else if (currentWellNum == 4) {
                maskedHouseImage4 = maskedHouseImage;
                origStartColor4 = origStartColor;
                defaultLineColor4 = defaultLineColor;
            }
//print("3touchesDealWi thSpecificFill maskedHouseImage: ",maskedHouseImage as Any);
//print("3touchesDealWi thSpecificFill origStartColor: ",origStartColor as Any);

            self.createOverlay()
        }

        pictureInput --> countoursGroup --> pictureOutput
        pictureInput.processImage(synchronously:true)
    }

    //----------------------------------------------------------------------------------
    func touchesDealWithClearPush(_ touch:CGPoint)
    {
//print("00 touchesDealWi thClearPush")
        self.pixelArray.removeAll()
        self.touchesOnSpecific(touch);

        self.readyView.isHidden = true;

        if (currentWellNum == 0)
        {
            origStartColor0 = origStartColor;
            defaultLineColor0 = defaultLineColor;
        } else if (currentWellNum == 1) {
            origStartColor1 = origStartColor;
            defaultLineColor1 = defaultLineColor;
        } else if (currentWellNum == 2) {
            origStartColor2 = origStartColor;
            defaultLineColor2 = defaultLineColor;
        } else if (currentWellNum == 3) {
            origStartColor3 = origStartColor;
            defaultLineColor3 = defaultLineColor;
        } else if (currentWellNum == 4) {
            origStartColor4 = origStartColor;
            defaultLineColor4 = defaultLineColor;
        }

    //    self.createOverlay()

//print("00 touchesDealWi thClearPush self.penTag:",self.penTag)
        dragSquare = false;

        self.paintPath.removeAllPoints();
        self.paintLayer.backgroundColor = UIColor.clear.cgColor;
        self.paintLayer.fillColor = UIColor.clear.cgColor;
        self.paintLayer.path = nil;
        self.paintObject = nil;
        self.theButtonV0 = nil;

        self.scrollView.curvePts.removeAll()

        self.d4Button.isHidden = true
        self.toolSizeSlider.isHidden = true

        self.scrollView.d1Button.isHidden = true;
        self.scrollView.d2Button.isHidden = true;
        self.scrollView.deleteDimButton.isHidden =  true;
        self.scrollView.editDimButton.isHidden =  true;

        for subView in self.scrollView.imageViewOverlay.subviews
        {
            if let button:UIButton = subView as? UIButton
            {
                button.removeFromSuperview();
            }
        }

        self.penTag = 9

        self.scrollView.garageImageView.isHidden = false;
        self.scrollView.garageMaskView.isHidden = false;   //************** change 1.1.0 *** put back in
        self.scrollView.frontDrMaskView.isHidden = false;  //************** change 1.1.0 *** put back in

        if (self.colorWellButton0.isSelected)
        {
            self.scrollView.paintImageOverlay0.isHidden = false;
        }
        if (self.colorWellButton1.isSelected)
        {
            self.scrollView.paintImageOverlay1.isHidden = false;
        }
        if (self.colorWellButton2.isSelected)
        {
            self.scrollView.paintImageOverlay2.isHidden = false;
        }
        if (self.colorWellButton3.isSelected)
        {
            self.scrollView.paintImageOverlay3.isHidden = false;
        }
        if (self.colorWellButton4.isSelected)
        {
            self.scrollView.paintImageOverlay4.isHidden = false;
        }


        self.checkPaintButton.isHidden = false;
        dragSquare = false;
        self.scrollView.wasStart = true;
//print("00 touchesDealWi thClearPush self.scrollView.wasStart:",self.scrollView.wasStart)
        self.scrollView.editDimButton.tag = 0;
        self.editPaintButton.isSelected = true;
        self.editPaintButton.isHidden = false;

        self.paintLayer.backgroundColor = UIColor.clear.cgColor;
        self.paintLayer.fillColor = UIColor.clear.cgColor;
        self.paintLayer.strokeColor = UIColor(displayP3Red: 0.0, green: 0.5, blue: 0.5, alpha: 1.0).cgColor;
        self.paintLayer.lineJoin = CAShapeLayerLineJoin.round
        self.paintLayer.lineCap = CAShapeLayerLineCap.round

        hostRectPEC = self.scrollView.imageView.frame

        if (hostRectPEC.size.width >= hostRectPEC.size.height)
        {
            theImageScale = min(hostRectPEC.size.width / 4096.0, 1.0);
        } else {
            theImageScale = min(hostRectPEC.size.height / 4096.0, 1.0);
        }

        let lineWidth = 10.0 * min(theImageScale*1.25,1.0);
        let lineScale = min(theImageScale*2.0,1.0);

        if (self.scrollView.reverseZoomFactor > 5.5)
        {
            self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,14.0*lineScale);
        } else if (self.scrollView.reverseZoomFactor > 3.75) {
            self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,10.0*lineScale);
        } else if (self.scrollView.reverseZoomFactor > 2.5) {
            self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,8.0*lineScale);
        } else if (self.scrollView.reverseZoomFactor > 1.0) {
            self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,5.0*lineScale);
        } else if (self.scrollView.reverseZoomFactor > 0.5) {
            self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,3.0*lineScale);
        } else {
            self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,1.5*lineScale);
        }
    }

    //----------------------------------------------------------------------------------
    func touchesDealWithThePush(_ touch:CGPoint)
    {
//print("touchesDealW ithThePush choiceIsOriginal:",choiceIsOriginal)
//print("touchesDealW ithThePush isaMaskPush:",isaMaskPush)
//print("touchesDealW ithThePush twasForModifai:",twasForModifai)
//print("touchesDealW ithThePush currentWellNum:",currentWellNum)

        if (choiceIsOriginal)
        {
            choiceIsOriginal = false;
            self.processingView.isHidden = false;
            self.startAnimatingDots()

            self.pixelArray.removeAll()
            self.touchesOnSpecific(touch);

            origStartColor0 = origStartColor;
            defaultLineColor0 = defaultLineColor;

            self.findColorMask(touch);
            self.touchesDealWithSpecificFill();

            if (twasForModifai)
            {
                self.combineMask(nil)
            }
            self.turnItAllOn()

        } else if (isaMaskPush) {
            self.processingView.isHidden = false;
        //    self.proces singView.center = self.view.center;
            self.startAnimatingDots()

            DispatchQueue.main.asyncAfter(deadline: (.now() + .milliseconds(100)))
            {
                self.touchesOnSpecific(touch);

                if (currentWellNum == 0)
                {
                    maskedHouseImage0 = maskedHouseImage;
                    origStartColor0 = origStartColor;
                    defaultLineColor0 = defaultLineColor;
                    isaMaskPush = false;

                    currentButtonToDisplay = 0;

                    if ( !twasForModifai)
                    {
                        maskedHouseImage1 = maskedSidingImage;
                        origStartColor1 = origStartColor;
                        defaultLineColor1 = defaultLineColor;
                        currentButtonToDisplay = 1;
                        self.colorWellButton0.isSelected = false;


                        origStartColor2 = origStartColor;
                        defaultLineColor2 = defaultLineColor;
                        currentButtonToDisplay = 2;
                        currentWellNum = 2
                        self.colorWellButton2.isSelected = true;
                        self.currentWell = self.colorWellButton2;
                        self.indButton2.isSelected = true;
                        self.indicatLabel2.isHidden = false;
                        self.currentOverlay = self.scrollView.paintImageOverlay2;
                        self.wellBorder2.strokeColor = UIColor.green.cgColor;
                    }

                    self.findColorMask(touch);
                    self.touchesDealWithSpecificFill();

//print("0 touchesDealW ithThePush house Mask:",houseMask as Any)
                    if (twasForModifai)
                    {
                        self.combineMask(nil)
                    }
//print("touchesDealW ithThePush isaMaskPush:",isaMaskPush)
                //    self.saveBut tonAction(nil);

                } else if (currentWellNum == 1) {
                    maskedHouseImage1 = maskedHouseImage;
                    origStartColor1 = origStartColor;
                    defaultLineColor1 = defaultLineColor;
                } else if (currentWellNum == 2) {
                    maskedHouseImage2 = maskedHouseImage;
                    origStartColor2 = origStartColor;
                    defaultLineColor2 = defaultLineColor;
                } else if (currentWellNum == 3) {
                    maskedHouseImage3 = maskedHouseImage;
                    origStartColor3 = origStartColor;
                    defaultLineColor3 = defaultLineColor;
                } else if (currentWellNum == 4) {
                    maskedHouseImage4 = maskedHouseImage;
                    origStartColor4 = origStartColor;
                    defaultLineColor4 = defaultLineColor;
                }

                masksOnorOff = false;
                //    currProject?.wasProcessed = true;    //*************** change 0.0.46  remove ***

                let userDefaults = UserDefaults.standard
                userDefaults.set(masksOnorOff, forKey: "masksOnorOff")
                userDefaults.synchronize();

                self.turnItAllOn()
            }

        } else {

            if (twasForModifai && (currentWellNum > 0))
            {
                self.pixelArray.removeAll()
                self.touchesOnSpecific(touch);

//print("1 touchesDealW ithThePush currentWellNum:",currentWellNum)

                if (currentWellNum == 1)
                {
                    maskedHouseImage1 = maskedHouseImage;
                    origStartColor1 = origStartColor;
                    defaultLineColor1 = defaultLineColor;
                } else if (currentWellNum == 2) {
                    maskedHouseImage2 = maskedHouseImage;
                    origStartColor2 = origStartColor;
                    defaultLineColor2 = defaultLineColor;
                } else if (currentWellNum == 3) {
                    maskedHouseImage3 = maskedHouseImage;
                    origStartColor3 = origStartColor;
                    defaultLineColor3 = defaultLineColor;
                } else if (currentWellNum == 4) {
                    maskedHouseImage4 = maskedHouseImage;
                    origStartColor4 = origStartColor;
                    defaultLineColor4 = defaultLineColor;
                }

        //        self.findColorMask(touch);
                self.touchesDealWithSpecificFill();

//print("1 touchesDealW ithThePush house Mask:",houseMask as Any)
//print("1 touchesDealW ithThePush maskedHouseImage:",maskedHouseImage as Any)
//print("1 touchesDealW ithThePush origStartColor:",origStartColor as Any)
//print("1 touchesDealW ithThePush defaultLineColor:",defaultLineColor as Any)
                if (twasForModifai)
                {
                    self.combineMask(nil)
                }

                self.turnItAllOn()

            } else {

                if (maskedSidingImage == nil)
                {
//print("maskedSid ingImage == nil here")
                    self.touchesOnSpecific(touch);
                    self.findColorMask(touch);
                    self.touchesDealWithSpecificFill();


                    self.turnItAllOn()
//print("maskedSid ingImage == nil currentWel lNum == 0  saveButto nAction")
                } else {
                    self.processingView.isHidden = false;
                    self.startAnimatingDots()

                    DispatchQueue.main.asyncAfter(deadline: (.now() + .milliseconds(100)))
                    {
                        self.touchesOnSpecific(touch);
                        self.findColorMask(touch);
                        self.touchesDealWithSpecificFill();

                        self.turnItAllOn()
//print("touchesDealW ithThePush here 3  ")
                    }
                }
            }
        }

        isaMaskPush = false;
    }

    // MARK: - paint edit tool
    //---------------------------------------------------------------------
    func setUpPaintPath()
    {
        if let ho = self.paintObject
        {
            guard let refImage = self.scrollView.imageView.image else {return;}

            let hostRectPEC2 = self.scrollView.imageView.frame

            if (hostRectPEC2.size.width >= hostRectPEC2.size.height)
            {
                theImageScale = min(hostRectPEC2.size.width / 4096.0, 1.0);
            } else {
                theImageScale = min(hostRectPEC2.size.height / 4096.0, 1.0);
            }

            self.paintLayer.lineWidth = 20.0 * min(theImageScale*0.85,1.0); //1.25 20.0
            self.paintPath.removeAllPoints();

            wasForModifai = false;

            var rois:[[Int]]

            if let resp = ho.response.last
            {
                for respItem in resp
                {
                    if (respItem.rois.count == 4)
                    {
                        rois = respItem.rois;
                    } else {
                        continue;
                    }

                    respItem.wasGoodDr = true;
                    var scalerValue:CGFloat = 1.0;
//                    if (resolutionOnorOff)
//                    {
                        scalerValue = 3.2;
//                    }

                    let scalerZoom:CGFloat = min(self.scrollView.reverseZoomFactor / (4.5 * theImageScale),1.0);

                    let ratioWidth:CGFloat  = (refImage.size.width  / paintImageWidth)  * scalerValue * scalerZoom;
                    let ratioHeight:CGFloat = (refImage.size.height / paintImageHeight) * scalerValue * scalerZoom;


                    let screenMid = CGPoint(x: self.mainBaseView.bounds.size.width*0.5, y: self.mainBaseView.bounds.size.height*0.5);

                    let theFrame = CGRect(origin: self.scrollView.frame.origin, size: self.scrollView.contentSize);
                    let theImageFrame = CGRect(origin: self.scrollView.frame.origin, size: refImage.size);

                    let midPt = CGPoint(x: screenMid.x*self.scrollView.reverseZoomFactor, y: screenMid.y*self.scrollView.reverseZoomFactor);
                    let startPt = self.convertPoint(point:self.scrollView.contentOffset, fromRect:theFrame, toRect:theImageFrame);

                    let ofsx:Float = min(Float(startPt.x+midPt.x),Float(refImage.size.width-(100.0*ratioWidth)));
                    let ofsy:Float = min(Float(startPt.y+midPt.y),Float(refImage.size.height-(100.0*ratioHeight)));

//print("ofsx,ofsy",ofsx,ofsy)

                    let lineWidth = 10.0 * min(theImageScale*1.25,1.0); //1.25
                    let lineScale = min(theImageScale*2.0,1.0);

                    if (self.scrollView.reverseZoomFactor > 5.5)
                    {
                        self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,14.0*lineScale);
                    } else if (self.scrollView.reverseZoomFactor > 3.75) {
                        self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,10.0*lineScale);
                    } else if (self.scrollView.reverseZoomFactor > 2.5) {
                        self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,8.0*lineScale);
                    } else if (self.scrollView.reverseZoomFactor > 1.0) {
                        self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,5.0*lineScale);
                    } else if (self.scrollView.reverseZoomFactor > 0.5) {
                        self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,3.0*lineScale);
                    } else {
                        self.paintLayer.lineWidth = min(lineWidth * self.scrollView.reverseZoomFactor,1.5*lineScale);
                    }


                    var minX =  999999
                    var maxX = -999999
                    var minY =  999999
                    var maxY = -999999

                    for xy in rois
                    {
                        if (xy[0] > maxX) {maxX = xy[0]}
                        if (xy[0] < minX) {minX = xy[0]}
                        if (xy[1] > maxY) {maxY = xy[1]}
                        if (xy[1] < minY) {minY = xy[1]}
                    }

//print("minX,maxX:",minX,maxX)
//print("minY,maxY:",minY,maxY)

                    let topLeft:Apoint = Apoint((Float(minX) * Float(ratioWidth))+ofsx,(Float(minY) * Float(ratioHeight))+ofsy)
                    let botLeft:Apoint = Apoint((Float(minX) * Float(ratioWidth))+ofsx,(Float(maxY) * Float(ratioHeight))+ofsy)

                    let topRght:Apoint = Apoint((Float(maxX) * Float(ratioWidth))+ofsx,(Float(minY) * Float(ratioHeight))+ofsy)
                    let botRght:Apoint = Apoint((Float(maxX) * Float(ratioWidth))+ofsx,(Float(maxY) * Float(ratioHeight))+ofsy)

                    respItem.rect = CGRect(x: CGFloat(topLeft.x), y: CGFloat(topLeft.y), width: CGFloat(topRght.x - topLeft.x),
                                                                                        height: CGFloat(botLeft.y - topLeft.y))

                    respItem.v1 = CIVector(x: CGFloat(topLeft.x), y: CGFloat(refImage.size.height - CGFloat(topLeft.y)));
                    respItem.v2 = CIVector(x: CGFloat(topRght.x), y: CGFloat(refImage.size.height - CGFloat(topRght.y)));
                    respItem.v3 = CIVector(x: CGFloat(botRght.x), y: CGFloat(refImage.size.height - CGFloat(botRght.y)));
                    respItem.v4 = CIVector(x: CGFloat(botLeft.x), y: CGFloat(refImage.size.height - CGFloat(botLeft.y)));

                    respItem.p1.pt = CGPoint(x: respItem.v1.x, y: CGFloat(topLeft.y));
                    respItem.p2.pt = CGPoint(x: respItem.v2.x, y: CGFloat(topRght.y));
                    respItem.p3.pt = CGPoint(x: respItem.v3.x, y: CGFloat(botRght.y));
                    respItem.p4.pt = CGPoint(x: respItem.v4.x, y: CGFloat(botLeft.y));

//print("respItem.p1.pt",respItem.p1.pt)
//print("respItem.p2.pt",respItem.p2.pt)
//print("respItem.p3.pt",respItem.p3.pt)
//print("respItem.p4.pt",respItem.p4.pt)

                    respItem.delButton = nil;

                    let theButtonV1:PECButton = self.cornerButton.pecPaintDuplicate()!
                    self.scrollView.imageViewOverlay.addSubview(theButtonV1)

                    theButtonV1.center = respItem.p1.pt;
                    theButtonV1.ppt = respItem.p1;
                    theButtonV1.ppt.button = theButtonV1;
                    theButtonV1.respItem = respItem;
                    theButtonV1.topLPt = true;
                    theButtonV1.isSelected = false;
                    theButtonV1.isForPaint = true;
                    self.paintPath.move(to:theButtonV1.center);

                    let theButtonV2:PECButton = self.cornerButton.pecPaintDuplicate()!
                    self.scrollView.imageViewOverlay.addSubview(theButtonV2)

                    theButtonV2.center = respItem.p2.pt;
                    theButtonV2.ppt = respItem.p2;
                    theButtonV2.ppt.button = theButtonV2;
                    theButtonV2.respItem = respItem;
                    theButtonV2.topRPt = true;
                    theButtonV2.isSelected = false;
                    theButtonV2.isForPaint = true;
                    self.paintPath.addLine(to:theButtonV2.center);

                    let theButtonV3:PECButton = self.cornerButton.pecPaintDuplicate()!
                    self.scrollView.imageViewOverlay.addSubview(theButtonV3)

                    theButtonV3.center = respItem.p3.pt;
                    theButtonV3.ppt = respItem.p3;
                    theButtonV3.ppt.button = theButtonV3;
                    theButtonV3.respItem = respItem;
                    theButtonV3.botRPt = true;
                    theButtonV3.isSelected = false;
                    theButtonV3.isForPaint = true;
                    self.paintPath.addLine(to:theButtonV3.center);

                    let theButtonV4:PECButton = self.cornerButton.pecPaintDuplicate()!
                    self.scrollView.imageViewOverlay.addSubview(theButtonV4)

                    theButtonV4.center = respItem.p4.pt;
                    theButtonV4.ppt = respItem.p4;
                    theButtonV4.ppt.button = theButtonV4;
                    theButtonV4.ppt.nx = respItem.p1;
                    theButtonV1.ppt.lt = respItem.p4;
                    theButtonV4.respItem = respItem;
                    theButtonV4.botLPt = true;
                    theButtonV4.isSelected = false;
                    theButtonV4.isForPaint = true;
                    self.paintPath.addLine(to:theButtonV4.center);
                    self.paintPath.close();

                      self.theButtonV0 = theButtonV1;


                    //------------- lock button -----------------
                    let lockButton:PECButton = self.cornerButton.pecPaintDuplicate()!
                    lockButton.setBackgroundImage(UIImage(named: "lockArea.png"), for: .normal);
                    lockButton.setBackgroundImage(UIImage(named: "lockArea.png"), for: .highlighted);
                    lockButton.tag = 14;
                    self.scrollView.imageViewOverlay.addSubview(lockButton)

                    var diag:Bline = Bline.init();
                    diag.spoint = theButtonV1.center;
                    diag.epoint = theButtonV3.center;

                    lockButton.center = self.findMidPt2Ddd(diag);
                    lockButton.isHidden = false;
                    lockButton.isForPaint = true;
                    lockButton.isLock = true;
                    lockButton.ppt = Ppoint.init(pt:lockButton.center);
                    lockButton.respItem = respItem;
                    respItem.lockButton = lockButton;
                    lockButton.topLeft = theButtonV1.center;
                    lockButton.botRght = theButtonV3.center;

                    lockButton.lockLinkFirst = self.theButtonV0;
                    theButtonV1.lockLinkFirst = lockButton;


                    //------------- sizing button -----------------
                    let sizeButton:PECButton = self.cornerButton.pecPaintDuplicate()!
                    sizeButton.setBackgroundImage(UIImage(named: "lockSizing.png"), for: .normal);
                    sizeButton.setBackgroundImage(UIImage(named: "lockSizing.png"), for: .highlighted);
                    sizeButton.tag = 15;
                    self.scrollView.imageViewOverlay.addSubview(sizeButton)

                    sizeButton.center = theButtonV3.center;
                    sizeButton.center.x -= theButtonV3.frame.size.width;
                    sizeButton.center.y -= theButtonV3.frame.size.height;

                    sizeButton.isHidden = false;
                    sizeButton.isForPaint = true;
                    sizeButton.isForSizing = true;
                    sizeButton.ppt = Ppoint.init(pt:sizeButton.center);
                    sizeButton.respItem = respItem;

                    sizeButton.showhideLt = theButtonV3;
                    theButtonV3.showhideLt = sizeButton;

                    //------------- cut button -----------------
                    let cutButton:UIButton = self.cutButton.duplicateCut()!
                    cutButton.tag = 12;
                    self.scrollView.imageViewOverlay.addSubview(cutButton)

                    var bottom:Bline = Bline.init();
                    bottom.spoint = theButtonV3.center;
                    bottom.epoint = theButtonV4.center;

                    cutButton.center = self.findMidPt2Ddd(bottom);
                    cutButton.isHidden = true;

                    theButtonV3.delButtonNx = cutButton;
                    theButtonV4.delButtonLt = cutButton;

                    //------------- bucket button -----------------
                    let bucketButton:UIButton = self.cutButton.duplicateCut()!
                    bucketButton.setBackgroundImage(UIImage(named: "bucketArea.png"), for: .normal);
                    bucketButton.tag = 13;
                    self.scrollView.imageViewOverlay.addSubview(bucketButton)

                    var top:Bline = Bline.init();
                    top.spoint = theButtonV1.center;
                    top.epoint = theButtonV2.center;

                    bucketButton.center = self.findMidPt2Ddd(top);
                    bucketButton.isHidden = true;

                    theButtonV1.delButtonNx = bucketButton;
                    theButtonV2.delButtonLt = bucketButton;


                    self.paintLayer.path = nil;
                    self.paintLayer.path = self.paintPath.cgPath;
                }
            }
        }
    }

    //---------------------------------------------------------------------
    func drawPaintLineAt()
    {
//print("draw Paint LineAt")
        if let button = self.theButtonV0
        {
            self.paintPath.removeAllPoints();
            let ppft:Ppoint = button.ppt;

            if (button.tag == 10)
            {
                self.paintPath.move(to:button.center);

                self.scrollView.deleteDimButton.center = CGPoint(x: ppft.pt.x - (maxDistance * 1.75),
                                                                 y: ppft.pt.y - (maxDistance * 1.75))

                self.scrollView.editDimButton.center   = CGPoint(x: ppft.lt.pt.x + (maxDistance * 1.75),
                                                                 y: ppft.lt.pt.y - (maxDistance * 1.75))

                if (self.scrollView.wasStart)
                {
                    self.paintPath.addLine(to:ppft.nx.pt)

                } else {

                    if var curPt = ppft.nx
                    {
                        while curPt != ppft
                        {
                            self.paintPath.addLine(to:curPt.pt)
                            curPt = curPt.nx;
                        }
                    }
                }

                if (self.scrollView.editDimButton.tag != 6)
                {
                    self.paintPath.close()
                }

                self.paintLayer.path = nil;
                self.paintLayer.path = self.paintPath.cgPath;

            } else {
                var sizeButton0:PECButton?

                if let delBuNx = ppft.button.delButtonNx
                {
                    delBuNx.center = findMidPtButton2Ddd(ppft.pt,ppft.nx.pt)
                }

                if let sizeButton = ppft.button.showhideLt
                {
                    sizeButton0 = sizeButton;
                    sizeButton.center = ppft.pt;
                    sizeButton.center.x -= sufaceButtonRadius*0.85;
                    sizeButton.center.y -= sufaceButtonRadius*0.85;
                }

                self.paintPath.move(to:ppft.pt);
                if let lockButton = button.lockLinkFirst
                {
                    lockButton.topLeft = ppft.pt;
                    lockButton.botRght = ppft.pt;
                    var botBut0:PECButton?

                    if ((ppft.pt.x > lockButton.center.x) && (ppft.pt.y > lockButton.center.y)) {botBut0 = button;}

                //    var amount:CGFloat = 0.0;

                    if var curPt = ppft.nx
                    {
                        while curPt != ppft
                        {
                            self.paintPath.addLine(to:curPt.pt)
                            if (curPt.pt.x < lockButton.topLeft.x) {lockButton.topLeft.x = curPt.pt.x}
                            if (curPt.pt.y < lockButton.topLeft.y) {lockButton.topLeft.y = curPt.pt.y}

                            if (curPt.pt.x > lockButton.botRght.x) {lockButton.botRght.x = curPt.pt.x;}
                            if (curPt.pt.y > lockButton.botRght.y) {lockButton.botRght.y = curPt.pt.y;}

                            if ((curPt.pt.x > lockButton.center.x) && (curPt.pt.y > lockButton.center.y))
                            {
                                if let botBut = botBut0
                                {
                                    if (curPt.pt.x > botBut.center.x) {botBut0 = curPt.button;}
                                    if (curPt.pt.y > botBut.center.y) {botBut0 = curPt.button;}
                                } else {
                                    botBut0 = curPt.button
                                }
                            }

                            if let delBuNx = curPt.button.delButtonNx
                            {
                                delBuNx.center = findMidPtButton2Ddd(curPt.pt,curPt.nx.pt)
                            }

                            if let sizeButton = curPt.button.showhideLt
                            {
                                sizeButton0 = sizeButton;
                                sizeButton.center = curPt.pt;
                                sizeButton.center.x -= sufaceButtonRadius*0.85;
                                sizeButton.center.y -= sufaceButtonRadius*0.85;
                            }

                            curPt = curPt.nx;
                        }
                    }
//print("lockButton.topLeft,botRght:",lockButton.topLeft,lockButton.botRght)
                    lockButton.findMidPt(lockButton.topLeft,lockButton.botRght);

                    if let sizeButton = sizeButton0
                    {
                        if let botBut = botBut0
                        {
                            let dist = findDistance2Ddd(botBut.center, sizeButton.center)
                            if (dist > sufaceButtonRadius*1.75)
                            {
                                let buttonV3 = sizeButton.showhideLt
                                buttonV3?.showhideLt = nil;

                                botBut.showhideLt = sizeButton;
                                sizeButton.showhideLt = botBut;

                                sizeButton.center = botBut.center;
                                sizeButton.center.x -= sufaceButtonRadius*0.85;
                                sizeButton.center.y -= sufaceButtonRadius*0.85;
                            }
                        }
                    }
                }

                if (self.scrollView.editDimButton.tag != 6)
                {
                    self.paintPath.close()
                }

                self.paintLayer.path = nil;
                self.paintLayer.path = self.paintPath.cgPath;
            }
        }
    }

    // MARK: - math
    //--------------------------------------------------------------------
    func findDistance2Ddd(_ pt1:CGPoint, _ pt2:CGPoint) -> CGFloat
    {
        return sqrt(((pt2.x - pt1.x) * (pt2.x - pt1.x)) + ((pt2.y - pt1.y) * (pt2.y - pt1.y)));
    }

    //-----------------------------------------------------------------------
    func findMidPt2Ddd(_ line1:Bline) -> CGPoint
    {
        let midpoint:CGPoint = CGPoint(x:((line1.epoint.x - line1.spoint.x) * 0.5) + line1.spoint.x,
                                       y:((line1.epoint.y - line1.spoint.y) * 0.5) + line1.spoint.y);
        return midpoint;
    }

    //-----------------------------------------------------------------
    func findMidPtButton2Ddd(_ spoint:CGPoint, _ epoint:CGPoint) -> CGPoint
    {
        let midpoint:CGPoint = CGPoint(x:((epoint.x - spoint.x) * 0.5) + spoint.x,
                                       y:((epoint.y - spoint.y) * 0.5) + spoint.y);
        return midpoint;
    }

    //--------------------------------------------------------------------
    func interLineLineX2Ddd(_ line1:Bline, _ line2:Bline) -> CGPoint
    {
        var x1, y1, x2, y2, x3, y3, x4, y4, t1, above1, below:CGFloat;

        var intersection1:CGPoint = line1.spoint;

        x1 = line1.spoint.x;
        y1 = line1.spoint.y;

        x2 = line1.epoint.x;
        y2 = line1.epoint.y;

        x3 = line2.spoint.x;
        y3 = line2.spoint.y;

        x4 = line2.epoint.x;
        y4 = line2.epoint.y;

        above1 = (y3 - y4) * (x1 - x3) + (x4 - x3) * (y1 - y3);
        below  = (y3 - y4) * (x1 - x2) + (x4 - x3) * (y1 - y2);

        if (abs(below) < CGFloat(Float.ulpOfOne)) {return intersection1;}

        t1 = above1 / below;

        intersection1.x = CGFloat(line1.spoint.x + t1 * (line1.epoint.x - line1.spoint.x));
        intersection1.y = CGFloat(line1.spoint.y + t1 * (line1.epoint.y - line1.spoint.y));

        return intersection1;
    }

    //MARK: - Paint Cha nges Without save method
//    //-----------------------------------------------------------------
//    func updateLastIndexImageToView()
//    {
////print("00a updateLast Index ImageToView")
//        if UserDefaults.standard.value(forKey: "Last_Image_url") != nil
//        {
//            let lastImageUrl = UserDefaults.standard.value(forKey: "Last_Image_url") as! String
////print("lastIma geUrl:",lastImageUrl)
//
//            if lastImageUrl.count > 0
//            {
//                lastChangedImageUrl = (lastImageUrl as NSString).lastPathComponent
////print("00a currentIm ageName:",currentImageName)
//                lastChangedImageUrl = lastChangedImageUrl.changeExtensions(name:lastChangedImageUrl)
//                getDataFromDirectory(nameOfImage: lastChangedImageUrl)
//            }
//        }
//    }

//    //-----------------------------------------------------------------
//    func getDataFromDirectory(nameOfImage : String)
//    {
//print("00a getData From Directory")
//        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
//        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
//        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
//        if let dirPath = paths.first
//        {
//            if let curHome = homeviewController
//            {
//                let paintMasksal0 = curHome.loadImgTypeGallery(type:self.paintName0,filename:nameOfImage, ext: "png")
//
//                var filenamePaint0 = nameOfImage.replacingOccurrences(of:".jpg", with: "_paint0.png")
//                if paintMasksal0.count>1
//                {
//                    filenamePaint0 = nameOfImage.replacingOccurrences(of:".jpg", with: "_paint0_\(paintMasksal0.count-1).png")
//
//                }
//                let imageURLpaint0 = URL(fileURLWithPath: dirPath).appendingPathComponent(filenamePaint0)
//                if let imagePaint0 = UIImage(contentsOfFile: imageURLpaint0.path)
//                {
//print("00a getData From Directory   self.scrollView.paintImageOverlay0.image new image")
//                    self.scrollView.paintImageOverlay0.image = imagePaint0
//                    //self.paintIm ageView.image = imagePaint0
//                }
//            }
//        }
//    }

    // MARK: -  UITextView delegate methods
    //-----------------------------------------------------------------
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool
    {
        return true;
    }

    //---------------------------------------------------------------
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool
    {
        return true;
    }


    //--------------------------------------------------------------------
    func textViewDidBeginEditing(_ textView: UITextView)
    {
    }

    //------------------------------------------------------------------
    func textViewDidEndEditing(_ textView: UITextView)
    {
    }

    //--------------------------------------------------------------------------------------------
    func textViewDidChange(_ textView: UITextView)
    {
    }

    //--------------------------------------------------------------------------------------------
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if(text == "\n")
        {
            textView.resignFirstResponder()
            textView.endEditing(true)

            return false
        }
        return true
    }


    // MARK: - Responding to keyboard events
    //------------------------------------------------------------------------------------
    @objc func currentInputModeDidChange(notification: Notification)
    {
        if let inputMode = textInputMode?.primaryLanguage
        {
            if lastInputMode == "dictation" && lastInputMode != inputMode
            {
                self.view.frame.origin.y = -lastKeybrdHeight;
            }

            if inputMode == "dictation" && lastInputMode != inputMode
            {
                isDictationRunning = true
                self.view.frame.origin.y = 0;
            }
            lastInputMode = inputMode
        }
    }

    //------------------------------------------------------------------------------------
    @objc func keyboardWillShow(_ notification: Notification)
    {
        if let keyboardSize = (((notification as NSNotification).userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue)
        {
            self.view.frame.origin.y = -keyboardSize.height;
            lastKeybrdHeight = keyboardSize.height
        }
    }

    //------------------------------------------------------------------------------------
    @objc func keyboardWillHide(_ notification: Notification)
    {
        //        if let keyboardSize = ((notification as NSNotification).userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        //        {
        //            UIView.animate(withDuration: 4.5, animations:
        //            {() in
        self.view.frame.origin.y = 0; //(keyboardSize.height - 34);
        //            }, completion: {(completion) in
        //            })
        //        }
    }


}

//--------------------------------------------------------
extension HomeViewController
{
    //--------------------------------------------------------
    func loadImgTypeGallery(type:String,filename:String,ext:String)->Array<URL>
    {
        var fileArray:Array<URL> = Array<URL>()
        let paint_filename1 = filename.replacingOccurrences(of:".jpg", with: type)
//print("paint_filename1 :",paint_filename1)

        do
        {
            let directory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            if let urlArray = try? FileManager.default.contentsOfDirectory(at: directory, includingPropertiesForKeys: [.creationDateKey], options:.skipsHiddenFiles).filter({ $0.pathExtension == ext })
            {
//print("urlArray :",urlArray)

                for url in urlArray
                {
//print("url.absoluteString :",url.absoluteString)
                    if url.absoluteString.contains(paint_filename1) && !url.absoluteString.contains("_original")
                    {
                        fileArray.append(url)
//print("a Hit ---------- url.absoluteString :",url.absoluteString)
                    }
                }
            } else {

            }
        }

        return fileArray
    }
}

//--------------------------------------------------------
extension UIButton
{
    func duplicateCut() -> UIButton?
    {
        let buttonDuplicate = UIButton.init(type:ButtonType.custom);
        buttonDuplicate.frame = self.frame;
        buttonDuplicate.setBackgroundImage(UIImage(named: "scissors"), for: .normal);

        buttonDuplicate.contentHorizontalAlignment = self.contentHorizontalAlignment;
        buttonDuplicate.contentVerticalAlignment =  self.contentVerticalAlignment;
        buttonDuplicate.adjustsImageWhenHighlighted =  false;
        buttonDuplicate.showsTouchWhenHighlighted =  false;
        buttonDuplicate.adjustsImageWhenDisabled =  false;
        buttonDuplicate.contentEdgeInsets = self.contentEdgeInsets;


        //        buttonDuplicate.lockCornerRecog = UITapGestureRecognizer.init(target: buttonDuplicate, action: #selector(buttonDuplicate.lockCorner(_:)));
        //        buttonDuplicate.lockCornerRecog.numberOfTapsRequired = 1;
        //        buttonDuplicate.lockCornerRecog.delegate = currentMainImageController.scrollView;
        //        buttonDuplicate.lockCornerRecog.isEnabled = true;
        //
        //        buttonDuplicate.gestureRecognizers = [buttonDuplicate.deleteCornerRecog,buttonDuplicate.lockCornerRecog];

        let eventArray:[UIControl.Event] = [UIControl.Event.touchDown,UIControl.Event.touchUpInside,
                                            UIControl.Event.touchUpOutside,UIControl.Event.touchDragInside,
                                            UIControl.Event.touchDragOutside];

        // Copy targets and associated actions
        self.allTargets.forEach
            { target in
                for controlEvent in eventArray
                {
                    if let actions = self.actions(forTarget: target, forControlEvent: controlEvent)
                    {
                        for action in actions
                        {
                            buttonDuplicate.addTarget(target, action: Selector(action), for: controlEvent)
                        }
                    }
                }
        }

        return buttonDuplicate
    }
}









