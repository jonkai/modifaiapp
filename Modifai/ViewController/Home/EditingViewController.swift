import Foundation
import UIKit
import Photos
import PECimage


//----------------------
public struct Bline
{
    public var spoint:CGPoint = CGPoint(x:0.0,y:0.0);
    public var epoint:CGPoint = CGPoint(x:0.0,y:0.0);

	//----------------------
    public init()
    {
    }

	//----------------------
    public init(_ s:CGPoint, _ e:CGPoint)
    {
    	spoint = s;
    	epoint = e;
    }
}

var showGarButton: Bool = false
var showFrntButton: Bool = false
var showWndwButton: Bool = false
var windowsOnorOff: Bool = false

var currentWindow = 0
var theImageScale:CGFloat = 1.0


let polyLayer:CAShapeLayer = CAShapeLayer.init()
//var doorTypeKount:Int = 4 //********** change 3.0.2 remove was already in class below ***
//var doorStyleKount:Int = 3 //********** change 3.0.2 remove was already in class below ***

var currentMainImageController:EditingViewController!
var currentController:String!

//--------------------------------------------------------
class EditingViewController: UIViewController, UIGestureRecognizerDelegate, UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource
{
    @IBOutlet weak var scrollView: ImageScrollView!
    @IBOutlet weak var questionView: UIView!
    @IBOutlet weak var barView: UIView!

    @IBOutlet weak var dot1: UIImageView!
    @IBOutlet weak var dot2: UIImageView!
    @IBOutlet weak var dot3: UIImageView!
    
    @IBOutlet weak var undoBtn: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var addDrButton: UIButton!
    @IBOutlet weak var doneBtn: UIButton!

    @IBOutlet weak var shareButton: UIButton!
    
    @IBOutlet weak var doorAddedView: UIView!
    
    @IBOutlet weak var garageComentButton: UIButton!
    @IBOutlet weak var frontDrComentButton: UIButton!
    
    @IBOutlet weak var goodButton: UIButton!
    @IBOutlet weak var wrongButton: UIButton!
    @IBOutlet weak var naButton: UIButton!
    
    @IBOutlet weak var goodImageButton: UIButton!
    @IBOutlet weak var badImageButton: UIButton!
    
    @IBOutlet weak var horzFrntDoors: UIButton!
    
    @IBOutlet weak var vertFrntDoors: UIButton!
    
	@IBOutlet weak var doorTypeView: UIView!
    @IBOutlet weak var drPicker: UIPickerView!


    var selectedReferenceItem: ReferenceItem?
    var doorPathsSet:Bool = false
    var sourceArray  = [[UIImage]]()
    var undoPECButton:PECButton?
    var redoPECButton:PECButton?
    var undoPoint:CGPoint!
    var redoPoint:CGPoint!

    var oneCarImgae:UIImage?
    var twoCarImgae:UIImage?
    var oneDoorImgae:UIImage?
    var twoDoorImgae:UIImage?
    var baseReferenceImage:UIImage = UIImage()

    //------- processing screen effects -------
    let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular) //prominent,regular,extraLight, light, dark
    var blurEffectView = UIVisualEffectView()

    var dotLayers = [CALayer]()
    var dotsScale = 1.5
    var dotsCount: Int = 3
    
    var doorModes: [String] = [String]()
    var doorStyle: [String] = [String]()
    
    var sourceSet = Set([garDoorSourceDdr,garDoorSourceSdr,frntDoorSourceDdr,frntDoorSourceSdr,nil])
    
    var currDoorType: Int = 100
    
    var currComment1Type: Int = 0
    var currComment2Type: Int = 0
    
    var kountDrs:Int = 0
    var currentWindow = 0
    let polyPath:UIBezierPath = UIBezierPath.init()
    var startY:CGFloat = 0.0
    var startSize:CGFloat = 1.0
    
    var length2:CGFloat = 1.0
    var length3:CGFloat = 1.0
    var length4:CGFloat = 1.0
    var length5:CGFloat = 1.0
    var length6:CGFloat = 1.0
    
    var length2y:CGFloat = 1.0
    var length3y:CGFloat = 1.0
    var length4y:CGFloat = 1.0
    var length5y:CGFloat = 1.0
    var length6y:CGFloat = 1.0
    
    var respToSize:Response?
    var doorTypeKount:Int = 4
    var doorStyleKount:Int = 3
    
    var jsonError:NSError?
    
    var wasChooseArch:Bool = false
    var wasChoosePoly:Bool = false
    var wasChooseRect:Bool = false
    var wasChooseNone:Bool = false
    var wasChoosen:Bool    = false
   // var wasProcessed:Bool  = false //********* change remove not used 0.0.46
    
    var doorWasPicked: Bool = false
    var whichDoor: Int = 0
    
    let reviveAlbum = "\(brand) Garages"
    var assetCollection: PHAssetCollection!
//    var shouldAd dToAlbum:Bool  = true
    var greyLumIsCalculated:Bool = false
//    var fullsize:Bool = true;  //---------- why?? ---

    var lastInputMode: String? = "random"
    var isDictationRunning: Bool = false
    var lastKeybrdHeight: CGFloat = 0.0

    
    @IBOutlet weak var addDblGarDrButton: UIButton!
    @IBOutlet weak var cornerButton: PECButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var showHideButton: UIButton!
    weak var lockButton: UIButton!

//	var theBut tonV0:PECButton?   //*************** change 0.0.15 add *** (never mind) don't add

    // MARK: - loading
    //------------------------------------------------------------------------------------
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    //------------------------------------------------------------------------------------
    override func viewDidLoad()
    {
//print("viewDidLoad")
        currentMainImageController = self
        super.viewDidLoad()
        
        //-----------------door types for picker view------------
        self.doorModes =
		[
			"two car garage",    //0
			"one car garage",    //1
			"double front door",//2
			"single front door",//3
			"double pane window",//4
			"single pane window" //5
        ]
        
        //-----------------door types for picker view------------
        self.doorStyle =
		[
			"arched",        //0
			"rectangular",    //1
			"dutch corner"    //2
        ]

        if (hasOnlyGarageDoors) //********** change 3.0.2 add if block ***
        {
            doorTypeKount = 2
        }
//print("viewDidLoad self.scrollView.containerView.frame:",self.scrollView.containerView.frame)

		scrollView.contentSize = self.scrollView.containerView.frame.size
		self.scrollView.delegate = self.scrollView

		self.scrollView.panGestureRecognizer.delaysTouchesBegan = false
		self.scrollView.panGestureRecognizer.minimumNumberOfTouches = 2

		self.scrollView.imageView.frame = self.view.frame
		self.scrollView.containerView.frame = self.view.frame
		self.scrollView.imageContainView.frame = self.view.frame

		self.scrollView.paintImageView.frame = self.view.frame
		self.scrollView.paintImageView1.frame = self.view.frame
		self.scrollView.paintImageView2.frame = self.view.frame

		self.scrollView.garageImageView.frame = self.view.frame

		self.scrollView.imageViewOverlay.frame = self.view.frame

		self.scrollView.isHidden = true
		//print("viewDidLoad self.scrollView.imageView.frame:",self.scrollView.imageView.frame)

		let editTutorial = UserDefaults.standard.bool(forKey: "editTutorial")

		if editTutorial
		{
			self.scrollView.isHidden = false
			AppUtility.lockOrientation(.all)

		} else {
			AppUtility.lockOrientation(.portrait)

			UserDefaults.standard.set(true, forKey: "editTutorial")
			let storyboard = UIStoryboard(name: "CustomTabBar", bundle: nil)
			let onboradingVC = storyboard.instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
			onboradingVC.isEditIntro = true
			self.navigationController?.pushViewController(onboradingVC, animated: true)
		}

		self.setupScreen()
    }

    //----------------------------------------------------------
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
 
        currentController = "edit"
        isThisForPaint = false;
        editedGarageAdded = false;
        editedDoorAdded = false;
		isTwoCar = false;
		isOneCar = false;
		isDoor = false;
		isDouble = false;

	//	self.fullsize = true;

        currentMainImageController = self
		shouldSave = false;

//print("edit viewWillAppear homeviewController:",homeviewController as Any)
		if let curHome = homeviewController
		{
//print("viewWillAppear curHome:",curHome)
            self.scrollView.paintImageView.image = curHome.paintImageView.image;
            self.scrollView.paintImageView1.image = curHome.paintImageView1.image;
            self.scrollView.paintImageView2.image = curHome.paintImageView2.image;  //***************** change 0.0.40 add ***
			curHome.tabBarController?.tabBar.isHidden = true
		}
    }

    //---------------------------------------------------------
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
    }

    //-------------------------------------------------------------
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
		super.viewWillTransition(to: size, with: coordinator) //*********** killed main function of app when you removed this??? why??? ***
//            viewTran sition = true;
//            self.viewSettings(size);
		navigationController?.setNavigationBarHidden(false, animated: true)

		if let curHome = homeviewController
		{
			curHome.tabBarController?.tabBar.isHidden = true
		}
	}

    //------------------------------------------------------------
    func setupScreen()
	{
		AppUtility.lockOrientation(.all)

		navigationController?.setNavigationBarHidden(false, animated: true)
				 let backButton = UIBarButtonItem()
					 backButton.title = "Home"
        self.navigationController?.navigationBar.tintColor = titleColor
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
		NotificationCenter.default.addObserver(self, selector: #selector(EditingViewController.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(EditingViewController.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)

		self.jsonError = nil
		self.doorPathsSet = false

		undoBtn.isHidden = true
		doneBtn.isHidden = false

		sourceSet.removeAll()
		currentOneCarGar = 0
		currentTwoCarGar = 0
		currentSinglFrntDoor = 0
		currentDoublFrntDoor = 0
		currentWindow = 0

		wasChooseArch = false
		wasChoosePoly = false
		wasChooseRect = false
		wasChooseNone = false
		wasChoosen    = false
		//wasProcessed  = false 0.0.46

		showGarButton = false
		showFrntButton = false
		showWndwButton = false

		doorWasPicked = false
		greyLumIsCalculated = false

		//print("versVnum", versVnum)

		//-------- V4 V2 V1 Version true for these -------------
		if ((versVnum == 4) || (versVnum == 2) || (versVnum == 1))
		{
			wasChooseArch = true
			wasChoosePoly = true
			wasChooseRect = true
			wasChoosen    = true

			//print("self.questionView", self.questionView)
			if let view = self.questionView
			{
			   //print("view")
			   view.removeFromSuperview()
			}
		}

		self.killBadDoors()
		self.setupLayers()
		self.imageSetup()

		phonePortrait = self.view.bounds.size.height > self.view.bounds.size.width
		lastPhonePortrait = phonePortrait
		skipConfigure = false

		polyLayer.backgroundColor = UIColor.clear.cgColor
		polyLayer.fillColor = UIColor.clear.cgColor
		polyLayer.strokeColor = UIColor(displayP3Red: 0.0, green: 0.5, blue: 0.5, alpha: 1.0).cgColor
		polyLayer.lineWidth = 20.0

		self.scrollView.imageViewOverlay.layer.addSublayer(polyLayer)

		finishProgressViewMain()
		editDrLocs()
	}

    //------------------------------------------------------------------------------------
    override func viewWillDisappear(_ animated: Bool)
    {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UITextInputMode.currentInputModeDidChangeNotification, object: nil)
        
        super.viewWillDisappear(animated)
    }
    
    // MARK: - image
    //-------------------------------------------------------------------------------
    func imageSetup()
    {
        self.scrollView.imageView.image = originalHomeImage
        self.addBlurEffect()
    }
    
    //-----------------------------------------------------
    func startAnimating()
    {
    }
    
    //------------------------------------------------------------------------------------
    func addBlurEffect()
    {
        self.blurEffectView = UIVisualEffectView(effect: self.blurEffect)
        self.blurEffectView.frame = self.view.bounds
        
        self.blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    //-----------------------------------------------------
    func setupLayers()
    {
        addDrButton.layer.cornerRadius = 15
        undoBtn.layer.cornerRadius = 15
        doneBtn.layer.cornerRadius = 15

        //       dotLayers.append(dot1.layer)
        //       dotLayers.append(dot2.layer)
        //       dotLayers.append(dot3.layer)
    }
    
    //----------------------------------------------------------------------------------------------------------
    func editDrLocs()
    {
//print("edit DrLocs")
        let rect:CGRect = self.scrollView.imageView.frame
        if (rect.size.width >= rect.size.height)
        {
            theImageScale = min(rect.size.width / 4096.0, 1.0)
        } else {
            theImageScale = min(rect.size.height / 4096.0, 1.0)
        }
        
        polyLayer.lineWidth = 20.0 * min(theImageScale*1.25,1.0)
        
        self.polyPath.removeAllPoints()

        if (houseObject == nil)
        {
            self.addDrAction(addDrButton)
        } else {
            if (self.doorPathsSet)
            {
                self.drawDoorObjectAt()
                self.scrollView.drawPolyCorners()

            } else {
//print("edit DrLocs else")
                self.editDoorObjectAt()
                self.drawDoorObjectAt()   //*************** change 0.0.15 add ***
                self.scrollView.drawPolyCorners()
                self.reDrawAllDoors()
            }
        }
    }

    //--------------------------------------------------------------------------------------------
    func finishProgressViewMain()
    {
//print("finishPr ogress ViewMain")
        self.scrollView.imageContainView.contentMode = .scaleAspectFit
        self.scrollView.imageView.contentMode = .scaleAspectFit
        self.scrollView.imageViewOverlay.contentMode = .scaleAspectFit

        dontRotate = false
        
        self.scrollView.zoomScale = 1.0
        
        self.scrollView.transform = CGAffineTransform.identity
        self.scrollView.imageContainView.transform = CGAffineTransform.identity
        self.scrollView.imageView.transform = CGAffineTransform.identity
        self.scrollView.imageViewOverlay.transform = CGAffineTransform.identity
        
		if let image = originalHomeImage
        {
            containWidthImage  = image.size.width
            containHeightImage = image.size.height
//print("finishProgres sViewMain containWi dthImage, containHeightImage",containW idthImage, containHeightImage)

            self.scrollView.imageView.frame = CGRect(x:0, y:0, width:containWidthImage, height:containHeightImage)
            self.scrollView.containerView.frame = self.scrollView.imageView.frame
            self.scrollView.imageContainView.frame = self.scrollView.imageView.frame

            self.scrollView.paintImageView.frame = self.scrollView.imageView.frame
            self.scrollView.paintImageView1.frame = self.scrollView.imageView.frame
            self.scrollView.paintImageView2.frame = self.scrollView.imageView.frame

            self.scrollView.garageImageView.frame = self.scrollView.imageView.frame

            self.scrollView.imageViewOverlay.frame = self.scrollView.imageView.frame

            if (containWidthImage  < 0.001) {return }
            if (containHeightImage < 0.001) {return }
            
            self.scrollView.configureForImageSize()
        }
    }

	//------------------------------------
    func setUpImageSize()->CGSize
    {
        let myImageWidth = originalHomeImage.size.width
        let myImageHeight = originalHomeImage.size.height
        let myViewWidth = self.scrollView.frame.size.width  //........... why this function? ...
        
        let ratio = myViewWidth/myImageWidth
        var scaledHeight =  myImageHeight * ratio
        
        if myImageWidth < myImageHeight && scaledHeight > self.scrollView.frame.height
        {
            scaledHeight = self.scrollView.frame.height
            
        }

        if scaledHeight < self.scrollView.frame.height
        {
            scaledHeight = self.scrollView.frame.height
        }

//print("setUpImageSize myViewWidth, scaledHeight",myViewWidth, scaledHeight)
        return CGSize(width: myViewWidth, height: scaledHeight)
    }

	//------------------------------------
    func drawDoorObjectAt()
    {
        if (undoPECButton != nil)
        {
            undoBtn.isHidden = false
            if undoBtn.titleLabel!.text == "Redo"
            {
                undoBtn.setTitle("Undo", for: .normal)
            }
        } else {
            undoBtn.isHidden = true
        }
//print("draw Door ObjectAt originalHomeImage:",originalHomeImage as Any)
//print("draw Door ObjectAt houseObject:",houseObject as Any)
        if let ho = houseObject
        {
            guard let refImage = originalHomeImage else { return }
            self.polyPath.removeAllPoints()
            for resp in ho.response
            {
                for respItem in resp
                {
//print("drawDoorObjectAt respItem.v1:",respItem.v1)
//print("drawDoorObjectAt respItem.roisouter:",respItem.roisouter)
//print("drawDoorObjectAt respItem.imageID_db:",respItem.imageID_db)
                    if ( !respItem.wasGoodDr) { continue }
                    if (respItem.p1.nx == nil) { continue }   //********** change 0.0.40 add *** 

                    let pp1:Ppoint! = respItem.p1
                    let pp2:Ppoint! = pp1.nx
                    let pp3:Ppoint! = pp2.nx
                    let pp4:Ppoint! = pp3.nx
                    let pp5:Ppoint! = pp4.nx
                    let pp6:Ppoint! = pp5.nx


                    if (respItem.wasPoly)
                    {
                        respItem.v1 = CIVector(x: pp1.pt.x, y: refImage.size.height-pp2.pt.y)
                        respItem.v2 = CIVector(x: pp4.pt.x, y: refImage.size.height-pp3.pt.y)
                        respItem.v3 = CIVector(x: pp5.pt.x, y: refImage.size.height-pp5.pt.y)
                        respItem.v4 = CIVector(x: pp6.pt.x, y: refImage.size.height-pp6.pt.y)

                        respItem.path.removeAllPoints()
                        respItem.path.move(to:pp1.pt)
                        respItem.path.addLine(to:pp2.pt)
                        respItem.path.addLine(to:pp3.pt)
                        respItem.path.addLine(to:pp4.pt)
                        respItem.path.addLine(to:pp5.pt)
                        respItem.path.addLine(to:pp6.pt)
                        respItem.path.close()
                        respItem.rect = respItem.path.cgPath.boundingBox
                        polyPath.append(respItem.path)

						if let lockButton = respItem.p1.button.lockLinkFirst
						{
							lockButton.topLeft = pp1.pt;
							lockButton.botRght = pp5.pt;
							lockButton.findMidPt(lockButton.topLeft,lockButton.botRght);
						}

						if let sizeButton = pp5.button.showhideLt
						{
							sizeButton.center = pp5.pt;
							sizeButton.center.x -= sufaceButtonRadius*0.75;
							sizeButton.center.y -= sufaceButtonRadius*0.75;
						}

						if let delBuNx = pp5.button.delButtonNx
						{
							delBuNx.center = self.findMidPtButton2D(pp5.pt,pp5.nx.pt)
						}

                    } else if (respItem.wasArch) {

                        let ratiox =  (pp2.pt.x - pp1.pt.x) / (pp3.pt.x - pp1.pt.x)
                        let posy =  ((pp3.pt.y - pp1.pt.y) * ratiox) + pp1.pt.y
                        let dify =  min(pp2.pt.y - posy,0.0)

                        respItem.v1 = CIVector(x: pp1.pt.x, y: refImage.size.height-(pp1.pt.y + dify))
                        respItem.v2 = CIVector(x: pp3.pt.x, y: refImage.size.height-(pp3.pt.y + dify))
                        respItem.v3 = CIVector(x: pp4.pt.x, y: refImage.size.height-pp4.pt.y)
                        respItem.v4 = CIVector(x: pp5.pt.x, y: refImage.size.height-pp5.pt.y)
                        respItem.path.removeAllPoints()

                        var theArc:Arctype = Arctype()
                        theArc.bpoint = Apoint(Float(pp1.pt.x), Float(pp1.pt.y))
                        theArc.mpoint = Apoint(Float(pp2.pt.x), Float(pp2.pt.y))
                        theArc.epoint = Apoint(Float(pp3.pt.x), Float(pp3.pt.y))
                        respItem.path = PECperspcCorrect.drawArc(theArc)
                        respItem.path.addLine(to:pp3.pt)
                        respItem.path.addLine(to:pp4.pt)
                        respItem.path.addLine(to:pp5.pt)
                        respItem.path.close()

                        polyPath.append(respItem.path)

                        let topPath2 = UIBezierPath.init()
                        topPath2.move(to: CGPoint.init(x:CGFloat(respItem.v1.x), y: CGFloat(pp1.pt.y + dify)))
                        topPath2.addLine(to: CGPoint.init(x:CGFloat(respItem.v2.x), y: CGFloat(pp3.pt.y + dify)))
                        topPath2.addLine(to: CGPoint.init(x:CGFloat(respItem.v3.x), y: CGFloat(pp4.pt.y)))
                        topPath2.addLine(to: CGPoint.init(x:CGFloat(respItem.v4.x), y: CGFloat(pp5.pt.y)))
                        topPath2.close()
                        topPath2.append(respItem.path)

                        respItem.rect = topPath2.cgPath.boundingBox

						if let lockButton = respItem.p1.button.lockLinkFirst
						{
							lockButton.topLeft = pp1.pt;
							lockButton.botRght = pp4.pt;
							lockButton.findMidPt(lockButton.topLeft,lockButton.botRght);
						}

						if let sizeButton = pp4.button.showhideLt
						{
							sizeButton.center = pp4.pt;
							sizeButton.center.x -= sufaceButtonRadius*0.75;
							sizeButton.center.y -= sufaceButtonRadius*0.75;
						}

						if let delBuNx = pp4.button.delButtonNx
						{
							delBuNx.center = self.findMidPtButton2D(pp4.pt,pp4.nx.pt)
						}

                    } else {

                        respItem.v1 = CIVector(x: pp1.pt.x, y: refImage.size.height-pp1.pt.y)
                        respItem.v2 = CIVector(x: pp2.pt.x, y: refImage.size.height-pp2.pt.y)
                        respItem.v3 = CIVector(x: pp3.pt.x, y: refImage.size.height-pp3.pt.y)
                        respItem.v4 = CIVector(x: pp4.pt.x, y: refImage.size.height-pp4.pt.y)

                        respItem.path.removeAllPoints()
                        respItem.path.move(to:pp1.pt)
                        respItem.path.addLine(to:pp2.pt)
                        respItem.path.addLine(to:pp3.pt)
                        respItem.path.addLine(to:pp4.pt)
                        respItem.path.close()

                        respItem.rect = respItem.path.cgPath.boundingBox

						if let lockButton = respItem.p1.button.lockLinkFirst
						{
							lockButton.topLeft = pp1.pt;
							lockButton.botRght = pp3.pt;
							lockButton.findMidPt(lockButton.topLeft,lockButton.botRght);
						}

						if let sizeButton = pp3.button.showhideLt
						{
							sizeButton.center = pp3.pt;
							sizeButton.center.x -= sufaceButtonRadius*0.75;
							sizeButton.center.y -= sufaceButtonRadius*0.75;
						}

						if let delBuNx = pp3.button.delButtonNx
						{
							delBuNx.center = self.findMidPtButton2D(pp3.pt,pp3.nx.pt)
						}

                        polyPath.append(respItem.path)
                    }

                }
            }

            polyLayer.path = nil
            polyLayer.path = polyPath.cgPath
            polyLayer.opacity = 1.0

			if (dragSingle)
			{
				self.reDrawDoor()
//			} else if (dragS quare) {
//				self.reDrawSquareDoor()
			} else {
				self.reDrawAllDoors()
			}
        }
    }

    //---------------------------------------------------------------------
    func editDoorObjectAt() //*************** change 0.0.15 replace whole function ***
    {
        if let ho = houseObject
        {
            for resp in ho.response
            {
                for respItem in resp
                {
                    if ( !respItem.wasGoodDr) { continue; }
                    self.doorPathsSet = true;
//print("respItem.wasGoodDr  :",respItem.wasGoodDr)

//					var lock = false;
//                    if (respItem.wasLocked)
//                    {
//                    	lock = true;
//					}

                    if (respItem.wasPoly)
                    {
                        let theButtonV1:PECButton = self.cornerButton.pecDuplicate()!
                        self.scrollView.imageViewOverlay.addSubview(theButtonV1)

                        theButtonV1.center = respItem.p1.pt;
                        theButtonV1.ppt = respItem.p1;
                        theButtonV1.ppt.button = theButtonV1;
                        theButtonV1.respItem = respItem;
                        theButtonV1.topLPt = true;
                        theButtonV1.isSelected = false;
                        polyPath.move(to:theButtonV1.center);

                        let theButtonV2:PECButton = self.cornerButton.pecDuplicate()!
                        self.scrollView.imageViewOverlay.addSubview(theButtonV2)

                        theButtonV2.center = respItem.p2.pt;
                        theButtonV2.ppt = respItem.p2;
                        theButtonV2.ppt.button = theButtonV2;
                        theButtonV2.deleteCornerRecog.isEnabled = true;
                        theButtonV2.respItem = respItem;
						theButtonV2.isSelected = false;
                       polyPath.addLine(to:theButtonV2.center);

                        let theButtonV3:PECButton = self.cornerButton.pecDuplicate()!
                        self.scrollView.imageViewOverlay.addSubview(theButtonV3)

                        theButtonV3.center = respItem.p3.pt;
                        theButtonV3.ppt = respItem.p3;
                        theButtonV3.ppt.button = theButtonV3;
                        theButtonV3.deleteCornerRecog.isEnabled = true;
                        theButtonV3.respItem = respItem;
						theButtonV3.isSelected = false;
                        polyPath.addLine(to:theButtonV3.center);

                        let theButtonV4:PECButton = self.cornerButton.pecDuplicate()!
                        self.scrollView.imageViewOverlay.addSubview(theButtonV4)

                        theButtonV4.center = respItem.p4.pt;
                        theButtonV4.ppt = respItem.p4;
                        theButtonV4.ppt.button = theButtonV4;
                        theButtonV4.respItem = respItem;
                        theButtonV4.topRPt = true;
						theButtonV4.isSelected = false;
                        polyPath.addLine(to:theButtonV4.center);

                        let theButtonV5:PECButton = self.cornerButton.pecDuplicate()!
                        self.scrollView.imageViewOverlay.addSubview(theButtonV5)

                        theButtonV5.center = respItem.p5.pt;
                        theButtonV5.ppt = respItem.p5;
                        theButtonV5.ppt.button = theButtonV5;
                        theButtonV5.respItem = respItem;
                        theButtonV5.botRPt = true;
						theButtonV5.isSelected = false;
//                        theButtonV5.isForSizing = true;
//                        theButtonV5.setBackgroundImage(UIImage(named: "lockSizing.png"), for: .selected);
                        polyPath.addLine(to:theButtonV5.center);

                        let theButtonV6:PECButton = self.cornerButton.pecDuplicate()!
                        self.scrollView.imageViewOverlay.addSubview(theButtonV6)

                        theButtonV6.center = respItem.p6.pt;
                        theButtonV6.ppt = respItem.p6;
                        theButtonV6.ppt.button = theButtonV6;
                        theButtonV6.respItem = respItem;
                        theButtonV6.botLPt = true;
						theButtonV6.isSelected = false;
                        polyPath.addLine(to:theButtonV6.center);
                        polyPath.close();


						//------------- delete button -----------------
                        let deleteButton:UIButton = self.deleteButton.duplicate()!
                        self.scrollView.imageViewOverlay.addSubview(deleteButton)

                        var bottom:Bline = Bline.init();
                        bottom.spoint = theButtonV5.center;
                        bottom.epoint = theButtonV6.center;

                        deleteButton.center = self.findMidPt2D(bottom);
                        deleteButton.isHidden = false;
						deleteButton.isSelected = false;

                        theButtonV5.delButtonNx = deleteButton;
                        theButtonV6.delButtonLt = deleteButton;
                        respItem.delButton = deleteButton;


						//------------- lock button -----------------
						let lockButton:PECButton = self.cornerButton.pecDuplicate()!
						lockButton.setBackgroundImage(UIImage(named: "lockArea.png"), for: .normal);
						lockButton.setBackgroundImage(UIImage(named: "lockArea.png"), for: .highlighted);
						lockButton.tag = 14;
						self.scrollView.imageViewOverlay.addSubview(lockButton)

						var diag:Bline = Bline.init();
						diag.spoint = theButtonV1.center;
						diag.epoint = theButtonV5.center;

						lockButton.center = self.findMidPt2D(diag);
						lockButton.isHidden = false;
						lockButton.isForPaint = false;
						lockButton.isLock = true;
						lockButton.ppt = Ppoint.init(pt:lockButton.center);
						lockButton.respItem = respItem;
						respItem.lockButton = lockButton;
						lockButton.topLeft = theButtonV1.center;
						lockButton.botRght = theButtonV5.center;

						lockButton.lockLinkFirst = theButtonV1;
						theButtonV1.lockLinkFirst = lockButton;


						//------------- sizing button -----------------
						let sizeButton:PECButton = self.cornerButton.pecDuplicate()!
						sizeButton.setBackgroundImage(UIImage(named: "lockSizing.png"), for: .normal);
						sizeButton.setBackgroundImage(UIImage(named: "lockSizing.png"), for: .highlighted);
						sizeButton.tag = 15;
						self.scrollView.imageViewOverlay.addSubview(sizeButton)

						sizeButton.center = theButtonV5.center;
						sizeButton.center.x -= theButtonV5.frame.size.width;
						sizeButton.center.y -= theButtonV5.frame.size.height;

						sizeButton.isHidden = false;
						sizeButton.isForPaint = false;
						sizeButton.isForSizing = true;
						sizeButton.ppt = Ppoint.init(pt:sizeButton.center);
						sizeButton.respItem = respItem;

						sizeButton.showhideLt = theButtonV5;
						theButtonV5.showhideLt = sizeButton;

                    } else if (respItem.wasArch) {

                        let theButtonV1:PECButton = self.cornerButton.pecDuplicate()!
                        self.scrollView.imageViewOverlay.addSubview(theButtonV1)

                        theButtonV1.center = respItem.p1.pt;
                        theButtonV1.ppt = respItem.p1;
                        theButtonV1.ppt.button = theButtonV1;
                        theButtonV1.respItem = respItem;
                        theButtonV1.topLPt = true;
						theButtonV1.isSelected = false;

                        let theButtonV2:PECButton = self.cornerButton.pecDuplicate()!
                        self.scrollView.imageViewOverlay.addSubview(theButtonV2)

                        theButtonV2.center = respItem.p2.pt;
                        theButtonV2.ppt = respItem.p2;
                        theButtonV2.ppt.button = theButtonV2;
                        theButtonV2.deleteCornerRecog.isEnabled = true;
                        theButtonV2.respItem = respItem;
						theButtonV2.isSelected = false;

                        let theButtonV3:PECButton = self.cornerButton.pecDuplicate()!
                        self.scrollView.imageViewOverlay.addSubview(theButtonV3)

                        theButtonV3.center = respItem.p3.pt;
                        theButtonV3.ppt = respItem.p3;
                        theButtonV3.ppt.button = theButtonV3;
                        theButtonV3.respItem = respItem;
                        theButtonV3.topRPt = true;
						theButtonV3.isSelected = false;

                        let theButtonV4:PECButton = self.cornerButton.pecDuplicate()!
                        self.scrollView.imageViewOverlay.addSubview(theButtonV4)

                        theButtonV4.center = respItem.p4.pt;
                        theButtonV4.ppt = respItem.p4;
                        theButtonV4.ppt.button = theButtonV4;
                        theButtonV4.respItem = respItem;
                        theButtonV4.botRPt = true;
//                        theButtonV4.isForSizing = true;
//                        theButtonV4.setBackgroundImage(UIImage(named: "lockSizing.png"), for: .selected);
						theButtonV4.isSelected = false;

                        let theButtonV5:PECButton = self.cornerButton.pecDuplicate()!
                        self.scrollView.imageViewOverlay.addSubview(theButtonV5)

                        theButtonV5.center = respItem.p5.pt;
                        theButtonV5.ppt = respItem.p5;
                        theButtonV5.ppt.button = theButtonV5;
                        theButtonV5.ppt.nx = respItem.p1;
                        theButtonV1.ppt.lt = respItem.p5;
                        theButtonV5.respItem = respItem;
                        theButtonV5.botLPt = true;
						theButtonV5.isSelected = false;

                        polyPath.append(respItem.path);


						//------------- delete button -----------------
                        let deleteButton:UIButton = self.deleteButton.duplicate()!
                        self.scrollView.imageViewOverlay.addSubview(deleteButton)

                        var bottom:Bline = Bline.init();
                        bottom.spoint = theButtonV4.center;
                        bottom.epoint = theButtonV5.center;

                        deleteButton.center = self.findMidPt2D(bottom);
                        deleteButton.isHidden = false;
						deleteButton.isSelected = false;

                        theButtonV4.delButtonNx = deleteButton;
                        theButtonV5.delButtonLt = deleteButton;
                        respItem.delButton = deleteButton;


						//------------- lock button -----------------
						let lockButton:PECButton = self.cornerButton.pecDuplicate()!
						lockButton.setBackgroundImage(UIImage(named: "lockArea.png"), for: .normal);
						lockButton.setBackgroundImage(UIImage(named: "lockArea.png"), for: .highlighted);
						lockButton.tag = 14;
						self.scrollView.imageViewOverlay.addSubview(lockButton)

						var diag:Bline = Bline.init();
						diag.spoint = theButtonV1.center;
						diag.epoint = theButtonV4.center;

						lockButton.center = self.findMidPt2D(diag);
						lockButton.isHidden = false;
						lockButton.isForPaint = false;
						lockButton.isLock = true;
						lockButton.ppt = Ppoint.init(pt:lockButton.center);
						lockButton.respItem = respItem;
						respItem.lockButton = lockButton;
						lockButton.topLeft = theButtonV1.center;
						lockButton.botRght = theButtonV4.center;

						lockButton.lockLinkFirst = theButtonV1;
						theButtonV1.lockLinkFirst = lockButton;


						//------------- sizing button -----------------
						let sizeButton:PECButton = self.cornerButton.pecDuplicate()!
						sizeButton.setBackgroundImage(UIImage(named: "lockSizing.png"), for: .normal);
						sizeButton.setBackgroundImage(UIImage(named: "lockSizing.png"), for: .highlighted);
						sizeButton.tag = 15;
						self.scrollView.imageViewOverlay.addSubview(sizeButton)

						sizeButton.center = theButtonV4.center;
						sizeButton.center.x -= theButtonV4.frame.size.width;
						sizeButton.center.y -= theButtonV4.frame.size.height;

						sizeButton.isHidden = false;
						sizeButton.isForPaint = false;
						sizeButton.isForSizing = true;
						sizeButton.ppt = Ppoint.init(pt:sizeButton.center);
						sizeButton.respItem = respItem;

						sizeButton.showhideLt = theButtonV4;
						theButtonV4.showhideLt = sizeButton;

                    } else {
                        let theButtonV1:PECButton = self.cornerButton.pecDuplicate()!
                        self.scrollView.imageViewOverlay.addSubview(theButtonV1)

                        theButtonV1.center = respItem.p1.pt;
                        theButtonV1.ppt = respItem.p1;
                        theButtonV1.ppt.button = theButtonV1;
                        theButtonV1.respItem = respItem;
                        theButtonV1.topLPt = true;
 						theButtonV1.isSelected = false;
                       polyPath.move(to:theButtonV1.center);

                        let theButtonV2:PECButton = self.cornerButton.pecDuplicate()!
                        self.scrollView.imageViewOverlay.addSubview(theButtonV2)

                        theButtonV2.center = respItem.p2.pt;
                        theButtonV2.ppt = respItem.p2;
                        theButtonV2.ppt.button = theButtonV2;
                        theButtonV2.respItem = respItem;
                        theButtonV2.topRPt = true;
						theButtonV2.isSelected = false;
                        polyPath.addLine(to:theButtonV2.center);

                        let theButtonV3:PECButton = self.cornerButton.pecDuplicate()!
                        self.scrollView.imageViewOverlay.addSubview(theButtonV3)

                        theButtonV3.center = respItem.p3.pt;
                        theButtonV3.ppt = respItem.p3;
                        theButtonV3.ppt.button = theButtonV3;
                        theButtonV3.respItem = respItem;
                        theButtonV3.botRPt = true;
//                        theButtonV3.isForSizing = true;
//                        theButtonV3.setBackgroundImage(UIImage(named: "lockSizing.png"), for: .selected);
						theButtonV3.isSelected = false;
                        polyPath.addLine(to:theButtonV3.center);

                        let theButtonV4:PECButton = self.cornerButton.pecDuplicate()!
                        self.scrollView.imageViewOverlay.addSubview(theButtonV4)

                        theButtonV4.center = respItem.p4.pt;
                        theButtonV4.ppt = respItem.p4;
                        theButtonV4.ppt.button = theButtonV4;
                        theButtonV4.ppt.nx = respItem.p1;
                        theButtonV1.ppt.lt = respItem.p4;
                        theButtonV4.respItem = respItem;
                        theButtonV4.botLPt = true;
						theButtonV4.isSelected = false;
                        polyPath.addLine(to:theButtonV4.center);
                        polyPath.close();


						//------------- delete button -----------------
						let deleteButton:UIButton = self.deleteButton.duplicate()!
						deleteButton.tag = 10;
						self.scrollView.imageViewOverlay.addSubview(deleteButton)

						var bottom:Bline = Bline.init();
						bottom.spoint = theButtonV3.center;
						bottom.epoint = theButtonV4.center;

						deleteButton.center = self.findMidPt2D(bottom);
						deleteButton.isHidden = false;
						deleteButton.isSelected = false;

						theButtonV3.delButtonNx = deleteButton;
						theButtonV4.delButtonLt = deleteButton;
						respItem.delButton = deleteButton;


						//------------- lock button -----------------
						let lockButton:PECButton = self.cornerButton.pecDuplicate()!
						lockButton.setBackgroundImage(UIImage(named: "lockArea.png"), for: .normal);
						lockButton.setBackgroundImage(UIImage(named: "lockArea.png"), for: .highlighted);
						lockButton.tag = 14;
						self.scrollView.imageViewOverlay.addSubview(lockButton)

						var diag:Bline = Bline.init();
						diag.spoint = theButtonV1.center;
						diag.epoint = theButtonV3.center;

						lockButton.center = self.findMidPt2D(diag);
						lockButton.isHidden = false;
						lockButton.isForPaint = false;
						lockButton.isLock = true;
						lockButton.ppt = Ppoint.init(pt:lockButton.center);
						lockButton.respItem = respItem;
						respItem.lockButton = lockButton;
						lockButton.topLeft = theButtonV1.center;
						lockButton.botRght = theButtonV3.center;

						lockButton.lockLinkFirst = theButtonV1;
						theButtonV1.lockLinkFirst = lockButton;


						//------------- sizing button -----------------
						let sizeButton:PECButton = self.cornerButton.pecDuplicate()!
						sizeButton.setBackgroundImage(UIImage(named: "lockSizing.png"), for: .normal);
						sizeButton.setBackgroundImage(UIImage(named: "lockSizing.png"), for: .highlighted);
						sizeButton.tag = 15;
						self.scrollView.imageViewOverlay.addSubview(sizeButton)

						sizeButton.center = theButtonV3.center;
						sizeButton.center.x -= theButtonV3.frame.size.width;
						sizeButton.center.y -= theButtonV3.frame.size.height;

						sizeButton.isHidden = false;
						sizeButton.isForPaint = false;
						sizeButton.isForSizing = true;
						sizeButton.ppt = Ppoint.init(pt:sizeButton.center);
						sizeButton.respItem = respItem;

						sizeButton.showhideLt = theButtonV3;
						theButtonV3.showhideLt = sizeButton;
                    }
			   }
           }

            polyLayer.path = nil;
            polyLayer.path = polyPath.cgPath;
        }
    }


	//---------------------------------------------------------------------
	func killBadDoors()
	{
//print("killB adDoors")
//if let resp = self.house Object?.response
//{
////print("  ")
////print("  ")
//for responseArray in resp
//{
////print("00 responseArray:",responseArray)
//}
//}


		if let ho = houseObject
		{
			guard let refImage = originalHomeImage else { return }
			var kount = ho.response.count;
			for resp in ho.response.reversed()
			{
                kount -= 1;
//print("kount",kount)

				for respItem in resp
				{
//print("respItem",respItem)
					if (respItem.p1.pt.y > refImage.size.height)
					{
//print("pp1.pt.y > refImage.size.height",respI tem.p1.pt.y,refImage.size.height)
							ho.response.remove(at: kount);
//if let resp = self.house Object?.response
//{
////print("  ")
////print("  ")
//for responseArray in resp
//{
////print("11 responseArray:",responseArray)
//}
//}
					}
			   }
		   }
		}
	}


    //---------------------------------------------------------------------
    func reDrawDoor()
	{
//print("reDraw Door")
//print("reDraw Door twoCa rImgae:",twoCarImgae as Any)
//print("reDraw Door oneDo orImgae:",oneDoorImgae as Any)

		if let ho = houseObject
		{
			guard let refImage = originalHomeImage else { return }

			var newReferenceImage = UIImage();
			let hostRect = CGRect.init(x: 0, y: 0, width: refImage.size.width, height: refImage.size.height)
			let context0:CIContext = CIContext(options: nil)

			for resp in ho.response
			{
				for respItem in resp
				{
                    if ( !respItem.wasGoodDr) {continue;}
					if (respItem.classIDS[0] != -88) {continue;}

					var replacement: UIImage =  UIImage(imageLiteralResourceName: "transparent")
					var lbl = "nothing"
					if (respItem.labels.count > 0)
					{
						lbl = respItem.labels[0]
					}
//print("reDraw Door :",respItem.class IDS[0])

					let gtype:GarageTypes = HouseObject.getGarageType(label: lbl) ?? .somethingNotInList
//print("reDraw Door twoDo orImgae,gtype:",twoDoorImgae as Any,gtype)

					switch (gtype)
					{
//					case .oneCar:
//						replacement = oneCarImgae ?? UIImage(imageLiteralResourceName: "transparent")
//					case .twoCar:
//						replacement = twoCarImgae ?? UIImage(imageLiteralResourceName: "transparent")
//					case .onefrontDoor:
//						replacement = oneDoorImgae ?? UIImage(imageLiteralResourceName: "transparent")
//					case .twofrontDoor:
//						replacement = twoDoorImgae ?? UIImage(imageLiteralResourceName: "transparent")

					case .oneCar:
						replacement = oneCarImgae ?? UIImage(imageLiteralResourceName: "1_s0.jpg")
					case .twoCar:
						replacement = twoCarImgae ?? UIImage(imageLiteralResourceName: "1_d0.jpg")
					case .onefrontDoor:
						replacement = oneDoorImgae ?? UIImage(imageLiteralResourceName: "f_s0")
					case .twofrontDoor:
						replacement = twoDoorImgae ?? UIImage(imageLiteralResourceName: "f_d10.jpg")



					case .window1:
						replacement = twoDoorImgae ?? UIImage(imageLiteralResourceName: "transparent")
					case .somethingNotInList:
						replacement = UIImage(imageLiteralResourceName: "transparent")
					}

//print("reDraw Door replacement:",replacement as Any)

					 //perspective warping of Door image using CIFilter,
					 if let perpFilter = CIFilter(name: "CIPerspectiveTransform")
					 {
						 let beginImage = CIImage(image: replacement)
						 perpFilter.setValue(beginImage, forKey:kCIInputImageKey)

						 perpFilter.setValue(respItem.v1, forKey:"inputTopLeft")
						 perpFilter.setValue(respItem.v2, forKey:"inputTopRight")
						 perpFilter.setValue(respItem.v3, forKey:"inputBottomRight")
						 perpFilter.setValue(respItem.v4, forKey:"inputBottomLeft")

						 if let output = perpFilter.outputImage
						 {
							 if let cgimg = context0.createCGImage(output, from: output.extent)
							 {
								 let processedImage = UIImage(cgImage: cgimg)

								if (respItem.wasPoly)
								{
									UIGraphicsBeginImageContext(CGSize.init(width: hostRect.size.width, height: hostRect.size.height))
									guard let context = UIGraphicsGetCurrentContext() else { return }
									newReferenceImage.draw(in: hostRect)

									context.addPath(respItem.path.cgPath)
									context.clip(using: .evenOdd)

									processedImage.draw(in: respItem.rect, blendMode: .normal, alpha: 1.0)

									newReferenceImage = UIGraphicsGetImageFromCurrentImageContext()!
									UIGraphicsEndImageContext()

								} else if (respItem.wasArch) {

									UIGraphicsBeginImageContext(CGSize.init(width: hostRect.size.width, height: hostRect.size.height))
									guard let context = UIGraphicsGetCurrentContext() else { return }
									newReferenceImage.draw(in: hostRect)

									context.addPath(respItem.path.cgPath)
									context.clip(using: .evenOdd)

									processedImage.draw(in: respItem.rect, blendMode: .normal, alpha: 1.0)

									newReferenceImage = UIGraphicsGetImageFromCurrentImageContext()!
									UIGraphicsEndImageContext()

								} else {

									UIGraphicsBeginImageContext(CGSize.init(width: hostRect.size.width, height: hostRect.size.height))
									newReferenceImage.draw(in: hostRect)
									processedImage.draw(in: respItem.rect, blendMode: .normal, alpha: 1.0)
									newReferenceImage = UIGraphicsGetImageFromCurrentImageContext()!
									UIGraphicsEndImageContext()
								}
							}
						}
					}
				}
			}

			DispatchQueue.main.async
			{
				UIGraphicsBeginImageContext(CGSize.init(width: hostRect.size.width, height: hostRect.size.height))
				self.baseReferenceImage.draw(in: hostRect)
				newReferenceImage.draw(in: hostRect)
				newReferenceImage = UIGraphicsGetImageFromCurrentImageContext()!
				UIGraphicsEndImageContext()

				self.scrollView.garageImageView.image = newReferenceImage
        		maskedDoorImage = newReferenceImage  //***************** change 0.0.30 add ***
			}
		}
	}

    //---------------------------------------------------------------------
    func reDrawBaseRef()
    {
//print(" ")
//print(" ")
//print("reDraw Base Ref")
//print("reDraw Base Ref twoCa rImgae:",twoCarImgae as Any)
//print("reDraw Base Ref oneDo orImgae:",oneDoorImgae as Any)

        if let ho = houseObject
        {
            guard let refImage = originalHomeImage else { return }

            var newReferenceImage = UIImage();
            self.baseReferenceImage = UIImage();
            let hostRect = CGRect.init(x: 0, y: 0, width: refImage.size.width, height: refImage.size.height)
			let context0:CIContext = CIContext(options: nil)

            for resp in ho.response
            {
//print("resp.count  :",resp.count)
                for respItem in resp
                {
                    if ( !respItem.wasGoodDr) {continue;}
//print("respItem.wasGoodDr  :",respItem.wasGoodDr)
//print("respItem.labels  :",respItem.labels)
				//	if (dragSq uare && respItem.classIDS[0] == -99) {continue;}
					if (dragSingle && respItem.classIDS[0] == -88) {continue;}



					var replacement: UIImage =  UIImage(imageLiteralResourceName: "transparent")
					var lbl = "nothing"
					if (respItem.labels.count > 0)
					{
						lbl = respItem.labels[0]
					}
//print("reDraw Base Ref res p[0].class IDS[0], dragSqu are,dragSin gle:",res p[0].class IDS[0],dragSqu are,dragSi ngle)

			  //      res p[0].class IDS[0] = 5
					let gtype:GarageTypes = HouseObject.getGarageType(label: lbl) ?? .somethingNotInList
//print("reDraw Base Ref twoDo orImgae,gtype:",twoDoorImgae as Any,gtype)

					switch (gtype)
					{
//					case .oneCar:
//						replacement = oneCarImgae ?? UIImage(imageLiteralResourceName: "transparent")
//					case .twoCar:
//						replacement = twoCarImgae ?? UIImage(imageLiteralResourceName: "transparent")
//					case .onefrontDoor:
//						replacement = oneDoorImgae ?? UIImage(imageLiteralResourceName: "transparent")
//					case .twofrontDoor:
//						replacement = twoDoorImgae ?? UIImage(imageLiteralResourceName: "transparent")

					case .oneCar:
						replacement = oneCarImgae ?? UIImage(imageLiteralResourceName: "1_s0.jpg")
					case .twoCar:
						replacement = twoCarImgae ?? UIImage(imageLiteralResourceName: "1_d0.jpg")
					case .onefrontDoor:
						replacement = oneDoorImgae ?? UIImage(imageLiteralResourceName: "f_s0")
					case .twofrontDoor:
						replacement = twoDoorImgae ?? UIImage(imageLiteralResourceName: "f_d10.jpg")



					case .window1:
						replacement = twoDoorImgae ?? UIImage(imageLiteralResourceName: "transparent")
					case .somethingNotInList:
						replacement = UIImage(imageLiteralResourceName: "transparent")
					}

//print("reDraw Base Ref replacement:",replacement as Any)

					 //perspective warping of Door image using CIFilter,
					 if let perpFilter = CIFilter(name: "CIPerspectiveTransform")
					 {
						 let beginImage = CIImage(image: replacement)
						 perpFilter.setValue(beginImage, forKey:kCIInputImageKey)

						 perpFilter.setValue(respItem.v1, forKey:"inputTopLeft")
						 perpFilter.setValue(respItem.v2, forKey:"inputTopRight")
						 perpFilter.setValue(respItem.v3, forKey:"inputBottomRight")
						 perpFilter.setValue(respItem.v4, forKey:"inputBottomLeft")

						 if let output = perpFilter.outputImage
						 {
							 if let cgimg = context0.createCGImage(output, from: output.extent)
							 {
								 let processedImage = UIImage(cgImage: cgimg)

								if (respItem.wasPoly)
								{
									UIGraphicsBeginImageContext(CGSize.init(width: hostRect.size.width, height: hostRect.size.height))
									guard let context = UIGraphicsGetCurrentContext() else { return }
									newReferenceImage.draw(in: hostRect)

									context.addPath(respItem.path.cgPath)
									context.clip(using: .evenOdd)

									processedImage.draw(in: respItem.rect, blendMode: .normal, alpha: 1.0)

									newReferenceImage = UIGraphicsGetImageFromCurrentImageContext()!
									UIGraphicsEndImageContext()

								} else if (respItem.wasArch) {

									UIGraphicsBeginImageContext(CGSize.init(width: hostRect.size.width, height: hostRect.size.height))
									guard let context = UIGraphicsGetCurrentContext() else { return }
									newReferenceImage.draw(in: hostRect)

									context.addPath(respItem.path.cgPath)
									context.clip(using: .evenOdd)

									processedImage.draw(in: respItem.rect, blendMode: .normal, alpha: 1.0)

									newReferenceImage = UIGraphicsGetImageFromCurrentImageContext()!
									UIGraphicsEndImageContext()

								} else {

									UIGraphicsBeginImageContext(CGSize.init(width: hostRect.size.width, height: hostRect.size.height))
									newReferenceImage.draw(in: hostRect)
									processedImage.draw(in: respItem.rect, blendMode: .normal, alpha: 1.0)
									newReferenceImage = UIGraphicsGetImageFromCurrentImageContext()!
									UIGraphicsEndImageContext()
								}
                			}
                		}
                	}
                }
            }

            DispatchQueue.main.async
			{
				self.baseReferenceImage = newReferenceImage
            }
        }
    }

    //---------------------------------------------------------------------
    func reSetAllDoors()
    {
//print("reSet All Doors")
        if let ho = houseObject
        {
            for resp in ho.response
            {
                for respItem in resp
                {
                    if ( !respItem.wasGoodDr) {continue;}
                	respItem.classIDS[0] = 5;
                }
			}
		}
	}

    //---------------------------------------------------------------------
    func reDrawAllDoors()
    {
//print("reDraw All Doors")
//print("reDraw All Doors twoCa rImgae:",twoCarImgae as Any)
//print("reDraw All Doors oneDo orImgae:",oneDoorImgae as Any)

        if let ho = houseObject
        {
            guard let refImage = originalHomeImage else { return }

            var newReferenceImage = UIImage();
            let hostRect = CGRect.init(x: 0, y: 0, width: refImage.size.width, height: refImage.size.height)
			let context0:CIContext = CIContext(options: nil)

            for resp in ho.response
            {
                for respItem in resp
                {
                    if ( !respItem.wasGoodDr) {continue;}

					var replacement: UIImage =  UIImage(imageLiteralResourceName: "transparent")
					var lbl = "nothing"
					if (respItem.labels.count > 0)
					{
						lbl = respItem.labels[0]
					}
					let gtype:GarageTypes = HouseObject.getGarageType(label: lbl) ?? .somethingNotInList
//print("reDraw All Doors twoDo orImgae,gtype:",twoDoorImgae as Any,gtype)
//print("reDraw All Doors respItem.wasArch:",respItem.wasArch)

					switch (gtype)
					{
//					case .oneCar:
//						replacement = oneCarImgae ?? UIImage(imageLiteralResourceName: "transparent")
//					case .twoCar:
//						replacement = twoCarImgae ?? UIImage(imageLiteralResourceName: "transparent")
//					case .onefrontDoor:
//						replacement = oneDoorImgae ?? UIImage(imageLiteralResourceName: "transparent")
//					case .twofrontDoor:
//						replacement = twoDoorImgae ?? UIImage(imageLiteralResourceName: "transparent")

					case .oneCar:
						replacement = oneCarImgae ?? UIImage(imageLiteralResourceName: "1_s0.jpg")
					case .twoCar:
						replacement = twoCarImgae ?? UIImage(imageLiteralResourceName: "1_d0.jpg")
					case .onefrontDoor:
						replacement = oneDoorImgae ?? UIImage(imageLiteralResourceName: "f_s0.jpg")
					case .twofrontDoor:
						replacement = twoDoorImgae ?? UIImage(imageLiteralResourceName: "f_d10.jpg")



					case .window1:
						replacement = twoDoorImgae ?? UIImage(imageLiteralResourceName: "transparent")
					case .somethingNotInList:
						replacement = UIImage(imageLiteralResourceName: "transparent")
					}

//print("reDraw All Doors replacement:",replacement as Any)

					 //perspective warping of Door image using CIFilter,
					 if let perpFilter = CIFilter(name: "CIPerspectiveTransform")
					 {
						 let beginImage = CIImage(image: replacement)
						 perpFilter.setValue(beginImage, forKey:kCIInputImageKey)

						 perpFilter.setValue(respItem.v1, forKey:"inputTopLeft")
						 perpFilter.setValue(respItem.v2, forKey:"inputTopRight")
						 perpFilter.setValue(respItem.v3, forKey:"inputBottomRight")
						 perpFilter.setValue(respItem.v4, forKey:"inputBottomLeft")

//print("reDraw All Doors respItem.v1:",respItem.v1 as Any)
//print("reDraw All Doors respItem.v2:",respItem.v2 as Any)
//print("reDraw All Doors respItem.v3:",respItem.v3 as Any)
//print("reDraw All Doors respItem.v4:",respItem.v4 as Any)

//print("reDraw All Doors perpFilter.outputImage:",perpFilter.outputImage as Any)
						 if let output = perpFilter.outputImage
						 {
							 if let cgimg = context0.createCGImage(output, from: output.extent)
							 {
								 let processedImage = UIImage(cgImage: cgimg)
//print("reDraw All Doors proces sedImage:",proces sedImage as Any)

								if (respItem.wasPoly)
								{
									UIGraphicsBeginImageContext(CGSize.init(width: hostRect.size.width, height: hostRect.size.height))
									guard let context = UIGraphicsGetCurrentContext() else { return }
									newReferenceImage.draw(in: hostRect)

									context.addPath(respItem.path.cgPath)
									context.clip(using: .evenOdd)

									processedImage.draw(in: respItem.rect, blendMode: .normal, alpha: 1.0)

									newReferenceImage = UIGraphicsGetImageFromCurrentImageContext()!
									UIGraphicsEndImageContext()

								} else if (respItem.wasArch) {

//print("1 reDraw All Doors hostR ect:",hostR ect)
									UIGraphicsBeginImageContext(CGSize.init(width: hostRect.size.width, height: hostRect.size.height))
									guard let context = UIGraphicsGetCurrentContext() else { return }
									newReferenceImage.draw(in: hostRect)
//print("reDraw All Doors respItem.path:",respItem.path)
//print("reDraw All Doors respItem.rect:",respItem.rect)

									context.addPath(respItem.path.cgPath)
									context.clip(using: .evenOdd)

									processedImage.draw(in: respItem.rect, blendMode: .normal, alpha: 1.0)

									newReferenceImage = UIGraphicsGetImageFromCurrentImageContext()!
									UIGraphicsEndImageContext()
//print("reDraw All Doors newReferenceImage:",newReferenceImage)

								} else {

									UIGraphicsBeginImageContext(CGSize.init(width: hostRect.size.width, height: hostRect.size.height))
									newReferenceImage.draw(in: hostRect)
									processedImage.draw(in: respItem.rect, blendMode: .normal, alpha: 1.0)
									newReferenceImage = UIGraphicsGetImageFromCurrentImageContext()!
									UIGraphicsEndImageContext()
								}
                			}
                		}
                	}
                }
            }
            
            DispatchQueue.main.async
			{
				self.scrollView.garageImageView.image = newReferenceImage
        		maskedDoorImage = newReferenceImage  //***************** change 0.0.30 add ***
            }
        }
    }

    // MARK: - math
    //----------------------------------------------------------------------------------------------------------
    func findDistance2Dcc(_ pt1:CGPoint, _ pt2:CGPoint) -> CGFloat
    {
        return sqrt(((pt2.x - pt1.x) * (pt2.x - pt1.x)) + ((pt2.y - pt1.y) * (pt2.y - pt1.y)))
    }
    
    //-----------------------------------------------------------------------------------------------------------------------
    func findMidPt2D(_ line1:Bline) -> CGPoint
    {
        let midpoint:CGPoint = CGPoint(x:((line1.epoint.x - line1.spoint.x) * 0.5) + line1.spoint.x,
                                       y:((line1.epoint.y - line1.spoint.y) * 0.5) + line1.spoint.y)
        return midpoint
    }

    //--------------------------------------------------------------------------------------------
    func findMidPtButton2D(_ spoint:CGPoint, _ epoint:CGPoint) -> CGPoint
    {
        let midpoint:CGPoint = CGPoint(x:((epoint.x - spoint.x) * 0.5) + spoint.x,
                                       y:((epoint.y - spoint.y) * 0.5) + spoint.y);
        return midpoint;
    }

    //------------------------------------------------------------------------------------
    @objc func keyboardWillShow(_ notification: Notification)
    {
        ////print("keyboardWillShow")
        if let keyboardSize = (((notification as NSNotification).userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue)
        {
            ////print("keyboardWillShow keyboardSize.height ",keyboardSize.height)
            self.view.frame.origin.y = -keyboardSize.height
            lastKeybrdHeight = keyboardSize.height
            ////print("0 keyboardWillShow lastKeybrdHeight", lastKeybrdHeight)
        }
    }
    
    //------------------------------------------------------------------------------------
    @objc func keyboardWillHide(_ notification: Notification)
    {
        ////print("keyboardWillHide")
        //        if let keyboardSize = ((notification as NSNotification).userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        //        {
        ////print("keyboardWillHide keyboardSize.height ",keyboardSize.height)
        //            UIView.animate(withDuration: 4.5, animations:
        //            {() in
        self.view.frame.origin.y = 0  //(keyboardSize.height - 34)
        //            }, completion: {(completion) in
        //            })
        //        }
    }
    
    // MARK: - door type picker delegate/source
    //----------------------------------------------------------------------------------------------------------
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 2
    }
    
    //----------------------------------------------------------------------------------------------------------
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        if (component == 1) {return doorStyleKount }
        return doorTypeKount
    }
    
    //----------------------------------------------------------------------------------------------------------
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if (component == 1) {return self.doorStyle[row] }
        return self.doorModes[row]
    }
    
    //----------------------------------------------------------------------------------------------------------
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if (component == 0)
        {
            if (row > 1) {doorStyleKount = 2 } else {doorStyleKount = 3 }
            
            let style:Int = self.drPicker.selectedRow(inComponent:1)
            if (style > 1)
            {
                self.drPicker.selectRow(1, inComponent: 1, animated: false)
                //print("component  style,row doorStyleKount:",component, style,row,doorStyleKount)
            }
            //print("reloadComponent")
            
            self.drPicker.reloadComponent(1)
        }
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func addDrAction(_ button: UIButton)
    {
        self.doorTypeView.isHidden = false
        doorStyleKount = 3
        self.drPicker.reloadAllComponents()
        self.drPicker.selectRow(0, inComponent: 0, animated: false)
        self.drPicker.selectRow(1, inComponent: 1, animated: false)
    }

    //MARK: - to and from disk
  	//---------------------------------------------------------------------
    func saveProjectList()
	{
//print("saveProj ectList currProject?.projectID:",currProject?.projectID as Any)
//print("saveProj ectList currProjectThumb?.projectID:",currProjectThumb?.projectID as Any)
		guard let project = currProject else {return;}
		guard let projectThumb = currProjectThumb else {return;}

//		if let appDelegate0 = (UIApplication.shared.delegate as? AppDelegate)
//		{
//			DispatchQueue.global().async
//			{
//				appDelegate0.backgroundTaskID = UIApplication.shared.beginBackgroundTask (withName: "Finish saving data")
//				{
//					// End the task if time expires.
//					UIApplication.shared.endBackgroundTask(appDelegate0.backgroundTaskID!)
//					appDelegate0.backgroundTaskID = UIBackgroundTaskIdentifier.invalid
//				}

				let encoder = JSONEncoder()
				project.house0 = try? encoder.encode(houseObject)

//print("saveProj ectList project.house0:",project.house0 as Any)

				if let projectId = projectThumb.projectID
				{
					do
					{
						//---------------- save main project  ------------------
						if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
						{
							let fileURL = dir.appendingPathComponent("project_"+projectId+".txt")
							let currProjectURL = dir.appendingPathComponent("currProject.txt")
                            NSKeyedArchiver.setClassName("Project", for: Project.self)
							let data = try NSKeyedArchiver.archivedData(withRootObject: project, requiringSecureCoding: false)
							try data.write(to: fileURL)
							try data.write(to: currProjectURL)
						}

					} catch {
  print(" ---------------------- savePaintObject    project_  error:",error)
					}
				}

//				UIApplication.shared.endBackgroundTask(appDelegate0.backgroundTaskID!)
//				appDelegate0.backgroundTaskID = UIBackgroundTaskIdentifier.invalid
//			}
//		}
	}


    //MARK: - actions picker
  	//---------------------------------------------------------------------
    @IBAction func undoBtnAction(_ sender: UIButton)
    {
        self.doorPathsSet = false
        if sender.titleLabel!.text == "Undo"
        {
            sender.setTitle("Redo", for: .normal)
            if undoPECButton != nil
            {
                redoPECButton = undoPECButton
              undoPECButton!.changePrevPosition(point: undoPoint)
            }
        } else {
            if redoPECButton != nil
            {
                redoPECButton!.changePrevPosition(point: redoPoint)
            }
            sender.setTitle("Undo", for: .normal)
        }
    }

	//-----------------------------------------
    @IBAction func doneBtnAction(_ sender: UIButton)
    {
//print("editing  doneWithO verlay  shoul dSave:",shouldSave);
        wasChangeOfProject = false //**************** Edit fix Method Lakshmi - 2.0.5

		if (shouldSave)
		{
			self.saveProjectList()
		}
		shouldSave = false;

//		sidings.removeAll();
//		houses.removeAll();
//		roofs.removeAll();
//		windows.removeAll();
//		trims.removeAll();
//
//		maskedHouseImage = nil;
//
//		self.paintLayer.path = nil;
//		self.paintPath.removeAllPoints();

		for subView in self.scrollView.imageViewOverlay.subviews
		{
			if let button:UIButton = subView as? UIButton
			{
				button.removeFromSuperview();
			}
		}

        self.navigationController?.popViewController(animated:true)
	}

	//-----------------------------------------
    @IBAction func cancelDoorType(_ sender: Any)
    {
//        self.bottomView.isHidden = false
        self.doorTypeView.isHidden = true
    }

    //----------------------------------------------------------------------------------------------------------
    @IBAction func chooseDoorType(_ sender: Any)
    {
        editedGarage = true
        self.doorTypeView.isHidden = true
        let row:Int = self.drPicker.selectedRow(inComponent:0)
        let style:Int = self.drPicker.selectedRow(inComponent:1)
        var path:String!

        switch row
        {
        case 0:  path = Bundle.main.path(forResource: "fakeDblGarDr", ofType: "json")!; editedGarageAdded = true; isTwoCar = true;
        case 1:  path = Bundle.main.path(forResource: "fakeSglGarDr", ofType: "json")!; editedGarageAdded = true; isOneCar = true;
        case 2:  path = Bundle.main.path(forResource: "fakeDblFrtDr", ofType: "json")!; editedDoorAdded = true; isDoor = true; isDouble = true;
        case 3:  path = Bundle.main.path(forResource: "fakeSglFrtDr", ofType: "json")!; editedDoorAdded = true; isDoor = true;
        case 4:  path = Bundle.main.path(forResource: "fakeDblWinDr", ofType: "json")!
        case 5:  path = Bundle.main.path(forResource: "fakeSglWinDr", ofType: "json")!
        default: path = Bundle.main.path(forResource: "fakeDblGarDr", ofType: "json")!
        }

//print("editingViewCont  choosDoorType.isDo or:",isDoor)

        //print("self.house Object:",self.house Object as Any)
        do
        {
            let responseData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let json = String(data: responseData, encoding: String.Encoding.utf8)
//print("json",json as Any)
//print(" ")
            if let j = json
            {
				  shouldSave = true;

//                do
//                {
                    //print("self.house Object:",self.house Object as Any)
                    if (houseObject == nil)
                    {
//print("new house object currentImageName:",currentImageName)
                        houseObject = HouseObject(jsonString:j)

 						//************** change 0.0.14 add if block ***
						if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
						{
							let fileURL = dir.appendingPathComponent(currentImageName)

							houseObject?.image_url = [fileURL.absoluteString];
//print("new house object self.house Object?.image_url:",self.houseO bject?.image_url as Any)
                		//	UserDefaults.standard.set(fileURL.absoluteString , forKey:"Last_Image_url")
						}


						//************** change 0.0.40 not needed ***
//						if let home = homeviewController
//						{
////print(" home:",home)
//							home.selectedReferenceItem?.hous eObject = houseZ ZZObject;
//						}

                 //       self.getRepFromHouse ObjectByCategory()
                        self.encodeAddedDoor(style)
                        self.editSingleObjectAt()
                        self.scrollView.drawPolyCorners()
                    } else {
                        let ho = HouseObject(jsonString:j)
                        
                        houseObject?.response.append(contentsOf: ho.response)
                     //   houseObject?.wasProcessed = false; 0.0.46
               //         self.getRepFromSingleHouseO bjectByCategory()
                        self.encodeAddedDoor(style)
                        self.editSingleObjectAt()
                        self.scrollView.drawPolyCorners()
                    }
            }
        } catch {
  print("error")
        }
    }

    //---------------------------------------------------------------------
    func editSingleObjectAt()
	{
            if let ho = houseObject
            {
                if let resp = ho.response.last
                {
    //print("resp  :",resp)
                    for respItem in resp
                    {
                        if ( !respItem.wasGoodDr) { continue; }
                        respItem.wasLocked = true;
                        self.doorPathsSet = true;

                        if (respItem.wasPoly)
                        {
                            let theButtonV1:PECButton = self.cornerButton.pecDuplicate()!
                            self.scrollView.imageViewOverlay.addSubview(theButtonV1)

                            theButtonV1.center = respItem.p1.pt;
                            theButtonV1.ppt = respItem.p1;
                            theButtonV1.ppt.button = theButtonV1;
                            theButtonV1.respItem = respItem;
                            theButtonV1.topLPt = true;
                            theButtonV1.isSelected = false;
                            polyPath.move(to:theButtonV1.center);

                            let theButtonV2:PECButton = self.cornerButton.pecDuplicate()!
                            self.scrollView.imageViewOverlay.addSubview(theButtonV2)

                            theButtonV2.center = respItem.p2.pt;
                            theButtonV2.ppt = respItem.p2;
                            theButtonV2.ppt.button = theButtonV2;
                            theButtonV2.deleteCornerRecog.isEnabled = true;
                            theButtonV2.respItem = respItem;
                            theButtonV2.isSelected = false;
                            polyPath.addLine(to:theButtonV2.center);

                            let theButtonV3:PECButton = self.cornerButton.pecDuplicate()!
                            self.scrollView.imageViewOverlay.addSubview(theButtonV3)

                            theButtonV3.center = respItem.p3.pt;
                            theButtonV3.ppt = respItem.p3;
                            theButtonV3.ppt.button = theButtonV3;
                            theButtonV3.deleteCornerRecog.isEnabled = true;
                            theButtonV3.respItem = respItem;
                            theButtonV3.isSelected = false;
                            polyPath.addLine(to:theButtonV3.center);

                            let theButtonV4:PECButton = self.cornerButton.pecDuplicate()!
                            self.scrollView.imageViewOverlay.addSubview(theButtonV4)

                            theButtonV4.center = respItem.p4.pt;
                            theButtonV4.ppt = respItem.p4;
                            theButtonV4.ppt.button = theButtonV4;
                            theButtonV4.respItem = respItem;
                            theButtonV4.topRPt = true;
                            theButtonV4.isSelected = false;
                            polyPath.addLine(to:theButtonV4.center);

                            let theButtonV5:PECButton = self.cornerButton.pecDuplicate()!
                            self.scrollView.imageViewOverlay.addSubview(theButtonV5)

                            theButtonV5.center = respItem.p5.pt;
                            theButtonV5.ppt = respItem.p5;
                            theButtonV5.ppt.button = theButtonV5;
                            theButtonV5.respItem = respItem;
                            theButtonV5.botRPt = true;
                            theButtonV5.isSelected = false;
    //                        theButtonV5.isForSizing = true;
    //                        theButtonV5.setBackgroundImage(UIImage(named: "lockSizing.png"), for: .selected);
                            polyPath.addLine(to:theButtonV5.center);

                            let theButtonV6:PECButton = self.cornerButton.pecDuplicate()!
                            self.scrollView.imageViewOverlay.addSubview(theButtonV6)

                            theButtonV6.center = respItem.p6.pt;
                            theButtonV6.ppt = respItem.p6;
                            theButtonV6.ppt.button = theButtonV6;
                            theButtonV6.respItem = respItem;
                            theButtonV6.botLPt = true;
                            theButtonV6.isSelected = false;
                            polyPath.addLine(to:theButtonV6.center);
                            polyPath.close();


                            //------------- delete button -----------------
                            let deleteButton:UIButton = self.deleteButton.duplicate()!
                            deleteButton.tag = 10;
                            self.scrollView.imageViewOverlay.addSubview(deleteButton)

                            var bottom:Bline = Bline.init();
                            bottom.spoint = theButtonV5.center;
                            bottom.epoint = theButtonV6.center;

                            deleteButton.center = self.findMidPt2D(bottom);
                            deleteButton.isHidden = false;
                            deleteButton.isSelected = false;

                            theButtonV5.delButtonNx = deleteButton;
                            theButtonV6.delButtonLt = deleteButton;
                            respItem.delButton = deleteButton;


                            //------------- lock button -----------------
                            let lockButton:PECButton = self.cornerButton.pecDuplicate()!
                            lockButton.setBackgroundImage(UIImage(named: "lockArea.png"), for: .normal);
                            lockButton.setBackgroundImage(UIImage(named: "lockArea.png"), for: .highlighted);
                            lockButton.tag = 14;
                            self.scrollView.imageViewOverlay.addSubview(lockButton)

                            var diag:Bline = Bline.init();
                            diag.spoint = theButtonV1.center;
                            diag.epoint = theButtonV5.center;

                            lockButton.center = self.findMidPt2D(diag);
                            lockButton.isHidden = false;
                            lockButton.isForPaint = false;
                            lockButton.isLock = true;
                            lockButton.ppt = Ppoint.init(pt:lockButton.center);
                            lockButton.respItem = respItem;
                            respItem.lockButton = lockButton;
                            lockButton.topLeft = theButtonV1.center;
                            lockButton.botRght = theButtonV5.center;

                            lockButton.lockLinkFirst = theButtonV1;
                            theButtonV1.lockLinkFirst = lockButton;


                            //------------- sizing button -----------------
                            let sizeButton:PECButton = self.cornerButton.pecDuplicate()!
                            sizeButton.setBackgroundImage(UIImage(named: "lockSizing.png"), for: .normal);
                            sizeButton.setBackgroundImage(UIImage(named: "lockSizing.png"), for: .highlighted);
                            sizeButton.tag = 15;
                            self.scrollView.imageViewOverlay.addSubview(sizeButton)

                            sizeButton.center = theButtonV5.center;
                            sizeButton.center.x -= theButtonV5.frame.size.width;
                            sizeButton.center.y -= theButtonV5.frame.size.height;

                            sizeButton.isHidden = false;
                            sizeButton.isForPaint = false;
                            sizeButton.isForSizing = true;
                            sizeButton.ppt = Ppoint.init(pt:sizeButton.center);
                            sizeButton.respItem = respItem;

                            sizeButton.showhideLt = theButtonV5;
                            theButtonV5.showhideLt = sizeButton;

                        } else if (respItem.wasArch) {

                            let theButtonV1:PECButton = self.cornerButton.pecDuplicate()!
                            self.scrollView.imageViewOverlay.addSubview(theButtonV1)

                            theButtonV1.center = respItem.p1.pt;
                            theButtonV1.ppt = respItem.p1;
                            theButtonV1.ppt.button = theButtonV1;
                            theButtonV1.respItem = respItem;
                            theButtonV1.topLPt = true;
                            theButtonV1.isSelected = false;

                            let theButtonV2:PECButton = self.cornerButton.pecDuplicate()!
                            self.scrollView.imageViewOverlay.addSubview(theButtonV2)

                            theButtonV2.center = respItem.p2.pt;
                            theButtonV2.ppt = respItem.p2;
                            theButtonV2.ppt.button = theButtonV2;
                            theButtonV2.deleteCornerRecog.isEnabled = true;
                            theButtonV2.respItem = respItem;
                            theButtonV2.isSelected = false;

                            let theButtonV3:PECButton = self.cornerButton.pecDuplicate()!
                            self.scrollView.imageViewOverlay.addSubview(theButtonV3)

                            theButtonV3.center = respItem.p3.pt;
                            theButtonV3.ppt = respItem.p3;
                            theButtonV3.ppt.button = theButtonV3;
                            theButtonV3.respItem = respItem;
                            theButtonV3.topRPt = true;
                            theButtonV3.isSelected = false;

                            let theButtonV4:PECButton = self.cornerButton.pecDuplicate()!
                            self.scrollView.imageViewOverlay.addSubview(theButtonV4)

                            theButtonV4.center = respItem.p4.pt;
                            theButtonV4.ppt = respItem.p4;
                            theButtonV4.ppt.button = theButtonV4;
                            theButtonV4.respItem = respItem;
                            theButtonV4.botRPt = true;
                            theButtonV4.isSelected = false;
    //                        theButtonV4.isForSizing = true;
    //                        theButtonV4.setBackgroundImage(UIImage(named: "lockSizing.png"), for: .selected);

                            let theButtonV5:PECButton = self.cornerButton.pecDuplicate()!
                            self.scrollView.imageViewOverlay.addSubview(theButtonV5)

                            theButtonV5.center = respItem.p5.pt;
                            theButtonV5.ppt = respItem.p5;
                            theButtonV5.ppt.button = theButtonV5;
                            theButtonV5.ppt.nx = respItem.p1;
                            theButtonV1.ppt.lt = respItem.p5;
                            theButtonV5.respItem = respItem;
                            theButtonV5.botLPt = true;
                            theButtonV5.isSelected = false;

                            polyPath.append(respItem.path);


                            //------------- delete button -----------------
                            let deleteButton:UIButton = self.deleteButton.duplicate()!
                            deleteButton.tag = 10;
                            self.scrollView.imageViewOverlay.addSubview(deleteButton)

                            var bottom:Bline = Bline.init();
                            bottom.spoint = theButtonV4.center;
                            bottom.epoint = theButtonV5.center;

                            deleteButton.center = self.findMidPt2D(bottom);
                            deleteButton.isHidden = false;
                            deleteButton.isSelected = false;

                            theButtonV4.delButtonNx = deleteButton;
                            theButtonV5.delButtonLt = deleteButton;
                            respItem.delButton = deleteButton;


                            //------------- lock button -----------------
                            let lockButton:PECButton = self.cornerButton.pecDuplicate()!
                            lockButton.setBackgroundImage(UIImage(named: "lockArea.png"), for: .normal);
                            lockButton.setBackgroundImage(UIImage(named: "lockArea.png"), for: .highlighted);
                            lockButton.tag = 14;
                            self.scrollView.imageViewOverlay.addSubview(lockButton)

                            var diag:Bline = Bline.init();
                            diag.spoint = theButtonV1.center;
                            diag.epoint = theButtonV4.center;

                            lockButton.center = self.findMidPt2D(diag);
                            lockButton.isHidden = false;
                            lockButton.isForPaint = false;
                            lockButton.isLock = true;
                            lockButton.ppt = Ppoint.init(pt:lockButton.center);
                            lockButton.respItem = respItem;
                            respItem.lockButton = lockButton;
                            lockButton.topLeft = theButtonV1.center;
                            lockButton.botRght = theButtonV4.center;

                            lockButton.lockLinkFirst = theButtonV1;
                            theButtonV1.lockLinkFirst = lockButton;


                            //------------- sizing button -----------------
                            let sizeButton:PECButton = self.cornerButton.pecDuplicate()!
                            sizeButton.setBackgroundImage(UIImage(named: "lockSizing.png"), for: .normal);
                            sizeButton.setBackgroundImage(UIImage(named: "lockSizing.png"), for: .highlighted);
                            sizeButton.tag = 15;
                            self.scrollView.imageViewOverlay.addSubview(sizeButton)

                            sizeButton.center = theButtonV4.center;
                            sizeButton.center.x -= theButtonV4.frame.size.width;
                            sizeButton.center.y -= theButtonV4.frame.size.height;

                            sizeButton.isHidden = false;
                            sizeButton.isForPaint = false;
                            sizeButton.isForSizing = true;
                            sizeButton.ppt = Ppoint.init(pt:sizeButton.center);
                            sizeButton.respItem = respItem;

                            sizeButton.showhideLt = theButtonV4;
                            theButtonV4.showhideLt = sizeButton;

                        } else {
                            let theButtonV1:PECButton = self.cornerButton.pecDuplicate()!
                            self.scrollView.imageViewOverlay.addSubview(theButtonV1)

                            theButtonV1.center = respItem.p1.pt;
                            theButtonV1.ppt = respItem.p1;
                            theButtonV1.ppt.button = theButtonV1;
                            theButtonV1.respItem = respItem;
                            theButtonV1.topLPt = true;
                            theButtonV1.isSelected = false;
                            polyPath.move(to:theButtonV1.center);

                            let theButtonV2:PECButton = self.cornerButton.pecDuplicate()!
                            self.scrollView.imageViewOverlay.addSubview(theButtonV2)

                            theButtonV2.center = respItem.p2.pt;
                            theButtonV2.ppt = respItem.p2;
                            theButtonV2.ppt.button = theButtonV2;
                            theButtonV2.respItem = respItem;
                            theButtonV2.topRPt = true;
                            theButtonV2.isSelected = false;
                            polyPath.addLine(to:theButtonV2.center);

                            let theButtonV3:PECButton = self.cornerButton.pecDuplicate()!
                            self.scrollView.imageViewOverlay.addSubview(theButtonV3)

                            theButtonV3.center = respItem.p3.pt;
                            theButtonV3.ppt = respItem.p3;
                            theButtonV3.ppt.button = theButtonV3;
                            theButtonV3.respItem = respItem;
                            theButtonV3.botRPt = true;
                            theButtonV3.isSelected = false;
    //                        theButtonV3.isForSizing = true;
    //                        theButtonV3.setBackgroundImage(UIImage(named: "lockSizing.png"), for: .selected);
                            polyPath.addLine(to:theButtonV3.center);

                            let theButtonV4:PECButton = self.cornerButton.pecDuplicate()!
                            self.scrollView.imageViewOverlay.addSubview(theButtonV4)

                            theButtonV4.center = respItem.p4.pt;
                            theButtonV4.ppt = respItem.p4;
                            theButtonV4.ppt.button = theButtonV4;
                            theButtonV4.ppt.nx = respItem.p1;
                            theButtonV1.ppt.lt = respItem.p4;
                            theButtonV4.respItem = respItem;
                            theButtonV4.botLPt = true;
                            theButtonV4.isSelected = false;
                            polyPath.addLine(to:theButtonV4.center);
                            polyPath.close();

                        //    self.theBut tonV0 = theButtonV1;

                            //------------- delete button -----------------
                            let deleteButton:UIButton = self.deleteButton.duplicate()!
                            deleteButton.tag = 10;
                            self.scrollView.imageViewOverlay.addSubview(deleteButton)

                            var bottom:Bline = Bline.init();
                            bottom.spoint = theButtonV3.center;
                            bottom.epoint = theButtonV4.center;

                            deleteButton.center = self.findMidPt2D(bottom);
                            deleteButton.isHidden = false;
                            deleteButton.isSelected = false;

                            theButtonV3.delButtonNx = deleteButton;
                            theButtonV4.delButtonLt = deleteButton;
                            respItem.delButton = deleteButton;


                            //------------- lock button -----------------
                            let lockButton:PECButton = self.cornerButton.pecDuplicate()!
                            lockButton.setBackgroundImage(UIImage(named: "lockArea.png"), for: .normal);
                            lockButton.setBackgroundImage(UIImage(named: "lockArea.png"), for: .highlighted);
                            lockButton.tag = 14;
                            self.scrollView.imageViewOverlay.addSubview(lockButton)

                            var diag:Bline = Bline.init();
                            diag.spoint = theButtonV1.center;
                            diag.epoint = theButtonV3.center;

                            lockButton.center = self.findMidPt2D(diag);
                            lockButton.isHidden = false;
                            lockButton.isForPaint = false;
                            lockButton.isLock = true;
                            lockButton.ppt = Ppoint.init(pt:lockButton.center);
                            lockButton.respItem = respItem;
                            respItem.lockButton = lockButton;
                            lockButton.topLeft = theButtonV1.center;
                            lockButton.botRght = theButtonV3.center;

                            lockButton.lockLinkFirst = theButtonV1;
                            theButtonV1.lockLinkFirst = lockButton;


                            //------------- sizing button -----------------
                            let sizeButton:PECButton = self.cornerButton.pecDuplicate()!
                            sizeButton.setBackgroundImage(UIImage(named: "lockSizing.png"), for: .normal);
                            sizeButton.setBackgroundImage(UIImage(named: "lockSizing.png"), for: .highlighted);
                            sizeButton.tag = 15;
                            self.scrollView.imageViewOverlay.addSubview(sizeButton)

                            sizeButton.center = theButtonV3.center;
                            sizeButton.center.x -= theButtonV3.frame.size.width;
                            sizeButton.center.y -= theButtonV3.frame.size.height;

                            sizeButton.isHidden = false;
                            sizeButton.isForPaint = false;
                            sizeButton.isForSizing = true;
                            sizeButton.ppt = Ppoint.init(pt:sizeButton.center);
                            sizeButton.respItem = respItem;

                            sizeButton.showhideLt = theButtonV3;
                            theButtonV3.showhideLt = sizeButton;
                        }
                    }
                }

                polyLayer.path = nil;
                polyLayer.path = polyPath.cgPath;
            }
        }

	//----------------------------------------------
    @IBAction func showHideGarageAction(_ button: UIButton)
    {
//print("showHideGarageAction ---- what is this doing?")

        var opacity:CGFloat = 1.0
        var wasRespItem = false
        button.isSelected = !(button.isSelected)
        if button.isSelected
        {
            opacity = 0.5
        }
        
        
        if let ho = houseObject
        {
            guard let refImage = originalHomeImage else { return }
            // let refImage = self.oldIm age
            var newReferenceImage = UIImage();  //************ change 0.444.3 change ***
            let hostRect = CGRect.init(x: 0, y: 0, width: refImage.size.width, height: refImage.size.height)
            
            let imageScale = refImage.scale
            for resp in ho.response
            {
				for respItem in resp
				{
               		var replacement: UIImage =  UIImage(imageLiteralResourceName: "transparent")
					var lbl = "nothing"
					if (respItem.labels.count > 0)
					{
						lbl = respItem.labels[0]
					}
					let gtype:GarageTypes = HouseObject.getGarageType(label: lbl) ?? .somethingNotInList
					//print(" ")
					//print(" ")
					//print("---- resp gtype -------:",gtype)
	//print("showHideGarageAction ---- twoDo orImgae:",twoDoorImgae as Any)

					switch (gtype)
					{
//					case .oneCar:
//						replacement = oneCarImgae ?? UIImage(imageLiteralResourceName: "transparent")
//					case .twoCar:
//						replacement = twoCarImgae ?? UIImage(imageLiteralResourceName: "transparent")
//					case .onefrontDoor:
//						replacement = oneDoorImgae ?? UIImage(imageLiteralResourceName: "transparent")
//					case .twofrontDoor:
//						replacement = twoDoorImgae ?? UIImage(imageLiteralResourceName: "transparent")

					case .oneCar:
						replacement = oneCarImgae ?? UIImage(imageLiteralResourceName: "1_s0.jpg")
					case .twoCar:
						replacement = twoCarImgae ?? UIImage(imageLiteralResourceName: "1_d0.jpg")
					case .onefrontDoor:
						replacement = oneDoorImgae ?? UIImage(imageLiteralResourceName: "f_s0.jpg")
					case .twofrontDoor:
						replacement = twoDoorImgae ?? UIImage(imageLiteralResourceName: "f_d10.jpg")



					case .window1:
						replacement = twoDoorImgae ?? UIImage(imageLiteralResourceName: "transparent")
					case .somethingNotInList:
						replacement = UIImage(imageLiteralResourceName: "transparent")
					}


					var currDrGrayLum:CGFloat = 1.0
					if (includeShade)
					{
						let pixelData:CFData = replacement.cgImage!.dataProvider!.data!
						let data = CFDataGetBytePtr(pixelData)!   //:const UInt8*

						let imageByteSize:Int = Int(replacement.size.width * replacement.size.height * 4.0)
						var totalNumberOfPixels:Int = 0

						var currentByte:Int = 0
						var normalizedRedTotal:CGFloat = 0.0
						var normalizedGreenTotal:CGFloat = 0.0
						var normalizedBlueTotal:CGFloat = 0.0

						var redTotal = 0, greenTotal = 0, blueTotal = 0, alpha = 0

						while (currentByte < imageByteSize)
						{
							//---------- find original image color -----------------
							alpha    = Int(data[currentByte + 3])
							if (alpha > 250)
							{
								redTotal    += Int(data[currentByte + 0])
								greenTotal    += Int(data[currentByte + 1])
								blueTotal    += Int(data[currentByte + 2])
								totalNumberOfPixels += 1
							}

							currentByte += 4
						}

						if (totalNumberOfPixels > 0)
						{
							normalizedRedTotal     = CGFloat(redTotal)   / CGFloat(totalNumberOfPixels) / 255.0
							normalizedGreenTotal = CGFloat(greenTotal) / CGFloat(totalNumberOfPixels) / 255.0
							normalizedBlueTotal  = CGFloat(blueTotal)  / CGFloat(totalNumberOfPixels) / 255.0

							currDrGrayLum = (0.2125 * normalizedRedTotal) + (0.7154 * normalizedGreenTotal) + (0.07210 * normalizedBlueTotal)
						}
					}
//                }
//
//                for respItem in resp
//                {
                    if respItem.imageID_db == button.accessibilityLabel
                    {
                        wasRespItem = true
                    }else
                    {
                        wasRespItem = false
                    }
                    
                    //print(" respItem.wasGoodDr ")
                    //print("  ")
                    //print("2 resp Item.rect",respItem.rect)
                    ////print("2 respItem.wasArch",respItem.wasArch)
                    //print("  ")
                    //print("2 respItem.v1",respItem.v1)
                    //print("2 respItem.v 2",respItem.v 2)
                    //print("2 respItem.v3",respItem.v3)
                    //print("2 respItem.v4",respItem.v4)
                    //print("  ")
                    
                    let context0:CIContext = CIContext(options: nil)
                    
                    //perspective warping of Door image using CIFilter,
                    if let perpFilter = CIFilter(name: "CIPerspectiveTransform")
                    {
                        let beginImage = CIImage(image: replacement)
                        perpFilter.setValue(beginImage, forKey:kCIInputImageKey)
                        
                        perpFilter.setValue(respItem.v1, forKey:"inputTopLeft")
                        perpFilter.setValue(respItem.v2, forKey:"inputTopRight")
                        perpFilter.setValue(respItem.v3, forKey:"inputBottomRight")
                        perpFilter.setValue(respItem.v4, forKey:"inputBottomLeft")
                        
                        if let output = perpFilter.outputImage
                        {
                            if let cgimg = context0.createCGImage(output, from: output.extent)
                            {
                                let processedImage = UIImage(cgImage: cgimg)
                                
                                var diffGrayLum:CGFloat = 0.0
                                var doorMaskImageShade:UIImage! = UIImage()
                                
                                if (includeShade)
                                {
                                    if (self.greyLumIsCalculated)
                                    {
                                        diffGrayLum = currDrGrayLum - respItem.grayLum
                                        if (diffGrayLum < 0.0) {diffGrayLum = 0.0 }
                                        diffGrayLum = diffGrayLum * 0.65
                                    } else {
                                        var newSize0 = respItem.rect.size
                                        let newSpot0 = respItem.rect.origin
                                        let centerPt:CIVector = CIVector(x:(newSize0.width * 0.5) + newSpot0.x, y:(newSize0.height * 0.5) + newSpot0.y)
                                        
                                        newSize0.width = floor(newSize0.width)
                                        newSize0.height = floor(newSize0.height)
                                        
                                        UIGraphicsBeginImageContextWithOptions(newSize0, false, imageScale)
                                        let context = UIGraphicsGetCurrentContext()!
                                        context.translateBy(x: respItem.rect.size.width*0.5, y: respItem.rect.size.height*0.5)
                                        refImage.draw(in: CGRect(x: -centerPt.x, y: -centerPt.y, width: refImage.size.width, height: refImage.size.height))
                                        processedImage.draw(in: CGRect(x: -respItem.rect.size.width*0.5, y: -respItem.rect.size.height*0.5, width: respItem.rect.size.width, height: respItem.rect.size.height), blendMode: .destinationIn, alpha: 1.0)
                                        let doorAreaImage = UIGraphicsGetImageFromCurrentImageContext()!
                                        UIGraphicsEndImageContext()
                                        
                                        
                                        let pixelData:CFData = doorAreaImage.cgImage!.dataProvider!.data!
                                        let data = CFDataGetBytePtr(pixelData)!   //:const UInt8*
                                        
                                        let imageByteSize:Int = Int(doorAreaImage.size.width * doorAreaImage.size.height * 4.0)
                                        var totalNumberOfPixels:Int = 0
                                        
                                        var currentByte:Int = 0
                                        var normalizedRedTotal:CGFloat = 0.0
                                        var normalizedGreenTotal:CGFloat = 0.0
                                        var normalizedBlueTotal:CGFloat = 0.0
                                        
                                        var redTotal = 0, greenTotal = 0, blueTotal = 0, alpha = 0
                                        var grayLum:CGFloat = 0.0
                                        
                                        
                                        while (currentByte < imageByteSize)
                                        {
                                            //---------- find original image color -----------------
                                            alpha    = Int(data[currentByte + 3])
                                            if (alpha > 250)
                                            {
                                                redTotal    += Int(data[currentByte + 0])
                                                greenTotal    += Int(data[currentByte + 1])
                                                blueTotal    += Int(data[currentByte + 2])
                                                
                                                totalNumberOfPixels += 1
                                            }
                                            
                                            currentByte += 4
                                        }
                                        
                                        if (totalNumberOfPixels > 0)
                                        {
                                            normalizedRedTotal     = CGFloat(redTotal)   / CGFloat(totalNumberOfPixels) / 255.0
                                            normalizedGreenTotal = CGFloat(greenTotal) / CGFloat(totalNumberOfPixels) / 255.0
                                            normalizedBlueTotal  = CGFloat(blueTotal)  / CGFloat(totalNumberOfPixels) / 255.0
                                            
                                            grayLum = (0.2125 * normalizedRedTotal) + (0.7154 * normalizedGreenTotal) + (0.07210 * normalizedBlueTotal)
                                            respItem.grayLum = grayLum
                                            diffGrayLum = currDrGrayLum - grayLum
                                            if (diffGrayLum < 0.0) {diffGrayLum = 0.0 }
                                            diffGrayLum = diffGrayLum * 0.65
                                        }
                                    }
                                    
                                    let fillColor:UIColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: diffGrayLum)
                                    
                                    UIGraphicsBeginImageContextWithOptions(respItem.rect.size, false, imageScale)
                                    let context3 = UIGraphicsGetCurrentContext()!
                                    context3.setFillColor(fillColor.cgColor)
                                    context3.fill(CGRect(x: 0, y: 0, width: respItem.rect.size.width, height: respItem.rect.size.height))
                                    processedImage.draw(in: CGRect(x: 0.0, y: 0.0, width: respItem.rect.size.width, height: respItem.rect.size.height), blendMode: .destinationIn, alpha: 1.0)
                                    doorMaskImageShade = UIGraphicsGetImageFromCurrentImageContext()!
                                    UIGraphicsEndImageContext()
                                }
                                
                                
                                
                                if (respItem.wasPoly)
                                {
                                    UIGraphicsBeginImageContext(CGSize.init(width: hostRect.size.width, height: hostRect.size.height))
                                    guard let context = UIGraphicsGetCurrentContext() else { return }
                                    newReferenceImage.draw(in: hostRect)
                                    
                                    context.addPath(respItem.path.cgPath)
                                    context.clip(using: .evenOdd)
                                    
                                    processedImage.draw(in: respItem.rect, blendMode: .normal, alpha: (wasRespItem ? opacity : 1.0))
                                    if (includeShade) {doorMaskImageShade.draw(in: respItem.rect, blendMode: .normal, alpha: 1.0)}
                                    
                                    newReferenceImage = UIGraphicsGetImageFromCurrentImageContext()!
                                    UIGraphicsEndImageContext()
                                    
                                } else if (respItem.wasArch) {
                                    
                                    UIGraphicsBeginImageContext(CGSize.init(width: hostRect.size.width, height: hostRect.size.height))
                                    guard let context = UIGraphicsGetCurrentContext() else { return }
                                    newReferenceImage.draw(in: hostRect)
                                    
                                    context.addPath(respItem.path.cgPath)
                                    context.clip(using: .evenOdd)
                                    
                                    processedImage.draw(in: respItem.rect, blendMode: .normal, alpha: (wasRespItem ? opacity : 1.0))
                                    if (includeShade) {doorMaskImageShade.draw(in: respItem.rect, blendMode: .normal, alpha: 1.0)}
                                    
                                    newReferenceImage = UIGraphicsGetImageFromCurrentImageContext()!
                                    UIGraphicsEndImageContext()
                                    
                                } else {
                                    
                                    UIGraphicsBeginImageContext(CGSize.init(width: hostRect.size.width, height: hostRect.size.height))
                                    newReferenceImage.draw(in: hostRect)
                                    processedImage.draw(in: respItem.rect, blendMode: .normal, alpha: (wasRespItem ? opacity : 1.0))
                                    if (includeShade) {doorMaskImageShade.draw(in: respItem.rect, blendMode: .normal, alpha: 1.0)}
                                    newReferenceImage = UIGraphicsGetImageFromCurrentImageContext()!
                                    UIGraphicsEndImageContext()
                                }
                            }
                        }
                    }
                }
            }

            DispatchQueue.main.async
			{
                    self.scrollView.garageImageView.image = newReferenceImage
        			maskedDoorImage = newReferenceImage  //***************** change 0.0.30 add ***
            }
        }
    }
    
    //----------------------------------------------------------------------------------------------------------
    @IBAction func deleteDrAction(_ button: UIButton)
    {
//print("00 delete DrAction")
        var kount:Int = -1;
        var wasBreak:Bool = false;
        var buttonToRemove:UIButton?;
        var respToRemove:Response?;

//print("00 self.house Object:",self.house Object as Any)
//print("00 self.hous eObject?.response:",self.ho useObject?.response as Any)

//if let resp = self.hou seObject?.response
//{
////print("  ")
////print("  ")
//for responseArray in resp
//{
////print("00 responseArray:",responseArray)
//}
//}


        if let ho = houseObject
        {
            for resp in ho.response
            {
                kount += 1;
                for respItem in resp
                {
                    if let delButton = respItem.delButton
                    {
                        if (delButton == button)
                        {
                            wasBreak = true;
							editedGarage = true;
                            buttonToRemove = respItem.delButton;
                            respToRemove = respItem;
                            break;
                        }
                    }
                }
                if (wasBreak) {break;}
            }
        }

//print("wasBreak,kount",wasBreak,kount)
        if (wasBreak && kount >= 0)
        {
			self.polyPath.removeAllPoints();
			polyLayer.path = nil;
			shouldSave = true;  //********** change 0.0.40 add ***

			for subView in self.scrollView.imageViewOverlay.subviews
			{
				if let button:PECButton = subView as? PECButton
				{
					if (button.respItem == respToRemove)
					{
						button.removeFromSuperview();
					}
				} else if let button:UIButton = subView as? UIButton {
					if (button == buttonToRemove)
					{
						button.removeFromSuperview();
					}
				}
			}

			houseObject?.response.remove(at: kount);

			self.drawDoorObjectAt();
			self.scrollView.drawPolyCorners();
        }
    }

    // MARK: - layer methods
    //----------------------------------------------------------------------------------
    func convertPointEdit(point:CGPoint, fromRect:CGRect, toRect:CGRect) -> CGPoint
    {
        return  CGPoint(x:(toRect.size.width/fromRect.size.width) * point.x,
                        y:(toRect.size.height/fromRect.size.height) * point.y);
    }

    //---------------------------------------------------------------------
    func encodeAddedDoor(_ style:Int)
    {
//print("encode Added Door")
        self.greyLumIsCalculated = false
        
        if let ho = houseObject
        {
            guard let refImage = originalHomeImage else { return }

            hostRectPEC = CGRect.init(x: 0, y: 0, width: refImage.size.width, height: refImage.size.height)
            wasForModifai = false
            
            image_url = "none"
            
            let fullurl = ho.image_url[0]
            
            if (fullurl.count > 0)
            {
                let fullurlArr = fullurl.components(separatedBy: " ")
                if (fullurlArr.count > 0)
                {
                    image_url = fullurlArr[0]
                }
            }
            
            var rois:[[Int]]
            
            if let resp = ho.response.last
            {
                for respItem in resp
                {
                	respItem.classIDS[0] = -88;
                respItem.wasProcessed = true;   //*************** change 0.0.47  add 

//print("encode Added Door resp,respItem:",resp,respItem)

                    if (respItem.rois.count == 4)
                    {
                        rois = respItem.rois
                    } else {
                        continue
                    }

                    if (style == 0)
                    {
                        respItem.wasArch = true
                    } else if (style == 2) {
                        respItem.wasPoly = true
                    }

					var garImageWidth:CGFloat  = 2048.0;
					var garImageHeight:CGFloat = 1536.0;

					if (refImage.size.height > refImage.size.width)
					{
						let tempWidth:CGFloat = garImageWidth;
						garImageWidth  = garImageHeight;
						garImageHeight = tempWidth;
					}


//print(" respItem.wasPoly:",respItem.wasPoly)
//print(" respItem.wasArch:",respItem.wasArch)
                    
                    respItem.wasGoodDr = true
                    var scalerValue:CGFloat = 1.0
//                    if (resolutionOnorOff)
//                    {
                        scalerValue = 3.2
//                    }

					let scalerZoom:CGFloat = min(self.scrollView.reverseZoomFactor / (4.5 * theImageScale),1.0);

					let ratioWidth:CGFloat  = (refImage.size.width  / garImageWidth)  * scalerValue * scalerZoom;
					let ratioHeight:CGFloat = (refImage.size.height / garImageHeight) * scalerValue * scalerZoom;

					let screenMid = CGPoint(x: self.view.bounds.size.width*0.5, y: self.view.bounds.size.height*0.5);

					let theFrame = CGRect(origin: self.scrollView.frame.origin, size: self.scrollView.contentSize);
					let theImageFrame = CGRect(origin: self.scrollView.frame.origin, size: refImage.size);

					let midPt = CGPoint(x: screenMid.x*self.scrollView.reverseZoomFactor, y: screenMid.y*self.scrollView.reverseZoomFactor);
					let startPt = self.convertPointEdit(point:self.scrollView.contentOffset, fromRect:theFrame, toRect:theImageFrame);

//print("  ")
//print("self.scrollView.reverseZoomFactor:",self.scrollView.reverseZoomFactor)
//print("theIma geScale:",theIma geScale)
//print("scalerValue:",scalerValue)
//print("scalerZoom:",scalerZoom)
//print("destImag eWidthP EC:",destImageWi dthP EC)
//print("destImageHei ghtP EC:",destImageHe ightP EC)
//print("ratioWidth:",ratioWidth)
//print("ratioHeight:",ratioHeight)
//print("theFrame:",theFrame)
//print("theIm ageFrame:",theImageFrame)
//print("self.scrollView.contentOffset:",self.scrollView.contentOffset)
//print("screenMid:",screenMid)
//print("midPt:",midPt)
//print("startPt:",startPt)
//print("  ")

					let ofsx:Float = min(Float(startPt.x+midPt.x),Float(refImage.size.width-(100.0*ratioWidth)));
					let ofsy:Float = min(Float(startPt.y+midPt.y),Float(refImage.size.height-(100.0*ratioHeight)));

//print("ofsx,ofsy",ofsx,ofsy)

                    var minX =  999999
                    var maxX = -999999
                    var minY =  999999
                    var maxY = -999999

                    for xy in rois
                    {
                        if (xy[0] > maxX) {maxX = xy[0]}
                        if (xy[0] < minX) {minX = xy[0]}
                        if (xy[1] > maxY) {maxY = xy[1]}
                        if (xy[1] < minY) {minY = xy[1]}
                    }

                    let topLeft:Apoint = Apoint((Float(minX) * Float(ratioWidth))+ofsx,(Float(minY) * Float(ratioHeight))+ofsy)
                    let botLeft:Apoint = Apoint((Float(minX) * Float(ratioWidth))+ofsx,(Float(maxY) * Float(ratioHeight))+ofsy)

                    let topRght:Apoint = Apoint((Float(maxX) * Float(ratioWidth))+ofsx,(Float(minY) * Float(ratioHeight))+ofsy)
                    let botRght:Apoint = Apoint((Float(maxX) * Float(ratioWidth))+ofsx,(Float(maxY) * Float(ratioHeight))+ofsy)

                    respItem.rect = CGRect(x: CGFloat(topLeft.x), y: CGFloat(topLeft.y), width: CGFloat(topRght.x - topLeft.x), height: CGFloat(botLeft.y - topLeft.y))

                    respItem.v1 = CIVector(x: CGFloat(topLeft.x), y: CGFloat(refImage.size.height - CGFloat(topLeft.y)));
                    respItem.v2 = CIVector(x: CGFloat(topRght.x), y: CGFloat(refImage.size.height - CGFloat(topRght.y)));
                    respItem.v3 = CIVector(x: CGFloat(botRght.x), y: CGFloat(refImage.size.height - CGFloat(botRght.y)));
                    respItem.v4 = CIVector(x: CGFloat(botLeft.x), y: CGFloat(refImage.size.height - CGFloat(botLeft.y)));

                    respItem.p1.pt = CGPoint(x: respItem.v1.x, y: CGFloat(topLeft.y));
                    respItem.p2.pt = CGPoint(x: respItem.v2.x, y: CGFloat(topRght.y));
                    respItem.p3.pt = CGPoint(x: respItem.v3.x, y: CGFloat(botRght.y));
                    respItem.p4.pt = CGPoint(x: respItem.v4.x, y: CGFloat(botLeft.y));

//print("respItem.p1.pt",respItem.p1.pt)
//print("respItem.p2.pt",respItem.p2.pt)
//print("respItem.p3.pt",respItem.p3.pt)
//print("respItem.p4.pt",respItem.p4.pt)



                    if (respItem.wasPoly)
                    {
                        respItem.p1.pt = CGPoint(x: respItem.v1.x, y: CGFloat(topLeft.y))
                        respItem.p4.pt = CGPoint(x: respItem.v2.x, y: CGFloat(topRght.y))
                        respItem.p5.pt = CGPoint(x: respItem.v3.x, y: CGFloat(botRght.y))
                        respItem.p6.pt = CGPoint(x: respItem.v4.x, y: CGFloat(botLeft.y))
                        
                        let dist:CGFloat = findDistance2Dcc(respItem.p1.pt, respItem.p4.pt) * 0.18
                        
                        respItem.p2.pt = CGPoint(x: respItem.p1.pt.x + dist, y: respItem.p1.pt.y)
                        respItem.p3.pt = CGPoint(x: respItem.p4.pt.x - dist, y: respItem.p4.pt.y)
                        
                        respItem.p1.pt.y += dist
                        respItem.p4.pt.y += dist
                        

                        respItem.path.move(to: respItem.p1.pt)
                        respItem.path.addLine(to: respItem.p2.pt)
                        respItem.path.addLine(to: respItem.p3.pt)
                        respItem.path.addLine(to: respItem.p4.pt)
                        respItem.path.addLine(to: respItem.p5.pt)
                        respItem.path.addLine(to: respItem.p6.pt)
                        respItem.path.close()
                        
						respItem.rect = respItem.path.cgPath.boundingBox
                        
                    } else if (respItem.wasArch) {
                        respItem.p1.pt = CGPoint(x: respItem.v1.x, y: CGFloat(topLeft.y))
                        
                        respItem.p3.pt = CGPoint(x: respItem.v2.x, y: CGFloat(topRght.y))
                        respItem.p4.pt = CGPoint(x: respItem.v3.x, y: CGFloat(botRght.y))
                        respItem.p5.pt = CGPoint(x: respItem.v4.x, y: CGFloat(botLeft.y))
//print("respItem.wasArch respItem.p5.pt",respItem.p5.pt)

                        var dist:CGFloat = findDistance2Dcc(respItem.p1.pt, respItem.p3.pt) * 0.50
                        
                        respItem.p2.pt = CGPoint(x: respItem.p1.pt.x + dist, y: respItem.p1.pt.y)  // - (dist*0.30))
                        
                        dist = dist * 0.30
                        
                        respItem.p1.pt.y += dist
                        respItem.p3.pt.y += dist
                        
                        var theArc:Arctype = Arctype()
                        theArc.bpoint = Apoint(Float(respItem.p1.pt.x), Float(respItem.p1.pt.y))
                        theArc.mpoint = Apoint(Float(respItem.p2.pt.x), Float(respItem.p2.pt.y))
                        theArc.epoint = Apoint(Float(respItem.p3.pt.x), Float(respItem.p3.pt.y))
//print(" ")
//print(" theArc.bpoint:",theArc.bpoint)
//print(" theArc.mpoint:",theArc.mpoint)
//print(" theArc.epoint:",theArc.epoint)
                        
                        respItem.path = PECperspcCorrect.drawArc(theArc)
                        
                        respItem.path.addLine(to: respItem.p3.pt)
                        respItem.path.addLine(to: respItem.p4.pt)
                        respItem.path.addLine(to: respItem.p5.pt)
                        respItem.path.close()
                        
						respItem.rect = respItem.path.cgPath.boundingBox
                     }
                }
				self.reDrawAllDoors()
			}
        }
    }


    @IBAction func horzChangeDoorsAction(_ sender: Any)
    {
        if (sourceArray.count < 1) {return }
        
        var index:Int = 0
        
        if let index0 = sourceArray.firstIndex(of: garDoorSourceDdr)
        {
            index = index0
        } else {
            if let index0 = sourceArray.firstIndex(of: garDoorSourceSdr)
            {
                index = index0
            }
        }
        
        if (index > sourceArray.count-1) {index = 0}
        doorSource = sourceArray[index]
        
        
    }
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage
    {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width:size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in:rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}


struct AppUtility
{
    
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask)
    {
        
        if let delegate = UIApplication.shared.delegate as? AppDelegate
        {
            delegate.orientationLock = orientation
        }
    }
    
    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
        
        self.lockOrientation(orientation)
        
        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        UINavigationController.attemptRotationToDeviceOrientation()
    }
    
}

//--------------------------------------------------------
extension UIButton
{
    func duplicate() -> UIButton?
    {
//print("  duplicate ")
          let buttonDuplicate = UIButton.init(type:ButtonType.custom);
          buttonDuplicate.frame = self.frame;
      //    let buttonDuplicate = PECButton(frame: self.frame)
    //    buttonDuplicate.setBackgroundImage(UIImage(named: "delete.png"), for: .normal);
    //    buttonDuplicate.setImage(UIImage(named: "delete.png"), for: .normal);
//        buttonDuplicate.setBackgroundImage(UIImage(named: "delete.png"), for: .selected); //*********** change 1.4.6 remove ***
    //    buttonDuplicate.setImage(UIImage(named: "sizing.png"), for: .selected);
//        buttonDuplicate.setBackgroundImage(UIImage(named: "sizing.png"), for: .highlighted);
    //    buttonDuplicate.setImage(UIImage(named: "sizing.png"), for: .highlighted);
//        buttonDuplicate.setBackgroundImage(UIImage(named: "sizing.png"), for: .disabled);
//        buttonDuplicate.setImage(UIImage(named: "sizing.png"), for: .disabled);

//          let buttonDuplicate = UIButton(frame: self.frame)
//        buttonDuplicate.setImage(self.image(for: .normal), for: .normal);
        buttonDuplicate.contentHorizontalAlignment = self.contentHorizontalAlignment;
        buttonDuplicate.contentVerticalAlignment =  self.contentVerticalAlignment;
//        buttonDuplicate.adjustsImageWhenHighlighted =  self.adjustsImageWhenHighlighted;
//        buttonDuplicate.showsTouchWhenHighlighted =  self.showsTouchWhenHighlighted;
        buttonDuplicate.adjustsImageWhenHighlighted =  false;
        buttonDuplicate.showsTouchWhenHighlighted =  false;
        buttonDuplicate.adjustsImageWhenDisabled =  false;
        buttonDuplicate.contentEdgeInsets = self.contentEdgeInsets;
        buttonDuplicate.imageView?.contentMode = .scaleAspectFit
    //    let controlEvents = [self.allControlEvents];
        let eventArray:[UIControl.Event] = [UIControl.Event.touchDown,UIControl.Event.touchUpInside,
                                            UIControl.Event.touchUpOutside,UIControl.Event.touchDragInside,
                                            UIControl.Event.touchDragOutside];
//print("   ")
//print("   ")
//print("  self.allControlEvents",self.allControlEvents)
//print("  controlEvents.count",controlEvents.count)
//
//print("  self.allTargets",self.allTargets)

        // Copy targets and associated actions
        self.allTargets.forEach
        { target in
//print("  target",target)
            for controlEvent in eventArray
            {
//print("  controlEvent",controlEvent)
                if let actions = self.actions(forTarget: target, forControlEvent: controlEvent)
                {
//print("  actions: ",actions)
                    for action in actions
                    {
//print("  action",action)
                        buttonDuplicate.addTarget(target, action: Selector(action), for: controlEvent)
                    }
                }
            }
        }

        return buttonDuplicate
    }
}


