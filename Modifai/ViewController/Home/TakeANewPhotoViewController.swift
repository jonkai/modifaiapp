
//  Created on 01/04/19.


import UIKit
import FirebaseAnalytics
import WebKit


protocol TakeANewPhotoViewControllerDelegate: class{
    func calProperVC(str:String)
}

class TakeANewPhotoViewController: UIViewController {
   
     weak var delegate: TakeANewPhotoViewControllerDelegate?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
    }

    override var prefersStatusBarHidden: Bool
    {
           return true
    }

	//-----------------------------------------------------------
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        let alert = UIAlertController(title: "", message: nil, preferredStyle: UIAlertController.Style.actionSheet)

		alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler:
		{ (action) in
			self.dismiss(animated: true, completion:
			{
			})
		}))
        
		// ------------ internal App -----------
        if hasInternalApp
        {
			alert.addAction(UIAlertAction(title: "Take New Photo", style: UIAlertAction.Style.default, handler:
			{ (action) in

				self.delegate?.calProperVC(str: "camera")
				self.dismiss(animated: true, completion:
				{
				})
			}))
//            if ( !hasOnlyGarageDoors) //********** change 1.0.0 add if block ***
//            {
//                alert.addAction(UIAlertAction(title: "Create a new order", style: UIAlertAction.Style.default, handler:
//                { (action) in
//
//                    self.dismiss(animated: false, completion:
//                    {
//                        self.delegate?.calProperVC(str: "create_order")
//                    })
//                }))
//
//                alert.addAction(UIAlertAction(title: "Enter an Order Number", style: UIAlertAction.Style.default, handler:
//                { (action) in
//
//                    self.dismiss(animated: false, completion:
//                    {
//                        self.delegate?.calProperVC(str: "agent_order")
//                    })
//                }))
//
//                if let curHome = homeviewController
//                {
//                    curHome.loadGallery()
//                    let urlsArray = curHome.getCreatedOrders()
//
//                    if urlsArray.count > 0
//                    {
//                        alert.addAction(UIAlertAction(title: "Previously viewed orders", style: UIAlertAction.Style.default, handler:
//                        { (action) in
//
//                            self.dismiss(animated: false, completion:
//                            {
//                                self.delegate?.calProperVC(str: "agent_create")
//                            })
//                        }))
//                    }
//                }
//            }
        }


		// ------------ home App -----------
		if isHomeApp
		{
			if let appMode = UserDefaults.standard.value(forKey: "AppMode") as? String
			{
				if appMode == "AgentApp"
				{
					alert.addAction(UIAlertAction(title: "Create a new order", style: UIAlertAction.Style.default, handler:
					{ (action) in

						self.dismiss(animated: false, completion:
						{
							self.delegate?.calProperVC(str: "create_order")
						})
					}))

					alert.addAction(UIAlertAction(title: "Enter an Order Number", style: UIAlertAction.Style.default, handler:
					{ (action) in

						self.dismiss(animated: false, completion:
						{
							self.delegate?.calProperVC(str: "agent_order")
						})
					}))

					if let curHome = homeviewController
					{
						curHome.loadGallery()
						let urlsArray = curHome.getCreatedOrders()

						if urlsArray.count > 0
						{
							alert.addAction(UIAlertAction(title: "Previously viewed orders", style: UIAlertAction.Style.default, handler:
							{ (action) in

								self.dismiss(animated: false, completion:
								{
									self.delegate?.calProperVC(str: "agent_create")
								})
							}))
						}
					}
				} else {

					alert.addAction(UIAlertAction(title: "Enter an Order Number", style: UIAlertAction.Style.default, handler:
					{ (action) in

						self.dismiss(animated: false, completion:
						{
							self.delegate?.calProperVC(str: "agent_order")
						})
					}))
				}

			} else {

				alert.addAction(UIAlertAction(title: "Enter an Order Number", style: UIAlertAction.Style.default, handler:
				{ (action) in

				self.dismiss(animated: false, completion:
				{
					self.delegate?.calProperVC(str: "agent_order")
				})
				}))
			}
		}






	   alert.addAction(UIAlertAction(title: "Choose from our Example Homes", style: UIAlertAction.Style.default, handler:
	   { (action) in

		   self.dismiss(animated: false, completion:
		   {
				self.delegate?.calProperVC(str: "modifai")     //********** change 0.0.40 change ***
		   })
	   }))



		// ------------ internal App -----------
		        // ------------ internal App -----------
        if hasInternalApp
        {
            if ( !hasOnlyGarageDoors) //********** change 1.0.0 add if block ***
            {
                alert.addAction(UIAlertAction(title: "Choose from Agent Homes", style: UIAlertAction.Style.default, handler:
                { (action) in

                    self.dismiss(animated: false, completion:
                    {
                        self.delegate?.calProperVC(str: "agent")  //********** change 0.0.40 change ***
                    })
                }))

                alert.addAction(UIAlertAction(title: "Completed Agent Homes", style: UIAlertAction.Style.default, handler:
                { (action) in

                    self.dismiss(animated: false, completion:
                    {
                        self.delegate?.calProperVC(str: "agenthome")  //********** change 0.0.40 change ***
                    })
                }))

                alert.addAction(UIAlertAction(title: "Deleted Agent Homes", style: UIAlertAction.Style.default, handler:
                { (action) in

                    self.dismiss(animated: false, completion:
                    {
                        self.delegate?.calProperVC(str: "agenthomedelete")  //********** change 0.0.40 change ***
                    })
                }))
            }

           alert.addAction(UIAlertAction(title: "Choose from Camera Roll", style: UIAlertAction.Style.default, handler:
           { (action) in

               self.dismiss(animated: false, completion:
               {
                    self.delegate?.calProperVC(str: "library")   //********** change 0.0.40 change ***
               })
            }))
        }

		if let popoverController = alert.popoverPresentationController  //********** change 3.0.3 add if block ***
		{
		  popoverController.sourceView = self.view
		  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
		  popoverController.permittedArrowDirections = []
		}

        self.present(alert, animated: true, completion: nil)
    }

    //-------------------------------------------------------
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        self.dismiss(animated:false, completion: nil)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    //-------------------------------------------------------
    @IBAction func btnModifaiClicked(_ sender: Any)
    {
     

//        let myPhotoVC = self.storyboard?.instantiateViewController(withIdentifier: "MyPhotoVC") as! MyPhotoViewController
//        self.navigationController?.pushViewController(myPhotoVC, animated: true)
    }

    //-------------------------------------------------------
    @IBAction func btnCancelClicked(_ sender: Any)
    {
        self.dismiss(animated: true, completion:
        {
        })
    }
}

class AgentPopupViewController: UIViewController,UITextFieldDelegate
{
    @IBOutlet var agentBtn: UIButton!
    @IBOutlet var buyerBtn: UIButton!
    @IBOutlet weak var orderNumberTF: UITextField!

    override func viewDidLoad()
       {
           super.viewDidLoad()
           orderNumberTF.placeholder = "Order Number"
           orderNumberTF.keyboardType = .alphabet
          // buyerBtn.isEnabled = false

       }

       override var prefersStatusBarHidden: Bool
       {
              return true
       }

	//----------------------------------------
    @IBAction func btnClickAction(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion:
            {
                if sender == self.agentBtn
                {
                    
                    UserDefaults.standard.set("AgentApp", forKey: "AppMode")
                    UserDefaults.standard.set(true, forKey: "OrderDownload")

//print("btnCli ckAction AgentApp orderDo wnloaded")
                   AppDelegate.orderDownloaded()
                    
                    
                } else {
					UserDefaults.standard.set("BuyerApp", forKey: "AppMode")
//print("btnCli ckAction BuyerApp checkOrd erExist")
                    self.checkOrderExist()
//                    let popOverVC = UIStoryboard(name: "CustomTabBar", bundle: nil).instantiateViewController(withIdentifier: "LocationPopUp") as? LocationPopUp
//
//                    popOverVC?.type = "order"
//                    UIApplication.shared.windows.first?.rootViewController = popOverVC
//                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                }
        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
          
           let currentString: NSString = textField.text! as NSString
           let newString: NSString =
               currentString.replacingCharacters(in: range, with: string) as NSString
//           if newString.length >= 4
//           {
//             //  buyerBtn.isEnabled = true
//           }else
//           {
//            //   buyerBtn.isEnabled = false
//
//           }
           return newString.length <= 4
       }


	//------------------------------------
    func checkOrderExist()
    {
        if orderNumberTF.text?.count == 4
        {
           // Analytics.logEvent("Zip_submit", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
            self.dismiss(animated:true, completion: nil)
            self.view.endEditing(true)
           
                LoadingIndicatorView.show("Please Wait")
                
                DataManager.getAgentHouseList(orderNumber:orderNumberTF.text!, completion: { (error,data) in
                    if data != nil
                    {
                        DataManager.GetPaintColorsForAgentHomes(orderNumber: self.orderNumberTF.text!,completion:{
                            (error,data) in
                            self.dismiss(animated:true, completion: nil)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 4, execute: {
                                // code to execute
                                LoadingIndicatorView.hide()
                                if data != nil {
                                    if let home = homeviewController{
                                        
                                        home.loadDownloadedImage()
                                        return
                                    }else{
                                        UserDefaults.standard.set(true, forKey: "loadedFirstTime")
                                        AppDelegate.orderDownloaded()
                                        
                                    }
                                }else
                                {
                                    AppDelegate.orderDownloaded()
                                    
                                }
                            })
                        
                            
                        })
                      //  self.delegate.onSelectionCompletionClick(sender: "0")

                    }else
                    {
                       
                        self.dismiss(animated:true, completion: nil)

                        AppDelegate.orderDownloaded()
                       LoadingIndicatorView.hide()
                    }

                })
                
            
        }else
        {
          let okAction = UIAlertAction.init(title:"OK", style: UIAlertAction.Style.cancel, handler:nil)
          
          AlertViewManager.shared.ShowAlert(title:"", message: "Please enter a valid Order Number", actions: [okAction])

      }
    }
}
class CreateOrderWebView: UIViewController
{
    var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var webView: WKWebView!
    override func viewDidLoad()
       {
           super.viewDidLoad()
           let url = URL(string: jotFormLink)
           webView.load(URLRequest(url: url!))
       
        activityIndicator = UIActivityIndicatorView()
               activityIndicator.center = self.view.center
               activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.medium
        activityIndicator.color = .black
               webView.addSubview(activityIndicator)
       }

       override var prefersStatusBarHidden: Bool
       {
              return true
       }
    func showActivityIndicator(show: Bool) {
        if show {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        showActivityIndicator(show: false)
    }

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        showActivityIndicator(show: true)
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        showActivityIndicator(show: false)
    }
}



