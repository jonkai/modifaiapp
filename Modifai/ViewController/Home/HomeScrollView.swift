
import UIKit

//-------------------------------------
class HomeScrollView: UIScrollView
{
    override func layoutSubviews()
    {
        super.layoutSubviews();

        homeviewController.paintImageView.frame = homeviewController.imageView.frame
        homeviewController.paintImageView1.frame = homeviewController.imageView.frame
        homeviewController.paintImageView2.frame = homeviewController.imageView.frame

        homeviewController.garageImageView.frame = homeviewController.imageView.frame
        homeviewController.garageMaskView.frame = homeviewController.imageView.frame
        homeviewController.frontDrMaskView.frame = homeviewController.imageView.frame

        homeviewController.logoContainer.frame = homeviewController.imageView.frame
        homeviewController.blurrImgView.frame = homeviewController.imageView.frame
   }
}

