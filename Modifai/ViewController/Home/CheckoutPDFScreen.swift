import Foundation
import UIKit

class CheckoutPDFScreen: UIViewController ,UITableViewDelegate,UITableViewDataSource
{
    var prodCatlogList: [CatelogList] =  [CatelogList]()
    var sortedProdList: NSMutableDictionary = NSMutableDictionary()
    var sortedKeys: Array<String> = Array<String>()

    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    
    @IBOutlet weak var pdfscrollView: UIScrollView!
    
    @IBOutlet weak var proposalsTableView: UITableView!
    
    @IBOutlet weak var requestView: UIView!
    
    
    @IBOutlet weak var nameLbl: UILabel!
    
    @IBOutlet weak var emailLbl: UILabel!
    
    
    @IBOutlet weak var zipCodeLbl: UILabel!
    
    @IBOutlet weak var phoneLbl: UILabel!
    
    @IBOutlet weak var lastNameLbl: UILabel!
    @IBOutlet weak var streetAdd1Lbl: UILabel!
    @IBOutlet weak var streetAdd2Lbl: UILabel!
    @IBOutlet weak var stateLbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white//UIColor.init(red: 26.0/255.0, green: 30.0/255.0, blue: 51.0/255.0, alpha: 1.0)
        proposalsTableView.backgroundColor = .white//UIColor.init(red: 26.0/255.0, green: 30.0/255.0, blue: 51.0/255.0, alpha: 1.0)
        proposalsTableView.separatorColor = UIColor.clear
        proposalsTableView.isHidden = false
        // btnsBgImgView.image = UIImage(named: "btnListBgImgView")
     //   requestView.isHidden = true
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.groupImagesbyHouse()
       
     //   navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    func groupImagesbyHouse()
    {
        sortedProdList = NSMutableDictionary()
        for prod in prodCatlogList
        {
        //  let product = prod as CatelogList
            let tempName = prod.filename
            if sortedProdList[tempName] != nil
            {
                
                var tempArray = sortedProdList.value(forKey:tempName) as! Array<CatelogList>
                tempArray.append(prod)
                sortedProdList.setValue(tempArray, forKey:tempName)

            }else
            {
                let tempArray:Array<CatelogList> = [prod]
                sortedProdList.setValue(tempArray, forKey:tempName)
            }
        }
//print(sortedProdList)
        sortedKeys = sortedProdList.allKeys as! Array<String>
        proposalsTableView.reloadData()
    }
    func setUpTextField(textField: UITextField) -> UITextField  {
        textField.backgroundColor = UIColor.white
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        textField.leftViewMode = .always
        textField.leftView = view
        return textField
    }
    //MARK:- IBActions
    
    
    
    //MARK:- UITableview Delegate and Datasource methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return sortedProdList.count
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        tableHeight.constant = CGFloat((self.prodCatlogList.count * 80) + (sortedKeys.count*170))
           let tempArray = sortedProdList.value(forKey:sortedKeys[section]) as? Array<CatelogList>
        return tempArray!.count
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
            return 80
       
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 150))
       
        let imageView = UIImageView()
        imageView.frame = CGRect.init(x: 0, y:5, width: headerView.frame.width, height: headerView.frame.height - 10)
        imageView.image = loadImageFromDocumentDirectory(nameOfImage:sortedKeys[section])
        imageView.contentMode = .scaleAspectFit
       
    
        headerView.backgroundColor = .white//UIColor.init(red: 26.0/255.0, green: 30.0/255.0, blue: 51.0/255.0, alpha: 1.0)
        headerView.addSubview(imageView)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 160
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            guard let proposalsGarageCell = tableView.dequeueReusableCell(withIdentifier: "CheckoutGarageTableViewCell") as? CheckoutGarageTableViewCell else{
                return UITableViewCell()
            }
            proposalsGarageCell.backgroundColor = UIColor.white
            // UIColor.init(red: 26.0/255.0, green: 30.0/255.0, blue: 51.0/255.0, alpha: 1.0)
        let tempArray = sortedProdList.value(forKey:sortedKeys[indexPath.section]) as? Array<CatelogList>
           proposalsGarageCell.imgView.image = tempArray![indexPath.row].image
         
            proposalsGarageCell.titleLabel.text = tempArray![indexPath.row].name
            proposalsGarageCell.titleLabel.textColor = UIColor.black

            return proposalsGarageCell;
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func loadImageFromDocumentDirectory(nameOfImage : String) -> UIImage {
              let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
              let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
              let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
              if let dirPath = paths.first{
                  let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(nameOfImage+".jpg")
                  let image    = UIImage(contentsOfFile: imageURL.path)
                if image == nil
                {
                    return UIImage.init(named: "loadingTemp")!
                }
                  return image!
              }
              return UIImage.init(named: "loadingTemp")!
          }
    
}

