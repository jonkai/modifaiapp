import UIKit
import CoreData
import Photos
import CoreLocation
import SDWebImage
import Alamofire
import Foundation
import PECimage
import Social
import AuthenticationServices
import FirebaseAnalytics
import MessageUI
import AVFoundation
import AVKit

//  Device IPHONE
let kIphone_4s : Bool =  (UIScreen.main.bounds.size.width == 480)
let kIphone_5 : Bool =  (UIScreen.main.bounds.size.width == 568)
let kIphone_6 : Bool =  (UIScreen.main.bounds.size.width == 667)
let kIphone_6_Plus : Bool =  (UIScreen.main.bounds.size.width == 736)
let kIphone_X: Bool =  (UIScreen.main.bounds.size.width == 812) || (UIScreen.main.bounds.size.height == 812)
let kIphone_XMAX : Bool =  (UIScreen.main.bounds.size.width == 896) || (UIScreen.main.bounds.size.height == 896)

var selectedStyle = ""
var selectedPrice = ""
var selectedBrand = ""
var skipTrace = false;
var canFindPaths:Bool = false
var noPaint:Bool = false
let paintMaxTime = 55
var paintTimeLeft = 55

var theImageProportionX:CGFloat = 1.0
var theImageProportionY:CGFloat = 1.0

var isOneCar:Bool = false
var isTwoCar:Bool = false
var isDoor:Bool = false
var isDouble:Bool = false

var wasALastFavorite = false;

var noProcessing = false;
var comingFromPaintTutorial = false;

var homeviewController:HomeViewController!

var destImageWidth:CGFloat  = 640.0;
var destImageHeight:CGFloat = 480.0;
var paintImageWidth:CGFloat  = 2048.0;
var paintImageHeight:CGFloat = 1536.0;
var targetMaskRect:CGRect = CGRect(origin: CGPoint.zero, size: CGSize(width: 2048.0, height: 1536.0));
var houseMaxTime = 30.0

var projectList00:[ProjectThumb] = [ProjectThumb]()

var smallImageSizeX:CGFloat = 999999.0
var theImageSizeX:CGFloat = 1.0
var theImageSizeY:CGFloat = 1.0

var well0tsel:Bool = false;
var well1tsel:Bool = false;
var well2tsel:Bool = false;
var well3tsel:Bool = false;
var well4tsel:Bool = false;

var wasChangeOfProject:Bool = true;

let tablePortraitTop: CGFloat = 0.0;
var tableLandscapeTop: CGFloat = -25.0;  //*************** change 3.0.3 change ***

var iPadForHome: CGFloat = 450.0;      //*************** change 3.0.3 add ***
var iPhoneForHome: CGFloat = 300.0;      //*************** change 3.0.3 add ***

var garimageID: String = ""
var doorimageID: String = ""

//------------------------------------
class HomeViewController: BaseViewController,UIScrollViewDelegate, FilterDataDelegate,ZipcodeSelectionDelegate,SignInClickDelegate
{
    var fromOrderNumber:Bool = false
    
    var imgLocation:CLLocation!
    let imagePicker = UIImagePickerController()
    var garageCatID: String = ""
    var doorCatID: String = ""
    var sceneID: String = ""
    var garageCatName: String = ""
    var popOverVC : LocationPopUp!
    var signUpOverVC : SignupPopUp!
    var imageViewScale: CGFloat = 1.0
    let maxScale: CGFloat = 3.0
    let minScale: CGFloat = 1.0
    
 
    var swipeStarted = false
    var arrPrice  = [String]()
    var arrStyle  = [String]()
    var arrBrand  = [String]()
    var arrDoorPrice  = [String]()    //*************** change 3.0.3 this was missing, why? ***
    var arrDoorBrand  = [String]()  //*************** change 3.0.3  this was missing, why?  ***

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var btnTry: UIButton!
    var oneCarImgae:UIImage?
    var twoCarImgae:UIImage?
    var oneDoorImgae:UIImage?
    var twoDoorImgae:UIImage?
    
    var selectedPaint:String = ""
    var cur1Garage:SingleGarageObj = SingleGarageObj.init()
    var cur2Garage:Garage = Garage.init()
    var cur1Door:Door = Door.init()
    var cur2Door:Door = Door.init()
    
    var galleryImagesArray = [Images]()
    var oldimageId:String = ""
    var selectedimage = 0
    var garageScrollIndex = 0
    var doorScrollIndex = 0
    var paintScrollIndex = 0
    
    var garageMaskScrollIndex = 0
    var doorMaskScrollIndex = 0
    var paintMaskScrollIndex = 0
    
    var selectedGalleryImage = -1
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var paintImageView: UIImageView!
    @IBOutlet weak var paintImageView1: UIImageView!
    @IBOutlet weak var paintImageView2: UIImageView!
    
    @IBOutlet weak var garageImageView: UIImageView!
    @IBOutlet weak var garageMaskView: UIImageView!
    @IBOutlet weak var frontDrMaskView: UIImageView!
    
    @IBOutlet weak var logoContainer: UIView!
    @IBOutlet weak var logoView: UIImageView!
    
    @IBOutlet weak var processingView: UIView!
    @IBOutlet weak var dot1: UIImageView!
    @IBOutlet weak var dot2: UIImageView!
    @IBOutlet weak var dot3: UIImageView!
    
    var doorsArray = [Door]()
    var doorsDoubleArray = [Door]()
    @IBOutlet weak var tableViewBottomConstrain: NSLayoutConstraint!
    @IBOutlet weak var tableViewTopConstrain: NSLayoutConstraint!
    @IBOutlet weak var shareviewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var blurrImgView: UIImageView!
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var btnGarage: UIButton!
    @IBOutlet weak var btnDoors: UIButton!
    @IBOutlet weak var btnPaint: UIButton!
    @IBOutlet weak var btnList: UIButton!
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnGallery: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet var garageImage: UIImageView!
    @IBOutlet var doorImage: UIImageView!
    @IBOutlet var paintImage: UIImageView!
    
    @IBOutlet weak var tabsViewLeadingConstrain: NSLayoutConstraint!
    @IBOutlet weak var tabsViewTopConstrain: NSLayoutConstraint!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet var scrollImg: HomeScrollView!

    @IBOutlet weak var photoView: UIView!
    @IBOutlet weak var shareVIview: UIView!
    
    
    @IBOutlet weak var filterWidthView: NSLayoutConstraint!
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var filterTabView: UIView!

    @IBOutlet weak var camView: UIView!
    @IBOutlet weak var garDrFrDrView: UIView!
    
    @IBOutlet weak var scrollViewHeightConstarin:NSLayoutConstraint!
//    @IBOutlet weak var scrollViewWidthConstarin:NSLayoutConstraint!  //........ //************** change 3.0.3 remove but leave, maybe used later ***
    
    @IBOutlet weak var garageCellHeightConstraint:NSLayoutConstraint!  //************** change 3.0.3 add ***

    @IBOutlet weak var tabsGarageWidth:NSLayoutConstraint!
    
    @IBOutlet weak var galShareXconst: NSLayoutConstraint!
    @IBOutlet weak var favShareXconst: NSLayoutConstraint!
    @IBOutlet weak var shareXconst: NSLayoutConstraint!
    @IBOutlet weak var editShareXconst: NSLayoutConstraint!

    @IBOutlet weak var lineLabel: UILabel!
    @IBOutlet weak var nodoorLabel: UILabel!
    
    @IBOutlet weak var photoCollectionView: UICollectionView!
    @IBOutlet weak var collectionView: UICollectionView!


	@IBOutlet weak var vertChangeDoors: UIButton! //************** change 3.0.3 add ***
	@IBOutlet weak var vertFrntDoors: UIButton! //************** change 3.0.3 add ***
	@IBOutlet weak var vertHousePaint: UIButton! //************** change 3.0.6 add ***
//	@IBOutlet weak var vertWndws: UIButton! //************** change 3.0.3 add but comment out ***

//	@IBOutlet weak var horizContainer: UIView!  //************** change 3.0.3 add  add but comment out ***
	@IBOutlet weak var vertContainer: UIView! //************** change 3.0.3 add ***

	@IBOutlet weak var hhvc: VertHorzCollectVC! //************** change 3.0.3 add ***  but not used yet ***
	@IBOutlet weak var vvvc: VertHorzCollectVC! //************** change 3.0.3 add ***

    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var  pinchGuest:UIPinchGestureRecognizer! = nil

    @IBOutlet weak var noProcessSwitch: UISwitch!
    @IBOutlet weak var completeBtn: UIButton!


    var newImage = UIImage()
    var image_url: String = ""
    var roisPersphm: String = ""

    var imagesArray: Array<URL> = []
    var imageNamesArray: Array <String> = []

    var homeObj:Home?
    var isRequiredorientation = false
    var firstTimeOrder = false
    let filename = "earth.jpg"
    var emailId = ""
    
    var btnGarageClicked = true
    var btnDoorClicked = false
    var btnPaintListClicked = false
    var btnListClicked = false
    var btnFilterClicked = false
    var loginStarted = false
    var btnFavWasClicked = false
    var filename1 = "earth.jpg"

    var referenceData = [ReferenceItem]()
    var selectedReferenceItem: ReferenceItem?
    
    var timer:Timer?
    var timeLeft = 35
    
    var paintTimer:Timer?
    
    var roationRightType:Bool = false
    var rotationalAngle: Double = 0.0
    
    let imageViewHeightValue = 0.0;
    var isLandScape:Bool = false
    var imagheViewHeight:CGFloat = 0.0
    var metadata: [String: Any] = [:]
    var asset: PHAsset?
    
    var slopeTopAvg:Double = 0.0;
    var slopeBotAvg:Double = 0.0;
    var processedImage: UIImage!
    
    
    var sourceSet = Set([garDoorSourceDdr,garDoorSourceSdr,frntDoorSourceDdr,frntDoorSourceSdr])
    var sourceArray  = [[UIImage]]()
    var showDrButton: Bool = true
    
    var lastInputMode: String? = "random";
    var isDictationRunning: Bool = false
    var lastKeybrdHeight: CGFloat = 0.0;
    //-----------------------------------------

    //------- processing screen effects -------
    let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular) //prominent,regular,extraLight, light, dark
    var blurEffectView = UIVisualEffectView()
    var dotLayers = [CALayer]()
    var dotsScale = 1.5
    var dotsCount: Int = 3
    
    var currProjectKount: Int = 0;
    
    var currDoorType: Int = 100;
    
    var currComment1Type: Int = 0;
    var currComment2Type: Int = 0;
    var currComment: String = "no_comment_made";
    var brdCntrl:Float = 0.22;
    var heightToAdjust:CGFloat = 330.0;
    var addedImageFromCameraStr = "defaultImage"


    
    //MARK:- LifeCycle methods
    //----------------------------------------------------------------
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    //---------------------------------------------------------------------
    override func viewDidLoad()
    {
//print("view Did Load")
        super.viewDidLoad()
        homeviewController = self

        heightToAdjust = UIScreen.main.bounds.size.height*0.35
        self.imageView.contentMode = .scaleAspectFit
        self.imageView.clipsToBounds = true
        
        self.tableViewTopConstrain.constant  = 0
        
        self.blurrImgView.isHidden = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(blurrImgGesture))
        self.blurrImgView.isUserInteractionEnabled = true
        tap.numberOfTapsRequired = 1
        self.blurrImgView.addGestureRecognizer(tap)
        
        //**************** shareview guesture Method Lakshmi - 2.0.4
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(shareImgGesture))
        self.shareVIview.isUserInteractionEnabled = true
        tap2.numberOfTapsRequired = 1
        self.shareVIview.addGestureRecognizer(tap2)
        
        self.setupDotLayers();
        
        let imageBtn = self.btnMenu.imageView?.image //********Change 2.2.1
        self.btnMenu.setImage(imageBtn?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal) //********Change 2.2.1
        self.btnMenu.tintColor = titleColor //********Change 2.2.1
        
        if (originalHomeImage != nil)
        {
            self.saveImageToDocumentDirectory(image0:originalHomeImage,type:".jpg")
        }
        
        lineLabel.isHidden = false
        
        
        let status = UserDefaults.standard.bool(forKey: "removeCamView")
        if status
        {
            camView.isHidden = true
        }
        
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        self.view.backgroundColor = UIColor.init(red: 232.0/255.0, green: 234.0/255.0, blue: 238.0/255.0, alpha: 1.0)
        
        homeTableView.backgroundColor = UIColor.init(red: 232.0/255.0, green: 234.0/255.0, blue: 238.0/255.0, alpha: 1.0)
        homeTableView.separatorColor = UIColor.clear
        
        self.setupButtonTitlesForImage()
        
        self.selectedReferenceItem = nil
        self.selectedReferenceItem = ReferenceItem.init(name: "Camera")
        self.selectedReferenceItem?.image = self.imageView.image
        
        isTwoCar = false
        isOneCar = false
        shouldSave = false;
        
        self.photoView.isHidden = true
        self.setUpFonts()
        homeTableView.layer.borderWidth = 2.0
        homeTableView.layer.borderColor = UIColor.clear.cgColor
        
        btnPaint.setTitleColor(UIColor.init(red: 134.0/255.0, green: 141.0/255.0, blue: 179.0/255.0, alpha: 1.0), for: .normal)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.orientationChanged(notification:)),
                                               name: UIDevice.orientationDidChangeNotification,
                                               object: nil)
        if hasInternalApp
        {
            btnEdit.isHidden = false
        } else {
            btnEdit.isHidden = false
        }
        
        if isHomeApp
        {
            if (UserDefaults.standard.bool(forKey: "showAgentAlert")) == false
            {
                UserDefaults.standard.set(true, forKey: "showAgentAlert")
                
                //"AgentApp"
                if let appMode = UserDefaults.standard.value(forKey: "AppMode") as? String
                {
                    if appMode == "AgentApp"
                    {
                        self.btnMenuclicked(btnGallery as Any)
                    }
                }
            }
        }
         
        if !hasOnlyGarageDoors
        {
            let nib = UINib(nibName: "PaintLandScapeCell", bundle: nil)
            homeTableView.register(nib, forCellReuseIdentifier: "PaintLandScapeCell")
        }

        if (isHomeAgent)  //********** change 3.0.7 this was missing... why?
        {
            self.loadAgentHomes()
        }
    }

    //---------------------------------------------------------
    override func viewWillAppear(_ animated: Bool)
    {
//print("view Will Appear")
        if (comingFromPaintTutorial) {comingFromPaintTutorial = false; return;}

        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.all)
        
        wasALastFavorite = false;
        homeviewController = self //-------------- keep even though repeated ---
        paintTimeLeft = paintMaxTime;
        self.paintTimer?.invalidate()
        
        if loginStarted
        {
            LoadingIndicatorView.show("Please wait")
        }
        
        noProcessing = UserDefaults.standard.bool(forKey:"noProcessing");


        if UIDevice.current.userInterfaceIdiom == .phone  //*************** change 3.0.3 add if block ***
        {
            wasIpad = false;
        } else {
            wasIpad = true;
        }


        if (noProcessing)
        {
            self.noProcessSwitch.isOn = false;
        } else {
            self.noProcessSwitch.isOn = true;
        }
        
        
        if hasInternalApp
        {
            noProcessSwitch.isHidden = false
        } else {
            noProcessSwitch.isHidden = true
        }
        
        if (hasOnlyGarageDoors)
        {
   //         tableLandscapeTop = -25.0 //*************** change 3.0.3 remove ***
            noProcessSwitch.isHidden = true
        } else {
   //         tableLandscapeTop = 0.0  //*************** change 3.0.3 remove ***
        }

		navigationController?.setNavigationBarHidden(false, animated: animated)  //*************** change 3.0.3 this is in garage app ***
        let status = UserDefaults.standard.bool(forKey: "isCameraRequire")
        if status
        {
            UserDefaults.standard.set(false, forKey: "isCameraRequire")
            self.openCamera()
        }

		self.view.frame.origin = CGPoint.zero; //*************** change 3.0.3 add ***

        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
        scrollImg.setZoomScale(1.0, animated: false) // change zoom pixels

        self.photoView.isHidden = true
        self.shareView.isHidden = false
        
        self.setupButtonTitlesForImage()
        
        self.homeTableView.scroll(to: .top, animated: true)
        
        if (self.btnFavWasClicked)
        {
            self.checkFavStatus(indexs: 0)
        }
        
        
        self.tableViewTopConstrain.constant = 0

        
        if (fromFavorites)
        {
            fromFavorites = false
            if garageScrollIndex > 0
            {
                self.btnGarageClicked(self)
            }
            
            if doorScrollIndex > 0
            {
                self.btnDoorsClicked(self)
            }
        }
        
        
//print("editedGarage  :",editedGarage)
//print("editedGarage currP roject  :",currProject as Any)
        
        screenScale = UIScreen.main.scale
        if let image = originalHomeImage
        {
            if (image.size.width > 1048) || (image.size.height > 1048)
            {
                screenScale = 1.0;
            }
        }
        
        commingFromProjectList = true;
//print("home viewWillappear commingFro mProjectList  :",commingFromProjectList)
//print("home viewWillappear comingFromfeed  :",comingFromfeed)
//print("home viewWillappear wasChangeOfProject  :",wasChangeOfProject)
//print("home viewWillappear currProject  :",currProject as Any)

        if comingFromfeed
        {
            self.loadDownloadedImage()
        } else {

        if (currProject == nil)
        {
          self.filterClearData()  //***************  change 1.0.2 add ***
          smallImageSizeX = 999999.0   //***************  change 1.0.2 add ***

          currentImageName = "modiGall2.jpg"
          originalHomeImage = UIImage(named:currentImageName);
          if (originalHomeImage.size.width > 1048) || (originalHomeImage.size.height > 1048)
          {
              screenScale = 1.0;
          }
          targetMaskRect.size = originalHomeImage.size;
          self.imageView.image = originalHomeImage;

          self.fetchHouseObject
          { (house0Object) in
              self.selectedExample(img: originalHomeImage,imgName: currentImageName)
              wasChangeOfProject = false;  //***************  change 1.0.2 add ***
          }
        } else if (wasChangeOfProject) {  //***************  change 1.0.2 change ***
            self.filterClearData()  //***************  change 1.0.2 add ***
            smallImageSizeX = 999999.0   //***************  change 1.0.2 add ***
//print("home viewWil lAppear  house Mask:",houseMask as Any)
            
            oneCarImgae = UIImage(named: "1_s0.jpg")
            twoCarImgae = UIImage(named: "1_d0.jpg")
            oneDoorImgae = UIImage(named: "f_s0.jpg")
            twoDoorImgae = UIImage(named: "f_d10.jpg")
            
            isTwoCar = false;
            isOneCar = false;
            isDoor = false;
            isDouble = false;
            
            currentTwoCarGar = 0;
            currentOneCarGar = 0;
            currentDoublFrntDoor = 0;
            currentSinglFrntDoor = 0;
            currentCombinedFrntDoor = 0;
            
            currentWindow = 0;
            
            currentToolIndex = -1
            currentWellNum = 0;
            currentButtonToDisplay = 0;
            
            
            currProjectThumb = self.loadThumbForProject(currProject?.projectID)
            curProjectChoiceList = self.loadThumbChoices(currProject?.projectID) //********** change 1.1.4 add,  needs to be universal ***

            self.imageView.image = originalHomeImage;
            self.projectSetUp(currProject)

            if let currPro = currProject
            {
                if (currPro.wasProcessed)
                {
                } else {
                    self.encodeThePhoto()
                }
            }
            projectList00 = self.loadProjects()
            //print("2 viewWillAppear projectList00.count:",projectList00.count)
        } else {
//print("view will load self.paintScrol lend")
            self.paintScrollend(index:self.paintScrollIndex)  //........... what in the world??? ...
        }
        


//print("viewWillAppear editedGarage:",editedGarage)
        
        if (editedGarage)
        {
            if let curGarage = currentMainImageController //*************** change 1.1.0 add ***
            {
                self.garageImageView.image = curGarage.scrollView.garageImageView.image;
            }

            editedGarage = false
            self.garageMaskView.image = nil;     //*************** change 0.0.30 add ***
            self.frontDrMaskView.image = nil;    //*************** change 0.0.30 add ***
            
            maskedDoorImage  = nil;   //*************** change 0.0.30 add ***
//            maskedDo orImage1 = nil;  //*************** change 1.1.0 remove ***
            
            maskedFrontDrImage  = nil;   //*************** change 0.0.30 add ***
//            maskedFro ntDrImage1 = nil;  //*************** change 1.1.0 remove ***
            
            //------------- initialize the startColor for garage and door ---- until read from table entry ----
            doorStartColor0 = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.0);
            defaultDoorColor0 = doorStartColor0;
//print(" homeView viewWill appear  defaultDo orColor0:",defaultDoorColor0 as Any)

            frontDrStartColor0 = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.0);
            defaultFrontDrColor0 = frontDrStartColor0;
            
            if (editedGarageAdded && self.garageScrollIndex < 1)
            {
                self.garageScrollIndex = 1;
            }
            
            if (editedDoorAdded && self.doorScrollIndex < 1)
            {
                self.doorScrollIndex = 1;
                
                if (self.garageScrollIndex < 1)
                {
                    //print("self.btnDoorsClicked")
                    self.btnDoorsClicked(self)
                }
            }
            
            editedGarageAdded = false;
            editedDoorAdded = false;
            
            self.changeDoorTypes()  //********** change 3.0.4 move this line to here ***
            self.checkDoorsAndGarages(from:"edit")  //********** change 3.0.4 move this line to here *** //.......... need to check this one ...

            self.garageScrollend(index:self.garageScrollIndex)
            self.doorScrollend(index:self.doorScrollIndex)

            self.saveHouseToDocumentDirectory(house0:houseObject);

        } else {
//print("viewWillAppear self.doorScrollIndex:",self.doorScrollIndex)

            if let curPaint = currentPaintImageController   //********** change 1.1.0 add if blocks below  ***
            {
                if (curPaint.maskOfDoor)  //********** change 1.1.0 add if blocks below  ***
                {
//print("viewWillAppear curPaint.gallIndex:",curPaint.gallIndex)
                    self.garageScrollIndex = curPaint.gallIndex;
                    self.garageMaskScrollIndex = curPaint.gallIndex;
                    self.garageScrollend(index:self.garageScrollIndex)
                   } else if (curPaint.maskOfFrontDoor) {
                    self.doorScrollIndex = curPaint.gallIndex;
                    self.doorMaskScrollIndex = curPaint.gallIndex;
                    self.doorScrollend(index:self.doorScrollIndex)
                } else {
                    self.garageScrollend(index:self.garageScrollIndex)
                    self.doorScrollend(index:self.doorScrollIndex)
                }
            } else {

                self.garageScrollend(index:self.garageScrollIndex)
                self.doorScrollend(index:self.doorScrollIndex)
            }

            self.checkDoorsAndGarages(from:"viewWillAppear")
            self.readJsonFile()
        }


         if projectList00.count > 0
        {
        checkIfHouseObject()
        }
        
          wasChangeOfProject = false;  //***************  change 1.0.2 add ***
        
        if hasInternalApp
        {
            let agentId = DataManager.checkAgentFileUrlWithName(imageName:currentImageName)
            if agentId.count > 0
            {
                completeBtn.isHidden = false
//print("viewWillappear completeBtn")
            } else {
                completeBtn.isHidden = true
            }
        } else {
            
            let orderFirstTime = UserDefaults.standard.bool(forKey: "loadedFirstTime")
            if orderFirstTime
            {
                UserDefaults.standard.set(false, forKey: "loadedFirstTime")
                fromOrderNumber = true
                //print("viewWillAppear before loadDownlo adedImage")
                self.loadDownloadedImage()
            }
        }
        

        
            if let curPaint = currentPaintImageController   //********** change 1.1.0 rearrange the if blocks below  ***
            {
                self.paintImageView.image  = curPaint.scrollView.paintImageOverlay0.image;
                self.paintImageView1.image = curPaint.scrollView.paintImageOverlay1.image;
                self.paintImageView2.image = curPaint.scrollView.paintImageOverlay2.image;

                self.paintImageView.isHidden = curPaint.scrollView.paintImageOverlay0.isHidden
                self.paintImageView1.isHidden = curPaint.scrollView.paintImageOverlay1.isHidden
                self.paintImageView2.isHidden = curPaint.scrollView.paintImageOverlay2.isHidden

                if (curPaint.maskOfDoor)
                {
                    self.garageMaskView.image  = curPaint.scrollView.garageMaskView.image;
//                    self.garageIm ageView.image  = curPaint.scrollView.garageIma geView.image;
//print("2b curPaint = currentPaintIm ageController   curPaint.scrollView.garageIma geView.image:",curPaint.scrollView.garageIm ageView.image as Any)
//print("2b curPaint = currentPaintIma geController   self.garageIma geView.image:",self.garageIma geView.image as Any)
                } else if (curPaint.maskOfFrontDoor) {
                    self.frontDrMaskView.image = curPaint.scrollView.frontDrMaskView.image;
//                    self.garageI mageView.image  = curPaint.scrollView.garageIm ageView.image;
                }
            }

            if (hasOnlyGarageDoors) //********** change 1.0.0 add if block ***
            {
                self.paintImageView.image  = nil;
                self.paintImageView1.image = nil;
                self.paintImageView2.image = nil;

                self.paintImageView.isHidden = true
                self.paintImageView1.isHidden = true
                self.paintImageView2.isHidden = true
            }
        }

        fromOrderNumber = false
    }
    
    //--------------------------------------------------------
    func checkIfHouseObject()
    {
		let house_filename = currentImageName.replacingOccurrences(of:".jpg", with: "_houseObject.txt")
//print("fetch From J son house_filename:",house_filename)
		if let house0Object = self.checkHouseProjectExist(filename:house_filename)
		{
				houseObject = house0Object

				let lastImageUrl = house0Object.image_url[0] as String
				//print("existed savingUrl",lastImageUrl)

				UserDefaults.standard.set(lastImageUrl, forKey:"Last_Image_url")

		}
    }

    //--------------------------------------------------------
    func setupButtonAction()
    {
        btnFav.isUserInteractionEnabled = true
        btnFav.addTarget(self, action: #selector(btnFavClicked(_:)), for: .touchUpInside)
    }

	//------------------------------------
	func setupShareView()
	{
		var xValue = (self.view.frame.size.width - 160)/5
		if hasInternalApp
		{
			if phonePortrait
			{
				galShareXconst.constant = xValue
			} else {
				xValue = (self.view.frame.size.width - (120 + 160))/4
				galShareXconst.constant = -40
			}
			btnEdit.isHidden = false

		} else {
			xValue = (self.view.frame.size.width - 120)/4
			if phonePortrait
			{
				galShareXconst.constant = xValue
			} else {
				xValue = (self.view.frame.size.width - (80 + 160))/3
				galShareXconst.constant = -40
			}

			btnEdit.isHidden = true
		}

		shareXconst.constant = xValue
		favShareXconst.constant = xValue
		editShareXconst.constant = xValue
		shareView.isUserInteractionEnabled = true
	}
    
    //--------------------------------------------------------
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)

		navigationController?.setNavigationBarHidden(false, animated: animated)
		UINavigationBar.appearance().barTintColor = themeColor
		UINavigationBar.appearance().tintColor = titleColor
		UINavigationBar.appearance().isTranslucent = false

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute:
        {
        	let size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height);
        	self.shallRotate(size)
        }) //**************** add 2.0.9

        self.paintImageView.frame = self.imageView.frame
        self.paintImageView1.frame = self.imageView.frame
        self.paintImageView2.frame = self.imageView.frame

        self.garageImageView.frame = self.imageView.frame
        self.garageMaskView.frame = self.imageView.frame
        self.frontDrMaskView.frame = self.imageView.frame

        self.logoContainer.frame = self.imageView.frame
        self.blurrImgView.frame = self.imageView.frame
        
        
    }
    
    //--------------------------------------------------------
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
    }
    
    //-------------------------------------------------------------------------------
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        super.viewWillTransition(to: size, with: coordinator)
        
        phonePortrait = size.height > size.width;
        
        
        if (phonePortrait != lastPhonePortrait)
        {
            self.shallRotate(size);
        }
        //print("viewWillTransition self.paintImageView.isHidden:",self.paintImageView.isHidden)
    }
    
    // MARK: - effects
    //************ change 0.0.30 add function ***
    //------------------------------------------------------------------------------------
    func addBlurEffect()
    {
        self.blurEffectView = UIVisualEffectView(effect: self.blurEffect)
        self.blurEffectView.frame = self.view.bounds
        
        self.blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(self.blurEffectView)
        self.view.addSubview(self.processingView)
        self.processingView.isHidden = false;
        self.btnMenu.isHidden = true;
    }
    
    //************ change 0.0.30 add function ***
    //------------------------------------------------------------------------------------
    func removeBlurEffect()
    {
        blurEffectView.removeFromSuperview()
        self.processingView.isHidden = true;
        self.btnMenu.isHidden = false;
    }
    
    //************ change 0.0.30 add function ***
    //-----------------------------------------------------
    func setupDotLayers()
    {
        dotLayers.append(dot1.layer)
        dotLayers.append(dot2.layer)
        dotLayers.append(dot3.layer)
    }
    
    //************ change 0.0.30 add function ***
    //-----------------------------------------------------
    func scaleAnimation(_ after: TimeInterval = 0) -> CAAnimationGroup
    {
        let scaleUp = CABasicAnimation(keyPath: "transform.scale")
        scaleUp.beginTime = after
        scaleUp.fromValue = 1
        scaleUp.toValue = dotsScale
        scaleUp.duration = 0.3
        scaleUp.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        
        let scaleDown = CABasicAnimation(keyPath: "transform.scale")
        scaleDown.beginTime = after+scaleUp.duration
        scaleDown.fromValue = dotsScale
        scaleDown.toValue = 1.0
        scaleDown.duration = 0.2
        scaleDown.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        
        let group = CAAnimationGroup()
        group.animations = [scaleUp, scaleDown]
        group.repeatCount = Float.infinity
        
        let sum = CGFloat(dotsCount)*0.2 + CGFloat(0.4)
        group.duration = CFTimeInterval(sum)
        
        return group
    }
    
    //************ change 0.0.30 add function ***
    //-----------------------------------------------------
    func startAnimatingDots()
    {
        var offset :TimeInterval = 0.0
        dotLayers.forEach
            {
                $0.removeAllAnimations()
                $0.add(scaleAnimation(offset), forKey: "scaleAnima")
                offset = offset + 0.25
        }
    }
    
    //************ change 0.0.30 add function ***
    //-----------------------------------------------------
    func stopAnimatingDots()
    {
        dotLayers.forEach { $0.removeAllAnimations() }
    }
    
    
    
    //MARK: view Setup Methods
    //-------------------------------------------------------------------------------
    @objc func  blurrImgGesture ()
    {
        self.blurrImgView.isHidden = true
        camView.isHidden = true
        self.photoView.isHidden = true
        self.shareView.isHidden = false
        self.setupButtonTitlesForImage()
        //self.selectedRefer enceItem = nil
    }
    //MARK: Share view Setup Methods  //**************** shareview guesture Method Lakshmi - 2.0.4
    //-------------------------------------------------------------------------------
    @objc func  shareImgGesture ()
    {
        self.shareVIview.isHidden = true
        //self.selectedRefer enceItem = nil
    }
    
    //-------------------------------------------------------------------------------
    @IBAction func scaleImage(_ recognizer: UIPinchGestureRecognizer)
    {
        if self.isLandScape
        {
            if recognizer.state == .began || recognizer.state == .changed
            {
                let pinchScale: CGFloat = recognizer.scale
                
                if imageViewScale * pinchScale > minScale {
                    imageViewScale *= pinchScale
                    self.imageView.transform = (self.imageView.transform.scaledBy(x: pinchScale, y: pinchScale))
                }
                recognizer.scale = 1.0
            }
        } else {
            self.imageView.transform = (self.imageView.transform.scaledBy(x: 1.0, y: 1.0))
        }
    }
    
    //-------------------------------------------------------------------------------
    func imageOrientation(image: UIImage) -> UIImage?
    {
        print(image.imageOrientation.rawValue)
        
        if image.imageOrientation.rawValue != 0 {
            var imageToDisplay: UIImage? = nil
            if let cg = image.cgImage {
                imageToDisplay = UIImage(cgImage: cg, scale: image.scale, orientation: .up)
                
            }
            return imageToDisplay
        }
        return image
    }
    
    //-------------------------------------------------------------------------------
    func setUpFonts()
    {
        btnTry.titleLabel?.font =  UIFont(name: "Lato-Bold", size: 14)
        btnGarage.titleLabel?.font =  UIFont(name: "Lato-Bold", size: 18)
        btnDoors.titleLabel?.font =  UIFont(name: "Lato-Bold", size: 18)
        btnPaint.titleLabel?.font =  UIFont(name: "Lato-Bold", size: 18)
        
    }
    
    //-------------------------------------------------------------------------------
    func textToImage(drawText text: String, inImage image: UIImage) -> UIImage
    {
        let textColor = UIColor.black
        let textFont = UIFont(name: "Helvetica Bold", size: 40)!
        
        let imageSize = CGSize(width:image.size.width, height: image.size.height + 120)
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(imageSize, false, scale)
        
        let textFontAttributes = [
            NSAttributedString.Key.font: textFont,
            NSAttributedString.Key.foregroundColor: textColor,
            ] as [NSAttributedString.Key : Any]
        image.draw(in: CGRect(origin: CGPoint.zero, size: imageSize))
        
        let rect = CGRect(origin: CGPoint(x: 10, y: 10), size: imageSize)
        text.draw(in: rect, withAttributes: textFontAttributes)
        
        let rect2 = CGRect(origin: CGPoint(x: 10, y: imageSize.height-80), size: imageSize)
        "Created by \(brand) Mobile app for iOS".draw(in: rect2, withAttributes: textFontAttributes)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    //MARK: Filter Action
    //-------------------------------------------------------------------------------
    func filterData(dicData : [String : String])
    {
        //print("filter Data")

        DataManager.getHomeCatalogsAfterFilter(filterDic: dicData, completion:
            {  (error, data) in
                let jsonDecoder = JSONDecoder()
                let model: ResponseData?
                do {
                    model = try jsonDecoder.decode(ResponseData.self, from: data!)
                    self.homeObj = Home(responseData: model!)
                    //print(model as Any)
                    
                    DispatchQueue.main.async
                        {
                            self.doorsArray = []
                            self.doorsDoubleArray = []

                            for doorsDouble in self.homeObj!.doors
                            {
                                let tempdouble:Door = doorsDouble
                                if tempdouble.typeID == 3
                                {
                                    self.doorsArray.insert(tempdouble, at: 0)
                                }else
                                {
                                    self.doorsDoubleArray.insert(tempdouble, at: 0)
                                }
                            }
                            self.garageScrollIndex = 0
                            self.doorScrollIndex = 0
                            self.paintScrollIndex = 0
                            
                            //print("why reset?")
                            self.homeTableView.reloadData()
                    }
                    
                } catch {
                    print("0 encoding error:\(error)")
                    return
                }
        })
    }
    
    //-------------------------------------------------------------------------------
    func filterClearData()
    {
        //print("filter Clear Data")
        selectedStyle = ""
        selectedPrice = ""
        if singleBrand // **********Change 2.2.1
        {
            selectedBrand = brand
        }else
        {
            selectedBrand = ""
        }
        let dic = ["brand": selectedBrand, "style" : selectedPrice, "price" : selectedPrice]
        self.filterData(dicData:dic)
    }
    
    //-------------------------------------------------------------------------------
    @IBAction func btnFilteerClicked(_ id: UIButton)
    {
        let filter = FilterCatalogViewController.storyboardInstance()
        filter.delegate = self
        

        if btnDoorClicked //**********Change 2.17  Change 3.0.3  this was missing,  why?
        {
            filter.arrPrice = self.arrDoorPrice
            filter.arrBrand = self.arrDoorBrand
        } else {
			filter.arrPrice = self.arrPrice
        	filter.arrBrand = self.arrBrand
        }

        let navigationController = UINavigationController(rootViewController: filter)
        navigationController.navigationBar.isHidden = true
        navigationController.setNavigationBarHidden(true, animated: false)
        navigationController.modalPresentationCapturesStatusBarAppearance = true
        
        self.present(navigationController, animated: false)
        {
            
        }
        
    }
    
    // MARK: - IBAction
    //------------------------------------
    @IBAction func proccessSwitch(_ sender: UISwitch) //****** change 0.0.21 add ***
    {
        //********** change 0.0.42 change ***
        if (sender.isOn)
        {
            noProcessing = false;
        } else {
            noProcessing = true;
        }
        
        UserDefaults.standard.set(noProcessing, forKey:"noProcessing")
    }
    
    //-------------------------------------------------------------------------------
    func showLoginController()
    {
        self.signUpOverVC = UIStoryboard(name: "CustomTabBar", bundle: nil).instantiateViewController(withIdentifier: "SignupPopUp") as? SignupPopUp
        self.signUpOverVC.view.frame = self.view.frame
        self.signUpOverVC.modalPresentationStyle = .overCurrentContext //.po pover   //********** change 3.0.3 change  ***
        self.signUpOverVC.fromOnboard = false
        self.signUpOverVC.delegate = self
        self.signUpOverVC.modalPresentationCapturesStatusBarAppearance = true
		self.signUpOverVC.preferredContentSize = self.view.bounds.size   //********** change 3.0.3 add  ***

        self.present(self.signUpOverVC, animated: true, completion: nil)
    }

    //-------------------------------------------------------------------------------
    func getSingleImg(image1: UIImage,imgText:String) -> UIImage
    {
        let imageNew = image1.resizeImageWithsize(newSize: imageView.frame.size)
        let calculateHeight = 40 + (imageNew.size.height) + 100
        let size = CGSize(width: imageNew.size.width , height: calculateHeight)
        // Setup the font specific variables
        let textColor = UIColor.black
        let textFont = UIFont(name: "Helvetica Bold", size: 30)!
        // Setup the font attributes that will be later used to dictate how the text should be drawn
        let textFontAttributes = [
            NSAttributedString.Key.font: textFont,
            NSAttributedString.Key.foregroundColor: textColor,
        ]
        
        UIGraphicsBeginImageContext(size)
        //Before
        let rect = CGRect(x: 10, y: 0, width: imageView.frame.size.width - 10, height: 40)
        // Draw the text into an image
        imgText.draw(in: rect, withAttributes: textFontAttributes)
        
        //space
        imageNew.draw(in: CGRect(x: 0, y: rect.height+5, width: imageNew.size.width, height: imageNew.size.height ))
        //space
        //after
        //space
        let rect2 = CGRect(x: 0, y: (rect.origin.y + 10 + rect.size.height) + imageNew.size.height , width: imageView.frame.size.width - 10, height:45)
        "Created by \(brand)".draw(in: rect2, withAttributes: textFontAttributes)
        let rect3 = CGRect(x: 0, y: (rect2.origin.y + 5 + rect2.size.height), width: imageView.frame.size.width - 10, height: 40)
        "Mobile app for iOS".draw(in: rect3, withAttributes: textFontAttributes)
        //promotion text
        
        let finalImage = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        return finalImage!
    }
    
    //-------------------------------------------------------------------------------
    func getMixedImgNew(image1: UIImage, image2: UIImage) -> UIImage
    {
        var isimgPotraite = false
        var sizecompress = image1.size.width/500
        
        
        if image1.size.height > image1.size.width
        {
            isimgPotraite = true
            sizecompress = image1.size.width/250
        }
        let imgWidth = image1.size.width/sizecompress
        let imgHeight = image1.size.height/sizecompress
        let resized_image1 = image1.resizeImageWithsize(newSize: CGSize(width: imgWidth, height: imgHeight))
        let resized_image2 = image2.resizeImageWithsize(newSize: CGSize(width:imgWidth, height: imgHeight))
        
        
        var calculateHeight = CGFloat(resized_image1.size.height * 2)
        if isimgPotraite
        {
            calculateHeight = CGFloat(resized_image1.size.width * 2)
        }
        // 40 + (image1New.size.height) + 40 + (image2New.size.height) + 100
        // let size = CGSize(width: image1New.size.width , height: calculateHeight)
        var size = CGSize(width: resized_image1.size.width, height: CGFloat(resized_image1.size.height * 2))
        if isimgPotraite
        {
            size = CGSize(width: CGFloat(resized_image1.size.width * 2) , height:resized_image1.size.height)
        }
        // Setup the font specific variables
        let textColor = UIColor.white
        let textFont = UIFont.italicSystemFont(ofSize:calculateHeight*0.09)
        // (name: "Snell Roundhand", size: calculateHeight*0.1)!
        // Setup the font attributes that will be later used to dictate how the text should be drawn
        let textFontAttributes = [
            NSAttributedString.Key.font: textFont,
            NSAttributedString.Key.foregroundColor: textColor,
        ]
        
        UIGraphicsBeginImageContext(size)
        
        //Before Image Draw
        if isimgPotraite
        {
            //space
            resized_image1.draw(in: CGRect(x:0, y:0, width:resized_image1.size.width, height: resized_image1.size.height))
            
        }else
        {//space
            resized_image1.draw(in: CGRect(x:0, y:0, width: resized_image1.size.width, height: resized_image1.size.height ))
            
        }
        
        //Space b/w two images
        var separatorLineRect = CGRect(x:0 , y:imgHeight , width: resized_image1.size.width, height: 2)
        if isimgPotraite
        {
            separatorLineRect = CGRect(x: imgWidth , y:0, width: 2, height: resized_image1.size.height)
        }
        " ".draw(in: separatorLineRect, withAttributes: textFontAttributes)
        
        // After Image Draw
        if isimgPotraite
        {
            resized_image2.draw(in: CGRect(x: imgWidth+separatorLineRect.size.width, y: 0 , width:resized_image2.size.width, height: resized_image2.size.height))
            
        }else
        {
            resized_image2.draw(in: CGRect(x:0, y: imgHeight + separatorLineRect.height  , width: resized_image2.size.width, height: resized_image2.size.height ))
            
        }
        //Before Text
        let beforeRect = CGRect(x:10, y: 10, width: calculateHeight, height:calculateHeight*0.1)
        "Before".draw(in: beforeRect, withAttributes: textFontAttributes)
        
        //After Text
        var afterRect = CGRect(x:10, y: resized_image2.size.height+10, width: calculateHeight, height: calculateHeight*0.1)
        if isimgPotraite
        {
            afterRect = CGRect(x: resized_image2.size.width+10, y: 10, width: imgWidth, height: calculateHeight*0.1)
        }
        "After".draw(in: afterRect, withAttributes: textFontAttributes)
        
        
        let finalImage = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        //Changing image to square and addnig brand
        var addSpace = CGFloat(200)
        if isimgPotraite
        {
            addSpace = CGFloat(100)
        }
        UIGraphicsBeginImageContext(CGSize(width: calculateHeight+addSpace, height:calculateHeight+addSpace))
        //UIColor.gray.setFill()
        let viewrect = CGRect(x: 0, y: 0, width: calculateHeight+addSpace, height: calculateHeight+addSpace)
        let bgview = UIView(frame: viewrect)
        bgview.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        bgview.layer.render(in: UIGraphicsGetCurrentContext()!)
        let imgRectFinal = CGRect(x:(viewrect.size.width-finalImage!.size.width)/2, y:40, width: finalImage!.size.width, height: finalImage!.size.height)
        
        finalImage!.draw(in:imgRectFinal)
        let logoRect = CGRect(x:imgRectFinal.origin.x-20, y:imgRectFinal.origin.y + imgRectFinal.size.height + 40, width:80, height: 80)
        UIImage(imageLiteralResourceName:"defaultDoor").draw(in:logoRect)
        
        let brandtextFontAttributes = [
            NSAttributedString.Key.font: UIFont.italicSystemFont(ofSize:40),
            NSAttributedString.Key.foregroundColor: UIColor.gray,
        ]
        //        if isimgPotraite
        //        {
        let brandRect1 = CGRect(x: logoRect.origin.x + 100, y: (logoRect.origin.y)-10 , width: calculateHeight, height:45)
        "Created by \(brand)".draw(in: brandRect1, withAttributes: brandtextFontAttributes)
        let brandRect2 = CGRect(x: logoRect.origin.x + 100, y: (logoRect.origin.y)+45 , width: calculateHeight, height:45)
        "Mobile app for iOS".draw(in: brandRect2, withAttributes: brandtextFontAttributes)
        //        }else
        //        {
        //            let brandRect = CGRect(x: logoRect.origin.x + 100, y: (logoRect.origin.y)+20 , width: calculateHeight, height:80)
        //            "Created by Modifai iOS App".draw(in: bra ndRect, withAttributes: bra ndtextFontAttributes)
        //        }
        
        
        let finalImage2 = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        return finalImage2!
    }
    
    //----------------------------------------------------------------
    func readJsonFile()
    {
        //print("read Json File")
        var dic = ["brand": "", "style" : "", "price" : ""]
        if singleBrand // **********Change 2.2.1
        {
            dic = ["brand": "\(brand)", "style" : "", "price" : ""]
        }

        DataManager.getHomeCatalogsAfterFilter(filterDic: dic, completion:
            {  (error, data) in
                let jsonDecoder = JSONDecoder()
                let model: ResponseData?
                do {
                    //print("data:",data as Any)
                    model = try jsonDecoder.decode(ResponseData.self, from: data!)
                    //print("model:",model as Any)
                    self.homeObj = Home(responseData: model!)
                    self.doorsArray = []
                    self.doorsDoubleArray = []
                    
                    for doorsDouble in self.homeObj!.doors
                    {
                        let tempdouble:Door = doorsDouble
                        
                        if tempdouble.typeID == 3
                        {
                            self.doorsArray.insert(tempdouble, at: 0)
                        } else {
                            self.doorsDoubleArray.insert(tempdouble, at: 0)
                        }
                    }
                    //print("model as Any")
                   
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute:
                    {
                        self.homeTableView?.delegate = self
                        self.homeTableView?.dataSource = self
                    self.homeTableView.reloadData()
                    self.homeTableView.setContentOffset(.zero, animated: true)

                    })//*************** change 2.0.9
                    
                    self.getFilterDataFromResponse()
                    
                } catch {
                    print("0 encoding error:\(error)")
                    return
                }
        })
      
    }
    
    //----------------------------------------------------------------
    func changeDoorTypes()
    {
        //print("changeDo orTypes")
        if let ho = houseObject
        {
            for resp in ho.response
            {
                for respItem in resp
                {
                    var lbl = "nothing"
                    if (respItem.labels.count > 0)
                    {
                        lbl = respItem.labels[0]
                    }
                    
                    if lbl.contains("two-car")
                    {
                        isTwoCar = true
                    }
                    
                    if lbl.contains("one-car")
                    {
                        isOneCar = true
                    }
                    
                    if lbl.contains("door")
                    {
                        if respItem.rois.count>0 || respItem.roisinner.count>0 || respItem.roisouter.count>0
                        {
                            if lbl.contains("double")
                            {
                                isDouble = true
                            } else {
                                isDouble = false
                            }
                            isDoor = true
                            //print("changeDo orTypes.isDo or:",isDoor)
                        }
                    }
                }
            }
        }
    }
    
    //------------------------------------
    func getFilterDataFromResponse()
    {
        //print("get Filter Data FromResponse")
        self.arrPrice.removeAll()
        self.arrBrand.removeAll()
        
        if let home  = self.homeObj
        {
            for obj in home.doors
            {
                //print(obj.subTitle)
                if (obj.subTitle).isEmpty
                {
                    
                } else {
                    if obj.subTitle != "" && !self.arrBrand.contains(obj.subTitle)
                    {
                        self.arrBrand.append(obj.subTitle)
                    }
                }
                if (obj.price).isEmpty
                {
                    
                } else {
                    if obj.price != "" && !self.arrPrice.contains(obj.price)
                    {
                        self.arrPrice.append(obj.price)
                    }
                }
                
            }
            
            for obj in home.singleGarage
            {
                //print(obj.subTitle)
                if (obj.subTitle).isEmpty
                {
                } else {
                    if obj.subTitle != "" && !self.arrBrand.contains(obj.subTitle)
                    {
                        self.arrBrand.append(obj.subTitle)
                    }
                }
                
                if (obj.price).isEmpty
                {
                } else {
                    if obj.price != "" && !self.arrPrice.contains(obj.price)
                    {
                        self.arrPrice.append(obj.price)
                    }
                }
            }
            
            for obj in home.garages
            {
                //print(obj.subTitle)
                if (obj.subTitle).isEmpty
                {
                } else {
                    if obj.subTitle != "" && !self.arrBrand.contains(obj.subTitle)
                    {
                        self.arrBrand.append(obj.subTitle)
                    }
                }
                
                if (obj.price).isEmpty
                {
                } else {
                    if obj.price != "" && !self.arrPrice.contains(obj.price)
                    {
                        self.arrPrice.append(obj.price)
                    }
                }
            }
        }
        //print("self.arrPr ice:\(self.arrBr and)")
        //print("self.arrP rice:\(self.arrPr ice)")
    }
    
    //------------------------------------------------
    func shallRotate(_ size:CGSize)
	{
		scrollImg.setZoomScale(1.0, animated: false) // change zo om pixels

		phonePortrait = size.height > size.width;
		lastPhonePortrait = phonePortrait;
//print("1 shall Rotate  phonePortrait:",phonePortrait)

		self.hhvc?.collectionView?.reloadData();  //************** change 3.0.3 add  ***
		self.vvvc?.collectionView?.reloadData();  //************** change 3.0.3 add  ***

 //       self.scrollViewHeightConstarin.constant = 300.0   //************** change 3.0.3 remove  ***

		self.view.frame.size = CGSize(width: size.width, height: size.height)


		if (phonePortrait) //************** change 3.0.3 change if block ***
		{
			self.imageView.frame.origin = .zero  //************** change 3.0.3 add ***
			if wasIpad //************** change 3.0.3 add if block ***
			{
				self.scrollViewHeightConstarin.constant = self.view.frame.size.height - iPadForHome;
				self.tabsViewTopConstrain.constant = self.view.frame.size.height - iPadForHome;
				self.garageCellHeightConstraint.constant = iPadForHome;
				self.garDrFrDrView.frame.origin = CGPoint(x: 0.0, y: self.view.frame.size.height - iPadForHome)
				self.garDrFrDrView.frame.size = CGSize(width: self.view.frame.size.width, height: self.garDrFrDrView.frame.size.height)

				self.homeTableView.frame.origin = CGPoint(x: 0.0, y: self.garDrFrDrView.frame.origin.y+45.0)
				self.homeTableView.frame.size = CGSize(width: self.view.frame.size.width, height: iPadForHome)

				self.scrollImg.frame.size = CGSize(width: size.width, height: self.scrollImg.frame.size.height)
				self.imageView.frame = self.scrollImg.frame

			} else {
				self.scrollViewHeightConstarin.constant = iPhoneForHome //************** change 3.0.3 change position ***
				self.tabsViewTopConstrain.constant = iPhoneForHome
				self.garageCellHeightConstraint.constant = self.view.frame.size.height - iPhoneForHome;

				self.garDrFrDrView.frame.origin = CGPoint(x: 0.0, y: iPhoneForHome)
				self.garDrFrDrView.frame.size = CGSize(width: self.view.frame.size.width, height: self.garDrFrDrView.frame.size.height)

				self.homeTableView.frame.origin = CGPoint(x: 0.0, y: self.garDrFrDrView.frame.origin.y+self.garDrFrDrView.frame.size.height)
				self.homeTableView.frame.size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height-self.homeTableView.frame.origin.y)

				self.scrollImg.frame.size = CGSize(width: size.width, height: self.scrollImg.frame.size.height)
				self.imageView.frame = self.scrollImg.frame
			}
		} else {  //************** change 3.0.3 change if block ***
//                self.scrollImg.frame.size = CGSize(width: size.width, height: size.height)
			if wasIpad //************** change 3.0.3 add if block ***
			{
				self.imageView.frame.origin = .zero  //************** change 3.0.3 add ***
			} else {
				self.imageView.frame.origin = CGPoint(x: -100.0, y: 0.0)  //************** change 3.0.3 add ***
			}
			self.garDrFrDrView.frame.origin = CGPoint(x: 0.0, y: self.view.frame.size.height)
		}

		self.scrollImg.minimumZoomScale = 1.0
		self.scrollImg.maximumZoomScale = 10.0


//print("  shall Rotate        self.view.frame:",self.view.frame)
//print("  shall Rotate        self.scrol lImg.frame:",self.scrollImg.frame)
//print("  shall Rotate        self.image View.frame:",self.imageView.frame)
//print("  shall Rotate        self.scrollImg.conte ntSize:",self.scrollImg.contentSize)


//print("1 shall Rotate  phonePortrait:",phonePortrait)

		let scaleFac:CGFloat = self.scrollImg.zoomScale
		theImageProportionX = 1.0
		theImageProportionY = 1.0

		theImageSizeX = self.imageView.frame.size.width
		theImageSizeY = self.imageView.frame.size.height
//print("          theImag eSizeX, theImageSizeY:",theImag eSizeX, theImag eSizeY)

//print("shall Rotate self.view.frame:",self.view.frame)
//print("shall Rotate self.imageView.frame:",self.imageView.frame)

		if (theImageSizeX < smallImageSizeX)
		{
			smallImageSizeX = theImageSizeX;
		}

//print("shall Rotate smallIma geSizeX:",smallImageSizeX)


		if let image = self.imageView.image
		{
			if (image.size.width >= image.size.height)
			{
				theImageProportionY = image.size.height / image.size.width
				theImageSizeY = self.imageView.frame.size.width * theImageProportionY
//print("shall Rotate size.width,theImageProportionY,theImageSizeY:",self.imageView.frame.size.width,theImageProportionY,theImageSizeY)


				if (phonePortrait)
				{
//					if (self.scrollImg.contentSize.width > theImageSizeX)   //************** change 3.0.3 remove if block ***
//					{
//						self.scrollImg.minimumZo omScale = (theImageSizeX / (self.scrollImg.contentSize.width+1.0))
//					}
				} else {
					if (self.scrollImg.contentSize.height > smallImageSizeX)
					{
						self.scrollImg.minimumZoomScale = (smallImageSizeX / (self.scrollImg.contentSize.height+1.0))
//print("shall Rotate self.scrollImg.minimumZo omScale:",self.scrollImg.minimumZo omScale)
					}

					if wasIpad //************** change 3.0.3 add if block ***
					{
						self.scrollImg.minimumZoomScale = self.scrollImg.minimumZoomScale * 0.88
					}
				}

			} else {
				theImageProportionX = image.size.width / image.size.height
				theImageSizeX = self.imageView.frame.size.height * theImageProportionX
			}
		}


//print("shall Rotate self.scrollImg.conte ntSize.height:",self.scrollImg.contentSize.height)
//print("shall Rotate self.view.frame.size.width:",self.view.frame.size.width)
//print("shall Rotate self.scrollImg.minimumZo omScale:",self.scrollImg.minimumZo omScale)


//print("         ")
//print("          theImageProp ortionX theImagePropo rtionY:", theImagePro portionX, theImagePro portionY)
//print("   rotate   origin:",self.imageView.frame.origin)
//print("    view  width height:",self.view.frame.size.width, self.view.frame.size.height)
//print("          width height:",self.imageView.frame.size.width, self.imageView.frame.size.height)
//print("          sizeX sizeY:",theIma geSizeX, theImageSizeY)

		self.logoView.frame.size = CGSize(width: 30.0*scaleFac, height: 30.0*scaleFac)

		let x = self.imageView.frame.origin.x + (5.0*scaleFac)
			+ ((self.imageView.frame.size.width - theImageSizeX) * 0.5)

		let y = theImageSizeY - (self.logoView.frame.size.height + (5.0*scaleFac))
			+ ((self.imageView.frame.size.height - theImageSizeY) * 0.5);

//print("          x y:",x, y)

		self.logoView.frame.origin = CGPoint(x: x, y: y)


		if (phonePortrait)
		{
//print("did Rotate portrait")
			btnListClicked = false
			self.isLandScape = false
			self.setupPortrait()
			self.tableViewTopConstrain.constant = tablePortraitTop

			DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute:
			{
//                self.scrollImg.zo omScale = self.scrollImg.minimumZo omScale  //*************** change 3.0.3 remove ***
				self.homeTableView.reloadData()
				self.scrollImg.setNeedsLayout()
				self.scrollImg.layoutIfNeeded()
			})
		} else {
//print("did Rotate isLand Scape")
			btnListClicked = true
			self.isLandScape = true
			self.setupLandscape()
			self.tableViewTopConstrain.constant  = tableLandscapeTop //*************** change 3.0.3 change ***

			DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute:
			{
				self.scrollImg.zoomScale = 1.0 //*************** change 3.0.3 change ***

				self.homeTableView.reloadData()
				self.scrollImg.setNeedsLayout()
				self.scrollImg.layoutIfNeeded()
			})
		}
	}
    
    //------------------------------------------------------------------
    func viewForZooming(in scrollView: UIScrollView) -> UIView?
    {
        return self.imageView
    }
    
    //------------------------------------------------------------------
    func setupLandscape()
    {
//print("  setupLands cape  ")
        setupShareView()
        noProcessSwitch.isHidden = true
        self.garDrFrDrView.isHidden = true;   //*************** change 3.0.3 add ***
		self.vertContainer.isHidden   = false; //*************** change 3.0.3 add ***

        if hasOnlyGarageDoors  //*************** change 3.0.3 add if block ***
        {
			self.vertChangeDoors.isHidden = true;
			self.vertFrntDoors.isHidden   = true;
			self.vertHousePaint.isHidden   = true;   //************** change 3.0.6 add ***
        } else {
			self.vertChangeDoors.isHidden = false;
			self.vertFrntDoors.isHidden   = false;
			self.vertHousePaint.isHidden  = false;   //************** change 3.0.6 add ***
		}

        self.tableViewBottomConstrain.constant = 45
        
        self.blurrImgView.isHidden = true
        camView.isHidden = true
        self.filterBtn.isHidden = true
        self.photoView.isHidden = true
        self.shareView.isHidden = false
        self.setupButtonTitlesForImage()
        
        self.btnMenu.isHidden = true
        self.tabBarController?.tabBar.isHidden = true

//        edgesForExtendedLayout = UIRectEdge.bottom //********** Change 2.2.0   //*************** change 3.0.3 remove ***
//        extendedLayoutIncludesOpaqueBars = true //********** Change 2.2.0   //*************** change 3.0.3 remove ***

        self.imageView.contentMode = .scaleAspectFit
        self.imageView.clipsToBounds = true
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        lineLabel.isHidden = true

        var myImageWidth1:CGFloat = 1.0
        var myImageHeight1:CGFloat = 1.0
        
        if let image = originalHomeImage
        {
            myImageWidth1 = image.size.width
            myImageHeight1 = image.size.height
        }
        
        if myImageWidth1>myImageHeight1
        {
//            if (self.scrollImg.minimumZo omScale < 1.0)  //*************** change 3.0.3 remove if block ***
//            {
//                self.scrollImg.setZo omScale(self.scrollImg.minimumZo omScale, animated: true) //*************** change 0.0.32 change remove ***
//            } else {
//                self.scrollImg.setZo omScale(1.316, animated: false) //*************** change 0.0.32 change remove ***
//            }
        } else {
            self.scrollImg.setZoomScale(1.0, animated: false)
        }
        
        self.tabsViewTopConstrain.constant = 0.0
		self.garageCellHeightConstraint.constant = 0.0; //*************** change 3.0.3 change add ***

        if self.view.frame.height > self.view.frame.width
        {
            if (kIphone_X || kIphone_XMAX)
            {
                
               // self.tabsViewLeadingConstrain.constant = self.view.frame.height - 160   //*************** change 3.0.3 change not removed in garage ??? ***
                self.tabsGarageWidth.constant = 35
            } else {
                self.tabsGarageWidth.constant = 30
            }
  //          self.tabsViewLeadingConstrain.constant = self.view.frame.height - 160 //**********Change  2.2.0  //*************** change 3.0.3 change remove ***

            self.scrollViewHeightConstarin.constant = self.view.frame.width //*************** change 0.0.32 change add ***
			self.garDrFrDrView.frame.origin = CGPoint(x: 0.0, y: self.view.frame.width)
//           	self.scrollViewWidthConstarin.constant = self.tabsViewLeadingConstrain.constant    //************** change 3.0.3 check if removed remove if not ***
	//		self.garageCellHei ghtConstraint.constant = self.view.frame.width; //*************** change 3.0.3 change remove ***

        } else {
            if (kIphone_X || kIphone_XMAX)
            {
                self.tabsGarageWidth.constant = 35
                //self.tabsViewLeadingConstrain.constant = self.view.frame.width - 160  //**********Change  2.2.0 remove  //......... need to check ...
            } else {
                self.tabsGarageWidth.constant = 30
            }
     //       self.tabsViewLeadingConstrain.constant = self.view.frame.width - 160 //**********Change  2.2.0  //*************** change 3.0.3 change remove ***

            self.scrollViewHeightConstarin.constant = self.view.frame.height //*************** change 0.0.32 change add ***
			self.garDrFrDrView.frame.origin = CGPoint(x: 0.0, y: self.view.frame.height)
//            self.scrollViewWidthConstarin.constant = self.tabsViewLeadingConstrain.constant    //************** change 3.0.3 check if removed remove if not ***
	//		self.garageCellHei ghtConstraint.constant = self.view.frame.height; //*************** change 3.0.3 change remove ***
        }
        self.shareviewTopConstraint.constant = 70

        
//print("  setupLa ndscape        self.view.frame:",self.view.frame)
//print("  setupLa ndscape        self.scrollViewHeightConstarin.constant:",self.scrollViewHeightConstarin.constant)
//print("  setupLa ndscape        self.scrollViewWidthConstarin.constant :",self.scrollViewWidthConstarin.constant)
//print("  setupLa ndscape        self.tabsViewLeadingConstrain.constant :",self.tabsViewLeadingConstrain.constant)
//*************** change 0.0.32 change add ***
//        self.scrollImg.frame.size = CGSize(width: self.scrollViewWidthConstarin.constant, height: self.scrollViewHeightConstarin.constant)
//        self.imageView.frame = self.scrollImg.frame
//print("  setupLa ndscape        self.scrol lImg.frame:",self.scrollImg.frame)
//print("  setupLa ndscape        self.image View.frame:",self.image View.frame)

//print("  setupLa ndscape        self.view.frame:",self.view.frame)
//print("  setupLa ndscape        self.scrol lImg.frame:",self.scrollImg.frame)
//print("  setupLa ndscape        self.image View.frame:",self.imageView.frame)
        
        self.btnList.isHidden = true
        
        self.filterWidthView.constant = 0
        self.camView.isHidden = true
       if (hasOnlyGarageDoors) //********** change 1.0.0 add if block ***
		{
            self.btnGarage.isHidden = true
            self.btnDoors.isHidden = true
            self.garageImage.isHidden = true
            self.doorImage.isHidden = true
            self.paintImage.isHidden = true
            self.btnPaint.isHidden = true

        } else {
            self.garageImage.isHidden = false //*************** change 1.0.0 move to here ***
            self.doorImage.isHidden = false
            self.paintImage.isHidden = false   // changed to false
            self.btnPaint.isHidden = false    // changed to false to show paint in revive and modifai
        }
      
        self.shareView.isHidden = false
       
        self.btnPaint.setTitle("", for: .normal)
        self.btnGarage.setTitle("", for:.normal)
        self.btnDoors.setTitle("", for:.normal)
        self.homeTableView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
      //  self.shareView.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        //*************** change 0.0.32 remove ***
        
        //print("  setupLa ndscape        btnGallery.isHidden being hidden")
        btnGallery.isHidden = true

     
    }
    
    //------------------------------------------------------------------
    func setupPortrait()
    {
//print("  setupPort rait  ")
        setupShareView()
        self.garDrFrDrView.isHidden = false  //*************** change 3.0.3 add ***
		self.vertChangeDoors.isHidden = true; //*************** change 3.0.3 add ***
		self.vertFrntDoors.isHidden   = true; //*************** change 3.0.3 add ***
		self.vertContainer.isHidden   = true; //*************** change 3.0.3 add ***
		self.vertHousePaint.isHidden  = true;   //************** change 3.0.6 add ***

        if hasInternalApp && !hasOnlyGarageDoors
        {
            noProcessSwitch.isHidden = false
        }


        homeTableView.layer.borderColor = UIColor.clear.cgColor
        self.tableViewBottomConstrain.constant = 0

//        self.scrollImg.setZo omScale(self.scrollImg.minimumZo omScale, animated: true) //*************** change 3.0.3 remove ***
        self.shareView.isHidden = false
        self.btnMenu.isHidden = false

        self.tabBarController?.tabBar.isHidden = false

//        edgesForExtendedLayout = UIRectEdge.bottom //********** Change 2.2.0   //*************** change 3.0.3 remove ***
//        extendedLayoutIncludesOpaqueBars = false //********** Change 2.2.0  //*************** change 3.0.3 remove ***

        self.imageView.contentMode = .scaleAspectFit
        self.imageView.transform = CGAffineTransform.identity  //*************** change 0.0.34 change place to here ***
        
        self.imageView.clipsToBounds = true
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        
        self.tabsViewLeadingConstrain.constant = 0.0
//        self.tabsViewTopConstrain.constant = 300.0  //*************** change 3.0.3 change not here? ***
//        self.scrollViewWidthConstarin.constant = 0.0 //*************** change 3.0.3 change not here? ***
        
        
        self.btnList.isHidden = false
        self.filterBtn.isHidden = false
        lineLabel.isHidden = false
        self.filterWidthView.constant = 110
        self.imageView.transform = CGAffineTransform.identity
        self.btnMenu.isHidden = false
        self.homeTableView.backgroundColor = .clear
        self.garageImage.isHidden = true
        self.doorImage.isHidden = true
        self.paintImage.isHidden = true
        self.btnPaint.isHidden = false

		if (hasOnlyGarageDoors)
        {
            self.btnGarage.isHidden = true
            self.btnDoors.isHidden = true
			self.garageImage.isHidden = true
            self.doorImage.isHidden = true
            self.paintImage.isHidden = true
            self.btnPaint.isHidden = true
        }

		self.shareviewTopConstraint.constant = 60

        self.btnGarage.setTitle("Garage", for:.normal)
        self.btnDoors.setTitle("Doors", for:.normal)
        self.btnPaint.setTitle("Paint", for:.normal)
        
        let status = UserDefaults.standard.bool(forKey: "removeCamView")
        if !status
        {
            camView.isHidden = false
        }
        
        btnGallery.isHidden = false
        imageView.isHidden = false
    }
    
    
    
    
    
    //MARK:- Private methods
    //------------------------------------------------------------
    func openPhotoLibrary(type: String)
    {
        //print("open Photo Library")
        if type == "photolibrary"
        {
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary)
            {
                //print("open Photo Library type == photolibrary")
                imagePicker.delegate = self
                imagePicker.sourceType = .photoLibrary
                imagePicker.allowsEditing = false
                imagePicker.modalPresentationCapturesStatusBarAppearance = false  //********** change 0.0.40 remmove ***
                imagePicker.modalPresentationStyle = .overCurrentContext //********** change 0.0.40 add ***  .currentContext .overCurrentContext
                
                present(imagePicker, animated: true, completion: nil)
            }
        } else {
            //print("open Photo Library type not")
            self.photoView.isHidden = false
            self.photoCollectionView.reloadData()
            self.setupButtonTitlesForGallery()
        }
    }
    
    //---------------------------------------------------------------
    func openExtranalPhotoLibrary()
    {
        //  self.showDocumentOptions()
        //print("open Extranal Photo Library")
        
        guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else
        {
            print("can't open photo library")
            return
        }
        
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        imagePicker.modalPresentationCapturesStatusBarAppearance = true
        
        present(imagePicker, animated: true)
    }
    
    //---------------------------------------------------------------
    func setupButtonTitlesForGallery()
    {
        //print("  setupButtonTitl esForGallery  ")
        self.btnFav.isHidden = true
        self.btnGallery.isHidden = true
        self.btnShare.isHidden = true
    }
    
    //---------------------------------------------------------------
    func setupButtonTitlesForImage()
    {
        //print("  setupButtonTit lesForImage  ")
        self.btnFav.isHidden = false
        self.btnGallery.isHidden = false
        self.btnShare.isHidden = false
    }
    
    //---------------------------------------------------------------
    func openCamera()
    {
        //print("open Camera")
        self.photoView.isHidden = true
        self.setupButtonTitlesForImage()
        let storyBoard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        let camaraVC = storyBoard.instantiateViewController(withIdentifier: "ModifiCameraVC") as! CameraViewController
        camaraVC.delegate = self
		camaraVC.modalPresentationStyle = .overFullScreen  //********** change 3.0.3 add  ***
        camaraVC.modalPresentationCapturesStatusBarAppearance = true
        camaraVC.hidesBottomBarWhenPushed = true //********** change 3.0.3 add  ***

        self.present(camaraVC, animated: true, completion: nil)
    }

    //---------------------------------------------------------------
    func openDeviceCamera()
    {
        self.photoView.isHidden = true
        self.setupButtonTitlesForImage()
        AppUtility.lockOrientation(.portrait)
        
        
        let storyboard = UIStoryboard(name: "CustomTabBar", bundle: nil)
        let onboradingVC = storyboard.instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
        onboradingVC.isTakePhoto = true
        onboradingVC.hidesBottomBarWhenPushed = true
        
        self.navigationController?.pushViewController(onboradingVC, animated: true)
    }
    
    //MARK:- setupButtonColors
    //---------------------------------------------------------------
    func  setupButtonSelection()
    {
//print(" setupBut tonSelection btnGarageClicked,btnDoorClicked,",btnGarageClicked,btnDoorClicked)
        //Garage Button

		self.vertChangeDoors.isSelected = false;   //*************** change 3.0.6 add ***
		self.vertFrntDoors.isSelected = false;   //*************** change 3.0.6 add ***
		self.vertHousePaint.isSelected  = false;   //************** change 3.0.6 add ***
		self.vertChangeDoors.isEnabled = true;   //*************** change 3.0.6 add ***
		self.vertFrntDoors.isEnabled = true;   //*************** change 3.0.6 add ***
		self.vertHousePaint.isEnabled  = true;   //************** change 3.0.6 add ***


        if btnGarageClicked
        {
            btnGarage.setTitleColor(UIColor.white, for: .normal)
            btnGarage.backgroundColor = UIColor.init(red: 48.0/255.0, green: 191.0/255.0, blue: 152.0/255.0, alpha: 1.0)
			self.vertChangeDoors.isSelected = true;   //*************** change 3.0.6 add ***
            btnDoors.setTitleColor(UIColor.init(red: 134.0/255.0, green: 141.0/255.0, blue: 179.0/255.0, alpha: 1.0), for: .normal)
            btnDoors.backgroundColor = UIColor.white
            btnPaint.setTitleColor(UIColor.init(red: 134.0/255.0, green: 141.0/255.0, blue: 179.0/255.0, alpha: 1.0), for: .normal)
            btnPaint.backgroundColor = UIColor.white
            
            if !isDoor
            {
                btnDoors.backgroundColor = UIColor.lightGray
				self.vertFrntDoors.isEnabled = false;   //*************** change 3.0.6 add ***
            }
            lineLabel.isHidden = false
            self.btnList.setImage(UIImage(named: "list-v1"), for: .normal)
        } else if btnDoorClicked {
            btnDoors.setTitleColor(UIColor.white, for: .normal)
            btnDoors.backgroundColor = UIColor.init(red: 48.0/255.0, green: 191.0/255.0, blue: 152.0/255.0, alpha: 1.0)
			self.vertFrntDoors.isSelected = true;   //*************** change 3.0.6 add ***
            btnGarage.setTitleColor(UIColor.init(red: 134.0/255.0, green: 141.0/255.0, blue: 179.0/255.0, alpha: 1.0), for: .normal)
            btnGarage.backgroundColor = UIColor.white
            btnPaint.setTitleColor(UIColor.init(red: 134.0/255.0, green: 141.0/255.0, blue: 179.0/255.0, alpha: 1.0), for: .normal)
            btnPaint.backgroundColor = UIColor.white

            if !isTwoCar && !isOneCar
            {
                btnGarage.backgroundColor = UIColor.lightGray
				self.vertChangeDoors.isEnabled = false;   //*************** change 3.0.6 add ***
            }
            lineLabel.isHidden = false
            self.btnList.setImage(UIImage(named: "cards-v1"), for: .normal)

        }  else if btnPaintListClicked {
            
            btnPaint.setTitleColor(UIColor.white, for: .normal)
            btnPaint.backgroundColor = UIColor.init(red: 48.0/255.0, green: 191.0/255.0, blue: 152.0/255.0, alpha: 1.0)
			self.vertHousePaint.isSelected = true;   //*************** change 3.0.6 add ***
            btnGarage.setTitleColor(UIColor.init(red: 134.0/255.0, green: 141.0/255.0, blue: 179.0/255.0, alpha: 1.0), for: .normal)
            btnGarage.backgroundColor = UIColor.white
            btnDoors.setTitleColor(UIColor.init(red: 134.0/255.0, green: 141.0/255.0, blue: 179.0/255.0, alpha: 1.0), for: .normal)
            btnDoors.backgroundColor = UIColor.white

            if !isTwoCar && !isOneCar
            {
                btnGarage.backgroundColor = UIColor.lightGray
				self.vertChangeDoors.isEnabled = false;   //*************** change 3.0.6 add ***
            }

            if !isDoor
            {
                btnDoors.backgroundColor = UIColor.lightGray
				self.vertFrntDoors.isEnabled = false;   //*************** change 3.0.6 add ***
            }
            lineLabel.isHidden = false
        } else {
//print(" setupBut tonSelection isTwoCar,isOneCar,isDoor,",isTwoCar,isOneCar,isDoor)
            btnGarage.setTitleColor(UIColor.init(red: 134.0/255.0, green: 141.0/255.0, blue: 179.0/255.0, alpha: 1.0), for: .normal)
            btnGarage.backgroundColor = UIColor.white

            if !isTwoCar && !isOneCar
            {
                btnGarage.backgroundColor = UIColor.lightGray
				self.vertChangeDoors.isEnabled = false;   //*************** change 3.0.6 add ***
            }
            btnDoors.setTitleColor(UIColor.init(red: 134.0/255.0, green: 141.0/255.0, blue: 179.0/255.0, alpha: 1.0), for: .normal)
            btnDoors.backgroundColor = UIColor.white

            if !isDoor
            {
                btnDoors.backgroundColor = UIColor.lightGray
				self.vertFrntDoors.isEnabled   = false;   //*************** change 3.0.6 add ***
            }
            btnPaint.setTitleColor(UIColor.init(red: 134.0/255.0, green: 141.0/255.0, blue: 179.0/255.0, alpha: 1.0), for: .normal)
            btnPaint.backgroundColor = UIColor.white
            lineLabel.isHidden = false
            self.btnList.setImage(UIImage(named: "cards-v1"), for: .normal)
        }
    }
    
    //Zipcode Delegate
    //---------------------------------------------------------------
    func onSelectionCompletionClick(sender:String)
    {
        self.readJsonFile()
    }
    
    //---------------------------------------------------------------
    func onSignInCompletionClick(sender: String)
    {
        //   self.uploadnewImage()
        if hasInternalApp
        {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.getPreviousHouseListAPI()
            DataManager.GetPaintColors()
            self.uploadEditedFilesToCloud()
        }
        
        DataManager.GetFavoriteScenes()
        self.createFavoriteAPI()
    }
    
    
    //MARK:- button handler methods
    //---------------------------------------------------------------
    @IBAction func editBtnClicked(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "CustomTabBar", bundle: nil)
        let editViewController = storyboard.instantiateViewController(withIdentifier: "EditingViewController") as! EditingViewController
        
        editViewController.oneCarImgae = self.oneCarImgae ?? UIImage(named: "1_s0.jpg")
        editViewController.twoCarImgae = self.twoCarImgae ?? UIImage(named: "1_d0.jpg")
        editViewController.oneDoorImgae = self.oneDoorImgae ?? UIImage(named: "f_s0.jpg")
        editViewController.twoDoorImgae = self.twoDoorImgae ?? UIImage(named: "f_d10.jpg")
        
        editViewController.hidesBottomBarWhenPushed = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.pushViewController(editViewController, animated: true)
    }
    
    //---------------------------------------------------------------
    @IBAction func btnMenuclicked(_ sender: Any)
    {
        //print("btn Menu clicked")
        camView.isHidden = true
        UserDefaults.standard.set(true, forKey: "removeCamView")
        self.photoView.isHidden = true
        self.setupButtonTitlesForImage()
       
        if (isHomeAgent)  //********** change 3.0.7 change ***
        {
            self.loadAgentHomes()
        }
        
        if hasInternalApp
        {
            PHPhotoLibrary.requestAuthorization(
                {
                    (newStatus) in
                    if newStatus ==  PHAuthorizationStatus.authorized
                    {
                    }
            })
        }
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let takeANewPhotoVC = storyboard.instantiateViewController(withIdentifier: "TakeANewPhotoVC") as! TakeANewPhotoViewController
        takeANewPhotoVC.modalPresentationStyle = .overCurrentContext
        takeANewPhotoVC.delegate = self
        takeANewPhotoVC.modalPresentationCapturesStatusBarAppearance = true
        
        self.present(takeANewPhotoVC, animated: true)
        {
        }
    }
    
    //---------------------------------------------------------------
    func btnDoneClickedAction()
    {
        self.blurrImgView.isHidden = true
        camView.isHidden = true
        UserDefaults.standard.set(true, forKey: "removeCamView")
        self.photoView.isHidden = true
        self.shareView.isHidden = false
        
        self.clearThePaint()
        
        //print("1 btn Done Clicked  clear The Paint selectedGal leryImage:",selectedGal leryImage)
        if ((selectedGalleryImage >= 0) && (selectedGalleryImage < projectList00.count)) //*************** change 0.0.40  change ***
        {
            //     self.clearThePaint()
            
            //print("1 btn Done Clicked current self.imagesArray:",self.imagesArray)
                     //   let url:URL = projectList00[selectedGalleryImage]    //*************** change 0.0.46  remove ***
//            currentImageName = projectList00[selectedGalleryImage].thumbnailName ?? ""
//                        currentImageName = currentImageName.changeExtensions(name:currentImageName)
            //print("1 btn Done Clicked current ImageName:",currentImageName)
            
            let first4 = String(currentImageName.prefix(4))
            if first4 == "modi"
            {
                addedImageFromCameraStr = "defaultImage"
            }else{
                addedImageFromCameraStr = "library"
            }
            
            //        self.loadUpImageFromDocumentDirectory(nameOfImage:currentImageName)  //********** change 1.0.0 remove ***

            
            let size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height);
            //print("btnDo neClicked size:",size)
            self.shallRotate(size)
            
            self.setupButtonTitlesForImage()
            
            //print("1 btn Done Clicked selectedGal leryImage:",selectedGal leryImage)
            self.selectedReferenceItem = nil
            self.selectedReferenceItem = ReferenceItem.init(name: "Camera")
            self.selectedReferenceItem?.image = self.imageView.image
            
            isTwoCar = false
            isOneCar = false
            //print("btnDoneClicked original Hom eImage:",originalHo meImage as Any)
            self.asset = nil
            self.metadata = ["":""] as [String : Any]
            debugPrint("currentImageName from done:",currentImageName)

            self.fetchHouseObject
                { (house0Object) in
            }
        } else {
        }
    }
    
    //---------------------------------------------------------------
    @IBAction func btnCamIconClicked(_ sender: Any)
    {
        self.photoView.isHidden = true
        self.shareView.isHidden = false
        let optionMenu = UIAlertController(title: nil, message: Constants.Text.Home.CHOSE_OPTION, preferredStyle: .actionSheet)
        
        
        let cameraAction = UIAlertAction(title: Constants.Text.Home.TAKE_PHOTO, style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            self.openDeviceCamera()
            
        })
        
        let libraryAction = UIAlertAction(title: Constants.Text.Home.PHOTO_LIBRARY, style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            self.openExtranalPhotoLibrary()
        })
        
        let modifyAction = UIAlertAction(title: Constants.Text.Home.MODIFY_LIBRARY, style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            let storyBoard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
            let modifaiHousesVC = storyBoard.instantiateViewController(withIdentifier: "ModifaiHousesVC") as! ModifaiHousesViewController
            modifaiHousesVC.delegate = self
            modifaiHousesVC.modalPresentationCapturesStatusBarAppearance = true
            
            self.present(modifaiHousesVC, animated: true, completion: nil)
        })
        
        let cancelAction = UIAlertAction(title: Constants.Text.Home.CANCAL, style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            
        })
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(libraryAction)
        optionMenu.addAction(modifyAction)
        optionMenu.addAction(cancelAction)
        optionMenu.modalPresentationCapturesStatusBarAppearance = true

		if let popoverController = optionMenu.popoverPresentationController  //********** change 3.0.3 add if block ***
		{
		  popoverController.sourceView = self.view
		  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
		  popoverController.permittedArrowDirections = []
		}

        self.present(optionMenu, animated: true, completion: nil)
    }
    
    //MARK:- Garage handler methods
    //------------------------------------
    @IBAction func btnGarageClicked(_ sender: Any)
    {
        self.nodoorLabel.isHidden = true
        
        if !isTwoCar && !isOneCar && hasInternalApp
        {
            if !btnGarageClicked
            {
                var msg = "We didn't detect any garage. Please try again or add a garage using the edit function."
                if !isDoor
                {
                    msg = "We didn't detect any garage or front doors.  Please try again or add a door using the edit function."
                }
                let alert = UIAlertController(title: "oops", message: msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))

				if let popoverController = alert.popoverPresentationController  //********** change 3.0.3 add if block ***
				{
				  popoverController.sourceView = self.view
				  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
				  popoverController.permittedArrowDirections = []
				}

                self.present(alert, animated: true, completion: nil)
            }
            self.btnDoorsClicked(btnDoors as Any)
            return
        }
        //Analytics.logEvent("Carousel_select_product_type_garage", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
        
        if garageScrollIndex == 0
        {
            self.btnFavWasClicked = false
            self.btnFav.setImage(UIImage(named: "heart-v1"), for: .normal)
        }
        
        btnGarageClicked = true
        btnDoorClicked = false
        btnListClicked = false
        btnPaintListClicked = false

        self.setupButtonSelection()

        if self.isLandScape
        {
            btnListClicked = true
        } else {
            btnListClicked = false
        }

        self.homeTableView.reloadData()
        self.homeTableView.scroll(to: .top, animated: true)

		self.hhvc?.collectionView?.reloadData();  //************** change 3.0.6 add  ***
		self.vvvc?.collectionView?.reloadData();  //************** change 3.0.6 add  ***
//		self.hhvc?.collectionView?.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: true)  //************** change 3.0.6 add  ***
//		self.vvvc?.collectionView?.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: true)  //************** change 3.0.6 add  ***
    }
    
    //---------------------------------------------------------------
    @IBAction func btnDoorsClicked(_ sender: Any)
    {
        self.nodoorLabel.isHidden = true
        //print("btnDoors Clicked isDo or:",isDoor);
        if ( !isDoor || hasOnlyGarageDoors)
        {
            var msg = "No doors were detected. Please re-take the picture with doors visible"
            //Analytics.logEvent("Alert_No_doors_were_detected.Please_re-take_the picture_with_doors_visible", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
            if !isTwoCar && !isOneCar
            {
                self.nodoorLabel.isHidden = false
                if (hasOnlyGarageDoors) //********** change 1.0.0 add if block ***
                {
                    msg = "Sorry,we couldn't find a garage in this image. Please re-take the picture with garage visible."
                    self.nodoorLabel.text = "We didn't detect any garage. Please try again or add a garage using the edit function."
                } else {
                    msg = "We did not detect any garage or front doors, please try again"
                }                //Analytics.logEvent("Alert_We_did not_detect_any_garage_or_frontdoors,please_try_again", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
            }
            
            let alert = UIAlertController(title: "oops", message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))

			if let popoverController = alert.popoverPresentationController  //********** change 3.0.3 add if block ***
			{
			  popoverController.sourceView = self.view
			  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
			  popoverController.permittedArrowDirections = []
			}

            self.present(alert, animated: true, completion: nil)
            return
        }
        //Analytics.logEvent("Carousel_select_product_type_door", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
        
        if doorScrollIndex == 0
        {
            self.btnFavWasClicked = false
            self.btnFav.setImage(UIImage(named: "heart-v1"), for: .normal)
        }

        btnGarageClicked = false
        btnDoorClicked = true
        btnListClicked = false
        btnPaintListClicked = false

        self.setupButtonSelection()

        if self.isLandScape
        {
            btnListClicked = true
        } else {
            btnListClicked = false
        }

        self.homeTableView.reloadData()
        self.homeTableView.scroll(to: .top, animated: true)

		self.hhvc?.collectionView?.reloadData();  //************** change 3.0.6 add  ***
		self.vvvc?.collectionView?.reloadData();  //************** change 3.0.6 add  ***
//		self.hhvc?.collectionView?.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: true)  //************** change 3.0.6 add  ***
//		self.vvvc?.collectionView?.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: true)  //************** change 3.0.6 add  ***
    }
    
    //---------------------------------------------------------------
    func afterVideoFinish(sender: String)
    {
        //print("afterVid eoFinish  sender:", sender)
        self.btnPaintClicked(btnPaint as Any)
    }
    
    //---------------------------------------------------------------
    @IBAction func btnPaintClicked(_ sender: Any)
    {
        self.nodoorLabel.isHidden = true
        
        btnGarageClicked = false
        btnPaintListClicked = true
        
        btnDoorClicked = false
        self.setupButtonSelection()
        if self.isLandScape
        {
            btnListClicked = true
        } else {
            btnListClicked = false
        }
        
        self.tableViewTopConstrain.constant  = 0
        self.homeTableView.reloadData()
        self.homeTableView.scroll(to: .top, animated: true)

		self.hhvc?.collectionView?.reloadData();  //************** change 3.0.6 add  ***
		self.vvvc?.collectionView?.reloadData();  //************** change 3.0.6 add  ***
//		self.hhvc?.collectionView?.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: true)  //************** change 3.0.6 add  ***
//		self.vvvc?.collectionView?.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: true)  //************** change 3.0.6 add  ***
    }

    //---------------------------------------------------------------
    func btnPaintLastItemClicked(_ sender: Any)
    {
        //print("btnPain tClicked  sender:", sender)
//        let editTutorial =  UserDefaults.standard.bool(forKey: "paintTutorial")
//        if editTutorial
//        {
            let storyboard = UIStoryboard(name: "PaintImage", bundle: nil)
            
            let paintViewController = storyboard.instantiateViewController(withIdentifier: "PaintViewController") as! PaintViewController
            paintViewController.maskOfDoor = false;
            paintViewController.maskOfFrontDoor = false;
            paintViewController.cameraImageSelected = addedImageFromCameraStr
            paintViewController.hidesBottomBarWhenPushed = true
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
            self.navigationController?.pushViewController(paintViewController, animated: true)
            AppUtility.lockOrientation(.all)
                UserDefaults.standard.set(true, forKey: "paintTutorial")

//        } else {
//            AppUtility.lockOrientation(.portrait)
//            comingFromPaintTutorial = true
//
//            UserDefaults.standard.set(true, forKey: "paintTutorial")
//            UserDefaults.standard.synchronize() //************** change 1.1.0 add ***
//
//            let storyboard = UIStoryboard(name: "CustomTabBar", bundle: nil)
//            let onboradingVC = storyboard.instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
//            onboradingVC.isPaintIntro = true
//            onboradingVC.isPaintTour = false
//
//            onboradingVC.maskOfDoor = false    //************** change 1.1.0 add ***
//            onboradingVC.maskOfFrontDoor = false  //************** change 1.1.0 add ***
//
//            onboradingVC.hidesBottomBarWhenPushed = true  //************ change 2.1.0
//            onboradingVC.modalPresentationStyle = .overFullScreen  //************ change 2.1.0
//            onboradingVC.modalPresentationCapturesStatusBarAppearance = true  //************ change 2.1.0
//            self.present(onboradingVC, animated: true, completion: nil)  //************ change 2.1.0
//          //  self.navigationController?.pushViewController(onboradingVC, animated: true)  //************ change 2.1.0
//        }
    }

    //---------------------------------------------------------------
    @IBAction func btnListClicked(_ sender: Any)
    {
        if btnListClicked
        {
            btnListClicked = false
          if hasInternalApp && !hasOnlyGarageDoors  //***************** change 1.0.0 change ***
            {
                noProcessSwitch.isHidden = false
            }
            // chevoronBtn.isHidden = false
            self.btnList.setImage(UIImage(named: "list-v1"), for: .normal)
        } else {
            if hasInternalApp
            {  //***************** change 0.0.42 change  ***
                //         noProcessSwitch.isHidden = true
            }
            btnListClicked = true
            // chevoronBtn.isHidden = true
            self.btnList.setImage(UIImage(named: "cards-v1"), for: .normal)
        }
        
        //     self.tableViewTopConstrain.constant  = 0   //*************** change 1.0.2 remove ***
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute:
        {
            self.homeTableView.reloadData()
            self.homeTableView.scroll(to: .top, animated: true)
        })//******* change 2.0.9
        
        //print("btnListClicked", btnListClicked)
        if btnListClicked
        {
            if garageScrollIndex > 0
            {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    let indexPathScroll = IndexPath(row: self.garageScrollIndex-1, section:0 )
                    if self.homeTableView.numberOfRows(inSection: 0) > indexPathScroll.row
                    {
                        self.homeTableView.scrollToRow(at:indexPathScroll , at: .top, animated: true)
                    }
                })
            }
            if ( !hasOnlyGarageDoors) //********** change 1.0.0 add if block ***
            {
                if doorScrollIndex > 0
                {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute:
                      {
                        let indexPathScroll = IndexPath(row: self.doorScrollIndex-1, section:0 )
                        if self.homeTableView.numberOfRows(inSection: 0) > indexPathScroll.row
                        {
                            self.homeTableView.scrollToRow(at:indexPathScroll , at: .top, animated: true)
                        }
                    })
                }
            }
        }
    }
    
    //---------------------------------------
    func getchangedImage() -> UIImage?
    {
        //print("get changed Image")
        guard let refImage = self.imageView.image else
        {
            return nil;
        }
        
        var fullRefImage:UIImage?
        var paintRefImage:UIImage = UIImage.init();
        var paintRefImage1:UIImage = UIImage.init();
        var paintRefImage2:UIImage = UIImage.init(); //*************** change 0.0.40 add ***
        var garagRefImage:UIImage = UIImage.init();
        var garagMaskRefImage:UIImage = UIImage.init();
        var frontDrMaskRefImage:UIImage = UIImage.init();
        
        if ( !self.paintImageView.isHidden)
        {
            if let paintImageOverlay = self.paintImageView.image
            {
                paintRefImage = paintImageOverlay;
            }
        }
        
        if ( !self.paintImageView1.isHidden)
        {
            if let paintImageOverlay1 = self.paintImageView1.image
            {
                paintRefImage1 = paintImageOverlay1;
            }
        }
        
        //*************** change 0.0.40 add ***
        if ( !self.paintImageView2.isHidden)
        {
            if let paintImageOverlay2 = self.paintImageView2.image
            {
                paintRefImage2 = paintImageOverlay2;
            }
        }
        
        if let garagImageOverlay = self.garageImageView.image
        {
            garagRefImage = garagImageOverlay;
        }
        
        if let garagMaskOverlay = self.garageMaskView.image
        {
            garagMaskRefImage = garagMaskOverlay;
        }
        
        if let frontDrMaskOverlay = self.frontDrMaskView.image
        {
            frontDrMaskRefImage = frontDrMaskOverlay;
        }
        
        let hostRect = CGRect.init(x: 0, y: 0, width: refImage.size.width, height: refImage.size.height)
        
        var scaleFac:CGFloat = 10.92;
        var anImageScale:CGFloat = 1.0;
        
        if (hostRect.size.width >= hostRect.size.height)
        {
            anImageScale = hostRect.size.width / 4096.0
        } else {
            anImageScale = hostRect.size.height / 4096.0
        }
        
        scaleFac = scaleFac * anImageScale;
        let oldFrame = self.logoView.frame;
        //print("scaleFac:",scaleFac)
        
        self.logoView.frame.size = CGSize(width: 30.0*scaleFac, height: 30.0*scaleFac)
        
        let x = (5.0*scaleFac)
        let y = hostRect.size.height - (self.logoView.frame.size.height + (5.0*scaleFac))
        
        //print("          x y:",x, y)
        
        self.logoView.frame.origin = CGPoint(x: x, y: y)
        self.logoContainer.isHidden = false;
        
        UIGraphicsBeginImageContext(CGSize.init(width: hostRect.size.width, height: hostRect.size.height))
        refImage.draw(in: hostRect)
        paintRefImage.draw(in: hostRect)
        paintRefImage1.draw(in: hostRect)
        paintRefImage2.draw(in: hostRect)   //*************** change 0.0.40 change ***
        garagRefImage.draw(in: hostRect)
        garagMaskRefImage.draw(in: hostRect)
        frontDrMaskRefImage.draw(in: hostRect)
        self.logoView.image?.draw(in: self.logoView.frame)
        
        fullRefImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.logoView.frame = oldFrame;
        self.logoContainer.isHidden = true;
        
        return fullRefImage
    }
    
    //---------------------------------------
    @IBAction func shareButtonTapped(_ sender: Any)
    {
        self.shareVIview.isHidden = false;
    }
    
    //---------------------------------------
    @IBAction func shareVideoTapped(_ sender: Any)
    {
        self.shareVIview.isHidden = true;
        
        self.addBlurEffect()
        self.startAnimatingDots()
        
        _ = Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false)
        { timer in
            self.shareRun()
        }
    }
    
    // MARK: - Photo/imaging stuff
    //************** change 0.0.40 move to here ***
    //---------------------------------------------------------------------------
    func rescaleImage(image: UIImage?, hostSize: CGSize) -> UIImage?
    {
        guard let image0 = image else {return nil;}
        guard let cgImage0 = image0.cgImage else {return nil;}

        UIGraphicsBeginImageContextWithOptions(hostSize, false, 0)
        let context = UIGraphicsGetCurrentContext()!
        
        // Set the quality level to use when rescaling
        context.interpolationQuality = CGInterpolationQuality.high //default
        let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: hostSize.height)
        
        context.concatenate(flipVertical)
        context.draw(cgImage0, in: CGRect(x: 0.0,y: 0.0, width: hostSize.width, height: hostSize.height))
        
        let newImageRef = context.makeImage()! as CGImage
        let newImage = UIImage(cgImage: newImageRef)
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    //************** change 0.0.40 replace function ***
    //----------------------------------------------------------------------------------
    func shareRun()
    {
        guard let refImage = self.imageView.image else
        {
            self.removeBlurEffect()
            self.stopAnimatingDots()
            return;
        }
        
        var ratio:CGFloat = 1.0;
        let fullWidth:CGFloat = 1024.0; // 1024.0
        var width:CGFloat = fullWidth;
        var height:CGFloat = fullWidth;
        
        if (refImage.size.width >= refImage.size.height)
        {
            ratio = fullWidth / refImage.size.width
            height = refImage.size.height * ratio;
        } else {
            ratio = fullWidth / refImage.size.height
            width = refImage.size.width * ratio;
        }
        
        
        let hostRect = CGRect.init(x: 0, y: 0, width: width, height: height).integral
        var clipAmount:CGFloat = 20.0;
        var reverseClipAmount:CGFloat = fullWidth - 20.0;
        let numPixelsToMove:CGFloat = CGFloat((fullWidth - (clipAmount*2.0)) / 120.0);
        let numClips:Int = Int((fullWidth - (clipAmount*2.0)) / numPixelsToMove);
        
        var paintRefImage0:UIImage? = nil;
        var paintRefImage1:UIImage? = nil;
        var paintRefImage2:UIImage? = nil;
        
        var garagRefImage:UIImage? = nil;
        var garagMaskRefImage:UIImage? = nil;
        var frontDrMaskRefImage:UIImage? = nil;
        
        
        
        if ( !self.paintImageView.isHidden)
        {
            paintRefImage0 = rescaleImage(image: self.paintImageView.image, hostSize: hostRect.size);
        }
        
        if ( !self.paintImageView1.isHidden)
        {
            paintRefImage1 = rescaleImage(image: self.paintImageView1.image, hostSize: hostRect.size);
        }
        
        if ( !self.paintImageView2.isHidden)
        {
            paintRefImage2 = rescaleImage(image: self.paintImageView2.image, hostSize: hostRect.size);
        }
        
        
        
        garagRefImage = rescaleImage(image: self.garageImageView.image, hostSize: hostRect.size);
        garagMaskRefImage = rescaleImage(image: self.garageMaskView.image, hostSize: hostRect.size);
        frontDrMaskRefImage = rescaleImage(image: self.frontDrMaskView.image, hostSize: hostRect.size);
        
        //print("  ")
        //print("hostRect:",hostRect)
        //print("numPixelsToMove:",numPixelsToMove)
        //print("numClips:",numClips)
        //print("self.logoConta iner.frame:",self.logoCont ainer.frame)
        //print("ratio:",ratio)
        //print("self.scrollImg.zo omScale:",self.scrollImg.zo omScale)
        
        let scaleFac:CGFloat = 1.7;
        let oldFrame = self.logoView.frame;
        //print("scaleFac:",scaleFac)
        
        self.logoView.frame.size = CGSize(width: 30.0*scaleFac, height: 30.0*scaleFac)
        
        let x = (5.0*scaleFac)
        let y = height - (self.logoView.frame.size.height + (5.0*scaleFac))
        //print("          x y:",x, y)
        
        self.logoView.frame = CGRect.init(x: x, y: y, width: self.logoView.frame.size.width, height: self.logoView.frame.size.height).integral
        let imageL = rescaleImage(image: self.logoView.image, hostSize: self.logoView.frame.size);
        self.logoContainer.isHidden = false;
        var images = [UIImage]()
        
        for _ in 0..<numClips
        {
            UIGraphicsBeginImageContextWithOptions(hostRect.size,true,1.0)
            let context = UIGraphicsGetCurrentContext()!
            refImage.draw(in: hostRect)
            context.saveGState()
            
            context.clip(to: CGRect(x: reverseClipAmount, y: 0.0, width:width-reverseClipAmount, height:height))
            
            paintRefImage0?.draw(in:hostRect);
            paintRefImage1?.draw(in:hostRect);
            paintRefImage2?.draw(in:hostRect);
            
            garagRefImage?.draw(in:hostRect);
            garagMaskRefImage?.draw(in:hostRect);
            frontDrMaskRefImage?.draw(in:hostRect);
            
            context.restoreGState()
            imageL?.draw(in: self.logoView.frame)
            
            let image = UIGraphicsGetImageFromCurrentImageContext()!
            images.append(image)
            
            UIGraphicsEndImageContext()
            reverseClipAmount -= numPixelsToMove;
        }
        
        UIGraphicsBeginImageContextWithOptions(hostRect.size,true,1.0)
        
        refImage.draw(in: hostRect)
        
        paintRefImage0?.draw(in:hostRect);
        paintRefImage1?.draw(in:hostRect);
        paintRefImage2?.draw(in:hostRect);
        
        garagRefImage?.draw(in:hostRect);
        garagMaskRefImage?.draw(in:hostRect);
        frontDrMaskRefImage?.draw(in:hostRect);
        
        imageL?.draw(in: self.logoView.frame)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        for _ in 0..<40
        {
            images.append(image)
        }
        
        
        UIGraphicsBeginImageContextWithOptions(hostRect.size,true,1.0)
        let context3 = UIGraphicsGetCurrentContext()!
        
        for _ in 0..<numClips
        {
            refImage.draw(in: hostRect)
            context3.saveGState()
            
            context3.clip(to: CGRect(x: clipAmount, y: 0.0, width:width-clipAmount, height:height))
            
            paintRefImage0?.draw(in:hostRect);
            paintRefImage1?.draw(in:hostRect);
            paintRefImage2?.draw(in:hostRect);
            
            garagRefImage?.draw(in:hostRect);
            garagMaskRefImage?.draw(in:hostRect);
            frontDrMaskRefImage?.draw(in:hostRect);
            
            context3.restoreGState()
            imageL?.draw(in: self.logoView.frame)
            
            let image = UIGraphicsGetImageFromCurrentImageContext()!
            images.append(image)
            clipAmount += numPixelsToMove;
        }
        UIGraphicsEndImageContext()
        
        self.logoView.frame = oldFrame;
        self.logoContainer.isHidden = true;
        
        self.removeBlurEffect()
        self.stopAnimatingDots()
        
        self.makeVideo(imageArray:images, width:width, height:height)
    }
    
    //--------------------------------------------
    func makeVideo(imageArray:[UIImage]!, width:CGFloat, height:CGFloat)
    {
        let documentsDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let path = documentsDirectoryPath.appending("/videoFile.mp4")
        let outURL = URL(fileURLWithPath: path)
        
        try? FileManager.default.removeItem(at: outURL)
        
        let avOutputSettings: [String: Any] =
            [
                AVVideoCodecKey: AVVideoCodecType.h264,
                AVVideoWidthKey: NSNumber(value: Float(width)),
                AVVideoHeightKey: NSNumber(value: Float(height))
        ]
        
        let sourcePixelBufferAttributesDictionary =
            [
                kCVPixelBufferPixelFormatTypeKey as String: NSNumber(value: kCVPixelFormatType_32ARGB),
                kCVPixelBufferWidthKey as String: NSNumber(value: Float(width)),
                kCVPixelBufferHeightKey as String: NSNumber(value: Float(height))
        ]
        
        let videoWriter = try! AVAssetWriter(outputURL: outURL, fileType: AVFileType.mp4)
        let videoWriterInput = AVAssetWriterInput(mediaType: AVMediaType.video, outputSettings: avOutputSettings)
        videoWriter.add(videoWriterInput)
        
        let pixelBufferAdaptor = AVAssetWriterInputPixelBufferAdaptor(assetWriterInput: videoWriterInput, sourcePixelBufferAttributes: sourcePixelBufferAttributesDictionary)
        
        videoWriter.startWriting()
        videoWriter.startSession(atSourceTime: CMTime.zero)
        
        let fps:Int32 = 30;
        
        var frameCount: Int = 0;
        //   let numberOfSecondsPerFrame:CGFloat = 0.1; //0.1  0.03125
        let frameDuration:CGFloat = 1.0 //CGFloat(fps) * numberOfSecondsPerFrame
        
        //print("frameDuration:",frameDuration)
        //print("imageArray.count:",imageArray.count)
        for img in imageArray
        {
            var canAppend = false
            var j: Int = 0
            
            while !canAppend && j < 30
            {
                if pixelBufferAdaptor.assetWriterInput.isReadyForMoreMediaData
                {
                    let frameTime = CMTimeMake(value: Int64(CGFloat(frameCount) * frameDuration), timescale: fps)
                    
                    if let pixelBufferPool = pixelBufferAdaptor.pixelBufferPool
                    {
                        if let pixelBuffer = self.pixelBufferFromImage(image: img, pixelBufferPool: pixelBufferPool, width:width, height:height)
                        {
                            canAppend = pixelBufferAdaptor.append(pixelBuffer, withPresentationTime: frameTime)
                            
                            if !canAppend
                            {
                                let error: Error! = videoWriter.error
                                
                                if error != nil
                                {
                                    //print("Unresolved error:", error as Any)
                                }
                            }
                        }
                    }
                } else {
                    //print("adaptor not ready frameCount, j:", frameCount, j)
                    Thread.sleep(forTimeInterval: 0.1)  //0.1
                }
                
                j += 1
            }
            
            if !canAppend
            {
                //print("error appending image  frameCount, j:", frameCount, j)
            }
            
            frameCount += 1
        }
        
        videoWriterInput.markAsFinished()
		videoWriter.finishWriting()
		{
			DispatchQueue.main.async
			{
				var filesToShare = [Any]()
				filesToShare.append(outURL)

				let activityVC = UIActivityViewController(activityItems: filesToShare, applicationActivities: nil)
				activityVC.excludedActivityTypes = [.print,.assignToContact,.copyToPasteboard,.addToReadingList,.markupAsPDF,.postToTencentWeibo,.postToTencentWeibo]
				activityVC.popoverPresentationController?.sourceView = self.view
				activityVC.preferredContentSize = self.scrollImg.bounds.size  //********** change 3.0.3 add ***
				activityVC.modalPresentationStyle = .popover  //********** change 3.0.3 add ***
				activityVC.popoverPresentationController?.sourceRect = self.btnShare.frame;   //********** change 3.0.3 add ***
				activityVC.popoverPresentationController?.permittedArrowDirections = [];  //********** change 3.0.3 add ***

				self.present(activityVC, animated: true, completion: nil)
			}
        }
    }
    
    //--------------------------------------------
    func pixelBufferFromImage(image: UIImage, pixelBufferPool: CVPixelBufferPool, width:CGFloat, height:CGFloat) -> CVPixelBuffer?
    {
        var pixelBufferOut: CVPixelBuffer?
        let status = CVPixelBufferPoolCreatePixelBuffer(kCFAllocatorDefault, pixelBufferPool, &pixelBufferOut)
        
        if status != kCVReturnSuccess
        {
            print("CVPixelBufferPoolCreatePixelBuffer() failed")
            return nil;
        }
        let pixelBuffer = pixelBufferOut!
        
        CVPixelBufferLockBaseAddress(pixelBuffer, CVPixelBufferLockFlags(rawValue: CVOptionFlags(0)))
        
        let data = CVPixelBufferGetBaseAddress(pixelBuffer)
        
        let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
        let context:CGContext! = CGContext(data: data, width: Int(width), height: Int(height),
                                           bitsPerComponent: 8, bytesPerRow: CVPixelBufferGetBytesPerRow(pixelBuffer),
                                           space: rgbColorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue)
        
        context.clear(CGRect(x: 0, y: 0, width:width, height:height))
        
        context!.draw(image.cgImage!, in: CGRect(x:0.0, y:0.0, width:width, height:height))
        CVPixelBufferUnlockBaseAddress(pixelBuffer, CVPixelBufferLockFlags(rawValue: CVOptionFlags(0)))
        
        return pixelBuffer
    }
    
    //---------------------------------------
    @IBAction func shareImageTapped(_ sender: Any)
    {
        self.shareVIview.isHidden = true;
        scrollImg.setZoomScale(1.0, animated: false) // change zoom pixels
        
        let documentsDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let path = documentsDirectoryPath.appending("/modiOutImage.jpg")
        let outURL = URL(fileURLWithPath: path)
        
        try? FileManager.default.removeItem(at: outURL)
        
        guard let aNewImage = self.getchangedImage() else { return }
        self.newImage = aNewImage;
        
        if let data2c = self.newImage.jpegData(compressionQuality:1.0)
        {
            do
            {
                try data2c.write(to: outURL)

                DispatchQueue.main.async
				{
					var filesToShare = [Any]()
					filesToShare.append(outURL)

					let activityVC = UIActivityViewController(activityItems: filesToShare, applicationActivities: nil)
					activityVC.excludedActivityTypes = [.print,.assignToContact,.copyToPasteboard,.addToReadingList,.markupAsPDF,.postToTencentWeibo,.postToTencentWeibo]
					activityVC.popoverPresentationController?.sourceView = self.view
					activityVC.preferredContentSize = self.scrollImg.bounds.size  //********** change 3.0.3 add ***
					activityVC.modalPresentationStyle = .popover  //********** change 3.0.3 add ***
					activityVC.popoverPresentationController?.sourceRect = self.btnShare.frame;   //********** change 3.0.3 add ***
					activityVC.popoverPresentationController?.permittedArrowDirections = [];  //********** change 3.0.3 add ***

					self.present(activityVC, animated: true, completion: nil)
                }

            } catch {
  print("shareImageTapped Couldn't write file")
            }
        }
    }
    
    //---------------------------------------
    func colorObjectAt(index: Int, color: UIColor)
    {
        guard let sri = self.selectedReferenceItem, let ho = houseObject else { return }
        guard let refImage = sri.image else { return }
        
        if let img = ImageManager.colorObjectInReference(house0Object: ho, referenceImage: refImage, color: color)
        {
            self.imageView.image = img
        }
    }
    
    //-----------------------------------------------------
    @IBAction func galleryButtonTapped(_ sender: Any)
    {
//print("gallery Button Tapped")
        scrollImg.setZoomScale(1.0, animated: false)  //why?
        self.blurrImgView.isHidden = false
        self.loadGallery()  //********** left in for backwards compatibility,  but should be removed ***
        self.photoView.isHidden = false
        self.photoCollectionView.reloadData()
    }
    
    //-----------------------------------------------------
    @IBAction func cameraButtonTapped(_ sender: Any)
    {
        scrollImg.setZoomScale(1.0, animated: false) // why?
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedCamera")
        if launchedBefore
        {
            //print("Not first launch.")
            self.openCamera()
        } else {
            //print("First launch, setting UserDefault.")
            UserDefaults.standard.set(true, forKey: "launchedCamera")
            UserDefaults.standard.synchronize()
            self.openDeviceCamera()
        }
    }
    
    //------------------------------------------------------
    @IBAction func btnFavClicked(_ sender: Any)
    {
        if (self.btnFavWasClicked)
        {
            self.removeFavoriteAPI()
            
        } else {
            
            wasALastFavorite = true;
            
            if UserDefaults.standard.bool(forKey: "signUpDone") == false
            {
                self.showLoginController()
            } else {
                self.createFavoriteAPI()
            }
        }
    }
    
    //--------------------------------------------------------------------
    func removeFavoriteAPI()
    {
    //print("remove Favorite API")
            self.btnFavWasClicked = false
            self.btnFav.setImage(UIImage(named: "heart-v1"), for: .normal)

            if let catArray = self.checkCombination()  //*************** change 1.0.2 add if block ***
            {
                self.btnFav.accessibilityLabel = catArray.description
    //print("before remove Favorite Scene catArray:",catArray)

                let filename = currentImageName.replacingOccurrences(of:".jpg", with: "")

                self.removeFavoriteFromDocumentDirectory(catArray:catArray, filename:filename)
            }
        }
    
    //--------------------------------------------------------------------
    func createFavoriteAPI()
    {
//print("create Favorite API")
		if self.imgLocation == nil
		{
			self.imgLocation = CLLocation(latitude: 0, longitude: 0)
		}

		if (self.garageCatID == "" && self.doorCatID == ""  && self.paintImageView.image == nil)
		{
//print("create Favorite API  alert")
			let alertController = UIAlertController(title: "Swipe the slide below first:",
													message: "Please select a garage or door or paint to favorite",
													preferredStyle: UIAlertController.Style.alert)
			alertController.addAction(UIAlertAction(title: "ok", style: .default, handler:
			{ (action) in
					self.dismiss(animated: true, completion: nil)
			}))


			if let popoverController = alertController.popoverPresentationController  //********** change 3.0.3 add if block ***
			{
			  popoverController.sourceView = self.view
			  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
			  popoverController.permittedArrowDirections = []
			}

			self.present(alertController, animated: true, completion:
			{
			})
			return
		}


		self.btnFavWasClicked = true
		self.btnFav.setImage(UIImage(named: "heart-v2"), for: .normal)

		if let catArray = self.checkCombination()
		{
			self.btnFav.accessibilityLabel = catArray.description
//print("before create Favorite Scene catArray.description:",catArray.description)

			let filename = currentImageName.replacingOccurrences(of:".jpg", with: "")
//print("before create Favorite Scene filename:",filename)

			guard let fullRefImage:UIImage = self.getchangedImage() else { return }

			self.saveFavoriteToDocumentDirectory(catArray:catArray, image0:fullRefImage, filename:filename)
		}
	}
    
    //--------------------------------------------------------------------
    func createDirectory(directoryName:String)
    {
        //print("create Directory.")
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(directoryName)
        if !fileManager.fileExists(atPath: paths)
        {
            try! fileManager.createDirectory(atPath: paths, withIntermediateDirectories: true, attributes: nil)
        } else {
  print("dictionary Already created.")
        }
    }
    
    //--------------------------------------------------------------------
    func deleteDirectory(directoryName:String)
    {
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(directoryName)
        if fileManager.fileExists(atPath: paths){
            try! fileManager.removeItem(atPath: paths)
        } else {
            print("Something wrong.")
        }
    }
    
    //--------------------------------------------------------------------
    func getDirectoryPath() -> String
    {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    //--------------------------------------------------------------------
    func getImage(imageName:String)
    {
        let fileManager = FileManager.default
        let imagePath = (self.getDirectoryPath() as NSString).appendingPathComponent(imageName)
        if fileManager.fileExists(atPath: imagePath)
        {
            self.imageView.image = UIImage(contentsOfFile: imagePath)
        } else {
            //print("No Image")
        }
    }
    
    //--------------------------------------------------------------------
    func resizeImage(image: UIImage, newSize: CGSize) -> UIImage
    {
        //print("modified VC resi zeImage ");
        var newImage: UIImage
        
        let renderFormat = UIGraphicsImageRendererFormat.default()
        renderFormat.scale = 1.0;
        renderFormat.opaque = true
        let renderer = UIGraphicsImageRenderer(size: newSize, format: renderFormat)
        newImage = renderer.image
            {
                (context) in
                image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        }
        
        return newImage
    }
    // fetch images web
    // simulator has already moved on
    func jsonData(from object:Any) -> String?{
        
        
        do {
            let data = try JSONSerialization.data(withJSONObject: object, options: [])
            if let string = String(data: data, encoding: String.Encoding.utf8) {
                return string
            }
        } catch {
            print("error:",error)
        }
        return nil
    }
    
    //--------------------------------------------------------------------
    func convertIntoJSONString(arrayObject: Any) -> String?
    {
        do
        {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: arrayObject, options: [])
            if  let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) {
                return jsonString as String
            }
            
        } catch let error as NSError {
            print("Array convertIntoJSON - \(error.description)")
        }
        return nil
    }
    
    
    
    //--------------------------------------------------------------------
    func fetchHouseObject(completion: @escaping (HouseObject?) -> Void)
    {
        //print(" ")
        //print(" ")
        //print(" ")
        //print("home fetch House Object ----------")
        canFindPaths = false;
        
        //        clearThePaint()
        self.nodoorLabel.isHidden = false
        self.oneCarImgae = nil
        self.twoCarImgae = nil
        self.oneDoorImgae = nil
        self.twoDoorImgae = nil
        
        self.garageScrollIndex = 0
        self.doorScrollIndex = 0
        self.paintScrollIndex = 0
        self.btnFav.isEnabled = true
        
        isTwoCar = false
        isOneCar = false
        isDoor = false
        isDouble = false
        
        self.btnGarageClicked = false
        self.btnListClicked = false
        self.btnPaintListClicked = false
        
        self.btnDoorClicked = false
        self.setupButtonSelection()
        //print("ho me fetch House Object originalHomeImage:",originalHomeImage)
        
        sceneID = ""
        self.btnFavWasClicked = false
        self.btnFav.setImage(UIImage(named: "heart-v1"), for: .normal)
        //     guard let originalRef = selectedReferenceItem else { return }
        //     guard let refImageOrig = originalRef.image else { return }
        
        guard let refImageOrig = originalHomeImage else { return }
        guard let _ = originalHomeImage.cgImage else {return;}
        
        destImageWidth  = 640.0;
        destImageHeight = 480.0;
        
        if (refImageOrig.size.height > refImageOrig.size.width)
        {
            let tempWidth = destImageWidth;
            destImageWidth  = destImageHeight;
            destImageHeight = tempWidth;
        }
        
        //print("fetchHouseObject   destImageWidth,destImageHeight,originalHomeImage.size:",destImageWidth,destImageHeight,originalHomeImage.size)
        
        let size = CGSize.init(width: destImageWidth, height: destImageHeight)
        
        var refImage = self.resizeImage(image: refImageOrig, newSize: size)
        //print("refIm age.size",refImage.size)
        
        if let imageDataOrig = refImageOrig.jpegData(compressionQuality:1.0)
        {
            //print("imageDataOrig",imageDataOrig)
            if (imageDataOrig.count > 2000000)
            {
                if let imageData = refImage.jpegData(compressionQuality:0.50)
                {
                    if let refImage00 = UIImage(data: imageData)
                    {
                        refImage = refImage00;
                    }
                }
            }
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd_HH-mm-ss"
        
        var userN = "wasBlank"
        if let user = UserDefaults.standard.string(forKey: "username")
        {
            userN = user;
        }
        
        userN = userN+" lat="
        
        var imageName = currentImageName
        var fileName = currentImageName
        
        if hasInternalApp
        {
            var lat = "0"
            var lng = "0"
            let agentId = DataManager.checkAgentFileUrlWithName(imageName:currentImageName)
            if agentId.count > 0
            {
                if ( !hasOnlyGarageDoors)   //*************** change 1.0.2 add if block ***
                {
                    completeBtn.isHidden = false
//print("fetchHouseObject completeBtn")
                }

			} else {
                completeBtn.isHidden = true
                
            }
            if let assetLocal = self.asset
            {
                imageName = currentImageName; //********* change 2.1.4  imageName = assetLocal.originalFilename ?? currentImageName;
                let date = assetLocal.creationDate ?? Date()
                let dateString = formatter.string(from: date)
                
                fileName = imageName+" date="+dateString+" username="
                
                if let location = location0
                {
                    lat = String(location.coordinate.latitude)
                    lng = String(location.coordinate.longitude)
                }
            } else {
                image_ID = currentImageName
                let date = Date()
                image_date = formatter.string(from: date)
                fileName = image_ID+" date="+image_date+" username="
            }
            userN = userN+lat+" lng="
            userN = userN+lng
        }
        
        fileName = fileName+userN;
        
        DispatchQueue.main.async
		{
			self.timeLeft = 20
			if (self.timer != nil)
			{
				self.timer?.invalidate()
				self.timer = nil
			}
//print("fetch garimageID:",garimageID as Any)
			garimageID = "" //***************  change 1.1.0 change ***
			doorimageID = "" //***************  change 1.1.0 change ***
			self.doorCatID = ""
			self.garageCatID = ""
			self.garageCatName = ""

			//noProcessing = true; //+++++++++++++ remove :(for testing only)+++

			//print("noProcessing:",noProcessing)
			//print("isHomeApp:",isHomeApp)

			if (noProcessing || isHomeApp)
			{
				//noProcessing = false; //+++++++++++++ remove :(for testing only)+++

			} else {
				self.view.aj_showDotLoadingIndicator()
			}

			DataManager.getImage(fileName:currentImageName, completion:
			{(error, json) in
					//print("  ")
					//print("  DataMa nager.getImage ")
					//print("error:",error as Any)
					//print("home fet ch House Object DataMan ager.getImage json: ",json as Any)
					//print("  ")
					//print("  ")

					if json != nil && json!.count > 0
					{
//print("  DataMan ager.getImage good json")
						let bundleIdentifier = Bundle.main.bundleIdentifier
						let bundleName = String(describing:bundleIdentifier)
						if let receivedData1 = KeyChain.load(key: "\(bundleName)agentzipcode")
						{
  print("agentzipcode: \(receivedData1)")
						} else {

							self.popOverVC = UIStoryboard(name: "CustomTabBar", bundle: nil).instantiateViewController(withIdentifier: "LocationPopUp") as? LocationPopUp
							self.popOverVC.view.frame = self.view.frame
							self.popOverVC.delegate = self
							self.popOverVC.modalPresentationStyle = .overCurrentContext
							self.popOverVC.modalPresentationCapturesStatusBarAppearance = true
							self.popOverVC.preferredContentSize = self.view.bounds.size   //********** change 3.0.3 add  ***

							UserDefaults.standard.set(true, forKey: "shopping")
							self.present(self.popOverVC, animated: true, completion: nil)
						}

						self.saveImageToDocumentDirectory(image0:originalHomeImage,type:".jpg")

						completion(nil)
						self.fetchFromJson(json: json)

					} else {

						//print("  ")
						//print(" DataMana ger.getImage bad json  hous eObject:",houseObject as Any)

						if houseObject == nil
						{
							var editedGarage = false
							let house_filename = currentImageName.replacingOccurrences(of:".jpg", with: "_houseObject.txt")
							//print("fetch From J son house_filename:",house_filename)
							if self.checkHouseProjectExist(filename:house_filename) != nil
							{
								editedGarage = true
							}
							if (noProcessing||isHomeApp||editedGarage)
							{
								self.view.aj_hideDotLoadingIndicator()
								self.saveImageToDocumentDirectory(image0:originalHomeImage,type:".jpg")    //********** change 0.0.40 change ***

								//        self.tableViewTopConstrain.constant  = 0   //*************** change 1.0.2 remove ***
//                                    self.homeTableView.reloadData()
//                                    self.homeTableView.scroll(to: .top, animated: false)
								completion(nil)
								self.fetchFromJson(json: json)

							} else {

								let agentId = DataManager.checkAgentFileUrlWithName(imageName:currentImageName)
								//print("  ")
								//print(" fetch house agentId:",agentId)

								DataManager.getObjectJsonNew(paramName: "image",agentID: agentId, fileName: fileName, image: refImage,imageFromGalleryName: currentImageName)
								{ (error, jsonresul) in

									//print("  ")
									//print(" DataManager.getObjectJsonNew ")
									//print("error:",error as Any)
									//print("fetch House Object json: \(String(describing: jsonresul))")
									//print("  ")
									//print("  ")
									if let shopping = UserDefaults.standard.value(forKey: "shopping")
									{
										print("shopping: \(shopping)")
									} else {

										//print("  ")
										//print("  ")
										//print("fetch house UIStoryboard(name: CustomTabBar   complet ion(nil) ")
										//print("  ")
										//print("  ")
										self.popOverVC = UIStoryboard(name: "CustomTabBar", bundle: nil).instantiateViewController(withIdentifier: "LocationPopUp") as? LocationPopUp
										self.popOverVC.view.frame = self.view.frame
										self.popOverVC.modalPresentationStyle = .overCurrentContext
										self.popOverVC.delegate = self
										self.popOverVC.modalPresentationCapturesStatusBarAppearance = true
										self.popOverVC.preferredContentSize = self.view.bounds.size   //********** change 3.0.3 add  ***

										UserDefaults.standard.set(true, forKey: "shopping")
										self.present(self.popOverVC, animated: true, completion: nil)
									}

									if let e = error
									{
										DispatchQueue.main.async(execute:
										{
												//print("  ")
												//print("  ")
												//print("fetch house let e = error DispatchQueue.main.async  complet ion(nil) ")
												//print("  ")
												//print("  ")
												self.view.aj_hideDotLoadingIndicator()
												let alertController = UIAlertController(title: "Error",
																						message: "Error processing photo\n\n\(e.localizedDescription)",
													preferredStyle: UIAlertController.Style.alert)
												alertController.addAction(UIAlertAction(title: "Dismiss", style: .default, handler:
													{ (action) in
														//print("fetch house self.dismiss   complet ion(nil) ")
														self.dismiss(animated: true, completion: nil)
												}))

												if let popoverController = alertController.popoverPresentationController  //********** change 3.0.3 add if block ***
												{
												  popoverController.sourceView = self.view
												  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
												  popoverController.permittedArrowDirections = []
												}

												self.present(alertController, animated: true, completion:
												{
														//print("fetch house self.present   complet ion(nil) ")
												})
										})

										//print("  ")
										//print("  ")
										//print("fetch house let e = error   complet ion(nil) return")
										//print("  ")
										//print("  ")

										completion(nil)
										return
									}

									//print("fetch house was not a json before fetch    jsonresul:",jsonresul as Any)
									if jsonresul != nil
									{
										LoadingIndicatorView.hide()
										//print("before 1 save Image ToDocument Directory originalHomeImage:",originalHomeImage as Any)
										self.saveImageToDocumentDirectory(image0:originalHomeImage,type:".jpg")  //********** change 0.0.40 change ***
										//print("  ")
										//print("  ")
										//print("fetch house self.loadUpPaint   comple tion(nil)")
										//print("  ")
										//print("  ")


										//        self.tableViewTopConstrain.constant  = 0   //*************** change 1.0.2 remove ***
//                                            self.homeTableView.reloadData()
//                                            self.homeTableView.scroll(to: .top, animated: false)

										if ( !commingFromProjectList)
										{
											self.loadUpPaint()
										}

										completion(nil)
										self.fetchFromJson(json: jsonresul!)

									} else {

										//print("  ")
										//print("  ")
										//print("fetch house self.saveImageToDocumentDirectory   complet ion(nil)")
										//print("  ")
										//print("  ")

										self.view.aj_hideDotLoadingIndicator()
										self.saveImageToDocumentDirectory(image0:originalHomeImage,type:".jpg")  //********** change 0.0.40 change ***

										//        self.tableViewTopConstrain.constant  = 0   //*************** change 1.0.2 remove ***
//                                            self.homeTableView.reloadData()
//                                            self.homeTableView.scroll(to: .top, animated: false)
										completion(nil)
										self.fetchFromJson(json: json)
									}

								}
							}
						} else {

							//print("  ")
							//print("  ")
							//print("fetch house self.view.aj_hideDotLoadingIndicator   comple tion(nil)")
							//print("  ")
							//print("  ")

							self.view.aj_hideDotLoadingIndicator()
							completion(nil)
							self.fetchFromJson(json: json)
						}
					}
			})
		}
    }
    
    
    //------------------------------------
    func loadUpPaint()
    {
        //print("loadU pPaint originalHomeImage:",originalHomeImage as Any)
        
        guard let refImageOrig = originalHomeImage else { return }
        guard let _ = originalHomeImage.cgImage else {return;}
        
        maskTargetSize = refImageOrig.size;
        
        maskedHouseReady0 = false;
        maskedHouseReady1 = false;
        noPaint = false;
        
        
        paintImageWidth  = 2048.0;
        paintImageHeight = 1536.0;
        
        if (maskTargetSize.height > maskTargetSize.width)
        {
            let tempWidth:CGFloat = paintImageWidth;
            paintImageWidth  = paintImageHeight;
            paintImageHeight = tempWidth;
        }
        
        let size = CGSize(width:CGFloat(paintImageWidth), height:CGFloat(paintImageHeight));
        
        let refImage = self.resizeImage(image: refImageOrig, newSize: size)
        
        //print("start:  \(self.paintTi meLeft) seconds left")
        paintTimeLeft = paintMaxTime;
        //print("start:  \(self.paintTi meLeft) seconds left")
        
        self.paintTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.onPaintTimerFired), userInfo: nil, repeats: true)
        
        if hasInternalApp
        {
            //print("loadU pPaint getPai ntJsonAfterGarages")
            self.getPaintJsonAfterGarages(paramName: "image", fileName: currentImageName, image: refImage)
            { (error, json) in
                
                //print("loadU pPaint getPa intJson error:",error as Any)
                
                self.paintTimer?.invalidate()
                if let e = error
                {
                    print("0 load Up Paint e:",e)
                    noPaint = true;
                } else {
                    //print("0 canFin dPaths")
                    canFindPaths = true;
                }
            }
        } else if isHomeApp {
            self.paintTimer?.invalidate()
            canFindPaths = true;
        }
    }
    
    //---------------------------------------------------------------------
    func getPaintJsonAfterGarages(paramName: String, fileName: String, image: UIImage, completion: @escaping (NSError?, String?) -> Void)
    {
        //print("getPai ntJsonAft erGarages fileName:",fileName)
        sidings.removeAll();
        houses.removeAll();
        roofs.removeAll();
        windows.removeAll();
        trims.removeAll();
        
        guard let imageData = image.jpegData(compressionQuality: 1.0) else
        {
            let error = NSError(domain:"com.modifai", code:1008, userInfo:[ NSLocalizedDescriptionKey: "bad Image data, camera (or photo from library) working?"])
            completion(error, nil)
            return
        }
        
        // generate boundary string using a unique per-app string
        let boundary = UUID().uuidString
        
        let session = URLSession.shared
        
        // Set the URLRequest to POST and to the specified URL
        var urlRequest = URLRequest(url: paintUrl)
        urlRequest.httpMethod = "POST"
        urlRequest.timeoutInterval = 75.0
        
        // Set Content-Type Header to multipart/form-data, this is equivalent to submitting form data with file upload in a web browser
        // And the boundary is also set here
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        var data = Data()
        
        // Add the image data to the raw http request data
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"\(paramName)\"; filename=\"\(fileName)\"\r\n".data(using: .utf8)!)
        data.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
        data.append(imageData)
        
        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
        
        //print("   ");
        //print("   ");
        //print(" fileName: ",fileName);
        
        // Send a POST request to the URL, with the data we created earlier
        session.uploadTask(with: urlRequest, from: data, completionHandler:
            { responseData, response, error0 in
                
                //print("  ")
                //print("  ")
                //print("getPa intJson error0aa: \(error0?.localizedDescription ?? "no error")")
                //print("  ")
                //print("  ")
                //print("responseData:",responseData as Any)
                //print("response:",response as Any)
                //print("error0:",error0 as Any)
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 500
                {
                    let error = NSError(domain:"com.modifai", code:500, userInfo:[ NSLocalizedDescriptionKey: "the server may not be responding."])
                    //print("error:",error as Any)
                    completion(error as NSError, nil)
                    return
                } else if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                    let error = NSError(domain:"com.modifai", code:httpStatus.statusCode, userInfo: [ NSLocalizedDescriptionKey: "unknown status code, something with the server side"])
                    completion(error as NSError, nil)
                    return
                }
                
                
                if error0 == nil
                {
                    if let r = responseData
                    {
                        do
                        {
                            if let jsonDict:Dictionary = try JSONSerialization.jsonObject(with: r, options: .allowFragments) as? Dictionary<String, Any>
                            {
                                //print(" jsonDict : ",jsonDict);
                                if let responseDict = jsonDict["response"] as? NSArray
                                {
                                    for innerResponse in responseDict
                                    {
                                        if let responseAr = innerResponse as? NSArray
                                        {
                                            for responseArDict in responseAr
                                            {
                                                if let responseArInDict = responseArDict as? NSDictionary
                                                {
                                                    ////print(" responseArInDict : ",responseArInDict);
                                                    ////                                                if (paintOnorOff)
                                                    ////                                                {
                                                    if let ptArrayTop = responseArInDict.value(forKey: "paintableMasks") as? NSArray
                                                    {
                                                        //print(" type(of: ptArrayTop : ",type(of: ptArrayTop));
                                                        houses.append(ptArrayTop);
                                                        sidings.append(ptArrayTop);
                                                    }
                                                    ////                                                } else {
                                                    if let ptArrayTop = responseArInDict.value(forKey: "houseMasks") as? NSArray
                                                    {
                                                        //print(" type(of: ptArrayTop : ",type(of: ptArrayTop));
                                                        houses.append(ptArrayTop);
                                                    }
                                                    //                                                }
                                                    
                                                    if let ptArrayTop = responseArInDict.value(forKey: "roofMasks") as? NSArray
                                                    {
                                                        //print(" type(of: ptArrayTop : ",type(of: ptArrayTop));
                                                        roofs.append(ptArrayTop);
                                                    }
                                                    
                                                    if let ptArrayTop = responseArInDict.value(forKey: "windowMasks") as? NSArray
                                                    {
                                                        //print("PaintViewC  windowMasks type(of: ptArrayTop : ",type(of: ptArrayTop));
                                                        roofs.append(ptArrayTop);
                                                    }
                                                    
                                                    if let ptArrayTop = responseArInDict.value(forKey: "trimMasks") as? NSArray
                                                    {
                                                        //print("PaintViewC  trimMasks type(of: ptArrayTop : ",type(of: ptArrayTop));
                                                        trims.append(ptArrayTop);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                            //print("   ");
                            //print("   ");
                            //print(" hous es.count :",hou ses.count);
                            //print(" roo  fs.count :",roofs.count);
                            //print(" windo ws.count:",windows.count);
                            //print(" tri ms.count:",tri ms.count);
                            //print("   ");
                            //print("   ");
                            
                            //print(" hous es:",hou ses);
                            //print(" hous es:",hou ses);
                            //print(" windo ws:",windows);
                            completion(nil, nil)
                            
                        } catch {
                            //print(" json error");
                            let error00 = NSError(domain:"com.modifai", code:1001, userInfo:[ NSLocalizedDescriptionKey: error0?.localizedDescription ?? "no contents or json data returned incorrectly"])
                            completion(error00 as NSError, nil)
                        }
                    } else {
                        let error = NSError(domain:"com.modifai", code:1006, userInfo:[ NSLocalizedDescriptionKey: "is cell or wifi slow or none existent?"])
                        completion(error as NSError, nil)
                    }
                } else {
                    let error00 = NSError(domain:"com.modifai", code:1001, userInfo:[ NSLocalizedDescriptionKey: error0?.localizedDescription ?? "not actually error:1001, not a known error"])
                    completion(error00 as NSError, nil)
                }
        }).resume()
    }
    
    //    {
    //print("0 getPaintJs onAfterGarages")
    //        houses.removeAll();
    //        roofs.removeAll();
    //        windows.removeAll();
    //
    //        guard let imageData = image.jpegData(compressionQuality: 1.0) else
    //        {
    //            let error = NSError(domain:"com.modifai", code:1008, userInfo:[ NSLocalizedDescriptionKey: "bad Image data, camera (or photo from library) working?"])
    //            completion(error, nil)
    //            return
    //        }
    //
    //        // generate boundary string using a unique per-app string
    //        let boundary = UUID().uuidString
    //
    //        let session = URLSession.shared
    //
    //        // Set the URLRequest to POST and to the specified URL
    //        var urlRequest = URLRequest(url: paintUrl)
    //        urlRequest.httpMethod = "POST"
    //        urlRequest.timeoutInterval = Double(paintMaxTime)
    //
    //        // Set Content-Type Header to multipart/form-data, this is equivalent to submitting form data with file upload in a web browser
    //        // And the boundary is also set here
    //        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
    //
    //        var data = Data()
    //
    //        // Add the image data to the raw http request data
    //        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
    //        data.append("Content-Disposition: form-data; name=\"\(paramName)\"; filename=\"\(fileName)\"\r\n".data(using: .utf8)!)
    //        data.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
    //        data.append(imageData)
    //
    //        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
    //
    //
    //        // Send a POST request to the URL, with the data we created earlier
    //        session.uploadTask(with: urlRequest, from: data, completionHandler:
    //            { responseData, response, error0 in
    //
    //                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 500
    //                {
    //                    let error = NSError(domain:"com.modifai", code:500, userInfo:[ NSLocalizedDescriptionKey: "the server may not be responding."])
    //                    completion(error as NSError, nil)
    //                    return
    //                } else if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
    //                    let error = NSError(domain:"com.modifai", code:httpStatus.statusCode, userInfo: [ NSLocalizedDescriptionKey: "unknown status code, something with the server side"])
    //                    completion(error as NSError, nil)
    //                    return
    //                }
    //
    //
    //
    //                if error0 == nil
    //                {
    //                    if let r = responseData
    //                    {
    //                        do
    //                        {
    //                            if let jsonDict:Dictionary = try JSONSerialization.jsonObject(with: r, options: .allowFragments) as? Dictionary<String, Any>
    //                            {
    ////print("getPa intJson jsonDict : ",jsonDict);
    //                                if let responseDict = jsonDict["response"] as? NSArray
    //                                {
    //                                    for innerResponse in responseDict
    //                                    {
    //                                        if let responseAr = innerResponse as? NSArray
    //                                        {
    //                                            for responseArDict in responseAr
    //                                            {
    //                                                if let responseArInDict = responseArDict as? NSDictionary
    //                                                {
    ////print(" responseArInDict : ",responseArInDict);
    //                                                    if (paintOnorOff)
    //                                                    {
    //                                                        if let ptArrayTop = responseArInDict.value(forKey: "paintableMasks") as? NSArray
    //                                                        {
    ////print(" type(of: ptArrayTop : ",type(of: ptArrayTop));
    //                                                            houses.append(ptArrayTop);
    //                                                        }
    //                                                    } else {
    //                                                        if let ptArrayTop = responseArInDict.value(forKey: "hous eMasks") as? NSArray
    //                                                        {
    ////print(" type(of: ptArrayTop : ",type(of: ptArrayTop));
    //                                                            houses.append(ptArrayTop);
    //                                                        }
    //                                                    }
    //
    //                                                    if let ptArrayTop = responseArInDict.value(forKey: "trimMasks") as? NSArray
    //                                                    {
    ////print("homeViewC trimMasks: ptArrayTop : ",type(of: ptArrayTop));
    //                                                        roofs.append(ptArrayTop);
    //                                                    }
    //
    //                                                    if let ptArrayTop = responseArInDict.value(forKey: "roofMasks") as? NSArray
    //                                                    {
    ////print(" type(of: ptArrayTop : ",type(of: ptArrayTop));
    //                                                        roofs.append(ptArrayTop);
    //                                                    }
    //
    //                                                    if let ptArrayTop = responseArInDict.value(forKey: "windowMasks") as? NSArray
    //                                                    {
    ////print("homeViewC windowMasks type(of: ptArrayTop : ",type(of: ptArrayTop));
    //                                                        windows.append(ptArrayTop);
    //                                                    }
    //                                                }
    //                                            }
    //                                        }
    //                                    }
    //                                }
    //                            }
    //
    //                            completion(nil, nil)
    //
    //                        } catch {
    //                            let error00 = NSError(domain:"com.modifai", code:1001, userInfo:[ NSLocalizedDescriptionKey: error0?.localizedDescription ?? "no contents or json data returned incorrectly"])
    //                            completion(error00 as NSError, nil)
    //                        }
    //                    } else {
    //                        let error = NSError(domain:"com.modifai", code:1006, userInfo:[ NSLocalizedDescriptionKey: "is cell or wifi slow or none existent?"])
    //                        completion(error as NSError, nil)
    //                    }
    //                } else {
    //                    let error00 = NSError(domain:"com.modifai", code:1001, userInfo:[ NSLocalizedDescriptionKey: error0?.localizedDescription ?? "not actually error:1001, not a known error"])
    //                    completion(error00 as NSError, nil)
    //                }
    //        }).resume()
    //    }
    
    
    //------------------------------------
    class func generateBoundaryString() -> String
    {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    
    //------------------------------------
    func checkHouseProjectExist(filename:String) -> HouseObject?
    {
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        //print("che ckHouse ProjectExist paths:",paths)
        
        if let dirPath = paths.first
        {
            do {
                let houseURL = URL(fileURLWithPath: dirPath).appendingPathComponent(filename)
                //print("che ckHouse ProjectExist houseURL:",houseURL)
                if let nsData = NSData(contentsOfFile: houseURL.path) {
                    do {
                        let data = Data(referencing:nsData)
                        
                        let decoder = JSONDecoder()
                        if let house0Object = try? decoder.decode(HouseObject.self, from: data) as HouseObject
                        {
                            return house0Object
                        }
                    }
                    
                }
                
            }
        }
        return nil
    }
    
    //------------------------------------------------
    func fetchFromJson(json: String?)
    {
//print("  ")
//print("  ")
//print("fetch From J son")
//print("fetch From J son garimageID:",garimageID as Any)
        garimageID = ""  //***************  change 1.1.0 change ***
        doorimageID = ""  //***************  change 1.1.0 change ***
        self.doorCatID = ""
        self.garageCatID = ""
        self.garageCatName = ""
        
        //    currentJs onString = j son
        //   var hous eObject:Hou seObject?
        if hasInternalApp
        {
            let agentId = DataManager.checkAgentFileUrlWithName(imageName:currentImageName)
            if agentId.count > 0
            {
                if ( !hasOnlyGarageDoors) //********** change 1.0.0 add if block ***
                {
                        completeBtn.isHidden = false
//print("fetchFromJson completeBtn")
				}
            } else {
                completeBtn.isHidden = true
            }
        }
        let house_filename = currentImageName.replacingOccurrences(of:".jpg", with: "_houseObject.txt")
        //print("fetch From J son house_filename:",house_filename)
        if let house0Object = self.checkHouseProjectExist(filename:house_filename)
        {
//            if (houseObject == nil) // it is causing issue for garage misplace
//            {
                houseObject = house0Object
                
                let lastImageUrl = house0Object.image_url[0] as String
                //print("existed savingUrl",lastImageUrl)
           
                UserDefaults.standard.set(lastImageUrl, forKey:"Last_Image_url")
            
           // }
            
        } else {
            
            //print("2 fetchFr omJson  before json:",json as Any)
            if let json0 = json
            {
                let house0Object = HouseObject(jsonString:json0)
                houseObject = house0Object
                //       skipDoors = false;   //************** change 0.0.46 remove ***
                
                let lastImageUrl = house0Object.image_url[0] as String
                //print("didn't exist savingUrl",lastImageUrl)
               
                    UserDefaults.standard.set(lastImageUrl, forKey:"Last_Image_url")
                
                if addedImageFromCameraStr != "library"
                {
                    currentImageName = (lastImageUrl as NSString).lastPathComponent
                    //print("2 self.currentIma geName:",currentImageName)
                }
                
                currentImageName = currentImageName.changeExtensions(name:currentImageName)
                
                //print("2 fetchFromJson  before encod eThePhoto self.currentIma geName:",currentImageName)
                
                //print("fetchFr omJson  destImageWidth,destImageHeight,originalHomeImage.size:",destImageWidth,destImageHeight,originalHomeImage.size)
                
                self.encodeThePhoto()
                self.saveHouseToDocumentDirectory(house0:house0Object);
            }
        }
        
        //print(" after saving or retrieving ")
        
        self.checkDoorsAndGarages(from:"first")
        // load up garages in reference item
        DispatchQueue.main.async(execute:
            {
                self.view.aj_hideDotLoadingIndicator()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute:
                {
                self.homeTableView.reloadData()
                
                })//************ add 2.0.9
                
                self.nodoorLabel.isHidden = false
                if isTwoCar || isOneCar
                {
                    self.btnGarageClicked = true
                    self.btnDoorClicked = false
                    self.btnPaintListClicked = false
                    
                    //print("fetch From Json self.btnGar ageClicked self.bt nGarage:",self.btnGar age as Any)
                    self.btnGarageClicked(self.btnGarage as Any)
                } else if isDoor {
                    self.btnGarageClicked = false
                    self.btnPaintListClicked = false
                    self.btnDoorClicked = true
                    self.btnDoorsClicked(self.btnDoors as Any)
                } else {
                    //
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute:
                        {
                            self.paintScrollIndex = 0
                            //
                            self.btnPaintClicked(self.btnPaint as Any)
                    })
                    
                }
                //   }
        })
    }
    
    //------------------------------------------------
    func checkDoorsAndGarages(from:String)
    {
        //print("  ")
        //print("checkDo orsAndGarages")
        if let house0Object = houseObject
        {
            for resp in house0Object.response
            {
                for respItem in resp
                {
                    
                    if (respItem.labels.count > 0)
                    {
                        //print("  ")
                        //print("respItem.labe ls[0]:",respItem.labels[0])
                        let lbl = respItem.labels[0]
                        
                        if lbl.contains("two-car")
                        {
                            isTwoCar = true
                            //print("self.isTwoCar:",self.isTwoCar)
                        }
                        if lbl.contains("one-car")
                        {
                            isOneCar = true
                            //print("self.isOneCar:",self.isOneCar)
                        }
                        
                        if lbl.contains("door")
                        {
                            //print("lbl:",lbl)
                            if respItem.rois.count>0 || respItem.roisinner.count>0 || respItem.roisouter.count>0
                            {
                                //print("respItem.rois.count:",respItem.rois.count)
                                //print("respItem.roisinner.count:",respItem.roisinner.count)
                                //print("respItem.roisouter.count:",respItem.roisouter.count)
                                if lbl.contains("double")
                                {
                                    isDouble = true
                                } else {
                                    isDouble = false
                                }
                                
                                isDoor = true
                                //print("fetch.isDo or:",isDoor)
                            }
                        }
                    } else {continue;}
                }
            }
            
            //print("fetch Fr om Json  fr om :",from)
            
            if from != "first"
            {
                self.btnGarageClicked = false
                self.btnDoorClicked = false
                self.nodoorLabel.isHidden = false
                
                //print("fetch Fr om Json  fr om != first isTwoCar || isOneCar:",isTwoCar,isOneCar)
                
                if isTwoCar || isOneCar
                {
                    self.btnGarageClicked = true
                    self.btnDoorClicked = false
                    self.btnPaintListClicked = false
                    self.btnGarageClicked(self.btnGarage as Any)
                } else if isDoor {
                    self.btnDoorClicked = true
                    self.btnGarageClicked = false
                    self.btnPaintListClicked = false
                    self.btnDoorsClicked(self.btnDoors as Any)
                }else
                {
                    //                  self.btnPaintListClicked = false
                    //                    self.setupButtonSelection()
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute:
                        {
                            self.paintScrollIndex = 0
                            //
                            self.btnPaintClicked(self.btnPaint as Any)
                    })
                    
                    
                }
                
            }
            self.setupButtonSelection()
        }
    }
    
        //--------------------------------------------------------------
        func checkCombination()-> Array<NSDictionary>? //*************** change 1.0.2 add optional ***
        {
//print("check Combination self.garageCatID:",self.garageCatID)
//print("check Combination self.doorCatID:",self.doorCatID)

            if self.garageCatID == ""
            {
                if (hasOnlyGarageDoors) //*************** change 1.0.2 add ***
                {
                    return nil
                }
                let dic:NSDictionary = ["catID":self.doorCatID,"imageID": doorimageID]
                return [dic]
            } else  if (self.doorCatID == "") { // || hasOnlyGarageDoors) {  //*************** change 1.1.0 change ***
                // for single garage to create favorite creating all combination of imageID and catID

                let imageIdArray:Array<String> = garimageID.components(separatedBy:"||")  //***************  change 1.1.0 change ***
//print("imageIdArray:",imageIdArray)

                if (imageIdArray.count > 1)
                {
                    var dicTemp = Array<NSDictionary>()

                    for imgid in imageIdArray
                    {
//print("imgid:",imgid)
                        let dic:NSDictionary = ["catID":self.garageCatID,"imageID": imgid]
                        dicTemp.append(dic)
                    }
                    return dicTemp
                }

                let dic:NSDictionary = ["catID":self.garageCatID,"imageID": garimageID] //***************  change 1.1.0 change ***

                return [dic]
            } else {
                let dic:NSDictionary = ["catID":self.doorCatID,"imageID": doorimageID]
                let imageIdArray:Array<String> = garimageID.components(separatedBy:"||")  //***************  change 1.1.0 change ***
//print("imageIdArray:",imageIdArray)
                if imageIdArray.count>1
                {
                    var dicTemp = Array<NSDictionary>()
                    dicTemp.append(dic)
                    for imgid in imageIdArray
                    {
//print("imgid:",imgid)

                        let dic1:NSDictionary = ["catID":self.garageCatID,"imageID": imgid]
                        dicTemp.append(dic1)
                    }
                    return dicTemp
                }
                let dic1:NSDictionary = ["catID":self.garageCatID,"imageID": garimageID]  //***************  change 1.1.0 change ***
//print("dic:",dic)
//print("dic1:",dic1)

                return [dic,dic1]
            }
        }
    
    //--------------------------------------------------------------
    func checkCombinationRemove()->Array<String>
    {
        if self.garageCatID == ""
        {
            return [self.doorCatID]
        } else  if self.doorCatID == "" {
            return [garageCatID]
        } else {
            return [ doorCatID,garageCatID]
        }
    }
    
    //-----------------------------
    func uploadFilesToCloud()
    {
        //print(" upload Files ToCloud self.old Image")
        if hasInternalApp
        {
            let agentId = DataManager.checkAgentFileUrlWithName(imageName:currentImageName)
            
            DataManager.uploadImageAndTextFile(imageUrl: currentImageName, agentID: agentId,image:originalHomeImage, txtUrl: currentImageName)  //********** change 0.0.40 change ***
            {(error,json) in
                
            }
            
        }
    }
    
    //-----------------------------
    func uploadEditedFilesToCloud()
    {
        if hasInternalApp
        {
            let nameTemp = currentImageName.replacingOccurrences(of:".jpg", with:"")
            let editedImageId = UserDefaults.standard.value(forKey:String(format:"%@_fullID",nameTemp)) ?? ""
            
            DataManager.uploadEditedROIS(fullSizeImageID:editedImageId as! String, txtUrl:currentImageName)
            {(error,json) in
                
            }
        }
    }
    
    //-----------------------------------------------------
    func loadGallery()
    {
        //print("load Gallery")
        do
        {
            let directory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            if let urlArray = try? FileManager.default.contentsOfDirectory(at: directory,
                                                                           includingPropertiesForKeys: [.creationDateKey],
                                                                           options:.skipsHiddenFiles).filter({ $0.pathExtension == "jpg" })
            {
                
                let tempArray =  urlArray.map
                { url in
                    (url, (try? url.resourceValues(forKeys: [.creationDateKey]))?.creationDate ?? Date.distantPast)
                }.sorted(by: { $0.1 > $1.1 }).map { $0.0 }
                
                self.imagesArray = tempArray.filter({$0.lastPathComponent != "modiOutImage.jpg"})
                //print("self.images Array:",self.images Array)
            } else {
                
            }
            self.photoCollectionView.reloadData()
        }
    }
    
    
    //-----------------------------------------------------
    func uploadnewImage()
    {
        
    }
    
    
    //----------------------------------------------
    @objc func onTimerFired()
    {
        self.timeLeft -= 1
        //print("\(timeLeft) seconds left")
        if self.timeLeft <= 0
        {
            self.timer?.invalidate()
            self.timer = nil
            let error = NSError(domain:"com.modifai", code:401, userInfo:[ NSLocalizedDescriptionKey: "Time Expired (20 seconds)"])
            processError(error: error, message: "Call took more than 20 seconds.")
        }
    }
    
    //----------------------------------------------
    @objc func onPaintTimerFired()
    {
        paintTimeLeft -= 1
        //print("onPaint TimerFired:  \(self.paintTim eLeft) seconds left")
        if paintTimeLeft <= 0
        {
            self.paintTimer?.invalidate()
            self.paintTimer = nil;
            noPaint = true;
        }
    }
    
    //----------------------------------------------
    func processError(error: NSError, message: String)
    {
        DispatchQueue.main.async(execute:
		{
			let errorMessage0 = "The processing of the image was unsuccessful,"
			let errorCodeString:String = " code:"+String(error.code)
			let errorMessage012 = errorMessage0+error.localizedDescription+errorCodeString;
			let message = NSLocalizedString(errorMessage012, comment: "there was an error from backend server")
			//Analytics.logEvent("Alert_The_processing_of_the_image_was_unsuccessful", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
			let alertController = UIAlertController(title: "Something went wrong processing this image", message: message, preferredStyle: .alert)

			alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "OK button"),
													style: .cancel,
													handler:
			{ _ in
					self.view.aj_hideDotLoadingIndicator()

			}))


			if let popoverController = alertController.popoverPresentationController  //********** change 3.0.3 add if block ***
			{
			  popoverController.sourceView = self.view
			  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
			  popoverController.permittedArrowDirections = []
			}

			self.present(alertController, animated: true, completion:
			{
			})
        })
    }
    
    //----------------------------------------
    func encodeThePhoto()
	{
//print("  ")
//print("  ")
//print("encode The Photo")
		if let ho = houseObject
		{
			guard let refImage = originalHomeImage else { return }
 //           guard let _ = originalHomeImage.cgImage else {return;}

			destImageWidth  = 640.0;
			destImageHeight = 480.0;

			if let refImageOrig = originalHomeImage
			{
				if (refImageOrig.size.height > refImageOrig.size.width)
				{
					let tempWidth = destImageWidth;
					destImageWidth  = destImageHeight;
					destImageHeight = tempWidth;
				}
			}

			destImageWidthPEC  = destImageWidth;
			destImageHeightPEC = destImageHeight;
			hostRectPEC = CGRect.init(x: 0, y: 0, width: refImage.size.width, height: refImage.size.height)
			wasForModifai = false;

//print(" ");
//print(" ");
//print(" originalHomeImage.size :",originalHomeImage.size);
//print(" destIma geWidth :",destImageWidth);
//print(" destIma geHeight :",destImageHeight);

			image_url = "none";

			let fullurl = ho.image_url[0];

			if (fullurl.count > 0)
			{
				let fullurlArr = fullurl.components(separatedBy: " ");
				if (fullurlArr.count > 0)
				{
					image_url = fullurlArr[0];
				}
			}

			var rois:[[Int]]
			var roisArch:[[Int]] = [[Int]]()

			var respKount:Int = 0;
			var respRemovedKount:Int = 0;
			var wasGoodDr = false;

			roisPersp = "";

			for resp in ho.response
			{
//print(" ");
//print(" ");
//print("resp :",resp);
				for respItem in resp
				{
//print("respItem.imageID_db :",respItem.imageID_db);
//print("respItem.roisinner.count :",respItem.roisinner.count);
//print("respItem.roisouter.count :",respItem.roisouter.count);
//print("respItem.labels :",respItem.labels);
//print("respItem.p1 :",respItem.p1);

					if (respItem.wasProcessed)
					{
						wasGoodDr = true;
					} else {

						if ((respItem.roisinner.count == 4) && (respItem.roisouter.count == 2))
						{
							rois = respItem.roisinner;
							roisArch = respItem.roisouter;
//print("1 roisArch :",roisArch);
							respItem.wasArch = true;
						} else if (respItem.rois.count == 6) {
							rois = respItem.rois;
//print("2 rois :",rois);
							respItem.wasPoly = true;
						} else if (respItem.rois.count == 4) {
							rois = respItem.rois;
//print("3 rois :",rois);
						} else {
							respRemovedKount = respRemovedKount+1;
//print("respRem ovedKount :",respRemovedKount);
//print("wasGoodDr",wasGoodDr)
							if (wasGoodDr && (respKount+respRemovedKount == ho.response.count))
							{
//print("ccc respK ount",respKount)
								roisPersp = roisPersp+"]"
//print("0 0 encodeph oto twice replac eObjectAt")
								self.replaceObjectAt(index: -1,type: "garage")
								self.replaceObjectAt(index: -1,type: "door")
							}

							continue;
						}

						respKount = respKount+1;
//print("respKo unt, ho.response.count :",respKount, ho.response.count);
//print("respItem, rois, roisArch :",respItem, rois, roisArch);

						if (respKount > 1)
						{
							roisPersp = roisPersp+", ";
						}

						_ = PECperspcCorrect(with:refImage, respItem:respItem, rois:rois, roisArch:roisArch, completionHandler:
							{
								if (respItem.wasGoodDr)
								{
									wasGoodDr = true;
								}

								if (respKount+respRemovedKount == ho.response.count)
								{
									roisPersp = roisPersp+"]"

//print("encodeph oto twice replac eObjectAt")
									self.replaceObjectAt(index: -1,type: "garage")
									self.replaceObjectAt(index: -1,type: "door")
								}
						})
					}
				}
			}

			if ( !wasGoodDr)
			{
				let error00 = NSError(domain:"com.modifai", code:505, userInfo:[ NSLocalizedDescriptionKey: "The door placement didn't include enough data from server   "])
				self.processError(error: error00, message: "Error processing response: couldn't parse house Object from json")
			}
		}
	}
    
    //--------------------------------------------------------
    func checkFavStatus(indexs:Int)
	{
        self.btnFavWasClicked = false
        self.btnFav.setImage(UIImage(named: "heart-v1"), for: .normal)

        guard let catArray = self.checkCombination() else {return}  //*************** change 1.0.2 add guard block ***

        self.btnFav.accessibilityValue = catArray.description
//print("  ")
//print("  ")
//print("  ")
//print("check Fav Status  catArray.description:",catArray.description)

        let filename = currentImageName.replacingOccurrences(of:".jpg", with: "")
        let folderName = filename+"_folder";

        let fileManager = FileManager.default
        let DocumentDirectory = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        if let filePath = DocumentDirectory.appendingPathComponent(folderName)
        {
//print("  ")
//print("filePath:",filePath)
            if (fileManager.fileExists(atPath: filePath.path))
            {
                var fileValues = String();

                for dict in catArray
                {
//print("dict:",dict)
                    if let catValue = dict.value(forKey: "catID") as? String
                    {
//print("  ")
//print("catValue:",catValue as Any)
                        let catArray = catValue.components(separatedBy: "-")
                        if let endCat = catArray.last
                        {
//print("  ")
//print("endCat:",endCat as Any)
                            fileValues = fileValues+"_cID_"+endCat
                        }
                    }

                    if let imgValue = dict.value(forKey: "imageID") as? String
                    {
//print("  ")
//print("imgValue:",imgValue as Any)
                        let imgArray = imgValue.components(separatedBy: "-")
                        if let endImg = imgArray.last
                        {
//print("  ")
//print("endImg:",endImg as Any)
                            fileValues = fileValues+"_iID_"+endImg
                        }
                    }
                }

                let folderName00 = fileValues+"_fd";
                let filePath00 = filePath.appendingPathComponent(folderName00)
//print("filePath00:",filePath00 as Any)

                if (fileManager.fileExists(atPath: filePath00.path))
                {
//print("did exist 2 ")



//print("fileValues:",fileValues as Any)
                    //                var filename0 = filename+fileValues;
                    //                filename0 = filename0+"_favorite.jpg";
//print("0 filename0:",filename0 as Any)
                    //
                    //                let fileURL = filePath.appendingPathComponent(filename0)

                    //                let fileManager = FileMa nager.default
                    //                let imagePath = (self.getDirectoryPath() as NSString).appendingPathComponent(filename)

                    //                if (fileManager.fileExists(atPath: fileURL.path))
                    //                {
//print("0 filename exists")
                    self.btnFavWasClicked = true
                    self.btnFav.setImage(UIImage(named: "heart-v2"), for: .normal)
                }
            }
        }


    }
    
    
    //-----------------------------------------
    func replaceDefaultFrontDoors()
    {
        //print("replace Default FrontDoors")
        btnFav.isEnabled = true
        //chevoronBtn.isHidden = true
        
        guard let s = self.selectedReferenceItem else { return }
        let garage:Door =  isDouble ?  doorsDoubleArray[doorScrollIndex-1] : doorsArray[doorScrollIndex-1]
        
        s.replacementGarageNames = [garage.image]
        doorCatID = garage.catalogID
        
        if let house0Object = houseObject
        {
            for resp in house0Object.response
            {
                for respItem in resp
                {
                    if (respItem.labels.count > 0)
                    {
                        let lbl = respItem.labels[0]
                        //print("replace Default FrontDoors lbl:",lbl)
                        if lbl.contains("door")
                        {
                            doorimageID = respItem.imageID_db
                        }
                    }
                }
            }
        }
        
        guard let rep = s.replacementGarageNames else { return }
        
        let currentPage = rep.count - 1
        //print("replaceDefau ltFrontDoors replac eObjectAt")
        self.replaceObjectAt(index: currentPage, type: "door")
    }
    
    
    //--------------------------------------
    func replaceDefaultGarageDoors()
    {
//print("replace Default GarageDoors")
        guard let s = self.selectedReferenceItem else { return }
        btnFav.isEnabled = true
//print("replaceDefaultGarageDoors garimageID:",garimageID as Any)
        garimageID = ""
        
        if isTwoCar
        {
            
            s.replacementGarageNames = [self.homeObj?.garages[garageScrollIndex-1].image] as? [String]
            garageCatID = (self.homeObj?.garages[garageScrollIndex-1].catalogID)!
            garageCatName = (self.homeObj?.garages[garageScrollIndex-1].title)!
            
            if let house0Object = houseObject
            {
                for resp in house0Object.response
                {
                    for respItem in resp
                    {
                        if (respItem.labels.count > 0)
                        {
                            let lbl = respItem.labels[0]
                            if lbl.contains("two-car")
                            {
                                let imgId = respItem.imageID_db
                                
                                if garimageID.count > 0
                                {
//print("2 replaceDefaultGarageDoors garimageID:",garimageID as Any)
                                    // for single garage to create favorite creating all combination of imageID and catID
                                    garimageID = garimageID+"||"+imgId
                                } else {
//print("3 replaceDefaultGarageDoors garimageID:",garimageID as Any)
                                    garimageID = imgId
                                }

                                if let gallID = currGallID  //******** change 1.1.0 add if Block ***
                                {
                                    // for single garage to create favorite creating all combination of imageID and catID
                                    garimageID = garimageID+"||"+gallID
                                }
                            }
                        }
                    }
                }
            }
            
        } else {
            s.replacementGarageNames = [self.homeObj?.singleGarage[garageScrollIndex-1].image] as? [String]
            garageCatID = (self.homeObj?.singleGarage[garageScrollIndex-1].catalogID)!
            garageCatName = (self.homeObj?.singleGarage[garageScrollIndex-1].title)!
            
            //            if s.hous eObject == nil
            //            {
            //                return
            //            }
            
//print("4 replaceDefaultGarageDoors garimageID:",garimageID as Any)
            garimageID = ""
            if let house0Object = houseObject
            {
                for resp in house0Object.response
                {
                    for respItem in resp
                    {
                        if (respItem.labels.count > 0)
                        {
                            let lbl = respItem.labels[0]
                            
                            if lbl.contains("one-car")
                            {
                                let imgId = respItem.imageID_db
                                
                                if garimageID.count > 0
                                {
                                    // for single garage to create favorite creating all combination of imageID and catID
//print("5 replaceDefaultGarageDoors garimageID:",garimageID as Any)
                                    garimageID = garimageID+"||"+imgId
                                } else {
//print("6 replaceDefaultGarageDoors garimageID:",garimageID as Any)
                                    garimageID = imgId
                                }

                                if let gallID = currGallID  //******** change 1.1.0 add if Block ***
                                {
                                    // for single garage to create favorite creating all combination of imageID and catID
                                    garimageID = garimageID+"||"+gallID
                                }
                            }
                        }
                    }
                }
            }
        }
        
        guard let rep = s.replacementGarageNames else { return }
        let currentPage = rep.count - 1
        
        
        //print(" ")
        //print("replaceDefaul tGarageDoors garimageID,rep:",garimageID,rep)
        
        //print("replace Default GarageDoors replac eObjectAt currentPage:",currentPage)
        self.replaceObjectAt(index: currentPage, type: "garage")
    }
    
    //*************** change 0.0.34 add function ***
    //------------------------------------------------
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ())
    {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    //*************** change 0.0.34 add function ***
    //------------------------------------------------
    func loadUpImage(url: URL)
    {
        //print("Download Started")
        self.getData(from: url)
        { data, response, error in
            guard let data0 = data, error == nil else { return }
            //print("Download Finished")
            garageSingleMaskImage = UIImage(data: data0)
            
            //            DispatchQueue.main.async()
            //            {
            //print("singleGarage self?.garageSingl eMaskImage:",self.garageSingle MaskImage as Any)
            //    //            self.replaceAfterLoad(imageviewTemp: imageviewTemp,imageviewTempSingle:imageviewTempSingle,type:type)
            //            }
        }
    }
    
    //------------------------------------------------
    func replaceObjectAt(index: Int,type:String)
    {
//print(" ")
//print(" ")
//print(" ")
//print("replac eObjectAt index,type:",index,type)
//print("replac eObjectAt garageMa skImage:",garageMa skImage as Any)

        if index == -1
        {
            if (type == "door")
            {
                //print(" ")
                //print("replac eObjectAt door cleared")
                self.oneDoorImgae = nil
                self.twoDoorImgae = nil
            } else {
                //print(" ")
                //print("replac eObjectAt garage cleared")
                self.oneCarImgae = nil
                self.twoCarImgae = nil
            }
            
            let imageviewTemp = UIImageView()
            imageviewTemp.backgroundColor = .white
            imageviewTemp.image = UIImage(imageLiteralResourceName:"transparent")
            
            //print("replac eObjectAt index replace After Load  ?")
            self.replaceAfterLoad(imageviewTemp: imageviewTemp,imageviewTempSingle:imageviewTemp,type:type)
            
            if doorScrollIndex > 0
            {
                self.replaceDefaultFrontDoors()
            }
            if garageScrollIndex > 0
            {
                self.replaceDefaultGarageDoors()
            }
            return
        }
        
        guard let sri = self.selectedReferenceItem else { return }
        guard let rep = sri.replacementGarageNames else { return }
        var repItem:String = "transparent"
        let imageviewTemp = UIImageView()
        var imageviewTempSingle = UIImageView()
        
        //print(" ")
        //print("replac eObjectAt 1 rep:",rep)
        
        if index >= 0
        {
            repItem = rep[0]
            LoadingIndicatorView.show(" ")
            
            if isOneCar && isTwoCar && type != "door"
            {
                imageviewTemp.sd_setImage(with: URL(string: repItem))
                { (image, error, cache, urls) in
                    
                    if (error != nil)
                    {
                    } else {
                        imageviewTemp.image = image
                        imageviewTempSingle.image = image
                        self.twoCarImgae = image
                        //print("  ")
                        //print("replac eObjectAt 2 self.twoCa rImgae:",self.twoCa rImgae as Any)
                    }
                    var singledoorAvail:Bool = false
                    
                    for sinGar in self.homeObj!.singleGarage
                    {
                        if self.garageCatName == sinGar.title
                        {
                            self.cur1Garage = sinGar
                            repItem = sinGar.image
                            singledoorAvail = true
                            LoadingIndicatorView.show(" ")

                            imageviewTempSingle.sd_setImage(with: URL(string: repItem))
                            { (image, error, cache, urls) in
                                if (error != nil)
                                {
                                    imageviewTempSingle.image = imageviewTemp.image
                                    self.replaceAfterLoad(imageviewTemp: imageviewTemp,imageviewTempSingle:imageviewTempSingle,type:type)
                                } else {
                                    imageviewTempSingle.image = image
                                    self.oneCarImgae = image
                                    LoadingIndicatorView.hide()
                                    //print("replac eObjectAt 1 self.oneCarI mgae:",self.oneCarIm gae as Any)
                                    //print("replac eObjectAt 1 sinGar.mask_url:",sinGar.mask_url as Any)

//print("replac eObjectAt 2 replace After Load  ?  self.garageMa skImage going to single")
                                    garageSingleMaskImage = garageMaskImage //*************** change 0.0.34 add  ***
                                    
                                    //*************** change 0.0.34 add if block ***
                                    if let imageItem = sinGar.mask_url
                                    {
                                        if (imageItem.count > 2)
                                        {
                                            if let aUrl = URL(string: imageItem)
                                            {
                                                let session = URLSession.shared
                                                let task = session.dataTask(with: aUrl, completionHandler:
                                                { data, response, error in
                                                    //print("singleGarage Download Finished")
                                                    if let data0 = data, error == nil
                                                    {
                                                        if let source =  CGImageSourceCreateWithData(data0 as CFData, nil)
                                                        {
                                                            if let imageccg = CGImageSourceCreateImageAtIndex(source, 0, nil)
                                                            {
                                                                garageSingleMaskImage = UIImage(cgImage: imageccg)
                                                            }
                                                        }
                                                        
                                                        //    self.garageSin gleMaskImage = UIImage(data: data0)
                                                        //print("singleGarage self?.garageSi ngleMaskImage:",self.garageSing leMaskImage as Any)
                                                    }
                                                    
                                                    DispatchQueue.main.async()
                                                        {
                                                            self.replaceAfterLoad(imageviewTemp: imageviewTemp,imageviewTempSingle:imageviewTempSingle,type:type)
                                                    }
                                                })
                                                task.resume()
                                                
                                            } else {
                                                self.replaceAfterLoad(imageviewTemp: imageviewTemp,imageviewTempSingle:imageviewTempSingle,type:type)
                                            }
                                        } else {
                                            self.replaceAfterLoad(imageviewTemp: imageviewTemp,imageviewTempSingle:imageviewTempSingle,type:type)
                                        }
                                    } else {
                                        self.replaceAfterLoad(imageviewTemp: imageviewTemp,imageviewTempSingle:imageviewTempSingle,type:type)
                                    }
                                }
                            }
                            
                            break
                        }
                    }
                    
                    if !singledoorAvail
                    {
                        imageviewTempSingle = imageviewTemp
                        self.oneCarImgae = image
                        LoadingIndicatorView.hide()
//print("replac eObjectAt 3 replace After Load  ?  self.garageMa skImage going to single")
                        garageSingleMaskImage = garageMaskImage     //*************** change 0.0.34 add ***
                        
                        self.replaceAfterLoad(imageviewTemp: imageviewTemp,imageviewTempSingle:imageviewTempSingle,type:type)
                    }
                }
            } else {
                //print("0 replac eObjectAt isDouble:",isDouble)
                
                imageviewTemp.sd_setImage(with: URL(string: repItem))
                { (image, error, cache, urls) in
                    
                    if (error != nil)
                    {
                    } else {
                        imageviewTemp.image = image
                        
                        //print("0 replac eObjectAt type:",type)
                        if type == "garage"
                        {
                            if isOneCar
                            {
                                self.oneCarImgae = image
                            }else
                            {
                                self.twoCarImgae = image
                            }
                        } else {
                            
                            //print("0 replac eObjectAt isDouble:",self.isDouble)
                            if isDouble
                            {
                                self.twoDoorImgae = image
                            } else {
                                self.oneDoorImgae = image
                                imageviewTempSingle.image = image;
                            }
                        }
                        
                        LoadingIndicatorView.hide()
                        //print("replac eObjectAt 4 replace After Load  ?")
                        self.replaceAfterLoad(imageviewTemp: imageviewTemp,imageviewTempSingle:imageviewTempSingle,type:type)
                    }
                }
            }
        } else {
            imageviewTemp.image = UIImage(named: repItem)
            
            //print("0 replac eObjectAt imageviewTemp.image:",imageviewTemp.image as Any)
            
            //            self.oneDo orImgae = UIImage(named: repItem)
            //            self.twoDo orImgae = UIImage(named: repItem)
            //print("0 replac eObjectAt isDouble:",self.isDouble)
            if isDouble
            {
                self.twoDoorImgae = UIImage(named: repItem)
            } else {
                self.oneDoorImgae = UIImage(named: repItem)
                imageviewTempSingle.image = self.oneDoorImgae;
            }
            
            LoadingIndicatorView.hide()
            //print("replac eObjectAt 5 replace After Load  ?")
            self.replaceAfterLoad(imageviewTemp: imageviewTemp,imageviewTempSingle:imageviewTempSingle,type:type)
        }
    }
    
    //-----------------------------------------------
    func replaceAfterLoad(imageviewTemp:UIImageView,imageviewTempSingle:UIImageView,type:String)
    {
//print("  ")
//print("  ")
//print("0 replace After Load  ? type:",type)
//print("0 replace After Load  ? garageMa skImage:",garageMaskImage as Any)
        guard let repImage = imageviewTemp.image  else { return }
        var repMask:UIImage? = nil;
        
        if (type.lowercased().contains("garage"))
        {
            repMask = garageMaskImage;
        } else {
            repMask = frontDrMaskImage;
        }
        
        //------------ house object loop -----------
        if let ho = houseObject
        {
            guard let refImage = originalHomeImage else { return }
            guard let _ = originalHomeImage.cgImage else {return;}
            
            var newReferenceImage = UIImage();
            var newMaskImage = UIImage();
            
            if let image = self.garageImageView.image
            {
                newReferenceImage = image;
            }
            
            if (type.lowercased().contains("garage"))
            {
//print("0 replace After Load  self.garageMa skView.image:",self.garageMas kView.image as Any)
                if let image = self.garageMaskView.image
                {
                    newMaskImage = image;
                }
            } else {
                if let image = self.frontDrMaskView.image
                {
                    newMaskImage = image;
                }
            }
            
            let hostRect = CGRect.init(x: 0, y: 0, width: refImage.size.width, height: refImage.size.height)
            
            //------------ each response loop -----------
            for resp in ho.response
            {
                var replacement: UIImage  = repImage
                var replaceMask: UIImage? = repMask
                
                //------------ each door loop -----------
                for respItem in resp
                {
//print("replaceAfterLoad respItem.v1:",respItem.v1)
                    //print("replaceAfterLoad respItem.roisouter:",respItem.roisouter)
                    //print("replaceAfterLoad respItem.imageID_db:",respItem.imageID_db)
                    
                    if (respItem.labels.count > 0)
                    {
                        let lbl = respItem.labels[0]
//print("0 respItem in lbl:",lbl)
                        
                        if lbl.lowercased().contains(type.lowercased())
                        {
                            if lbl.lowercased().contains("one-car") && imageviewTempSingle.image != nil && isTwoCar
                            {
                                replacement = imageviewTempSingle.image!
                                replaceMask = garageSingleMaskImage;
//print("0 replace After Load repla ceMask = garageSingleMa skImage")
                            }

//print("0 respItem in respItem.wasGoodDr:",respItem.wasGoodDr)
                            //print("0 respItem in resp respItem.path:",respItem.path)
                            if ( !respItem.wasGoodDr) { continue; }
//print("0 replace After Load replaceMask:",replaceMask as Any)

                            if (respItem.path.cgPath.isEmpty)
                            {
                                let pp1:Ppoint! = respItem.p1
                                let pp2:Ppoint! = respItem.p2
                                let pp3:Ppoint! = respItem.p3
                                let pp4:Ppoint! = respItem.p4
                                let pp5:Ppoint! = respItem.p5
                                let pp6:Ppoint! = respItem.p6
                                
                                pp1.lt = pp6;
                                pp1.nx = pp2;
                                
                                pp2.lt = pp1;
                                pp2.nx = pp3;
                                
                                pp3.lt = pp2;
                                pp3.nx = pp4;
                                
                                pp4.lt = pp3;
                                pp4.nx = pp5;
                                
                                pp5.lt = pp4;
                                pp5.nx = pp6;
                                
                                pp6.lt = pp5;
                                pp6.nx = pp1;
                                
//print(" pp1.pt:",pp1.pt)
//print(" pp2.pt:",pp2.pt)
//print(" pp3.pt:",pp3.pt)
//print(" pp4.pt:",pp4.pt)
//print(" pp5.pt:",pp5.pt)
//print(" pp6.pt:",pp6.pt)
                                if (respItem.wasPoly)
                                {
                                    respItem.v1 = CIVector(x: pp1.pt.x, y: refImage.size.height-pp2.pt.y)
                                    respItem.v2 = CIVector(x: pp4.pt.x, y: refImage.size.height-pp3.pt.y)
                                    respItem.v3 = CIVector(x: pp5.pt.x, y: refImage.size.height-pp5.pt.y)
                                    respItem.v4 = CIVector(x: pp6.pt.x, y: refImage.size.height-pp6.pt.y)
                                    
                                    respItem.path.removeAllPoints()
                                    respItem.path.move(to:pp1.pt)
                                    respItem.path.addLine(to:pp2.pt)
                                    respItem.path.addLine(to:pp3.pt)
                                    respItem.path.addLine(to:pp4.pt)
                                    respItem.path.addLine(to:pp5.pt)
                                    respItem.path.addLine(to:pp6.pt)
                                    respItem.path.close()
                                    respItem.rect = respItem.path.cgPath.boundingBox
                                    
                                } else if (respItem.wasArch) {
                                    
                                    pp1.lt = pp5;
                                    pp5.nx = pp1;
                                    
                                    let ratiox =  (pp2.pt.x - pp1.pt.x) / (pp3.pt.x - pp1.pt.x)
                                    let posy =  ((pp3.pt.y - pp1.pt.y) * ratiox) + pp1.pt.y
                                    let dify =  min(pp2.pt.y - posy,0.0)
                                    
                                    respItem.v1 = CIVector(x: pp1.pt.x, y: refImage.size.height-(pp1.pt.y + dify))
                                    respItem.v2 = CIVector(x: pp3.pt.x, y: refImage.size.height-(pp3.pt.y + dify))
                                    respItem.v3 = CIVector(x: pp4.pt.x, y: refImage.size.height-pp4.pt.y)
                                    respItem.v4 = CIVector(x: pp5.pt.x, y: refImage.size.height-pp5.pt.y)
                                    respItem.path.removeAllPoints()
                                    
                                    var theArc:Arctype = Arctype()
                                    theArc.bpoint = Apoint(Float(pp1.pt.x), Float(pp1.pt.y))
                                    theArc.mpoint = Apoint(Float(pp2.pt.x), Float(pp2.pt.y))
                                    theArc.epoint = Apoint(Float(pp3.pt.x), Float(pp3.pt.y))
                                    respItem.path = PECperspcCorrect.drawArc(theArc)
                                    respItem.path.addLine(to:pp3.pt)
                                    respItem.path.addLine(to:pp4.pt)
                                    respItem.path.addLine(to:pp5.pt)
                                    respItem.path.close()
                                    
                                    //        polyPath.append(respItem.path)
                                    
                                    let topPath2 = UIBezierPath.init()
                                    topPath2.move(to: CGPoint.init(x:CGFloat(respItem.v1.x), y: CGFloat(pp1.pt.y + dify)))
                                    topPath2.addLine(to: CGPoint.init(x:CGFloat(respItem.v2.x), y: CGFloat(pp3.pt.y + dify)))
                                    topPath2.addLine(to: CGPoint.init(x:CGFloat(respItem.v3.x), y: CGFloat(pp4.pt.y)))
                                    topPath2.addLine(to: CGPoint.init(x:CGFloat(respItem.v4.x), y: CGFloat(pp5.pt.y)))
                                    topPath2.close()
                                    topPath2.append(respItem.path)
                                    
                                    respItem.rect = topPath2.cgPath.boundingBox
                                    
                                } else {
                                    
                                    pp1.lt = pp4;
                                    pp4.nx = pp1;
                                    
                                    respItem.v1 = CIVector(x: pp1.pt.x, y: refImage.size.height-pp1.pt.y)
                                    respItem.v2 = CIVector(x: pp2.pt.x, y: refImage.size.height-pp2.pt.y)
                                    respItem.v3 = CIVector(x: pp3.pt.x, y: refImage.size.height-pp3.pt.y)
                                    respItem.v4 = CIVector(x: pp4.pt.x, y: refImage.size.height-pp4.pt.y)
                                    
                                    respItem.path.removeAllPoints()
                                    respItem.path.move(to:pp1.pt)
                                    respItem.path.addLine(to:pp2.pt)
                                    respItem.path.addLine(to:pp3.pt)
                                    respItem.path.addLine(to:pp4.pt)
                                    respItem.path.close()
                                    
                                    respItem.rect = respItem.path.cgPath.boundingBox
                                }
                                
                                polyLayer.path = nil
                                polyLayer.path = respItem.path.cgPath
                                polyLayer.opacity = 1.0
                            }
                            
                            
                            let context:CIContext = CIContext(options: nil);
                            
                            //warping of garage/front Door image using CIFilter,  not warping the host image/  makes sense
                            //in context of three garages different angles.
                            if let perpFilter = CIFilter(name: "CIPerspectiveTransform")
                            {
                                let beginImage = CIImage(image: replacement)
                                perpFilter.setValue(beginImage, forKey:kCIInputImageKey)
                                
                                perpFilter.setValue(respItem.v1, forKey:"inputTopLeft");
                                perpFilter.setValue(respItem.v2, forKey:"inputTopRight");
                                perpFilter.setValue(respItem.v3, forKey:"inputBottomRight");
                                perpFilter.setValue(respItem.v4, forKey:"inputBottomLeft");

//print("1 replace After Load perpFilter.outputImage:",perpFilter.outputImage as Any)
                                if let output = perpFilter.outputImage
                                {
                                    if let cgimg = context.createCGImage(output, from: output.extent)
                                    {
                                        let processedImage = UIImage(cgImage: cgimg)
                                        var processedMask:UIImage? = nil
//print("1 replace After Load repla ceMask:",repla ceMask as Any)

                                        //---------- door mask ----------------------
                                        if let beginreplaceMask = replaceMask
                                        {
                                            let beginMaskImage = CIImage(image: beginreplaceMask)
                                            perpFilter.setValue(beginMaskImage, forKey:kCIInputImageKey)
                                            
                                            if let outputMask = perpFilter.outputImage
                                            {
                                                if let cgimgMask = context.createCGImage(outputMask, from: outputMask.extent)
                                                {
                                                    processedMask = UIImage(cgImage: cgimgMask)
//print("0 replace After Load proces sedMask:",proces sedMask as Any)
                                                }
                                            }
                                        }
                                        
                                        
                                        if (respItem.wasPoly)
                                        {
                                            //print("0 respItem.was Poly respItem.path:",respItem.path)
                                            
                                            UIGraphicsBeginImageContext(CGSize.init(width: refImage.size.width, height: refImage.size.height))
                                            guard let context = UIGraphicsGetCurrentContext() else { return }
                                            newReferenceImage.draw(in: hostRect)
                                            
                                            context.addPath(respItem.path.cgPath)
                                            context.clip(using: .evenOdd)
                                            
                                            processedImage.draw(in: respItem.rect, blendMode: .normal, alpha: 1.0)
                                            
                                            newReferenceImage = UIGraphicsGetImageFromCurrentImageContext()!
                                            UIGraphicsEndImageContext()
                                            
                                            if let processedMaskOut = processedMask
                                            {
                                                UIGraphicsBeginImageContext(CGSize.init(width: refImage.size.width, height: refImage.size.height))
                                                guard let maskContext = UIGraphicsGetCurrentContext() else { return }
                                                newMaskImage.draw(in: hostRect)
                                                
                                                maskContext.addPath(respItem.path.cgPath)
                                                maskContext.clip(using: .evenOdd)
                                                
                                                processedMaskOut.draw(in: respItem.rect, blendMode: .normal, alpha: 1.0)
                                                
                                                newMaskImage = UIGraphicsGetImageFromCurrentImageContext()!
                                                UIGraphicsEndImageContext()
                                            }
                                            
                                        } else if (respItem.wasArch) {
                                            //print("0 respItem.wasArch respItem.path:",respItem.path)
                                            
                                            UIGraphicsBeginImageContext(CGSize.init(width: refImage.size.width, height: refImage.size.height))
                                            guard let context = UIGraphicsGetCurrentContext() else { return }
                                            newReferenceImage.draw(in: hostRect)
                                            
                                            //print("00 respItem.wasArch respItem.path.cgPath.isEmpty:",respItem.path.cgPath.isEmpty)
                                            
                                            context.addPath(respItem.path.cgPath)
                                            context.clip(using: .evenOdd)
                                            
                                            processedImage.draw(in: respItem.rect, blendMode: .normal, alpha: 1.0)
                                            
                                            newReferenceImage = UIGraphicsGetImageFromCurrentImageContext()!
                                            UIGraphicsEndImageContext()
                                            
                                            
                                            if let processedMaskOut = processedMask
                                            {
                                                UIGraphicsBeginImageContext(CGSize.init(width: refImage.size.width, height: refImage.size.height))
                                                guard let maskContext = UIGraphicsGetCurrentContext() else { return }
                                                newMaskImage.draw(in: hostRect)
                                                
                                                maskContext.addPath(respItem.path.cgPath)
                                                maskContext.clip(using: .evenOdd)
                                                
                                                processedMaskOut.draw(in: respItem.rect, blendMode: .normal, alpha: 1.0)
                                                
                                                newMaskImage = UIGraphicsGetImageFromCurrentImageContext()!
                                                UIGraphicsEndImageContext()
                                            }
                                            
                                        } else {
                                            
                                            UIGraphicsBeginImageContext(CGSize.init(width: refImage.size.width, height: refImage.size.height))
                                            newReferenceImage.draw(in: hostRect)
                                            processedImage.draw(in: respItem.rect, blendMode: .normal, alpha: 1.0)
                                            newReferenceImage = UIGraphicsGetImageFromCurrentImageContext()!
                                            UIGraphicsEndImageContext()

//print("1 replace After Load processedMask:",processedMask as Any)
//print("1 replace After Load refImage.size:",refImage.size as Any)
//print("1 replace After Load hostRect:",hostRect as Any)
//print("1 replace After Load respItem.rect:",respItem.rect as Any)
                                            if let processedMaskOut = processedMask
                                            {
                                                UIGraphicsBeginImageContext(CGSize.init(width: refImage.size.width, height: refImage.size.height))
                                                newMaskImage.draw(in: hostRect)
                                                processedMaskOut.draw(in: respItem.rect, blendMode: .normal, alpha: 1.0)
                                                newMaskImage = UIGraphicsGetImageFromCurrentImageContext()!
                                                UIGraphicsEndImageContext()
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            DispatchQueue.main.async
            {
                    self.garageImageView.image = newReferenceImage;
                    
                    if (type.lowercased().contains("garage"))
                    {
                        maskedDoorImage = newMaskImage;
//print("0 replace After Load  maskedDo orImage:",maskedDoorImage as Any)
                    } else {
                        maskedFrontDrImage = newMaskImage;
//print("0 replace After Load  maskedFro ntDrImage is front door")
                    }
                    //print("0 replace after load maskedDo orImage:",maskedDo orImage as Any)
            }
        }
    }
    
    //---------------------------------------------------
    func getDocumentsDirectory() -> URL
    {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    //---------------------------------------------------
    func saveColorToDocumentDirectory(color0: UIColor?, type:String )
    {
        if let color = color0
        {
            var filename = currentImageName;
            
            do
            {
                filename = currentImageName.replacingOccurrences(of:".jpg", with: type)
                //print("saveColor ToDocument Directory filename:",filename)
                if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                {
                    let fileURL = dir.appendingPathComponent(filename)
                    //print("saveColor ToDocument Directory fileURL:",fileURL)
                    let data = try NSKeyedArchiver.archivedData(withRootObject: color, requiringSecureCoding: false)
                    try data.write(to: fileURL)
                }
            } catch {
                //print("Couldn't write UICo lor")
            }
        }
    }
    
    //---------------------------------------------------
    func saveHouseToDocumentDirectory(house0: HouseObject?)
    {
        if hasInternalApp
        {
            
            if let house = house0
            {
                var filename = currentImageName
                //print("0 saveHouseTo Document Directory filename:",filename)
                
                do
                {
                    filename = currentImageName.replacingOccurrences(of:".jpg", with: "_houseObject.txt")
                    //print("saveHouseTo DocumentDirectory filename:",filename)
                    if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                    {
                        let fileURL = dir.appendingPathComponent(filename)
                        let encoder = JSONEncoder()
                        let houseData = try encoder.encode(house)
                        //                            let str = String(decoding: houseData, as: UTF8.self)
                        //
                        // //print("saveHouseTo DocumentDirectory fileURL:",fileURL)
                        //                            let data = try NSKeyedArchiver.archivedData(withRootObject: house, requiringSecureCoding: false)
                        try houseData.write(to: fileURL)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now()) {
                            // your code here
                            self.uploadEditedFilesToCloud()
                        }
                        
                        
                    }
                } catch {
                    //print("saveHouseTo DocumentDirectory Couldn't write house")
                }
            }
        }
        
    }
    
    
    //-----------------------------------------------
    func removeFavoriteFromDocumentDirectory(catArray:Array<NSDictionary>, filename:String )
    {
        //print("  ")
        //print("  ")
        //print("  ")
        //print("remove From DocumentDirectory")
        
        let folderName = filename+"_folder";
        
        let fileManager = FileManager.default
        let DocumentDirectory = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        if let filePath = DocumentDirectory.appendingPathComponent(folderName)
        {
            if (fileManager.fileExists(atPath: filePath.path))
            {
                //print("did  exist")
                var fileValues = String();
                
                for dict in catArray
                {
                    //print("dict:",dict)
                    if let catValue = dict.value(forKey: "catID") as? String
                    {
                        //print("  ")
                        //print("catValue:",catValue as Any)
                        let catArray = catValue.components(separatedBy: "-")
                        if let endCat = catArray.last
                        {
                            //print("  ")
                            //print("endCat:",endCat as Any)
                            fileValues = fileValues+"_cID_"+endCat
                        }
                    }
                    
                    if let imgValue = dict.value(forKey: "imageID") as? String
                    {
                        //print("  ")
                        //print("imgValue:",imgValue as Any)
                        let imgArray = imgValue.components(separatedBy: "-")
                        if let endImg = imgArray.last
                        {
                            //print("  ")
                            //print("endImg:",endImg as Any)
                            fileValues = fileValues+"_iID_"+endImg
                        }
                    }
                }
                
                let folderName00 = fileValues+"_fd";
                let filePath00 = filePath.appendingPathComponent(folderName00)
                //print("filePath00:",filePath00 as Any)
                
                if (fileManager.fileExists(atPath: filePath00.path))
                {
                    //print("did exist 2 ")
                    
                    
                    
                    //print("fileValues:",fileValues as Any)
                    //                var filename0 = filename+fileValues;
                    //                filename0 = filename0+"_favorite.jpg";
                    //print("0 filename0:",filename0 as Any)
                    //
                    //                let fileURL = filePath.appendingPathComponent(filename0)
                    //print("0 fileURL:",fileURL as Any)
                    
                    do
                    {
                        try fileManager.removeItem(atPath: filePath00.path)
                    } catch {
                        print("Something wrong.")
                    }
                }
            }
        }
    }
    
    //-----------------------------------------------
    func saveFavoriteToDocumentDirectory(catArray:Array<NSDictionary>, image0: UIImage?, filename:String )
    {
        //print("  ")
        //print("  ")
        //print("  ")
        //print("save Favorite To DocumentDirectory")
        if let image = image0
        {
            if let data = image.jpegData(compressionQuality:1.0)
            {
                let folderName = filename+"_folder";
                
                let fileManager = FileManager.default
                let DocumentDirectory = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
                
                if let filePath = DocumentDirectory.appendingPathComponent(folderName)
                {
                    if !fileManager.fileExists(atPath: filePath.path)
                    {
                        //print("didn't exist")
                        do
                        {
                            try fileManager.createDirectory(atPath: filePath.path, withIntermediateDirectories: true, attributes: nil)
                        } catch let error as NSError {
                            print("Unable to create directory \(error.debugDescription)")
                        }
                    }
                    //print("filePath:",filePath as Any)
                    
                    var fileValues = String();
                    //print("  ")
                    //print("  ")
                    
                    for dict in catArray
                    {
                        //print("dict:",dict)
                        if let catValue = dict.value(forKey: "catID") as? String
                        {
                            //print("  ")
                            //print("catValue:",catValue as Any)
                            let catArray = catValue.components(separatedBy: "-")
                            if let endCat = catArray.last
                            {
                                //print("  ")
                                //print("endCat:",endCat as Any)
                                
                                fileValues = fileValues+"_cID_"+endCat
                            }
                        }
                        
                        if let imgValue = dict.value(forKey: "imageID") as? String
                        {
                            //print("  ")
                            //print("imgValue:",imgValue as Any)
                            let imgArray = imgValue.components(separatedBy: "-")
                            if let endImg = imgArray.last
                            {
                                //print("  ")
                                //print("endImg:",endImg as Any)
                                fileValues = fileValues+"_iID_"+endImg
                            }
                        }
                    }
                    
                    //print("fileValues:",fileValues as Any)
                    
                    let folderName00 = fileValues+"_fd";
                    let filePath00 = filePath.appendingPathComponent(folderName00)
                    
                    if !fileManager.fileExists(atPath: filePath00.path)
                    {
                        //print("didn't exist 2 ")
                        do
                        {
                            try fileManager.createDirectory(atPath: filePath00.path, withIntermediateDirectories: true, attributes: nil)
                        } catch let error as NSError {
                            print("catId folder Unable to create directory \(error.debugDescription)")
                        }
                    }
                    //print("filePath:",filePath as Any)
                    
                    
                    let filename0 = filename+"_favorite.jpg";
                    //print("10 filename0:",filename0 as Any)
                    
                    let fileURL = filePath00.appendingPathComponent(filename0)
                    //print("10 fileURL:",fileURL as Any)
                    do
                    {
                        try data.write(to: fileURL)
                    } catch let error as NSError {
                        print("favorite file Unable to write \(error.debugDescription)")
                    }
                    var filenamesArray:[URL] = [URL]()
                    filenamesArray.append(fileURL)
                    if (isTwoCar) //********** change 1.0.1 add if block ***
                    {
                        if let image = self.twoCarImgae
                        {
                            if let data2c = image.jpegData(compressionQuality:1.0)
                            {
                                let filename2c = filename+"_2car.jpg";
                                //print("10 filename2c:",filename2c as Any)

                                let fileURL2c = filePath00.appendingPathComponent(filename2c)
                                try? data2c.write(to: fileURL2c)

                                let filenamedetailsname = filename+"_2cardetails.txt";
                                let fileURLdeails = filePath00.appendingPathComponent(filenamedetailsname)

                                //*************zipcode Changes****************  //******** change 0.1.0 ***
                                let dic = ["name":self.cur2Garage.title,"desc":self.cur2Garage.desc,"price":self.cur2Garage.price,"catID":self.cur2Garage.catalogID,"checkout":false,"fileURL":"\(folderName)/\(folderName00)__\(filenamedetailsname)","fileName":filename,"mID": self.cur2Garage.manufacturerID] as [String : Any]
                                do {
                                    let jsonData: Data? = try? JSONSerialization.data(withJSONObject: dic)
                                    let theJSONText = String(data: jsonData!,encoding: .utf8)
                                    try theJSONText?.write(to: fileURLdeails, atomically: true, encoding:.utf8)
                                    DataManager.UpdateFavoriteScenes(dataToConve:jsonData!,filename:"\(folderName)/\(folderName00)__\(filenamedetailsname)" , typeName: "garagerName")

                                } catch {

                                }
                                filenamesArray.append(fileURL2c)
                                filenamesArray.append(fileURLdeails)

                            }
                        }
                    }
                    if (isOneCar) //********** change 1.0.1 add if block ***
                    {
                        if let image = self.oneCarImgae
                        {
                            if (self.oneCarImgae != self.twoCarImgae)
                            {
                                if let data1c = image.jpegData(compressionQuality:1.0)
                                {
                                    let filename1c = filename+"_1car.jpg";
                                    //print("10 filename1c:",filename1c as Any)
                                    let fileURL1c = filePath00.appendingPathComponent(filename1c)
                                    try? data1c.write(to: fileURL1c)


                                    let filenamedetailsname = filename+"_1cardetails.txt";
                                    let fileURLdeails = filePath00.appendingPathComponent(filenamedetailsname)

                                    //*************zipcode Changes****************  //******** change 0.1.0 ***
                                    let dic = ["name":self.cur1Garage.title,"desc":self.cur1Garage.desc,"price":self.cur1Garage.price,"catID":self.cur1Garage.catalogID,"checkout":false,"fileURL":"\(folderName)/\(folderName00)__\(filenamedetailsname)","fileName":filename,"mID": self.cur1Garage.manufacturerID] as [String : Any]
                                    do {
                                        let jsonData: Data? = try? JSONSerialization.data(withJSONObject: dic)
                                        try jsonData!.write(to: fileURLdeails)
                                        DataManager.UpdateFavoriteScenes(dataToConve:jsonData!,filename:"\(folderName)/\(folderName00)__\(filenamedetailsname)" , typeName: "garagerName")

                                    } catch {

                                    }

                                    filenamesArray.append(fileURLdeails)
                                    filenamesArray.append(fileURL1c)
                                }
                            }
                        }
                    }
                    
                    //print("10 self.twoDo orImgae:",self.twoDoorImgae as Any)
                    //print("10 self.oneDo orImgae:",self.oneDoorImgae as Any)
                    //print("10 self.cur2Door.title:",self.cur2Door.title as Any)
                    //print("10 self.cur1Door.title:",self.cur1Door.title as Any)
                    
                    if (isDouble) //********** change 1.0.1 add if block ***
                    {
                        if let image = self.twoDoorImgae
                        {
                            if let data2d = image.jpegData(compressionQuality:1.0)
                            {
                                let filename2d = filename+"_2door.jpg";
                                //print("10 filename2d:",filename2d as Any)
                                let fileURL2d = filePath00.appendingPathComponent(filename2d)
                                try? data2d.write(to: fileURL2d)

                                let filenamedetailsname = filename+"_2doordetails.txt";
                                let fileURLdeails = filePath00.appendingPathComponent(filenamedetailsname)

                                //*************zipcode Changes****************  //******** change 0.1.0 ***
                                let dic = ["name":self.cur2Door.title,"desc":self.cur2Door.desc,"price":self.cur2Door.price,"catID":self.cur2Door.catalogID,"checkout":false,"fileURL":"\(folderName)/\(folderName00)__\(filenamedetailsname)","fileName":filename,"mID": self.cur2Door.manufacturerID] as [String : Any]
                                do {
                                    let jsonData: Data? = try? JSONSerialization.data(withJSONObject: dic)

                                    try jsonData!.write(to: fileURLdeails)
                                    DataManager.UpdateFavoriteScenes(dataToConve:jsonData!,filename:"\(folderName)/\(folderName00)__\(filenamedetailsname)" , typeName: "doorName")

                                } catch {

                                }
                                filenamesArray.append(fileURLdeails)

                                filenamesArray.append(fileURL2d)

                            }
                        }
                    }
                    
                    if (isDoor) //********** change 1.0.1 add if block ***
                    {
                        if let image = self.oneDoorImgae
                        {
                            if (self.oneDoorImgae != self.twoDoorImgae)
                            {
                                if let data1d = image.jpegData(compressionQuality:1.0)
                                {
                                    let filename1d = filename+"_1door.jpg";
                                    //print("10 filename1d:",filename1d as Any)
                                    let fileURL1d = filePath00.appendingPathComponent(filename1d)
                                    try? data1d.write(to: fileURL1d)

                                    let filenamedetailsname = filename+"_1doordetails.txt";
                                    let fileURLdeails = filePath00.appendingPathComponent(filenamedetailsname)

                                    //*************zipcode Changes****************  //******** change 0.1.0 ***
                                    let dic = ["name":self.cur1Door.title,"desc":self.cur1Door.desc,"price":self.cur1Door.price,"catID":self.cur1Door.catalogID,"checkout":false,"fileURL":"\(folderName)/\(folderName00)__\(filenamedetailsname)","fileName":filename,"mID": self.cur1Door.manufacturerID] as [String : Any]
                                    do {
                                        let jsonData: Data? = try? JSONSerialization.data(withJSONObject: dic)

                                        try jsonData!.write(to: fileURLdeails)
                                        DataManager.UpdateFavoriteScenes(dataToConve:jsonData!,filename:"\(folderName)/\(folderName00)__\(filenamedetailsname)" , typeName: "doorName")

                                    } catch {

                                    }
                                    filenamesArray.append(fileURLdeails)
                                    filenamesArray.append(fileURL1d)
                                }
                            }
                        }
                    }
                    DataManager.createFavoriteScene(catalogArray: catArray, mainFolderName:folderName, catFolderName: folderName00, filenamesArray: filenamesArray,
                                                    completion:
                        {(error,sceneid) in
//print("after create Favorite sceneid:",sceneid as Any)
                            LoadingIndicatorView.hide()
                    })
                }
            }
        }
    }
    
    //-----------------------------------------------
    func saveImageToDocumentDirectory(image0: UIImage?, type:String )
    {
        //print("save Image To DocumentDirectory")
        if let image = image0
        {
            var filename = currentImageName
            
            if (type == ".jpg")
            {
                if let data = image.jpegData(compressionQuality:1.0)
                {
                    if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                    {
                        let fileURL = dir.appendingPathComponent(filename)
                        
                        if FileManager.default.fileExists(atPath:fileURL.path)
                        {
                            //print("1 FILE AVAILABLE")
                            
                        } else {
                            //print("FILE NOT AVAILABLE")
                            
                            try? data.write(to: fileURL)
                            
                        }
                        self.uploadFilesToCloud()
                        
                    }
                }
            } else {
                filename = currentImageName.replacingOccurrences(of:".jpg", with: type)
                if let data = image.pngData()
                {
                    if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                    {
                        let fileURL = dir.appendingPathComponent(filename)
                        
                        try? data.write(to: fileURL)
                    }
                }
            }
        }
    }
    
    //    //----------------------------------------------
    func checkImageExist(filename:String)->UIImage?
    {
        //print("check Image Exist")
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath = paths.first
        {
            let imageURLpaint = URL(fileURLWithPath: dirPath).appendingPathComponent(filename)
            let imagePaint    = UIImage(contentsOfFile: imageURLpaint.path)
            if imagePaint != nil
            {
                return imagePaint
            }else
            {
                return nil
            }
        }else
        {
            return nil
        }
        
    }
    
    //---------------------------------------------- //*************** Issues 1.0
    func loadUpImageFromDocumentDirectory(nameOfImage : String)
    {
        
    }
//    {
//        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
//        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
//        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
//        if let dirPath = paths.first
//        {
//            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(nameOfImage)
//            //print("loadUpImage From DocumentDirectory  imageURL:",imageURL)
//            if let image = UIImage(contentsOfFile: imageURL.path)
//            {
//                self.imageView.image = image
//                //print("loadUpImage From DocumentDirectory  self.imageView.image:",self.imageView.image as Any)
//            }
//
//            //print("loadUpImage From DocumentDirectory  currentPaintImageController:",currentPaintImageController as Any)
//            if currentPaintImageController != nil
//            {
//                self.paintImageView.isHidden = true;
//                self.paintImageView1.isHidden = true;
//                self.paintImageView2.isHidden = true; //******** change 0.0.40 add ***
//
//                let well0Sel = nameOfImage.replacingOccurrences(of:".jpg", with: "_wasWell0Sel.txt")
//
//                //print("loadUpImage From DocumentDirectory  well0Sel:",well0Sel)
//                let dicWell = self.checkIfWellNumExists(filename:well0Sel)
//                //print("loadUpImage From DocumentDirectory  dicWell:",dicWell)
//
//                if dicWell["wasWell0Sel"] != nil
//                {
//                    if (dicWell["wasWell0Sel"] as? Int == 1)
//                    {
//                        self.paintImageView.isHidden = false;
//                    }
//
//                }
//
//                if dicWell["wasWell1Sel"] != nil
//                {
//                    if (dicWell["wasWell1Sel"] as? Int == 1)
//                    {
//                        self.paintImageView1.isHidden = false;
//                    }
//
//                }
//
//                //******** change 0.0.40 add ***
//                if dicWell["wasWell2Sel"] != nil
//                {
//                    if (dicWell["wasWell2Sel"] as? Int == 1)
//                    {
//                        self.paintImageView2.isHidden = false;
//                    }
//                }
//
//
//                let paintMasksal0 = self.loadImgTypeGallery(type:"_paint0",filename:nameOfImage, ext: "png")
//
//                var filenamePaint0 = nameOfImage.replacingOccurrences(of:".jpg", with: "_paint0.png")
//                if paintMasksal0.count>0
//                {
//                    // let paintUrl = paintMasksal0.last?.absoluteURL.lastPathComponent
//                    filenamePaint0 = nameOfImage.replacingOccurrences(of:".jpg", with: "_paint0.png")
//                    if currentPaintImageController != nil
//                    {
//                        filenamePaint0 = nameOfImage.replacingOccurrences(of:".jpg", with: "_paint0\(selectedPaint).png")
//                    }
//                }
//
//                let imageURLpaint0 = URL(fileURLWithPath: dirPath).appendingPathComponent(filenamePaint0)
//                if let imagePaint0 = UIImage(contentsOfFile: imageURLpaint0.path)
//                {
//                    //print("loadUpImage From DocumentDirectory  imagePaint0:",imagePaint0)
//                    self.paintImageView.image = imagePaint0
//                    self.paintImageView.isHidden = false;
//                }
//
//                var filenamePaint1 = nameOfImage.replacingOccurrences(of:".jpg", with: "_paint1.png")
//                let tempPaint  = filenamePaint0.replacingOccurrences(of:"_paint0", with: "_paint1")
//                if paintMasksal0.count > 0
//                {
//                    filenamePaint1 = tempPaint
//                }
//
//                let imageURLpaint1 = URL(fileURLWithPath: dirPath).appendingPathComponent(filenamePaint1)
//                if let imagePaint1 = UIImage(contentsOfFile: imageURLpaint1.path)
//                {
//                    self.paintImageView1.image = imagePaint1
//                    self.paintImageView1.isHidden = false;
//
//                }
//
//                //******** change 0.0.40 add ***
//                var filenamePaint2 = nameOfImage.replacingOccurrences(of:".jpg", with: "_paint2.png")
//                let tempPaint2  = filenamePaint0.replacingOccurrences(of:"_paint0", with: "_paint2")
//                if paintMasksal0.count > 0
//                {
//                    filenamePaint2 = tempPaint2
//                }
//
//                let imageURLpaint2 = URL(fileURLWithPath: dirPath).appendingPathComponent(filenamePaint2)
//                if let imagePaint2 = UIImage(contentsOfFile: imageURLpaint2.path)
//                {
//                    self.paintImageView2.image = imagePaint2
//                    self.paintImageView2.isHidden = false;
//
//                }
//            }
//        }
//    }
    
    //-------------------------------------------------------------------------------
    func removeOldFileIfExist(fileName:String)
    {
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        if paths.count > 0 {
            let dirPath = paths[0]
            let imageName = fileName.changeExtensions(name:fileName)
            let docName = fileName.replacingOccurrences(of:".jpg", with: "_doc.txt")
            let persDocName = fileName.replacingOccurrences(of:".jpg", with: "_perspdoc.txt")
            let removeFolder = fileName.replacingOccurrences(of:".jpg", with: "_folder.png")
            
            var paintMasksal0 = self.loadImgTypeGallery(type:"_paint0",filename:fileName, ext: "png")
            let paintMasksall = self.loadImgTypeGallery(type:"_paint1",filename:fileName, ext: "png")
            
            if paintMasksall.count > 0
            {
                paintMasksal0.append(contentsOf:paintMasksall)
            }
            
            let filesToremove = [imageName,docName,persDocName,removeFolder]
            
            for file in filesToremove
            {
                let filePath = NSString(format:"%@/%@", dirPath, file) as String
                
                do {
                    try FileManager.default.removeItem(atPath: filePath)
                    print("file has been removed: ",file)
                } catch {
                    print("an error during a removing: ",file)
                }
            }
            
            for paintFile in paintMasksal0
            {
                let pathFile = paintFile.lastPathComponent
                let maskFile = pathFile.replacingOccurrences(of:"_paint", with: "_paintMask")
                
                var startColor = pathFile.replacingOccurrences(of:"_paint", with: "_startColor")
                startColor = startColor.replacingOccurrences(of: ".png", with: ".txt")
                
                var defaultColor = pathFile.replacingOccurrences(of:"_paint", with: "_defaultColor")
                defaultColor = defaultColor.replacingOccurrences(of: ".png", with: ".txt")
                
                var wasWellTxt = pathFile.replacingOccurrences(of:"_paint", with: "_wasWell0Sel")
                wasWellTxt = wasWellTxt.replacingOccurrences(of: ".png", with: ".txt")
                
                let filesToremove1 = [pathFile,maskFile,startColor,defaultColor,wasWellTxt]
                for file in filesToremove1
                {
                    let filePath = NSString(format:"%@/%@", dirPath, file) as String
                    
                    do {
                        try FileManager.default.removeItem(atPath: filePath)
                        print("file has been removed: ",file)
                    } catch {
                        print("an error during a removing: ",file)
                    }
                }
            }
        }
    }
    
    //----------------------------------------------------
    @IBAction func completeButtonAction(_ sender: Any)
    {
        let agentId = DataManager.checkAgentFileUrlWithName(imageName:currentImageName)

        if agentId.count > 0   //********** change 3.0.3 this was missing ???
        {
            let alertSuccess = UIAlertController(title: "", message: "Are you sure you want to complete this project", preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction (title: "Yes", style: UIAlertAction.Style.default, handler: { _ in

                LoadingIndicatorView.show()
                DataManager.completeProject(agentID:agentId,status:"completed",completion: {(error,status) in
                    LoadingIndicatorView.hide()
                    self.deleteFromLocal(agentID:agentId)
                     DataManager.getCompletedAgentHomes(range: "0", limit: "1000", completion:{ //*********Change 2.1.6
                                    (error,data) in
                                    self.loadDataWithResponse(data:data,type:"agenthome")

                                })
                })
            })

            alertSuccess.addAction(okAction)

            let cancelAction = UIAlertAction (title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
            alertSuccess.addAction(cancelAction)


			if let popoverController = alertSuccess.popoverPresentationController  //********** change 3.0.3 add if block ***
			{
			  popoverController.sourceView = self.view
			  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
			  popoverController.permittedArrowDirections = []
			}

            self.present(alertSuccess, animated: true, completion: nil)
            
        } else {

        }
    }

	//--------------------------------
    func mailPopup(agentId:String)
    {
        if let agentItem = DataManager.getAgentDetailsFromAgentID(agentID:agentId)
        {
            let mailAlert = UIAlertController(title: "", message: "Would you like to send Order Number to agent", preferredStyle: UIAlertController.Style.alert)
            let sendAction = UIAlertAction (title: "Yes", style: UIAlertAction.Style.default, handler: { _ in
                
                
                self.sendMail(agentDetails:agentItem)
                
            })
            mailAlert.addAction(sendAction)
            
            let noAction = UIAlertAction (title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
            mailAlert.addAction(noAction)


			if let popoverController = mailAlert.popoverPresentationController  //********** change 3.0.3 add if block ***
			{
			  popoverController.sourceView = self.view
			  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
			  popoverController.permittedArrowDirections = []
			}

            self.present(mailAlert, animated: true, completion: nil)
        }
    }

	//--------------------------------
    func getCreatedOrders()->[URL]
    {
        var modiOrder:[URL] = [URL]()
        for location in 0..<self.imagesArray.count
        {
            let url:URL = self.imagesArray[location]
            var name = (url.absoluteString as NSString).lastPathComponent
            name = name.changeExtensions(name:name)
            if !imageNames.contains(name)
            {
                modiOrder.append(url)
            }
        }
        return modiOrder
    }
    
    //---------------------------------------
    func deleteFromLocal(agentID:String)
    {
        if projectList00.count > 1
        {
            if (selectedGalleryImage < projectList00.count)
            {
                var shouldChange = false;
              
                self.mailPopup(agentId:agentID)
               
                if selectedGalleryImage == -1
                {
                    selectedGalleryImage = 0
                }
                let projectThumb:ProjectThumb = projectList00[selectedGalleryImage]
                //print("deleteHouseImage projectThumb.projectID:",projectThumb.projectID as Any)
                
                projectList00.remove(at: selectedGalleryImage)
                
                //print("2 deleteHouseImage projectList00.count:",projectList00.count)
                //                        if (self.selectedGalleryImage == 0)
                //                        {
                shouldChange = true;
                self.selectedGalleryImage = max(self.selectedGalleryImage-1,0)
                self.btnDoneClickedAction()
                
                //  }
                if (projectList00.count < 1)
                {
                    self.selectedGalleryImage = -1
                }
                
                do
                {
                    if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                    {
                        let fileURL = dir.appendingPathComponent("projectThumbs.txt")
                        //print("saveHouseTo DocumentDirectory fileURL:",fileURL)
                        let data = try NSKeyedArchiver.archivedData(withRootObject: projectList00, requiringSecureCoding: false)
                        try data.write(to: fileURL)
                    }
                    
                } catch {
                    //print("deleteBt nAction  error:",error)
                }
                
                photoCollectionView.reloadData()
                
                if let projectId = projectThumb.projectID
                {
                    do
                    {
                        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                        {
                            NSKeyedArchiver.setClassName("Project", for: Project.self)
                            let fileURL = dir.appendingPathComponent("project_"+projectId+".txt")
                            try FileManager.default.removeItem(at: fileURL)
                            //print("del eteBt nAction project has Been del ete")
                        }
                        
                    } catch {
                        //print("deleteBt nAction  error:",error)
                    }
                }
                
                //print("deleteHouseImage shouldChange:",shouldChange)
                //print("deleteHouseImage self.selectedGal leryImage:",self.selectedGalleryImage)
                //print("deleteHouseImage projectList00.count:",projectList00.count)
                if (shouldChange && (self.selectedGalleryImage >= 0) && (self.selectedGalleryImage < projectList00.count))
                {
                    currProjectThumb = projectList00[selectedGalleryImage]
                    self.changeToProject(currProjectThumb);
                    self.photoView.isHidden = true;
                    shouldSave = false;
                }
            }
        }else
        {
            self.photoCollectionView.reloadData()
        }
       
    }
    
    //-----------------------------------------------------
    func clearThePaint()
    {
//print("clear The Paint  self.garageMa skView.image = nil")
        garageScrollIndex = 0
        
        wasChangeOfProject = true;  //***************  change 1.0.2 add ***

        self.paintImageView.image = nil;
        self.paintImageView1.image = nil;
        self.paintImageView2.image = nil; //******** change 0.0.40 add ***
        self.garageImageView.image = nil;
        
        self.garageMaskView.image = nil;  //*************** change 0.0.30 add ***
        self.frontDrMaskView.image = nil;  //*************** change 0.0.30 add ***
        
        maskedDoorImage  = nil;   //*************** change 0.0.30 add ***
//        maskedDo orImage1 = nil;  //*************** change 1.1.0 remove ***
        
        maskedFrontDrImage  = nil;   //*************** change 0.0.30 add ***
//        maskedFro ntDrImage1 = nil;  //*************** change 1.1.0 remove ***
        
        //------------- initialize the startColor for garage and door ---- until read from table entry ----
        doorStartColor0 = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.0);
        defaultDoorColor0 = doorStartColor0;
//print(" clearPaint   defaultDo orColor0:",defaultDoorColor0 as Any)

        frontDrStartColor0 = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.0);
        defaultFrontDrColor0 = frontDrStartColor0;
        
        maskedHouseImage0 = nil;
        maskedHouseImage1 = nil;
        maskedHouseImage2 = nil; //************** change 1.1.2 add **
        currGallThumb = nil; //************** change 1.1.2 add **
        maskedRoofImage = nil;
        wasGoodLayer = false;
        polyLayer.path = nil
        
        if let mainCont = currentMainImageController
        {
            mainCont.scrollView.paintImageView.image = nil;
            mainCont.scrollView.paintImageView1.image = nil;
            mainCont.scrollView.paintImageView2.image = nil;  //******** change 0.0.40 add ***
            mainCont.scrollView.garageImageView.image = nil;
        }
        
        if let paintCont = currentPaintImageController
        {
            paintCont.turnItAllAllOff()
            paintCont.paintLayer.path = nil
            
            paintCont.scrollView.paintImageOverlay0.image = nil;
            paintCont.scrollView.paintImageOverlay1.image = nil;
            paintCont.scrollView.paintImageOverlay2.image = nil;
            paintCont.scrollView.paintImageOverlay3.image = nil;
            paintCont.scrollView.paintImageOverlay4.image = nil;
            
            paintCont.scrollView.garageImageView.image = nil;
//print(" clearPaint   paintCont.scrollView.garageIma geView.image:",paintCont.scrollView.garageIma geView.image as Any)
            paintCont.scrollView.garageMaskView.image = nil;
            paintCont.scrollView.frontDrMaskView.image = nil;
            
            paintCont.scrollView.paintImageOverlay0.highlightedImage = nil;
            paintCont.scrollView.paintImageOverlay1.highlightedImage = nil;
            paintCont.scrollView.paintImageOverlay2.highlightedImage = nil;
            paintCont.scrollView.paintImageOverlay3.highlightedImage = nil;
            paintCont.scrollView.paintImageOverlay4.highlightedImage = nil;
            
            paintCont.scrollView.garageImageView.highlightedImage = nil;
            paintCont.scrollView.garageMaskView.highlightedImage = nil;
            paintCont.scrollView.frontDrMaskView.highlightedImage = nil;
            
            paintCont.currentOverlay = nil;
        }
        
        currentMainImageController = nil;
        currentPaintImageController = nil;
    }
    
    
    //MARK: agentHome Background call
    // -------------
    func loadAgentHomes()
    {
//print("0 loadAgentHomes hasInternalApp",hasInternalApp)
        DataManager.getCompletedAgentHomes(range: "0", limit: "1000", completion:{
                       (error,data) in
//print("1 loadAgentHomes agenthome")
                       self.loadDataWithResponse(data:data,type:"agenthome")
                   })
      
        		if hasInternalApp
              {
              DataManager.getAllAgentHomes(range: "0", limit: "1000", completion:{
                         (error,data) in
                         self.loadDataWithResponse(data:data,type:"agent")
                        
                     })
              DataManager.getDeletedAgentHomes(range: "0", limit: "1000", completion:{
                         (error,data) in
                         self.loadDataWithResponse(data:data,type:"agenthomedelete")
                     })
              }
    }

    //----------------------------------------------
    func loadDataWithResponse(data:NSDictionary?, type:String)
    {
//print("0 loadDataWit hResponse data", data as Any)
       if data != nil
        {
            
            var file_name = "AllAgentHomesData.txt"
            if type == "agenthome"
            {
                file_name = "CompletedHomesData.txt"
                
            }
            if type == "agenthomedelete"
            {
                file_name = "DeletedHomesData.txt"
                
            }
            
            
            // let fullsizedArray =  DataManager.loadPaintTextFile(fileName:file_name)
            var totalArray:[Any] = [Any]()
            totalArray.append(contentsOf:(data!["data"] as? [Any] ?? []) )
            
            // totalArray.append(contentsOf:fullsizedArray)
            
            var tempArray:[Any] = [Any]()
            for item in totalArray
            {
                if let itemDic = item as? NSDictionary
                {
                    let imageURl = itemDic["image_url"] as? String ?? ""
                    if verifyUrl(urlString:imageURl)
                    {
                        tempArray.append(item)
                    }
                }
                
            }
            totalArray = tempArray

//print("0 loadDataWit hResponse totalArray", totalArray)


            if totalArray.count > 0
            {
                let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
                let filename = file_name
                let filenameTemppath =  paths[0].appendingPathComponent(filename)
                
                do {
                    
                    let jsonData: Data? = try? JSONSerialization.data(withJSONObject: totalArray)
                    try jsonData!.write(to: filenameTemppath)
                } catch {
  print("Couldn't write file")
                }
            }

			if (isHomeAgent)  //********** change 3.0.7 change ***
			{
				let agentId = DataManager.checkAgentFileUrlWithName(imageName:currentImageName)
//print("loadDataWit hResponse agentId",agentId)
				if agentId.count > 0
				{
					completeBtn.isHidden = false
//print("loadDataWit hResponse completeBtn")
				} else {
					completeBtn.isHidden = true
				}
			}
        }
    }

    //----------------------------------------------
    func loadDownloadedImage()
    {
        LoadingIndicatorView.show()
        //print("loadDownload edImage")
        self.loadGallery()
        
        self.blurrImgView.isHidden = true
        camView.isHidden = true
        UserDefaults.standard.set(true, forKey: "removeCamView")
        self.photoView.isHidden = true
        self.shareView.isHidden = false
        
        self.clearThePaint()
        self.clearOldProject()
         self.setupButtonTitlesForImage()
        //        loadUpPaint()
       
        if UserDefaults.standard.value(forKey: "Download_Last_Image_url") != nil
        {
            comingFromfeed = false
            
            let lastImageUrl = UserDefaults.standard.value(forKey: "Download_Last_Image_url") as! String
            debugPrint("currentImage URL",lastImageUrl)
            UserDefaults.standard.set(lastImageUrl, forKey:"Last_Image_url")

            let url:URL = URL(fileURLWithPath: lastImageUrl)
            currentImageName = (url.absoluteString as NSString).lastPathComponent
            if currentImageName.contains("--")
            {
                let imgarray = currentImageName.components(separatedBy:"--")
                // let tempfilename2 = imgarray[0]
                currentImageName = imgarray[0] + ".jpg"
            }
            currentImageName = currentImageName.changeExtensions(name:currentImageName)
            
            if let image = self.checkImageExist(filename:currentImageName)
            {
                LoadingIndicatorView.hide()
                self.imageView.image = image
                self.afterImageDownload()
            } else {
                let urlStr = URL(string: lastImageUrl)
                self.imageView.sd_setImage(with: urlStr) { (image, error, imageCacheType, imageUrl) in
                    if (error != nil) {
                        // Failed to load image
                        LoadingIndicatorView.hide()
                    } else {
                        LoadingIndicatorView.hide()
                        self.afterImageDownload()
                        // Successful in loading image
                    }
                }
              
            }
            
            
        }
    }
    
    func afterImageDownload()
    {
        
        
        let first4 = String(currentImageName.prefix(4))
        
        if first4 == "modi"
        {
            addedImageFromCameraStr = "defaultImage"
        } else {
            addedImageFromCameraStr = "library"
        }
        
        //           if let curPaint = currentPaintImageController
        //           {
        //                if !fromOrderNumber
        //                {
        //                    self.paintImageView.isHidden = true;
        //                    self.paintImageView1.isHidden = true;
        //                    self.paintImageView2.isHidden = true;
        //                    self.paintImageView.image  = curPaint.scrollView.paintImageOverlay0.image;
        //                    self.paintImageView1.image = curPaint.scrollView.paintImageOverlay1.image;
        //                    self.paintImageView2.image = curPaint.scrollView.paintImageOverlay2.image;
        //                }
        //            }
        
        fromOrderNumber = false
        
        originalHomeImage = self.imageView.image
        targetMaskRect.size = originalHomeImage.size;
        
        
        let size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height);
        //print("loadDownload edImage size:",size)
        self.shallRotate(size)
        
//        self.clearThePaint()
//        self.clearOldProject()
        
        self.setupButtonTitlesForImage()
        
        //print("1 loadDownload edImage selectedGal leryImage:",selectedGalleryImage)
        self.selectedReferenceItem = nil
        self.selectedReferenceItem = ReferenceItem.init(name: "Camera")
        self.selectedReferenceItem?.image = self.imageView.image
        
        commingFromProjectList = true;
        
        self.fetchHouseObject
            { (house0Object) in
                
                //print(" -------------------- selectedAgent here ----------------")
                
                self.selectedAgent()
                
                //********** change 0.0.40 add ***
        }
    }
    
    // MARK: - project to disk and back
    //********** change 0.0.40 add ***
    //-------------------------------------
    func selectedAgent()
    {
        
        //print("  selectedAgent  currentImageName:",currentImageName)
        guard let _ = originalHomeImage else {return;}
        guard let _ = originalHomeImage.cgImage else {return;}
        
        //--------------- set up as a Project -------------------
        var targetSize:CGSize = CGSize(width:originalHomeImage.size.width*0.35, height:originalHomeImage.size.height*0.35);
        if (originalHomeImage.size.width < 2000.0) || (originalHomeImage.size.height < 2000.0)
        {
            targetSize = CGSize(width:originalHomeImage.size.width*0.75, height:originalHomeImage.size.height*0.75);
        }
        
        let project = Project.init()
        let projectThumb = ProjectThumb.init()
        let choiceThumb:ProjectThumb = ProjectThumb.init()
        
        project.projectID = projectThumb.projectID
        
        currProject = project;
        currProject?.wasProcessed = true;     //*************** change 0.0.46  add  ***
        
        currProjectThumb = projectThumb;
        shouldAddToAlbum = false;
        
        commingFromProjectList = true;
        currentWellNum = 0
        currentButtonToDisplay = 0
        
        forcedImageSize = Size(width: Float(originalHomeImage.size.width),
                               height:Float(originalHomeImage.size.height));
        
        targetMaskRect.size = originalHomeImage.size;
        currentImageName = currentImageName.changeExtensions(name:currentImageName)
        
        let paintsDownloadArray = self.loadImgTypeGallery(type:"_paintMask0", filename: currentImageName, ext:"png")
        
        let paintsThumbDownloadArray = self.loadImgTypeGallery(type:"_projectChoices", filename: currentImageName, ext:"txt")
        let fileManager = FileManager.default
        //------------------- read in the layers ----------------
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        {
            
            //------------------- first layer doesn't have _0 but should ----------------
            
            var filenamePaint0 = currentImageName.replacingOccurrences(of:".jpg", with: "_paintMask0.png")
            if paintsDownloadArray.count>0
            {
                filenamePaint0 = paintsDownloadArray[0].lastPathComponent
            }
            var fileURL = dir.appendingPathComponent(filenamePaint0)
            //print("0  selectedAgent  filenamePaint0:",filenamePaint0)
            
            if let mask = UIImage(contentsOfFile: fileURL.path)
            {
                //print("  selectedAgent  mask:",mask)
                maskedHouseImage0 = mask;
            }
            
            
            let filenamePaint1 = filenamePaint0.replacingOccurrences(of:"_paintMask0", with: "_paintMask1")
            fileURL = dir.appendingPathComponent(filenamePaint1)
            
            if let mask = UIImage(contentsOfFile: fileURL.path)
            {
                maskedHouseImage1 = mask;
            }
            
            let filenamePaint2 = filenamePaint0.replacingOccurrences(of:"_paintMask0", with: "_paintMask2")
            fileURL = dir.appendingPathComponent(filenamePaint2)
            
            if let mask = UIImage(contentsOfFile: fileURL.path)
            {
                maskedHouseImage2 = mask;
            }
            
            
            
            //------------------- now colors layer 0  first layer doesn't have _0 but should ----------------
            let tempStart = filenamePaint0.replacingOccurrences(of:"_paintMask0", with: "_startColor0")
            let startColorName0 = tempStart.replacingOccurrences(of:".png", with: ".txt")
            fileURL = dir.appendingPathComponent(startColorName0)
            //print("startColorName0:",startColorName0)
            
            if let rawdata = try? Data(contentsOf: URL(fileURLWithPath: fileURL.path))
            {
                if let color = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? UIColor
                {
                    origStartColor0 = color
                    //print("origStartColor0:",origStartColor0)
                }
            }
            
            let tempDef = filenamePaint0.replacingOccurrences(of:"_paintMask0", with: "_defaultColor0")
            
            let defColorName0 = tempDef.replacingOccurrences(of:".png", with: ".txt")
            fileURL = dir.appendingPathComponent(defColorName0)
            
            if let rawdata = try? Data(contentsOf: URL(fileURLWithPath: fileURL.path))
            {
                if let color = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? UIColor
                {
                    defaultLineColor0 = color
                    //print("defaultLineColor0:",defaultLineColor0)
                }
            }
            
            //------------------- now colors layer 1  first layer doesn't have _0 but should ----------------
            let tempstart1 = filenamePaint0.replacingOccurrences(of:"_paintMask0", with: "_startColor1")
            
            let startColorName1 = tempstart1.replacingOccurrences(of:".png", with: ".txt")
            fileURL = dir.appendingPathComponent(startColorName1)
            
            if let rawdata = try? Data(contentsOf: URL(fileURLWithPath: fileURL.path))
            {
                if let color = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? UIColor
                {
                    origStartColor1 = color
                    //print("origStartColor1:",origStartColor1)
                }
            }
            
            let tempdef1 = filenamePaint0.replacingOccurrences(of:"_paintMask0", with: "_defaultColor1")
            let defColorName1 = tempdef1.replacingOccurrences(of:".png", with: ".txt")
            fileURL = dir.appendingPathComponent(defColorName1)
            
            if let rawdata = try? Data(contentsOf: URL(fileURLWithPath: fileURL.path))
            {
                if let color = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? UIColor
                {
                    defaultLineColor1 = color
                    //print("defaultLineColor1:",defaultLineColor1)
                }
            }
            
            //------------------- now colors layer 2  first layer doesn't have _0 but should ----------------
            let tempstart2 = filenamePaint0.replacingOccurrences(of:"_paintMask0", with: "_startColor2")
            let startColorName2 = tempstart2.replacingOccurrences(of:".png", with: ".txt")
            fileURL = dir.appendingPathComponent(startColorName2)
            
            if let rawdata = try? Data(contentsOf: URL(fileURLWithPath: fileURL.path))
            {
                if let color = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? UIColor
                {
                    origStartColor2 = color
                }
            }
            
            let tempdef2 = filenamePaint0.replacingOccurrences(of:"_paintMask0", with: "_defaultColor2")
            let defColorName2 = tempdef2.replacingOccurrences(of:".png", with: "txt")
            fileURL = dir.appendingPathComponent(defColorName2)
            
            if let rawdata = try? Data(contentsOf: URL(fileURLWithPath: fileURL.path))
            {
                if let color = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? UIColor
                {
                    defaultLineColor2 = color
                }
            }
            
            //------------------- set defaults changed below ----------------
            defaultLineColor = defaultLineColor0;
            origStartColor = origStartColor0;
            maskedHouseImage = maskedHouseImage0;
            
            
            //--------- establish which layers are on, formated like this ----- {"wellNum":0,"wasWell0Sel":1,"wasWell1Sel":0,"wellNumTot":0}
            let tempwelSel1 = filenamePaint0.replacingOccurrences(of:"_paintMask0", with: "_wasWell0Sel")
            
            let paintWell0Sel = tempwelSel1.replacingOccurrences(of:".png", with: ".txt")
            fileURL = dir.appendingPathComponent(paintWell0Sel)
            
            var paintDicWell0:[String:Any]? = nil
            
            if let contents = try? String(contentsOfFile:fileURL.path)
            {
                let data :Data = contents.data(using: String.Encoding.utf8)! as Data
                
                if let jsonArray = try? JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String:Any]
                {
                    paintDicWell0 = jsonArray
                }
            }
            //print("paintDicWell0:",paintDicWell0 as Any)
            
            if let paintDicWell = paintDicWell0
            {
                //***************  change 0.0.48 add these lines up to the "end" below  ***
                well0tsel = false;
                well1tsel = false;
                well2tsel = false;
                well3tsel = false;
                well4tsel = false;
                
                if paintDicWell["wasWell0Sel"] != nil
                {
                    if (paintDicWell["wasWell0Sel"] as? Int == 1)
                    {
                        well0tsel = true;
                    }
                }
                
                if paintDicWell["wasWell1Sel"] != nil
                {
                    if (paintDicWell["wasWell1Sel"] as? Int == 1)
                    {
                        well1tsel = true;
                    }
                }
                
                if paintDicWell["wasWell2Sel"] != nil
                {
                    if (paintDicWell["wasWell2Sel"] as? Int == 1)
                    {
                        well2tsel = true;
                    }
                }
                //***************  end of change 0.0.48 add these lines  ***
                if paintDicWell["wasWell0Sel"] != nil
                {
                    if (paintDicWell["wasWell0Sel"] as? Int == 1)
                    {
                        project.well0 = true;
                    }
                }
                
                if paintDicWell["wasWell1Sel"] != nil
                {
                    if (paintDicWell["wasWell1Sel"] as? Int == 1)
                    {
                        project.well1 = true;
                    }
                }
                
                if paintDicWell["wasWell2Sel"] != nil
                {
                    if (paintDicWell["wasWell2Sel"] as? Int == 1)
                    {
                        project.well2 = true;
                    }
                }
                
                if paintDicWell["wellNumTot"] != nil
                {
                    if (paintDicWell["wellNumTot"] as? Int == 1)
                    {
                        currentButtonToDisplay = 1;
                    }
                    
                    if (paintDicWell["wellNumTot"] as? Int == 2)
                    {
                        currentButtonToDisplay = 2;
                    }
                }
                
                
                if paintDicWell["wellNum"] != nil
                {
                    if (paintDicWell["wellNum"] as? Int == 1)
                    {
                        currentWellNum = 1;
                        defaultLineColor = defaultLineColor1;
                        origStartColor = origStartColor1;
                        maskedHouseImage = maskedHouseImage1;
                    }
                    
                    if (paintDicWell["wellNum"] as? Int == 2)
                    {
                        currentWellNum = 2;
                        defaultLineColor = defaultLineColor2;
                        origStartColor = origStartColor2;
                        maskedHouseImage = maskedHouseImage2;
                    }
                }
            }
            //--------- end formated like this ----- {"wellNum":0,"wasWell0Sel":1,"wasWell1Sel":0,"wellNumTot":0}
            
            
            //------------- now save the project to disk, along with first blank thumbnail, and next gallery image if it exists -------
            DispatchQueue.global().async
            {
                    autoreleasepool
                    {
                            var thumbImage:UIImage? = nil;
                            
                            UIGraphicsBeginImageContextWithOptions(targetSize, false, 1.0);
                            originalHomeImage.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
                            thumbImage = UIGraphicsGetImageFromCurrentImageContext();
                            UIGraphicsEndImageContext();
                            
                            project.thumbnailData = thumbImage?.jpegData(compressionQuality: 0.50)
                            projectThumb.thumbnailData = project.thumbnailData
                            choiceThumb.thumbnailData = project.thumbnailData;
                            thumbImage = nil;
                    }
                    
                    project.imageData = originalHomeImage.jpegData(compressionQuality: 1.0)
                    project.imageName = currentImageName;
                    
                    project.defaultLineColor0d = defaultLineColor0.encode()
                    project.origStartColor0d   = origStartColor0.encode()
                    
                    project.defaultLineColor1d = defaultLineColor1.encode()
                    project.origStartColor1d   = origStartColor1.encode()
                    
                    project.defaultLineColor2d = defaultLineColor2.encode()
                    project.origStartColor2d   = origStartColor2.encode()
                    
                    if let house0Object = houseObject
                    {
                        let encoder = JSONEncoder()
                        project.house0 = try? encoder.encode(house0Object)
                    }
                    
                    project.mask0 = maskedHouseImage0?.pngData()
                    project.mask1 = maskedHouseImage1?.pngData()
                    project.mask2 = maskedHouseImage2?.pngData()
                    
                    project.currentWellNum = currentWellNum
                    project.currentButtonToDisplay = currentButtonToDisplay
                    
                    projectList00 = self.loadProjects()
                    projectList00.insert(projectThumb, at: 0)
                    self.selectedGalleryImage = 0;
                    
                    
                    curProjectChoiceList = self.loadThumbChoices(projectThumb.projectID) //********** change 1.1.4 change,  needs to be universal ***
                    curProjectChoiceList.append(choiceThumb)  //********** change 1.1.4 change ***
                    
                    //---------------- save main project thumbnails to list ------------------
                    do
                    {
                        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                        {
                            let fileURL = dir.appendingPathComponent("projectThumbs.txt")
                            let data = try NSKeyedArchiver.archivedData(withRootObject: projectList00, requiringSecureCoding: false)
                            try data.write(to: fileURL)
                            hasBeenSaved = true;
                        }
                        
                    } catch {
  print(" ---------------------- selectedA gent projectThumbs    error:",error)
                    }
                    
                    
                    if let projectId = projectThumb.projectID
                    {
                        do
                        {
                            //---------------- save main project  ------------------
                            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                            {
                                let fileURL = dir.appendingPathComponent("project_"+projectId+".txt")
                                let currProjectURL = dir.appendingPathComponent("currProject.txt")
                                NSKeyedArchiver.setClassName("Project", for: Project.self)
                                
                                let data = try NSKeyedArchiver.archivedData(withRootObject: project, requiringSecureCoding: false)
                                try data.write(to: fileURL)
                                try data.write(to: currProjectURL)
                            }
                            
                        } catch {
  print(" ---------------------- selectedA gent    project_  error:",error)
                        }

                        do
                        {
                            //---------------- save choices thumbnails to choices list  ------------------
                            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                            {
                                let fileURL = dir.appendingPathComponent("projectThumbs_"+projectId+".txt")
                                let data = try NSKeyedArchiver.archivedData(withRootObject: curProjectChoiceList, requiringSecureCoding: false)   //********** change 1.1.4 change ***
                                try data.write(to: fileURL)
                            }
                            
                        } catch {
  print(" ---------------------- selectedA gent projec tThumbs_   error:",error)
                        }
                    }
                    
                    if let thumbProjectId = choiceThumb.projectID
                    {
                        do
                        {
                            //---------------- save current choice as a project  ------------------
                            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                            {
                                let fileURL = dir.appendingPathComponent("projectChoices_"+thumbProjectId+".txt")
                                NSKeyedArchiver.setClassName("Project", for: Project.self)
                                let data = try NSKeyedArchiver.archivedData(withRootObject: project, requiringSecureCoding: false)
                                try data.write(to: fileURL)
                            }
                            
                        } catch {
  print(" ---------------------- selectedA gent    proje ctChoices  error:",error)
                        }
                    }
                    
                    //------------ add the first gallery project past the blank original -----------
                    currProjectThumb?.thumbnailName = filenamePaint0
                    self.addProjectsChoiceForExample(currProjectThumb)

//print(" ---------------------- selectedA gent    start loop --------------------")
                    
                    //---------------- start loop of gallery entries  ------------------
                    if paintsDownloadArray.count > 0
                    {
                        for kount in 0..<paintsDownloadArray.count
                        {
                            
                            let filenamePaint0 = paintsDownloadArray[kount].lastPathComponent
                            var fileURL = dir.appendingPathComponent(filenamePaint0)
//print("0  selectedAgent  filenamePaint0:",filenamePaint0)
                            
                            if let mask = UIImage(contentsOfFile: fileURL.path)
                            {
                                maskedHouseImage0 = mask;
//print("  selectedAgent  maskedHouseImage0:",maskedHouseImage0 as Any)
                            } else {
                                break;
                            }
                            
                            let filenamePaint1 = filenamePaint0.replacingOccurrences(of:"_paintMask0", with: "_paintMask1")
                            fileURL = dir.appendingPathComponent(filenamePaint1)
                            
                            if let mask = UIImage(contentsOfFile: fileURL.path)
                            {
                                maskedHouseImage1 = mask;
                            }
                            
                            let filenamePaint2 = filenamePaint0.replacingOccurrences(of:"_paintMask0", with: "_paintMask2")
                            fileURL = dir.appendingPathComponent(filenamePaint2)
                            
                            if let mask = UIImage(contentsOfFile: fileURL.path)
                            {
                                maskedHouseImage2 = mask;
                            }
                            
                            
                            
                            //------------------- now colors layer 0  first layer doesn't have _0 but should ----------------
                            let tempStart = filenamePaint0.replacingOccurrences(of:"_paintMask0", with: "_startColor0")
                            let startColorName0 = tempStart.replacingOccurrences(of:".png", with: ".txt")
                            
                            fileURL = dir.appendingPathComponent(startColorName0)
                            
                            if let rawdata = try? Data(contentsOf: URL(fileURLWithPath: fileURL.path))
                            {
                                if let color = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? UIColor
                                {
                                    origStartColor0 = color
//print("origStartColor0:",origStartColor0)
                                }
                            }
                            
                            let tempdef = filenamePaint0.replacingOccurrences(of:"_paintMask0", with: "_defaultColor0")
                            let defColorName0 = tempdef.replacingOccurrences(of:".png", with: ".txt")
                            
                            fileURL = dir.appendingPathComponent(defColorName0)
                            
                            if let rawdata = try? Data(contentsOf: URL(fileURLWithPath: fileURL.path))
                            {
                                if let color = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? UIColor
                                {
                                    defaultLineColor0 = color
//print("defaultLineColor0:",defaultLineColor0)
                                }
                            }
                            
                            //------------------- now colors layer 1  first layer doesn't have _0 but should ----------------
                            let tempStart1 = filenamePaint0.replacingOccurrences(of:"_paintMask0", with: "_startColor1")
                            
                            let startColorName1 = tempStart1.replacingOccurrences(of:".png", with: ".txt")
                            fileURL = dir.appendingPathComponent(startColorName1)
                            
                            if let rawdata = try? Data(contentsOf: URL(fileURLWithPath: fileURL.path))
                            {
                                if let color = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? UIColor
                                {
                                    origStartColor1 = color
//print("origStartColor1:",origStartColor1)
                                }
                            }
                            
                            let tempDef1 = filenamePaint0.replacingOccurrences(of:"_paintMask0", with: "_defaultColor1")
                            let defColorName1 = tempDef1.replacingOccurrences(of:".png", with: ".txt")
                            fileURL = dir.appendingPathComponent(defColorName1)
                            
                            if let rawdata = try? Data(contentsOf: URL(fileURLWithPath: fileURL.path))
                            {
                                if let color = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? UIColor
                                {
                                    defaultLineColor1 = color
//print("defaultLineColor1:",defaultLineColor1)
                                }
                            }
                            
                            
                            //------------------- now colors layer 2  first layer doesn't have _0 but should ----------------
                            let tempStart2 = filenamePaint0.replacingOccurrences(of:"_paintMask0", with: "_startColor2")
                            
                            let startColorName2 = tempStart2.replacingOccurrences(of:".png", with: ".txt")
                            fileURL = dir.appendingPathComponent(startColorName2)
                            
                            if let rawdata = try? Data(contentsOf: URL(fileURLWithPath: fileURL.path))
                            {
                                if let color = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? UIColor
                                {
                                    origStartColor2 = color
                                }
                            }
                            
                            
                            let tempDef2 = filenamePaint0.replacingOccurrences(of:"_paintMask0", with: "_defaultColor2")
                            
                            let defColorName2 = tempDef2.replacingOccurrences(of:".png", with: ".txt")
                            fileURL = dir.appendingPathComponent(defColorName2)
                            
                            if let rawdata = try? Data(contentsOf: URL(fileURLWithPath: fileURL.path))
                            {
                                if let color = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? UIColor
                                {
                                    defaultLineColor2 = color
                                }
                            }
                            
                            //------------------- set defaults changed below ----------------
                            defaultLineColor = defaultLineColor0;
                            origStartColor = origStartColor0;
                            maskedHouseImage = maskedHouseImage0;
                            
                            
                            //--------- establish which layers are on, formated like this ----- {"wellNum":0,"wasWell0Sel":1,"wasWell1Sel":0,"wellNumTot":0}
                            let tempwasSel = filenamePaint0.replacingOccurrences(of:"_paintMask0", with: "_wasWell0Sel")
                            
                            let paintWell0Sel = tempwasSel.replacingOccurrences(of:".png", with: ".txt")
                            fileURL = dir.appendingPathComponent(paintWell0Sel)
                            
                            var paintDicWell0:[String:Any]? = nil
                            
                            if let contents = try? String(contentsOfFile:fileURL.path)
                            {
                                let data :Data = contents.data(using: String.Encoding.utf8)! as Data
                                
                                if let jsonArray = try? JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String:Any]
                                {
                                    paintDicWell0 = jsonArray
                                    
                                }
                            }
//print("paintDicWell0:",paintDicWell0 as Any)
                            
                            if let paintDicWell = paintDicWell0
                            {
                                well0tsel = false;
                                well1tsel = false;
                                well2tsel = false;
                                well3tsel = false;
                                well4tsel = false;
                                if paintDicWell["wasWell0Sel"] != nil
                                {
                                    if (paintDicWell["wasWell0Sel"] as? Int == 1)
                                    {
                                        well0tsel = true;
                                    }
                                }
                                
                                if paintDicWell["wasWell1Sel"] != nil
                                {
                                    if (paintDicWell["wasWell1Sel"] as? Int == 1)
                                    {
                                        well1tsel = true;
                                    }
                                }
                                
                                if paintDicWell["wasWell2Sel"] != nil
                                {
                                    if (paintDicWell["wasWell2Sel"] as? Int == 1)
                                    {
                                        well2tsel = true;
                                    }
                                }
                                
                                
                                if paintDicWell["wellNumTot"] != nil
                                {
                                    if (paintDicWell["wellNumTot"] as? Int == 1)
                                    {
                                        currentButtonToDisplay = 1;
                                    }
                                    
                                    if (paintDicWell["wellNumTot"] as? Int == 2)
                                    {
                                        currentButtonToDisplay = 2;
                                    }
                                }
                                
                                
                                if paintDicWell["wellNum"] != nil
                                {
                                    if (paintDicWell["wellNum"] as? Int == 1)
                                    {
                                        currentWellNum = 1;
                                        defaultLineColor = defaultLineColor1;
                                        origStartColor = origStartColor1;
                                        maskedHouseImage = maskedHouseImage1;
                                    }
                                    
                                    if (paintDicWell["wellNum"] as? Int == 2)
                                    {
                                        currentWellNum = 2;
                                        defaultLineColor = defaultLineColor2;
                                        origStartColor = origStartColor2;
                                        maskedHouseImage = maskedHouseImage2;
                                    }
                                }
                            }
                            //--------- end formated like this ----- {"wellNum":0,"wasWell0Sel":1,"wasWell1Sel":0,"wellNumTot":0}
                            
                            //--------- now add to gallery -----
                            currProjectThumb?.thumbnailName = filenamePaint0
                            self.addProjectsChoiceForExample(currProjectThumb)
                        }
                    }

                    if paintsThumbDownloadArray.count>0
                    {
                        for kount in 0..<paintsThumbDownloadArray.count
                        {
                            var tempDownloadProj:Project = Project.init()
                            let filenamePaint0 = paintsThumbDownloadArray[kount].lastPathComponent
                            let fileURL = dir.appendingPathComponent(filenamePaint0)
                            
                            if let nsData = NSData(contentsOfFile: fileURL.path) {
                                do {
                                    let data = Data(referencing:nsData)
                                    NSKeyedUnarchiver.setClass(Project.self, forClassName: "Project")
                                    
                                    if let project0 = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? Project
                                    {
                                        tempDownloadProj = project0
//print("0  selectedAgent  filenamePaint0:",filenamePaint0)
                                        if let data = tempDownloadProj.mask0 { maskedHouseImage0 = UIImage(data:data); }
                                        if let data = tempDownloadProj.mask1 { maskedHouseImage1 = UIImage(data:data); }
                                        if let data = tempDownloadProj.mask2 { maskedHouseImage2 = UIImage(data:data); }
                                        if let data = tempDownloadProj.mask3 { maskedHouseImage3 = UIImage(data:data); }
                                        if let data = tempDownloadProj.mask4 { maskedHouseImage4 = UIImage(data:data); }
                                        
                                        defaultLineColor0 = UIColor.color(data:tempDownloadProj.defaultLineColor0d)
                                        origStartColor0   = UIColor.color(data:tempDownloadProj.origStartColor0d)
                                        
                                        defaultLineColor1 = UIColor.color(data:tempDownloadProj.defaultLineColor1d)
                                        origStartColor1   = UIColor.color(data:tempDownloadProj.origStartColor1d)
                                        
                                        defaultLineColor2 = UIColor.color(data:tempDownloadProj.defaultLineColor2d)
                                        origStartColor2   = UIColor.color(data:tempDownloadProj.origStartColor2d)
                                        
                                        defaultLineColor3 = UIColor.color(data:tempDownloadProj.defaultLineColor3d)
                                        origStartColor3   = UIColor.color(data:tempDownloadProj.origStartColor3d)
                                        
                                        defaultLineColor4 = UIColor.color(data:tempDownloadProj.defaultLineColor4d)
                                        origStartColor4   = UIColor.color(data:tempDownloadProj.origStartColor4d)
                                        
                                        
                                        defaultDoorColor0 = UIColor.color(data:tempDownloadProj.defaultLineGaraged) //*************** change 1.1.0 change ***
                                        doorStartColor0   = UIColor.color(data:tempDownloadProj.origStartGaraged) //*************** change 1.1.0 change ***
                                        
                                        defaultFrontDrColor0 = UIColor.color(data:tempDownloadProj.defaultLineFrontDrd) //*************** change 1.1.0 change ***
                                        frontDrStartColor0   = UIColor.color(data:tempDownloadProj.origStartFrontDrd) //*************** change 1.1.0 change ***
                                        
                                        
                                        
                                        //------------------- set defaults changed below ----------------
                                        defaultLineColor = defaultLineColor0;
                                        origStartColor = origStartColor0;
                                        maskedHouseImage = maskedHouseImage0;
                                        
                                        
                                        well0tsel = tempDownloadProj.well0
                                        well1tsel = tempDownloadProj.well1
                                        well2tsel = tempDownloadProj.well2
                                        well3tsel = tempDownloadProj.well3
                                        well4tsel = tempDownloadProj.well4
                                        currentButtonToDisplay = tempDownloadProj.currentButtonToDisplay
                                        
                                        if (tempDownloadProj.currentWellNum == 1)
                                        {
                                            currentWellNum = 1;
                                            defaultLineColor = defaultLineColor1;
                                            origStartColor = origStartColor1;
                                            maskedHouseImage = maskedHouseImage1;
                                        }
                                        
                                        if (tempDownloadProj.currentWellNum == 2)
                                        {
                                            currentWellNum = 2;
                                            defaultLineColor = defaultLineColor2;
                                            origStartColor = origStartColor2;
                                            maskedHouseImage = maskedHouseImage2;
                                        }
                                        
                                        do {
                                            try fileManager.removeItem(atPath: fileURL.path)
                                        }
                                        catch {
                                            print("Error")
                                        }
                                        currProjectThumb?.thumbnailName = filenamePaint0
                                        self.addProjectsChoiceForExample(currProjectThumb)
                                    }
                                } catch {
  print(" ---------------------- changeToPa intChoice Couldn't read project -----------------------------")
                                }
                                
                            }
                        }
                    }
            }
        }
    }
    
    
    //------------------------------------
    func loadProjects() -> [ProjectThumb]
    {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        {
            let fileURL = dir.appendingPathComponent("projectThumbs.txt")
            
            do
            {
//print("che ckHouse ProjectExist houseURL:",houseURL)
                let rawdata = try Data(contentsOf: fileURL)
//print("che ckHouse ProjectExist rawdata.count:",rawdata.count)
                if let projectList = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? [ProjectThumb]
                {
//print("che ckHouse ProjectExist projectList:",projectList)
                    return projectList
                } else {
                    return [ProjectThumb]()
                }
            } catch {
  print(" ---------------------- load Projects ProjectExist Couldn't read projectList (probably new project) ---------------------- ")
                return [ProjectThumb]()
            }
        }
        
        return [ProjectThumb]()
    }
    
    //------------------------------------
    func loadThumbForProject(_ projectID0:String?) -> ProjectThumb
    {
//print("loadThum bForProject")
        guard let projectId = projectID0 else {return ProjectThumb.init()}
        
        let projectList = self.loadProjects()
        
        var kount = 0;
        
        for projectThumb in projectList
        {
            if (projectThumb.projectID == projectId)
            {
                return projectThumb;
            }
            kount += 1
        }
        
        return ProjectThumb.init()
    }
    
    
    //------------------------------------
    func loadThumbChoices(_ projectID0:String?) -> [ProjectThumb]
    {
        guard let projectId = projectID0 else {return [ProjectThumb]()}
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        {
            let fileURL = dir.appendingPathComponent("projectThumbs_"+projectId+".txt")
//print("loadPro jectChoices  fileURL:",fileURL)
            
            do
            {
//print("che ckHouse  houseURL:",houseURL)
                let rawdata = try Data(contentsOf: fileURL)
//print("che ckHouse  rawdata.count:",rawdata.count)
                if let projectList = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? [ProjectThumb]
                {
//print("che ckHouse  projectList:",projectList)
                    return projectList
                } else {
                    return [ProjectThumb]()
                }
            } catch {
  print(" ---------------------- loadThumb Choices  Couldn't read projectList, (probably new project) ---------------------- ")
                return [ProjectThumb]()
            }
        }
        
        return [ProjectThumb]()
    }
    
    
    //---------------------------------------------------------------------
    func sendHouseObjectJson()  //************** keep as proper way to save/get data to server **** (is this the latest? i think it is)
    {
        //print(" sendHou seObjectJson");
        if let house0Object = houseObject
        {
            do
            {
                let encoder = JSONEncoder()
                let houseData = try encoder.encode(house0Object)
                
                let session = URLSession.shared
                let url:URL! = URL(string: "http://35.203.106.101:5083/housedata")
                var urlRequest = URLRequest(url: url)
                
                urlRequest.httpMethod = "POST"
                urlRequest.timeoutInterval = 35.0
                urlRequest.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
                urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
                urlRequest.setValue("\(String(describing: houseData.count))", forHTTPHeaderField: "Content-Length")
                urlRequest.httpBody = houseData
                
                let task = session.dataTask(with: urlRequest)
                { responseData, response, error in
                    //print(" responseData: ",responseData as Any);
                    //print(" response: ",response as Any);
                    //print(" error: ",error as Any);
                }
                task.resume()
                
            } catch {
                print("err or:",error)
            }
        }
    }

 //********** change 1.1.4 not sure this is needed, as the project should already have been read in and is universal, "currProject",  did you add this? ***
    //-----------------------------------------------------
    func getMaskColors(index:Int)->Project?
    {
   //     let curProje ctChoiceListLocal:[ProjectThumb] = self.loadThu mbChoices(currProjectThumb?.projectID)   //********** change 1.1.4 remove,  needs to be universal ***

//print("getMaskColors curProjectChoiceList.count",curProjectChoiceList.count as Any)

		if curProjectChoiceList.count == 0 {return nil}  //************** change 3.0.6 several changes below  ***


		var index0 = index
		if (index >= curProjectChoiceList.count)
		{
			index0 = curProjectChoiceList.count
        }

        let projectThumb = curProjectChoiceList[index0]  //********** change 1.1.4 change ***
        
        if let projectId = projectThumb.projectID
        {
            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
            {
                let fileURL = dir.appendingPathComponent("projectChoices_"+projectId+".txt")
                do
                {
                    let rawdata = try Data(contentsOf: fileURL)
                    NSKeyedUnarchiver.setClass(Project.self, forClassName: "Project")
                    if let project0 = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? Project
                    {
                        return project0
                    }
                } catch {
  print(" ---------------------- changeToPa intChoice Couldn't read project -----------------------------")
                    return nil
                }
            }
        }
        return nil
    }

//************** change 3.0.3 add function ***
	// MARK: - home View Overlay - actions
//	//----------------------------------------------------------------------------------------------------------
//	@IBAction func frntDoorsAction(_ sender: Any) //************** change 3.0.6 remove function ***
//	{
////		if (sourceArray.count < 1) {return;}
//
//		self.vertCha ngeDoors.isSelected = false;
//		self.vertFr ntDoors.isSelected = true;
//
////		var index:Int = 0;
////
////		if let index0 = sourceArray.firstIndex(of: frntDoorSourceDdr)
////		{
////			index = index0;
////		} else {
////			if let index0 = sourceArray.firstIndex(of: frntDoorSourceSdr)
////			{
////				index = index0;
////			}
////		}
//////print("horzFrntDoorsAction index: ",index);
////
////		if (index > sourceArray.count-1) {index = 0}
////		doorSource = sourceArray[index]
////
////		self.hhvc?.collectionView?.reloadData();
////		self.vvvc?.collectionView?.reloadData();
////
////		let indiP = IndexPath(row: currentCombinedFrntDoor, section: 0)
////
////		self.hhvc?.collectionView?.scrollToItem(at: indiP, at: UICollectionView.ScrollPosition(rawValue: 0), animated: false);
////		self.vvvc?.collectionView?.scrollToItem(at: indiP, at: UICollectionView.ScrollPosition(rawValue: 0), animated: false);
//	}

//************** change 3.0.3 add function ***
//	//----------------------------------------------------------------------------------------------------------
//	@IBAction func changeDoorsAction(_ sender: Any) //************** change 3.0.6 remove function ***
//	{
////		if (sourceArray.count < 1) {return;}
//
//		self.vertChang eDoors.isSelected = true;
//		self.vertFrn tDoors.isSelected = false;
//
////		var index:Int = 0;
////
////		if let index0 = sourceArray.firstIndex(of: garDoorSourceDdr)
////		{
////			index = index0;
////		} else {
////			if let index0 = sourceArray.firstIndex(of: garDoorSourceSdr)
////			{
////				index = index0;
////			}
////		}
//////print("horzChangeDoorsAction index: ",index);
////
////		if (index > sourceArray.count-1) {index = 0}
////		doorSource = sourceArray[index]
////
////		self.hhvc?.collectionView?.reloadData();
////		self.vvvc?.collectionView?.reloadData();
////
////		let indiP = IndexPath(row: currentTwoCarGar, section: 0)  //********* combined with my version ***
////
////		self.hhvc?.collectionView?.scrollToItem(at: indiP, at: UICollectionView.ScrollPosition(rawValue: 0), animated: false);
////		self.vvvc?.collectionView?.scrollToItem(at: indiP, at: UICollectionView.ScrollPosition(rawValue: 0), animated: false);
//	}

//************** change 3.0.3 add function ***
    // MARK: -  seque methods
	//--------------------------------------------------------------------------------------------
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
		if segue.identifier == "horzVCseque"
		{
			hhvc = segue.destination as? VertHorzCollectVC
		}

		if segue.identifier == "vertVCseque"
		{
			vvvc = segue.destination as? VertHorzCollectVC
		}
    }
}


//-----------------------------------------------------
extension UIImage
{
    //-----------------------------------------------------
    func resizeImageWithsize(newSize: CGSize) -> UIImage
    {
        let horizontalRatio = newSize.width / size.width
        let verticalRatio = newSize.height / size.height
        
        let ratio = max(horizontalRatio, verticalRatio)
        let newSize = CGSize(width: size.width * ratio, height: size.height * ratio)
        UIGraphicsBeginImageContextWithOptions(newSize, true, 0)
        draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
//--------------------
extension HomeViewController:MFMailComposeViewControllerDelegate
{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    //----------------------
    func sendMail(agentDetails:AgentList)
    {
        
        let mailComposer = MFMailComposeViewController()
        
        mailComposer.mailComposeDelegate = self
        
        //Set to recipients
        mailComposer.setToRecipients([agentDetails.email]) //email
        //Set the subject
        mailComposer.setSubject("Order Number -\(agentDetails.orderNumber)")
        
        guard MFMailComposeViewController.canSendMail() else
        {
            //showMailServiceErrorAlert()
            return
        }
        
        mailComposer.setMessageBody("Order Number -\(agentDetails.orderNumber)", isHTML: true)
        mailComposer.modalPresentationCapturesStatusBarAppearance = true
        
        self.present(mailComposer, animated: true, completion: nil)
        
    }
}
//--------------------------------------------------
extension HomeViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    //--------------------------------------------------
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        currentImageName = ""
        //Start remove change 2.0.6
//        if let imageURL = info[.imageURL]
//        {
//            currentImageName = (imageURL as! URL).lastPathComponent
//        }
//        var asset: PHAsset?
//        asset = info[.phAsset] as? PHAsset
//        if let theAsset = asset
//        {
//            let assetResources = PHAssetResource.assetResources(for: theAsset)
//            let fileAsset = assetResources.filter({$0.type == .photo})
//
//            currentImageName = fileAsset.first?.originalFilename ?? ""
//
//            if assetResources.count > 1 && currentImageName.count > 0
//            {
//                let newwidth = CGFloat(asset?.pixelWidth ?? 0)
//                let newHeight = CGFloat(asset?.pixelHeight ?? 0)
//
//                var tempNameCurr = currentImageName.replacingOccurrences(of:".", with: "_cropped.")
//                tempNameCurr = tempNameCurr.changeExtensions(name:tempNameCurr)
//
//                if let image = self.checkImageExist(filename:tempNameCurr)
//                {
//                    if image.size.width != newwidth ||  image.size.height != newHeight
//                    {
//                        let alphaArray = ["a","b","c","d","e","f","g","h","i","j","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
//                        let randomAlpha = alphaArray.randomElement()
//
//                        let number = Int.random(in: 0 ..< 1000)
//
//                        currentImageName = tempNameCurr.replacingOccurrences(of:"_cropped", with: "_\(randomAlpha ?? "")\(number)")
//                    } else {
//                        currentImageName = tempNameCurr
//                    }
//                } else {
//                    currentImageName = tempNameCurr
//                }
//            }
//        }
//
//        if currentImageName == ""
//        {
//            let alphaArray = ["a","b","c","d","e","f","g","h","i","j","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
//            let randomAlpha = alphaArray.randomElement()
//
//            let number = Int.random(in: 0 ..< 1000)
//            let imageNameRandom = "Image_\(randomAlpha ?? "abc")\(number).jpg"
//            currentImageName = imageNameRandom
//        }
        //end remove change 2.0.6
        currentImageName = "".randomString() //************* change add 2.0.6 adding new name
        currentImageName = currentImageName.changeExtensions(name:currentImageName)
        //print("finished photo pick  currentIma geName:",currentImageName)
        
        //print("finished photo pick  clear The Paint")
        self.clearThePaint()
        self.clearOldProject()
        
        if picker.sourceType == .photoLibrary
        {
            //print("finished photo pick  picker.sourceType == .photoLibrary")
            guard let image = info[.originalImage] as? UIImage else { return;}
            
            addedImageFromCameraStr = "library"
            self.imageView.image = image.fixedOrientation()
            originalHomeImage = self.imageView.image
            targetMaskRect.size = originalHomeImage.size;
            //        self.oldImage = self.imageView.image!
            //        self.landImageView = imageView
            
            
            self.selectedReferenceItem = nil
            self.selectedReferenceItem = ReferenceItem.init(name: "Library")
            self.selectedReferenceItem?.image = self.imageView.image
            
            //*************** change 0.0.32 add  ***
            let size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height);
            //print("1 finished photo pick  size:",size)
            self.shallRotate(size)
            
            isTwoCar = false
            isOneCar = false
            
            
        } else {
            //print("finished photo pick  was not? was camera?")
            guard let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else { return;}
            pitch = 0.0;
            
            //print("8c finished photo pick why???? ......... :",currentImageName)
            
            self.imageView.image = image.fixedOrientation()
            originalHomeImage = self.imageView.image;
            targetMaskRect.size = originalHomeImage.size;
            
            let size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height);
            //print("2  finished photo pick  size:",size)
            self.shallRotate(size)
        }
        
        //print("finished photo pick  before fetchHo useObject")
        self.fetchHouseObject
            { (house0Object) in
                self.selectedImageRoll()  //********** change 0.0.40 add ***
        }
        camView.isHidden = true
        UserDefaults.standard.set(true, forKey: "removeCamView")
        
        picker.dismiss(animated: true)
    }
    
    //--------------------------------------------------
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        do
        {
            picker.dismiss(animated: true)
        }
    }
    
    //----------------------------------------------------------------------------------------------------------
    func selectedImageRoll()
    {
//print("  selectedIm ageRoll originalHomeImage:",originalHomeImage as Any)
//print("  selectedIm ageRoll currentImageName:",currentImageName as Any)
        //--------------- now save as a Project -------------------
        var targetSize:CGSize = CGSize(width:originalHomeImage.size.width*0.35, height:originalHomeImage.size.height*0.35);
        if (originalHomeImage.size.width < 2000.0) || (originalHomeImage.size.height < 2000.0)
        {
            targetSize = CGSize(width:originalHomeImage.size.width*0.75, height:originalHomeImage.size.height*0.75);
        }
        
        let project = Project.init()
        let projectThumb = ProjectThumb.init()
        let choiceThumb:ProjectThumb = ProjectThumb.init()
        
        project.projectID = projectThumb.projectID
        
        currProject = project;
        currProjectThumb = projectThumb;
        shouldAddToAlbum = false;
        
        
        currentButtonToDisplay = 0
        currentWellNum = 0
        defaultLineColor = defaultLineColor0;
        origStartColor = origStartColor0;
        maskedHouseImage = maskedHouseImage0;
        
        commingFromProjectList = false;
        
//        if let appDelegate0 = (UIApplication.shared.delegate as? AppDelegate)
//        {
            DispatchQueue.global().async
                {
//                    appDelegate0.backgroundTaskID = UIApplication.shared.beginBackgroundTask (withName: "Finish saving data")
//                    {
//                        // End the task if time expires.
//                        UIApplication.shared.endBackgroundTask(appDelegate0.backgroundTaskID!)
//                        appDelegate0.backgroundTaskID = UIBackgroundTaskIdentifier.invalid
//                    }
                    
                    autoreleasepool
                    {
                            var thumbImage:UIImage? = nil;
                            
                            UIGraphicsBeginImageContextWithOptions(targetSize, false, 1.0);
                            originalHomeImage.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
                            thumbImage = UIGraphicsGetImageFromCurrentImageContext();
                            UIGraphicsEndImageContext();
                            
                            project.thumbnailData = thumbImage?.jpegData(compressionQuality: 0.50)
                            projectThumb.thumbnailData = project.thumbnailData
                            choiceThumb.thumbnailData = project.thumbnailData;
                            thumbImage = nil;
                    }
                    //print(" saveProje ctList projectThu mb:",projectTh umb);
                    
                    project.imageData = originalHomeImage.jpegData(compressionQuality: 1.0)
                    project.imageName = currentImageName;
                    //    project.wasProces sed = false;
                    //print(" saveProje ctList project.imageData:",project.imageData as Any);
                    //print(" saveProje ctList originalHomeImage:",originalHomeImage as Any);
                    
                    if let house0Object = houseObject
                    {
                        let encoder = JSONEncoder()
                        project.house0 = try? encoder.encode(house0Object)
                    }
                    
                    project.currentWellNum = currentWellNum
                    project.currentButtonToDisplay = currentButtonToDisplay
                    
                    projectList00 = self.loadProjects()
                    projectList00.insert(projectThumb, at: 0)
                    self.selectedGalleryImage = 0;
                    
                    //print("3 selectedIm ageRoll projectList00.count:",projectList00.count)
                    
                    
                    curProjectChoiceList = self.loadThumbChoices(projectThumb.projectID) //********** change 1.1.4 change,  needs to be universal ***
                    curProjectChoiceList.append(choiceThumb)  //********** change 1.1.4 change,  needs to be universal ***
                    //print("selectedIm ageRoll thumbCho iceList:",thumbCho iceList.count);
                    
                    //---------------- save main project thumbnails to list ------------------
                    do
                    {
                        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                        {
                            let fileURL = dir.appendingPathComponent("projectThumbs.txt")
                            //print("selectedIm ageRoll fileURL:",fileURL)
                            let data = try NSKeyedArchiver.archivedData(withRootObject: projectList00, requiringSecureCoding: false)
                            try data.write(to: fileURL)
                            hasBeenSaved = true;
                            //print("selectedIm ageRoll    hasBe enSaved:",hasBeenSaved)
                        }
                        
                    } catch {
  print(" ---------------------- selectedEx ample projectThumbs    error:",error)
                    }
                    
                    
                    if let projectId = projectThumb.projectID
                    {
                        do
                        {
                            //---------------- save main project  ------------------
                            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                            {
                                let fileURL = dir.appendingPathComponent("project_"+projectId+".txt")
                                let currProjectURL = dir.appendingPathComponent("currProject.txt")
                                //print("selectedIm ageRoll fileURL:",fileURL)
                                NSKeyedArchiver.setClassName("Project", for: Project.self)
                                
                                let data = try NSKeyedArchiver.archivedData(withRootObject: project, requiringSecureCoding: false)
                                try data.write(to: fileURL)
                                try data.write(to: currProjectURL)
                                //print("selectedIm ageRoll project has Been Saved")
                            }
                            
                        } catch {
  print(" ---------------------- selectedEx ample    project_  error:",error)
                        }
                        
                  //                    project.imageData = nil;    //*************** change 1.0.1 remove these where ever they occur ***
                        //                    project.siding0 = nil;
                        //                    project.roof0 = nil;

                        
                        do
                        {
                            //---------------- save choices thumbnails to choices list  ------------------
                            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                            {
                                let fileURL = dir.appendingPathComponent("projectThumbs_"+projectId+".txt")
                                //print("selectedIm ageRoll DocumentDirectory fileURL:",fileURL)
                                let data = try NSKeyedArchiver.archivedData(withRootObject: curProjectChoiceList, requiringSecureCoding: false)  //********** change 1.1.4 change,  needs to be universal ***
                                try data.write(to: fileURL)
                                //print("selectedIm ageRoll    hasBe enSaved")
                            }
                            
                        } catch {
  print(" ---------------------- selectedEx ample projec tThumbs_   error:",error)
                        }
                    }
                    
                    if let thumbProjectId = choiceThumb.projectID
                    {
                        do
                        {
                            //---------------- save current choice as a project  ------------------
                            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                            {
                                let fileURL = dir.appendingPathComponent("projectChoices_"+thumbProjectId+".txt")
                                //print("selectedIm ageRoll DocumentDirectory fileURL:",fileURL)
                                NSKeyedArchiver.setClassName("Project", for: Project.self)
                                
                                let data = try NSKeyedArchiver.archivedData(withRootObject: project, requiringSecureCoding: false)
                                try data.write(to: fileURL)
                                //print("selectedIm ageRoll projectChoices_ has Been Saved")
                            }
                            
                        } catch {
                            print(" ---------------------- selectedEx ample    projec tChoices  error:",error)
                        }
                    }
                    
//                    UIApplication.shared.endBackgroundTask(appDelegate0.backgroundTaskID!)
//                    appDelegate0.backgroundTaskID = UIBackgroundTaskIdentifier.invalid
            }
     //   }
    }
    
    //--------------------------------------------------
    @objc func orientationChanged(notification: Notification)
    {
        let deviceOrientation = UIDevice.current.orientation
        var angle: Double = 0.0
        
        switch deviceOrientation
        {
        case .portrait:
            angle = 0
            roationRightType = false
            break
        case .portraitUpsideDown:
            angle = Double.pi
            roationRightType = false
            break
        case .landscapeLeft:
            angle = -Double.pi / 2
            roationRightType = false
            break
        case .landscapeRight:
            angle = Double.pi / 2
            roationRightType = true
            break
        default:
            break
        }
        
        rotationalAngle = angle
        //        if let angle = angle {
        //            let transform = CGAffineTransform(rotationAngle: CGFloat(angle))
        
        //            UIView.animate(withDuration: 0.3, animations: {
        //                transform
        //            })
        
        //        }
    }
    
    //-----------------------------------------------------
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer)
    {
        //This is to show prompt to the user that the image is saved to photo...will uncomment if needed
        /*if let error = error {
         // we got back an error!
         let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
         ac.addAction(UIAlertAction(title: "OK", style: .default))
         present(ac, animated: true)
         } else {
         let ac = UIAlertController(title: "Saved!", message: "image has been saved to your photos.", preferredStyle: .alert)
         ac.addAction(UIAlertAction(title: "OK", style: .default))
         present(ac, animated: true)
         }*/
    }
}



//-----------------------------------------------------
extension HomeViewController:ModifiCameraVCDelegate
{
    //    func sendCapt ureImag2(img: UIImage)    //***************** change 0.444.7 is this being used??? remove all if not ***
    //    {
    //
    //    }
    
    //-------------------------------------
    func sendCaptureImag()    //*************** change 1.0.2 replace this whole function ***
    {
    //print("HomeViewController send CaptureImag")

            camView.isHidden = true
            UserDefaults.standard.set(true, forKey: "removeCamView")

            let index = IndexPath.init(item: 0, section: 0)

    //print("send CaptureImag finished capture camera  clear The Paint")
            self.clearThePaint()
            self.clearOldProject()

            self.photoCollectionView.reloadData()
            self.photoCollectionView.scrollToItem(at: index, at: .left, animated: false)

            let status = UserDefaults.standard.bool(forKey: "removeCamView")
            if status
            {
                camView.isHidden = true
            }

            addedImageFromCameraStr = "Camera"
            self.selectedReferenceItem = nil
            self.selectedReferenceItem = ReferenceItem.init(name: "Camera")
            self.selectedReferenceItem?.image = self.imageView.image

    //        originalH omeImage = self.ima geView.image
    //        targetMa skRect.size = originalH omeImage.size;

            isTwoCar = false
            isOneCar = false

            self.photoView.isHidden = true
            self.setUpFonts()
            homeTableView.layer.borderWidth = 2.0
            homeTableView.layer.borderColor = UIColor.clear.cgColor

        //    self.tableViewTopConstrain.constant  = 0   //*************** change 1.0.2 remove ***
//            self.homeTableView.reloadData() //************ not required 2.0.9
            self.homeTableView.scroll(to: .top, animated: false)
        debugPrint("currentImageName from camera:",currentImageName)
    //print("5 sendCaptureImag  before fetchHo useObject")
            self.fetchHouseObject
            { (house0Object) in     //*************** change 1.0.2 change this block to include all below ***
                self.selectedImageRoll()

                DispatchQueue.main.asyncAfter(deadline: .now() + 0.05, execute:
                {
                    let size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height);
    //print("2  send CaptureImag  size:",size)
                    self.shallRotate(size)
                })
            }
        }
    
    //-------------------------------------
    func dismissCamaraVC()
    {
    }
}


//MARK:- UITableview Delegate and Datasource methods
//-------------------------------------
extension HomeViewController: UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegateFlowLayout
{
    //-------------------------------------
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if btnGarageClicked
        {
            
            return 1
            
        }else if btnDoorClicked{
            
            return 1
            
        }else if btnPaintListClicked{
            
            return 1
        }

        return 0
    }
    
    //-------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
//print("numberOfRowsInSection  btnGar ageClicked:",btnGar ageClicked)
//print("numberOfRowsInSection  btnDoorClicked:",btnDoorClicked)
//print("numberOfRowsInSection  btnPaintListClicked:",btnPaintListClicked)


        if btnGarageClicked
        {
            if btnListClicked
            {
                if !isTwoCar && !isOneCar && !isDoor
                {
                    return 0
                }
                
                if isTwoCar
                {
                    return self.homeObj?.garages.count ?? 0
                } else {
                    return self.homeObj?.singleGarage.count ?? 0
                }
            } else {
                return 1
            }
            
        } else if btnDoorClicked {
            
            if btnListClicked
            {
                if !isTwoCar && !isOneCar && !isDoor
                {
                    return 0
                }
                if (hasOnlyGarageDoors)//***** putback 2.1.4 some times its getting true somewhere  //********** change 1.1.4 remove if block ***  btnDoorClicked is never true for hasOnlyGarageDoors?   shouldn't need to do this, as eventually it will have doors here.
                {
                    return 0
                }
                if isDouble
                {
                    return doorsDoubleArray.count
                } else {
                    return doorsArray.count
                }
                
            } else {
                if (hasOnlyGarageDoors)  //***** putback 2.1.4 some times its getting true somewhere//********** change 1.1.4 remove if block ***  btnDoorClicked is never true for hasOnlyGarageDoors?   shouldn't need to do this, as eventually it will have doors here.
                {
                    return 0
                }
                return 1
            }
            
		} else if (btnPaintListClicked && !hasOnlyGarageDoors) {   //********** change 1.0.0 add if block ***
                if btnListClicked
                {
//                    let curProjectCh oiceList:[ProjectThumb] = self.loadThu mbChoices(currProjectThumb?.projectID)  //********** change 1.1.4 remove,  needs to be universal, changed only when project changes ***
                    return curProjectChoiceList.count-1
                } else {
                    return 1
                }
        } else {
            return 0
        }
    }
    
    
    //--------------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if btnGarageClicked
        {
            if self.btnListClicked
            {
                return 75
            } else {
                if !isLandScape
                {
					if wasIpad //************** change 3.0.3 add if block ***
					{
						self.scrollViewHeightConstarin.constant = self.view.frame.size.height - iPadForHome;
						self.tabsViewTopConstrain.constant = self.view.frame.size.height - iPadForHome;
						self.garageCellHeightConstraint.constant = iPadForHome;
						self.garDrFrDrView.frame.origin = CGPoint(x: 0.0, y: self.view.frame.size.height - iPadForHome)
						self.garDrFrDrView.frame.size = CGSize(width: self.view.frame.size.width, height: self.garDrFrDrView.frame.size.height)

					} else {
						self.scrollViewHeightConstarin.constant = iPhoneForHome
						self.tabsViewTopConstrain.constant = iPhoneForHome //************** change 3.0.3 change position ***
						self.garageCellHeightConstraint.constant = self.view.frame.size.height - iPhoneForHome;
						self.garDrFrDrView.frame.origin = CGPoint(x: 0.0, y: iPhoneForHome)
						self.garDrFrDrView.frame.size = CGSize(width: self.view.frame.size.width, height: self.garDrFrDrView.frame.size.height)
					}
                } else { //************** change 3.0.3 add else ***
					self.scrollViewHeightConstarin.constant = self.view.frame.size.height;
					self.tabsViewTopConstrain.constant = 0.0
					self.garageCellHeightConstraint.constant = 0.0;
					self.garDrFrDrView.frame.origin = CGPoint(x: 0.0, y: self.view.frame.size.height)
				}
                return tableView.frame.size.height
            }
        } else if btnDoorClicked {
            if btnDoorClicked && btnGarageClicked == false && btnListClicked == false
            {
                if !isLandScape
                {
					if wasIpad //************** change 3.0.3 add if block ***
					{
						self.scrollViewHeightConstarin.constant = self.view.frame.size.height - iPadForHome;
						self.tabsViewTopConstrain.constant = self.view.frame.size.height - iPadForHome;
						self.garageCellHeightConstraint.constant = iPadForHome;
						self.garDrFrDrView.frame.origin = CGPoint(x: 0.0, y: self.view.frame.size.height - iPadForHome)
						self.garDrFrDrView.frame.size = CGSize(width: self.view.frame.size.width, height: self.garDrFrDrView.frame.size.height)

					} else {
						self.scrollViewHeightConstarin.constant = iPhoneForHome
						self.tabsViewTopConstrain.constant = iPhoneForHome //************** change 3.0.3 change position ***
						self.garageCellHeightConstraint.constant = self.view.frame.size.height - iPhoneForHome;
						self.garDrFrDrView.frame.origin = CGPoint(x: 0.0, y: iPhoneForHome)
						self.garDrFrDrView.frame.size = CGSize(width: self.view.frame.size.width, height: self.garDrFrDrView.frame.size.height)
					}
                } else { //************** change 3.0.3 add else ***
					self.scrollViewHeightConstarin.constant = self.view.frame.size.height;
					self.tabsViewTopConstrain.constant = 0.0
					self.garageCellHeightConstraint.constant = 0.0;
					self.garDrFrDrView.frame.origin = CGPoint(x: 0.0, y: self.view.frame.size.height)
				}
                return tableView.frame.size.height
            } else {
                return 75
            }
		} else if (btnPaintListClicked && !hasOnlyGarageDoors) {   //********** change 1.0.0 add if block ***

            if self.btnListClicked
            {
                return 70
            } else {
                if !isLandScape
                {
					if wasIpad //************** change 3.0.3 add if block ***
					{
						self.scrollViewHeightConstarin.constant = self.view.frame.size.height - iPadForHome;
						self.tabsViewTopConstrain.constant = self.view.frame.size.height - iPadForHome;
						self.garageCellHeightConstraint.constant = iPadForHome;
						self.garDrFrDrView.frame.origin = CGPoint(x: 0.0, y: self.view.frame.size.height - iPadForHome)
						self.garDrFrDrView.frame.size = CGSize(width: self.view.frame.size.width, height: self.garDrFrDrView.frame.size.height)
					} else {
						self.scrollViewHeightConstarin.constant = iPhoneForHome
						self.tabsViewTopConstrain.constant = iPhoneForHome //************** change 3.0.3 change position ***
						self.garageCellHeightConstraint.constant = self.view.frame.size.height - iPhoneForHome;
						self.garDrFrDrView.frame.origin = CGPoint(x: 0.0, y: iPhoneForHome)
						self.garDrFrDrView.frame.size = CGSize(width: self.view.frame.size.width, height: self.garDrFrDrView.frame.size.height)
					}
                } else { //************** change 3.0.3 add else ***
					self.scrollViewHeightConstarin.constant = self.view.frame.size.height;
					self.tabsViewTopConstrain.constant = 0.0
					self.garageCellHeightConstraint.constant = 0.0;
					self.garDrFrDrView.frame.origin = CGPoint(x: 0.0, y: self.view.frame.size.height)
				}
                return tableView.frame.size.height
            }

        } else {
            return 0
        }
    }
    
    //--------------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        //print("tableView btnGarag eClicked:",btnGa rageClicked)
        if btnGarageClicked
        {
            //print("tableView self.btnLis tClicked:",self.btnListClicked)
            if self.btnListClicked
            {
                guard let garageFilterCell = tableView.dequeueReusableCell(withIdentifier: "GarageFilterCell") as? GarageFilterTableViewCell else
                {
                    return UITableViewCell()
                }
                //    garageFilterCell.imgView.frame.size.width = 120.0;
                
                if isTwoCar
                {
                    if let garage = self.homeObj?.garages[indexPath.row] //*************** change 0.0.31 change ***
                    {
                        
                        //print("UIImage(named: loadingTemp")
                        //print("garage Scroll end garage.mask_url:",garage.mask_url as Any)
                        
                        garageFilterCell.imgView.sd_setImage(with: URL(string: garage.image), placeholderImage: UIImage(named: "loadingTemp"))
                        garageFilterCell.imgView.clipsToBounds = true
                        garageFilterCell.cardPaintBtn.isHidden = true //*************** change 2.0.7 add if block
                        garageFilterCell.cardPaintBtnWidth.constant = 45 //*************** change 2.0.7 add if block
                        if isLandScape
                        {
                            garageFilterCell.titleLabel.text = "" //garage?.title
                            garageFilterCell.subTitleLabel.text = "" // garage?.subTitle
                            garageFilterCell.imageWidth.constant = 140 //homeTableView.frame.size.width-30 //**********Change 2.2.0
                            //print("tableView garageFilterCell.im ageWidth.constant:",garageFilterCell.imageWidth.constant)
                            garageFilterCell.backgroundColor = .clear
                            garageFilterCell.imgbgView.backgroundColor = .clear
                           
                        } else {
                            garageFilterCell.imageWidth.constant = 100
                            //print("3 tableView garageFilterCell.ima geWidth.constant:",garageFilterCell.imageWidth.constant)
                            garageFilterCell.titleLabel.text = garage.title
                            garageFilterCell.subTitleLabel.text = String(format:"%@\n%@", garage.subTitle, garage.price)
                            garageFilterCell.imgbgView.backgroundColor = .white
                            if hasOnlyGarageDoors //**************** hide dealer Name Lakshmi - 2.0.2
                            {
                                garageFilterCell.subTitleLabel.text = ""
                                if (garage.isPaintable == 1)
                                {
                                    garageFilterCell.cardPaintBtnWidth.constant = 95
                                    garageFilterCell.cardPaintBtn.isHidden = false    //*************** change 2.0.7 add
                                }
                                
                            }
                        }
                        
                        garageFilterCell.titleLabel.font = UIFont(name: "Lato-Bold", size: 18)
                        garageFilterCell.subTitleLabel.font = UIFont(name: "Lato-Bold", size: 14)
                    }
                } else {
                    
                    if let singleGarage = self.homeObj?.singleGarage[indexPath.row] //*************** change 0.0.31 change ***
                    {
                        garageFilterCell.imgView.sd_setImage(with: URL(string: singleGarage.image), placeholderImage: UIImage(named: "loadingTemp"))
                        garageFilterCell.imgView.clipsToBounds = true
                        garageFilterCell.cardPaintBtn.isHidden = true //*************** change 2.0.7 add if block
                        garageFilterCell.cardPaintBtnWidth.constant = 45 //*************** change 2.0.7 add if block
                        if isLandScape
                        {
                            garageFilterCell.titleLabel.text = "   " //singleGa rage?.title
                            garageFilterCell.subTitleLabel.text = "   " //singleGa rage?.subTitle
                            garageFilterCell.imageWidth.constant = 120 //homeTableView.frame.size.width-30
                            //print("2 tableView garageFilterCell.imag eWidth.constant:",garageFilterCell.imageWidth.constant)
                            garageFilterCell.backgroundColor = .clear
                            garageFilterCell.imgbgView.backgroundColor = .clear
                           
                        } else {
                            garageFilterCell.imageWidth.constant = 100
                            //print("3 tableView garageFilterCell.imageW idth.constant:",garageFilterCell.imageWidth.constant)
                            garageFilterCell.titleLabel.text = singleGarage.title
                            garageFilterCell.subTitleLabel.text = String(format:"%@\n%@", singleGarage.subTitle, singleGarage.price)
                            garageFilterCell.imgbgView.backgroundColor = .white
                            if hasOnlyGarageDoors  //**************** hide dealer Name Lakshmi - 2.0.2
                            {
                                garageFilterCell.subTitleLabel.text = ""
                                if (singleGarage.isPaintable == 1) //*************** change 2.0.7 add if block
                                {
                                    garageFilterCell.cardPaintBtnWidth.constant = 95
                                    garageFilterCell.cardPaintBtn.isHidden = false
                                }
                            }
                        }
                    }
                    
                    garageFilterCell.titleLabel.font = UIFont(name: "Lato-Bold", size: 18)
                    garageFilterCell.subTitleLabel.font = UIFont(name: "Lato-Bold", size: 14)
                }
                
                garageFilterCell.detailBtn.tag = indexPath.row
                garageFilterCell.cardPaintBtn.tag = indexPath.row //*************** change 2.0.10
                garageFilterCell.detailBtn.addTarget(self, action: #selector(garageListClickAction(sender:)), for:.touchUpInside)
                garageFilterCell.cardPaintBtn.addTarget(self, action: #selector(changeListSelectionForPaint(sender:)), for:.touchUpInside) //*************** change 2.0.10 add
                 return garageFilterCell
            } else {
                //print("3 tableView not list")
                guard let garageCell = tableView.dequeueReusableCell(withIdentifier: "GarageCell") as? GarageTableViewCell else
                {
                    return UITableViewCell()
                }
                
                let garage:[Garage] = self.homeObj?.garages ?? [Garage]() //(self.homeObj?.garages[selectedimage])!
                let singleGarage:[SingleGarageObj] = self.homeObj?.singleGarage ?? [SingleGarageObj]()
                
                garageCell.isGarageSelected = true
                garageCell.isPaintSelected = false
                garageCell.delegate = self
                
                if isTwoCar
                {
                    garageCell.garage = garage
                    garageCell.singleGarage = [SingleGarageObj]()
                } else {  // *********** change 2.1.8 else condition
                    garageCell.garage =  [Garage]()
                    
                    if !isTwoCar && !isOneCar && !isDoor
                    {
                        garageCell.singleGarage = [SingleGarageObj]()
                    } else {
                        garageCell.singleGarage = singleGarage
                    }
                }
                
                garageCell.pagingView.reloadData()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute:
                    {
                        let indexPathScroll = IndexPath(row: self.garageScrollIndex, section:0 )
                        if garageCell.pagingView.numberOfItems(inSection:0) > self.garageScrollIndex
                        {
                            garageCell.pagingView.scrollToItem(at:indexPathScroll, at:.centeredHorizontally, animated: true)
                            garageCell.selectedItemIndex = self.garageScrollIndex
                        }
                        
                        if  self.garageScrollIndex > 0
                        {
                            self.garageScrollend(index:self.garageScrollIndex)
                        }
                })
                
                return garageCell
            }
            
        } else if btnDoorClicked && btnGarageClicked == false && btnListClicked == false {
            //print("22 tableView door clicked")
            
            guard let garageCell = tableView.dequeueReusableCell(withIdentifier: "GarageCell") as? GarageTableViewCell else
            {
                return UITableViewCell()
            }
            
            var doors:[Door] = self.doorsArray
            
            if isDouble
            {
                doors = self.doorsDoubleArray
            }
           
            garageCell.isGarageSelected = false
            garageCell.isPaintSelected = false
            garageCell.delegate = self
            
            if !isTwoCar && !isOneCar && !isDoor
            {
                doors = [Door]()
            }
            garageCell.doors = doors
            garageCell.pagingView.reloadData()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute:
                {
                    let indexPathScroll = IndexPath(row: self.doorScrollIndex, section:0 )
                    if garageCell.pagingView.numberOfItems(inSection:0) > self.doorScrollIndex
                    {
                        garageCell.pagingView.scrollToItem(at:indexPathScroll, at:.centeredHorizontally, animated: true)
                        garageCell.selectedItemIndex = self.doorScrollIndex
                    }
                    
                    if  self.doorScrollIndex > 0
                    {
                        self.doorScrollend(index:self.doorScrollIndex)
                    }
            })
            
            return garageCell
            
            } else if (btnPaintListClicked && !hasOnlyGarageDoors) {   //********** change 1.0.0 add if block ***
            if btnListClicked
            {
                //print("32 tableView nothing clicked")
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "PaintLandScapeCell") as? PaintLandScapeCell else
                {
                    return UITableViewCell()
                }
                
 //********** change 1.1.4 not sure this is needed, as the project should already have been read in and is universal, "currProject",  did you add this? ***
 //..... also will this crash as you are adding "+1" to a list that should be a fixed number?   leaving for now, as I don't know what this is doing here.
                let proj = getMaskColors(index:indexPath.item+1)


//print("  ")
//print("homeView proj",proj as Any)
//print("proj?.mask0:",proj?.mask0 as Any)
//print("proj?.mask1:",proj?.mask1 as Any)
//print("proj?.mask2:",proj?.mask2 as Any)

                cell.color2View.isHidden = true
                cell.color3View.isHidden = true
                
                cell.color1Lbl.isHidden = true
                
                if proj?.mask0 != nil
                {
                    cell.maskView1?.setImageColor(color:UIColor.color(data:proj?.defaultLineColor0d))
                    
                }
                if proj?.mask1 != nil
                {
                    cell.maskView2?.setImageColor(color:UIColor.color(data:proj?.defaultLineColor1d))
                    cell.color1Lbl.isHidden = false
                    
                    cell.color2View.isHidden = false
                }
                
                if proj?.mask2 != nil
                {
                    cell.maskView3?.setImageColor(color:UIColor.color(data:proj?.defaultLineColor2d))
                    cell.color3View.isHidden = false
                }
                return cell;
            } else {
                guard let garageCell = tableView.dequeueReusableCell(withIdentifier: "GarageCell") as? GarageTableViewCell else
                {
                    return UITableViewCell()
                }
                
            //    let curProjectCh oiceList:[ProjectThumb] = self.loadThu mbChoices(currProjectThumb?.projectID) //********** change 1.1.4 remove,  needs to be universal ***
            //    garageCell.proje ctChoiceList = curProje ctChoiceList  //********** change 1.1.4 remove ***
                garageCell.isGarageSelected = false
                garageCell.isPaintSelected = true
                
                garageCell.delegate = self
                garageCell.pagingView.reloadData()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute:
                    {
                        let indexPathScroll = IndexPath(row: self.paintScrollIndex, section:0 )
                        if garageCell.pagingView.numberOfItems(inSection:0) > self.paintScrollIndex
                        {
                            garageCell.pagingView.scrollToItem(at:indexPathScroll, at:.centeredHorizontally, animated: true)
                            garageCell.selectedItemIndex = self.paintScrollIndex
                        }
                        
                        if  self.paintScrollIndex >= 0
                        {
                            self.paintScrollend(index:self.paintScrollIndex)
                        }
                })
                return garageCell
            }
        } else{
//print("32 tableView nothing clicked")
            guard let doorFilterCell = tableView.dequeueReusableCell(withIdentifier: "DoorFilterCell") as? DoorFilterTableViewCell else
            {
                return UITableViewCell()
            }
            
            let garage:Door =  isDouble ?  doorsDoubleArray[indexPath.row] : doorsArray[indexPath.row]
            
            doorFilterCell.imgView.sd_setImage(with: URL(string: garage.image), placeholderImage: UIImage(named: "loadingTemp"))
            doorFilterCell.imgView.clipsToBounds = true
            
            if isLandScape
            {
                doorFilterCell.backgroundColor = .clear
                doorFilterCell.imgbgView.backgroundColor = .clear
                doorFilterCell.titleLabel.text = "   " //garage.title
                doorFilterCell.subTitleLabel.text = "   " //garage.subTitle
                doorFilterCell.imageWidth.constant = 120 //homeTableView.frame.size.width-30
//print("32 tableView doorFilterCell.ima geWidth.constant:",doorFilterCell.imageWidth.constant)
            } else {
                doorFilterCell.imageWidth.constant = 100
//print("32 tableView doorFilterCell.ima geWidth.constant:",doorFilterCell.imageWidth.constant)
                doorFilterCell.titleLabel.text = garage.title
                doorFilterCell.subTitleLabel.text = String(format:"%@\n%@", garage.subTitle, garage.price)
                doorFilterCell.imgbgView.backgroundColor = .white

                if hasOnlyGarageDoors  //**************** hide dealer Name Lakshmi - 2.0.2
                {
                    doorFilterCell.subTitleLabel.text = ""
                }
            }

            doorFilterCell.titleLabel.font = UIFont(name: "Lato-Bold", size: 18)
            doorFilterCell.subTitleLabel.font = UIFont(name: "Lato-Bold", size: 14)
            doorFilterCell.detailBtn.addTarget(self, action: #selector(doorListClickAction(sender:)), for:.touchUpInside)
            doorFilterCell.detailBtn.tag = indexPath.row
            return doorFilterCell;
        }
    }
    
    //--------------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        //print("tableView btnGarag eClicked,self.btnListClicked,self.garageScr ollIndex:",btnGarag eClicked,self.btnListClicked,self.garageScrollIndex)

//print("tableView didSelectRowAt indexPath:",indexPath)
        
        if btnGarageClicked
        {
            if self.btnListClicked
            {
                self.garageMaskView.image = nil;  //*************** change 0.0.30 add ***
                maskedDoorImage  = nil;   //*************** change 0.0.30 add ***
//                maskedDo orImage1 = nil;  //*************** change 1.1.0 remove ***

                if let paintCont = currentPaintImageController
                {
                    paintCont.scrollView.garageMaskView.image = nil;  //*************** change 0.0.30 add ***
                }
//print("tableView didSelectRowAt paintCont.scrollView.garageMa skView.image = nil")

                
                //   self.btnListClicked = false
                //Analytics.logEvent("Carousel_select_garage_product", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
                
                selectedimage = indexPath.row
                self.garageScrollIndex = indexPath.row+1
                //print("tableView self.garageScr ollIndex:",self.garageScrollIndex)
                guard let s = self.selectedReferenceItem else { return }
                if isTwoCar
                {
                    s.replacementGarageNames = [self.homeObj?.garages[indexPath.row].image] as? [String]
                    guard let rep = s.replacementGarageNames else { return }
                    
                    //*************zipcode Changes****************  //******** change 0.1.0 ***
                    if let garage = self.homeObj?.garages[indexPath.row]
                    {
                        self.cur2Garage = garage;
                    }
                    
                    garageCatID = (self.homeObj?.garages[indexPath.row].catalogID)!
                    garageCatName = (self.homeObj?.garages[indexPath.row].title)!
                    let currentPage = rep.count - 1
//print("1 didSelectRow garimageID:",garimageID as Any)
                    garimageID = ""
                    
                    //                    if s.hous eObject == nil
                    //                    {
                    //                        return
                    //                    }
                    
                    if let house0Object = houseObject
                    {
                        for resp in house0Object.response
                        {
                            for respItem in resp
                            {
                                if (respItem.labels.count > 0)   //************ change 0.0.17 fix for crash ***
                                {
                                    let lbl = respItem.labels[0]
                                    if lbl.contains("two-car")
                                    {
                                        let imgId = respItem.imageID_db
                                        if garimageID.count > 0
                                        {
//print("2 didSelectRow garimageID:",garimageID as Any)
                                            garimageID = garimageID+"||"+imgId
                                        } else {
//print("3 didSelectRow garimageID:",garimageID as Any)
                                            garimageID = imgId
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    checkFavStatus(indexs: selectedimage)
                    //print("0 tableView didselect replac eObjectAt")
                    self.replaceObjectAt(index: currentPage,type: "garage")
                } else {
                    s.replacementGarageNames = [self.homeObj?.singleGarage[indexPath.row].image] as? [String]
                    garageCatID = (self.homeObj?.singleGarage[indexPath.row].catalogID)!
                    garageCatName = (self.homeObj?.singleGarage[indexPath.row].catalogID)!
                    guard let rep = s.replacementGarageNames else { return }
//print("4 didSelectRow garimageID:",garimageID as Any)
                    garimageID = ""
                    
                    //                    if s.house Object == nil
                    //                    {
                    //                        return
                    //                    }
                    
                    //*************zipcode Changes****************  //******** change 0.1.0 ***
                    if let garage = self.homeObj?.singleGarage[indexPath.row]
                    {
                        self.cur1Garage = garage;
                    }
                    
                    if let house0Object = houseObject
                    {
                        for resp in house0Object.response
                        {
                            for respItem in resp
                            {
                                if (respItem.labels.count > 0)
                                {
                                    let lbl = respItem.labels[0]
                                    if lbl.contains("one-car") || lbl.contains("two-car")
                                    {
                                        let imgId = respItem.imageID_db
                                        if garimageID.count > 0
                                        {
                                            // for single garage to create favorite creating all combination of imageID and catID
//print("5 didSelectRow garimageID:",garimageID as Any)
                                            garimageID = garimageID+"||"+imgId
                                        } else {
//print("6 didSelectRow garimageID:",garimageID as Any)
                                            garimageID = imgId
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    let currentPage = rep.count - 1
                    //self.colorObjectAt(index: currentPage, color: .clear)
                    checkFavStatus(indexs: selectedimage)
                    //print("1 tableView didselect replac eObjectAt")
                    self.replaceObjectAt(index: currentPage,type: "garage")
                }
                
            } else {
                //************** remove change 2.0.8
//                let storyboard = UIStoryboard(name: "Home", bundle: nil)
//                let garageDetailVC = storyboard.instantiateViewController(withIdentifier: "GarageDetailVC") as! GarageDetailViewController
//                garageDetailVC.view.backgroundColor = UIColor(red: 75.0 / 255.0, green: 75.0 / 255.0, blue: 75.0 / 255.0, alpha: 0.8)
//                garageDetailVC.providesPresentationContextTransitionStyle = true
//                garageDetailVC.definesPresentationContext = true
//                garageDetailVC.modalPresentationStyle = .overCurrentContext
//
//                present(garageDetailVC, animated: true, completion: nil)
            }
            } else if (btnPaintListClicked && !hasOnlyGarageDoors) {   //********** change 1.0.0 add if block ***
            if self.btnListClicked
            {
                self.paintScrollend(index:indexPath.row+1)
            }
        } else {
            
            if self.btnListClicked
            {
                self.frontDrMaskView.image = nil;  //*************** change 0.0.30 add ***
                maskedFrontDrImage  = nil;   //*************** change 0.0.30 add ***
//                maskedFrontDrImage1 = nil;   //*************** change 1.1.0 remove ***
                
                if let paintCont = currentPaintImageController
                {
                    paintCont.scrollView.frontDrMaskView.image = nil;  //*************** change 0.0.30 add ***
                }
                
                
                //self.btnListClicked = false
                //Analytics.logEvent("Carousel_select_door_product", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
                selectedimage = indexPath.row
                doorScrollIndex = indexPath.row+1
                //self.selectedRef erenceItem = Referenc eItem.init(name: "Camera")
                //self.selectedRef erenceItem?.image = self.imageV iew.image
                guard let s = self.selectedReferenceItem else { return }
                let garage:Door =  isDouble ?  doorsDoubleArray[indexPath.row] : doorsArray[indexPath.row]
                
                s.replacementGarageNames = [garage.image]
                doorCatID = garage.catalogID
                
                //                if s.hous eObject == nil
                //                {
                //                    return
                //                }
                
                //*************zipcode Changes****************  //******** change 0.1.0 ***
                if (isDouble)
                {
                    self.cur2Door = garage;
                } else {
                    self.cur1Door = garage;
                }
                
                if let house0Object = houseObject
                {
                    for resp in house0Object.response
                    {
                        for respItem in resp
                        {
                            if (respItem.labels.count > 0)
                            {
                                let lbl = respItem.labels[0]
                                if lbl.contains("door")
                                {
                                    doorimageID = respItem.imageID_db
                                }
                            }
                        }
                    }
                }
                guard let rep = s.replacementGarageNames else { return }
                let currentPage = rep.count - 1
                
                checkFavStatus(indexs: selectedimage)
                //print("2 tableView replac eObjectAt")
                self.replaceObjectAt(index: currentPage,type: "door")
                
            } else {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let doorsDetailVC = storyboard.instantiateViewController(withIdentifier: "DoorsDetailVC") as! DoorsDetailViewController
                doorsDetailVC.view.backgroundColor = UIColor(red: 75.0 / 255.0, green: 75.0 / 255.0, blue: 75.0 / 255.0, alpha: 0.8)
                doorsDetailVC.providesPresentationContextTransitionStyle = true
                doorsDetailVC.definesPresentationContext = true
                doorsDetailVC.modalPresentationStyle = .overCurrentContext
                present(doorsDetailVC, animated: true, completion: nil)
                
            }
        }
    }
    
    //------------------------------------------------------------
    @objc func garageListClickAction(sender:UIButton)
    {
        self.garageButtonActionOnSelection(sender:sender.tag)
    }
    
    //------------------------------------------------------------
    @objc func doorListClickAction(sender:UIButton)
    {
        self.doorButtonActionOnSelection(sender:sender.tag)
    }
    
    //************** change 1.1.0 fully replace *** put back in
    //------------------------------------------------------------
    @objc func paintActionOnSelection(sender: Any?)  //*************** change 1.1.4 change to Any? optional ***
    {
//        let editTutorial =  UserDefaults.standard.bool(forKey: "paintTutorial")
//        if editTutorial
//        {
            let storyboard = UIStoryboard(name: "PaintImage", bundle: nil)

            self.garageMaskScrollIndex = self.garageScrollIndex
            self.doorMaskScrollIndex = self.doorScrollIndex
            self.paintMaskScrollIndex = self.paintScrollIndex

            let paintViewController = storyboard.instantiateViewController(withIdentifier: "PaintViewController") as! PaintViewController

            paintViewController.maskOfDoor = true;
            paintViewController.maskOfFrontDoor = false;
            paintViewController.cameraImageSelected = ""
            paintViewController.gallIndex = self.garageMaskScrollIndex
//print("paintActionO nSelection paintViewController.gallIndex:",paintViewController.gallIndex)

            paintViewController.hidesBottomBarWhenPushed = true
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
            self.navigationController?.pushViewController(paintViewController, animated: true)

            AppUtility.lockOrientation(.all)
                    UserDefaults.standard.set(true, forKey: "paintTutorial")

//        } else {
//            AppUtility.lockOrientation(.portrait)
//            comingFromPaintTutorial = true
//
//            UserDefaults.standard.set(true, forKey: "paintTutorial")
//            UserDefaults.standard.synchronize()
//
//            let storyboard = UIStoryboard(name: "CustomTabBar", bundle: nil)
//            let onboradingVC = storyboard.instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
//            onboradingVC.isPaintIntro = true
//            onboradingVC.isPaintTour = false
//
//            onboradingVC.maskOfDoor = true
//            onboradingVC.maskOfFrontDoor = false
//
//            onboradingVC.hidesBottomBarWhenPushed = true  //************ change 2.1.0
//            onboradingVC.modalPresentationStyle = .overFullScreen  //************ change 2.1.0
//            onboradingVC.modalPresentationCapturesStatusBarAppearance = true  //************ change 2.1.0
//            self.present(onboradingVC, animated: true, completion: nil)  //************ change 2.1.0
//           // self.navigationController?.pushViewController(onboradingVC, animated: true)
//        }
    }
    
    //************** change 1.1.0 fully replace *** put back in
    //------------------------------------------------------------
    func paintFDActionOnSelection(sender: Any?) //*************** change 1.1.4 change to Any? optional ***
    {
//print("paint FDAction OnSelection sender:",sender as Any)
//        let editTutorial =  UserDefaults.standard.bool(forKey: "paintTutorial")
//        if editTutorial
//        {
            let storyboard = UIStoryboard(name: "PaintImage", bundle: nil)

            self.garageMaskScrollIndex = self.garageScrollIndex
            self.doorMaskScrollIndex = self.doorScrollIndex
            self.paintMaskScrollIndex = self.paintScrollIndex

            let paintViewController = storyboard.instantiateViewController(withIdentifier: "PaintViewController") as! PaintViewController
            paintViewController.maskOfDoor = false;
            paintViewController.maskOfFrontDoor = true;
            paintViewController.cameraImageSelected = ""
            paintViewController.gallIndex = self.doorMaskScrollIndex

            paintViewController.hidesBottomBarWhenPushed = true
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
            self.navigationController?.pushViewController(paintViewController, animated: true)

            AppUtility.lockOrientation(.all)
                   UserDefaults.standard.set(true, forKey: "paintTutorial")

//        } else {
//            AppUtility.lockOrientation(.portrait)
//            comingFromPaintTutorial = true
//
//            UserDefaults.standard.set(true, forKey: "paintTutorial")
//            UserDefaults.standard.synchronize()
//
//            let storyboard = UIStoryboard(name: "CustomTabBar", bundle: nil)
//            let onboradingVC = storyboard.instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
//            onboradingVC.isPaintIntro = true
//            onboradingVC.isPaintTour = false
//
//            onboradingVC.maskOfDoor = false    //************** change 1.1.0 add ***
//            onboradingVC.maskOfFrontDoor = true  //************** change 1.1.0 add ***
//
//            onboradingVC.hidesBottomBarWhenPushed = true  //************ change 2.1.0
//            onboradingVC.modalPresentationStyle = .overFullScreen  //************ change 2.1.0
//            onboradingVC.modalPresentationCapturesStatusBarAppearance = true  //************ change 2.1.0
//            self.present(onboradingVC, animated: true, completion: nil)  //************ change 2.1.0
//           // self.navigationController?.pushViewController(onboradingVC, animated: true) //************ change 2.1.0
//        }
    }
    //***************change 2.0.10 add function
    @objc func changeListSelectionForPaint(sender:UIButton)
    {
        let selectIndex = sender.tag
        
        self.garageScrollend(index: selectIndex+1)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute:
           {
           self.paintActionOnSelection(sender:selectIndex)
           })


    }
    //------------------------------------------------------------
    func garageButtonActionOnSelection(sender: Int)
    {
        //print("home garageButtonActi onOnSelection  sender:",sender)
        guard  sender >= 0 else
        {
            // self.colorObjectAt(index: 0, color: .clear)
            return
        }
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let garageDetailVC = storyboard.instantiateViewController(withIdentifier: "GarageDetailVC") as! GarageDetailViewController
        garageDetailVC.view.backgroundColor = UIColor(red: 75.0 / 255.0, green: 75.0 / 255.0, blue: 75.0 / 255.0, alpha: 0.8)
        
        if isTwoCar
        {
            garageDetailVC.descriptionLabel.text = self.homeObj?.garages[sender].desc
            garageDetailVC.selectedImgView.sd_setImage(with: URL(string: (self.homeObj?.garages[sender].image)!), placeholderImage: UIImage(named: ""))
            garageDetailVC.titleLabel.text = self.homeObj?.garages[sender].title
            
            garageDetailVC.subTitleLabel.text = self.homeObj?.garages[sender].subTitle
           
        } else {
            garageDetailVC.descriptionLabel.text = self.homeObj?.singleGarage[sender].desc
            garageDetailVC.selectedImgView.sd_setImage(with: URL(string: (self.homeObj?.singleGarage[sender].image)!), placeholderImage: UIImage(named: ""))
            garageDetailVC.titleLabel.text = self.homeObj?.singleGarage[sender].title
            garageDetailVC.subTitleLabel.text = self.homeObj?.singleGarage[sender].subTitle
            
        }
        if hasOnlyGarageDoors
        {
            garageDetailVC.subTitleLabel.text = ""
        }
        garageDetailVC.providesPresentationContextTransitionStyle = true
        garageDetailVC.definesPresentationContext = true
        garageDetailVC.modalPresentationStyle = .overCurrentContext
        present(garageDetailVC, animated: true, completion: nil)
    }
    
    //-------------------------------------
    func doorButtonActionOnSelection(sender: Int)
    {
        guard sender >= 0 else
        {
            //self.colorObjectAt(index: 0, color: .clear)
            return
        }
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let doorsDetailVC = storyboard.instantiateViewController(withIdentifier: "DoorsDetailVC") as! DoorsDetailViewController
        doorsDetailVC.view.backgroundColor = UIColor(red: 75.0 / 255.0, green: 75.0 / 255.0, blue: 75.0 / 255.0, alpha: 0.8)
        doorsDetailVC.providesPresentationContextTransitionStyle = true
        doorsDetailVC.definesPresentationContext = true
        doorsDetailVC.modalPresentationStyle = .overCurrentContext
        let garage:Door =  isDouble ?  doorsDoubleArray[sender] : doorsArray[sender]
//print("doorButtonActi onOnSelection sender, garage",sender,garage)

        doorsDetailVC.selectedImgView.sd_setImage(with: URL(string: garage.image), placeholderImage: UIImage(named: ""))
        
        doorsDetailVC.titleLabel.text = garage.title
        doorsDetailVC.subTitleLabel.text = String(format:"%@\n%@", garage.subTitle, garage.price)
        if hasOnlyGarageDoors
        {
            doorsDetailVC.subTitleLabel.text = String(format:"%@", garage.price)
        }
        doorsDetailVC.selectedImgView.backgroundColor = .white
        present(doorsDetailVC, animated: true, completion: nil)
    }
}

//----------------------------------------
extension HomeViewController:GarageTableViewCellDelegate
{
    //----------------------------------------
    func paintScrollend(index: Int)
    {
        self.paintScrollIndex = index
        
        if index == 0
        {
            self.paintImageView.image = nil;
            self.paintImageView1.image = nil;
            self.paintImageView2.image = nil;
            return
        }

        //............ do i need this?  it is now universal...,  also needs to change on a save... check that...
          //********** change 1.1.4 remove, but why is it here?, no longer needed,  needs to change when project is changed in home view,
      //    curProjectChoiceList = self.loadThumbChoices(currProjectThumb?.projectID)
        
        if (index < curProjectChoiceList.count) //********** change 1.1.4 change ***
        {
            let projectThumb:ProjectThumb = curProjectChoiceList[index] //********** change 1.1.4 change ***
//print("choice didSelectItemAt projectThumb:",projectThumb as Any);
//print("choice didSelectItemAt projectThumb.projectID:",projectThumb.projectID as Any);
//print("paintScrol lend total",curProjectCh oiceList.count)
//print("paintScrol lend index",index)
            
            if let projectId = projectThumb.projectID
            {
                if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                {
                    let fileURL = dir.appendingPathComponent("projectChoices_"+projectId+".txt")
                    do
                    {
                        let rawdata = try Data(contentsOf: fileURL)
                        NSKeyedUnarchiver.setClass(Project.self, forClassName: "Project")
                        if let project0 = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? Project
                        {
                            self.projectSetUp(project0)
                        }
                    } catch {
  print(" ---------------------- changeToPa intChoice Couldn't read project -----------------------------")
                    }
                }
            }
            self.garageScrollend(index:garageScrollIndex)
            self.doorScrollend(index:doorScrollIndex)
        }
    }
    
    //----------------------------------------
    func garageScrollend(index: Int)
    {
        guard let s = self.selectedReferenceItem else { return }
//print("garage Scroll end s:",s)
//print("garageScrol lend index:",index)

//print("  ")
//print("garage Scroll end index:",index)

//print("garage Scroll end self.homeObj:",self.homeObj as Any)
//print("garage Scroll end self.homeObj?.garages:",self.homeObj?.garages as Any)
//print("garage Scroll end self.homeObj?.garages.count:",self.homeObj?.garages.count as Any)

        if let garagesCount = self.homeObj?.garages.count
        {
            self.garageScrollIndex = index
//print("garage Scroll end garagesCount:",garagesCount)
            if index == 0
            {
                self.garageImageView.image = nil;
                self.garageMaskView.image = nil;
//print("00 self.garageMa skView.image = nil")
                garageMaskImage = nil;
//print("00 self.garageMa skImage = nil")
                garageSingleMaskImage = nil;
                maskedDoorImage  = nil;
                theDoorsImage = nil;
//print("garage Scroll was cleared")

                //------------- initialize the startColor for garage and door ---- until read from table entry ----
                doorStartColor0 = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.0);
                defaultDoorColor0 = doorStartColor0;
//print("0 garageScrol lend   defaultDo orColor0:",defaultDoorColor0 as Any)

                polyLayer.path = nil

//print("garage Scroll end self.garageIm ageView.image:",self.garage Imag eView.image as Any)
//print("garageScr ollend self.image View.frame:",self.image View.frame)
//print("garageScr ollend self.garageIm ageView.frame:",self.garage Image View.frame)
                if let mainCont = currentMainImageController
                {
                    mainCont.scrollView.garageImageView.image = nil;
                }

                if let paintCont = currentPaintImageController
                {
                    paintCont.scrollView.garageImageView.image = nil;
                    paintCont.scrollView.garageMaskView.image = nil;
//print(" garageScrol lend   paintCont.scrollView.garageIma geView.image:",paintCont.scrollView.garageIma geView.image as Any)
                    paintCont.paintLayer.path = nil
                }

                garageCatID = ""
//print("2 garageScrol lend garimageID:",garimageID as Any)
                garimageID = ""
                garageCatName = ""

                btnFav.isEnabled = true
                self.btnFav.setImage(UIImage(named: "heart-v1"), for: .normal)

                var type = "singleGarage"
                if index < garagesCount
                {
                    type = "garage"
                }

//print("0 garage Scroll end replac eObjectAt")
                self.replaceObjectAt(index: -1, type: type)
                return
            }

            btnFav.isEnabled = true

//print("00 self.garageMaskScrol lIndex self.garageScrol lIndex:",self.garageMaskScrollIndex,self.garageScrollIndex)
            if (self.garageMaskScrollIndex != self.garageScrollIndex)
            {
                self.garageMaskView.image = nil;
//print("01 self.garageMa skView.image = nil")
                garageMaskImage = nil;
//print("01 self.garageMa skImage = nil")
                garageSingleMaskImage = nil;
                maskedDoorImage  = nil;

                if let paintCont = currentPaintImageController
                {
                    paintCont.scrollView.garageMaskView.image = nil;
                }

                //------------- initialize the startColor for garage and door ---- until read from table entry ----
                doorStartColor0 = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.0);
                defaultDoorColor0 = doorStartColor0;
                currGallID = nil;
                currGallThumb = nil;
//print("1b garageScrol lend      self.currGal lThumb = nil")
            }

//print("2b garageScrol lend self.garageMa skView.image:",self.garageMaskView.image as Any)
//print("2b garageScrol lend garageMa skImage:",garageMaskImage as Any)
//print("2b garageScrol lend garimageID:",garimageID as Any)
            garimageID = ""
//print("garage Scroll end self.garageScr ollIndex,index:",self.garageScrollIndex,index)

//print(" ")
//print(" ")
//print("garage Scroll end isTwoCar:",isTwoCar)
//print("garage Scroll end isOneCar:",isOneCar)
            var noGoodMask = false;

            if (isTwoCar)
            {
                if let garages = self.homeObj?.garages    //*************** change 3.0.2 add if block ***
                {
                    if (index-1 < garages.count) && (garages.count > 0) && (index-1 >= 0)    //*************** change 3.0.2 add if block ***
                    {
                        let garage = garages[index-1]  //*************** change 3.0.2 change ***
                        s.replacementGarageNames = [garage.image]
//print("garage Scroll end s.replacementGa rageNames:",s.replacementGar ageNames as Any)
//print("garage Scroll end garage:",garage as Any)

                        var currentPage:Int = 0
                        if let rep = s.replacementGarageNames
                        {
                            currentPage = rep.count - 1
                        }

                        self.cur2Garage = garage;

                        garageCatID = garage.catalogID
                        garageCatName = garage.title
                        //print("garage Scroll end garageCatName:",garageCatName as Any)
                        //print("garage Scroll end garage.isPaintable:",garage.isPaintable as Any)
                        //print("garage Scroll end garage.mask_url:",garage.mask_url as Any)
//print("garage Scroll end self.garageMa skImage:",garageMa skImage as Any)

                        //print("self.garageMaskScrol lIndex self.garageScrol lIndex:",self.garageMaskScrol lIndex,self.garageScrollIndex)

                        if ((self.garageMaskScrollIndex != self.garageScrollIndex) || (garageMaskImage == nil))
                        {
//print("garage.isPaintable:",garage.isPaintable as Any)
                            if (garage.isPaintable == 1)
                            {
                                self.garageMaskScrollIndex = self.garageScrollIndex
                                //print("self.garageMaskScrol lIndex:",self.garageMaskScrol lIndex)
                                doorStartColor0 = UIColor(red: garage.rValue ?? 0.0, green: garage.gValue ?? 0.0, blue: garage.bValue ?? 0.0, alpha: 1.0);
                                defaultDoorColor0 = doorStartColor0;
                                garageMaskImage = UIImage(imageLiteralResourceName: "transparent")
                                scrollEndDoor    = true;

//print("3 garage Scroll end doorStartCo lor0:",doorStartColor0 as Any)
//print("3 garage Scroll end defaultDo orColor0 :",defaultDoorColor0 as Any)

                                if let imageItem = garage.mask_url
                                {
                                    if (imageItem.count > 2)
                                    {
                                        if let aUrl = URL(string: imageItem)
                                        {
                                            //print("two car Garage aUrl:", aUrl)
                                            let session = URLSession.shared
                                            let task = session.dataTask(with: aUrl, completionHandler:
                                            { data, response, error in
//print("two car Garage Download Finished")
                                                if let data0 = data, error == nil
                                                {
                                                    if let source =  CGImageSourceCreateWithData(data0 as CFData, nil)
                                                    {
                                                        if let imageccg = CGImageSourceCreateImageAtIndex(source, 0, nil)
                                                        {
                                                            garageMaskImage = UIImage(cgImage: imageccg)
//print("garageMa skImage:",garageMas kImage as Any)
                                                        }
                                                    }
//print("3 garageScrol lend garage MaskImage:",garageMaskImage as Any)
                                                    DispatchQueue.main.async()
                                                    {
                                                            self.replaceObjectAt(index: currentPage, type: "garage")
                                                    }
                                                }
                                            })
                                            task.resume()
                                        } else {noGoodMask = true;}
                                    } else {noGoodMask = true;}
                                } else {noGoodMask = true;}
                            } else {noGoodMask = true;}
                        } else {noGoodMask = true;}

                        if (noGoodMask)
                        {
                            self.replaceObjectAt(index: currentPage, type: "garage")
                        }
                    }
                }


                if let house0Object = houseObject
                {
                    for resp in house0Object.response
                    {
                        for respItem in resp
                        {
                            if (respItem.labels.count > 0)
                            {
                                let lbl = respItem.labels[0]
                                if lbl.contains("two-car")
                                {
                                    let imgId = respItem.imageID_db

                                    if garimageID.count > 0
                                    {
                                        //-------------- for single garage to create favorite creating all combination of imageID and catID
//print("2c garageScrol lend garimageID:",garimageID as Any)
                                        garimageID = garimageID+"||"+imgId
//print("garage Scroll end 0 garimageID:",garimageID as Any)
                                    } else {
//print("2d garageScrol lend garimageID:",garimageID as Any)
                                        garimageID = imgId
//print("garage Scroll end 1 garimageID:",garimageID as Any)
                                    }

                                    if let gallID = currGallID
                                    {
                                        //-------------- for single garage to create favorite creating all combination of imageID and catID
                                        garimageID = garimageID+"||"+gallID
                                    }
                                }
                            }
                        }
                    }
                }

//print("garage Scroll end 22 isOneCar:",isOneCar)
                if (isOneCar)
                {
                    if let house0Object = houseObject
                    {
                        for resp in house0Object.response
                        {
                            for respItem in resp
                            {
                                if (respItem.labels.count > 0)
                                {
                                    let lbl = respItem.labels[0]
                                    if lbl.contains("one-car")
                                    {
                                        let imgId = resp[0].imageID_db

                                        if garimageID.count > 0
                                        {
                                            //----------------- for single garage to create favorite creating all combination of imageID and catID
//print("2e garageScrol lend garimageID:",garimageID as Any)
                                            garimageID = garimageID+"||"+imgId
                                        } else {
//print("2f garageScrol lend garimageID:",garimageID as Any)
                                            garimageID = imgId
                                        }

                                        if let gallID = currGallID
                                        {
                                            //----------------- for single garage to create favorite creating all combination of imageID and catID
                                            garimageID = garimageID+"||"+gallID
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {

//print("not twoCar")

				if let garages = self.homeObj?.singleGarage    //*************** change 3.0.2 add if block ***
                {
                    if (index-1 < garages.count) && (garages.count > 0) && (index-1 >= 0)    //*************** change 3.0.2 add if block ***
                    {
                        let garage = garages[index-1]  //*************** change 3.0.2 change ***
                        s.replacementGarageNames = [garage.image]

                        var currentPage:Int = 0
                        if let rep = s.replacementGarageNames
                        {
                            currentPage = rep.count - 1
                        }

//print("garage Scroll singleGarage s.replacementGarageNames:",s.replacementGarageNames as Any)
//print("garage Scroll singleGarage garage:",garage as Any)
                        self.cur1Garage = garage;

                        garageCatID = garage.catalogID
                        garageCatName = garage.title
//print("garage Scroll singleGarage garageCatName:",garageCatName as Any)
//print("garage Scroll singleGarage garage.isPaintable:",garage.isPaintable as Any)
//print("garage Scroll singleGarage garage.mask_url:",garage.mask_url as Any)
//print("garage Scroll self.garageMaskScrol lIndex self.garageScrol lIndex:",self.garageMaskScrol lIndex,self.garageScrollIndex)
//print("  self.garageMa skImage:",self.garageMa skImage as Any)

                        if ((self.garageMaskScrollIndex != self.garageScrollIndex) || (garageMaskImage == nil))
                        {
                            if (garage.isPaintable == 1)
                            {
                                self.garageMaskScrollIndex = self.garageScrollIndex
                                doorStartColor0 = UIColor(red: garage.rValue ?? 0.0, green: garage.gValue ?? 0.0, blue: garage.bValue ?? 0.0, alpha: 1.0);
                                defaultDoorColor0 = doorStartColor0;
//print("4 garageScrol lend   defaultDo orColor0:",defaultDoorColor0 as Any)
                                garageMaskImage = UIImage(imageLiteralResourceName: "transparent")
                                scrollEndDoor    = true;

                                if let imageItem = garage.mask_url
                                {
                                    if (imageItem.count > 2)
                                    {
                                        if let aUrl = URL(string: imageItem)
                                        {
                                            let session = URLSession.shared
                                            let task = session.dataTask(with: aUrl, completionHandler:
                                            { data, response, error in
//print("Download Finished")
                                                if let data0 = data, error == nil
                                                {
                                                    if let source =  CGImageSourceCreateWithData(data0 as CFData, nil)
                                                    {
                                                        if let imageccg = CGImageSourceCreateImageAtIndex(source, 0, nil)
                                                        {
                                                            garageMaskImage = UIImage(cgImage: imageccg)
                                                        }
                                                    }
//print("4 garageScrol lend garage MaskImage:",garageMaskImage as Any)
                                                    DispatchQueue.main.async()
                                                    {
                                                            self.replaceObjectAt(index: currentPage, type: "garage")
                                                    }
                                                }
                                            })
                                            task.resume()
                                        } else {noGoodMask = true;}
                                    } else {noGoodMask = true;}
                                } else {noGoodMask = true;}
                            } else {noGoodMask = true;}
                        } else {noGoodMask = true;}

                        if (noGoodMask)
                        {
                            self.replaceObjectAt(index: currentPage, type: "garage")
                        }
                    }
                }

//print("2f garageScrol lend garimageID:",garimageID as Any)
                garimageID = ""

                if let house0Object = houseObject
                {
                    for resp in house0Object.response
                    {
                        for respItem in resp
                        {
                            if (respItem.labels.count > 0)
                            {
                                let lbl = respItem.labels[0]
                                if lbl.contains("one-car")
                                {
                                    let imgId = resp[0].imageID_db

                                    if garimageID.count > 0
                                    {
                                        //------------- for single garage to create favorite creating all combination of imageID and catID
//print("2g garageScrol lend garimageID:",garimageID as Any)
                                        garimageID = garimageID+"||"+imgId
                                    } else {
//print("2h garageScrol lend garimageID:",garimageID as Any)
                                        garimageID = imgId
                                    }

                                    if let gallID = currGallID
                                    {
                                        //------------- for single garage to create favorite creating all combination of imageID and catID
                                        garimageID = garimageID+"||"+gallID
                                    }
                                }
                            }
                        }
                    }
                }
            }

            self.checkFavStatus(indexs: index-1)
        }
    }
    
    
    //-------------------------------------
    func doorScrollend(index: Int)
    {
        //print("door Scroll end index:",index)
        self.doorScrollIndex = index  //*************** change 0.0.30 move to here ***
        
        if index == 0
        {
            self.garageImageView.image = nil;
            self.frontDrMaskView.image = nil;  //*************** change 0.0.30 add ***
            frontDrMaskImage = nil; //*************** change 0.0.31 add ***
            maskedFrontDrImage  = nil;   //*************** change 0.0.30 add ***
//            maskedFrontDrImage1 = nil;    //*************** change 1.1.0 remove ***
            polyLayer.path = nil
            
            frontDrStartColor0 = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.0); //*************** change 0.0.30 add ***
            defaultFrontDrColor0 = frontDrStartColor0; //*************** change 0.0.30 add ***
            
            if let mainCont = currentMainImageController
            {
                mainCont.scrollView.garageImageView.image = nil;
            }
            
            if let paintCont = currentPaintImageController
            {
                paintCont.scrollView.garageImageView.image = nil;
//print(" doorScrol lend   paintCont.scrollView.garageIma geView.image:",paintCont.scrollView.garageImag eView.image as Any)
                paintCont.scrollView.frontDrMaskView.image = nil;  //*************** change 0.0.30 add ***
                paintCont.paintLayer.path = nil
            }
            
            doorCatID = ""
            doorimageID = ""
            
            btnFav.isEnabled = true
            self.btnFav.setImage(UIImage(named: "heart-v1"), for: .normal)
            let type = "door"
            //print("0 doorScrol lend replac eObjectAt")
            self.replaceObjectAt(index: -1, type: type)
            return
        }
        
        if (self.doorMaskScrollIndex != self.doorScrollIndex)
        {
            self.frontDrMaskView.image = nil;
            frontDrMaskImage = nil;
            maskedFrontDrImage  = nil;
//            maskedFrontDrImage1 = nil;   //*************** change 1.1.0 remove ***
            
            if let paintCont = currentPaintImageController
            {
                paintCont.scrollView.frontDrMaskView.image = nil;
            }
            
            frontDrStartColor0 = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.0);
            defaultFrontDrColor0 = frontDrStartColor0;
        }
        
        btnFav.isEnabled = true
        guard let s = self.selectedReferenceItem else { return }
        
        let garage:Door =  isDouble ?  doorsDoubleArray[index-1] : doorsArray[index-1]
        
        //print("door Scroll end isDouble:",isDouble)
        
        if (isDouble)
        {
            self.cur2Door = garage;
        } else {
            self.cur1Door = garage;
        }
        
        s.replacementGarageNames = [garage.image]
        doorCatID = garage.catalogID
        
//print(" ")
//print("garage Scroll frontDoor garageCatName:",doorCatID as Any)
//print("garage Scroll frontDoor garage.isPaintable:",garage.isPaintable as Any)
//print("garage Scroll frontDoor garage.mask_url:",garage.mask_url as Any)
        
        if (self.doorMaskScrollIndex != self.doorScrollIndex)
        {
            if (garage.isPaintable == 1)
            {
                frontDrStartColor0 = UIColor(red: garage.rValue ?? 0.0, green: garage.gValue ?? 0.0, blue: garage.bValue ?? 0.0, alpha: 1.0);
                defaultFrontDrColor0 = frontDrStartColor0;
                
                if let imageItem = garage.mask_url
                {
                    if (imageItem.count > 2)
                    {
                        let imageView:UIImageView = UIImageView()
                        imageView.frame.origin = CGPoint.zero;
                        imageView.frame = CGRect(origin: CGPoint.zero, size: CGSize(width: 300.0, height: 300.0));
                        imageView.contentMode = .scaleAspectFit;
                        imageView.transform = CGAffineTransform.identity;
                        //print("  before")
                        
                        imageView.sd_setImage(with: URL(string: imageItem))
                        { (image, error, cache, urls) in
                            
                            //print("  inside")
                            //print("garage Scroll singleGarage image:",image as Any)
                            //print("garage Scroll singleGarage error:",error as Any)
                            //print("garage Scroll singleGarage cache:",cache as Any)
                            //print("garage Scroll singleGarage urls:",urls as Any)
                            
                            if (error != nil)
                            {
                                //print("garage Scroll singleGarage error:",error as Any)
                            } else {
                                frontDrMaskImage = image
                                //print("garage Scroll singleGarage self.garageMa skImage:",self.garageM askImage as Any)
                                //print("garage Scroll singleGarage self.garageMas kImage.cgimage:",self.garageM askImage?.cgImage as Any)
                            }
                        }
                        //print("  after")
                    }
                }
            }
        }
        
        //        if s.hous eObject == nil
        //        {
        //            return
        //        }
        
        if let house0Object = houseObject
        {
            for resp in house0Object.response
            {
                for respItem in resp
                {
                    if (respItem.labels.count > 0)
                    {
                        let lbl = respItem.labels[0]
                        if lbl.contains("door")
                        {
                            doorimageID = resp[0].imageID_db
                        }
                    }
                }
            }
        }
        
        guard let rep = s.replacementGarageNames else { return }
        
        //print("door Scroll end index end index-1:",index-1)
        
        checkFavStatus(indexs: index-1)
        let currentPage = rep.count - 1
        //print("doorScrol lend replac eObjectAt")
        self.replaceObjectAt(index: currentPage, type: "door")
    }
    
    
    //-------------------------------------
    func leftGesture()
    {
        if selectedimage > 0
        {
            selectedimage = selectedimage - 1
            homeTableView.reloadData()
            
        } else {
            AlertViewManager.shared.ShowOkAlert(title: Constants.Alerts.Home.OPP, message: Constants.Alerts.Home.NO_MORE_BACKWORD_MESSAGE, handler: nil)
        }
    }
    
    //-------------------------------------
    func rightGesture()
    {
        if self.btnGarageClicked
        {
            if selectedimage < (self.homeObj?.garages.count)!-1
            {
                selectedimage = selectedimage + 1
                homeTableView.reloadData()
            } else {
                AlertViewManager.shared.ShowOkAlert(title: Constants.Alerts.Home.OPP, message: Constants.Alerts.Home.NO_MORE_FORWARD_MESSAGE, handler: nil)
            }
        } else {
            let count =  isDouble ?  doorsDoubleArray.count : doorsArray.count
            if selectedimage < count-1 {
                selectedimage = selectedimage + 1
                homeTableView.reloadData()
            }else{
                AlertViewManager.shared.ShowOkAlert(title: Constants.Alerts.Home.OPP, message: Constants.Alerts.Home.NO_MORE_FORWARD_MESSAGE, handler: nil)
            }
        }
        
        
    }
    
    //-------------------------------------
    func tapGesture()
    {
    }
}

//********** change 0.0.40 replace whole extension ***
//-------------------------------------
extension HomeViewController:UICollectionViewDelegate,UICollectionViewDataSource
{
    //-------------------------------------
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        //  return imagesArray.count
        return projectList00.count
    }
    
    //-------------------------------------
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    //-------------------------------------
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let multipleHomeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MultipleHomeCell", for: indexPath) as! MultipleHomeCollectionViewCell
        
        if (indexPath.item < projectList00.count)
        {
            if selectedGalleryImage == indexPath.item
            {
                multipleHomeCell.imgView.layer.cornerRadius = 5.0
                multipleHomeCell.imgView.clipsToBounds = true
                multipleHomeCell.imgView.layer.borderColor =  UIColor.white.cgColor
                multipleHomeCell.imgView.layer.borderWidth = 2.0
            } else {
                multipleHomeCell.imgView.layer.cornerRadius = 5.0
                multipleHomeCell.imgView.clipsToBounds = true
                multipleHomeCell.imgView.layer.borderColor =  UIColor.white.cgColor
                multipleHomeCell.imgView.layer.borderWidth = 0.0
            }
            
            //            let imagefromDoc = UIImage(contentsOfFile: self.imagesArray[indexPath.item].path)
            //            multipleHomeCell.imgView.image = imagefromDoc
            
            if projectList00.count == 1
            {
                multipleHomeCell.delBtn.isHidden = true
            } else {
                multipleHomeCell.delBtn.isHidden = false
            }
            
            let projectThumb = projectList00[indexPath.item]
            
            if let thumbData = projectThumb.thumbnailData
            {
                multipleHomeCell.imgView.image = UIImage(data:thumbData);
            }
            
            
            multipleHomeCell.delBtn.tag = indexPath.item
            multipleHomeCell.projectThumb = projectThumb;
            
            multipleHomeCell.imgView.contentMode = .scaleAspectFill
            multipleHomeCell.delBtn.addTarget(self, action: #selector(deleteHouseImage(sender:)), for: .touchUpInside)
        }
        
        return multipleHomeCell
    }
    
    //-------------------------------------
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
//print("UICollectionView indexPath:",indexPath as Any)
        //        selectedGalleryImage = 0   //********** change 1.0.1 remove ***
        if (indexPath.item < projectList00.count)
        {
            self.selectedGalleryImage = indexPath.item
            //print("didSelectItemAt indexPath.item:",indexPath.item)
            self.btnDoneClickedAction()
            currProjectThumb = projectList00[indexPath.item]
            //print("didSelectItemAt currProjectThumb.projectID:",currProjectThumb?.projectID as Any)
            self.changeToProject(currProjectThumb)

            self.photoView.isHidden = true;
            shouldSave = false;
        }
    }
    
    //-------------------------------------------------
    @objc func deleteHouseImage(sender:UIButton)
    {
        let indexValue = sender.tag
//print("deleteHouseImage indexValue:",indexValue as Any)
        if (indexValue < projectList00.count)
        {
            var shouldChange = false;
            //            if (indexValue < self.imagesArray.count)
            //            {
            //                let url:URL = self.imagesArray[sender.tag]
            //                let filename = (url.absoluteString as NSString).lastPathComponent
            //
            //                self.removeOldFileIfExist(fileName: filename)
            //                self.imagesArray.remove(at:sender.tag)
            //            //    self.photoCollectionView.reloadData()
            //            //    self.selectedGal leryImage = 0
            //            }
            
            //print("deleteHouseImage self.selectedGal leryImage:",self.selectedGalleryImage)
            //print("deleteHouseImage indexValue:",indexValue)
            
            
            //print("del eteBtnAction indexValue:",indexValue)
            //print("1 deleteHouseImage projectList00.count:",projectList00.count)
            
            let projectThumb:ProjectThumb = projectList00[indexValue]
            //print("deleteHouseImage projectThumb.projectID:",projectThumb.projectID as Any)
            
            projectList00.remove(at: indexValue)
            //print("2 deleteHouseImage projectList00.count:",projectList00.count)
            if (indexValue == self.selectedGalleryImage)
            {
                shouldChange = true;
                self.selectedGalleryImage = max(self.selectedGalleryImage-1,0)
                self.btnDoneClickedAction()
            }
            if (projectList00.count < 1)
            {
                self.selectedGalleryImage = -1
            }
            
            do
            {
                if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                {
                    let fileURL = dir.appendingPathComponent("projectThumbs.txt")
                    //print("saveHouseTo DocumentDirectory fileURL:",fileURL)
                    let data = try NSKeyedArchiver.archivedData(withRootObject: projectList00, requiringSecureCoding: false)
                    try data.write(to: fileURL)
                }
                
            } catch {
                //print("deleteBt nAction  error:",error)
            }
            
            photoCollectionView.reloadData()
            
            if let projectId = projectThumb.projectID
            {
                do
                {
                    if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                    {
                        NSKeyedArchiver.setClassName("Project", for: Project.self)
                        let fileURL = dir.appendingPathComponent("project_"+projectId+".txt")
                        try FileManager.default.removeItem(at: fileURL)
                        //print("del eteBt nAction project has Been del ete")
                    }
                    
                } catch {
                    //print("deleteBt nAction  error:",error)
                }
            }
            
            //print("deleteHouseImage shouldChange:",shouldChange)
            //print("deleteHouseImage self.selectedGal leryImage:",self.selectedGalleryImage)
            //print("deleteHouseImage projectList00.count:",projectList00.count)
            if (shouldChange && (self.selectedGalleryImage >= 0) && (self.selectedGalleryImage < projectList00.count))
            {
                currProjectThumb = projectList00[selectedGalleryImage]
                self.changeToProject(currProjectThumb);
                self.photoView.isHidden = true;
                shouldSave = false;
            }
        }
    }
    
    //------------------------------------
    func getProject(projectId:String) -> Project?
    {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        {
            let fileURL = dir.appendingPathComponent("project_"+projectId+".txt")
            
            do
            {
                //print("che ckHouse ProjectExist houseURL:",houseURL)
                let rawdata = try Data(contentsOf: fileURL)
                //print("che ckHouse ProjectExist rawdata.count:",rawdata.count)
                NSKeyedUnarchiver.setClass(Project.self, forClassName: "Project")
                
                if let project = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? Project
                {
                    //print("che ckHouse ProjectExist house:",house)
                    return project
                } else {
                    return nil
                }
            } catch {
                print("get Project ProjectExist Couldn't read house")
                return nil
            }
        }
        
        return nil
    }
    
    //------------------------------------------------------------------------------------
    func changeToProject( _ projectThumb0:ProjectThumb?)
    {
//print(" changeToP roject projectThumb0:",projectThumb0 as Any);
            if let projectThumb = projectThumb0
            {
//print(" changeToP roject projectThumb.projectID:",projectThumb.projectID as Any);
                if let projectId = projectThumb.projectID
                {
                    if let project = self.getProject(projectId:projectId)
                    {
                        dontRotate = true;
                        commingFromProjectList = true;
                        self.clearThePaint()

                        self.clearOldProject()
                        self.filterClearData()
                        smallImageSizeX = 999999.0
                        wasChangeOfProject = false;
                        
                        currProject = project
                        currProjectThumb = projectThumb;


                   //     self.clearOldProject()  //********** change 1.1.4 remove  called twice ***

//print("  ")
//print("changeToP roject currPr oject?.projectID:",currPro ject?.projectID as Any)
//print("changeToP roject currPr ojectThumb?.projectID:",currP rojectThumb?.projectID as Any)
//print("changeToP roject project.imageData:",project.imageData as Any)
                        if let name = project.imageName
                                              {
                                                  currentImageName = name;
                                              }
                        if let imageData = project.imageData
                        {
                            originalHomeImage = UIImage(data:imageData);
                        } else {
                            originalHomeImage = checkImageExist(filename:currentImageName) ?? UIImage()
                        }

                        targetMaskRect.size = originalHomeImage.size;
                        self.imageView.image = originalHomeImage;
                      //  }
    //print("changeToP roject originalHomeImage:",originalHomeImage as Any)

    //print(" changeToP roject  currPro ject.imageName:",currP roject?.imageName as Any);
                      
    //print(" changeToP roject  currentImageName:",currentImageName as Any);
                        let house_filename = currentImageName.replacingOccurrences(of:".jpg", with: "_houseObject.txt")
                        if let house0Object = self.checkHouseProjectExist(filename:house_filename)
                        {
                            houseObject = house0Object
                            for resp in house0Object.response
                            {
                                for respItem in resp
                                {
                                    respItem.repairLinkedList()
                                }
                            }
                        } else {
                            
                            if let houseData = project.house0
                            {
                                let decoder = JSONDecoder()
                                if let house0Object = try? decoder.decode(HouseObject.self, from: houseData)
                                {
                                    //print(" houseOb ject: ",houseObject as Any);
                                    houseObject = house0Object;
                                    
                                    for resp in house0Object.response
                                    {
                                        for respItem in resp
                                        {
                                            respItem.repairLinkedList()
                                        }
                                    }
                                }
                            }
                        }

                        self.asset = nil;
                        shouldAddToAlbum = false;

                        screenScale = UIScreen.main.scale
                        if let image = originalHomeImage
                        {
                            if (image.size.width > 1048) || (image.size.height > 1048)
                            {
                                screenScale = 1.0;
                            }
                        }

    //print("1 changeToP roject houseOb ject: ",houseObject as Any);

                        curProjectChoiceList = self.loadThumbChoices(currProjectThumb?.projectID)  //********** change 1.1.4 add,  needs to be universal ***


                        self.projectSetUp(project)
                    //    self.getRepFromHouseObjectByCategory()
    //print("2 changeToP roject houseOb ject: ",houseObject as Any);

                        if let currPro = currProject
                        {
    //print("3 changeToP roject currPro.wasPro cessed: ",currPro.wasPr ocessed as Any);
                            if (currPro.wasProcessed)
                            {
                            } else {
                                self.encodeThePhoto()
                            }
                        }
                        

    //                    if let appDelegate0 = (UIApplication.shared.delegate as? AppDelegate)
    //                    {
    //                        DispatchQueue.global().async
    //                        {
    //                            appDelegate0.backgroundTaskID = UIApplication.shared.beginBackgroundTask (withName: "Finish saving data")
    //                            {
    //                                // End the task if time expires.
    //                                UIApplication.shared.endBackgroundTask(appDelegate0.backgroundTaskID!)
    //                                appDelegate0.backgroundTaskID = UIBackgroundTaskIdentifier.invalid
    //                            }

                                do
                                {
                                    //---------------- save main project  ------------------
                                    if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                                    {
                                        let currProjectURL = dir.appendingPathComponent("currProject.txt")
                                                                   NSKeyedArchiver.setClassName("Project", for: Project.self)
    
                                                                   let data = try NSKeyedArchiver.archivedData(withRootObject: project, requiringSecureCoding: false)
                                                                   try data.write(to: currProjectURL)
                                       
    //print("  ")
    //print("  ")
    //print("saveProje ctList project has Been Saved  currPr ojectURL:",currP rojectURL)
    //print("saveProje ctList project has Been Saved  currP roject?.projectID:",currP roject?.projectID as Any)
    //print("saveProje ctList project has Been Saved  currPro jectThumb?.projectID:",curr ProjectThumb?.projectID as Any)
                                    }

                                } catch {
      print(" ---------------------- selectedEx ample    project_  error:",error)
                                }
                        
    //                            UIApplication.shared.endBackgroundTask(appDelegate0.backgroundTaskID!)
    //                            appDelegate0.backgroundTaskID = UIBackgroundTaskIdentifier.invalid
    //                        }
    //                    }
                    }
                }
            }
        }
    
    //------------------------------------
    func projectSetUp(_ project0:Project?)
    {
//print("projectS etUp")
        guard let project = project0 else {return;}
        
        screenScale = UIScreen.main.scale
        if let image = originalHomeImage
        {
            forcedImageSize = Size(width: Float(image.size.width),
                                   height:Float(image.size.height));
            
            if (image.size.width > 1048) || (image.size.height > 1048)
            {
                screenScale = 1.0;
            }
        }
        
        defaultLineColor0 = UIColor.color(data:project.defaultLineColor0d)
        origStartColor0   = UIColor.color(data:project.origStartColor0d)
        
        defaultLineColor1 = UIColor.color(data:project.defaultLineColor1d)
        origStartColor1   = UIColor.color(data:project.origStartColor1d)
        
        defaultLineColor2 = UIColor.color(data:project.defaultLineColor2d)
        origStartColor2   = UIColor.color(data:project.origStartColor2d)
        
        defaultLineColor3 = UIColor.color(data:project.defaultLineColor3d)
        origStartColor3   = UIColor.color(data:project.origStartColor3d)
        
        defaultLineColor4 = UIColor.color(data:project.defaultLineColor4d)
        origStartColor4   = UIColor.color(data:project.origStartColor4d)
        
        
        defaultDoorColor0 = UIColor.color(data:project.defaultLineGaraged)
        doorStartColor0   = UIColor.color(data:project.origStartGaraged)
//print("projectS etup   defaultDo orColor0:",defaultDoorColor0 as Any)

        defaultFrontDrColor0 = UIColor.color(data:project.defaultLineFrontDrd)
        frontDrStartColor0   = UIColor.color(data:project.origStartFrontDrd)
        
        if let data = project.mask0 { maskedHouseImage0 = UIImage(data:data); }
        if let data = project.mask1 { maskedHouseImage1 = UIImage(data:data); }
        if let data = project.mask2 { maskedHouseImage2 = UIImage(data:data); }
        if let data = project.mask3 { maskedHouseImage3 = UIImage(data:data); }
        if let data = project.mask4 { maskedHouseImage4 = UIImage(data:data); }
        
        if let data = project.garageMask  { maskedDoorImage = UIImage(data:data); }
        if let data = project.garageSMask { theDoorsImage = UIImage(data:data); }
        if let data = project.frontDrMask { maskedFrontDrImage = UIImage(data:data); }
        
        if let data = project.houseMask0 { houseMask = UIImage(data:data); }
        if let data = project.siding0 { maskedSidingImage = UIImage(data:data); }
        if let data = project.roof0 { maskedRoofImage = UIImage(data:data); }
        
//print("project Setup  house Mask:",houseMask as Any)
//print("project Setup  project.well0:",project.well0)
//print("project Setup  project.well1:",project.well1)
//print("project Setup  project.well2:",project.well2)
        currentWellNum = project.currentWellNum;
        currentButtonToDisplay = project.currentButtonToDisplay;

        if (hasOnlyGarageDoors) //********** change 1.0.0 add if block ***
        {
            self.paintImageView.isHidden = true
            self.paintImageView1.isHidden = true
            self.paintImageView2.isHidden = true
        } else {
            self.paintImageView.isHidden  = !project.well0;
            self.paintImageView1.isHidden = !project.well1;
            self.paintImageView2.isHidden = !project.well2;
        }
        
        var targetImageRect:CGRect = CGRect(x:0.0, y:0.0, width:1.0, height:1.0);
        
        //    var highlightedImage:UIImage? = nil;
        
        //-----------  create overlay -----------------
        if let origImage = originalHomeImage
        {
            targetImageRect = CGRect(x:0.0, y:0.0, width:origImage.size.width, height:origImage.size.height);
            
            autoreleasepool
            {
                if let mask = maskedHouseImage0
                {
                    UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                        origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
                        mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                        let highlightedImage0 = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();

                    let brightHueFilter = PECbrightHue(origStartColor0, endColor:defaultLineColor0)
                    self.paintImageView.image = highlightedImage0?.filterWithOperation(brightHueFilter)
                }
            }
            
            autoreleasepool
            {
                if let mask = maskedHouseImage1
                {
                    UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                        origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
                        mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                        let highlightedImage1 = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();

                    let brightHueFilter = PECbrightHue(origStartColor1, endColor:defaultLineColor1)
                    self.paintImageView1.image = highlightedImage1?.filterWithOperation(brightHueFilter)
                }

                if let mask = maskedHouseImage2
                {
                    UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                        origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
                        mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                        let highlightedImage2 = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();

                    let brightHueFilter = PECbrightHue(origStartColor2, endColor:defaultLineColor2)
                    self.paintImageView2.image = highlightedImage2?.filterWithOperation(brightHueFilter)
                }
                    
//                if let mask = maskedHouseImage3
//                {
//                    if defaultLineColor3 != UIColor.black
//                    {
//                        UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
//                            origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
//                            mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
//                            currentPaintImageController.scrollView.paintImageOverlay3.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
//                        UIGraphicsEndImageContext();
//
//                        let brightHueFilter = PECbrightHue(origStartColor3, endColor:defaultLineColor3)
//                        currentPaintImageController.scrollView.paintImageOverlay3.image = currentPaintImageController.scrollView.paintImageOverlay3.highlightedImage?.filterWithOperation(brightHueFilter)
//                    } else {
//                        currentPaintImageController.scrollView.paintImageOverlay3.image = maskedHouseImage3
//                    }
//                }
//
//                if let mask = maskedHouseImage4
//                {
//                    if defaultLineColor4 != UIColor.black
//                    {
//                        UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
//                            origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
//                            mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
//                            currentPaintImageController.scrollView.paintImageOverlay4.highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
//                        UIGraphicsEndImageContext();
//
//                        let brightHueFilter = PECbrightHue(origStartColor4, endColor:defaultLineColor4)
//                        currentPaintImageController.scrollView.paintImageOverlay4.image = currentPaintImageController.scrollView.paintImageOverlay4.highlightedImage?.filterWithOperation(brightHueFilter)
//                    } else {
//                        currentPaintImageController.scrollView.paintImageOverlay4.image = maskedHouseImage4
//                    }
//                }
                    
                    

//                if let mask = garageSingleMaskImage  //************** change 1.1.0 *** remove but keep for possible use later
//                {
//                    UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
//                        origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
//                        mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
//                        let highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
//                    UIGraphicsEndImageContext();
//
//                    let brightHueFilter = PECbrightHue(doorStartColor0, endColor:defaultDo orColor0)
//                    self.garageIma geView.image = highlightedImage?.filterWithOperation(brightHueFilter)
//                }


//                    if let mask = maskedDoorImage  //************** change 1.1.0 *** remove, and change
//                    {
//                        UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
//                            origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
//                            mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
//                            let highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
//                        UIGraphicsEndImageContext();

//                    if (self.maskOfDoor) //************** change 1.1.0 *** remove
//                    {
//                        let brightHueFilter = PECbrightHue(doorStartColor0, endColor:defaultDoorColor0)
//                        self.garageMa skView.image = maskedDoorImage?.filterWithOperation(brightHueFilter)  //************** change 1.1.0 *** change
//print("2 garageScrol lend didSelectRowAt paintCont.scrollView.garageMa skView.image = highlightedImage?.filterWithOperation")
//                    }
////                    }
//
////                    if let mask = maskedFrontDrImage  //************** change 1.1.0 *** remove, and change
////                    {
////                        UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
////                            origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
////                            mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
////                            let highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
////                        UIGraphicsEndImageContext();
//
//                    if (self.maskOfFrontDoor) //************** change 1.1.0 *** remove
//                    {
//                        let brightHueFilterFrDr = PECbrightHue(frontDrStartColor0, endColor:defaultFrontDrColor0)//************** change 1.1.0 *** change
//                        self.frontDrMaskView.image = maskedFrontDrImage?.filterWithOperation(brightHueFilterFrDr)  //************** change 1.1.0 *** change
//                    }
//                    }
            }
        }
    }
}

//-------------------------------------
extension HomeViewController:TakeANewPhotoViewControllerDelegate
{
    //-------------------------------------
    func calProperVC(str: String)
    {
        if str == "create_order"
        {
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
            let createOrder = storyBoard.instantiateViewController(withIdentifier: "CreateOrderWebView") as! CreateOrderWebView
            createOrder.hidesBottomBarWhenPushed = true
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
            self.navigationController?.pushViewController(createOrder, animated: true)
            
        }
        
        if str == "agent_order"
        {
            self.clearThePaint()
            let popOverVC = UIStoryboard(name: "CustomTabBar", bundle: nil).instantiateViewController(withIdentifier: "LocationPopUp") as? LocationPopUp
            
            popOverVC?.view.frame = self.view.frame
            
            popOverVC?.type = "order"
            
            popOverVC?.modalPresentationStyle = .overCurrentContext
            self.fromOrderNumber = true
            popOverVC?.modalPresentationCapturesStatusBarAppearance = true
            self.present((popOverVC)!, animated: true, completion: nil)
        }
        
        if str == "camera"
        {
            let launchedBefore = UserDefaults.standard.bool(forKey: "launchedCamera")
            
            if launchedBefore
            {
                //print("Not first launch.")
                self.openCamera()
            } else {
                print("First launch, setting UserDefault.")
                UserDefaults.standard.set(true, forKey: "launchedCamera")
                UserDefaults.standard.synchronize()
                self.openDeviceCamera()
            }
            
        } else if str == "modifai" || str == "agent" || str == "agenthome" || str == "agent_create" || str == "agenthomedelete" {
            addedImageFromCameraStr = "defaultImage"
            
            //print("TakeANewPhotoViewControllerDelegate modifai.")
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
            let modifaiHousesVC = storyBoard.instantiateViewController(withIdentifier: "ModifaiHousesVC") as! ModifaiHousesViewController
            modifaiHousesVC.delegate = self
            modifaiHousesVC.type = str
            modifaiHousesVC.modalPresentationCapturesStatusBarAppearance = false   //********** change 0.0.40 change ***
            modifaiHousesVC.modalPresentationStyle = .overCurrentContext   //********** change 0.0.40 add ***
            present(modifaiHousesVC, animated: false, completion: nil)   //********** change 0.0.40 change ***
            
        } else if str == "library" {
            
            //print("TakeANewPhotoViewControllerDelegate library.")
            
            if UserDefaults.standard.bool(forKey: "showCameraAlert")
            {
                self.openPhotoLibrary(type: "photolibrary")
            } else {
                
                let alert = UIAlertController(title: "Just a Heads Up", message: "Using pictures not taken with the \(brand) application may not produce accurate results. We recommend you use the camera feature to get the best possible outcome.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler:
                    { (actionSheetController) -> Void in
                        print("handle open action...")
                        //self.openPhot oLibrary(type: "photolibrary")
                        UserDefaults.standard.set(true, forKey: "showCameraAlert")
                        
                        self.btnMenuclicked(self.btnMenu as Any)
                        
                }))


				if let popoverController = alert.popoverPresentationController  //********** change 3.0.3 add if block ***
				{
				  popoverController.sourceView = self.view
				  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
				  popoverController.permittedArrowDirections = []
				}

                self.present(alert, animated: true, completion: nil)
                
            }
        } else {
        }
    }
}

//-------------------------------------
extension HomeViewController:ModifaiHousesViewControllerDelegate
{
    //-------------------------------------
    func selectedPhoto(img: UIImage,imgName: String)
    {
        //print("  selected Photo imgName:",imgName)
        // self.imageView.image = img;// removed 0.0.50
        self.imageView.image = img.fixedOrientation() //********* change 0.0.50 change **
        
        //print("btnUsePhotoClicked library  self.old Image selected Photo. self.imageVi ew.image.size:",self.imageV iew.image?.size as Any)
        originalHomeImage = self.imageView.image
        targetMaskRect.size = originalHomeImage.size;
        //     self.oldImage = self.imageView.image!
        //      self.landImageView = self.imageView
        
        
        //        self.imageHeightConstarin.constant = self.setUpIm ageSize().height
        //        if self.imageHeightConstarin.constant > heightToAdjust && !isLandScape
        //        {
        //            self.imageHeightConstarin.constant = heightToAdjust
        //        }
        //        self.scrollViewHeightConstarin.constant = self.imageHeightConstarin.constant
        
        currentImageName = imgName
        currentImageName = currentImageName.changeExtensions(name:currentImageName)
        //print("selected Photo currentImageName:",currentImageName as Any)
        
        //print("selected Photo  clear The Paint")
        self.clearThePaint()
        self.clearOldProject()
        
        self.collectionView?.isHidden = true
        self.setupButtonTitlesForImage()
        
        self.selectedReferenceItem = nil
        self.selectedReferenceItem = ReferenceItem.init(name: "Camera")
        self.selectedReferenceItem?.image = self.imageView.image
        
        commingFromProjectList = true;
        
        self.fetchHouseObject
            { (house0Object) in
                self.selectedExample(img:originalHomeImage, imgName:imgName) //********* change 0.0.50 change **
                
                //self.selec tedExample(img:img, imgName:imgName)  //********** change 0.0.40 add ***
        }
        
        
        let size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height);
        //print("2  selec tedPhoto  size:",size)
        self.shallRotate(size)
    }
    
    //********** change 0.0.40 add ***
    //----------------------------------------------------------------------------------------------------------
    func clearOldProject()
    {
        //print("  clearOl dProject")
        houseObject = nil;
        paintTimeLeft = paintMaxTime;
        self.paintTimer?.invalidate()
        
        oneCarImgae = UIImage(named: "1_s0.jpg")
        twoCarImgae = UIImage(named: "1_d0.jpg")
        oneDoorImgae = UIImage(named: "f_s0.jpg")
        twoDoorImgae = UIImage(named: "f_d10.jpg")
        
        isTwoCar = false;
        isOneCar = false;
        isDoor = false;
        isDouble = false;
        shouldSave = false;
        
        currentTwoCarGar = 0;
        currentOneCarGar = 0;
        currentDoublFrntDoor = 0;
        currentSinglFrntDoor = 0;
        currentCombinedFrntDoor = 0;
        
        currentWindow = 0;
        
        currentToolIndex = -1
        currentWellNum = 0;
        currentButtonToDisplay = 0;
        
        maskedHouseImage0 = nil;
        maskedHouseImage1 = nil;
        maskedHouseImage2 = nil;
        maskedHouseImage3 = nil;
        maskedHouseImage4 = nil;
        
        maskedSidingImage = nil;
        maskedRoofImage = nil;
        houseMask = nil;
        
        garageMaskImage = nil;
//print("clearOldProject  garageMa skImage = nil")
        garageSingleMaskImage = nil;
        frontDrMaskImage = nil;
        self.garageScrollIndex = 0
               self.doorScrollIndex = 0
               self.paintScrollIndex = 0
        self.btnFavWasClicked = false
        self.btnGarageClicked = false
              self.btnListClicked = false
              self.btnPaintListClicked = false
              
              self.btnDoorClicked = false
              self.setupButtonSelection()
      
        self.btnFav.setImage(UIImage(named: "heart-v1"), for: .normal)

    }
    
    //********** change 0.0.40 add ***
    //-------------------------------------
    func selectedExample(img: UIImage,imgName: String)
    {
        //print("  selected Example imgName:",imgName)
        //print("  selected Example img:",img)
        originalHomeImage = img
        
        forcedImageSize = Size(width: Float(originalHomeImage.size.width),
                               height:Float(originalHomeImage.size.height));
        
        targetMaskRect.size = originalHomeImage.size;
        
        currentImageName = imgName
        currentImageName = currentImageName.changeExtensions(name:currentImageName)
        
        let mask0fileName = currentImageName.replacingOccurrences(of:".jpg", with: "_paintMask0.png")
        let mask1fileName = currentImageName.replacingOccurrences(of:".jpg", with: "_paintMask1.png")
        
        maskedHouseImage0 = UIImage(named:mask0fileName)
        maskedHouseImage1 = UIImage(named:mask1fileName)
        
        //print("maskedHouseImage0:",maskedHouseImage0 as Any)
        //print("maskedHouseImage1:",maskedHouseImage1 as Any)
        
        let defColorName0 = currentImageName.replacingOccurrences(of:".jpg", with: "_defaultColor0")
        if let defColorPath0 = Bundle.main.path(forResource: defColorName0, ofType: "txt")
        {
            if let rawdata = try? Data(contentsOf: URL(fileURLWithPath: defColorPath0))
            {
                if let color = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? UIColor
                {
                    defaultLineColor0 = color
                    //print("defaultLineColor0:",defaultLineColor0)
                }
            }
        }
        
        let startColorName0 = currentImageName.replacingOccurrences(of:".jpg", with: "_startColor0")
        if let startColorPath0 = Bundle.main.path(forResource: startColorName0, ofType: "txt")
        {
            if let rawdata = try? Data(contentsOf: URL(fileURLWithPath: startColorPath0))
            {
                if let color = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? UIColor
                {
                    origStartColor0 = color
                    //print("origStartColor0:",origStartColor0)
                }
            }
        }
        
        let defColorName1 = currentImageName.replacingOccurrences(of:".jpg", with: "_defaultColor1")
        if let defColorPath1 = Bundle.main.path(forResource: defColorName1, ofType: "txt")
        {
            if let rawdata = try? Data(contentsOf: URL(fileURLWithPath: defColorPath1))
            {
                if let color = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? UIColor
                {
                    defaultLineColor1 = color
                    //print("defaultLineColor1:",defaultLineColor1)
                }
            }
        }
        
        let startColorName1 = currentImageName.replacingOccurrences(of:".jpg", with: "_startColor1")
        if let startColorPath1 = Bundle.main.path(forResource: startColorName1, ofType: "txt")
        {
            if let rawdata = try? Data(contentsOf: URL(fileURLWithPath: startColorPath1))
                //    if let rawdata = try? Data(contentsOf: startColorPath1)
            {
                if let color = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? UIColor
                {
                    origStartColor1 = color
                    //print("origStartColor1:",origStartColor1)
                }
            }
        }
        
        
        //--------------- now save as a Project -------------------
        var targetSize:CGSize = CGSize(width:originalHomeImage.size.width*0.35, height:originalHomeImage.size.height*0.35);
        if (originalHomeImage.size.width < 2000.0) || (originalHomeImage.size.height < 2000.0)
        {
            targetSize = CGSize(width:originalHomeImage.size.width*0.75, height:originalHomeImage.size.height*0.75);
        }
        
        let project = Project.init()
        let projectThumb = ProjectThumb.init()
        let choiceThumb:ProjectThumb = ProjectThumb.init()
        
        project.projectID = projectThumb.projectID
        
        currProject = project;
        currProjectThumb = projectThumb;
        shouldAddToAlbum = false;
        
        //print("  ")
        //print("currProject?.projectID:",currProject?.projectID as Any)
        //print("currProjectThumb?.projectID:",currProjectThumb?.projectID as Any)
        
        //--------- formated like this ----- {"wellNum":0,"wasWell0Sel":1,"wasWell1Sel":0,"wellNumTot":0}
        let paintWell0Sel = currentImageName.replacingOccurrences(of:".jpg", with: "_wasWell0Sel")
        //print("paintWell0Sel:",paintWell0Sel)
        let paintDicWell = self.checkIfPaintWellNumExists(filename:paintWell0Sel)
        //print("paintDicWell:",paintDicWell)
        
        //***************  change 0.0.48 add these 5 lines  ***
        well0tsel = false;
        well1tsel = false;
        well2tsel = false;
        well3tsel = false;
        well4tsel = false;
        
        if paintDicWell["wasWell0Sel"] != nil
        {
            if (paintDicWell["wasWell0Sel"] as? Int == 1)
            {
                well0tsel = true; //***************  change 0.0.48 change ***
            }
        }
        
        if paintDicWell["wasWell1Sel"] != nil
        {
            if (paintDicWell["wasWell1Sel"] as? Int == 1)
            {
                well1tsel = true; //***************  change 0.0.48 change ***
            }
        }
        
        //        if paintDicWell["wasWell2Sel"] != nil //***************  change 0.0.48 don't show for now ***
        //        {
        //            if (paintDicWell["wasWell2Sel"] as? Int == 1)
        //            {
        //                well2tsel = true; //***************  change 0.0.48 change ***
        //            }
        //        }
        
        currentButtonToDisplay = 0
        if paintDicWell["wellNumTot"] != nil
        {
            if (paintDicWell["wellNumTot"] as? Int == 1)
            {
                currentButtonToDisplay = 1;
            }
        }
        
        currentWellNum = 0
        defaultLineColor = defaultLineColor0;
        origStartColor = origStartColor0;
        maskedHouseImage = maskedHouseImage0;
        
        if paintDicWell["wellNum"] != nil
        {
            if (paintDicWell["wellNum"] as? Int == 1)
            {
                currentWellNum = 1;
                defaultLineColor = defaultLineColor1;
                origStartColor = origStartColor1;
                maskedHouseImage = maskedHouseImage1;
            }
        }
        //--------- end formated like this ----- {"wellNum":0,"wasWell0Sel":1,"wasWell1Sel":0,"wellNumTot":0}
        
        
        commingFromProjectList = true;
   
        DispatchQueue.global().async //*************** change 1.0.1 remove background process where ever it occurs ***
        {

                
                autoreleasepool
                    {
                        var thumbImage:UIImage? = nil;
                        
                        UIGraphicsBeginImageContextWithOptions(targetSize, false, 1.0);
                        originalHomeImage.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
                        thumbImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        
                        project.thumbnailData = thumbImage?.jpegData(compressionQuality: 0.50)
                        projectThumb.thumbnailData = project.thumbnailData
                        choiceThumb.thumbnailData = project.thumbnailData;
                        thumbImage = nil;
                }
                //print(" saveProje ctList projectThu mb:",projectTh umb);
                
                project.imageData = originalHomeImage.jpegData(compressionQuality: 1.0)
                project.imageName = currentImageName;
                //        project.wasProces sed = false;
                //print(" saveProje ctList project.imageData:",project.imageData as Any);
                //print(" saveProje ctList originalHom eImage:",originalHom eImage as Any);
                
                project.defaultLineColor0d = defaultLineColor0.encode()
                project.origStartColor0d   = origStartColor0.encode()
                
                project.defaultLineColor1d = defaultLineColor1.encode()
                project.origStartColor1d   = origStartColor1.encode()
                
                if let house0Object = houseObject
                {
                    let encoder = JSONEncoder()
                    project.house0 = try? encoder.encode(house0Object)
                }
                
                project.mask0 = maskedHouseImage0?.pngData()
                project.mask1 = maskedHouseImage1?.pngData()
                
                project.currentWellNum = currentWellNum
                project.currentButtonToDisplay = currentButtonToDisplay
                
                projectList00 = self.loadProjects()
                projectList00.insert(projectThumb, at: 0)
                self.selectedGalleryImage = 0;
                
                //print("1 selected Example projectList00.count:",projectList00.count)
                
                
                curProjectChoiceList = self.loadThumbChoices(projectThumb.projectID) //********** change 1.1.4 change,  needs to be universal ***
                curProjectChoiceList.append(choiceThumb)  //********** change 1.1.4 change,  needs to be universal ***
//print("selected Example curProjectCho iceList:",curProjectCho iceList.count);
                
                //---------------- save main project thumbnails to list ------------------
                do
                {
                    if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                    {
                        let fileURL = dir.appendingPathComponent("projectThumbs.txt")
                        //print("saveHouseTo DocumentDirectory fileURL:",fileURL)
                        let data = try NSKeyedArchiver.archivedData(withRootObject: projectList00, requiringSecureCoding: false)
                        try data.write(to: fileURL)
                        hasBeenSaved = true;
                        //print("projectThumbs    hasBe enSaved:",hasBeenSaved)
                    }
                    
                } catch {
                    print(" ---------------------- selectedEx ample projectThumbs    error:",error)
                }
                
                
                if let projectId = projectThumb.projectID
                {
                    do
                    {
                        //---------------- save main project  ------------------
                        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                        {
                            let fileURL = dir.appendingPathComponent("project_"+projectId+".txt")
                            let currProjectURL = dir.appendingPathComponent("currProject.txt")
                            //print("saveHouseTo DocumentDirectory fileURL:",fileURL)
                            NSKeyedArchiver.setClassName("Project", for: Project.self)
                            
                            let data = try NSKeyedArchiver.archivedData(withRootObject: project, requiringSecureCoding: false)
                            try data.write(to: fileURL)
                            try data.write(to: currProjectURL)
                            //print("saveProje ctList project has Been Saved")
                        }
                        
                    } catch {
                        print(" ---------------------- selectedEx ample    project_  error:",error)
                    }
                    
                 //                    project.imageData = nil;    //*************** change 1.0.1 remove these where ever they occur ***
                    //                    project.siding0 = nil;
                    //                    project.roof0 = nil;


                    
                    do
                    {
                        //---------------- save choices thumbnails to choices list  ------------------
                        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                        {
                            let fileURL = dir.appendingPathComponent("projectThumbs_"+projectId+".txt")
                            //print("saveBut tonAction DocumentDirectory fileURL:",fileURL)
                            let data = try NSKeyedArchiver.archivedData(withRootObject: curProjectChoiceList, requiringSecureCoding: false)   //********** change 1.1.4 change,  needs to be universal ***
                            try data.write(to: fileURL)
                            //print("saveBut tonAction    hasBe enSaved")
                        }
                        
                    } catch {
                        print(" ---------------------- selectedEx ample projec tThumbs_   error:",error)
                    }
                }
                
                if let thumbProjectId = choiceThumb.projectID
                {
                    do
                    {
                        //---------------- save current choice as a project  ------------------
                        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                        {
                            let fileURL = dir.appendingPathComponent("projectChoices_"+thumbProjectId+".txt")
                            //print("saveBut tonAction DocumentDirectory fileURL:",fileURL)
                            NSKeyedArchiver.setClassName("Project", for: Project.self)
                            
                            let data = try NSKeyedArchiver.archivedData(withRootObject: project, requiringSecureCoding: false)
                            try data.write(to: fileURL)
                            //print("saveBut tonAction projectChoices_ has Been Saved")
                        }
                        
                    } catch {
                        print(" ---------------------- selectedEx ample    proje ctChoices  error:",error)
                    }
                }
                
                self.addProjectsChoiceForExample(currProjectThumb)
                
        }
          //  }
    }
    
    //------------------------------------
    func addProjectsChoiceForExample( _ thumb:ProjectThumb?)
    {
//print("addProjectsCho iceExamples")
        if (hasOnlyGarageDoors) //************** change 1.1.0 add if block ***
        {
            return;
        }

        var targetImageRect:CGRect = CGRect(x:0.0, y:0.0, width:1.0, height:1.0);
        
        //-----------  create overlay -----------------
        guard let origImage = originalHomeImage else {return;}
        guard let _ = originalHomeImage.cgImage else {return;}
        
        guard let curThumb = thumb else {return;}
        guard let _ = maskedHouseImage0 else {return;}
        
        screenScale = UIScreen.main.scale
        if (origImage.size.width > 1048) || (origImage.size.height > 1048)
        {
            screenScale = 1.0;
        }
        
        targetImageRect = CGRect(x:0.0, y:0.0, width:origImage.size.width, height:origImage.size.height);
        var over00:UIImage? = nil;
        var over10:UIImage? = nil;
        var over20:UIImage? = nil;
        var over30:UIImage? = nil;
        var over40:UIImage? = nil;
        
        //        let over50:UIImage? = nil;    //************** change 1.3.0 remove ***
        var over60:UIImage? = nil;   //************** change 1.1.0 change to var ***
        var over70:UIImage? = nil;   //************** change 1.1.0 change to var ***
        
        var highlightedImage:UIImage? = nil;
        
        autoreleasepool
        {
                if let mask = maskedHouseImage0
                {
                    UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                    origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
                    mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                    highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    
                    let brightHueFilter = PECbrightHue(origStartColor0, endColor:defaultLineColor0)
                    over00 = highlightedImage?.filterWithOperation(brightHueFilter)
                }
                
                if let mask = maskedHouseImage1
                {
                    UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                    origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
                    mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                    highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    
                    let brightHueFilter = PECbrightHue(origStartColor1, endColor:defaultLineColor1)
                    over10 = highlightedImage?.filterWithOperation(brightHueFilter)
                }
                
                if let mask = maskedHouseImage2
                {
                    UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                    origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
                    mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                    highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    
                    let brightHueFilter = PECbrightHue(origStartColor2, endColor:defaultLineColor2)
                    over20 = highlightedImage?.filterWithOperation(brightHueFilter)
                }
                
                if let mask = maskedHouseImage3
                {
                    UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                    origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
                    mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                    highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    
                    let brightHueFilter = PECbrightHue(origStartColor3, endColor:defaultLineColor3)
                    over30 = highlightedImage?.filterWithOperation(brightHueFilter)
                }
                
                if let mask = maskedHouseImage4
                {
                    UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
                    origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
                    mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
                    highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    
                    let brightHueFilter = PECbrightHue(origStartColor4, endColor:defaultLineColor4)
                    over40 = highlightedImage?.filterWithOperation(brightHueFilter)
                }
                
//                if let mask = garageSingleMaskImage     //************** change 1.1.0 *** put back in with several changes
//                {
//                    UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
//                        origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
//                        mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
//                        highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
//                    UIGraphicsEndImageContext();
//
//                    let brightHueFilter = PECbrightHue(doorStartColor0, endColor:defaultDo orColor0) //*************** change 1.1.0 change ***
//                    over50 = highlightedImage?.filterWithOperation(brightHueFilter)
//                }

//print("addProjectsCho iceExamples maskedDoorImage :",maskedDoorImage as Any)
//print("addProjectsCho iceExamples defaultDoorColor0 :",defaultDoorColor0 as Any)
            	//************* change 1.2.0 add if block
            	if maskedDoorImage?.cgImage != nil     //************** change 1.1.0 *** remove with several changes
				{
//                    UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
//                        origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
//                        mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
//                        highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
//                    UIGraphicsEndImageContext();

                    let brightHueFilter = PECbrightHue(doorStartColor0, endColor:defaultDoorColor0) //*************** change 1.1.0 change ***
                    over60 = maskedDoorImage?.filterWithOperation(brightHueFilter)//*************** change 1.1.0 change ***
                }
            	//************* change 1.2.0 add if block
                if maskedFrontDrImage?.cgImage != nil     //************** change 1.1.0 *** remove with several changes
                {
//                    UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, screenScale);
//                        origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
//                        mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
//                        highlightedImage = UIGraphicsGetImageFromCurrentImageContext();
//                    UIGraphicsEndImageContext();

                    let brightHueFilterFrDr = PECbrightHue(frontDrStartColor0, endColor:defaultFrontDrColor0) //*************** change 1.1.0 change ***
                    over70 = maskedFrontDrImage?.filterWithOperation(brightHueFilterFrDr)//*************** change 1.1.0 change ***
                }
        }
        
        
        
        //print("saveBut tonAction")
        var targetSize:CGSize = CGSize(width:origImage.size.width*0.35, height:origImage.size.height*0.35);
        if (origImage.size.width < 2000.0) || (origImage.size.height < 2000.0)
        {
            targetSize = CGSize(width:origImage.size.width*0.75, height:origImage.size.height*0.75);
        }
        
        shouldSave = false;
        
        let project:Project = Project()
        let projectThumb:ProjectThumb = ProjectThumb.init()
        
        autoreleasepool
        {
                var thumbImage:UIImage? = nil;
                
                UIGraphicsBeginImageContextWithOptions(targetSize, false, 1.0);
                origImage.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
                if (well0tsel) {over00?.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));} //***************  change 0.0.48 change  ***
                if (well1tsel) {over10?.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));} //***************  change 0.0.48 change  ***
                if (well2tsel) {over20?.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));} //***************  change 0.0.48 change  ***
                if (well3tsel) {over30?.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));} //***************  change 0.0.48 change  ***
                if (well4tsel) {over40?.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));} //***************  change 0.0.48 change  ***
                
            //      over50?.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));    //************** change 1.3.0 remove ***
                over60?.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
                over70?.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
                
                thumbImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
                project.thumbnailData = thumbImage?.jpegData(compressionQuality: 0.50)
                projectThumb.thumbnailName = curThumb.thumbnailName
                projectThumb.thumbnailData = project.thumbnailData
                thumbImage = nil;
        }
        
      //  project.imageData = nil
        
        project.defaultLineColor0d = defaultLineColor0.encode()
        project.origStartColor0d   = origStartColor0.encode()
        
        project.defaultLineColor1d = defaultLineColor1.encode()
        project.origStartColor1d   = origStartColor1.encode()
        
        project.defaultLineColor2d = defaultLineColor2.encode()
        project.origStartColor2d   = origStartColor2.encode()
        
        project.defaultLineColor3d = defaultLineColor3.encode()
        project.origStartColor3d   = origStartColor3.encode()
        
        project.defaultLineColor4d = defaultLineColor4.encode()
        project.origStartColor4d   = origStartColor4.encode()
        
//print("2 addProjectsCho iceExamples defaultDo orColor0 :",defaultDoorColor0 as Any)

        project.defaultLineGaraged = defaultDoorColor0.encode() //*************** change 1.1.0 change ***
        project.origStartGaraged   = doorStartColor0.encode() //*************** change 1.1.0 change ***
        
        project.defaultLineFrontDrd = defaultFrontDrColor0.encode() //*************** change 1.1.0 change ***
        project.origStartFrontDrd   = frontDrStartColor0.encode() //*************** change 1.1.0 change ***
        
        project.house0 = nil;
        
        project.mask0 = maskedHouseImage0?.pngData()
        project.mask1 = maskedHouseImage1?.pngData()
        project.mask2 = maskedHouseImage2?.pngData()
        project.mask3 = maskedHouseImage3?.pngData()
        project.mask4 = maskedHouseImage4?.pngData()
        
//print(" addProjectsCho iceExamples maskedDo orImage:",maskedDoorImage as Any)
        project.garageMask  = maskedDoorImage?.pngData()    //************** change 1.1.0 *** change
        project.garageSMask = theDoorsImage?.pngData()    //************** change 1.1.0 *** change
        project.frontDrMask = maskedFrontDrImage?.pngData()  //************** change 1.1.0 *** change
        
        project.siding0 = nil;
        project.roof0 = nil;
        
//print("  ")
//print(" saveButto nAction proje ct.currentWellNum:",pro ject.currentWellNum)
//print(" saveButt onAction proj ect.currentButtonToDisplay:",pr oject.currentButtonToDisplay)
//print("  ")

        project.well0 = well0tsel;
        project.well1 = well1tsel;
        project.well2 = well2tsel;
        project.well3 = well3tsel;
        project.well4 = well4tsel;
        
        project.currentWellNum = currentWellNum
        project.currentButtonToDisplay = currentButtonToDisplay

   //     curProj ectChoiceList = self.loadThu mbChoices(curThumb.projectID) //********** change 1.1.4 remove,  needs to be universal ***
        curProjectChoiceList.append(projectThumb)  //********** change 1.1.4 change,  needs to be universal ***

//print("addProjectsCho iceExamples curProj ectChoiceList:",curProje ctChoiceList.count);
//print("  ")
//print(" saveButt onAction curProje ctChoiceList:",curProje ctChoiceList)
//print("  ")
//print("  ")
//print(" saveButt onAction self.paintProjec tThumb.projectID:",self.paintPr ojectThumb.projectID as Any)
//print("  ")
        
        if let projectId = curThumb.projectID
        {
            do
            {
                if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                {
                    let fileURL = dir.appendingPathComponent("projectThumbs_"+projectId+".txt")
                    let data = try NSKeyedArchiver.archivedData(withRootObject: curProjectChoiceList, requiringSecureCoding: false)   //********** change 1.1.4 change,  needs to be universal ***
                    try data.write(to: fileURL)
                }
                
            } catch {
                print(" ---------------------- saveButto nAction proje ctThumbs_    error:",error)
            }
            
            if let thumbProjectId = projectThumb.projectID
            {
                do
                {
                    if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                    {
                        let fileURL = dir.appendingPathComponent("projectChoices_"+thumbProjectId+".txt")
                        NSKeyedArchiver.setClassName("Project", for: Project.self)
                        
                        let data = try NSKeyedArchiver.archivedData(withRootObject: project, requiringSecureCoding: false)
                        try data.write(to: fileURL)
                    }
                    
                } catch {
                    print(" ---------------------- saveButto nAction    proje ctChoices_      error:",error)
                }
            }
        }
    }
    
    //********** change 0.0.40 add ***
    //-------------------------------------
    func checkIfPaintWellNumExists(filename:String) -> [String:Any]
    {
        let temp = filename.replacingOccurrences(of:".txt", with: "")
        
        if let filePath = Bundle.main.path(forResource:temp, ofType: "txt")
        {
            if FileManager.default.fileExists(atPath:filePath)
            {
                if let contents = try? String(contentsOfFile:filePath)
                {
                    let data :Data = contents.data(using: String.Encoding.utf8)! as Data
                    
                    if let jsonArray = try? JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String:Any]
                    {
                        return jsonArray
                    }
                }
            }
        }
        
        return [:]
    }
}

//********** change 0.0.40 add ***
//--------------------------------------------------------
extension Response
{
    func repairLinkedList()
    {
        //print(" ---------------------- repairLinkedList---------------------")
        self.p1.lt = p6;
        self.p1.nx = p2;
        
        self.p2.lt = p1;
        self.p2.nx = p3;
        
        self.p3.lt = p2;
        self.p3.nx = p4;
        
        self.p4.lt = p3;
        self.p4.nx = p5;
        
        self.p5.lt = p4;
        self.p5.nx = p6;
        
        self.p6.lt = p5;
        self.p6.nx = p1;
        
        //print("self.v1:",self.v1)
        //print("self.roisouter:",self.roisouter)
        //print("self.imageID_db:",self.imageID_db)
        
        if (self.wasPoly)
        {
            if let image = originalHomeImage
            {
                self.v1 = CIVector(x: self.p1.pt.x, y: image.size.height-self.p2.pt.y)
                self.v2 = CIVector(x: self.p4.pt.x, y: image.size.height-self.p3.pt.y)
                self.v3 = CIVector(x: self.p5.pt.x, y: image.size.height-self.p5.pt.y)
                self.v4 = CIVector(x: self.p6.pt.x, y: image.size.height-self.p6.pt.y)
            }
            
            self.path.removeAllPoints()
            self.path.move(to:self.p1.pt)
            self.path.addLine(to:self.p2.pt)
            self.path.addLine(to:self.p3.pt)
            self.path.addLine(to:self.p4.pt)
            self.path.addLine(to:self.p5.pt)
            self.path.addLine(to:self.p6.pt)
            self.path.close()
            self.rect = self.path.cgPath.boundingBox
            
        } else if (self.wasArch) {
            self.p5.nx = self.p1;
            self.p1.lt = self.p5;
            
            let ratiox =  (self.p2.pt.x - self.p1.pt.x) / (self.p3.pt.x - self.p1.pt.x)
            let posy =  ((self.p3.pt.y - self.p1.pt.y) * ratiox) + self.p1.pt.y
            let dify =  min(self.p2.pt.y - posy,0.0)
            let difx =  (self.p2.pt.x - self.p1.pt.x) / 3.0
            
            if (self.roisinner.count < 4)
            {
                self.roisinner = [[Int(round(self.p1.pt.x)), Int(round(self.p1.pt.y))], [Int(round(self.p3.pt.x)), Int(round(self.p3.pt.y))], [Int(round(self.p4.pt.x)), Int(round(self.p4.pt.y))], [Int(round(self.p5.pt.x)), Int(round(self.p5.pt.y))]]
            }
            
            if (self.roisouter.count < 2)
            {
                self.roisouter = [[Int(round(self.p1.pt.x + difx)), Int(round(self.p1.pt.y + dify))], [Int(round(self.p3.pt.x - difx)), Int(round(self.p3.pt.y + dify))]]
            }
            
            if let image = originalHomeImage
            {
                self.v1 = CIVector(x: self.p1.pt.x, y: image.size.height-(self.p1.pt.y + dify))
                self.v2 = CIVector(x: self.p3.pt.x, y: image.size.height-(self.p3.pt.y + dify))
                self.v3 = CIVector(x: self.p4.pt.x, y: image.size.height-self.p4.pt.y)
                self.v4 = CIVector(x: self.p5.pt.x, y: image.size.height-self.p5.pt.y)
            }
            
            self.path.removeAllPoints()
            
            var theArc:Arctype = Arctype()
            theArc.bpoint = Apoint(Float(self.p1.pt.x), Float(self.p1.pt.y))
            theArc.mpoint = Apoint(Float(self.p2.pt.x), Float(self.p2.pt.y))
            theArc.epoint = Apoint(Float(self.p3.pt.x), Float(self.p3.pt.y))
            self.path = PECperspcCorrect.drawArc(theArc)
            
            self.path.addLine(to:self.p3.pt)
            self.path.addLine(to:self.p4.pt)
            self.path.addLine(to:self.p5.pt)
            self.path.close()
            
            let topPath2 = UIBezierPath.init()
            topPath2.move(to: CGPoint.init(x:CGFloat(self.v1.x), y: CGFloat(self.p1.pt.y + dify)))
            topPath2.addLine(to: CGPoint.init(x:CGFloat(self.v2.x), y: CGFloat(self.p3.pt.y + dify)))
            topPath2.addLine(to: CGPoint.init(x:CGFloat(self.v3.x), y: CGFloat(self.p4.pt.y)))
            topPath2.addLine(to: CGPoint.init(x:CGFloat(self.v4.x), y: CGFloat(self.p5.pt.y)))
            topPath2.close()
            topPath2.append(self.path)
            
            self.rect = topPath2.cgPath.boundingBox
            
        } else {
            self.p4.nx = self.p1;
            self.p1.lt = self.p4;
            
            if let image = originalHomeImage
            {
                self.v1 = CIVector(x: self.p1.pt.x, y: image.size.height-self.p1.pt.y)
                self.v2 = CIVector(x: self.p2.pt.x, y: image.size.height-self.p2.pt.y)
                self.v3 = CIVector(x: self.p3.pt.x, y: image.size.height-self.p3.pt.y)
                self.v4 = CIVector(x: self.p4.pt.x, y: image.size.height-self.p4.pt.y)
            }
            
            self.path.removeAllPoints()
            self.path.move(to:self.p1.pt)
            self.path.addLine(to:self.p2.pt)
            self.path.addLine(to:self.p3.pt)
            self.path.addLine(to:self.p4.pt)
            self.path.close()
            self.rect = self.path.cgPath.boundingBox
        }
        
        //print("2 self.v1:",self.v1)
        //print("2 self.roisouter:",self.roisouter)
        //print("2 self.imageID_db:",self.imageID_db)
        //print("2 self.labels:",self.labels)
    }
}






