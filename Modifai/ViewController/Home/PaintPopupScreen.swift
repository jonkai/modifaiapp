//
//  PaintPopupScreen.swift
//  Modifai
//
//  Created by gcsadmin on 29/02/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit
class PaintPopupScreen: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    var paintImgName:String!

    var mainImage:UIImage!
    var paintUrls:Array<URL> = Array<URL>()
    var paint1Urls:Array<URL> = Array<URL>()
    var allImagesArray:Array<[String:Any]> = Array<[String:Any]>()
    @IBOutlet weak var imgCollectionView:UICollectionView!

    override func viewDidLoad() {
       
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.imgCollectionView.backgroundColor = UIColor.clear

    }
    override func viewDidAppear(_ animated: Bool) {
        
       
        if paintUrls.count > 0
        {
         LoadingIndicatorView.show()
        self.getchangedImage()
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allImagesArray.count + 1
    }
    override var prefersStatusBarHidden: Bool
       {
           return true
       }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let multipleHomeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PaintPopUpCell", for: indexPath) as! PaintPopUpCell
        multipleHomeCell.imgView.layer.cornerRadius = 5.0
        multipleHomeCell.imgView.clipsToBounds = true
        multipleHomeCell.imgView.layer.borderColor =  UIColor.white.cgColor
        multipleHomeCell.imgView.layer.borderWidth = 0.0
        
        // 03/19 start
        if indexPath.item == 0 {
            multipleHomeCell.imgView.image = originalHomeImage

            multipleHomeCell.deleteBtn.isHidden = true
        }else {
            if indexPath.item == 1 && imageNames.contains(paintImgName)
            {
                multipleHomeCell.deleteBtn.isHidden = true
            }else
            {
                multipleHomeCell.deleteBtn.isHidden = false

            }
            let dic = allImagesArray[indexPath.item-1]

            multipleHomeCell.imgView.image = dic["image"] as? UIImage
            multipleHomeCell.deleteBtn.tag = indexPath.item-1

        }
        multipleHomeCell.imgView.contentMode = .scaleAspectFit

        multipleHomeCell.deleteBtn.addTarget(self, action: #selector(deleteBtnAction(sender:)), for: .touchUpInside)
        // end
        return multipleHomeCell
        
    }

	//-----------------------------------------------------------------
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        self.dismiss(animated:true, completion:
            {
                currentPaintImageController.editSelectedImage = true
                currentPaintImageController.isPaintChanged = true  //*************** change 0.0.31 add ***
                if indexPath.item > 0
                {
                    let dic = self.allImagesArray[indexPath.row-1]
                    var name = dic["name"] as? String
                    name = name?.replacingOccurrences(of:".png", with:"")
                    let nameArray = name?.components(separatedBy:"_paint0")
                    
                    hidingViewWasPreferenceContoller = true;  //******* this is a fix ***** basically you are reseting everything which is bad ***
                    if nameArray!.count > 1 && indexPath.item > 0
                    {
//print("collectionView before loadPrevio usPaintImage0")
              //          currentPaintImageController.loadPreviousPaintImage(nameType:"\(nameArray![1])")
                    }
                    
                } else {
//print("collectionView before loadPrevio usPaintImage1")
              //      currentPaintImageController.loadPreviousPaintImage(nameType:"original")
                }
        })
    }

    @IBAction func backButtonAction(_ sender: Any)
    {
        self.dismiss(animated:true, completion:nil)
    }
        
    func checkPaint1AVailable(paintName:String)->String
    {
        
        let renamed = paintName.replacingOccurrences(of:"_paint0", with: "_paint1")
        let filterArray = paint1Urls.filter({$0.absoluteString.contains(renamed)})
        if filterArray.count > 0
        {
            return filterArray[0].absoluteString
        }
        return ""
    }
    func getchangedImage()
    {
        self.paintUrls =  paintUrls.sorted(by: { $0.absoluteString.compare(
            $1.absoluteString, options: .numeric) == .orderedAscending
        })
       
        for i in 0 ..< paintUrls.count
        {
            let paint = paintUrls[i]
        let refImage = mainImage
            var fullRefImage:UIImage!
            var paintRefImage:UIImage = UIImage.init();
            var paintRefImage1:UIImage = UIImage.init();
            
            let paint0Name = paint.lastPathComponent

                if let paintImageOverlay = UIImage(contentsOfFile: paint.path)
                {
                    paintRefImage = paintImageOverlay;
                }
            
            
           for paint1 in paint1Urls
           {
            let renamed = paint0Name.replacingOccurrences(of:"_paint0", with: "_paint1")
            
                  if paint1.absoluteString.contains(renamed)
                  {
                    if let paintImageOverlay1 = UIImage(contentsOfFile: paint1.path)
                    {
                        paintRefImage1 = paintImageOverlay1;
                    }
                 }
           }
            
         
            
            let hostRect = CGRect.init(x: 0, y: 0, width: refImage!.size.width, height: refImage!.size.height)
            
            UIGraphicsBeginImageContext(CGSize.init(width: hostRect.size.width, height: hostRect.size.height))
            refImage!.draw(in: hostRect)
            paintRefImage.draw(in: hostRect)
            paintRefImage1.draw(in: hostRect)
            fullRefImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            let dic = ["image":fullRefImage!,"name":paint0Name] as [String : Any]
            allImagesArray.append(dic)
            if paintUrls.count == allImagesArray.count
            {
            LoadingIndicatorView.hide()
            }
        }
       
        imgCollectionView.reloadData()
    }
    
    // 03/19
    @objc func deleteBtnAction(sender:UIButton)  {
        let indexValue = sender.tag
        currentPaintImageController.editSelectedImage = true
        var dic1:NSDictionary
        if self.allImagesArray.count>indexValue {
            
                   
            
            dic1 = self.allImagesArray[indexValue] as NSDictionary
            var name1 = dic1["name"] as? String
            name1 = name1?.replacingOccurrences(of:".png", with:"")
           // let nameArray = name1?.components(separatedBy:"_paint0")
            allImagesArray.remove(at: indexValue)
            guard let name = dic1["name"] as? String else { return }
            deleteMaskFromLocalPath(pathFile: name)
            let paint1name = checkPaint1AVailable(paintName:name)
            if paint1name.count > 0
            {
                deleteMaskFromLocalPath(pathFile: paint1name)

            }
            imgCollectionView.reloadData()
           //*************** change 0.0.32 remove all,  why reload a previous when there is already paint in controller ***
        }
        
    }
    
    func deleteMaskFromLocalPath(pathFile:String)  {
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        if paths.count > 0 {
            let dirPath = paths[0]
            
            //let filePath = NSString(format:"%@/%@", dirPath, pathFile) as String
            let maskFile = pathFile.replacingOccurrences(of:"_paint", with: "_paintMask")
            
            var startColor = pathFile.replacingOccurrences(of:"_paint", with: "_startColor")
            startColor = startColor.replacingOccurrences(of: ".png", with: ".txt")
            
            var defaultColor = pathFile.replacingOccurrences(of:"_paint", with: "_defaultColor")
            defaultColor = defaultColor.replacingOccurrences(of: ".png", with: ".txt")
            
            var wasWellTxt = pathFile.replacingOccurrences(of:"_paint", with: "_wasWell0Sel")
            wasWellTxt = wasWellTxt.replacingOccurrences(of: ".png", with: ".txt")
            
                        let paintNamewith = pathFile.replacingOccurrences(of:".png", with: "--")
            let paintNameser = pathFile.replacingOccurrences(of:".png", with: "")

           if hasInternalApp
           {
            DataManager.removePaintImageFromServer(filename:paintNameser, paintURL:paintNamewith)
            }
            let filesToremove1 = [pathFile,maskFile,startColor,defaultColor,wasWellTxt]
            for file in filesToremove1
            {
                let filePath = NSString(format:"%@/%@", dirPath, file) as String
                
                // if FileManager.default.fileExists(atPath: filePath) {
                
                do {
                    try FileManager.default.removeItem(atPath: filePath)
                    print("file has been removed: ",file)
                } catch {
                    print("an error during a removing: ",file)
                }
                // }
            }
        }
    }
    // end
}


