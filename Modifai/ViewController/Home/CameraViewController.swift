import UIKit
import AVFoundation
import Photos
import CoreLocation
import MobileCoreServices
import PECimage
import FirebaseAnalytics
//import CoreMotion
//import simd

var locatManager:CLLocationManager!
var photoButtonG: UIButton!
var turnDeviceG: UILabel!
var focusTapG: UITapGestureRecognizer!
var dontShowTurnLabel:Bool = false;
var lastZoomFactor: CGFloat = 1.75

//-------------------------------------------------------------------------------
protocol ModifiCameraVCDelegate: class
{
//    func sendCap tureImag2(img: UIImage)    //***************** change 0.444.7 is this being used??? remove all if not  ***
    func sendCaptureImag()
    func dismissCamaraVC()
}
//-------------------------------------------------------------------------------
class CameraViewController: UIViewController, UINavigationControllerDelegate, AVCaptureFileOutputRecordingDelegate,
    UIImagePickerControllerDelegate, CLLocationManagerDelegate, UIGestureRecognizerDelegate
{
    private enum SessionSetupResult
    {
        case success
        case notAuthorized
        case configurationFailed
    }
     weak var delegate: ModifiCameraVCDelegate?
    private let photoOutput = AVCapturePhotoOutput()
    private let session = AVCaptureSession()
    private let sessionQueue = DispatchQueue(label: "session queue")
    //    private let videoDeviceDisc overySession = AVCapt ureDevice.DiscoverySession(deviceTypes: [.buil tInWideAngleCamera, .buil tInDualCamera],
    //                                                                               mediaType: .video, position: .unspecified)
    //    private let videoDeviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera],
    //                                                                               mediaType: .video, position: .back)

    private var isSessionRunning = false
    private var setupResult: SessionSetupResult = .success
    private var inProgressPhotoCaptureDelegates = [Int64: PhotoCaptureProcessor]()
    private var keyValueObservations = [NSKeyValueObservation]()
    private var inProgressLivePhotoCapturesCount = 0
    private var movieFileOutput: AVCaptureMovieFileOutput?
    private var backgroundRecordingID: UIBackgroundTaskIdentifier?

    //------- processing screen effects -------
    let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular) //prominent,regular,extraLight, light, dark
    var blurEffectView = UIVisualEffectView()

    //---------- zoom effect --------
    let minimumZoom: CGFloat = 1.0
    let maximumZoom: CGFloat = 3.0
   var lastZoomFactor: CGFloat = 1.75

    var videoDeviceInput: AVCaptureDeviceInput!
    var pickerController = UIImagePickerController()

    @IBOutlet private weak var previewView: PreviewView!

    @IBOutlet private weak var cameraUnavailableLabel: UILabel!
    @IBOutlet private weak var photoButton: UIButton!
    @IBOutlet private weak var libraryButton: UIButton!
    @IBOutlet private weak var resumeButton: UIButton!
    @IBOutlet weak var turnDevice: UILabel!
    @IBOutlet weak var processingView: UIView!

    @IBOutlet var focusTap: UITapGestureRecognizer!


    //--------------------------------------------------------------------------------------------
      override var prefersStatusBarHidden: Bool {
           return true
       }

    // MARK: - methods
    //-------------------------------------------------------------------------------
    override func viewDidLoad()
    {
        //print("CameraVie wController viewDidLoad")
        super.viewDidLoad()

        locatManager = CLLocationManager()
        locatManager.delegate = self
        locatManager.desiredAccuracy = kCLLocationAccuracyBest

        photoButton.isEnabled = false
        photoButtonG = self.photoButton;
        turnDeviceG = self.turnDevice;
        focusTapG = self.focusTap;
        dontShowTurnLabel = false;

        previewView.session = session

        pickerController.modalPresentationStyle = .currentContext
        pickerController.delegate = self //as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        dontRotate = false;
//print("viewDidLoad dontRotate:",dontRotate)

        //print("viewDidLoad before checkCameraAthorization")
        switch AVCaptureDevice.authorizationStatus(for: .video)
        {
        case .authorized:
            self.checkPhotoLibraryAthorization();
            self.setupResult = .success
            break

        case .notDetermined:
            sessionQueue.suspend()
            AVCaptureDevice.requestAccess(for: .video, completionHandler:
                { granted in
                    if !granted
                    {
                        self.setupResult = .notAuthorized
                    } else {
                        //print("checkCameraAthorization before checkPhotoLibraryAthorization")
                        self.checkPhotoLibraryAthorization();
                    }
                    self.sessionQueue.resume()
                    //print("checkCameraAthorization after self.sessionQueue.resume")
            })

        default:
            self.setupResult = .notAuthorized
        }

        //print("checkCameraAthorization before sessionQueue.async")
        sessionQueue.async
            {
                //print("checkCameraAthorization before configureSession")
                self.configureSession()
        }
    }

    //-------------------------------------------------------------------------------
    func checkPhotoLibraryAthorization()
    {
        //print("checkPhotoLibraryAthorization")
        if PHPhotoLibrary.authorizationStatus() != .authorized
        {
            PHPhotoLibrary.requestAuthorization(
                {(status:PHAuthorizationStatus) in
                    switch status
                    {
                    case .authorized:
                        //print("PhotoLibrary has been authorized")
                        break
                    default:
                        //print("PhotoLibrary not authorized in some way")
                        break
                    }
            })
        } else {
            //print("PhotoLibrary already has been authorized")
        }
    }

    //-------------------------------------------------------------------------------
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.turnDevice.isHidden = true;
        self.processingView.isHidden = true;
        self.focusTap.isEnabled = true;

        sessionQueue.async
            {
                switch self.setupResult
                {
                case .success:
                    //print("about to addObservers")
                    self.addObservers()
                    self.session.startRunning()
                    self.isSessionRunning = self.session.isRunning

                case .notAuthorized:
                    DispatchQueue.main.async
                        {
                            //print("tell user that photo taking is failing because no authorization to use Camera yet")
                            let changePrivacySetting = "This App doesn't have permission to use the camera, change privacy settings to access camera"
                            let message = NSLocalizedString(changePrivacySetting, comment: "Alert message when the user has denied access to the camera")
                        //Analytics.logEvent("Alert_This_App_doesn't_have_permission_to_use_the_camera_change_ privacy_settings_to_access_camera", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
                            let alertController = UIAlertController(title: "App not Authorized to use Camera", message: message, preferredStyle: .alert)

                            alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"),
                                                                    style: .cancel,
                                                                    handler:
                                { _ in
                                    self.needToAbandonCameraNotAuthorized()
                            }))

                            alertController.addAction(UIAlertAction(title: NSLocalizedString("Settings", comment: "Alert button to open Settings"),
                                                                    style: .`default`,
                                                                    handler:
                                { _ in

                                    DispatchQueue.main.async
                                        {
                                            let onBoardingStory = UIStoryboard(name: "CustomTabBar", bundle: nil)
                                            let onBoardVC = onBoardingStory.instantiateViewController(withIdentifier: "CustomTabBarVC") as! CustomTabBarVC

                                             if let keyWindow = UIWindow.key {
                                                                                     keyWindow.rootViewController?.dismiss(animated: false, completion: nil)
                                                                                     keyWindow.rootViewController = onBoardVC
                                                                                     }

                                    }

                                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                            }))


							if let popoverController = alertController.popoverPresentationController  //********** change 3.0.3 add if block ***
							{
							  popoverController.sourceView = self.view
							  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
							  popoverController.permittedArrowDirections = []
							}

                           self.present(alertController, animated: true, completion: nil)
                    }

                case .configurationFailed:
                    DispatchQueue.main.async
                        {
                            let alertMsg = "Alert message when something goes wrong during capture session configuration"
                            let message = NSLocalizedString("Unable to capture media", comment: alertMsg)
                            //Analytics.logEvent("Alert_Unable_to_capture_media", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
                            let alertController = UIAlertController(title: "App not able to capture media", message: message, preferredStyle: .alert)

                            alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"),
                                                                    style: .cancel,
                                                                    handler: nil))

							if let popoverController = alertController.popoverPresentationController  //********** change 3.0.3 add if block ***
							{
							  popoverController.sourceView = self.view
							  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
							  popoverController.permittedArrowDirections = []
							}

                            self.present(alertController, animated: true, completion: nil)
                    }
                }
        }

        self.checkUsersLocationServicesAuthorization()
    }

    //-------------------------------------------------------------------------------
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)

        //if let appDelegate = (UIApplication.shared.delegate as? AppDelegate)
        //{
        //appDelegate.startUpdates()
        //}
    }

    //-------------------------------------------------------------------------------
    override func viewWillDisappear(_ animated: Bool)
    {
        locatManager.stopUpdatingLocation()
        locatManager.stopUpdatingHeading()

        sessionQueue.async
            {
                if self.setupResult == .success
                {
                    self.session.stopRunning()
                    self.isSessionRunning = self.session.isRunning
                    self.removeObservers()
                }
        }

        super.viewWillDisappear(animated)
    }

    //-------------------------------------------------------------------------------
    override var shouldAutorotate: Bool
    {
//print("shouldAutorot ate: dontR otate",dontR otate)
        if (dontRotate)
        {
            return false
        }

        if let movieFileOutput = movieFileOutput
        {
            return !movieFileOutput.isRecording
        }
        return true
    }

    //-------------------------------------------------------------------------------
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask
    {
        return .all
    }

    //-------------------------------------------------------------------------------
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        //print("CameraVie wController viewWillTransition")
        super.viewWillTransition(to: size, with: coordinator)

        if let videoPreviewLayerConnection = previewView.videoPreviewLayer.connection
        {
            let deviceOrientation = UIDevice.current.orientation
            guard let newVideoOrientation = AVCaptureVideoOrientation(deviceOrientation: deviceOrientation),
                deviceOrientation.isPortrait || deviceOrientation.isLandscape else
            {
                return
            }

            //            if ((newVideoOrientation == .portrait) || (newVideoOrientation == .portraitUpsideDown))
            //            {
            //                self.turnDe vice.isHidden = false;
            //                self.phot oButton.isEnabled = false;
            //                self.phot oButton.alpha = 0.85;
            //            } else {
            //                self.turnDe vice.isHidden = true;
            //                self.phot oButton.isEnabled = true;
            //                self.phot oButton.alpha = 1.0;
            //            }

            videoPreviewLayerConnection.videoOrientation = newVideoOrientation
        }
    }

    //------------------------------------------------------------
    func needToAbandonCameraNotAuthorized()
    {
        DispatchQueue.main.async
            {

                let onBoardingStory = UIStoryboard(name: "CustomTabBar", bundle: nil)
                let onBoardVC = onBoardingStory.instantiateViewController(withIdentifier: "CustomTabBarVC") as! CustomTabBarVC
                if let snapshot: UIView = self.view.window!.snapshotView(afterScreenUpdates: false)
                {
                    onBoardVC.view.addSubview(snapshot)

                     if let keyWindow = UIWindow.key {
                    keyWindow.rootViewController?.dismiss(animated: false, completion: nil)
                    }
               //     UIApplication.shared.keyWindow?.rootViewController = onBoardVC

                    UIView.animate(withDuration: 1.0, animations:
                        {() in
                            snapshot.layer.opacity = 0.0
                    }, completion: {(completion) in
                        snapshot.removeFromSuperview()
                    })
                } else {
                    if let keyWindow = UIWindow.key {
                    keyWindow.rootViewController?.dismiss(animated: false, completion: nil)
                    }
                    //UIApplication.shared.keyWindow?.rootViewController = onBoardVC
                }
        }
    }

    //------------------------------------------------------------------------------------
    func checkUsersLocationServicesAuthorization()
    {
        if CLLocationManager.locationServicesEnabled()
        {
            switch CLLocationManager.authorizationStatus()
            {
            case .notDetermined:
                locatManager.requestWhenInUseAuthorization()
                break

            case .restricted, .denied:
                //print("Location Access denied")
                break

            case .authorizedWhenInUse, .authorizedAlways:
                //print("Location Access Available")
                locatManager.startUpdatingLocation()
                locatManager.startUpdatingHeading()
                break
			@unknown default:
                break;
			}
        } else {
            //print("Entire device is turned off for location")
        }
    }

    //------------------------------------------------------------------------------------
    func locationManager(_ manager:CLLocationManager, didUpdateLocations locations:[CLLocation])
    {
        //print("locations = \(locations)")
        //    //    gpsResult.text = "success"
        //
        //        if CLLocationManager.headingAvailable()  //headingAvailable() //UIDeviceOrientation
        //        {
        ////            let currentOrientation = UIDevice.current.orientation;
        ////            self.locationManager.headingOrientation = (CLDeviceOrientation)currentOrientation;
        //print("self.locationManager.headingOrientation = \(locatM anager.headingOrientation)")
        //print("self.locationManager.heading",locatM anager.heading as Any)
        //print("self.locationManager.heading?.trueHeading",locatM anager.heading?.trueHeading as Any)
        //
        //            //    if (locationHeading) [metadata setHeading:locationHeading];
        //
        //        }
        //
        //        if CLLocationManager.locationServicesEnabled()
        //        {
        ////print("self.locationManager.location",locatM anager.location as Any)
        //print("self.locationManager.location",locatMa nager.location as Any)
        //            //if (location) [metadata setLocation:location];
        //        }
    }

    //------------------------------------------------------------------------------------
    func locationManager(_ manager:CLLocationManager, didUpdateHeading newHeading:CLHeading)
    {
        //print("didUpdateHeading newHeading = \(newHeading)")
    }

    // MARK: Session Management
    //-------------------------------------------------------------------------------
    private func configureSession()
    {
        if setupResult != .success
        {
            return
        }

        session.beginConfiguration()
        session.sessionPreset = .photo

        do
        {
            var defaultVideoDevice: AVCaptureDevice?

            if let backCameraDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back)
            {
                //print("backCameraDevice:",backCameraDevice)
                defaultVideoDevice = backCameraDevice
            } else if let frontCameraDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front) {
                defaultVideoDevice = frontCameraDevice
            }

            //            if let defaultDevice = defaultVideoDevice
            //            {
            //                let FOV = defaultDevice.activeFormat.videoFieldOfView;
            //print("FOV:",FOV)
            //            }


            let videoDeviceInput = try AVCaptureDeviceInput(device: defaultVideoDevice!)

            if session.canAddInput(videoDeviceInput)
            {
                session.addInput(videoDeviceInput)
                self.videoDeviceInput = videoDeviceInput

                DispatchQueue.main.async
                    {
						let deviceOrientation:UIDeviceOrientation = UIDevice.current.orientation;  //********** change 0.0.40 replace

                    //    let statusBarOrientation = UIApplication.shared.statusBarOrientation
                        var initialVideoOrientation: AVCaptureVideoOrientation = .portrait
                        if deviceOrientation != .unknown
                        {
                            if let videoOrientation = AVCaptureVideoOrientation(deviceOrientation: deviceOrientation)
                            {
                                initialVideoOrientation = videoOrientation
                            }
                        }

                        self.previewView.videoPreviewLayer.connection?.videoOrientation = initialVideoOrientation
                }
            } else {
                //print("Could not add video device input to the session")
                setupResult = .configurationFailed
                session.commitConfiguration()
                return
            }
        } catch {
            //print("Could not create video device input: \(error)")
            setupResult = .configurationFailed
            session.commitConfiguration()
            return
        }

        if session.canAddOutput(photoOutput)
        {
            session.addOutput(photoOutput)

            photoOutput.isHighResolutionCaptureEnabled = true
            photoOutput.isLivePhotoCaptureEnabled = false
            photoOutput.isDepthDataDeliveryEnabled = false

        } else {
            //print("Could not add photo output to the session")
            setupResult = .configurationFailed
            session.commitConfiguration()
            return
        }

        session.commitConfiguration()
    }

    //-------------------------------------------------------------------------------
    @IBAction private func resumeInterruptedSession(_ resumeButton: UIButton)
    {
        sessionQueue.async
            {
                self.session.startRunning()
                self.isSessionRunning = self.session.isRunning
                if !self.session.isRunning
                {
                    DispatchQueue.main.async
                        {
                            let message = NSLocalizedString("Unable to resume", comment: "something is going wrong with this camera session running")
                            //Analytics.logEvent("Alert_Unable_to_resume", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
                            let alertController = UIAlertController(title: "Unable to resume", message: message, preferredStyle: .alert)
                            let cancelAction = UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil)
                            alertController.addAction(cancelAction)

							if let popoverController = alertController.popoverPresentationController  //********** change 3.0.3 add if block ***
							{
							  popoverController.sourceView = self.view
							  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
							  popoverController.permittedArrowDirections = []
							}

                            self.present(alertController, animated: true, completion: nil)
                    }
                } else {
                    DispatchQueue.main.async
                        {
                            self.resumeButton.isHidden = true
                    }
                }
        }
    }



    // MARK: Device Configuration
    //-------------------------------------------------------------------------------
    @IBAction private func focusAndExposeTap(_ gestureRecognizer: UITapGestureRecognizer)
    {
        let devicePoint = previewView.videoPreviewLayer.captureDevicePointConverted(fromLayerPoint: gestureRecognizer.location(in: gestureRecognizer.view))
        focus(with: .autoFocus, exposureMode: .autoExpose, at: devicePoint, monitorSubjectAreaChange: true)
    }

    //-------------------------------------------------------------------------------
    private func focus(with focusMode: AVCaptureDevice.FocusMode, exposureMode: AVCaptureDevice.ExposureMode, at devicePoint: CGPoint, monitorSubjectAreaChange: Bool)
    {
        sessionQueue.async
            {
                let device = self.videoDeviceInput.device
                do
                {
                    try device.lockForConfiguration()

                    if device.isFocusPointOfInterestSupported && device.isFocusModeSupported(focusMode)
                    {
                        device.focusPointOfInterest = devicePoint
                        device.focusMode = focusMode
                    }

                    if device.isExposurePointOfInterestSupported && device.isExposureModeSupported(exposureMode)
                    {
                        device.exposurePointOfInterest = devicePoint
                        device.exposureMode = exposureMode
                    }

                    device.isSubjectAreaChangeMonitoringEnabled = monitorSubjectAreaChange
                    device.unlockForConfiguration()
                } catch {
                    //print("Could not lock device for configuration: \(error)")
                }
        }
    }

    //-------------------------------------------------------------------------------
    @IBAction private func pinchRecognizer(_ pinch: UIPinchGestureRecognizer)
    {
        //print("pinchRecognizer")
        guard let device = videoDeviceInput?.device else { return }

        //        let FOV = device.activeFormat.videoFieldOfView;
        //print("pinchRecognizer FOV:",FOV)

        func minMaxZoom(_ factor: CGFloat) -> CGFloat
        {
            return min(min(max(factor, minimumZoom), maximumZoom), device.activeFormat.videoMaxZoomFactor)
        }

        func update(scale factor: CGFloat)
        {
            do
            {
                try device.lockForConfiguration()
                defer { device.unlockForConfiguration() }
                device.videoZoomFactor = factor
            } catch {
                //print("error:  \(error.localizedDescription)")
            }
        }

        let newScaleFactor = minMaxZoom(pinch.scale * lastZoomFactor)

        switch pinch.state
        {
        case .began: fallthrough
        case .changed: update(scale: newScaleFactor)
        case .ended:
            lastZoomFactor = minMaxZoom(newScaleFactor)
            update(scale: lastZoomFactor)
        default: break
        }
    }

    //------------------------------------------------------------------------------------
    func addBlurEffect()
    {
        self.blurEffectView = UIVisualEffectView(effect: self.blurEffect)
        self.blurEffectView.frame = self.view.bounds

        self.blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.previewView.addSubview(self.blurEffectView)
    }

    // MARK: - after Photo is taken/picked
    //-------------------------------------------------------------------------------
	func usePhoto(_ originalImage: UIImage, metadata:Dictionary<String, Any>, asset:PHAsset?)
	{
//print("camera  use Photo")
		//print("metadata:",metadata as Any)
		dontRotate = true;
//print(" 0 useP hoto dont Rot ate:",dontRo tate)

		DispatchQueue.main.async
		{
//               let onBoardingStory = UIStoryboard(name: "CustomTabBar", bundle: nil)
//                let onBoardVC = onBoardingStory.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController

			homeviewController.imgLocation = locatManager.location
			let imageO = originalImage.fixedOrientation()
			originalHomeImage = imageO;
			targetMaskRect.size = originalHomeImage.size;
			homeviewController.imageView.image = originalHomeImage

			homeviewController.metadata = metadata;
			homeviewController.asset = asset;

//			if let asset0 = asset  //************* change add 2.0.6
//			{
				//currentImageName = asset0.originalFilename! //************* change add 2.0.6 adding new name

                currentImageName = "".randomString() //************* change add 2.0.6 adding new name
				currentImageName = currentImageName.changeExtensions(name:currentImageName)
//print("usePhoto original currentImageName:",currentImageName as Any)
		//	}

			self.delegate?.sendCaptureImag()

//                onBoardVC.metadata = metadata;
//                onBoardVC.asset = asset;
//print("usePhoto onBoardVC:",onBoardVC as Any)
//print("usePhoto original Hom Image:",originalHo meImage as Any)
//
//                if let asset0 = asset
//                {
//                    onBoardVC.currentI ageName = asset0.originalFilename!
//                    onBoardVC.currentIm ageName = onBoardVC.currentIma geName.changeExte nsions(name:onBoardVC.currentIm ageName)
//                }

			dontRotate = false;
			self.dismiss(animated:true, completion: nil)
		}
	}

    // MARK: - use Camera
    //-------------------------------------------------------------------------------
    @IBAction private func capturePhoto(_ photoButton: UIButton)
    {
            let videoPreviewLayerOrientation = previewView.videoPreviewLayer.connection?.videoOrientation
            pitch = 0.0;

            sessionQueue.async
                {
                    // Update the photo output's connection to match the video orientation of the video preview layer.
                    if let photoOutputConnection = self.photoOutput.connection(with: .video)
                    {
                        photoOutputConnection.videoOrientation = videoPreviewLayerOrientation!
                    }

                    var photoSettings = AVCapturePhotoSettings()
                    //            if  self.photoOutput.availablePhotoCodecTypes.contains(.hevc)
                    //            {
                    //                let formats = self.photoOutput.supportedPhotoPixelFormatTypes(for: .tif) // else { return }
                    //print("available pixel formats: \(formats)")
                    //
                    //            if (formats.count > 0)
                    //            {
                    //                uncompressedPixelType = formats.first! //else
                    //print("uncompressedPixelType: \(uncompressedPixelType)")
                    //                photoSettings = AVCapturePhotoSettings(format: [kCVPixelBufferPixelFormatTypeKey as String : uncompressedPixelType])
                    //            } else {
                    //print("No raw pixel format types available")
                    photoSettings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])  //AVVideoCodecType.hevc
                    //            }
                    //            }

                    if self.videoDeviceInput.device.isFlashAvailable
                    {
                        photoSettings.flashMode = .off
                    }

                    FOV = self.videoDeviceInput.device.activeFormat.videoFieldOfView
    //print("pinchRecognizer FOV:",FOV)

                    photoSettings.isHighResolutionPhotoEnabled = true
                    if !photoSettings.__availablePreviewPhotoPixelFormatTypes.isEmpty
                    {
                        photoSettings.previewPhotoFormat = [kCVPixelBufferPixelFormatTypeKey as String: photoSettings.__availablePreviewPhotoPixelFormatTypes.first!]
                    }

                    photoSettings.isDepthDataDeliveryEnabled = false

                    // Use a separate object for the photo capture delegate to isolate each capture life cycle.
                    let photoCaptureProcessor = PhotoCaptureProcessor(with: photoSettings, willCapturePhotoAnimation:
                    {
                        //print("photoCaptu reProcessor ")
                        DispatchQueue.main.async
                            {
                                self.turnDevice.isHidden = true;
                                self.libraryButton.isHidden = true;
                                self.focusTap.isEnabled = false;
                                self.photoButton.isEnabled = false;
                                self.photoButton.alpha = 0.85;
                                dontShowTurnLabel = true;


                                if let snapshot: UIView = self.view.window!.snapshotView(afterScreenUpdates: false)
                                {
                                    self.previewView.addSubview(snapshot)
                                    self.processingView.center = self.view.center;
                                    self.view.bringSubviewToFront(self.processingView)

                                    UIView.animate(withDuration: 0.25, animations:
                                        {() in
                                            self.addBlurEffect()
                                            self.processingView.isHidden = false;
                                            self.processingView.layer.opacity = 1.0;
                                            self.previewView.videoPreviewLayer.opacity = 0.0
                                    }, completion: {(completion) in
                                        self.processingView.layer.opacity = 1.0;
                                        self.previewView.videoPreviewLayer.opacity = 1.0
                                    })
                                }
                        }
                    }, livePhotoCaptureHandler: { capturing in

                        //self.sessionQueue.async
                        DispatchQueue.main.async
                            {
                                if capturing
                                {
                                    self.inProgressLivePhotoCapturesCount += 1
                                } else {
                                    self.inProgressLivePhotoCapturesCount -= 1
                                }
                        }
                    }, completionHandler: { photoCaptureProcessor in

                        //self.sessionQueue.async
                        DispatchQueue.main.async
                            {
                                self.inProgressPhotoCaptureDelegates[photoCaptureProcessor.requestedPhotoSettings.uniqueID] = nil
                                if let photoData = photoCaptureProcessor.photoData
                                {
                                    //print("  ")
                                    //print("  ")
                                    //print("photoData = photoCaptureProcessor.photoData ")
                                    if let originalImage = UIImage(data: photoData)
                                    {
                                        self.usePhoto(originalImage,metadata:photoCaptureProcessor.metadataPC, asset:photoCaptureProcessor.assetPC);
                                    }
                                } else {
  print("something went really wrong ")
                                }
                        }
                    }
                    )

                    self.inProgressPhotoCaptureDelegates[photoCaptureProcessor.requestedPhotoSettings.uniqueID] = photoCaptureProcessor
                    self.photoOutput.capturePhoto(with: photoSettings, delegate: photoCaptureProcessor)
            }
        }

    // MARK: - use photo Library
    //-------------------------------------------------------------------------------
    @IBAction func showImagePicker(_ sender: UIButton)
    {
        //print("showImagePicker")
        if PHPhotoLibrary.authorizationStatus() == .authorized
        {
            self.pickImage()
        } else {
            DispatchQueue.main.async
			{
                    //print("tell user that image picking will fail because of no authorization to use photos library yet")
                    let changePrivacySetting = "This App doesn't have permission to save to, or use photos from your library, change privacy settings to access photos"
                    let message = NSLocalizedString(changePrivacySetting, comment: "Alert message when the user has denied access to the photos library")
                //Analytics.logEvent("Alert_This_App_doesn't_have_permission_to_save_to_or_use_photos_from_your_library_change_privacy_settings_to_access_photos", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
                    let alertController = UIAlertController(title: "App not Authorized to save to or use photos library", message: message, preferredStyle: .alert)

                    alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"),
                                                            style: .cancel,
                                                            handler: nil))

                    alertController.addAction(UIAlertAction(title: NSLocalizedString("Settings", comment: "Alert button to open Settings"),
                                                            style: .`default`,
                                                            handler:
                        { _ in
                            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                    }))


					if let popoverController = alertController.popoverPresentationController  //********** change 3.0.3 add if block ***
					{
					  popoverController.sourceView = self.view
					  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
					  popoverController.permittedArrowDirections = []
					}

                    self.present(alertController, animated: true, completion: nil)
            }
        }
    }


    //-------------------------------------------------------------------------------
    func pickImage()
    {
//print("pickImage")
        DispatchQueue.main.async
		{
                self.pickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
                self.pickerController.modalPresentationStyle = UIModalPresentationStyle.popover
                if let presentationController = self.pickerController.popoverPresentationController  //********** change 3.0.3 and and change if block ***
                {
                	presentationController.permittedArrowDirections = UIPopoverArrowDirection.any
  					presentationController.sourceView = self.view
  					presentationController.sourceRect = self.view.bounds

                	self.present(self.pickerController, animated: true, completion:
					{
                	})
                }
        }
    }

    //-------------------------------------------------------------------------------
    func fileOutput(_ output: AVCaptureFileOutput, didStartRecordingTo fileURL: URL, from connections: [AVCaptureConnection])
    {
        //print("didStartRecordingTo  URL used in this app")
    }

    //-------------------------------------------------------------------------------
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?)
    {
        //print("didFinishRecordingTo outputFileURL  URL used in this app")

        //-------------------------------------------------------------------------------
        func cleanUp()
        {
            let path = outputFileURL.path
            if FileManager.default.fileExists(atPath: path)
            {
                do
                {
                    try FileManager.default.removeItem(atPath: path)
                } catch {
                    //print("Could not remove file at url: \(outputFileURL)")
                }
            }

            if let currentBackgroundRecordingID = backgroundRecordingID
            {
                backgroundRecordingID = UIBackgroundTaskIdentifier(rawValue: convertFromUIBackgroundTaskIdentifier(UIBackgroundTaskIdentifier.invalid))

                if currentBackgroundRecordingID != UIBackgroundTaskIdentifier.invalid
                {
                    UIApplication.shared.endBackgroundTask(currentBackgroundRecordingID)
                }
            }
        }

        var success = true

        if error != nil
        {
            //print("Movie file finishing error: \(String(describing: error))")
            success = (((error! as NSError).userInfo[AVErrorRecordingSuccessfullyFinishedKey] as AnyObject).boolValue)!
        }

        if success
        {
            PHPhotoLibrary.requestAuthorization
                { status in
                    if status == .authorized
                    {
                        // Save the movie file to the photo library and cleanup.
                        PHPhotoLibrary.shared().performChanges(
                            {
                                let options = PHAssetResourceCreationOptions()
                                options.shouldMoveFile = true
                                let creationRequest = PHAssetCreationRequest.forAsset()
                                creationRequest.addResource(with: .video, fileURL: outputFileURL, options: options)
                        }, completionHandler: { success, error in
                            if !success
                            {
                                //print("Could not save movie to photo library: \(String(describing: error))")
                            }
                            cleanUp()
                        }
                        )
                    } else {
                        cleanUp()
                    }
            }
        } else {
            cleanUp()
        }
    }

    // MARK: KVO and Notifications
    //-------------------------------------------------------------------------------
    private func addObservers()
    {
        let keyValueObservation = session.observe(\.isRunning, options: .new)
        { _, change in
            guard let isSessionRunning = change.newValue else { return }

            DispatchQueue.main.async
                {
                    self.photoButton.isEnabled = isSessionRunning
                    //print("add Observers self.photoButton",self.photoButton)
            }
        }
        keyValueObservations.append(keyValueObservation)

        NotificationCenter.default.addObserver(self, selector: #selector(subjectAreaDidChange), name: .AVCaptureDeviceSubjectAreaDidChange, object: videoDeviceInput.device)
        NotificationCenter.default.addObserver(self, selector: #selector(sessionRuntimeError), name: .AVCaptureSessionRuntimeError, object: session)
        NotificationCenter.default.addObserver(self, selector: #selector(sessionWasInterrupted), name: .AVCaptureSessionWasInterrupted, object: session)
        NotificationCenter.default.addObserver(self, selector: #selector(sessionInterruptionEnded), name: .AVCaptureSessionInterruptionEnded, object: session)
    }

    //-------------------------------------------------------------------------------
    private func removeObservers()
    {
        NotificationCenter.default.removeObserver(self)

        for keyValueObservation in keyValueObservations
        {
            keyValueObservation.invalidate()
        }
        keyValueObservations.removeAll()
        //print("remove Observers self.photoButton",self.photoButton)
    }

    //-------------------------------------------------------------------------------
    @objc func subjectAreaDidChange(notification: NSNotification)
    {
        let devicePoint = CGPoint(x: 0.5, y: 0.5)
        focus(with: .continuousAutoFocus, exposureMode: .continuousAutoExposure, at: devicePoint, monitorSubjectAreaChange: false)
    }

    //-------------------------------------------------------------------------------
    @objc func sessionRuntimeError(notification: NSNotification)
    {
        guard let error = notification.userInfo?[AVCaptureSessionErrorKey] as? AVError else
        { return }

        //print("Capture session runtime error: \(error)")
        if error.code == .mediaServicesWereReset
        {
            sessionQueue.async
                {
                    if self.isSessionRunning
                    {
                        self.session.startRunning()
                        self.isSessionRunning = self.session.isRunning
                    } else {
                        DispatchQueue.main.async
                            {
                                self.resumeButton.isHidden = false
                        }
                    }
            }
        } else {
            resumeButton.isHidden = false
        }
    }

    //-------------------------------------------------------------------------------
    @objc func sessionWasInterrupted(notification: NSNotification)
    {
        //print("sessionWasInterrupted")
        if let userInfoValue = notification.userInfo?[AVCaptureSessionInterruptionReasonKey] as AnyObject?,
            let reasonIntegerValue = userInfoValue.integerValue,
            let reason = AVCaptureSession.InterruptionReason(rawValue: reasonIntegerValue)
        {
            //print("Capture session was interrupted with reason \(reason)")
            //print("because app went to background if reason was generic")

            var showResumeButton = false

            if reason == .audioDeviceInUseByAnotherClient || reason == .videoDeviceInUseByAnotherClient
            {
                showResumeButton = true
            } else if reason == .videoDeviceNotAvailableWithMultipleForegroundApps {

                cameraUnavailableLabel.alpha = 0
                cameraUnavailableLabel.isHidden = false
                UIView.animate(withDuration: 0.25)
                {
                    self.cameraUnavailableLabel.alpha = 1
                }
            }

            if showResumeButton
            {
                resumeButton.alpha = 0
                resumeButton.isHidden = false
                UIView.animate(withDuration: 0.25)
                {
                    self.resumeButton.alpha = 1
                }
            }
        }
    }

    //-------------------------------------------------------------------------------
    @objc
    func sessionInterruptionEnded(notification: NSNotification)
    {
        //print("Capture session interruption ended")
        if !resumeButton.isHidden
        {
            UIView.animate(withDuration: 0.25, animations:
                {
                    self.resumeButton.alpha = 0
            }, completion: { _ in
                self.resumeButton.isHidden = true
            }
            )
        }

        if !cameraUnavailableLabel.isHidden
        {
            UIView.animate(withDuration: 0.25, animations:
                {
                    self.cameraUnavailableLabel.alpha = 0
            }, completion: { _ in
                self.cameraUnavailableLabel.isHidden = true
            }
            )
        }
    }

    // MARK: - UIImagePickerControllerDelegate

    ////---------------------------------------------------------------------------------------
    //func metadata(url:URL)
    //{
    //    let result = PHAsset.fetchAssets(withALAssetURLs:[url],options:nil)
    //
    //print("  ")
    //print("url:",url)
    //
    //    if let asset = result.firstObject
    //    {
    //print("  ")
    //print("asset:",asset)
    //        // get photo info from this asset
    //        let options = PHImageRequestOptions();
    //            options.isNetworkAccessAllowed = true
    //            options.deliveryMode = .highQualityFormat
    //            options.version = .current
    //            options.resizeMode = .exact
    //            options.isSynchronous = false
    //
    //
    ////    let requestSize = !CGSizeEqualToSize(size, CGSizeZero) ? size : PHImageManagerMaximumSize
    ////    let requestContentMode = contentMode == .ScaleAspectFit ? PHImageContentMode.AspectFit : PHImageContentMode.AspectFill
    //
    //        PHImageManager.default().requestImageData(for: asset, options: options)
    //        { (imageData: Data!, dataUTI:String?, orientation:UIImage.Orientation, info: [AnyHashable : Any]?) in
    //
    //
    //print("  ")
    //print("imageData:",imageData)
    //print("info:",info as Any)
    //
    ////            var metadata:Dictionary = self.metadataFromImageData(imageData)
    //
    ////            if let image = image {
    ////                let degraded = info[PHImageResultIsDegradedKey] as? Bool ?? false
    ////                completionBlock(image: photoBlock.rot atedImage(image), isPlaceholder: degraded)
    ////
    ////            } else {
    ////                let error = info[PHImageErrorKey] as? NSError
    ////                NSLog("Nil image error = \(error?.localizedDescription)")
    ////           }
    //    }
    //
    //
    ////        PHImageManager.default.requestImageData(_ asset, options: imageRequestOptions, resultHandler: @escaping (Data?, String?, UIImage.Orientation, [AnyHashable : Any]?) -> Void)
    ////         requestImageDataForAsset:asset
    ////         options:imageRequestOptions
    ////         resultHandler:^(NSData *imageData, NSString *dataUTI,
    ////                         UIImageOrientation orientation,
    ////                         NSDictionary *info)
    ////         {
    ////             NSDictionary *dict = [self metadataFromImageData:imageData];// as this imageData is in NSData format so we need a method to convert this NSData into NSDictionary to display metadata
    ////         }];
    //    }
    //
    //}




    //func metadataFromImageData(imageData:Data) // -> NSDictionary
    //{
    //    let data:NSData = imageData as NSData
    //
    //    let bytes = data.bytes.assumingMemoryBound(to: UInt8.self)
    //    let dataPtr = CFDataCreate(kCFAllocatorDefault, bytes, data.length);
    //
    //    if let imageSource:CGImageSource = CGImageSourceCreateWithData(dataPtr!, nil)
    //    {
    //                if let imageProperties =  CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) // as? Any//[String: AnyObject]
    //                {
    //print("  ")
    //print("imageProperties:",imageProperties)
    //
    //                }
    //
    ////        NSDictionary *options = @{(NSString *)kCGImageSourceShouldCache : [NSNumber numberWithBool:NO]};
    ////        CFDictionaryRef imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, (__bridge CFDictionaryRef)options);
    ////        if (imageProperties)
    ////        {
    ////            NSDictionary *metadata = (__bridge NSDictionary *)imageProperties;
    ////            CFRelease(imageProperties);
    ////            CFRelease(imageSource);
    ////print("Metadata of selected image ",metadata);// It will display the metadata of image after converting NSData into NSDictionary
    ////            return metadata;
    ////
    ////        }
    ////        CFRelease(imageSource);
    //    }
    //
    //print("Can't read metadata");
    ////    return nil;
    //}


    //---------------------------------------------------------------------------------------
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        //print("  imagePickerController")
        var asset: PHAsset?
        let metadata:Dictionary = Dictionary<String, Any>()
        asset = info[.phAsset] as? PHAsset
        pitch = 0.0;
        lensModel = "none"
        print("asset?.pixelWidth :",asset?.pixelWidth as Any)
        print("asset?.pixelheight:",asset?.pixelHeight as Any)

        //        if let image = info[UIImagePickerController.InfoKey.origin alImage] as? UIImage
        //        {

        if let asset0 = asset
        {
            let options = PHImageRequestOptions();
            options.isNetworkAccessAllowed = true
            options.deliveryMode = .highQualityFormat
            options.version = .current
            options.resizeMode = .exact
            options.isSynchronous = false

//            PHImageManager.default().requestImageData(for: asset0, options: options)
//            { (imageData: Data!, dataUTI:String?, orientation:UIImage.Orientation, info: [AnyHashable : Any]?) in

			let manager = PHImageManager.default() //********** change 0.0.40 replace this area ***
			manager.requestImageDataAndOrientation(for: asset0, options: options, resultHandler:
			{(imageData, dataUTI, orientation, info) in

            //    let data:NSData = imageData as NSData

				if let imageData0 = imageData
				{
					let data:NSData = imageData0 as NSData

					if let image = UIImage(data: imageData0)
					{
						print("image.size:",image.size)

						forcedImageSize = Size(width: Float(image.size.width), height: Float(image.size.height))

						let bytes = data.bytes.assumingMemoryBound(to: UInt8.self)

						if let dataPtr = CFDataCreate(kCFAllocatorDefault, bytes, data.length)
						{
							if let imageSource:CGImageSource = CGImageSourceCreateWithData(dataPtr, nil)
							{
								if let metadata0:Dictionary =  CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as? [String: Any] //Any//[String: AnyObject]
								{
	//print("  ")
	//print("did get meta data0")
	////print("meta data0:",metadata0)
	//print("  ")
	//print("  ")
									eyeLevelFromTop = 0.0;

									//--------------- take out when trying to compare to iPhoneXs ---
									if let exifData = metadata0["{Exif}"] as? [String: Any]
									{
										if let userName = exifData[kCGImagePropertyExifUserComment as String] as? String
										{
											if (userName.hasPrefix("userName"))
											{
	//print(" ")
	//print(" ")
	//print("didFinishPickingMediaWithInfo already had userName:",userName as Any)
	//print(" ")
	//print(" ")
												//parse This String To eyelevel
												if (userName.contains("eyeLevel:"))
												{
													let fullStringParts = userName.components(separatedBy: "eyeLevel:")
													let eyeLevelString:String = fullStringParts.last ?? "0.0";
													eyeLevelFromTop = Float(eyeLevelString) ?? 0.0;
													//print("fullStringParts: ", fullStringParts);
													//print("eyeLevelString: ", eyeLevelString);
													//print("eyeLev elFromTop: ", eyeLev elFromTop);
													//print(" ")
												}

												self.usePhoto(image, metadata:metadata0, asset:asset0);
											} else {

												if let lmodel =  exifData[kCGImagePropertyExifLensModel as String] as? String
												{
													lensModel = lmodel;
												}

	//                                            let focalLength = exifData[kCGImagePropertyExifFocalLength as String]
	//                                            let focalLenIn35mmFilm = exifData[kCGImagePropertyExifFocalLenIn35mmFilm as String]
	//                                            let digitalZoom = exifData[kCGImagePropertyExifDigitalZoomRatio as String]

	//print("didFinishPickingMediaWithInfo userName:",userName as Any)
	//print("didFinishPickingMediaWithInfo focalLength:",focalLength as Any)
	//print("didFinishPickingMediaWithInfo lensModel:",lensModel)
	//print("didFinishPickingMediaWithInfo focalLenIn35mmFilm:",focalLenIn35mmFilm as Any)
	//print("didFinishPickingMediaWithInfo digitalZoom:",digitalZoom as Any)

												//                                            let lensCorrFilter = PECLensDistortCorr(lens:lensModel)
												//                                            let filteredImage = image.filterWithOperation(lensCorrFilter)

												self.usePhoto(image, metadata:metadata0, asset:asset0);
											}
										} else {
											if let lmodel =  exifData[kCGImagePropertyExifLensModel as String] as? String
											{
												lensModel = lmodel;
											}
											//                                        let focalLenIn35mmFilm = exifData[kCGImagePropertyExifFocalLenIn35mmFilm as String]
											//                                        let digitalZoom = exifData[kCGImagePropertyExifDigitalZoomRatio as String]

	//print("2 didFinishPickingMediaWithInfo lensModel:",lensModel)

											//                                        let lensCorrFilter = PECLensDistortCorr(lens:lensModel)
											//                                        let filteredImage = image.filterWithOperation(lensCorrFilter)

											self.usePhoto(image, metadata:metadata0, asset:asset0);
										}
									} else {
										//                                    let lensCorrFilter = PECLensDistortCorr(lens:lensModel)
										//                                    let filteredImage = image.filterWithOperation(lensCorrFilter)

										self.usePhoto(image, metadata:metadata0, asset:asset0);
									}

									  //---------------- take out - put in when testing for iPhoneXs temporary ---
									//self.usePhoto(image, metadata:metadata0, asset:asset0);

								} else {
									//                                let lensCorrFilter = PECLensDistortCorr(lens:lensModel)
									//                                let filteredImage = image.filterWithOperation(lensCorrFilter)

									self.usePhoto(image, metadata:metadata, asset:asset0);
								}
							} else {
								self.usePhoto(image, metadata:metadata, asset:asset0);
							}
						} else {
							self.usePhoto(image, metadata:metadata, asset:asset0);
						}
					}
                }
            })
        }

        picker.dismiss(animated: false, completion:
            {
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
        })
    }

    //---------------------------------------------------------------------------------------
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion:
            {
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
        })
    }
}

//-------------------------------------------------------------------------------
extension AVCaptureVideoOrientation
{
    init?(deviceOrientation: UIDeviceOrientation)
    {
        switch deviceOrientation
        {
        case .portrait: self = .portrait
        case .portraitUpsideDown: self = .portraitUpsideDown
        case .landscapeLeft: self = .landscapeRight
        case .landscapeRight: self = .landscapeLeft
        default: return nil
        }
    }

    init?(interfaceOrientation: UIInterfaceOrientation)
    {
        switch interfaceOrientation
        {
        case .portrait: self = .portrait
        case .portraitUpsideDown: self = .portraitUpsideDown
        case .landscapeLeft: self = .landscapeLeft
        case .landscapeRight: self = .landscapeRight
        default: return nil
        }
    }
}

//-------------------------------------------------------------------------------
extension AVCaptureDevice.DiscoverySession
{
    var uniqueDevicePositionsCount: Int
    {
        var uniqueDevicePositions: [AVCaptureDevice.Position] = []

        for device in devices
        {
            if !uniqueDevicePositions.contains(device.position)
            {
                uniqueDevicePositions.append(device.position)
            }
        }

        return uniqueDevicePositions.count
    }
}

//---------------------------------------------------
extension UIImage
{
    //---------------------------------------------------
    func rotate(radians: Double) -> UIImage?
    {
        if (radians == 0.0) {return self}
        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
        // Trim off the extremely small float value to prevent core graphics from rounding it up
        //print("0 newSize:",newSize)
        newSize.width = floor(newSize.width)
        newSize.height = floor(newSize.height)
        //print("0 newSize:",newSize)

        UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
        let context = UIGraphicsGetCurrentContext()!

        context.translateBy(x: newSize.width/2, y: newSize.height/2)
        context.rotate(by: CGFloat(radians))
        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage
    }

    //---------------------------------------------------
    func crop(centerPt: CIVector, newSize:CGSize) -> UIImage?
    {
        var newSize0 = newSize;
        newSize0.width = floor(newSize0.width)
        newSize0.height = floor(newSize0.height)

        UIGraphicsBeginImageContextWithOptions(newSize0, false, self.scale)
        let context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: newSize0.width/2.0, y: newSize0.height/2.0)
        self.draw(in: CGRect(x: -centerPt.x, y: -centerPt.y, width: self.size.width, height: self.size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage
    }
}

//-------------------------------------------------------------------------------
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any]
{
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}

//-------------------------------------------------------------------------------
fileprivate func convertFromUIBackgroundTaskIdentifier(_ input: UIBackgroundTaskIdentifier) -> Int
{
    return input.rawValue
}

