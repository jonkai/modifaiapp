import UIKit
import FirebaseAnalytics
public protocol FilterDataDelegate: class
{
    func filterData(dicData : [String : String])
    func filterClearData()
}

class FilterCatalogViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    let kHeaderSectionTag: Int = 6900;

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var topHeaderView: UIView!

    @IBOutlet weak var cancelBtn: UIButton!  //*********Change 2.2.1
    @IBOutlet weak var applyBtn: UIButton!  //*********Change 2.2.1
    @IBOutlet weak var headerText: UILabel!  //*********Change 2.2.1

    var expandedSectionHeaderNumber: Int = -1
    var expandedSectionHeader: UITableViewHeaderFooterView!
    var sectionItems: Array<Any> = []
    var sectionNames: Array<Any> = []
    var openSections: Array<Int> = []

    var arrPrice  = [String]()
    var arrBrand  = [String]()
    var arrStyle  = [String]()
    weak var delegate: FilterDataDelegate!
    
    static func storyboardInstance() -> FilterCatalogViewController
    {
        let story = UIStoryboard(name: "CustomTabBar",bundle: nil)
        return story.instantiateViewController(withIdentifier: "FilterCatalogViewController") as! FilterCatalogViewController
    }

    override var prefersStatusBarHidden: Bool
    {
        return true
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
//print("selectedPrice",selectedPrice)
//print("selectedStyle",selectedStyle)
        if selectedStyle.count == 0  && selectedPrice.count == 0 && selectedBrand.count == 0
        {
            btnClear.isHidden = true
        }
        
        btnClear.setTitleColor(titleColor, for: .normal)  //*********Change 2.2.1
        headerText.textColor = titleColor  //*********Change 2.2.1
        cancelBtn.setTitleColor(titleColor, for: .normal) //*********Change 2.2.1
        applyBtn.setTitleColor(titleColor, for: .normal) //*********Change 2.2.1

       // self.navigationController?.navigationBar.barTintColor = UIColor.colorWithHexString(hexStr: "#42CCBA")
        //self.navigationController?.navigationBar.tintColor = .white
        
        /*let nav = self.navigationController?.navigationBar
        nav?.barStyle = .default
        nav?.tintColor = .white
        nav?.barTintColor = UIColor(red: 57.0/255.0, green: 64.0/255.0, blue: 108.0/255.0, alpha: 1.0) //UIColor.colorWithHexString(hexStr: "#42CCBA")
        nav?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        self.title = "Filter Catalog"*/
       


        if hasOnlyGarageDoors && singleBrand // **********Change 2.2.1
        {
            sectionNames = ["Style"]
            sectionItems = [ ["Contemporary", "Traditional"]];

        }else
        {
            sectionNames = ["Style","Brand","Price"]
            sectionItems = [ ["Contemporary", "Traditional"],
                             arrBrand,arrPrice];
        }
        self.tableView!.tableFooterView = UIView()
        topHeaderView.backgroundColor = themeColor
        btnClear.backgroundColor = themeColor
       // let btnBarCancel = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelTap))
       //  navigationItem.leftBarButtonItem = btnBarCancel
        
       // let btnBarApply = UIBarButtonItem(title: "Apply", style: .plain, target: self, action: #selector(applyTap))
       // navigationItem.rightBarButtonItem = btnBarApply
       
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
    }

    @IBAction func cancelTap()
    {
        self.dismiss(animated: false)
        {
            
        }
    }
    
    @IBAction func applyTap()
    {
//print("filter catalog applyTap")
    if hasOnlyGarageDoors
               {
                if selectedStyle.count == 0  && selectedPrice.count == 0 {
                           AlertViewManager.shared.ShowOkAlert(title: Constants.Alerts.Home.OPP, message: Constants.Alerts.Home.NO_FILTER_SELECTED_MESSAGE, handler: nil)
                           return

                       }
        }else
    {
        if selectedStyle.count == 0  && selectedPrice.count == 0 && selectedBrand.count == 0{
                   AlertViewManager.shared.ShowOkAlert(title: Constants.Alerts.Home.OPP, message: Constants.Alerts.Home.NO_FILTER_SELECTED_MESSAGE, handler: nil)
                   return

               }
        }
       
        self.dismiss(animated: false)
        {
            
            if(selectedBrand.count > 0)
            {
                //Analytics.logEvent("Filter_Brand_selected", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
                
            }
            if(selectedStyle.count > 0)
            {
                 //Analytics.logEvent("Filter_Style_selected", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
            }
            if(selectedPrice.count > 0)
            {
                 //Analytics.logEvent("Filter_Price_selected", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
            }
            selectedBrand = "\(brand)"
            let dic = ["brand": selectedBrand, "style" : selectedStyle, "price" : selectedPrice]
            self.delegate.filterData(dicData: dic)
        }
    }

    @IBAction func clearFilterTap()
    {
       // Analytics.logEvent("Filter_clear_selected", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
        self.dismiss(animated: false) {
            selectedPrice = ""
            selectedBrand = ""
            selectedStyle = ""
            self.delegate.filterClearData()
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Tableview Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if sectionNames.count > 0 {
            tableView.backgroundView = nil
            return sectionNames.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if hasOnlyGarageDoors && singleBrand //**********change 2.2.1
        {
            let arrayOfItems = self.sectionItems[section] as! NSArray
            return arrayOfItems.count;
        }
        
        if  self.openSections.contains(section){
            let arrayOfItems = self.sectionItems[section] as! NSArray
            return arrayOfItems.count;
        } else {
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (self.sectionNames.count != 0) {
            return self.sectionNames[section] as? String
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0;
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 0;
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        //recast your view as a UITableViewHeaderFooterView
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        
        header.contentView.backgroundColor = themeColor //UIColor.colorWithHexString(hexStr: "#42CCBA")
        header.textLabel?.textColor = titleColor
        
        if let viewWithTag = self.view.viewWithTag(kHeaderSectionTag + section) {
            viewWithTag.removeFromSuperview()
        }
        let headerFrame = self.view.frame.size
        let theImageView = UIImageView(frame: CGRect(x: headerFrame.width - 32, y: 13, width: 18, height: 18));
        theImageView.image = UIImage(named: "Chevron-Dn-Wht")
        theImageView.tag = kHeaderSectionTag + section
        header.tag = section
        if !singleBrand // **********Change 2.2.1
        {
        header.addSubview(theImageView) //**********change 2.2.0
        
        // make headers touchable
       
        let headerTapGesture = UITapGestureRecognizer()//**********change 2.2.0
        headerTapGesture.addTarget(self, action: #selector(FilterCatalogViewController.sectionHeaderWasTouched(_:)))
        header.addGestureRecognizer(headerTapGesture)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as UITableViewCell
        let section = self.sectionItems[indexPath.section] as! NSArray
        cell.textLabel?.textColor = UIColor.black
        cell.textLabel?.text = section[indexPath.row] as? String
        cell.selectionStyle = .none
        let title = section[indexPath.row] as! String
        if indexPath.section == 0  {
            if title == selectedStyle {
                cell.accessoryType = .checkmark
            }
            else{
                cell.accessoryType = .none
            }
        }else  if indexPath.section == 1{
            if title == selectedBrand {
                cell.accessoryType = .checkmark
            }
            else{
                cell.accessoryType = .none
            }
        }
        else  if indexPath.section == 2{
            if title == selectedPrice {
                cell.accessoryType = .checkmark
            }
            else{
                cell.accessoryType = .none
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let section = self.sectionItems[indexPath.section] as! NSArray
           let title = section[indexPath.row] as! String
        if indexPath.section == 0  {
            if title == selectedStyle {
                selectedStyle = ""
            }
            else{
                selectedStyle = title
            }
        }
        else  if indexPath.section == 1{
            if title == selectedBrand {
                selectedBrand = ""
            }
            else{
                selectedBrand = title
            }
        }
        else  if indexPath.section == 2{
                   if title == selectedPrice {
                       selectedPrice = ""
                   }
                   else{
                       selectedPrice = title
                   }
               }
        tableView.reloadData()
    }
    
    // MARK: - Expand / Collapse Methods
    
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        let headerView = sender.view as! UITableViewHeaderFooterView
        let section    = headerView.tag
        let eImageView = headerView.viewWithTag(kHeaderSectionTag + section) as? UIImageView
        
        if (self.openSections.count == 0) {
           // self.expandedSectionHeaderNumber = section
            self.openSections.insert(section, at: 0)
            tableViewExpandSection(section, imageView: eImageView!)
        } else {
            if self.openSections.contains(section) {
                let index = openSections.firstIndex(of:section)
                self.openSections.remove(at: index!)
               tableViewCollapeSection(section, imageView: eImageView!)
            } else {
              //  let cImageView = self.view.viewWithTag(kHeaderSectionTag + //self.expandedSectionHeaderNumber) as? UIImageView
             //   tableViewCollapeSection(self.expandedSectionHeaderNumber, imageView: cImageView!)
           //      self.expandedSectionHeaderNumber = section;
                    self.openSections.insert(section, at: 0)
               tableViewExpandSection(section, imageView: eImageView!)
            }
        }
    }
    
    func tableViewCollapeSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
    //    self.expandedSectionHeaderNumber = -1;
        if (sectionData.count == 0) {
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.tableView!.beginUpdates()
            self.tableView!.deleteRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tableView!.endUpdates()
        }
    }
    
    func tableViewExpandSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        if (sectionData.count == 0) {
          //  self.expandedSectionHeaderNumber = -1;
           
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
           // self.expandedSectionHeaderNumber = section
            self.tableView!.beginUpdates()
            self.tableView!.insertRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tableView!.endUpdates()
        }
    }

}

