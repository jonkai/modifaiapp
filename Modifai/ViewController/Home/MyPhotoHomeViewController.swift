//  Created  on 01/04/19.

import UIKit
import MessageUI

class camIntro1ViewController: UIViewController {
    @IBOutlet weak var camImage: UIImageView!

    override func viewDidLoad() {
           super.viewDidLoad()
        camImage.layer.cornerRadius = 20
        camImage.layer.masksToBounds = true
           // Do any additional setup after loading the view.
       }
       override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
        self.view.backgroundColor = themeColor

           navigationController?.setNavigationBarHidden(true, animated: animated)
       }
       override func viewWillDisappear(_ animated: Bool) {
           super.viewWillDisappear(animated)
           navigationController?.setNavigationBarHidden(false, animated: animated)
       }
}
class camIntro2ViewController: UIViewController {
    @IBOutlet weak var camImage: UIImageView!

       override func viewDidLoad() {
              super.viewDidLoad()
           camImage.layer.cornerRadius = 20
           camImage.layer.masksToBounds = true
              // Do any additional setup after loading the view.
          }
       override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
        self.view.backgroundColor = themeColor

           navigationController?.setNavigationBarHidden(true, animated: animated)
       }
       override func viewWillDisappear(_ animated: Bool) {
           super.viewWillDisappear(animated)
           navigationController?.setNavigationBarHidden(false, animated: animated)
       }
}
class camIntro3ViewController: UIViewController {
    @IBOutlet weak var camImage: UIImageView!

       override func viewDidLoad() {
              super.viewDidLoad()
           camImage.layer.cornerRadius = 20
           camImage.layer.masksToBounds = true
              // Do any additional setup after loading the view.
          }
       override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
        self.view.backgroundColor = themeColor

           navigationController?.setNavigationBarHidden(true, animated: animated)
       }
       override func viewWillDisappear(_ animated: Bool) {
           super.viewWillDisappear(animated)
           navigationController?.setNavigationBarHidden(false, animated: animated)
       }
}
class camIntro4ViewController: UIViewController {
     @IBOutlet weak var camImage: UIImageView!

       override func viewDidLoad() {
              super.viewDidLoad()
           camImage.layer.cornerRadius = 20
           camImage.layer.masksToBounds = true
              // Do any additional setup after loading the view.
          }
       override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
        self.view.backgroundColor = themeColor

           navigationController?.setNavigationBarHidden(true, animated: animated)
       }
       override func viewWillDisappear(_ animated: Bool) {
           super.viewWillDisappear(animated)
           navigationController?.setNavigationBarHidden(false, animated: animated)
       }
}
class camIntro5ViewController: UIViewController,MFMailComposeViewControllerDelegate {
     @IBOutlet weak var camImage: UIImageView!
     @IBOutlet weak var emailBtn: UIButton!

       override func viewDidLoad() {
              super.viewDidLoad()
           camImage.layer.cornerRadius = 20
           camImage.layer.masksToBounds = true
              // Do any additional setup after loading the view.
          }
       override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
        self.view.backgroundColor = themeColor

           navigationController?.setNavigationBarHidden(true, animated: animated)
       }
       override func viewWillDisappear(_ animated: Bool) {
           super.viewWillDisappear(animated)
           navigationController?.setNavigationBarHidden(false, animated: animated)
       }
   
    @IBAction func TapButtonAction(_ sender: UIButton) {
       // self.sendMail()
    }
   
}
