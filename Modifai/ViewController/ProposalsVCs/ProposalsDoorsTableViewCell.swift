//
//  ProposalsDoorsTableViewCell.swift
//  LifeStyleApp
//
//  Created by IosMac on 02/04/19.
//  Copyright © 2019 IosMac. All rights reserved.
//

import UIKit

class ProposalsDoorsTableViewCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var btnCross: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
