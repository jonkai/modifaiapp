//
//  InfoViewController.swift
//  Modifai
//
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import MessageUI
import WebKit
import FirebaseAnalytics
class InfoViewController: UIViewController,MFMailComposeViewControllerDelegate,ZipcodeSelectionDelegate{
    
    @IBOutlet weak var faqHeadinf: UILabel!
    @IBOutlet weak var contactLabel: UILabel!
    @IBOutlet weak var faqLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var updateLocation: UIButton!
    @IBOutlet weak var zipCodeLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.lockOrientation(.portrait)
if isHomeApp
{
    faqHeadinf.text = ""
    faqLabel.text = ""
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(InfoViewController.tapFunction))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(InfoViewController.tapFaqFunction))
        
        contactLabel.isUserInteractionEnabled = true
        contactLabel.addGestureRecognizer(tap)
        faqLabel.isUserInteractionEnabled = true
        faqLabel.addGestureRecognizer(tap2)
        let bundleVsstrg:String = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String;
        let buildVsstrg:String = Bundle.main.infoDictionary!["CFBundleVersion"] as! String
        versionLabel.text = "Version - \(bundleVsstrg) (\(buildVsstrg))"
        let zipcode = UserDefaults.standard.value(forKey:"zipcode") as? String ?? ""
        zipCodeLabel.text = "\(zipcode)"
    }
    override func viewWillAppear(_ animated: Bool) {
       
        let zipcode = UserDefaults.standard.value(forKey:"zipcode") as? String ?? ""
        zipCodeLabel.text = "\(zipcode)"
        UINavigationBar.appearance().barTintColor = themeColor
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: titleColor] //*********Change 2.2.1
        UINavigationBar.appearance().isTranslucent = false
         //Analytics.logEvent("Info_tab_selected", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
    }

    func onSelectionCompletionClick(sender: String)
    {
//print("updated zipcode",sender)
        let zipcode = UserDefaults.standard.value(forKey:"zipcode") as? String ?? ""
               zipCodeLabel.text = "\(zipcode)"
    }

    // 03/13 changed
    @IBAction func contactUsBtnAction(_ sender: Any) {
        sendMail()
    }
    //changes end
    @IBAction func updateZipcodeAction(_ sender: Any)
    {
        let popOverVC:LocationPopUp = UIStoryboard(name: "CustomTabBar", bundle: nil).instantiateViewController(withIdentifier: "LocationPopUp") as! LocationPopUp
        popOverVC.view.frame = self.view.frame
        popOverVC.delegate = self
        popOverVC.modalPresentationStyle = .overCurrentContext    //********** change 3.0.3 change  ***
        popOverVC.view.backgroundColor = .white
        popOverVC.modalPresentationCapturesStatusBarAppearance = true
        self.present(popOverVC, animated: true, completion: nil)
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer)
    {
        self.sendMail()
//print("tap working")
    }

    @objc func tapFaqFunction(sender:UITapGestureRecognizer)
    {
        let webViewController:WebViewController = WebViewController()
        self.present(webViewController, animated: true, completion: nil)
//print("tap2 working")
    }

    func sendMail()
    {
        
        let mailComposer = MFMailComposeViewController()
        
        mailComposer.mailComposeDelegate = self
        
        //Set to recipients
        mailComposer.setToRecipients(["mobiledev@reviveai.co"])
        //Set the subject
        mailComposer.setSubject("Feedback")
        
        guard MFMailComposeViewController.canSendMail() else
        {
            //showMailServiceErrorAlert()
            return
        }
        
        mailComposer.setMessageBody("Please write your feedback here: ", isHTML: true)
        mailComposer.modalPresentationCapturesStatusBarAppearance = true
        
        self.present(mailComposer, animated: true, completion: nil)
        
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
class WebViewController: UIViewController, WKNavigationDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        let webView = WKWebView(frame: CGRect(x: 0, y: 0, width:self.view.frame.size.width, height: self.view.frame.size.height))
        self.view.addSubview(webView)
        let url = URL(string: "https://reviveai.co/faq.html")
        webView.load(URLRequest(url: url!))
        //  self.sendMail()
    }
    
}

