//
//  ProposalsViewController.swift
//  LifeStyleApp
//
//  Created by IosMac on 02/04/19.
//  Copyright © 2019 IosMac. All rights reserved.
//

import UIKit
import MessageUI
import FirebaseAnalytics

class ProposalsViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate,UITextFieldDelegate{
    var checkoutPDF:CheckoutPDFScreen!
    
     var prodCatlogList: [CatelogList] = [CatelogList]()

    var foldersArray:[URL] = [URL]()
    @IBOutlet weak var btnsBgImgView: UIImageView!
    @IBOutlet weak var btnList: UIButton!
    @IBOutlet weak var btnRequest: UIButton!
    @IBOutlet weak var requestPropposal: UIButton!

    @IBOutlet weak var proposalsTableView: UITableView!
    @IBOutlet weak var nodataLbl: UILabel!

    @IBOutlet weak var requestView: UIView!
    
    
    @IBOutlet weak var nameTextField: UITextField!
     @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var streetName1TextField: UITextField!
    @IBOutlet weak var streetName2TextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!

    
    @IBOutlet weak var zipCodeTextField: UITextField!
    
    @IBOutlet weak var phoneTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white//init(red: 89.0/255.0, green: 133.0/255.0, blue: 134.0/255.0, alpha: 0.3)
        proposalsTableView.backgroundColor = UIColor.white
        proposalsTableView.separatorColor = UIColor.clear
        proposalsTableView.isHidden = false
       // btnsBgImgView.image = UIImage(named: "btnListBgImgView")
        requestView.isHidden = true
        

        nameTextField = self.setUpTextField(textField: nameTextField)
        emailTextField = self.setUpTextField(textField: emailTextField)
        zipCodeTextField = self.setUpTextField(textField: zipCodeTextField)
        phoneTextField = self.setUpTextField(textField: phoneTextField)
        passwordTextField = self.setUpTextField(textField: passwordTextField)
        lastNameTextField = self.setUpTextField(textField: lastNameTextField)
        streetName2TextField = self.setUpTextField(textField: streetName2TextField)
        streetName1TextField = self.setUpTextField(textField: streetName1TextField)
        cityTextField = self.setUpTextField(textField: cityTextField)
        stateTextField = self.setUpTextField(textField: stateTextField)
        self.btnRequest.backgroundColor = themeColor
        btnRequest.setTitleColor(titleColor, for: .normal)

        if isClopay
        {
            btnList.setTitleColor(titleColor, for: .normal) //********Change 2.2.1
            self.btnList.backgroundColor = .white //********Change 2.2.1
        }else
        {
            self.btnList.backgroundColor = titleColor //********Change 2.2.1
            btnList.setTitleColor(themeColor, for: .normal) //********Change 2.2.1

        }
        requestPropposal.backgroundColor = themeColor
        requestPropposal.setTitleColor(titleColor, for: .normal)  //********Change 2.2.1
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.portrait)

        //Analytics.logEvent("Proposal_tab_selected", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
     AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        getFolderNames()
        UINavigationBar.appearance().barTintColor = themeColor
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: titleColor] //*********Change 2.2.1
        UINavigationBar.appearance().isTranslucent = false
//        DataManager.getCheckoutItems(){ (error, data) in
//            if data!.count > 0
//            {
//                self.prodCatlogList = [ProposalCatalogList]()
//                let catArray:NSArray = data!
//                for catDic in catArray {
//                    let catlist =  ProposalCatalogList( catDic as! NSDictionary)
//                    self.prodCatlogList.append(catlist)
//                }
//
//                self.proposalsTableView.reloadData()
//            }
//        }

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    func setUpTextField(textField: UITextField) -> UITextField  {
        textField.backgroundColor = UIColor.white
        textField.attributedPlaceholder = NSAttributedString(string: textField.placeholder!, attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])

        let view = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        textField.leftViewMode = .always
        textField.leftView = view
        return textField
    }
        //MARK:- load Folders
     
    func getFolderNames()
    {
        self.foldersArray = [URL]()
                do
                {
                    let directory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                     
                    if let urlArray = try? FileManager.default.contentsOfDirectory(at: directory,includingPropertiesForKeys: [.creationDateKey], options:.skipsHiddenFiles)
                    {

                      //  self.foldersArray =  urlArray
                        for filname in urlArray
                        {
                            if filname.lastPathComponent.contains("_folder")
                            {
                                self.foldersArray.append(filname)
                            }
                        }
                        if foldersArray.count>0
                        {
                            self.loadImageFromDocumentDirectory()
                        }
                    }

                   
                }
    }
         func loadImageFromDocumentDirectory()
         {
     //print("loadImage From Document Directory")
             var catList: [CatelogList] = [CatelogList]()
            self.prodCatlogList = [CatelogList]()
     //       var nameDoors:[String:CatelogList] = [String:CatelogList]()
            var nameGarages:[String:CatelogList] = [String:CatelogList]()

            for folderName in self.foldersArray
            {
            let fileName = folderName.lastPathComponent
             let fileManager = FileManager.default
             let DocumentDirectory = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
             if let filePath = DocumentDirectory.appendingPathComponent(fileName)
             {
                 if (fileManager.fileExists(atPath: filePath.path))
                 {
                     do
                     {
                         let folderURLs = try fileManager.contentsOfDirectory(at: filePath, includingPropertiesForKeys: nil, options: .skipsHiddenFiles )
                   
                         for folderURL in folderURLs
                         {
    
                             do
                             {
                                 let fileURLs = try fileManager.contentsOfDirectory(at: folderURL, includingPropertiesForKeys: nil, options: .skipsHiddenFiles )
                                
                             //    var cat:CatelogList = CatelogList.init()

                                 var garDic1:[String:Any] = [String:Any]()
                                 var garDic2:[String:Any] = [String:Any]()
                                 var doorDic1:[String:Any] = [String:Any]()
                                 var doorDic2:[String:Any] = [String:Any]()

                                 
                                 //------------- read in door Descriptions from disk ----------------
                                 for fileURL in fileURLs
                                 {
                                     if fileURL.path.contains("_1cardetails.txt")
                                     {
                                         if let contents = try? String(contentsOfFile: fileURL.path)
                                         {
                                             let data :Data = contents.data(using: String.Encoding.utf8)! as Data
                                             
                                             if let catDic = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String:Any]
                                             {
                                                 //print(jsonArray) // use the json here
                                                 garDic1 = catDic
                                                 var urlpath = fileURL.path
                                                 urlpath = urlpath.replacingOccurrences(of:"_1cardetails.txt", with: "_1car.jpg")
                                                 if let image = UIImage(contentsOfFile:urlpath)
                                                 {
                                              //       let garName1 = (garDic1["name"] as? String) ?? ""
                                                     let cat:CatelogList = CatelogList.init(image: image, name:  (garDic1["name"] as? String) ?? "",
                                                     										descriptions: (garDic1["desc"] as? String) ?? "" ,
                                                     										catlogID: (garDic1["catID"] as? String) ?? "",
                                                     										price: (garDic1["price"] as? String) ?? "",
                                                     										checkout: (garDic1["checkout"] as? Bool) ?? false,
                                                     										fileURL:(garDic1["fileURL"] as? String) ?? "",
                                                     										fileName:(garDic1["fileName"] as? String) ?? "",
                                                     										type:"1car",mid:garDic1["mID"] as? Int ?? 0,
    																						mask_url:(garDic1["mask_url"] as? String) ?? " ",
    																						isPaintable:garDic1["isPaintable"] as? Int ?? 0,
    																						rValue:garDic1["rValue"] as? CGFloat ?? 0.0,
    																						gValue:garDic1["gValue"] as? CGFloat ?? 0.0,
    																						bValue:garDic1["bValue"] as? CGFloat ?? 0.0)

                                                    let availZip = self.checkZipCodeAvailabilityforMID(mID:cat.manufacturerID)
                                                    if cat.checkout && availZip
                                                        {
                                                         catList.append(cat)
                                                         nameGarages[cat.name] = cat
                                                     }
                                                 }
                                             }
                                             
                                         }
                                     }

                                     
                                     if fileURL.path.contains("_2cardetails.txt")
                                     {
                                         if let contents = try? String(contentsOfFile: fileURL.path)
                                         {
                                            let data :Data = contents.data(using: String.Encoding.utf8)! as Data
                                            
                                            if let catDic = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String:Any]
                                            {
                                                //print(jsonArray) // use the json here
                                                garDic2 = catDic
                                                var urlpath = fileURL.path
                                                urlpath = urlpath.replacingOccurrences(of:"_2cardetails.txt", with: "_2car.jpg")
                                                if let image = UIImage(contentsOfFile:urlpath)
                                                {
                                            //        let garName2 = (garDic2["name"] as? String) ?? ""
                                                    let cat:CatelogList = CatelogList.init(image: image, name:  (garDic2["name"] as? String) ?? "",
                                                    										descriptions: (garDic2["desc"] as? String) ?? "" ,
                                                    										catlogID: (garDic2["catID"] as? String) ?? "",
                                                    										price: (garDic2["price"] as? String) ?? "",
                                                    										checkout: (garDic2["checkout"] as? Bool) ?? false,
                                                    										fileURL:(garDic2["fileURL"] as? String) ?? "",
                                                    										fileName:(garDic2["fileName"] as? String) ?? "",
                                                    										type:"2car",mid:garDic2["mID"] as? Int ?? 0,
     																						mask_url:(garDic2["mask_url"] as? String) ?? " ",
    																						isPaintable:garDic2["isPaintable"] as? Int ?? 0,
    																						rValue:garDic2["rValue"] as? CGFloat ?? 0.0,
    																						gValue:garDic2["gValue"] as? CGFloat ?? 0.0,
    																						bValue:garDic2["bValue"] as? CGFloat ?? 0.0)

                                                     let availZip = self.checkZipCodeAvailabilityforMID(mID:cat.manufacturerID)
                                                                                                       if cat.checkout && availZip
                                                                                                           {
                                                                                                            catList.append(cat)
                                                                                                            nameGarages[cat.name] = cat
                                                                                                        }
                                                    
                                                }
                                                
                                            }
                                            
                                        }
                                     }

                                     

                                     if fileURL.path.contains("_1doordetails.txt")
                                     {
                                         if let contents = try? String(contentsOfFile: fileURL.path)
                                         {
                                             let data :Data = contents.data(using: String.Encoding.utf8)! as Data
                                             
                                             if let catDic = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String:Any]
                                             {
                                                //print(jsonArray) // use the json here
                                                doorDic1 = catDic
                                                var urlpath = fileURL.path
                                                urlpath = urlpath.replacingOccurrences(of:"_1doordetails.txt", with: "_1door.jpg")
                                                
                                                if let image = UIImage(contentsOfFile:urlpath)
                                                {
                                           //         let doorName1 = (doorDic1["name"] as? String) ?? ""
                                                    let cat:CatelogList = CatelogList.init(image: image, name:  (doorDic1["name"] as? String) ?? "",
                                                    										descriptions: (doorDic1["desc"] as? String) ?? "" ,
                                                    										catlogID: (doorDic1["catID"] as? String) ?? "",
                                                    										price: (doorDic1["price"] as? String) ?? "",
                                                    										checkout: (doorDic1["checkout"] as? Bool) ?? false,
                                                    										fileURL:(doorDic1["fileURL"] as? String) ?? "",
                                                    										fileName:(doorDic1["fileName"] as? String) ?? "",
                                                    										type:"1door",mid:doorDic1["mID"] as? Int ?? 0,
     																						mask_url:(doorDic1["mask_url"] as? String) ?? " ",
    																						isPaintable:doorDic1["isPaintable"] as? Int ?? 0,
    																						rValue:doorDic1["rValue"] as? CGFloat ?? 0.0,
    																						gValue:doorDic1["gValue"] as? CGFloat ?? 0.0,
    																						bValue:doorDic1["bValue"] as? CGFloat ?? 0.0)

                                                    let availZip = self.checkZipCodeAvailabilityforMID(mID:cat.manufacturerID)
                                                    if cat.checkout && availZip
                                                    {
                                                        catList.append(cat)
                                                        nameGarages[cat.name] = cat
                                                    }
                                                }
                                            }
                                             
                                         }
                                     }

                                     if fileURL.path.contains("_2doordetails.txt")
                                     {
                                        if let contents = try? String(contentsOfFile: fileURL.path)
                                        {
                                            let data :Data = contents.data(using: String.Encoding.utf8)! as Data
                                            
                                            if let catDic = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String:Any]
                                            {
                                                //print(jsonArray) // use the json here
                                                doorDic2 = catDic
                                                var urlpath = fileURL.path
                                                urlpath = urlpath.replacingOccurrences(of:"_2doordetails.txt", with: "_2door.jpg")
                                                
                                                if let image = UIImage(contentsOfFile:urlpath)
                                                {
                                           //         let doorName2 = (doorDic2["name"] as? String) ?? ""
                                                    let cat:CatelogList = CatelogList.init(image: image, name:  (doorDic2["name"] as? String) ?? "",
                                                    										descriptions: (doorDic2["desc"] as? String) ?? "" ,
                                                    										catlogID: (doorDic2["catID"] as? String) ?? "",
                                                    										price: (doorDic2["price"] as? String) ?? "",
                                                    										checkout: (doorDic2["checkout"] as? Bool) ?? false,
                                                    										fileURL:(doorDic2["fileURL"] as? String) ?? "",
                                                    										fileName:(doorDic2["fileName"] as? String) ?? "",
                                                    										type:"2door",mid:doorDic2["mID"] as? Int ?? 0,
     																						mask_url:(doorDic2["mask_url"] as? String) ?? " ",
    																						isPaintable:doorDic2["isPaintable"] as? Int ?? 0,
    																						rValue:doorDic2["rValue"] as? CGFloat ?? 0.0,
    																						gValue:doorDic2["gValue"] as? CGFloat ?? 0.0,
    																						bValue:doorDic2["bValue"] as? CGFloat ?? 0.0)

                                                    let availZip = self.checkZipCodeAvailabilityforMID(mID:cat.manufacturerID)
                                                    if cat.checkout && availZip
                                                    {
                                                        catList.append(cat)
                                                        nameGarages[cat.name] = cat
                                                    }
                                                }
                                            }
                                            
                                        }
                                    }
                                }

                               
                             } catch {
     //print("no files")
                             }
                         }

                    
                     } catch {
     //print("no folders")
                     }

                 } else {
     //print("00 no folders")
                 }
             }
            }
            
            //Note: to display single garage for duplicates
//            for (_,value) in nameGarages
//                               {
//                                   self.prodCatlogList.append(value)
//                               }
         self.prodCatlogList = catList

            proposalsTableView.reloadData()
     //print("  ")
     //print("  ")
     //print("  ")
         }
    //MARK:- IBActions
    @IBAction func btnListClicked(_ sender: Any) {
      //  btnsBgImgView.image = UIImage(named: "btnListBgImgView")
        if self.prodCatlogList.count == 0
        {
            nodataLbl.isHidden = false
        }else
        {
            nodataLbl.isHidden = true
        }

        proposalsTableView.isHidden = false
        requestView.isHidden = true
     
        self.btnRequest.backgroundColor = themeColor
        btnRequest.setTitleColor(titleColor, for: .normal)  //*********Change 2.2.1

        if isClopay
        {
            btnList.setTitleColor(titleColor, for: .normal) //********Change 2.2.1
            self.btnList.backgroundColor = .white //********Change 2.2.1
        }else
        {
            self.btnList.backgroundColor = titleColor //********Change 2.2.1
            btnList.setTitleColor(themeColor, for: .normal) //********Change 2.2.1
        }
      
    }
    
    @IBAction func requestBtnAction(_ sender: Any) {
        
        if nameTextField.text?.count == 0 || lastNameTextField.text?.count == 0 || streetName1TextField.text?.count == 0 || phoneTextField.text?.count == 0 || cityTextField.text?.count == 0 || stateTextField.text?.count == 0 || zipCodeTextField.text?.count == 0
        {
            return
        }
        let storyBoard: UIStoryboard = UIStoryboard(name: "CheckOut", bundle: nil)

               checkoutPDF = (storyBoard.instantiateViewController(withIdentifier: "CheckoutPDFScreen") as! CheckoutPDFScreen)
               checkoutPDF.prodCatlogList = prodCatlogList
               present(checkoutPDF, animated: false, completion: nil)
               checkoutPDF.view.isHidden = true
               checkoutPDF.proposalsTableView.reloadData()
           //   CheckoutPDF.dismiss(animated:false, completion: nil)
               checkoutPDF.nameLbl.text = "First Name: " + nameTextField.text!
               checkoutPDF.emailLbl.text = "Email: " + emailTextField.text!
               checkoutPDF.phoneLbl.text = "Phone: " + phoneTextField.text!
               checkoutPDF.zipCodeLbl.text = "Zip: " + zipCodeTextField.text!
               checkoutPDF.streetAdd1Lbl.text = "Address1: " + streetName1TextField.text!
               checkoutPDF.streetAdd2Lbl.text = "Address2: " + streetName2TextField.text!
               checkoutPDF.lastNameLbl.text = "Last Name: " + lastNameTextField.text!
               checkoutPDF.cityLbl.text = "City: " + cityTextField.text!
               checkoutPDF.stateLbl.text = "State: " + stateTextField.text!
        

       // let path = checkoutPDF.pdfscrollView.exportAsPdfFromView()
       // checkoutPDF.view.isHidden = true
        let when = DispatchTime.now() + 1
               DispatchQueue.main.asyncAfter(deadline: when){
                 // your code with delay
                 let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
                                   let docDirectoryPath = paths[0]
                                     let pdfPath = docDirectoryPath.appendingPathComponent("ModifaiDetails.pdf")
               // self.checkoutPDF.view.isHidden = false
                try! PDFGenerator.generate(self.checkoutPDF.requestView, to:pdfPath.path)
                
                
                self.sendMail(filepath:pdfPath.path)
               

               }
//
        let userDetailsDic:NSDictionary = ["firstname":nameTextField.text!,"lastname":lastNameTextField.text!,"streetname1":streetName1TextField.text!,"streetname2":streetName2TextField.text!,"PhoneNumber":phoneTextField.text!,"city":cityTextField.text!,"state":stateTextField.text!,"zipcode":zipCodeTextField.text!,"Email":emailTextField.text!]
        DataManager.updateUserDetails(userDetails:userDetailsDic)
        {(error, data) in
            
        }
        //Analytics.logEvent("Request_proposal_button_selected", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
    }
    func sendMail(filepath:String) {
        let mailComposer = MFMailComposeViewController()
        
        mailComposer.mailComposeDelegate = self
        
        //Set to recipients
        mailComposer.setToRecipients(["mobiledev@reviveai.co"])
        
        //Set the subject
        mailComposer.setSubject("\(brand) customer details pdf")
        
        guard MFMailComposeViewController.canSendMail() else {
            //showMailServiceErrorAlert()
            return
        }
        if  let fileData = NSData(contentsOfFile: filepath)
        {
//print("File data loaded.")
            mailComposer.addAttachmentData(fileData as Data, mimeType: "application/pdf", fileName: "ModifaiDetails.pdf")
        }
        
        mailComposer.setMessageBody("Please find details in attachment.", isHTML: true)
        navigationController?.setNavigationBarHidden(true, animated: false)
        if let topMostController = UIApplication.getTopViewController()
        {
        topMostController.present(mailComposer, animated: true, completion: nil)
        }
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
    {
        controller.dismiss(animated: true, completion: nil)
        navigationController?.setNavigationBarHidden(false, animated: false)
        switch result
        {
                       case .cancelled:
//print("Sending Mail is cancelled")
                        break;
                       case .sent :
                        self.showPopup()
//print("Your Mail has been sent successfully")
                        break;
                       case .saved :
//print("Sending Mail is Saved")
                        break;
                       case .failed :
//print("Message sending failed")
                        break;
                       default:
//print("Message sending failed")
                        break;
        }
        self.checkoutPDF.dismiss(animated:false, completion: nil)
    }

    func showPopup()
    {
        let alert = UIAlertController(title: "", message: "Your Request Proposal Mail has been sent successfully", preferredStyle: .alert)

		if let popoverController = alert.popoverPresentationController  //********** change 3.0.3 add if block ***
		{
		  popoverController.sourceView = self.view
		  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
		  popoverController.permittedArrowDirections = []
		}

        self.present(alert, animated: true, completion: nil)
        let tabBarViewController = UIApplication.shared.windows.first!.rootViewController as!   UITabBarController
        tabBarViewController.selectedIndex = 0
        // change to desired number of seconds (in this case 5 seconds)
        let when = DispatchTime.now() + 4
        DispatchQueue.main.asyncAfter(deadline: when)
        {
          // code with delay
          alert.dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func btnRequestClicked(_ sender: Any)
    {
        nodataLbl.isHidden = true
      //  btnsBgImgView.image = UIImage(named: "btnRequestBgImgView")
        proposalsTableView.isHidden = true
        requestView.isHidden = false
        self.btnList.backgroundColor = themeColor
        btnList.setTitleColor(titleColor, for: .normal)
      
        if isClopay
        {
            btnRequest.setTitleColor(titleColor, for: .normal) //********Change 2.2.1
            self.btnRequest.backgroundColor = .white //********Change 2.2.1
        }else
        {
            self.btnRequest.backgroundColor = titleColor //********Change 2.2.1
            btnRequest.setTitleColor(themeColor, for: .normal) //********Change 2.2.1
        }
        DataManager.getUserDetails()
        { (error, data) in
            if data != nil
            {
                self.nameTextField.text = data?.value(forKey:"firstname") as? String
                self.emailTextField.text = data?.value(forKey:"username") as? String
                if !(data?.value(forKey:"PhoneNumber") is NSNull && ((data?.value(forKey:"PhoneNumber")) != nil))
             {
                self.phoneTextField.text = data?.value(forKey:"PhoneNumber") as? String
             }
                if !(data?.value(forKey:"zipcode") is NSNull&&((data?.value(forKey:"zipcode")) != nil))
                {
                self.zipCodeTextField.text = String( format:"%d", (data?.value(forKey:"zipcode") as! Int))
                }
                self.streetName1TextField.text = data?.value(forKey:"streetname1") as? String
                self.streetName2TextField.text = data?.value(forKey:"streetname2") as? String
                self.lastNameTextField.text = data?.value(forKey:"lastname") as? String
                self.cityTextField.text = data?.value(forKey:"city") as? String
                self.stateTextField.text = data?.value(forKey:"state") as? String
                
            }
            self.checkTextFields()
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        if textField ==  phoneTextField{
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
            
            return textField.text!.count <= 9
        }
        if textField ==  zipCodeTextField{
                   let char = string.cString(using: String.Encoding.utf8)
                   let isBackSpace = strcmp(char, "\\b")
                   if isBackSpace == -92 {
                       return true
                   }
                   
                   return textField.text!.count <= 4
               }
        return true
    }
    func checkTextFields()
    {
        if nameTextField.text?.count == 0 || lastNameTextField.text?.count == 0 || streetName1TextField.text?.count == 0 || phoneTextField.text!.count < 10 || cityTextField.text?.count == 0 || stateTextField.text?.count == 0 || zipCodeTextField.text!.count < 5                       {
                       requestPropposal.isEnabled = false
                      requestPropposal.alpha = 0.8
                }else
               {
                   requestPropposal.isEnabled = true
                requestPropposal.alpha = 1


               }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.checkTextFields()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
          self.checkTextFields()
    }

    //MARK:- UITableview Delegate and Datasource methods
    func numberOfSections(in tableView: UITableView) -> Int {
       return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
            if self.prodCatlogList.count == 0 && requestView.isHidden
            {
                nodataLbl.isHidden = false
            }else
            {
                nodataLbl.isHidden = true
            }
             return self.prodCatlogList.count;
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 67
       
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
        //Set seperator
        let lineLabel = UILabel()
        lineLabel.backgroundColor = themeColor
        lineLabel.frame = CGRect.init(x: 0, y: 7, width: headerView.frame.width, height: 1.0)

        
        let label = UILabel()
        switch (section) {
        case 0:
            label.frame = CGRect.init(x: 15, y: 5, width: 140, height: 30)
            label.text = "Garage";
        case 1:
            label.frame = CGRect.init(x: 15, y: 12, width: 140, height: 30)
            label.text = "Doors";
            headerView.addSubview(lineLabel)
        default:
            label.text = "";
        }
        label.font = UIFont(name: "Lato-Bold", size: 18)

        label.textColor = titleColor
        headerView.backgroundColor = themeColor
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.prodCatlogList.count == 0
        {
            return 0
        }
        return 44
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
            guard let proposalsGarageCell = tableView.dequeueReusableCell(withIdentifier: "ProposalsGarageCell") as? ProposalsGarageTableViewCell else{
                return UITableViewCell()
            }
        proposalsGarageCell.backgroundColor =  UIColor.lightGray
            proposalsGarageCell.imgView.image = prodCatlogList[indexPath.row].image
            proposalsGarageCell.titleLabel.text = prodCatlogList[indexPath.row].name
            proposalsGarageCell.subTitleLabel.text = prodCatlogList[indexPath.row].descriptions
            proposalsGarageCell.subTitleLabel.textColor = .darkGray
            proposalsGarageCell.btnCross.setImage(UIImage(named: "proposals-v3"), for: .normal)
            proposalsGarageCell.btnCross.tag = indexPath.row
            proposalsGarageCell.btnCross.addTarget(self, action: #selector(checkoutSelected(sender:)), for: .touchUpInside)
            
            return proposalsGarageCell;
        
    }
    @objc func checkoutSelected(sender: UIButton){  //******** replace total 2.2.0
          LoadingIndicatorView.show("Please wait")
       
        sender.isSelected = !sender.isSelected
        if sender.isSelected
                       {
                           sender.setImage(UIImage(named: "proposals-v3"), for: .normal)
                       } else {
                           sender.setImage(UIImage(named: "proposals-v2"), for: .normal)
                       }
       
            
        let catlogSelected:CatelogList = self.prodCatlogList[sender.tag]
            if catlogSelected.type.contains("door")
                {
                                     AlertViewManager.shared.ShowOkAlert(title:"Sorry", message: "Front doors are not available for purchase at this time in your area.", handler: nil)
                    sender.setImage(UIImage(named: "proposals-v2"), for: .normal)

                                       return
                }
            let checkout = false
             catlogSelected.checkout = false

            let destFile = catlogSelected.file
            if destFile.count > 2
            {
                let arrayTemp = destFile.components(separatedBy:"__")
                let dic = ["name":catlogSelected.name,"desc":catlogSelected.descriptions ,"price":catlogSelected.price ,"catID":catlogSelected.catlogID,"checkout":checkout,"fileURL":catlogSelected.file,"fileName":catlogSelected.filename,"mID":catlogSelected.manufacturerID] as [String : Any]
                        
                        let DocumentDirectory = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
                        if let filePath = DocumentDirectory.appendingPathComponent(arrayTemp[0])
                        {
                            let filenamedetailsname = arrayTemp[1]
                            let fileURLdeails = filePath.appendingPathComponent(filenamedetailsname)

                       do {
                           let jsonData: Data? = try? JSONSerialization.data(withJSONObject: dic)
                     
                        try jsonData!.write(to:fileURLdeails,options:.atomic)
                        DataManager.UpdateFavoriteScenes(dataToConve:jsonData!,filename:catlogSelected.file , typeName: "garagerName")
                        
                       } catch {
                        print("error saving",error.localizedDescription)
                       }
                        }
            }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: { //******** Chage 2.2.0
            LoadingIndicatorView.hide()
            self.prodCatlogList.remove(at:sender.tag)
            self.proposalsTableView.reloadData()
        })

    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
   
       func checkZipCodeAvailabilityforMID(mID:Int)->Bool
       {
           let zipcode = UserDefaults.standard.value(forKey:"zipcode") as? String ?? ""

           let manufactureZipcodes = DataManager.loadManufacturerZipCodes()

           if manufactureZipcodes.count>0
           {
               
               
               if let zipArray = manufactureZipcodes[String(mID)] as? [String]
               {
                   if zipArray.contains(zipcode)
                   {
                       return true
                   }else
                   {
                       return true // changing for production ModifaiPro for showing cart for all products
                   }
               }else
               {
                   return true // changing for production ModifaiPro for showing cart for all products
               }
               
               
           }else
           {
               return true // changing for production ModifaiPro for showing cart for all products
           }
       }
}
