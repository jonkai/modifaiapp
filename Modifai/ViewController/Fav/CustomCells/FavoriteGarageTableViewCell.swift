import UIKit

//----------------------------------------------
class FavoriteGarageTableViewCell: UITableViewCell
{
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var btnCart: UIButton!

	//----------------------------------------------
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
   
	//----------------------------------------------
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
}
