import UIKit
import Social
import PECimage
import FirebaseAnalytics

//-----------------------------------------------------------------
class FavoriteViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate
{
    var productCatalogList:NSArray!
    var designListArray :NSArray!
    var currentSceneDesignList: DesignList!
    var mainFolderName = ""

    //   var catlogList: [CatelogList] = [CatelogList]()
    
    var prodGarageCatlogList: [CatelogList] = [CatelogList]()
    var prodDoorCatlogList: [CatelogList]  = [CatelogList]()
    
    //    var designL istArray:[UIImage] = [UIImage]()
    //    var defaultSelectedImage:UIImageView!
    //    var currentVisibleImage:UIImageView!
    //    var productListDict:NSArray = []
    //
    //    var lastImageUrl = ""
    //    var selectedR eferenceItem: ReferenceItem?
    //    var isTwoCar:Bool = false
    //    var isDo or:Bool = false
    //    var isDo uble:Bool = false
    //    var isOn eCar:Bool = false
    
    var imagesArray:[UIImage] = [UIImage]()
    var fileUrlArray:[URL] = [URL]()
    var sceneList: [[CatelogList]] = [[CatelogList]]()
    var favoriteImageName = ""
    
    @IBOutlet weak var myGalleryTableViewYConstarin:NSLayoutConstraint!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    @IBOutlet weak var myGalleryTableView: UITableView!
    @IBOutlet weak var btnDesigns: UIButton!
    @IBOutlet weak var btnProducts: UIButton!
    @IBOutlet weak var nodataLabel: UILabel!
    @IBOutlet weak var favView: UIView!
    
    var btnDesignsWasClicked = true
    var btnProductsWasClicked = false
    //    var btnFavClicked = false
    
    //    var currentIndex = 0
    var sceneIndex = 0
    
    
    //    var homeObj:Home?
    
    
    //MARK:- loading cycle
    //----------------------------------------------
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    //----------------------------------------------
    override func viewDidLoad()
    {
        //print("favorite view Did Load")
        super.viewDidLoad()
        //self.view.backgroundColor = UIColor.white
        myGalleryTableView.backgroundColor = UIColor.white
        myGalleryTableView.separatorColor = UIColor.clear
        self.myGalleryTableView.delegate = self
        self.myGalleryTableView.dataSource = self
        self.imageCollectionView.delegate = self
        self.imageCollectionView.dataSource = self
        myGalleryTableView.allowsSelection = true
        //     myGalleryTableView.relo adData()
        // carousel.type = .coverFlow
        self.designListArray = []
        self.productCatalogList = []
        
        let nib1 = UINib(nibName: "FavCollectionCell", bundle: nil)
        imageCollectionView.register(nib1, forCellWithReuseIdentifier: "FavCollectionCell")
    }
    
    //---------------------------------------------------------
    override func viewWillAppear(_ animated: Bool)
    {
        //print(" ")
        //print(" ")
        //print(" ")
        //print("favorite view Will Appear")
        super.viewWillAppear(animated)
        fromFavorites = true
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 375, height: 215)
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        
        let nib1 = UINib(nibName: "FavCollectionCell", bundle: nil)
        
        imageCollectionView.register(nib1, forCellWithReuseIdentifier: "FavCollectionCell")
        imageCollectionView.isPagingEnabled = true
        imageCollectionView!.collectionViewLayout = layout
        
        //		self.btnDesignsWasClicked  = true;
        //		self.btnProductsWasClicked = false;
        
        //		isT woCar = false
        //		isDo or = false
        //		isDo ble = false
        //		isOn eCar = false
        //		defaultSelectedImage = UIImageView()
        //		currentVisibleImage = UIImageView()
        //		defaultSelectedImage = UIImageView()
        //		currentVisibleImage = UIImageView()
        
        //	self.imagesArray.remo veAll()
        
        AppUtility.lockOrientation(.portrait)
        
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        //	self.currentIndex = 0
       
        
        self.favoriteImageName = currentImageName
        self.loadImageFromDocumentDirectory(nameOfImage:self.favoriteImageName)
        
        //		if (wasALastFavorite)
        //		{
        //			if (self.imagesArray.count > 1)
        //			{
        //				let indexPath = IndexPath.init(row: 1, section: 0)
        //				self.imageCollectionView.selectItem(at: indexPath, animated:false, scrollPosition:.centeredHorizontally)
        //				self.scen eIndex = 1
        //print("wasALastFavorite indexPath:",indexPath)
        //			}
        //		}
        
        
        //print("self.images Array:",self.imagesArray)
        
        if self.imagesArray.count > 0
        {
            nodataLabel.isHidden = true
            self.favView.isHidden = false;
        } else {
            nodataLabel.isHidden = false
            nodataLabel.text = "Create favorites to save ideas and share them with others."
            self.favView.isHidden = true;
        }
        UINavigationBar.appearance().barTintColor = themeColor
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: titleColor] //*********Change 2.2.1
        UINavigationBar.appearance().isTranslucent = false
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    //---------------------------------------------------------
    override func viewDidAppear(_ animated: Bool)
    {
        //print("favorite view did Appear")
        super.viewDidAppear(animated)
        
        self.sceneIndex = 0
        self.imageCollectionView.reloadData()
        self.myGalleryTableView.reloadData()
        self.btnDesignsClicked(btnDesigns as Any)
        if self.imagesArray.count > 0
        {
            self.imageCollectionView.scrollToItem(at:IndexPath(item:0, section: 0), at: .right, animated: false)
        }
        let paths = self.imageCollectionView.indexPathsForVisibleItems
        //print("paths:",paths)
        if (paths.count == 1)
        {
            let path = paths[0];
            self.sceneIndex = path.row
            //print("self.sce neIndex:",self.sceneIndex)
        }
    }
    
    
    //	//----------------------------------------------
    //    func getROISData() // not using change 2.0.6
    //
    
    //	//----------------------------------------------
    //    func fetchFr omJson(json: String) // not using change 2.0.6
  
    
    //	//----------------------------------------------
    //    func getDesignAPICall() // not using change 2.0.6
    //	
    
    //----------------------------------------------
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.all)
    }
    
    //MARK:- actions
    //----------------------------------------------
    @IBAction func shareButtonTapped(_ sender: Any)
    {
        if (self.imagesArray.count > self.sceneIndex)
        {
            let image = self.imagesArray[self.sceneIndex]

            let activityVC = UIActivityViewController(activityItems: [image], applicationActivities: nil)
            activityVC.excludedActivityTypes =
                [ .airDrop,.postToTwitter,.print,.assignToContact,.assignToContact,.copyToPasteboard,.addToReadingList,.markupAsPDF,.postToFlickr,.postToTencentWeibo,.postToTencentWeibo,.postToVimeo]

            activityVC.popoverPresentationController?.sourceView = self.view //********** change 3.0.3 add ***
			activityVC.preferredContentSize = self.view.bounds.size  //********** change 3.0.3 add ***
			activityVC.modalPresentationStyle = .popover  //********** change 3.0.3 add ***
			activityVC.popoverPresentationController?.sourceRect = self.view.frame;   //********** change 3.0.3 add ***
			activityVC.popoverPresentationController?.permittedArrowDirections = [];  //********** change 3.0.3 add ***

            self.present(activityVC, animated: true, completion: nil)
        }
    }

    //----------------------------------------------
    @IBAction func btnFavClicked(_ sender: Any)
    {
    //print("btn Fav Clicked self.scene Index:",self.sceneIndex)
            if (self.imagesArray.count > self.sceneIndex)
            {
                self.imagesArray.remove(at: self.sceneIndex)

                let fileURL = self.fileUrlArray[self.sceneIndex]
    //print("btn Fav Clicked fileURL:",fileURL as Any)

                do
                {
                    try FileManager.default.removeItem(atPath: fileURL.path)
                } catch {
      print("btn Fav Clicked Something wrong.")
                }
                let lastpath = fileURL.lastPathComponent
                DataManager.removeFavoriteSceneNew(mainFolderName:mainFolderName, catFolderName: lastpath)
                if (self.sceneList.count > self.sceneIndex) //******** crash fix 2.1.5
                {
                	self.fileUrlArray.remove(at: self.sceneIndex)
                	self.sceneList.remove(at: self.sceneIndex)
                }

    //print("btn Fav Clicked self.sceneIndex:",self.sceneIndex)
    //print("btn Fav Clicked self.sceneList.count:",self.sceneList.count)

                if (self.sceneIndex > self.sceneList.count-1)
                {
                    self.sceneIndex = max(self.sceneList.count-1,0);
                }

                if (self.imagesArray.count < 1)
                {
                    nodataLabel.isHidden = false
                    nodataLabel.text = "Create favorites to save ideas and share them with others."
                    self.favView.isHidden = true;
                }
            }

            self.loadImageFromDocumentDirectory(nameOfImage:self.favoriteImageName)

            self.imageCollectionView.reloadData()
            self.myGalleryTableView.reloadData()
        }
    
    //-------------------------------------------------
    @IBAction func btnDesignsClicked(_ sender: Any)
    {
        //print("0 btn Designs Clicked")
        self.btnDesignsWasClicked = true
        self.btnProductsWasClicked = false
        
        self.myGalleryTableView.delegate = self
        self.myGalleryTableView.dataSource = self
        self.imageCollectionView.delegate = self
        self.imageCollectionView.dataSource = self
        
        btnProducts.setTitleColor(titleColor, for: .normal)
        btnProducts.backgroundColor = themeColor

        if isClopay
        {
            btnDesigns.setTitleColor(titleColor, for: .normal)
            btnDesigns.backgroundColor = .white
        }else
        {
            btnDesigns.setTitleColor(themeColor, for: .normal)
            btnDesigns.backgroundColor = titleColor
        }
        
        
        self.myGalleryTableViewYConstarin.constant = 0
        self.imageCollectionView.isHidden = false
        self.myGalleryTableView.isHidden = false
        //     currentIndex = 0
        //     self.getDesignAPICall()
        myGalleryTableView.reloadData()
        //        self.imageCollectionView.isHidden = true
        //        self.myGalleryTableView.isHidden = true
        if self.imagesArray.count > 0
        {
            nodataLabel.isHidden = true
            self.favView.isHidden = false;
        } else {
            nodataLabel.isHidden = false
            nodataLabel.text = "Create favorites to save ideas and share them with others."
            self.favView.isHidden = true;
        }
        
        self.imageCollectionView.reloadData()
        self.myGalleryTableView.reloadData()
    }
    
    //	//-------------------------------------------------
    //    func productAPICall()  // not using change 2.0.6
    
    
    //----------------------------------------------
    @IBAction func btnProductsClicked(_ sender: Any)
    {
        //print("0 btn Products Clicked")
        self.btnProductsWasClicked = true
        self.btnDesignsWasClicked = false
        
        self.myGalleryTableView.delegate = self
        self.myGalleryTableView.dataSource = self
        
        btnDesigns.setTitleColor(titleColor, for: .normal)
        btnDesigns.backgroundColor = themeColor
        
        
        if isClopay
        {
            btnProducts.setTitleColor(titleColor, for: .normal)
            btnProducts.backgroundColor = .white
        }else
        {
            btnProducts.setTitleColor(themeColor, for: .normal)
            btnProducts.backgroundColor = titleColor
        }
        self.myGalleryTableViewYConstarin.constant = -245
        
        self.favView.isHidden = true;
        self.imageCollectionView.isHidden = true
        self.myGalleryTableView.isHidden = false
        //     self.productAPICall()
        
        
        //    if self.prodDoorCatlogList.count+self.prodGara geCatlogList.count > 0
        if self.imagesArray.count > 0
        {
            nodataLabel.isHidden = true
            //		self.favView.isHidden = false;
        } else {
            nodataLabel.isHidden = false
            //     nodataLabel.text = "No Products Selected"
            nodataLabel.text = "Create favorites to save ideas and share them with others."
            //		self.favView.isHidden = true;
        }
        
        self.myGalleryTableView.reloadData()
    }
    

   
    
    //MARK:- UITableview Delegate and Datasource methods
    //----------------------------------------------
    func numberOfSections(in tableView: UITableView) -> Int
    {
        //print("numberOfSections self.btnDesignsWasClicked:",self.btnDesignsWasClicked)
        if (self.btnDesignsWasClicked)
        {
            return 1
        } else {
            return 2
        }
    }
    
    //----------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (self.btnDesignsWasClicked)
        {
            //print("numberOfRowsInSection self.scen eIndex:",self.sceneIndex)
//            let paths = self.imageCollectionView.indexPathsForVisibleItems
//            //print("paths:",paths)
//            if (paths.count == 1)
//            {
//                let path = paths[0];
//                self.sceneIndex = path.row
//            }
//
            //print("tableView cellForRowAt indexPath self.scen eIndex:",self.sceneIndex)
            if (self.sceneList.count > self.sceneIndex)
            {
                let curCatList = self.sceneList[self.sceneIndex];
                //print("numberOfRowsInSection curCatList.count:",curCatList.count)
                return curCatList.count;
            } else {
                return 0;
            }
        } else {
            //print("numberOfRowsInSection section:",section)
            //print("numberOfRowsInSection self.prodGarageCatlogList.count:",self.prodGarageCatlogList.count)
            //print("numberOfRowsInSection self.prodDoorCatlogList.count:",self.prodDoorCatlogList.count)
            if section == 0
            {
                return self.prodGarageCatlogList.count
            } else {
                return self.prodDoorCatlogList.count
            }
        }
    }
    
    //----------------------------------------------
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 67;
    }
    
    //----------------------------------------------
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        //print("viewFor HeaderIn Section")
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
        //Set seperator
        let lineLabel = UILabel()
        lineLabel.frame = CGRect.init(x: 0, y: 7, width: headerView.frame.width, height: 1.5)
        lineLabel.backgroundColor = UIColor.init(red: 235.0/255.0, green: 235.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        
        let label = UILabel()
        
        //print("viewFor HeaderIn Section:",section)
        
        switch (section)
        {
        case 0:
            label.frame = CGRect.init(x: 15, y: 5, width: 140, height: 30)
            label.text = "GARRAGE";
        case 1:
            label.frame = CGRect.init(x: 15, y: 8, width: 140, height: 30)
            label.text = "DOORS";
            headerView.addSubview(lineLabel)
        default:
            label.text = "";
        }
        
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = titleColor
        headerView.addSubview(label)
        
        return headerView
    }
    
    //----------------------------------------------
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if btnDesignsWasClicked
        {
            return 0
        } else {
            if (self.prodGarageCatlogList.count == 0 && section == 0) || (self.prodDoorCatlogList.count == 0 && section == 1)
            {
                return 0
            }
            return 44
        }
    }
    
    //----------------------------------------------
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if btnDesignsWasClicked
        {
            //  if indexPath.row==0 {
            guard let myGalleryGarageCell = tableView.dequeueReusableCell(withIdentifier: "MyGalleryGarageCell") as? FavoriteGarageTableViewCell else
            {
                return UITableViewCell()
            }
            
            
            //print("tableView cellForRowAt self.scen eIndex:",self.sceneIndex)
            //print("tableView cellForRowAt indexPath:",indexPath)
            
            let curCatList = self.sceneList[self.sceneIndex];
            //print("tableView cellForRowAt curCatList:",curCatList)
            
            if (curCatList.count > indexPath.row)
            {
                myGalleryGarageCell.imgView.image = curCatList[indexPath.row].image
                myGalleryGarageCell.titleLabel.text = curCatList[indexPath.row].name
                myGalleryGarageCell.subTitleLabel.text = curCatList[indexPath.row].descriptions
                
                myGalleryGarageCell.btnCart.tag = indexPath.row
                let catAvail = self.checkZipCodeAvailabilityforMID(mID: curCatList[indexPath.row].manufacturerID)
                
                if shoppingVal == "1" && catAvail
                {
                    myGalleryGarageCell.btnCart.isHidden = false
                } else {
                    myGalleryGarageCell.btnCart.isHidden = true
                }
                
                
                if curCatList[indexPath.row].checkout
                {
                    myGalleryGarageCell.btnCart.setImage(UIImage(named: "proposals-v3"), for: .normal)
                    myGalleryGarageCell.btnCart.isSelected = true
                } else {
                    myGalleryGarageCell.btnCart.setImage(UIImage(named: "proposals-v2"), for: .normal)
                    myGalleryGarageCell.btnCart.isSelected = false
                }
                myGalleryGarageCell.btnCart.addTarget(self, action: #selector(checkoutSelected(sender:)), for: .touchUpInside)
                
                tableView.allowsSelection = true
            }
            
            return myGalleryGarageCell;
            
        } else {
            if indexPath.section == 0
            {
                guard let myGalleryGarageCell = tableView.dequeueReusableCell(withIdentifier: "MyGalleryGarageCell") as? FavoriteGarageTableViewCell else
                {
                    return UITableViewCell()
                }
                //      myGalleryGarageCell.imgView.sd_setImage(with: URL(string: prodGarageCatlogList[indexPath.row].img_url), placeholderImage: UIImage(named: ""))
                
                myGalleryGarageCell.imgView.image = prodGarageCatlogList[indexPath.row].image
                myGalleryGarageCell.titleLabel.text = prodGarageCatlogList[indexPath.row].name
                myGalleryGarageCell.subTitleLabel.text = prodGarageCatlogList[indexPath.row].descriptions
                
                myGalleryGarageCell.btnCart.tag = indexPath.row
                               let catAvail = self.checkZipCodeAvailabilityforMID(mID: prodGarageCatlogList[indexPath.row].manufacturerID)
                
                if shoppingVal == "1" && catAvail
                {

                    myGalleryGarageCell.btnCart.isHidden = false
                }else
                {
                    myGalleryGarageCell.btnCart.isHidden = true
                }
                
                if prodGarageCatlogList[indexPath.row].checkout
                {
                    myGalleryGarageCell.btnCart.setImage(UIImage(named: "proposals-v3"), for: .normal)
                    myGalleryGarageCell.btnCart.isSelected = true
                } else {
                    myGalleryGarageCell.btnCart.setImage(UIImage(named: "proposals-v2"), for: .normal)
                    myGalleryGarageCell.btnCart.isSelected = false
                }
                myGalleryGarageCell.btnCart.addTarget(self, action: #selector(checkoutSelected(sender:)), for: .touchUpInside)
                
                tableView.allowsSelection = true
                return myGalleryGarageCell
            } else {
                guard let myGalleryGarageCell = tableView.dequeueReusableCell(withIdentifier: "MyGalleryGarageCell") as? FavoriteGarageTableViewCell else
                {
                    return UITableViewCell()
                }
                //       myGalleryGarageCell.imgView.sd_setImage(with: URL(string: prodDoorCatlogList[indexPath.row].img_url), placeholderImage: UIImage(named: ""))
                
                myGalleryGarageCell.imgView.image = prodDoorCatlogList[indexPath.row].image
                myGalleryGarageCell.titleLabel.text = prodDoorCatlogList[indexPath.row].name
                myGalleryGarageCell.subTitleLabel.text = prodDoorCatlogList[indexPath.row].descriptions
                
                myGalleryGarageCell.btnCart.tag = indexPath.row+100
                
                let catAvail = self.checkZipCodeAvailabilityforMID(mID: prodDoorCatlogList[indexPath.row].manufacturerID)
                              
                if shoppingVal == "1" && catAvail
                                              {
                    myGalleryGarageCell.btnCart.isHidden = false
                } else {
                    myGalleryGarageCell.btnCart.isHidden = true
                }
                
                if prodDoorCatlogList[indexPath.row].checkout
                {
                    myGalleryGarageCell.btnCart.setImage(UIImage(named: "proposals-v3"), for: .normal)
                    myGalleryGarageCell.btnCart.isSelected = true
                } else {
                    myGalleryGarageCell.btnCart.setImage(UIImage(named: "proposals-v2"), for: .normal)
                    myGalleryGarageCell.btnCart.isSelected = false
                }
                
                myGalleryGarageCell.btnCart.addTarget(self, action: #selector(checkoutSelected(sender:)), for: .touchUpInside)
                
                tableView.allowsSelection = true
                
                return myGalleryGarageCell
            }
        }
    }
    
    @objc func checkoutSelected(sender: UIButton)
    {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected
        {
            sender.setImage(UIImage(named: "proposals-v3"), for: .normal)
        } else {
            sender.setImage(UIImage(named: "proposals-v2"), for: .normal)
        }
        if btnProductsWasClicked
        {
            
            
            if sender.tag >= 100
            {
                AlertViewManager.shared.ShowOkAlert(title:"Sorry", message: "Front doors are not available for purchase at this time in your area.", handler: nil)
                sender.setImage(UIImage(named: "proposals-v2"), for: .normal)
                
                return
            }else
            {
                let catlogSelected:CatelogList = prodGarageCatlogList[sender.tag]
                
                var checkout = false
                if sender.isSelected
                {
                    checkout = true
                }else
                {
                    checkout = false
                }
                let destFile = catlogSelected.file
                if destFile.count > 2
                {
                    let arrayTemp = destFile.components(separatedBy:"__")
                    let dic = ["name":catlogSelected.name,"desc":catlogSelected.descriptions ,"price":catlogSelected.price ,"catID":catlogSelected.catlogID,"checkout":checkout,"fileURL":catlogSelected.file,"fileName":catlogSelected.filename,"mID":catlogSelected.manufacturerID] as [String : Any]
                    
                    let DocumentDirectory = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
                    if let filePath = DocumentDirectory.appendingPathComponent(arrayTemp[0])
                    {
                        let filenamedetailsname = arrayTemp[1]
                        let fileURLdeails = filePath.appendingPathComponent(filenamedetailsname)
                        
                        do {
                            let jsonData: Data? = try? JSONSerialization.data(withJSONObject: dic)
                            DataManager.UpdateFavoriteScenes(dataToConve:jsonData!,filename:catlogSelected.file , typeName: "garagerName")
                            try jsonData!.write(to:fileURLdeails,options:.atomic)
                        } catch {
                            print("error saving",error.localizedDescription)
                        }
                    }
                }
            }
        }else
        {
            
            var curCatList = self.sceneList[self.sceneIndex]
            let catlogSelected:CatelogList = curCatList[sender.tag]
            if catlogSelected.type.contains("door")
            {
                AlertViewManager.shared.ShowOkAlert(title:"Sorry", message: "Front doors are not available for purchase at this time in your area.", handler: nil)
                sender.setImage(UIImage(named: "proposals-v2"), for: .normal)
                
                return
            }
            var checkout = false
            if sender.isSelected
            {
                checkout = true
                catlogSelected.checkout = true
                
            }else
            {
                checkout = false
                catlogSelected.checkout = false
                
            }
            curCatList[sender.tag] = catlogSelected
            self.sceneList[self.sceneIndex] = curCatList
            let destFile = catlogSelected.file
            if destFile.count > 2
            {
                let arrayTemp = destFile.components(separatedBy:"__")
                let dic = ["name":catlogSelected.name,"desc":catlogSelected.descriptions ,"price":catlogSelected.price ,"catID":catlogSelected.catlogID,"checkout":checkout,"fileURL":catlogSelected.file,"fileName":catlogSelected.filename,"mID":catlogSelected.manufacturerID] as [String : Any]
                
                let DocumentDirectory = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
                if let filePath = DocumentDirectory.appendingPathComponent(arrayTemp[0])
                {
                    let filenamedetailsname = arrayTemp[1]
                    let fileURLdeails = filePath.appendingPathComponent(filenamedetailsname)
                    
                    do {
                        let jsonData: Data? = try? JSONSerialization.data(withJSONObject: dic)
                        DataManager.UpdateFavoriteScenes(dataToConve:jsonData!,filename:catlogSelected.file , typeName: "garagerName")
                        try jsonData!.write(to:fileURLdeails,options:.atomic)
                    } catch {
                        print("error saving",error.localizedDescription)
                    }
                }
            }
            
            self.myGalleryTableView.reloadData()
        }
        
    }
    //-----------------------------------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let garageDetailVC = storyboard.instantiateViewController(withIdentifier: "GarageDetailVC") as! GarageDetailViewController
        garageDetailVC.view.backgroundColor = UIColor(red: 75.0 / 255.0, green: 75.0 / 255.0, blue: 75.0 / 255.0, alpha: 0.8)
        
        if (btnDesignsWasClicked)
        {
            if (self.sceneList.count > self.sceneIndex)
            {
                let curCatList = self.sceneList[self.sceneIndex];
                //print("didSelectRowAt indexPath:",indexPath)
                //print("didSelectRowAt curCatList.count:",curCatList.count)
                
                garageDetailVC.descriptionLabel.text = curCatList[indexPath.row].descriptions
                garageDetailVC.selectedImgView.image = curCatList[indexPath.row].image
                
                garageDetailVC.titleLabel.text = curCatList[indexPath.row].name
                garageDetailVC.subTitleLabel.text = ""
            }
            
            
        } else {
            
            if (self.sceneList.count > self.sceneIndex)
            {
                //		let curCatList = self.sce neList[self.sceneIndex];
                
                if indexPath.section == 0
                {
                    garageDetailVC.descriptionLabel.text = prodGarageCatlogList[indexPath.row].descriptions
                    //		garageDetailVC.selectedImgView.sd_setImage(with: URL(string: prodGarageCatlogList[indexPath.row].img_url), placeholderImage: UIImage(named: ""))
                    garageDetailVC.selectedImgView.image = prodGarageCatlogList[indexPath.row].image
                    
                    garageDetailVC.titleLabel.text = prodGarageCatlogList[indexPath.row].name
                    garageDetailVC.subTitleLabel.text = ""
                } else {
                    garageDetailVC.descriptionLabel.text = prodDoorCatlogList[indexPath.row].descriptions
                    //		garageDetailVC.selectedImgView.sd_setImage(with: URL(string: prodDoorCatlogList[indexPath.row].img_url), placeholderImage: UIImage(named: ""))
                    garageDetailVC.selectedImgView.image = prodDoorCatlogList[indexPath.row].image
                    
                    garageDetailVC.titleLabel.text = prodDoorCatlogList[indexPath.row].name
                    garageDetailVC.subTitleLabel.text = ""
                    
                    
                }
            }
        }
        
        garageDetailVC.providesPresentationContextTransitionStyle = true
        garageDetailVC.definesPresentationContext = true
        garageDetailVC.modalPresentationStyle = .overCurrentContext
        present(garageDetailVC, animated: true, completion: nil)
    }
    
    
    
    //MARK:- collectionView Delegate and Datasource methods
    //-------------------------------------------------
    func loadImageFromDocumentDirectory(nameOfImage : String)
    {
            //print("loadImage From Document Directory")
            let filename = nameOfImage.replacingOccurrences(of:".jpg", with: "")
            let folderName = filename+"_folder";
            mainFolderName = folderName
            self.imagesArray.removeAll()
            self.fileUrlArray.removeAll()
            self.sceneList.removeAll()
            self.prodGarageCatlogList.removeAll()
            self.prodDoorCatlogList.removeAll()
    
            let fileManager = FileManager.default
            let DocumentDirectory = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
            if let filePath = DocumentDirectory.appendingPathComponent(folderName)
            {
                if (fileManager.fileExists(atPath: filePath.path))
                {
                    do
                    {
                        let folderURLs = try fileManager.contentsOfDirectory(at: filePath, includingPropertiesForKeys: nil, options: .skipsHiddenFiles )
                        //    var fileURLs.filter{ $0.pathExtension = "jpg" }
                        //print("  ")
                        //print("  ")
                        //print("folderURLs:",folderURLs)
                        //print("  ")
                        //print("  ")
                        var nameDoors:[String:CatelogList] = [String:CatelogList]()
                        var nameGarages:[String:CatelogList] = [String:CatelogList]()
    
                        for folderURL in folderURLs
                        {
                            //print("  ")
                            //print("  ")
                            //print("  ")
                            //print("folderURL:",folderURL)
                            do
                            {
                                let fileURLs = try fileManager.contentsOfDirectory(at: folderURL, includingPropertiesForKeys: nil, options: .skipsHiddenFiles )
                                var catList: [CatelogList] = [CatelogList]()
                                //    var cat:CatelogList = CatelogList.init()
    
    
                                var garDic1:[String:Any] = [String:Any]()
                                var garDic2:[String:Any] = [String:Any]()
                                var doorDic1:[String:Any] = [String:Any]()
                                var doorDic2:[String:Any] = [String:Any]()
    
                                //------------- read in door Descriptions from disk ----------------
                                for fileURL in fileURLs
                                {
                                    if fileURL.path.contains("_favorite.jpg")
                                    {
                                        //print("         fileURL.path:",fileURL.path)
    
                                        if let image = UIImage(contentsOfFile: fileURL.path)
                                        {
                                            self.imagesArray.append(image)
                                            self.fileUrlArray.append(folderURL)
                                        }
                                    }
    
                                    if fileURL.path.contains("_1cardetails.txt")
                                    {
                                        if let contents = try? String(contentsOfFile: fileURL.path)
                                        {
                                            let data :Data = contents.data(using: String.Encoding.utf8)! as Data
                                            
                                            if let catDic = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String:Any]
                                            {
                                                //print(jsonArray) // use the json here
                                                garDic1 = catDic
                                                var urlpath = fileURL.path
                                                urlpath = urlpath.replacingOccurrences(of:"_1cardetails.txt", with: "_1car.jpg")
                                                if let image = UIImage(contentsOfFile:urlpath)
                                                {
                                                    
                                                    let garName1 = (garDic1["name"] as? String) ?? ""
                                                    
                                                    let cat:CatelogList = CatelogList.init(image: image, name:  (garDic1["name"] as? String) ?? "",
                                                    										descriptions: (garDic1["desc"] as? String) ?? "" ,
                                                    										catlogID: (garDic1["catID"] as? String) ?? "",
                                                    										price: (garDic1["price"] as? String) ?? "",
                                                    										checkout: (garDic1["checkout"] as? Bool) ?? false,
                                                    										fileURL:(garDic1["fileURL"] as? String) ?? "",
                                                    										fileName:(garDic1["fileName"] as? String) ?? "",
                                                    										type:"1car",mid:garDic1["mID"] as? Int ?? 0,
    																						mask_url:(garDic1["mask_url"] as? String) ?? " ",
    																						isPaintable:garDic1["isPaintable"] as? Int ?? 0,
    																						rValue:garDic1["rValue"] as? CGFloat ?? 0.0,
    																						gValue:garDic1["gValue"] as? CGFloat ?? 0.0,
    																						bValue:garDic1["bValue"] as? CGFloat ?? 0.0)

                                                    catList.append(cat)
                                                    nameGarages[garName1] = cat
                                                }
                                            }
                                            
                                            
                                        }
                                    }
    
    
                                    if fileURL.path.contains("_2cardetails.txt")
                                    {
                                        if let contents = try? String(contentsOfFile: fileURL.path)
                                        {
                                            let data :Data = contents.data(using: String.Encoding.utf8)! as Data
    
                                            if let catDic = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String:Any]
                                            {
                                                //print(jsonArray) // use the json here
                                                garDic2 = catDic
                                                var urlpath = fileURL.path
                                                urlpath = urlpath.replacingOccurrences(of:"_2cardetails.txt", with: "_2car.jpg")
                                           if let image = UIImage(contentsOfFile:urlpath)
                                        {
                                            let garName2 = (garDic2["name"] as? String) ?? ""
    
                                            let cat:CatelogList = CatelogList.init(image: image, name:  (garDic2["name"] as? String) ?? "",
                                            												descriptions: (garDic2["desc"] as? String) ?? "" ,
                                            												catlogID: (garDic2["catID"] as? String) ?? "",
                                            												price: (garDic2["price"] as? String) ?? "",
                                            												checkout: (garDic2["checkout"] as? Bool) ?? false,
                                            												fileURL:(garDic2["fileURL"] as? String) ?? "",
                                            												fileName:(garDic2["fileName"] as? String) ?? "",
                                            												type:"2car",mid:garDic2["mID"] as? Int ?? 0,
    																						mask_url:(garDic2["mask_url"] as? String) ?? " ",
    																						isPaintable:garDic2["isPaintable"] as? Int ?? 0,
    																						rValue:garDic2["rValue"] as? CGFloat ?? 0.0,
    																						gValue:garDic2["gValue"] as? CGFloat ?? 0.0,
    																						bValue:garDic2["bValue"] as? CGFloat ?? 0.0)

                                            catList.append(cat)
                                            nameGarages[garName2] = cat
                                        
                                            }
    
                                        }
                                    }
    
                                    }
    
                                    if fileURL.path.contains("_1doordetails.txt")
                                    {
                                        if let contents = try? String(contentsOfFile: fileURL.path)
                                        {
                                            let data :Data = contents.data(using: String.Encoding.utf8)! as Data
                                            
                                            if let catDic = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String:Any]
                                            {
                                                //print(jsonArray) // use the json here
                                                doorDic1 = catDic
                                                var urlpath = fileURL.path
                                                urlpath = urlpath.replacingOccurrences(of:"_1doordetails.txt", with: "_1door.jpg")
                                                
                                                if let image = UIImage(contentsOfFile:urlpath)
                                                {
                                                    let doorName1 = (doorDic1["name"] as? String) ?? ""
                                                    let cat:CatelogList = CatelogList.init(image: image, name:  (doorDic1["name"] as? String) ?? "",
                                                    										descriptions: (doorDic1["desc"] as? String) ?? "" ,
                                                    										catlogID: (doorDic1["catID"] as? String) ?? "",
                                                    										price: (doorDic1["price"] as? String) ?? "",
                                                    										checkout: (doorDic1["checkout"] as? Bool) ?? false,
                                                    										fileURL:(doorDic1["fileURL"] as? String) ?? "",
                                                    										fileName:(doorDic1["fileName"] as? String) ?? "",
                                                    										type:"1door",mid:doorDic1["mID"] as? Int ?? 0,
    																						mask_url:(doorDic1["mask_url"] as? String) ?? " ",
    																						isPaintable:doorDic1["isPaintable"] as? Int ?? 0,
    																						rValue:doorDic1["rValue"] as? CGFloat ?? 0.0,
    																						gValue:doorDic1["gValue"] as? CGFloat ?? 0.0,
    																						bValue:doorDic1["bValue"] as? CGFloat ?? 0.0)

                                                    catList.append(cat)
                                                    nameDoors[doorName1] = cat
                                                }
                                            }
                                            
                                            
                                        }
                                    }
    
                                    if fileURL.path.contains("_2doordetails.txt")
                                    {
                                        if let contents = try? String(contentsOfFile: fileURL.path)
                                        {
                                            let data :Data = contents.data(using: String.Encoding.utf8)! as Data
    
                                            if let catDic = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String:Any]
                                            {
                                                //print(jsonArray) // use the json here
                                                doorDic2 = catDic
                                                var urlpath = fileURL.path
                                                                                               urlpath = urlpath.replacingOccurrences(of:"_2doordetails.txt", with: "_2door.jpg")
                                                                                               
                                                                                               if let image = UIImage(contentsOfFile:urlpath)
                                                                                               {
                                   
                                       
                                            let doorName2 = (doorDic2["name"] as? String) ?? ""
    
                                            let cat:CatelogList = CatelogList.init(image: image, name:  (doorDic2["name"] as? String) ?? "",
                                            												descriptions: (doorDic2["desc"] as? String) ?? "" ,
                                            												catlogID: (doorDic2["catID"] as? String) ?? "",
                                            												price: (doorDic2["price"] as? String) ?? "",
                                            												checkout: (doorDic2["checkout"] as? Bool) ?? false,
                                            												fileURL:(doorDic2["fileURL"] as? String) ?? "",
                                            												fileName:(doorDic2["fileName"] as? String) ?? "",
                                            												type:"2door",mid:doorDic2["mID"] as? Int ?? 0,
    																						mask_url:(doorDic2["mask_url"] as? String) ?? " ",
    																						isPaintable:doorDic2["isPaintable"] as? Int ?? 0,
    																						rValue:doorDic2["rValue"] as? CGFloat ?? 0.0,
    																						gValue:doorDic2["gValue"] as? CGFloat ?? 0.0,
    																						bValue:doorDic2["bValue"] as? CGFloat ?? 0.0)

                                            catList.append(cat)
                                            nameDoors[doorName2] = cat
                                        }
                                    }
                                            
    
                                        }
                                    }
                                }
    
                                if catList.count>0 //********** garage load issie 2.1.5
                                {
                                    self.sceneList.append(catList)
								}
    
                            } catch {
                                //print("no files")
                            }
                        }
    
                        for (_,value) in nameDoors
                        {
                            self.prodDoorCatlogList.append(value)
                        }
    
                        for (_,value) in nameGarages
                        {
                            self.prodGarageCatlogList.append(value)
                        }
    
                    } catch {
                        //print("no folders")
                    }
    
                } else {
                    //print("00 no folders")
                }
            }
            //print("  ")
            //print("  ")
            //print("  ")
        }
//    func loadImageFromDocumentDirectory(nameOfImage : String) // not using change 2.0.6
//
    
    
    
    //	//-----------------------------------------------------------------
    //	func replaceObjectAtAll(oneCarIndex: UIImageView, twoCarIndex: UIImageView, singlFrntDrIndex: UIImageView, doublFrntDrIndex: UIImageView) // not using change 2.0.6
   
    
    //--------------------------------------------
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        //        if (designListArray != nil)
        //        {
        //            return self.designListArray.count
        //        } else {
        //            return 0
        //        }
        
        return self.imagesArray.count
    }
    
    //--------------------------------------------
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    
    //--------------------------------------------
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let multipleHomeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavCollectionCell", for: indexPath) as! FavCollectionCell
        //print("collectionView multipleHomeCell:",multipleHomeCell as Any)
        let origImage = UIImage(named: "double-chevron")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        multipleHomeCell.chevoronBtn.setImage(tintedImage, for: .normal)
        multipleHomeCell.chevoronBtn.tintColor = themeColor
        if indexPath.row == 0 && self.imagesArray.count > 1
        {
            multipleHomeCell.chevoronBtn.isHidden = false
        } else {
            multipleHomeCell.chevoronBtn.isHidden = true
        }
        
        //print("collectionView self.imagesArray.count:",self.imagesArray.count)
        
        multipleHomeCell.doorImageView.image = self.imagesArray[indexPath.row]
        //print("collectionView multipleHomeCell.doorImageView.image:",multipleHomeCell.doorImageView.image as Any)
        
        multipleHomeCell.layoutIfNeeded()
        return multipleHomeCell
    }
    
    //--------------------------------------------
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        //print(" ")
        //print(" ")
        //print(" ")
        //print("scrollView Did End Decelerating")
        if (btnDesignsWasClicked && (scrollView == imageCollectionView))
        {
            var visibleRect = CGRect()
            visibleRect.origin = imageCollectionView.contentOffset
            visibleRect.size = imageCollectionView.bounds.size
            
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            
            guard let indexPath = imageCollectionView.indexPathForItem(at: visiblePoint) else { return }
            //print("indexPath",indexPath)
            
            //			let paths = self.imageCollectionView.indexPathsForVisibleItems
            //print("paths:",paths)
            //			if (paths.count > 0)
            //			{
            //				let path = paths[0];
            //			}
            
            self.sceneIndex = indexPath.row
            //print("self.scen eIndex:",self.sceneIndex)
            
            self.myGalleryTableView.reloadData()
        }
    }
    
    
    //--------------------------------------------
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 373 , height: 215)
    }
    func checkZipCodeAvailabilityforMID(mID:Int)->Bool
    {
        let zipcode = UserDefaults.standard.value(forKey:"zipcode") as? String ?? ""

        let manufactureZipcodes = DataManager.loadManufacturerZipCodes()

        if manufactureZipcodes.count>0
        {
            
            
            if let zipArray = manufactureZipcodes[String(mID)] as? [String]
            {
                if zipArray.contains(zipcode)
                {
                    return true
                }else
                {
                    return true // changing for production ModifaiPro for showing cart for all products
                }
            }else
            {
                return true // changing for production ModifaiPro for showing cart for all products
            }
            
            
        }else
        {
            return true // changing for production ModifaiPro for showing cart for all products
        }
    }
}
