import UIKit
import AuthenticationServices
import FirebaseAnalytics
class OnboardingViewController: UIViewController, UIScrollViewDelegate,SignInClickDelegate{

    @IBOutlet weak var scrollView: UIScrollView!{
        didSet{
            scrollView.delegate = self
        }
    }
    
    @IBOutlet weak var pageControl: UIPageControl!
   // @IBOutlet weak var previousButton:UIButton!
    @IBOutlet weak var nextButton:UIButton!
    @IBOutlet weak var startendButton:UIButton!
    @IBOutlet weak var skip:UIButton!
    var changeDescLabl:UILabel!
    var isTakePhoto  = false
    var isEditIntro  = false
    var isPaintIntro  = false
    var isPaintTour  = false

    var maskOfDoor  = false  //************** change 1.1.0 add ***
    var maskOfFrontDoor  = false  //************** change 1.1.0 add *** 


    var signInWithAppleButton = ASAuthorizationAppleIDButton()
var usesrExists = false
    var isSignIn  = false
    var signUpOverVC : SignupPopUp!

    var zipcode = ""
    struct OnboardingItem {
        var title = String()
        var description = String()
        var description1 = String()
        var description2 = String()
        var Image1 =  String()
        var Image2 =  String()
        var Image =  String()
        var Image3 =  String()

    }
    
    var onboardingItems = [OnboardingItem(title: "Welcome", description: "We use AI to let you easily see what new garage doors look like on your home.",description1:"Just take a photo or pick an image from your camera roll and within seconds you'll be ready to modifai your home with new products from our catalog.",description2:"It's easy to share your ideas with family and friends for feedback.", Image1: "intelligence",Image2:"picture home" ,Image:"cam1",Image3:"add house")]
    
   
    var slides:[Slide] = [];
    var views:[UIView] = [];
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        AppUtility.lockOrientation(.portrait)
        
        if (hasOnlyGarageDoors) //********** change 1.0.0 add if block ***
        {
            //********** change 2.1.1
        onboardingItems = [OnboardingItem(title: "Welcome", description: "We use AI to let you easily see what new garage doors look like on your home.",description1:"Just take a photo or pick an image from your camera roll and within seconds you'll be ready to modifai your home with new products from our catalog.",description2:"It's easy to share your ideas with family and friends for feedback.", Image1: "intelligence",Image2:"picture home" ,Image:"cam1",Image3:"add house")]
        }


        self.startendButton.layer.cornerRadius = 16
        self.nextButton.layer.cornerRadius = 16
        self.skip.layer.cornerRadius = 16

        if isTakePhoto {
            //Analytics.logEvent("Camera_tutorial", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
            self.views = self.createPhoto()
            setupSlideScrollView(slides: slides)
            pageControl.numberOfPages = self.views .count
        } else if isEditIntro {
            //Analytics.logEvent("Camera_tutorial", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
            self.views = self.createEditSlides()
            setupSlideScrollView(slides: slides)
            pageControl.numberOfPages = self.views .count
        } else if isPaintIntro {
            //Analytics.logEvent("Camera_tutorial", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
            self.views = self.createPaintSlides()
            setupSlideScrollView(slides: slides)
            pageControl.numberOfPages = self.views.count
        }else {
            // Analytics.logEvent("Onboarding_tutorial", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
            self.loadKeychainValues()
            slides = createSlides()
            setupSlideScrollView(slides: slides)
            pageControl.numberOfPages = slides.count
        }
        
        //   UIScrollView.appearance().contentInsetAdjustmentBehavior = .never //scroll issue in sharing option
        pageControl.currentPage = 0
        view.bringSubviewToFront(pageControl)
        //view.bringSubviewToFront(previousButton)
        view.bringSubviewToFront(nextButton)
        view.bringSubviewToFront(startendButton)
        self.setupButtonsBasedOnPage()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        UserDefaults.standard.set("pending", forKey: "downloadDone")
        
    }
    func loadKeychainValues()
    {
       

        let bundleIdentifier = Bundle.main.bundleIdentifier
        let bundleName = String(describing:bundleIdentifier)
        if let receivedData = KeyChain.load(key: "\(bundleName)newUserID") {
            let result = String(decoding: receivedData, as: UTF8.self)
  print(" UserID result: ", result)
             usesrExists = true
           // UserDefaults.standard.set(result, forKey: "UserID")
             
        }
        if let receivedData1 = KeyChain.load(key: "\(bundleName)agentzipcode") {
                   let result = String(decoding: receivedData1, as: UTF8.self)
  print(" zipcode result: ", result)
                   zipcode = result
                  // UserDefaults.standard.set("0" , forKey:"shopping")
               }
       


    }

    override func viewWillAppear(_ animated: Bool)
    {
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
   
    func createSlides() -> [Slide]
    {
        for onboaedingItem in onboardingItems
        {
            let slide1:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
            slide1.imageView.image = UIImage(named: onboaedingItem.Image)
            slide1.imageView.layer.cornerRadius = 20
            slide1.imageView.layer.masksToBounds = true
            slide1.labelTitle.text = onboaedingItem.title
            slide1.labelDesc.text = onboaedingItem.description
            slide1.labelDesc2.text = onboaedingItem.description1
            slide1.labelDesc3.text = onboaedingItem.description2

            slide1.image1.image = UIImage(named: onboaedingItem.Image1)
            slide1.image2.image = UIImage(named: onboaedingItem.Image2)
            slide1.image3.image = UIImage(named: onboaedingItem.Image3)

            slide1.backgroundColor = themeColor

            slides.append(slide1)
        }
        return slides


    }
   
    func createEditSlides() -> [UIView]
    {
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Walkthrough", bundle: nil)
    //                let myPhotoVC = storyBoard.instantiateViewController(withIdentifier: "MyPhotoVC") as! MyPhotoViewController
      
            let myPhotoVC1 = storyBoard.instantiateViewController(withIdentifier: "EditingTutorialViewController") as! EditingTutorialViewController
                       
            let myPhotoVC2 = storyBoard.instantiateViewController(withIdentifier: "EditingTutorial1ViewController") as! EditingTutorial1ViewController
                              
            let myPhotoVC3 = storyBoard.instantiateViewController(withIdentifier: "EditingTutorial2ViewController") as! EditingTutorial2ViewController
                                
            let myPhotoVC4 = storyBoard.instantiateViewController(withIdentifier: "EditingTutorial3ViewController") as! EditingTutorial3ViewController
        
               self.views.append(myPhotoVC1.view)
               self.views.append(myPhotoVC2.view)
               self.views.append(myPhotoVC3.view)
               self.views.append(myPhotoVC4.view)
             
           

   
              return self.views
            
        }
     func createPaintSlides() -> [UIView]
     {
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Walkthrough", bundle: nil)
   
       
        let imgArray = ["paint1","paint2","paint3","paint4","paint5"]

        for i in 0..<imgArray.count
        {
//            let myPhotoVC1 = PaintIntoView.instantiate(title:tutorialLabels[i], imgName: imgArray[i])
            let myPhotoVC1 =  UINib(nibName: "PaintIntoView", bundle: nil).instantiate(withOwner: self, options: nil).first as! PaintIntoView

            myPhotoVC1.topImagView.image = UIImage(imageLiteralResourceName:imgArray[i])

            self.views.append(myPhotoVC1)
        }
        return self.views
             
         }
    func createPhoto() -> [UIView] {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Walkthrough", bundle: nil)
//                let myPhotoVC = storyBoard.instantiateViewController(withIdentifier: "MyPhotoVC") as! MyPhotoViewController
          let myPhotoVC1 = storyBoard.instantiateViewController(withIdentifier: "camIntro1ViewController") as! camIntro1ViewController
          let myPhotoVC2 = storyBoard.instantiateViewController(withIdentifier: "camIntro2ViewController") as! camIntro2ViewController
          let myPhotoVC3 = storyBoard.instantiateViewController(withIdentifier: "camIntro3ViewController") as! camIntro3ViewController
          let myPhotoVC4 = storyBoard.instantiateViewController(withIdentifier: "camIntro4ViewController") as! camIntro4ViewController
          let myPhotoVC5 = storyBoard.instantiateViewController(withIdentifier: "camIntro5ViewController") as! camIntro5ViewController
        //myPhotoVC5.emailBtn.addTarget(self, action: #selector(TapButtonAction(_:)), for: .touchUpInside)
        self.views.append(myPhotoVC1.view)
        self.views.append(myPhotoVC2.view)
        self.views.append(myPhotoVC3.view)
        self.views.append(myPhotoVC4.view)
        self.views.append(myPhotoVC5.view)

//            let photoHomeView = storyBoard.instantiateViewController(withIdentifier: "MyPhotoHomeVC") as! MyPhotoHomeViewCon troller
//            self.views.append(photoHomeView.view)
        
          return self.views
        
    }
    
  
    
    func setupSlideScrollView(slides : [Slide]) {
        scrollView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        scrollView.isScrollEnabled = false
        scrollView.isPagingEnabled = true
        if self.isTakePhoto || isEditIntro || isPaintIntro {
            var height:CGFloat = 580
            
            if isPaintIntro
            {
                height = UIScreen.main.bounds.size.height
            }
            for i in 0 ..< views.count {
                views[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: height)
                scrollView.addSubview(views[i])
            }
            scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(views.count), height: height)
        }else {
            for i in 0 ..< slides.count {
                slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: 580)
                scrollView.addSubview(slides[i])
            }
            scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: 580)
        }
       
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
        self.setupButtonsBasedOnPage()
    }
    
    /*
     * default function called when view is scolled. In order to enable callback
     * when scrollview is scrolled, the below code needs to be called:
     * slideScrollView.delegate = self or
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
         self.setupButtonsBasedOnPage()
    }

    func scrollView(_ scrollView: UIScrollView, didScrollToPercentageOffset percentageHorizontalOffset: CGFloat) {
        if(pageControl.currentPage == 0) {
            //Change background color to toRed: 103/255, fromGreen: 58/255, fromBlue: 183/255, fromAlpha: 1
            //Change pageControl selected color to toRed: 103/255, toGreen: 58/255, toBlue: 183/255, fromAlpha: 0.2
            //Change pageControl unselected color to toRed: 255/255, toGreen: 255/255, toBlue: 255/255, fromAlpha: 1
            
            let pageUnselectedColor: UIColor = fade(fromRed: 255/255, fromGreen: 255/255, fromBlue: 255/255, fromAlpha: 1, toRed: 103/255, toGreen: 58/255, toBlue: 183/255, toAlpha: 1, withPercentage: percentageHorizontalOffset * 3)
           
            pageControl.pageIndicatorTintColor = pageUnselectedColor
        
            
            let bgColor: UIColor = fade(fromRed: 103/255, fromGreen: 58/255, fromBlue: 183/255, fromAlpha: 1, toRed: 255/255, toGreen: 255/255, toBlue: 255/255, toAlpha: 1, withPercentage: percentageHorizontalOffset * 3)
            if isTakePhoto || isEditIntro  || isPaintIntro{
                views[pageControl.currentPage].backgroundColor = bgColor
            }else {
                 slides[pageControl.currentPage].backgroundColor = bgColor
            }
           
            
            let pageSelectedColor: UIColor = fade(fromRed: 81/255, fromGreen: 36/255, fromBlue: 152/255, fromAlpha: 1, toRed: 103/255, toGreen: 58/255, toBlue: 183/255, toAlpha: 1, withPercentage: percentageHorizontalOffset * 3)
            pageControl.currentPageIndicatorTintColor = pageSelectedColor
        }
         self.setupButtonsBasedOnPage()
    }
    
    
    func fade(fromRed: CGFloat,
              fromGreen: CGFloat,
              fromBlue: CGFloat,
              fromAlpha: CGFloat,
              toRed: CGFloat,
              toGreen: CGFloat,
              toBlue: CGFloat,
              toAlpha: CGFloat,
              withPercentage percentage: CGFloat) -> UIColor {
        
        let red: CGFloat = (toRed - fromRed) * percentage + fromRed
        let green: CGFloat = (toGreen - fromGreen) * percentage + fromGreen
        let blue: CGFloat = (toBlue - fromBlue) * percentage + fromBlue
        let alpha: CGFloat = (toAlpha - fromAlpha) * percentage + fromAlpha
        
        // return the fade colour
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }

   @IBAction func pageChanged(_ sender: Any) {
        let xVal = CGFloat(pageControl.currentPage) * scrollView.frame.size.width
        self.scrollView.setContentOffset(CGPoint(x: xVal, y: 0), animated: true)
        self.setupButtonsBasedOnPage()
    }
    
    @IBAction func previousButtonTapped(_ sender: Any) {
        let xVal = CGFloat(pageControl.currentPage - 1) * scrollView.frame.size.width
        self.scrollView.setContentOffset(CGPoint(x: xVal, y: 0), animated: true)
          self.setupButtonsBasedOnPage()
    }
    @IBAction func nextButtonTapped(_ sender: Any) {
        
        let xVal = CGFloat(pageControl.currentPage + 1) * scrollView.frame.size.width
        self.scrollView.setContentOffset(CGPoint(x: xVal, y: 0), animated: true)
          self.setupButtonsBasedOnPage()
    }
    override var prefersStatusBarHidden: Bool {
             return true
         }
    @IBAction func startEndButtonTapped(_ sender: UIButton) {
         if(sender == skip)
         {
            if isTakePhoto
            {
             //Analytics.logEvent("Photo_tutorial_skip_button_selected", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
            }
            else
            {
                //Analytics.logEvent("Onboarding_tutorial_skip_button_selected", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
            }
         }
         if(sender == startendButton)
                {
                   if isTakePhoto
                   {
                    //Analytics.logEvent("Photo_tutorial_completed", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
                   }
                   else
                   {
                       //Analytics.logEvent("Onboarding_tutorial_completed", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
                   }
                }
         self.navigationController?.setNavigationBarHidden(true, animated: true)
         let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if isTakePhoto {
           // appDelegate.showCustomTabbar(isCameraOpen: true)
            self.navigationController?.setNavigationBarHidden(false, animated: true)
                       let backButton = UIBarButtonItem()
                       backButton.title = "Home"
                       self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
            self.navigationController?.popViewController(animated:true)
            homeviewController.openCamera()
        }else if isEditIntro {
            
            self.navigationController?.setNavigationBarHidden(false, animated: true)
                let backButton = UIBarButtonItem()
                              backButton.title = "Home"
                              self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
            self.navigationController?.popViewController(animated:true)
            currentMainImageController.scrollView.isHidden = false
        } else if isPaintIntro {

            self.navigationController?.setNavigationBarHidden(true, animated: true) //********* change 2.1.0
//            let backButton = UIBarButtonItem() //********* change 2.1.0
//            backButton.title = "Home"
//            self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
            self.dismiss(animated:true, completion: nil) //********* change 2.1.0
//print("isPaintTour:",isPaintTour)
			if (self.maskOfDoor)  //************** change 1.1.0 add if block ***
			{
				//self.navigationController?.popViewController(animated:false) //********* change 2.1.0
				homeviewController.paintActionOnSelection(sender: nil)   //*************** change 1.1.4 change to nil? optional ***
			} else if (self.maskOfFrontDoor) {
				//self.navigationController?.popViewController(animated:false) //********* change 2.1.0
				homeviewController.paintFDActionOnSelection(sender: nil)   //*************** change 1.1.4 change to nil? optional ***
			} else {
				if isPaintTour
				{
					self.dismiss(animated:true, completion: nil)
				} else {
				//	self.navigationController?.popViewController(animated:false) //********* change 2.1.0
					homeviewController.btnPaintLastItemClicked("")
				}
            }
        } else {
          //  appDelegate.showCustomTabbar(isCameraOpen: false)
            let bundleIdentifier = Bundle.main.bundleIdentifier
            let bundleName = String(describing:bundleIdentifier)
                    
            if let receivedData2 = KeyChain.load(key: "\(bundleName)newaccessTokenValue") {
                print("acessToken",receivedData2)
            self.signUpOverVC = UIStoryboard(name: "CustomTabBar", bundle: nil).instantiateViewController(withIdentifier: "SignupPopUp") as? SignupPopUp
                   //                        self.addChild(self.popOverVC)
                   self.signUpOverVC.view.frame = self.view.frame
                   //                        self.view.addSubview(self.popOverVC.view)
                   //                        self.popOverVC.didMove(toParent: self)
					self.signUpOverVC.modalPresentationStyle = .overCurrentContext  //.overFullScreen //.po pover   //********** change 3.0.3 change  ***
                //  self.signUpOverVC.titleLabel.text = "In order to restore your favorites and pictures you need to Sign In" //**************** remove Lakshmi - 2.0.2
            self.signUpOverVC.fromOnboard = true
                   self.signUpOverVC.delegate = self
                   self.signUpOverVC.modalPresentationCapturesStatusBarAppearance = true

                   self.present(self.signUpOverVC, animated: true, completion: nil)
            }
            else{
                appDelegate.showCustomTabbar(isCameraOpen: false)
            }
        }
        
    }
    func onSignInCompletionClick(sender: String)
       {
           self.navigationController?.setNavigationBarHidden(true, animated: true)
                      let appDelegate = UIApplication.shared.delegate as! AppDelegate
                      appDelegate.showCustomTabbar(isCameraOpen: false)
                     // appDelegate.getPreviousHouseListAPI()
                      DataManager.GetFavoriteScenes()
       // DataManager.GetPai ntColors()

       }
    func setupButtonsBasedOnPage() {
        if !isTakePhoto && !isEditing && !isPaintTour && !isPaintIntro //********** change 2.1.1 if block
        {
            self.nextButton.isHidden = true
            self.startendButton.isHidden = false
            self.skip.isHidden = true
            return
        }
        //self.previousButton.isHidden = false
        self.nextButton.isHidden = false
        self.startendButton.isHidden = true
        self.skip.isHidden = true
        if self.pageControl.currentPage == 0 {
       //     self.previousButton.isHidden = true
            self.startendButton.isHidden = true
           
            if usesrExists
            {
                 self.skip.isHidden = false
            }else
            {
            self.skip.isHidden = true
            }
           

        }else if self.pageControl.currentPage == self.pageControl.numberOfPages - 1 {
             self.nextButton.isHidden = true
            self.startendButton.isHidden = false
            self.skip.isHidden = true

        }else {
            self.startendButton.isHidden = true
           
                
           if usesrExists
            {
                 self.skip.isHidden = false
            }else
            {
            self.skip.isHidden = true
            }
        }

        if isTakePhoto {
            self.startendButton.setTitle("Take a Photo", for: .normal)
             self.skip.isHidden = true
            self.signInWithAppleButton.isHidden = true

        }else {
             self.startendButton.setTitle("Get Started", for: .normal)
        }
        
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.all)
        
    }
        
}


