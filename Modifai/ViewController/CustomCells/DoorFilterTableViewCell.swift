//
//  DoorFilterTableViewCell.swift
//  Modifai
//
//  Created by IosMac on 13/04/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class DoorFilterTableViewCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    @IBOutlet weak var imgbgView: UIView!
    @IBOutlet weak var detailBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
