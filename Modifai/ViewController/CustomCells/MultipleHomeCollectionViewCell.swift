import UIKit

//*************** change 0.0.40 replace whole swift file ***
class MultipleHomeCollectionViewCell: UICollectionViewCell
{
      @IBOutlet weak var imgView: UIImageView!
      @IBOutlet weak var delBtn: UIButton!

	var projectThumb:ProjectThumb?

	//-----------------------------------------
    required override init(frame: CGRect)
    {
        super.init(frame: frame)
    }

	//-----------------------------------------
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }

	//-----------------------------------------
    override func prepareForReuse()
    {
        super.prepareForReuse()
    }
}
