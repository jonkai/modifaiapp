import UIKit
import Foundation

//------------------------------------------------------------
protocol GarageTableViewCellDelegate: class
{
    func leftGesture()
    func rightGesture()
    func tapGesture()
    func garageButtonActionOnSelection(sender: Int)
    func doorButtonActionOnSelection(sender: Int)

    func btnPaintLastItemClicked(_ sender: Any)  //*************** change 1.1.1 add ***
    func paintActionOnSelection(sender: Any?)   //*************** change 1.1.4 change to Any? optional ***
    func paintFDActionOnSelection(sender: Any?)  //*************** change 1.1.4 change to Any? optional ***

    func garageScrollend(index:Int)
    func paintScrollend(index:Int)
    func doorScrollend(index:Int)
}

//------------------------------------------------------------
class GarageTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource,
                            UICollectionViewDelegateFlowLayout
{
    weak var delegate: GarageTableViewCellDelegate?
//    var projectCho iceList:[ProjectThumb] = [ProjectThumb]()  //*************** change 1.1.4 remove,  now universal "curProjectChoiceList" ***
    

    @IBOutlet weak var pagingView: UICollectionView!
    var selectedItemIndex: Int = 0
    var isGarageSelected:Bool = false   //*************** change 1.0.1 this shouldn't be true on start up? ***
    var isPaintSelected:Bool = false //*************** change 1.0.3 add *** already there in internal app

    var garage = [Garage]()
    var singleGarage = [SingleGarageObj]()
    var doorscount = 0
    var garagecount = 0
    var doors = [Door]()

	var currGallThumb1:ProjectThumb? = nil    //*************** change 1.1.4 add ***
	var currGallThumb2:ProjectThumb? = nil    //*************** change 1.1.4 add ***
	var currGallThumb3:ProjectThumb? = nil    //*************** change 1.1.4 add ***
	var currGallThumb4:ProjectThumb? = nil    //*************** change 1.1.4 add ***
	var currGallThumb5:ProjectThumb? = nil    //*************** change 1.1.4 add ***

	var defaultDoorColor1:UIColor = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.0);    //*************** change 1.1.4 add ***
	var doorStartColor1:UIColor = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.0);    //*************** change 1.1.4 add ***
	var defaultDoorColor2:UIColor = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.0);    //*************** change 1.1.4 add ***
	var doorStartColor2:UIColor = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.0);    //*************** change 1.1.4 add ***
	var defaultDoorColor3:UIColor = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.0);    //*************** change 1.1.4 add ***
	var doorStartColor3:UIColor = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.0);    //*************** change 1.1.4 add ***
	var defaultDoorColor4:UIColor = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.0);    //*************** change 1.1.4 add ***
	var doorStartColor4:UIColor = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.0);    //*************** change 1.1.4 add ***
	var defaultDoorColor5:UIColor = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.0);    //*************** change 1.1.4 add ***
	var doorStartColor5:UIColor = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.0);    //*************** change 1.1.4 add ***


    //------------------------------------------------------------
    override func awakeFromNib()
    {
        super.awakeFromNib()

        pagingView.dataSource = self
        pagingView.delegate = self

        pagingView.showsVerticalScrollIndicator = false
        pagingView.showsHorizontalScrollIndicator = false

       

        let nib1 = UINib(nibName: "CarouselDoorView", bundle: nil)
        pagingView.register(nib1, forCellWithReuseIdentifier: "CarouselDoorView")
        let nib2 = UINib(nibName: "CarouselGarageView", bundle: nil)
        pagingView.register(nib2, forCellWithReuseIdentifier: "CarouselGarageView")
        let nib3 = UINib(nibName: "GarageView", bundle: nil)  //*************** change 1.0.3 add *** already there in internal app
        pagingView.register(nib3, forCellWithReuseIdentifier: "GarageView")

    }

    //------------------------------------------------------------
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

    //------------------------------------------------------------
    @objc func paintButAct(indexPath:IndexPath)
    {
//print("collectionView paintBu tAct indexPath:",indexPath)
        self.delegate?.paintActionOnSelection(sender: nil)   //*************** change 1.1.4 change to nil? optional ***
    }

    //------------------------------------------------------------
    @objc func paintButtonActionOnSelection(sender: Any?)
    {
        self.delegate?.btnPaintLastItemClicked("unused")
    }

    //------------------------------------------------------------
    @objc func paintFDButAct(indexPath:IndexPath)
    {
//print("collectionView paintFDBu tAct indexPath:",indexPath)
        self.delegate?.paintFDActionOnSelection(sender: nil)  //*************** change 1.1.4 change to nil? optional ***
    }

     //------------------------------------------------------------
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
     {
        if self.isGarageSelected
        {
            garagecount = garage.count + singleGarage.count+1
            return garagecount
        } else if self.isPaintSelected{
            return curProjectChoiceList.count + 1  //*************** change 1.1.4 change ***
        }else {
            doorscount = doors.count+1
                       return doorscount
           
        }
    }

    //------------------------------------------------------------
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
//print("collectionView self.isGarageSelected:",self.isGarageSelected)
//print("collectionView self.isPaintSelected:",self.isPaintSelected)
        if (self.isGarageSelected == true)
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarouselGarageView", for: indexPath) as! CarouselGarageView
            cell.contentView.isUserInteractionEnabled = false //*************** change 2.0.8 add

            cell.imgView.isHidden = false
            cell.defaultGarage.isHidden = true
            cell.defaultTitle.text = ""
            cell.chevoronBtn.isHidden = true
            cell.paintButton.isHidden = true;

            cell.chip1Button.isHidden = true; //*************** change 1.1.4 add *** also add changed xib file for both garage and door ***
            cell.chip2Button.isHidden = true; //*************** change 1.1.4 add ***
            cell.chip3Button.isHidden = true; //*************** change 1.1.4 add ***
            cell.chip4Button.isHidden = true; //*************** change 1.1.4 add ***
            cell.chip5Button.isHidden = true; //*************** change 1.1.4 add ***

            let origImage = UIImage(named: "double-chevron")
            let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
            cell.chevoronBtn.setImage(tintedImage, for: .normal)
            cell.chevoronBtn.tintColor = themeColor


   			//*************** change 1.1.4 change whole block ***
            cell.btnTapAction =
            { (sender) in
//print("Edit tapped in cell", inde xPath)
//print("Edit tapped in sender:", sender as Any)
				if sender == cell.chip1Button
				{
//print("Edit tapped cell.chip1Button")
					currGallThumb = self.currGallThumb1;
					scrollEndDoor = true;
					choiceIsOriginal = false;
					shouldSave = false;
            		defaultDoorColor0 = self.defaultDoorColor1;
					doorStartColor0 = self.doorStartColor1;
            		defaultLineColor = defaultDoorColor0;
					origStartColor = doorStartColor0
				} else if sender == cell.chip2Button {
//print("Edit tapped cell.chip2Button")
//print("Edit tapped cell.chip2Button")
					currGallThumb = self.currGallThumb2;
					scrollEndDoor = true;
					choiceIsOriginal = false;
					shouldSave = false;
            		defaultDoorColor0 = self.defaultDoorColor2;
					doorStartColor0 = self.doorStartColor2;
            		defaultLineColor = defaultDoorColor0;
					origStartColor = doorStartColor0
				} else if sender == cell.chip3Button {
//print("Edit tapped cell.chip3Button")
					currGallThumb = self.currGallThumb3;
					scrollEndDoor = true;
					choiceIsOriginal = false;
					shouldSave = false;
            		defaultDoorColor0 = self.defaultDoorColor3;
					doorStartColor0 = self.doorStartColor3;
            		defaultLineColor = defaultDoorColor0;
					origStartColor = doorStartColor0
				} else if sender == cell.chip4Button {
//print("Edit tapped cell.chip3Button")
					currGallThumb = self.currGallThumb4;
					scrollEndDoor = true;
					choiceIsOriginal = false;
					shouldSave = false;
            		defaultDoorColor0 = self.defaultDoorColor4;
					doorStartColor0 = self.doorStartColor4;
            		defaultLineColor = defaultDoorColor0;
					origStartColor = doorStartColor0
				} else if sender == cell.chip5Button {
//print("Edit tapped cell.chip3Button")
					currGallThumb = self.currGallThumb5;
					scrollEndDoor = true;
					choiceIsOriginal = false;
					shouldSave = false;
            		defaultDoorColor0 = self.defaultDoorColor5;
					doorStartColor0 = self.doorStartColor5;
            		defaultLineColor = defaultDoorColor0;
					origStartColor = doorStartColor0
				}

                self.paintButAct(indexPath:indexPath)
            }

            if kIphone_6 && hasOnlyGarageDoors //************** add change 2.1.1
                       {
                           cell.topConst.constant = 20
                           cell.bottomConst.constant = 20
                           cell.leadingConst.constant = 20
                           cell.trailConst.constant = 20
                       }
            if indexPath.row == 0
            {
                if garagecount > 1
                {
                    cell.chevoronBtn.isHidden = false
                }

                cell.imgView.isHidden = true
                cell.defaultGarage.isHidden = false
                cell.delegate = self
                cell.defaultGarage.image = UIImage(named: "defaultDoor")
                cell.selectedButton.tag = -1
                cell.titleLabel.text = ""
                cell.defaultTitle.text = "Swipe to update your home"
                cell.subTitleLabel.text = ""
                cell.descriptionLabel.text = ""
                cell.paintButton.isHidden = true;

            } else if indexPath.row-1 < garage.count {
                    let garageData = garage[indexPath.row-1]
                    cell.delegate = self

  					//*************** change 1.1.4 change if block ***
					if (hasOnlyGarageDoors)
                    {
                        if (garageData.isPaintable == 1)
                        {
                            cell.paintButton.isHidden = false;

//print("1collectionView  indexP ath.row",indexP ath.row)
//print("1collectionView curProjectChoiceList:",curProjectChoiceList as Any)

							var firstChoice:Project? = nil
							var secndChoice:Project? = nil
							var thirdChoice:Project? = nil
							var forthChoice:Project? = nil
							var fifthChoice:Project? = nil


							var numthischoice = 0
							for choice in curProjectChoiceList.reversed()
							{
	//print("3collectionView choice.gal lIndex,indexP ath.row:",choice.gallIndex,indexP ath.row)
								if (choice.gallIndex == indexPath.row)
								{
									numthischoice += 1;
									if let proj = changeToPaintChoice(choice)
									{
										if (numthischoice == 1) {firstChoice = proj;  self.currGallThumb1 = currGallThumb;}
										if (numthischoice == 2) {secndChoice = proj;  self.currGallThumb2 = currGallThumb;}
										if (numthischoice == 3) {thirdChoice = proj;  self.currGallThumb3 = currGallThumb;}
										if (numthischoice == 4) {forthChoice = proj;  self.currGallThumb4 = currGallThumb;}
										if (numthischoice == 5) {fifthChoice = proj;  self.currGallThumb5 = currGallThumb;}
									}
								}
							}


							if firstChoice != nil
							{
								self.defaultDoorColor1 = UIColor.color(data:firstChoice?.defaultLineGaraged)
								self.doorStartColor1 = UIColor.color(data:firstChoice?.origStartGaraged)
								cell.chip1Button.backgroundColor = self.defaultDoorColor1
								cell.chip1Button.isHidden = false;
							}

							if secndChoice != nil
							{
								self.defaultDoorColor2 = UIColor.color(data:secndChoice?.defaultLineGaraged)
								self.doorStartColor2 = UIColor.color(data:secndChoice?.origStartGaraged)
								cell.chip2Button.backgroundColor = self.defaultDoorColor2
								cell.chip2Button.isHidden = false;
							}

							if thirdChoice != nil
							{
								self.defaultDoorColor3 = UIColor.color(data:thirdChoice?.defaultLineGaraged)
								self.doorStartColor3 = UIColor.color(data:thirdChoice?.origStartGaraged)
								cell.chip3Button.backgroundColor = self.defaultDoorColor3
								cell.chip3Button.isHidden = false;
							}

							if forthChoice != nil
							{
								self.defaultDoorColor4 = UIColor.color(data:forthChoice?.defaultLineGaraged)
								self.doorStartColor4 = UIColor.color(data:forthChoice?.origStartGaraged)
								cell.chip4Button.backgroundColor = self.defaultDoorColor4
								cell.chip4Button.isHidden = false;
							}

							if fifthChoice != nil
							{
								self.defaultDoorColor5 = UIColor.color(data:fifthChoice?.defaultLineGaraged)
								self.doorStartColor5 = UIColor.color(data:fifthChoice?.origStartGaraged)
								cell.chip5Button.backgroundColor = self.defaultDoorColor5
								cell.chip5Button.isHidden = false;
							}
						}
                    }

                    cell.imgView.sd_setImage(with: URL(string: garageData.image), placeholderImage: UIImage(named:"loadingTemp"))

                    cell.selectedButton.tag = indexPath.row-1
                    cell.titleLabel.text = garageData.title
                    cell.titleLabel.textColor = UIColor.black
                    cell.subTitleLabel.text = ""
                    cell.descriptionLabel.text = garageData.desc
       //             cell.paintBut ton.frame.origin.x = cell.descriptionLabel.frame.origin.x + 82.0; //*************** change 1.1.4 remove ***
            } else {

                    let garageData = singleGarage[indexPath.row-1-garage.count]
                    cell.delegate = self
                    cell.selectedButton.tag = indexPath.row-1
                    cell.titleLabel.text = garageData.title
                    cell.subTitleLabel.text = ""
                    cell.descriptionLabel.text = garageData.desc
       //             cell.paintBut ton.frame.origin.x = cell.descriptionLabel.frame.origin.x + 82.0; //*************** change 1.1.4 remove ***

  					//*************** change 1.1.4 change if block ***
                    if (hasOnlyGarageDoors)
                    {
                        if (garageData.isPaintable == 1)
                        {
                            cell.paintButton.isHidden = false;

//print("collectionView  indexPa th.row",indexPa th.row)

							var firstChoice:Project? = nil
							var secndChoice:Project? = nil
							var thirdChoice:Project? = nil
							var forthChoice:Project? = nil
							var fifthChoice:Project? = nil


							var numthischoice = 0
							for choice in curProjectChoiceList.reversed()
							{
	//print("3collectionView choice.gal lIndex,indexP ath.row:",choice.gallIndex,inde xPath.row)
								if (choice.gallIndex == indexPath.row)
								{
									numthischoice += 1;
									if let proj = changeToPaintChoice(choice)
									{
										if (numthischoice == 1) {firstChoice = proj;  self.currGallThumb1 = currGallThumb;}
										if (numthischoice == 2) {secndChoice = proj;  self.currGallThumb2 = currGallThumb;}
										if (numthischoice == 3) {thirdChoice = proj;  self.currGallThumb3 = currGallThumb;}
										if (numthischoice == 4) {forthChoice = proj;  self.currGallThumb4 = currGallThumb;}
										if (numthischoice == 5) {fifthChoice = proj;  self.currGallThumb5 = currGallThumb;}
									}
								}
							}


							if firstChoice != nil
							{
								self.defaultDoorColor1 = UIColor.color(data:firstChoice?.defaultLineGaraged)
								self.doorStartColor1 = UIColor.color(data:firstChoice?.origStartGaraged)
								cell.chip1Button.backgroundColor = self.defaultDoorColor1
								cell.chip1Button.isHidden = false;
							}

							if secndChoice != nil
							{
								self.defaultDoorColor2 = UIColor.color(data:secndChoice?.defaultLineGaraged)
								self.doorStartColor2 = UIColor.color(data:secndChoice?.origStartGaraged)
								cell.chip2Button.backgroundColor = self.defaultDoorColor2
								cell.chip2Button.isHidden = false;
							}

							if thirdChoice != nil
							{
								self.defaultDoorColor3 = UIColor.color(data:thirdChoice?.defaultLineGaraged)
								self.doorStartColor3 = UIColor.color(data:thirdChoice?.origStartGaraged)
								cell.chip3Button.backgroundColor = self.defaultDoorColor3
								cell.chip3Button.isHidden = false;
							}

							if forthChoice != nil
							{
								self.defaultDoorColor4 = UIColor.color(data:forthChoice?.defaultLineGaraged)
								self.doorStartColor4 = UIColor.color(data:forthChoice?.origStartGaraged)
								cell.chip4Button.backgroundColor = self.defaultDoorColor4
								cell.chip4Button.isHidden = false;
							}

							if fifthChoice != nil
							{
								self.defaultDoorColor5 = UIColor.color(data:fifthChoice?.defaultLineGaraged)
								self.doorStartColor5 = UIColor.color(data:fifthChoice?.origStartGaraged)
								cell.chip5Button.backgroundColor = self.defaultDoorColor5
								cell.chip5Button.isHidden = false;
							}
                        }
                    }

                    cell.imgView.sd_setImage(with: URL(string: garageData.image), placeholderImage: UIImage(named: "loadingTemp"))
            }

            cell.layoutIfNeeded()
            return cell

        } else if self.isPaintSelected {
            if indexPath.row == 0 || indexPath.row == curProjectChoiceList.count //*************** change 1.1.4 change ***
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarouselGarageView", for: indexPath) as! CarouselGarageView
                cell.contentView.isUserInteractionEnabled = false //*************** change 2.1.0 add
                cell.imgView.isHidden = false
                cell.defaultGarage.isHidden = true
                cell.defaultTitle.text = ""
                cell.chevoronBtn.isHidden = true
                cell.paintButton.isHidden = true;

                if curProjectChoiceList.count > 1 && indexPath.row == 0 //*************** change 1.1.4 change ***
                {
                    cell.chevoronBtn.isHidden = false
                }
                
                cell.imgView.isHidden = true
                cell.defaultGarage.isHidden = false
                cell.delegate = self
                cell.selectedButton.tag = -1
                cell.titleLabel.text = ""
                cell.defaultTitle.text = "Swipe to update your home"
                cell.defaultGarage.image = UIImage(named: "defaultDoor")
                
                if indexPath.row == curProjectChoiceList.count  //*************** change 1.1.4 change ***
                {
                    cell.selectedButton.tag = -100
                    cell.defaultTitle.text = "Tap here to use the advanced paint tools"
                    cell.defaultGarage.image = UIImage(named: "paintCard")
                    let colorImg = UIColor(red: 35.0/255, green: 41.0/255, blue: 77.0/255, alpha: 1.0)
                    cell.defaultGarage.setImageColor(color:colorImg)
                    cell.contentView.isUserInteractionEnabled = true //*************** change 2.1.0
                }
                cell.selectedButton.isHidden = true

                cell.subTitleLabel.text = ""
                cell.descriptionLabel.text = ""
                cell.paintButton.isHidden = true;
                cell.layoutIfNeeded()
                
                return cell
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GarageView", for: indexPath) as! GarageView
            cell.contentView.isUserInteractionEnabled = false  //*************** change 2.1.0 add
            cell.color1View.isHidden = false
            cell.color2View.isHidden = true
            cell.color3View.isHidden = true
            cell.color1Lbl.isHidden = true
            cell.paintButton.isHidden = false

            let proj = getMaskColors(index:indexPath.row)

//print("  ")
//print("garageTable proj",proj as Any)
//print("proj?.mask0:",proj?.mask0 as Any)
//print("proj?.mask1:",proj?.mask1 as Any)
//print("proj?.mask2:",proj?.mask2 as Any)

            if proj?.mask0 != nil
            {
                cell.imgView.setImageColor(color:UIColor.color(data:proj?.defaultLineColor0d))
            }

            if proj?.mask1 != nil
            {
                cell.imgView1?.setImageColor(color:UIColor.color(data:proj?.defaultLineColor1d))
               cell.color2View.isHidden = false
                cell.color3View.isHidden = true
                cell.color1Lbl.isHidden = false

            } else {
                
                cell.color2View.isHidden = true
                cell.color3View.isHidden = true
            }

            if proj?.mask2 != nil
            {
                cell.imgView2?.setImageColor(color:UIColor.color(data:proj?.defaultLineColor2d))
                           cell.color3View.isHidden = false
                cell.color1Lbl.isHidden = false

            } else {
                           cell.color3View.isHidden = true
            }

//....... check to see if this is in correct place ...  need to find or change to new button ...
            cell.paintButton.addTarget(self, action: #selector(paintButtonActionOnSelection(sender:)), for: .touchUpInside)
            cell.titleLabel.text = ""
            cell.titleLabel.textColor = UIColor.black
            cell.layoutIfNeeded()
            return cell
            
        } else {
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarouselDoorView", for: indexPath) as! CarouselDoorView
            cell.contentView.isUserInteractionEnabled = false //*************** change 2.0.8 add

            cell.doorImageView.isHidden = false
            cell.defalutDoor.isHidden = true
            cell.defaultLabel.text = ""
            cell.chevoronBtn.isHidden = true
            cell.paintButton.isHidden = true;

            cell.chip1Button.isHidden = true; //*************** change 1.1.4 add ***  also replace xib file with one provided with chips on it.
            cell.chip2Button.isHidden = true; //*************** change 1.1.4 add ***
            cell.chip3Button.isHidden = true; //*************** change 1.1.4 add ***
            cell.chip4Button.isHidden = true; //*************** change 1.1.4 add ***
            cell.chip5Button.isHidden = true; //*************** change 1.1.4 add ***

   			//*************** change 1.1.4 change whole door block ***
            cell.doorTapAction =
            { (sender) in
//print("Edit tapped in cell", indexPath)
//print("Edit tapped in sender:", sender as Any)
				if sender == cell.chip1Button
				{
//print("Edit tapped cell.chip1Button")
					currGallThumb = self.currGallThumb1;
					scrollEndDoor = true;
					choiceIsOriginal = false;
					shouldSave = false;
            		defaultFrontDrColor0 = self.defaultDoorColor1;
					frontDrStartColor0 = self.doorStartColor1;
            		defaultLineColor = defaultDoorColor0;
					origStartColor = doorStartColor0
				} else if sender == cell.chip2Button {
//print("Edit tapped cell.chip2Button")
//print("Edit tapped cell.chip2Button")
					currGallThumb = self.currGallThumb2;
					scrollEndDoor = true;
					choiceIsOriginal = false;
					shouldSave = false;
            		defaultFrontDrColor0 = self.defaultDoorColor2;
					frontDrStartColor0 = self.doorStartColor2;
            		defaultLineColor = defaultDoorColor0;
					origStartColor = doorStartColor0
				} else if sender == cell.chip3Button {
//print("Edit tapped cell.chip3Button")
					currGallThumb = self.currGallThumb3;
					scrollEndDoor = true;
					choiceIsOriginal = false;
					shouldSave = false;
            		defaultFrontDrColor0 = self.defaultDoorColor3;
					frontDrStartColor0 = self.doorStartColor3;
            		defaultLineColor = defaultDoorColor0;
					origStartColor = doorStartColor0
				} else if sender == cell.chip4Button {
//print("Edit tapped cell.chip3Button")
					currGallThumb = self.currGallThumb4;
					scrollEndDoor = true;
					choiceIsOriginal = false;
					shouldSave = false;
            		defaultFrontDrColor0 = self.defaultDoorColor4;
					frontDrStartColor0 = self.doorStartColor4;
            		defaultLineColor = defaultDoorColor0;
					origStartColor = doorStartColor0
				} else if sender == cell.chip5Button {
//print("Edit tapped cell.chip3Button")
					currGallThumb = self.currGallThumb5;
					scrollEndDoor = true;
					choiceIsOriginal = false;
					shouldSave = false;
            		defaultFrontDrColor0 = self.defaultDoorColor5;
					frontDrStartColor0 = self.doorStartColor5;
            		defaultLineColor = defaultDoorColor0;
					origStartColor = doorStartColor0
				}

                self.paintFDButAct(indexPath:indexPath)
            }

            if indexPath.row == 0
            {
                if doorscount > 1
                {
                    cell.chevoronBtn.isHidden = false
                }
                cell.doorImageView.isHidden = true
                cell.defalutDoor.isHidden = false
                cell.delegate = self
                cell.selectedButton.tag = -1
                cell.titleLabel.text = ""
                cell.defaultLabel.text = "Swipe to update your home"
                cell.subTitleLabel.text = ""
                cell.descriptionLabel.text = ""
                cell.paintButton.isHidden = true;

            } else {
                cell.doorImageView.isHidden = false
                let doorData = doors[indexPath.row-1]
                cell.delegate = self

				//*************** change 1.1.4 change if block ***
                if (hasOnlyGarageDoors)
                {
                    if (doorData.isPaintable == 1)
                    {
						cell.paintButton.isHidden = false;

//print("collectionView  indexPath.row",indexPath.row)

						var firstChoice:Project? = nil
						var secndChoice:Project? = nil
						var thirdChoice:Project? = nil
						var forthChoice:Project? = nil
						var fifthChoice:Project? = nil


						var numthischoice = 0
						for choice in curProjectChoiceList.reversed()
						{
//print("3collectionView choice.gal lIndex,indexPath.row:",choice.gallIndex,indexPath.row)
							if (choice.gallIndex == indexPath.row)
							{
								numthischoice += 1;
								if let proj = changeToPaintChoice(choice)
								{
									if (numthischoice == 1) {firstChoice = proj;  self.currGallThumb1 = currGallThumb;}
									if (numthischoice == 2) {secndChoice = proj;  self.currGallThumb2 = currGallThumb;}
									if (numthischoice == 3) {thirdChoice = proj;  self.currGallThumb3 = currGallThumb;}
									if (numthischoice == 4) {forthChoice = proj;  self.currGallThumb4 = currGallThumb;}
									if (numthischoice == 5) {fifthChoice = proj;  self.currGallThumb5 = currGallThumb;}
								}
							}
						}


//            let proj = getMa skColors(index:indexPath.row)
//print("collectionView  currProject",currProject as Any)
//print("collectionView  currProject?.garageMask",currProject?.garageMask as Any)
//print("collectionView  currProject?.defaultLineGaraged",currProject?.defaultLineGaraged as Any)

						if firstChoice != nil
						{
							self.defaultDoorColor1 = UIColor.color(data:firstChoice?.defaultLineGaraged)
							self.doorStartColor1 = UIColor.color(data:firstChoice?.origStartGaraged)
							cell.chip1Button.backgroundColor = self.defaultDoorColor1
							cell.chip1Button.isHidden = false;
						}

						if secndChoice != nil
						{
							self.defaultDoorColor2 = UIColor.color(data:secndChoice?.defaultLineGaraged)
							self.doorStartColor2 = UIColor.color(data:secndChoice?.origStartGaraged)
							cell.chip2Button.backgroundColor = self.defaultDoorColor2
							cell.chip2Button.isHidden = false;
						}

						if thirdChoice != nil
						{
							self.defaultDoorColor3 = UIColor.color(data:thirdChoice?.defaultLineGaraged)
							self.doorStartColor3 = UIColor.color(data:thirdChoice?.origStartGaraged)
							cell.chip3Button.backgroundColor = self.defaultDoorColor3
							cell.chip3Button.isHidden = false;
						}

						if forthChoice != nil
						{
							self.defaultDoorColor4 = UIColor.color(data:forthChoice?.defaultLineGaraged)
							self.doorStartColor4 = UIColor.color(data:forthChoice?.origStartGaraged)
							cell.chip4Button.backgroundColor = self.defaultDoorColor4
							cell.chip4Button.isHidden = false;
						}

						if fifthChoice != nil
						{
							self.defaultDoorColor5 = UIColor.color(data:fifthChoice?.defaultLineGaraged)
							self.doorStartColor5 = UIColor.color(data:fifthChoice?.origStartGaraged)
							cell.chip5Button.backgroundColor = self.defaultDoorColor5
							cell.chip5Button.isHidden = false;
						}
					}
                }

                cell.doorImageView.sd_setImage(with: URL(string: doorData.image), placeholderImage: UIImage(named: "loadingTemp"))
                cell.selectedButton.tag = indexPath.row-1
                cell.titleLabel.text = doorData.title
                cell.subTitleLabel.text = ""
                cell.descriptionLabel.text = doorData.desc
     //           cell.paintBut ton.frame.origin.x = cell.descriptionLabel.frame.origin.x + 42.0; //*************** change 1.1.4 remove ***
           }

            cell.layoutIfNeeded()
            return cell
        }
        
    }

    //------------------------------------------------------------
    func collectionView(_ collectionView: UICollectionView,
                    layout collectionViewLayout: UICollectionViewLayout,
                    sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let kWhateverHeightYouWant = self.frame.size.height
        return CGSize(width: self.frame.size.width , height: CGFloat(kWhateverHeightYouWant))
    }

    //------------------------------------------------------------
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
//print("collectionView  didSelectItemAt indexPath",indexPath)
        if self.isGarageSelected
        {
          //  self.delegate?.garageButtonActionOnSelection(sender: indexPath.row-1) //*************** change 2.0.8
        } else if self.isPaintSelected {
            if indexPath.row == curProjectChoiceList.count && indexPath.row > 0 //*************** change 1.1.4 change,  now universal "curProjectChoiceList" ***
			{
               self.delegate?.btnPaintLastItemClicked("unused")
            }
        } else {
           // self.delegate?.doorButtonActionOnSelection(sender: indexPath.row-1) //*************** change 2.0.8
        }
    }

	//*************** change 1.1.4 replace whole function ***
    //------------------------------------------------------------
    func getMaskColors(index:Int) -> Project?
    {
    	if (index < curProjectChoiceList.count)
		{
			let projectThumb = curProjectChoiceList[index]  //*************** change 1.1.4 change,  now universal "curProjectChoiceList" ***

			if let projectId = projectThumb.projectID
			{
				if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
				{
					let fileURL = dir.appendingPathComponent("projectChoices_"+projectId+".txt")
					do
					{
						let rawdata = try Data(contentsOf: fileURL)
						NSKeyedUnarchiver.setClass(Project.self, forClassName: "Project")
						if let project0 = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? Project
						{
							return project0
						}
					} catch {
  print(" ---------------------- changeToPa intChoice Couldn't read project -----------------------------")
						 return nil
					}
				}
			}
		}

		return nil
	}

	//*************** change 1.1.4 add function ***
    //------------------------------------------------------------------------------------
    func changeToPaintChoice( _ projectThumb0:ProjectThumb?) -> Project?
    {
//print(" changeToPai ntChoice");
        if let projectThumb = projectThumb0
        {
            currGallThumb = projectThumb;
//print(" changeToPai ntChoice currGallThumb:",currGallThumb as Any);

            if let projectId = projectThumb.projectID
            {
                currGallID = projectId;
//print(" changeToPai ntChoice currGallID:",currGallID as Any);
                if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                {
                    let fileURL = dir.appendingPathComponent("projectChoices_"+projectId+".txt")
                    do
                    {
                        let rawdata = try Data(contentsOf: fileURL)
                        NSKeyedUnarchiver.setClass(Project.self, forClassName: "Project")

                        if let project0 = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? Project
                        {
							return project0
                        }
                    } catch {
  print(" ---------------------- changeToPa intChoice Couldn't read project -----------------------------")
						 return nil
                    }
                }
            }
        }

		return nil
    }
}


//------------------------------------------------------------
extension GarageTableViewCell:UIScrollViewDelegate,
                                GaragenewViewSelectionProtocol,DoorViewSelectionProtocol
{
    //------------------------------------------------------------
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        var visibleRect = CGRect()
        var pageIndex = 0
        visibleRect.origin = pagingView.contentOffset
        visibleRect.size = pagingView.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath = pagingView.indexPathForItem(at: visiblePoint) else { return }
         pageIndex = indexPath.row
//print("scrollViewDidEndDecelerating indexPath",indexPath)
//print("pageIndex",pageIndex)
                if self.isGarageSelected
                {
                    self.delegate?.garageScrollend(index:Int(pageIndex))
                }else if self.isPaintSelected
                               {
                                   self.delegate?.paintScrollend(index:Int(pageIndex))
                               }else {
                    self.delegate?.doorScrollend(index:Int(pageIndex))
                }
    }

    
    //------------------------------------------------------------
    func garageButtonActionOnSelection(sender: UIButton)
    {
//print("garagetable garageButtonAct ionOnSelection  sender:",sender)
        if self.isGarageSelected
        {
            self.delegate?.garageButtonActionOnSelection(sender: sender.tag)
        } else {
            self.delegate?.doorButtonActionOnSelection(sender: sender.tag)
        }
    }

    //------------------------------------------------------------
    func doorButtonActionOnSelection(sender: UIButton)
    {
//print("garagetable doorButtonActi onOnSelection  sender:",sender)
        if self.isGarageSelected
        {
            self.delegate?.garageButtonActionOnSelection(sender: sender.tag)
        } else {
            self.delegate?.doorButtonActionOnSelection(sender: sender.tag)
        }
    }
}


extension UIImageView {
  func setImageColor(color: UIColor) {
    let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
    self.image = templateImage
    self.tintColor = color
  }
}


