import UIKit

class GarageFilterTableViewCell: UITableViewCell
{
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var imgbgView: UIView!
    @IBOutlet weak var detailBtn: UIButton!
    @IBOutlet weak var cardPaintBtn: UIButton! //*************** change 2.0.7 add
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    @IBOutlet weak var cardPaintBtnWidth: NSLayoutConstraint! //*************** change 2.0.7 add

    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
}
