import UIKit

//var widthPaintButFD:CGFloat = 1.0 //*************** change 0.0.30 add ***

//--------------------------------------------
protocol DoorViewSelectionProtocol
{
    func doorButtonActionOnSelection(sender: UIButton)
}

//--------------------------------------------
class CarouselDoorView: UICollectionViewCell
{
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    @IBOutlet weak var trailConst: NSLayoutConstraint!
    @IBOutlet weak var topConst: NSLayoutConstraint!
    @IBOutlet weak var leadingConst: NSLayoutConstraint!
    @IBOutlet weak var chevoronBtn: UIButton!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    @IBOutlet weak var selectedButton: UIButton!
    @IBOutlet weak var defalutDoor: UIImageView!
    @IBOutlet weak var defaultLabel: UILabel!

    @IBOutlet weak var doorImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var paintButton: UIButton! // adding Constraints Lakshmi 2.0.5

    @IBOutlet weak var chip1Button: UIButton! //*************** change 1.1.4 add ***
    @IBOutlet weak var chip2Button: UIButton! //*************** change 1.1.4 add ***
    @IBOutlet weak var chip3Button: UIButton! //*************** change 1.1.4 add ***
    @IBOutlet weak var chip4Button: UIButton! //*************** change 1.1.4 add ***
    @IBOutlet weak var chip5Button: UIButton! //*************** change 1.1.4 add ***

    var delegate: DoorViewSelectionProtocol?

    var doorTapAction : ((_ sender: UIButton)->())?   //*************** change 1.1.4 change ***

    //--------------------------------------------
    required override init(frame: CGRect)
    {
        super.init(frame: frame)
       // self.setupViews()  // remove not using Lakshmi 2.0.5
    }
    
    //--------------------------------------------
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
      //  self.setupViews()  // remove not using Lakshmi 2.0.5
    }
    
    //--------------------------------------------
//    let pai ntButton: UIButton =
//    {
//        let buttonDuplicate = UIButton.init(type:.custom);
//        buttonDuplicate.frame = CGRect(origin: CGPoint(x: 82.0, y: 220), size: CGSize(width: 42.0, height: 42.0)); //*************** change 1.1.0 change ***
//        buttonDuplicate.translatesAutoresizingMaskIntoConstraints = true
//        buttonDuplicate.backgroundColor = .clear
//        buttonDuplicate.setBackgroundImage(UIImage(named: "paintableIcon.png"), for: .normal); //*************** change 1.1.0 change ***
//        return buttonDuplicate
//    }()

    //--------------------------------------------
   // func setupViews() // remove not using Lakshmi 2.0.5
//    {
////        widthPaintButFD = self.frame.size.width
//      //  addSubview(paintBu tton) // remove here Constraints Lakshmi 2.0.5
//        paintBut ton.addTarget(self, action: #selector(btnTapped), for: .touchUpInside)
////        if kIphone_6
////        {
////
////        }
//    }

    //--------------------------------------------
    override func prepareForReuse()
    {
        super.prepareForReuse()
    }
    
        //............ this is not being used ...
    //--------------------------------------------
    @IBAction func GarageSelectionAction(_ sender: UIButton)
    {
        self.delegate?.doorButtonActionOnSelection(sender: sender)
    }

    //--------------------------------------------
    @IBAction func btnTapped(_ sender: UIButton)   //*************** change 1.1.4 change ***
    {
        doorTapAction?(sender)   //*************** change 1.1.4 change ***
    }
}

