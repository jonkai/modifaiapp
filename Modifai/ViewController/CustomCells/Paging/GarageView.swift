import UIKit

//--------------------------------------------
//protocol GarageViewSelectionProtocol
//{
//    func paintButtonActionOnSelection(sender: UIButton)
//}

//--------------------------------------------
class GarageView: UICollectionViewCell
{

	@IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imgView1: UIImageView!
    @IBOutlet weak var imgView2: UIImageView!
    @IBOutlet weak var color1View: UIView!
    @IBOutlet weak var color2View: UIView!
    @IBOutlet weak var color3View: UIView!
    @IBOutlet weak var color1Lbl: UIView!
       @IBOutlet weak var color2Lbl: UIView!
       @IBOutlet weak var color3Lbl: UIView!
    @IBOutlet weak var paintButton: UIButton!

  
    @IBOutlet weak var titleLabel: UILabel!
	//var delegate: GarageViewSelectionProtocol?
   var paintTapAction : (()->())?
	//--------------------------------------------
	required override init(frame: CGRect)
	{
		super.init(frame: frame)

	}

	//--------------------------------------------
	required init?(coder aDecoder: NSCoder)
	{
		super.init(coder: aDecoder)
	}

	//--------------------------------------------
	override func prepareForReuse()
	{
		super.prepareForReuse()
	}

//	//--------------------------------------------
	@IBAction func GarageSelectionAction(_ sender: UIButton)
	{
        paintTapAction?()
//print("GarageView Garage Selection Action")
	//	self.delegate?.paintButtonActionOnSelection(sender: sender)
	}
}
