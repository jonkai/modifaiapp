//
//  GarageView.swift
//  Modifai
//
//  Created by Apple on 16/04/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class FavCollectionCell: UICollectionViewCell {
    @IBOutlet weak var chevoronBtn: UIButton!
    

    @IBOutlet weak var doorImageView: UIImageView!
   
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    required override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    
}
