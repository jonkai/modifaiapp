import Foundation
import UIKit

//var widthPaintButG:CGFloat = 1.0 //*************** change 0.0.30 add ***

//--------------------------------------------
protocol GaragenewViewSelectionProtocol
{
    func garageButtonActionOnSelection(sender: UIButton)
}

//--------------------------------------------
class CarouselGarageView: UICollectionViewCell
{
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    @IBOutlet weak var trailConst: NSLayoutConstraint!
    @IBOutlet weak var topConst: NSLayoutConstraint!
    @IBOutlet weak var leadingConst: NSLayoutConstraint!
    @IBOutlet weak var selectedButton: UIButton!

    @IBOutlet weak var chevoronBtn: UIButton!
    @IBOutlet weak var defaultGarage: UIImageView!
    @IBOutlet weak var defaultTitle: UILabel!

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var paintButton: UIButton! // adding Constraints Lakshmi 2.0.5

    @IBOutlet weak var chip1Button: UIButton! //*************** change 1.1.4 add ***
    @IBOutlet weak var chip2Button: UIButton! //*************** change 1.1.4 add ***
    @IBOutlet weak var chip3Button: UIButton! //*************** change 1.1.4 add ***
    @IBOutlet weak var chip4Button: UIButton! //*************** change 1.1.4 add ***
    @IBOutlet weak var chip5Button: UIButton! //*************** change 1.1.4 add ***

    var delegate: GaragenewViewSelectionProtocol?

    var btnTapAction : ((_ sender: UIButton)->())?   //*************** change 1.1.4 change ***

    //--------------------------------------------
    required override init(frame: CGRect)
    {
        super.init(frame: frame)
      //  self.setupViews()  // remove not using Lakshmi 2.0.5
    }
    
    //--------------------------------------------
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
       // self.setupViews()  // adding Constraints Lakshmi 2.0.5
    }

    //-------------------------------------------- // adding Constraints Lakshmi 2.0.5
//    let paintBut ton: UIButton =
//    {
//        let buttonDuplicate = UIButton.init(type:.custom);
//        buttonDuplicate.frame = CGRect(origin: CGPoint(x: 82.0, y: self.conte), size: CGSize(width: 42.0, height: 42.0));
//        buttonDuplicate.translatesAutoresizingMaskIntoConstraints = true
//        buttonDuplicate.backgroundColor = .clear
//        buttonDuplicate.setBackgroundImage(UIImage(named: "paintableIcon.png"), for: .normal);
//        return buttonDuplicate
//    }()

    //--------------------------------------------
  //  func setupViews() // remove not using Lakshmi 2.0.5
  //  {
//print("garage setupViews")
//        widthPaintButG = self.frame.size.width
       // addSubview(paintBut ton) // adding Constraints Lakshmi 2.0.5

       // paintBut ton.addTarget(self, action: #selector(doorBt nTapped), for: .touchUpInside)
  //  }


    //--------------------------------------------
    override func prepareForReuse()
    {
        super.prepareForReuse()
    }

    //--------------------------------------------
    @IBAction func garageSelectionAction(_ sender: UIButton)
    {
//print("garage Selection Action")
        self.delegate?.garageButtonActionOnSelection(sender: sender)
    }

    //--------------------------------------------
    @IBAction func doorBtnTapped(_ sender: UIButton)  //*************** change 1.1.4 change ***
    {
//print("doorBt nTapped Tapped!  sender:",sender as Any)
        btnTapAction?(sender)  //*************** change 1.1.4 change ***
	}
}

