import Foundation
import UIKit

//--------------------------------------------------
class PaintCollectionCell: UICollectionViewCell
{
	@IBOutlet weak var cellPaintView: UIView!

	@IBOutlet weak var color1View: UIView!
	@IBOutlet weak var color2View: UIView!
	@IBOutlet weak var color3View: UIView!

	@IBOutlet weak var maskView1: UIImageView!
	@IBOutlet weak var maskView2: UIImageView!
	@IBOutlet weak var maskView3: UIImageView!

	@IBOutlet weak var color1Lbl: UILabel!
	@IBOutlet weak var color2Lbl: UILabel!
	@IBOutlet weak var color3Lbl: UILabel!

	@IBOutlet weak var originalPaint: UILabel!   //********** change 3.0.7 add ***

    //--------------------------------------------
    required override init(frame: CGRect)
    {
        super.init(frame: frame)
    }

    //--------------------------------------------
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }

    //--------------------------------------------
    override func prepareForReuse()
    {
        super.prepareForReuse()
    }
}

