//
//  PaintPopUpCell.swift
//  Modifai
//
//  Created by gcsadmin on 01/03/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit
class PaintPopUpCell: UICollectionViewCell {
    

    @IBOutlet weak var imgView: UIImageView!
    // 03/19 start
    @IBOutlet weak var deleteBtn: UIButton!
    // end
  

    required override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    
}
