import Foundation
import UIKit

//--------------------------------------------------
class PaintLandScapeCell: UITableViewCell
{
    @IBOutlet weak var maskView1: UIImageView!
    @IBOutlet weak var maskView2: UIImageView!
    @IBOutlet weak var maskView3: UIImageView!
    @IBOutlet weak var color1View: UIView!
       @IBOutlet weak var color2View: UIView!
       @IBOutlet weak var color3View: UIView!
    @IBOutlet weak var color2Lbl: UIView!
        @IBOutlet weak var color3Lbl: UIView!
    @IBOutlet weak var color1Lbl: UIView!

     @IBOutlet weak var paintButton: UIButton!
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
}

