//
//  GarageView.swift
//  Modifai
//
//  Created by Apple on 16/04/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
protocol GarageViewSelectionProtocol {
    func garageButtonActionOnSelection(sender: UIButton)
}
class GarageView: UICollectionViewCell {
    
    @IBOutlet weak var selectedButton: UIButton!
    @IBOutlet weak var imgView: UIImageView!
     @IBOutlet weak var doorImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    var delegate: GarageViewSelectionProtocol?
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    required override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    @IBAction func GarageSelectionAction(_ sender: UIButton) {
        self.delegate?.garageButtonActionOnSelection(sender: sender)
    }
    
}
