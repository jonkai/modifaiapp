import UIKit
import CoreData
import simd
import CoreMotion
import GLKit
import Alamofire
import CoreData
import Firebase

//*************** change 3.0.5 remove several variables, put in header file ***

var comingFromfeed:Bool = false
var isHomeAgent:Bool = false //********** change 3.0.7 add ***

var originalHomeImage: UIImage!
var houseObject:HouseObject?
var currentImageName:String = "xxx"

let revSite = "https://reviveai.co/"
let revSubSiteLS = ".cssFiles/"
let revNoteFile = "note.php?"

let MinutesMult = 60.0
let HoursMult = 3600.0
let DaysMult = 86400.0

var lastMessageTime:Date! = Date();
var messageTimer = Timer()

let QQ:Float = 0.017453292519943;
let radian45:Double = 0.78539816339745;
let raddeg:Double = 57.29577951308238
let radian90:Double = 1.5707963267949;
let fifteenRad:CGFloat = 0.26179938779915

let Ten_EPSILON:Double =  0.0000001
let Nine_EPSILON:Double =  0.00001
let Eight_EPSILON:Float =  0.001
let One_EPSILON:Float =  0.01

let cropRatio:CGFloat = 0.82;

let minProt:CGFloat = CGFloat(-30.0 * QQ)
let maxProt:CGFloat =  CGFloat(60.0 * QQ)
let numOfTools:CGFloat =  6.0

let minInner:CGFloat = CGFloat(-30.0 * QQ)
let maxInner:CGFloat =  CGFloat(30.0 * QQ)
let numOfControls:CGFloat =  5.0

var pecHome1 = false
var pecHome2 = false
var dontRotate = false
var uploadLater = false
var includeShade = true
var wasAddDoor = false
var editedGarage = false
var editedGarageAdded = false
var editedDoorAdded = false
var fromFavorites = false

var shoppingVal = "-1"
var downloadCount = 0
var totalCount = 0
var orientationLock = UIInterfaceOrientationMask.all

var doorSource  = [UIImage]()
var garDoorSourceDdr   = [UIImage]()
var garDoorSourceSdr   = [UIImage]()
var frntDoorSourceDdr  = [UIImage]()
var frntDoorSourceSdr  = [UIImage]()
var windowSource       = [UIImage]()

var motionManager = CMMotionManager()
var attitude = simd_double3([0.0, 0.0, 0.0])
var gravity  = simd_double3([0.0, 0.0, 0.0])
//var currentAttitude_CM:CMQuaternion = CMQuaternion(x:0.0, y:0.0, z:0.0, w:1.0)
var rotationMatrix:CMRotationMatrix = CMRotationMatrix()
var rotationMatrix0:CMRotationMatrix = CMRotationMatrix()
var roisAfter:[[Int]]!
var roisInnerAfter:[[Int]]!
var roisOuterAfter:[[Int]]!

var pitch = 0.0;
var lensModel:String!
var FOV:Float = 1.0;
var focalLengthInPixels:Float = 1.0;
var mainPlate = [simd_float4]()
var eyeLevelFromTop:Float = 0.0;
var image_ID:String!;
var image_url:String!;
var image_date:String!;
var image_user:String!;
var image_lat:String!;
var image_lng:String!;
var currDoor1Type: Int = 100;
var currDoor2Type: Int = 200;
var imageNames:[String] = ["modiNewGall1.jpg","modiGall2.jpg","modiGall3.jpg","modiGall4.jpg","modiGall5.jpg","modiNewGall7.jpg","modiGall8.jpg","modiGall9.jpg","modiGall11.jpg","modiGall12.jpg"]
var AFManager = SessionManager()
let versVnum:Int = 4;

//************** change 3.0.0 add SherwinObject.swift file  ***
//************** change 3.0.0 add SherwinColorTable.txt file  ***
//************** change 3.0.0 pecFramework was changed replaced  ***

var sherwinColors:[SherwinObject] = [SherwinObject]()  //*************** change 3.0.0 add ***

//------------------------------------------------------------
@UIApplicationMain class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?
	var backgroundTaskID:UIBackgroundTaskIdentifier?
    var orientationLock = UIInterfaceOrientationMask.all

    //----------------------------------------------------------------------
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        AppUtility.lockOrientation(.portrait)
        
        lastMessageTime = Calendar.current.date(byAdding: .day, value: 1, to: lastMessageTime);

        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 20 // seconds
        AFManager = Alamofire.SessionManager(configuration: configuration)

        self.setItAllUp()  //*************** change 3.0.5 add change ***
        
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")

        if launchedBefore
        {
//print("Not first launch.")
            self.showCustomTabbar(isCameraOpen: false)
        } else {
//print("First launch, setting UserDefault.")
            UserDefaults.standard.set(true, forKey: "launchedBefore")
            self.showTutorialScreen(isphotoView: false)
            UserDefaults.standard.set(false, forKey: "showCameraAlert")

            if isHomeApp
            {
            	UserDefaults.standard.set(false, forKey: "showAgentAlert")
            }

            UserDefaults.standard.set(false, forKey: "signUpDone")
        }

		proOnorOff = UserDefaults.standard.bool(forKey: "proOnorOff")
		isSecretTurnOn = UserDefaults.standard.bool(forKey: "isSecretTurnOn")

		//------------------ find the and install current project if available -----------------
		if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
		{
			let fileURL = dir.appendingPathComponent("currProject.txt")

			do
			{
				let rawdata = try Data(contentsOf: fileURL)
                NSKeyedUnarchiver.setClass(Project.self, forClassName: "Project")

				if let project = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? Project
				{
					currProject = project
//print("appDelegate currPro ject: ",currProject as Any);
//print("appDelegate currProject?.projectID:",currProject?.projectID as Any)

					if let imageData = project.imageData
					{
						originalHomeImage = UIImage(data:imageData);
            			targetMaskRect.size = originalHomeImage.size;
					}

					if let name = project.imageName
					{
						currentImageName = name;
					}

//print("appDelegate project.house0: ",project.house0 as Any);
					if let houseData = project.house0
					{
						let decoder = JSONDecoder()
						if let house0Object = try? decoder.decode(HouseObject.self, from: houseData)
						{
							houseObject = house0Object;
//print("appDelegate houseOb ject: ",houseObject as Any);

							for resp in house0Object.response
							{
//print("appDelegate resp: ",resp as Any);
								for respItem in resp
								{
//print("appDelegate respItem: ",respItem as Any);
									respItem.repairLinkedList()
								}
							}
						}
					}
				}
			} catch {
  print(" ---------------------- Couldn't read/load project (probably new install) ---------------------- ")
			}
		}

		self.convertColorTable() //************** change 3.0.0 add ***

		FirebaseApp.configure()

        if UserDefaults.standard.value(forKey: "deviceID") != nil
        {
        } else {
            let udid = UIDevice.current.identifierForVendor!.uuidString
            UserDefaults.standard.set(udid, forKey: "deviceID")
        }

        UINavigationBar.appearance().barTintColor = themeColor
        UINavigationBar.appearance().tintColor = titleColor
        UINavigationBar.appearance().isTranslucent = false

        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        DataManager.updateUserIDforOldImageIDs()

        return true
    }

	//*************** change 3.0.5 add function ***
  	//------------------------------------------------------------
	func setItAllUp()
	{
		if (isPrecision)
		{
			brand = "Precision"
			themeColor = UIColor.init(red: 0.0117647058, green: 0.2039215686, blue: 0.15294117647, alpha: 1.0)
			titleColor = UIColor.white
			revNotePref = "precision"
			singleBrand = false
		} else if (isClopay) {
			brand = "Clopay"
			themeColor = UIColor.init(red: 0.9675490196, green: 0.8420588235, blue: 0.3910784314, alpha: 1.0)
			titleColor = UIColor.black
			revNotePref = "clopay"
			singleBrand = true
		} else if (isAmarr) {
			brand = "Amarr"
			themeColor = UIColor.init(red: 0.34901960784314, green: 0.52156862745098, blue: 0.52549019607843, alpha: 1.0)
			titleColor = UIColor.white
			revNotePref = "amarr"
			singleBrand = true
		} else if (isGarageApp) {
			brand = "Garage"
			themeColor = UIColor.init(red: 0.14117647058824, green: 0.16078431372549, blue: 0.29019607843137, alpha: 1.0)
			titleColor = UIColor.white
			revNotePref = "garage"
			singleBrand = false
		} else if (isHomeApp) {
			brand = "Home"
			themeColor = UIColor.init(red: 0.14117647058824, green: 0.16078431372549, blue: 0.29019607843137, alpha: 1.0)
			titleColor = UIColor.white
			revNotePref = "home"
			isHomeAgent =  true; //********** change 3.0.7 add ***

			if (isHomeDev) //********** change 3.0.9 add if block ***
			{
				brand = "HomeDev" //********** this doesn't have to be different ***
				revNotePref = "homedev"
				isProduction = false
			}
		} else if (isInternalApp) {
			brand = "Internal"
			themeColor = UIColor.init(red: 0.14117647058824, green: 0.16078431372549, blue: 0.29019607843137, alpha: 1.0)
			titleColor = UIColor.white
			revNotePref = "internal"
			isHomeAgent =  true; //********** change 3.0.7 add ***
		} else if (isGarageDev) {
			brand = "Garage"
			themeColor = UIColor.init(red: 0.14117647058824, green: 0.16078431372549, blue: 0.29019607843137, alpha: 1.0)
			titleColor = UIColor.white
			revNotePref = "garage"
			singleBrand = false
			isProduction = false
//		} else if (isHomeDev) { //********** change 3.0.9 remove ***
//			brand = "HomeDev"
//			themeColor = UIColor.init(red: 0.14117647058824, green: 0.16078431372549, blue: 0.29019607843137, alpha: 1.0)
//			titleColor = UIColor.white
//			revNotePref = "homedev"
//			isHomeAgent =  true;
//			isProduction = false
		} else if (isInternalDev) { //********** change 3.0.8 add ***
			brand = "InternalDev"
			themeColor = UIColor.init(red: 0.14117647058824, green: 0.16078431372549, blue: 0.29019607843137, alpha: 1.0)
			titleColor = UIColor.white
			revNotePref = "internaldev"
			isHomeAgent =  true;
			isProduction = false
		}

		if (isProduction)
		{
			baseUrl = "https://modifaiprod-dot-homes-196619.appspot.com"
			applytransBase = "http://ec2-44-238-132-205.us-west-2.compute.amazonaws.com:5083/applyTrans"
			jotFormLink = "https://form.jotform.com/201259212801040"
			paintUrl = URL(string: "http://ec2-44-236-52-217.us-west-2.compute.amazonaws.com:5083/applyTrans")!
		} else {
			baseUrl = "https://homes-196619.appspot.com"
//let applytransBase = "http://35.203.65.160:5083/applyTrans"
			applytransBase = "http://ec2-44-238-155-221.us-west-2.compute.amazonaws.com:5083/applyTrans"
			jotFormLink = "https://form.jotform.com/201016711662040"
			paintUrl = URL(string: "http://ec2-44-236-52-217.us-west-2.compute.amazonaws.com:5083/applyTrans")!
		}
	}

    //---------------------------------------
    struct AppUtility
    {
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask)
        {
            if let delegate = UIApplication.shared.delegate as? AppDelegate
            {
                delegate.orientationLock = orientation
            }
        }
        
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation)
        {
            self.lockOrientation(orientation)
            UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        }
    }

    //---------------------------------------
    func saveValues()
    {
        let bundleIdentifier = Bundle.main.bundleIdentifier
        let bundleName = String(describing:bundleIdentifier)
//        if let download = UserDefaults.standard.value(forKey: "downloadDone")
//        {
//            let data1 = download as! String
//            if data1 == "pending"
//            {
//                //self.getPreviousHouseListAPI()
//            }
//        }

        if let deviceID = UserDefaults.standard.value(forKey: "deviceID")
        {
            let data1 = Data((deviceID as! String).utf8)
            let status1 = KeyChain.save(key:"\(bundleName)newdeviceID", data: data1)
  print("\(bundleName) deviceID keychain status: ", status1)
        }

        if let shopping = UserDefaults.standard.value(forKey: "shopping")
        {
  print("shopping",shopping)
            //        let data1 = Data((shopping as! String).utf8)
            //        let status1 = KeyChain.save(key:"\(bundleName)newshopping", data: data1)
            //        print("\(bundleName)shopping keychain status: ", status1)
        } else {
            UserDefaults.standard.set("-1" , forKey:"shopping")
        }

        if let token = UserDefaults.standard.value(forKey:"accessTokenValue")
        {
            let data2 = Data((token as! String).utf8)
            let status2 = KeyChain.save(key: "\(bundleName)newaccessTokenValue", data: data2)
  print("\(bundleName) accessTokenValue keychain status: ", status2)
        }else
        {
            let uuid = UIDevice.current.identifierForVendor?.uuidString
            let data2 = Data((uuid!).utf8)
            let status2 = KeyChain.save(key: "\(bundleName)newaccessTokenValue", data: data2)
            print("\(bundleName) accessTokenValue keychain status: ", status2)

        }

        if let userID = UserDefaults.standard.value(forKey:"UserID")
        {
            let data3 = Data((userID as! String).utf8)
            let status3 = KeyChain.save(key: "\(bundleName)newUserID", data: data3)
  print("\(bundleName) UserID keychain status: ", status3)
        }

        if let username = UserDefaults.standard.value(forKey:"UserEmail")
        {
            let data4 = Data((username as! String).utf8)
            let status4 = KeyChain.save(key: "\(bundleName)newUserEmail", data: data4)
  print("\(bundleName) UserEmail keychain status: ", status4)
        }

        if let lastImageUrl = UserDefaults.standard.value(forKey:"Last_Image_url")
        {
            let data4 = Data((lastImageUrl as! String).utf8)
            let status4 = KeyChain.save(key: "\(bundleName)newLast_Image_url", data: data4)
  print("\(bundleName) Last_Imag e_url keychain status: ", status4)
        }
    }

	//------------------------------------------
	func CheckForZipCode()
	{
		let bundleIdentifier = Bundle.main.bundleIdentifier
		let bundleName = String(describing:bundleIdentifier)

		if let receivedData1 = KeyChain.load(key: "\(bundleName)agentzipcode")
		{
			let result = String(decoding: receivedData1, as: UTF8.self)
  print(" zipcode result : ", result)

			DataManager.UpdateUserZip(ZipCode: result, completion:
			{ (error,shopping) in
				UserDefaults.standard.set(shopping , forKey:"shopping")

				//*********** change 0.444.7 check before it crashes... was a crash when I was in Camera ***
				/* if let cameraVc = UIApplication.shared.windows.first!.rootViewController as? CameraViewController
				{
				print(" was a cameraVc and would have crashed : ", cameraVc)

				} else {*/

				//*********** change 0.444.7 check before it crashes... possible crash ***
				if let tabBarViewController = UIApplication.shared.windows.first!.rootViewController as? UITabBarController
				{
					if shopping  == "1"
					{
						if (tabBarViewController.viewControllers?.count == 3 && hasInternalApp) || (tabBarViewController.viewControllers?.count == 4 && isHomeApp)
						{
							tabBarViewController.viewControllers?.remove(at: 2)
							let storyBoard: UIStoryboard = UIStoryboard(name: "CustomTabBar", bundle: nil)

							let shopNavVC = storyBoard.instantiateViewController(withIdentifier: "ShoppingNavController") as! UINavigationController
							let infoNavVC = storyBoard.instantiateViewController(withIdentifier: "InfoNavController") as! UINavigationController
							let feedInfoVC = storyBoard.instantiateViewController(withIdentifier: "FeedNavController") as! UINavigationController

							tabBarViewController.viewControllers?.append(shopNavVC)

							if isHomeApp
							{
								tabBarViewController.viewControllers?.append(feedInfoVC)
							}
							tabBarViewController.viewControllers?.append(infoNavVC)
						}

					} else {

						if (tabBarViewController.viewControllers?.count == 4 && hasInternalApp) || (tabBarViewController.viewControllers?.count == 5 && isHomeApp)
						{
							tabBarViewController.viewControllers?.remove(at: 2)
						}
					}
				}
			})
		}
	}

	//---------------------------------------------
	func showTutorialScreen(isphotoView:Bool)
	{
        let storyboard = UIStoryboard(name: "CustomTabBar", bundle: nil)
        let onboradingVC = storyboard.instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
        onboradingVC.isTakePhoto = isphotoView
        self.window?.rootViewController = onboradingVC
        self.window?.makeKeyAndVisible()
    }

    //--------------------------------------------------------
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask
    {
        return self.orientationLock
    }

    //--------------------------------------------------------
    func getPreviousHouseListAPI()
    {
        DataManager.getPreviousHouseList(completion:
        {(error,data) in
            if data != nil
            {
                if data!.count>0
                {
                    totalCount = data!.count
                    for dic  in data!
                    {
                        let tempDic = dic as? [String:Any]
                        if tempDic != nil && tempDic!.count > 0
                        {
                            let imgurl = tempDic!["image_url"]
                            var imgurlName = (imgurl! as! NSString).lastPathComponent
                            let imgarray = imgurlName.components(separatedBy:"--")
                            let tempfilename2 = imgarray[0]
                            imgurlName = imgarray[0] + ".jpg"

                            let fullSizeimageID = (tempDic!["imageID"] as? String)!
                            UserDefaults.standard.set(fullSizeimageID, forKey:String(format:"%@_fullID",tempfilename2))

                            if imageNames.contains(tempfilename2)
                            {
                                let fileurl2:String = (tempDic!["editedrois_file_url"] as? String) ?? ""

                                if !fileurl2.isEmpty{
                                 
                                 var fileurlName2 = (fileurl2 as NSString).lastPathComponent
                                 let filearray2 = fileurlName2.components(separatedBy:"--")
                                 fileurlName2 = filearray2[0] + ".txt"
                                 if !fileurlName2.contains("_houseObject")
                                 {
                                     fileurlName2 = filearray2[0] + "_houseObject" + ".txt"
                                   

                                 }
                                      AppDelegate.downloadUsingAlamofire(url: URL(string: fileurl2)!, fileName: fileurlName2)
                                 }
                                
                                let fileUrl:String = (tempDic!["file_url"] as? String) ?? ""

                                if fileUrl.count > 0
                                {
                                    let fileurlName = tempfilename2+"_doc.txt"
                                    AppDelegate.downloadUsingAlamofire(url: URL(string: fileUrl)!, fileName: fileurlName)
                                } else {
                                }

                                if  tempDic!["perspectiverois_file_url"] != nil
                                {
                                    let fileurl1 = (tempDic!["perspectiverois_file_url"] as? String) ?? ""
                                    if fileurl1.count > 0
                                    {
                                        var fileurlName1 = (fileurl1 as NSString).lastPathComponent
                                        let filearray1 = fileurlName1.components(separatedBy:"--")
                                        fileurlName1 = filearray1[0] + ".txt"
                                        AppDelegate.downloadUsingAlamofire(url: URL(string: fileurl1 )!, fileName: fileurlName1)
                                    }
                                    
                                }

                                AppDelegate.downloadUsingAlamofire(url: URL(string: imgurl! as! String)!, fileName: imgurlName)
                                
							} else {
                                let fileurl:String = (tempDic!["file_url"] as? String) ?? ""
                                if !fileurl.isEmpty{
                                    
                                    var fileurlName = (fileurl as NSString).lastPathComponent
                                    let filearray = fileurlName.components(separatedBy:"--")
                                    fileurlName = filearray[0] + ".txt"
                                    if  tempDic!["perspectiverois_file_url"] != nil
                                    {
                                        let fileurl1 = (tempDic!["perspectiverois_file_url"] as? String) ?? ""
                                        if fileurl1.count > 0
                                        {
                                            var fileurlName1 = (fileurl1 as NSString).lastPathComponent
                                            let filearray1 = fileurlName1.components(separatedBy:"--")
                                            fileurlName1 = filearray1[0] + ".txt"
                                            AppDelegate.downloadUsingAlamofire(url: URL(string: fileurl1 )!, fileName: fileurlName1)
                                        }
                                        
                                    }

                                    let fileurl2:String = (tempDic!["editedrois_file_url"] as? String) ?? ""

                                    if !fileurl2.isEmpty
                                    {
                                     
										 var fileurlName2 = (fileurl2 as NSString).lastPathComponent
										 let filearray2 = fileurlName2.components(separatedBy:"--")
										 fileurlName2 = filearray2[0] + ".txt"

										 if !fileurlName2.contains("_houseObject")
										 {
											 fileurlName2 = filearray2[0] + "_houseObject" + ".txt"

										 }
										AppDelegate.downloadUsingAlamofire(url: URL(string: fileurl2)!, fileName: fileurlName2)
									}
                                    
                                    AppDelegate.downloadUsingAlamofire(url: URL(string: fileurl)!, fileName: fileurlName)
                                    AppDelegate.downloadUsingAlamofire(url: URL(string: imgurl! as! String)!, fileName: imgurlName)
                                }
                            }
                        }
                    }
                }
            }
        })
    }

	//-----------------------------------------
    static func orderDownloaded()
    {
        if let home = homeviewController
        {
            LoadingIndicatorView.hide()
            home.loadDownloadedImage()
            return
        }
        
        LoadingIndicatorView.hide()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let storyboard = UIStoryboard(name: "CustomTabBar", bundle: nil)
        let onboradingVC = storyboard.instantiateViewController(withIdentifier: "CustomTabBarVC") as! CustomTabBarVC
        UserDefaults.standard.set(false, forKey: "isCameraRequire")
        
        appDelegate.window?.rootViewController = onboradingVC
        appDelegate.window?.makeKeyAndVisible()

        if let shopping = UserDefaults.standard.value(forKey: "shopping")
        {
            shoppingVal = shopping as? String ?? "-1"
            let tabBarViewController = UIApplication.shared.windows.first!.rootViewController as!   UITabBarController
            
            if shoppingVal  != "1"
            {
                if tabBarViewController.viewControllers?.count == 5
                {
                    tabBarViewController.viewControllers?.remove(at: 2)
                    if hasInternalApp
                    {
                        tabBarViewController.viewControllers?.remove(at: 2)
                    }
                }
                
            } else {
                if tabBarViewController.viewControllers?.count == 5 && hasInternalApp
                {
                    tabBarViewController.viewControllers?.remove(at: 3)
                }
            }
        }
    }

	//-----------------------------------
    func showCustomTabbar(isCameraOpen:Bool)
    {
        self.saveValues()
        self.CheckForZipCode()
        
        if isHomeApp
        {
//print("showCust omTabbar isHomeApp before checkAp pMode")
            self.checkAppMode()
        } else {
//print("showCust omTabbar  before orderDow nloaded")
            AppDelegate.orderDownloaded()
        }
    }

	//-----------------------------------
    func checkAppMode()
    {
        if let _ = UserDefaults.standard.value(forKey: "AppMode") as? String
        {
            AppDelegate.orderDownloaded()
        } else {
            let popOverVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "AgentPopupViewController") as? AgentPopupViewController
			self.window?.rootViewController = popOverVC
			self.window?.makeKeyAndVisible()
        }
    }

	//--------------------------------------------------------
    func getPreviousHouseListAPI(orderNumber:String)
    {
        DataManager.getAgentHouseList(orderNumber:orderNumber,completion:
        {(error,data) in
            if data != nil
            {
                if data!.count>0
                {
                        let tempDic = data as? [String:Any]
                        if tempDic != nil && tempDic!.count > 0
                        {
                            
                            let imgurl = tempDic!["image_url"]
                            var imgurlName = (imgurl! as! NSString).lastPathComponent
                            let imgarray = imgurlName.components(separatedBy:"--")
                            let tempfilename2 = imgarray[0]
                            imgurlName = imgarray[0] + ".jpg"
                            if imageNames.contains(tempfilename2)
                            {
                                var fileUrl = ""
                                if  tempDic!["editedrois_file_url"] != nil
                                {
                                    fileUrl = (tempDic!["editedrois_file_url"] as? String) ?? ""
                                    
                                }
                                if fileUrl.count > 0
                                {
                                    
                                    let fileurlName = tempfilename2+"_doc.txt"
                                    AppDelegate.downloadUsingAlamofire(url: URL(string: fileUrl )!, fileName: fileurlName)
                                } else {
                                }
                                if  tempDic!["perspectiverois_file_url"] != nil
                                {
                                    let fileurl1 = (tempDic!["perspectiverois_file_url"] as? String) ?? ""
                                    if fileurl1.count > 0
                                    {
                                        var fileurlName1 = (fileurl1 as NSString).lastPathComponent
                                        let filearray1 = fileurlName1.components(separatedBy:"--")
                                        fileurlName1 = filearray1[0] + ".txt"
                                        AppDelegate.downloadUsingAlamofire(url: URL(string: fileurl1 )!, fileName: fileurlName1)
                                    }
                                    
                                }
                                AppDelegate.downloadUsingAlamofire(url: URL(string: imgurl! as! String)!, fileName: imgurlName)
                                
                                
                                
                            } else {
                                var fileurl:String = (tempDic!["file_url"] as? String) ?? ""
                                if !fileurl.isEmpty{
                                    
                                    var fileurlName = (fileurl as NSString).lastPathComponent
                                    let filearray = fileurlName.components(separatedBy:"--")
                                    fileurlName = filearray[0] + ".txt"
                                    if  tempDic!["perspectiverois_file_url"] != nil
                                    {
                                        let fileurl1 = (tempDic!["perspectiverois_file_url"] as? String) ?? ""
                                        if fileurl1.count > 0
                                        {
                                            var fileurlName1 = (fileurl1 as NSString).lastPathComponent
                                            let filearray1 = fileurlName1.components(separatedBy:"--")
                                            fileurlName1 = filearray1[0] + ".txt"
                                            AppDelegate.downloadUsingAlamofire(url: URL(string: fileurl1 )!, fileName: fileurlName1)
                                        }
                                        
                                    }
                                    if  tempDic!["editedrois_file_url"] != nil
                                    {
                                        let fileurl1 = (tempDic!["editedrois_file_url"] as? String) ?? ""
                                        if fileurl1.count > 0
                                        {
                                            fileurl = fileurl1
                                            
                                        }
                                    }
                                    if  tempDic!["editedrois_file_url"] != nil
                                    {
                                        let fileurl1 = (tempDic!["editedrois_file_url"] as? String) ?? ""
                                        if fileurl1.count > 0
                                        {
                                            fileurl = fileurl1
                                            
                                        }
                                    }
                                    
                                    AppDelegate.downloadUsingAlamofire(url: URL(string: fileurl)!, fileName: fileurlName)
                                    
                                    
                                    AppDelegate.downloadUsingAlamofire(url: URL(string: imgurl! as! String)!, fileName: imgurlName)
                                }
                            }
                        
                            
                        }
                }
            }
        })
    }


    // MARK: -------------- Load URL --------------
	//--------------------------------------------------------
    static func downloadFromResourceToDocumentry(fileName:String,Ext:String)
    {
        var bundlePath = Bundle.main.path(forResource: fileName, ofType: Ext)
        
        if Ext == ".jpg"
        {
            bundlePath = Bundle.main.path(forResource: fileName, ofType: ".png")
            
        }
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        
        let fileName1 = "\(fileName).\(Ext)"
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent(fileName1)
        let fullDestPathString = fullDestPath!.path
        if fileManager.fileExists(atPath: fullDestPathString)
        {
//print("File is available")
        } else {
            do
            {
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
                downloadCount = downloadCount + 1
                
            } catch {
  print("fileManager.copyItem error",error)
            }
        }
    }
    
	//--------------------------------------------------------
    static func downloadUsingAlamofire(url: URL, fileName: String)
    {
        
        //      let manager = Alamofire.SessionManager.default
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            documentsURL.appendPathComponent(fileName)
            return (documentsURL, [.removePreviousFile])
        }

        Alamofire.download(url, to: destination).response
        { response in

            if (response.destinationURL?.path) != nil
            {
               
            }
        }
    }

	//--------------------------------------------------------
    func applicationWillResignActive(_ application: UIApplication)
    {
        self.stopUpdates()
        self.removeObservers();
    }
    
	//--------------------------------------------------------
    func applicationDidEnterBackground(_ application: UIApplication)
    {
        self.stopUpdates()
        self.removeObservers();
    }
    
	//--------------------------------------------------------
    func applicationWillEnterForeground(_ application: UIApplication)
    {
    }
    
	//--------------------------------------------------------
    func applicationDidBecomeActive(_ application: UIApplication)
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0)
        {
            self.startUpdates()
        }
        
        messageTimer = Timer.scheduledTimer(timeInterval: 22.0,
            target:self,
            selector: #selector(AppDelegate.checkForMessage),
            userInfo: nil, repeats: false);
        self.checkAppUpdate()
        
    }

	//************** change 3.0.0 add this function which was missed in an update for Garage ***
    //************ change 1.2.0 add function ***
    //************ change 1.2.0 add entitlement, look in "Signing & Capabilities" of app
    //                         and add "applinks:reviveai.co/homeapplink/" to "Associated Domains" ***
    //-------------------------------------------------------------------------------
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
       restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool
    {
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb, let url = userActivity.webpageURL else { return false }

//print("components:",components as Any)
//print("components.path.last:",components.path.last as Any)
//print("homeviewController:",homeviewController as Any)

        let order = url.lastPathComponent

//print("Please Wait  order:",order)
        LoadingIndicatorView.show("Please Wait")

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7, execute:
        {
            LoadingIndicatorView.show("Please Wait")
        })

        DataManager.getAgentHouseList(orderNumber:order, completion:
        { (error,data) in

//print(" ")
//print(" ")
//print("error:",error as Any)
//print(" ")
//print("data:",data as Any)
//print(" ")

            if data != nil
            {
//print("data != nil")
                DataManager.GetPaintColorsForAgentHomes(orderNumber: order,completion:
                {
                    (error,data) in

//                    comingFromfeed = true;
//print("data  comingFromfeed:",comingFromfeed)

                    DispatchQueue.main.asyncAfter(deadline: .now() + 4.0, execute:
                    {
//print("userActivity 1 LoadingIn dicatorView.hide Please Wait")
                        LoadingIndicatorView.hide()
                        if data != nil
                        {
                            if let home = homeviewController
                            {
//print("home view Controller loadDownloadedImage")
                                home.loadDownloadedImage()
                                return
                            } else {
//print("AppDelegate.orderDownloaded")
                                UserDefaults.standard.set(true, forKey: "loadedFirstTime")
                                AppDelegate.orderDownloaded()
                            }
                        } else {
                        }
                    })
                })

            } else {
//print("else")

//print("userActivity 2 LoadingIn dicatorView.hide Please Wait")
                LoadingIndicatorView.hide()
                AppDelegate.orderDownloaded()
            }
        })

        return true;
    }


	//************** change 3.0.0 add function ***
	//---------------------------------------------------------------------
	func convertColorTable()
	{
		let path:String! = Bundle.main.path(forResource: "SherwinColorTable", ofType: "txt")!;

		do
		{
			let responseData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe);
			let colors0 = String(data: responseData, encoding: String.Encoding.utf8)

			if let colors = colors0
			{
				let parts:[String] = colors.components(separatedBy: "\n")

//print(" Sherwi nObject parts:",parts);
//print("  ");
//print("  ");

				var kount:Int = -1
				var code:Int = 0
				var name:String = "0"
				var red:Int = 0
				var grn:Int = 0
				var blu:Int = 0

				for part in parts
				{
//print(" Sherwi nObject part:",part);
					if (part.count == 0) {continue}
					kount += 1

					if kount == 5
					{
						let sherwin = SherwinObject(code:code,name:name,red:red,grn:grn,blu:blu)
						sherwinColors.append(sherwin)
						if (sherwin.hueSatLumKeyE != 0)
						{
							let sherwinE = SherwinObject(code:code,name:name,red:red,grn:grn,blu:blu)
							sherwinE.hueSatLumKey = sherwin.hueSatLumKeyE
							sherwinE.hueSatLumKeyE = -999

//							sherwinE.code = -sherwin.code
//							sherwinE.name = sherwin.name+"002"
//
							sherwinColors.append(sherwinE)

//print("  ");
//print("  ");
//print(" Sherwi nObject sherwin,sherwinE:",sherwin,sherwinE);
//print("  ");
//print("  ");
//print(" Sherwi nObject sherwinColors:",sherwinColors);
//print("  ");
//print("  ");
						}
						kount = 0;
					}

					switch kount
					{
						case 0:
							code = Int(part) ?? 0;
						case 1:
							name = part;
						case 2:
							red = Int(part) ?? 0;
						case 3:
							grn = Int(part) ?? 0;
						case 4:
							blu = Int(part) ?? 0;

						default:
							break;
					}
				}

				if kount == 4
				{
					let sherwin = SherwinObject(code:code,name:name,red:red,grn:grn,blu:blu)
					sherwinColors.append(sherwin)
					if (sherwin.hueSatLumKeyE != 0)
					{
						let sherwinE = SherwinObject(code:code,name:name,red:red,grn:grn,blu:blu)
						sherwinE.hueSatLumKey = sherwin.hueSatLumKeyE
						sherwinE.hueSatLumKeyE = -999
						sherwinColors.append(sherwinE)
					}
					kount = 0;
				}

//print("  ");
//print("  ");
//print(" Sherwi nObject sherwinColors:",sherwinColors);
//print("  ");
//print("  ");

			}
		} catch {
  print(" convertColorTable error ");
			return
		}
	}

    //-------------------------------------------------------------------------------
    func checkAppUpdate()
    {
        let standardUserDefaults = UserDefaults.standard
        let shortVersionKey = "CFBundleShortVersionString"
        let currentVersion = Bundle.main.infoDictionary![shortVersionKey] as! String
        let previousVersion = standardUserDefaults.object(forKey: shortVersionKey) as? String
        if previousVersion == currentVersion {
            // same version
        } else {
            // replace with `if let previousVersion = previousVersion {` if you need the exact value
            if previousVersion != nil {
                // new version
                
            } else {
                // first launch
            }
            
            standardUserDefaults.set(currentVersion, forKey: shortVersionKey)
            standardUserDefaults.synchronize()
        }
    }

    //-------------------------------------------------------------------------------
    func applicationWillTerminate(_ application: UIApplication)
    {
        PersitanceService.saveContext()
        self.stopUpdates()
        self.removeObservers();
    }

    //-------------------------------------------------------------------------------
    func degrees(radians:Double) -> Double
    {
        return (180.0 / Double.pi) * radians
    }
    
    //-------------------------------------------------------------------------------
    func startUpdates()
    {
        //print("start Updates")
        if ( !motionManager.isDeviceMotionAvailable) { return }
        
        motionManager.deviceMotionUpdateInterval = 0.001
        attitude = simd_double3([0.0, 0.0, 0.0])
        
        motionManager.startDeviceMotionUpdates(using: .xArbitraryZVertical, to: .main)
        { deviceMotion, error in
            guard let deviceMotion = deviceMotion else { return }
            
            attitude = simd_double3([deviceMotion.attitude.roll, deviceMotion.attitude.pitch, deviceMotion.attitude.yaw])
            gravity = simd_double3([deviceMotion.gravity.x, deviceMotion.gravity.y, deviceMotion.gravity.z])
            rotationMatrix = deviceMotion.attitude.rotationMatrix
            

        }
    }
    
    //-------------------------------------------------------------------------------
    func stopUpdates()
    {
//print("stop Updates")
        //    let motionManager = CMMotionManager()
        if ( !motionManager.isDeviceMotionAvailable) { return }
        motionManager.stopDeviceMotionUpdates()
    }
    
    //-------------------------------------------------------------------------------
    private func removeObservers()
    {
        //        NotificationCenter.default.removeObserver(self)
//print("remove Observers self.photoButton")
    }

    // MARK: - message check
    //----------------------------------------------------------------------------
    @objc func checkForMessage()
    {
        messageTimer.invalidate()
        
        let mainBundle = Bundle.main;
        //    let bundleIDstrg  = mainBundle.bundleIdentifier;
        let bundleVsstrg:String = mainBundle.infoDictionary!["CFBundleShortVersionString"] as! String;
		let buildVsstrg:String  = mainBundle.infoDictionary!["CFBundleVersion"] as! String;
//print("bundle IDstrg : ",buildVsstrg as Any);
//print("bundle Vsstrg : ",bundleVsstrg as Any);
//print("build  Vsstrg : ",buildVsstrg as Any);
        
        let safeBundleVs = bundleVsstrg.replacingOccurrences(of: ".", with: "_")
		let safeBuildVs  =  buildVsstrg.replacingOccurrences(of: ".", with: "_")
        
//print("safeBundleVs : ",safeBuildVs as Any);
        
        let submitString = revSite+revSubSiteLS+revNotePref+safeBundleVs+safeBuildVs+revNoteFile;
//print("submitString : ",submitString as Any);
        
        
        let url = URL(string: submitString)!;
        
//print(" ");
//print(" ");
//print(" ");
//print("url = ", url);
//print("urlRequest = ", urlRequest);
        
        let task = URLSession.shared.downloadTask(with: url)
        { localURL, urlResponse, error in
//print("error: ", error as Any)
            if let localURL = localURL
            {
                if let string = try? String(contentsOf: localURL)
                {
                    //print("string: ", string)
                    if (string.count > 0)
                    {
                        self.setUpBulletinBrd(returnResponseOfBullet:string);
                    }
                }
            }
        }
        
        task.resume()
    }

	//------------------------------------------------------------------
    func putUpMessage(returnResponse:String)
    {
        lastMessageTime = Date();
//print("putUpMes sage lastMessageTime: ", lastMessageTime);

        DispatchQueue.main.async
		{
			if let topMostController = UIApplication.getTopViewController()
			{
				let message = NSLocalizedString(returnResponse, comment: "Alert message about important events in app's future environment")
				//Analytics.logEvent("Alert_Alert_message_about_important_events_in_apps_future_environment", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
				let alertController = UIAlertController(title: "fyi:", message: message, preferredStyle: .alert)

				alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"),
														style: .cancel,
														handler:
				{ _ in
				}))

				if let popoverController = alertController.popoverPresentationController  //********** change 3.0.3 add if block ***
				{
				  popoverController.sourceView = topMostController.view
				  popoverController.sourceRect = CGRect(x: topMostController.view.bounds.midX, y: topMostController.view.bounds.midY, width: 0, height: 0)
				  popoverController.permittedArrowDirections = []
				}

				topMostController.present(alertController, animated: true, completion: nil)
			}
        }
    }
    
	//------------------------------------------------------------------
    func setUpBulletinBrd(returnResponseOfBullet:String)
    {
        //print(" setUpBul letinBrd");
        
        let defaults = UserDefaults.standard;
        var numTimesMessageDisplayed = defaults.integer(forKey: "numTimesMessageDisplayed");
        
        //print("numTimesMessageDisplayed: ", numTimesMessageDisplayed);
        //print("  ");
        //print("  ");
        //print("lastMessageTime = ", lastMessageTime);
        //print("returnResponseOfBullet: ", returnResponseOfBullet);
        if (returnResponseOfBullet.count > 4)
        {
            if (returnResponseOfBullet.hasPrefix("<"))
            {
                //print("returnResponse hasPrefix:<");
            } else if(returnResponseOfBullet.hasPrefix("none")) {
                //print("returnResponse hasPrefix:none");
                defaults.set(0, forKey:"numTimesMessageDisplayed");
                defaults.synchronize();
            } else {
                var firstPartString:String = "junk";
                if (returnResponseOfBullet.count >= 6)
                {
                    firstPartString = (returnResponseOfBullet as NSString).substring(to: 6)
                }
                let origFirstPartString:String = defaults.string(forKey: "firstPartString") ?? " ";
                
                //print("firstPartString: ", firstPartString);
                //print("origFirstPartString: ", origFirstPartString);
                
                if (firstPartString != origFirstPartString)
                {
                    //print("firstPartString is NOT EqualToString:  origFirstPartString");
                    numTimesMessageDisplayed = 0;
                    defaults.set(firstPartString, forKey:"firstPartString");
                } else {
                    //print("lastMessageTime = ", lastMessageTime);
                    let lengthOfTime:TimeInterval = lastMessageTime.timeIntervalSinceNow * -1.0;
                    //print("lengthOfTime = ", lengthOfTime);
                    if (lengthOfTime < (HoursMult * 3.0)) {return;}
                }
                //print("numTimesMessageDisplayed = ", numTimesMessageDisplayed);
                
                if (numTimesMessageDisplayed > 1) {return;}
                
                defaults.set((numTimesMessageDisplayed + 1), forKey:"numTimesMessageDisplayed");
                defaults.synchronize();
                
                self.putUpMessage(returnResponse: returnResponseOfBullet);
            }
        }
    }
}


// MARK: UIApplication extensions
//---------------------------------------------------------------------
extension UIApplication
{
    class func getTopViewController(base: UIViewController? = UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.rootViewController) -> UIViewController?
    {
        if let nav = base as? UINavigationController
        {
            return getTopViewController(base: nav.visibleViewController)
            
        } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return getTopViewController(base: selected)
            
        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}

//---------------------------------------------------------------------
extension simd_float4
{
    //---------------------------------------------------------------------
    func magnitude() -> Float
    {
        return sqrtf((self.x * self.x) + (self.y * self.y) + (self.z * self.z))
    }
}

//---------------------------------------------------------------------
extension simd_double3
{
    //---------------------------------------------------------------------
    func magnitude() -> double_t
    {
        return sqrt((self.x * self.x) + (self.y * self.y) + (self.z * self.z))
    }
    
    //---------------------------------------------------------------------
    func normalize() -> simd_double3
    {
        let ratio = 1.0 / self.magnitude();
        return simd_double3((self.x * ratio), (self.y * ratio), (self.z * ratio))
    }
    
    //---------------------------------------------------------------------
    func angleBetween(secondVector:simd_double3) -> double_t
    {
        let l1 = self.magnitude();
        let l2 = secondVector.magnitude();
        if(l1 == 0.0 || l2 == 0.0) {return 0.0;}
        return acos(self.dotProduct(secondVector: secondVector)/(l1*l2));
    }
    
    //---------------------------------------------------------------------
    func dotProduct(secondVector:simd_double3) -> double_t
    {
        return ((self.x * secondVector.x) + (self.y * secondVector.y) + (self.z * secondVector.z))
    }
    
    //---------------------------------------------------------------------
    func crossProduct(secondVector:simd_double3) -> simd_double3
    {
        return simd_double3([(self.y * secondVector.z) - (self.z * secondVector.y), (self.z * secondVector.x) - (self.x * secondVector.z), (self.x * secondVector.y) - (self.y * secondVector.x)]);
    }
    
    //    //---------------------------------------------------------------------
    //    func rotationMatrixFromGravity(vector:double3)
    //    {
    //        // The Z axis of our rotated frame is opposite gravity //CMRotationMatrix
    //        let zRow:double3 = vector.normalize();
    //
    //        // The Y axis of our rotated frame is an arbitrary vector perpendicular to gravity
    //        // Note that this convention will have problems as zAxis.x approaches +/-1 since the magnitude of
    //        // [0, zAxis.z, -zAxis.y] will approach 0
    //        let yRow:double3 = double3([ 0.0, -1.0, 0.0]);
    //
    //        // The X axis is just the cross product of Y and Z
    //        let xRow:double3 = yRow.crossProduct(secondVector:zRow);
    //
    ////        CMRotationMatrix mat = {
    ////            xAxis.x, yAxis.x, zAxis.x,
    ////            xAxis.y, yAxis.y, zAxis.y,
    ////            xAxis.z, yAxis.z, zAxis.z};
    //
    ////        return mat;
    //    }
}

//---------------------------------------------------------------------
extension simd_quatf
{
    init(_ cmq: CMQuaternion)
    {
        self.init(ix: Float(cmq.x), iy: Float(cmq.y), iz: Float(cmq.z), r: Float(cmq.w))
    }
}

//---------------------------------------------------------------------
extension simd_float4x4
{
    //---------------------------------------------------------------------
    init(matrixGLK: GLKMatrix4)
    {
        self.init(columns: (simd_float4(x: matrixGLK.m00, y: matrixGLK.m01, z: matrixGLK.m02, w: matrixGLK.m03),
                            simd_float4(x: matrixGLK.m10, y: matrixGLK.m11, z: matrixGLK.m12, w: matrixGLK.m13),
                            simd_float4(x: matrixGLK.m20, y: matrixGLK.m21, z: matrixGLK.m22, w: matrixGLK.m23),
                            simd_float4(x: matrixGLK.m30, y: matrixGLK.m31, z: matrixGLK.m32, w: matrixGLK.m33)))
    }
    
    //---------------------------------------------------------------------
    init(matrixCMR: CMRotationMatrix)
    {
        self.init(columns: (simd_float4(x: Float(matrixCMR.m11), y: Float(matrixCMR.m12), z: Float(matrixCMR.m13), w: 0.0),
                            simd_float4(x: Float(matrixCMR.m21), y: Float(matrixCMR.m22), z: Float(matrixCMR.m23), w: 0.0),
                            simd_float4(x: Float(matrixCMR.m31), y: Float(matrixCMR.m32), z: Float(matrixCMR.m33), w: 0.0),
                            simd_float4(x: 0.0, y: 0.0, z: 0.0, w: 1.0)))
    }
    
    //---------------------------------------------------------------------
    init(angleZaxis: Float)
    {
        self.init(columns: (simd_float4(x: cos(angleZaxis), y: sin(angleZaxis), z: 0.0, w: 0.0),
                            simd_float4(x:-sin(angleZaxis), y: cos(angleZaxis), z: 0.0, w: 0.0),
                            simd_float4(x: 0.0, y: 0.0, z: 1.0, w: 0.0),
                            simd_float4(x: 0.0, y: 0.0, z: 0.0, w: 1.0)))
    }
    
    //---------------------------------------------------------------------
    static func makeRotate(radians: Float, _ x: Float, _ y: Float, _ z: Float) -> simd_float4x4
    {
        return unsafeBitCast(GLKMatrix4MakeRotation(radians, x, y, z), to: simd_float4x4.self)
    }
    
    //---------------------------------------------------------------------
    func rotate(radians: Float, _ x: Float, _ y: Float, _ z: Float) -> simd_float4x4
    {
        return self * simd_float4x4.makeRotate(radians:radians, x, y, z)
    }
}

//---------------------------------------------------------------------
extension float3x3
{
    func makeRotMatrix3(angle: Float) -> simd_float3x3
    {
        let rows =
		[
                simd_float3( cos(angle), sin(angle), 0.0),
                simd_float3(-sin(angle), cos(angle), 0.0),
                simd_float3( 0.0,          0.0,      1.0)
        ]

        return float3x3(rows: rows)
    }
}
