import Foundation
import UIKit
import PECimage

//********** change 0.0.40 replace whole swift file ***
//----------------------
public struct dotsGroup
{
    public var index:Int = 0;
    public var length:CGFloat = 0.0;

	//----------------------
    public init()
    {
    }

	//----------------------
    public init(_ index:Int, _ length:CGFloat)
    {
    	self.index = index
    	self.length = length
    }
}

let mpiDiv6:CGFloat = CGFloat.pi / 6.0;
let mpiDiv24:CGFloat = CGFloat.pi / 24.0;
let mpiDiv24Half:CGFloat = mpiDiv24 * 0.5;
let mpi2:CGFloat = CGFloat.pi*2.0;
let mpi3:CGFloat = CGFloat.pi*3.0;
let PIdivTwo:CGFloat = CGFloat.pi / 2.0
let PIdiv180:CGFloat = CGFloat.pi / 180.0
let pip30:CGFloat = CGFloat.pi+mpiDiv6;

var sufaceButtonRadius:CGFloat = 1.0;
var scalingLineWidth:CGFloat = 1.0;
var scalingOldToolSize:CGFloat = 1.0;

let dimLineFactor:CGFloat = 6.0
var lineWidthDistance:CGFloat = 1.0;

var maxDistance:CGFloat = 1.0;
var maxDistExtra:CGFloat = 1.0;

var lastPoint02:CGPoint = CGPoint();
var firstPoint0:CGPoint = CGPoint();


//--------------------------------------------------------------------------------------
class PaintScrollView: UIScrollView, UIScrollViewDelegate, UIGestureRecognizerDelegate
{
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var imageContainView: UIView!

	@IBOutlet weak var paintImageOverlay0: UIImageView!
    @IBOutlet weak var paintImageOverlay1: UIImageView!
    @IBOutlet weak var paintImageOverlay2: UIImageView!
    @IBOutlet weak var paintImageOverlay3: UIImageView!
    @IBOutlet weak var paintImageOverlay4: UIImageView!

    @IBOutlet weak var garageImageView: UIImageView!
    @IBOutlet weak var garageMaskView: UIImageView!
    @IBOutlet weak var frontDrMaskView: UIImageView!

//	@IBOutlet weak var logoContainer: UIView!
//    @IBOutlet weak var logoView: UIImageView!

	@IBOutlet weak var imageViewOverlay: UIImageView!

	@IBOutlet var dblTapLineRecog: UITapGestureRecognizer!

	//------------------- pad views -------------------------------------
	@IBOutlet weak var touchPadView: RoundTouchPadView!
	@IBOutlet weak var pullBarButton: UIButton!

	@IBOutlet weak var d1Button: UIPenButton!
	@IBOutlet weak var d2Button: UIPenButton!

	@IBOutlet weak var deleteDimButton: UIButton!
	@IBOutlet weak var editDimButton: UIButton!
//	@IBOutlet weak var cutButton: UIButton!

	@IBOutlet weak var layrBarButton: UIButton!
	@IBOutlet weak var layrControlView: UIView!

	var buttonV0:PECButton?
	var interXButton:PECButton?

	var cutOffX:CGFloat = 757.0;
	var cutOffY:CGFloat = 568.2;
	var maxOffsetX:CGFloat = 343.0
	var maxOffsetY:CGFloat = 1470.0

	var origFrameWidth:CGFloat = 2.0
	var firstIn:Bool = false;

	var reverseZoomFactor:CGFloat = 1.0;
	let distScaleFactor:CGFloat = 19.0;

	var magnifyOffset:CGPoint = CGPoint();
	var magnifyMask:CAShapeLayer = CAShapeLayer();
	var magnifyTarget:CAShapeLayer = CAShapeLayer();
	var magnifyRect:CGRect = CGRect();
	var magnifyOriginalFrame:CGRect = CGRect();

	var touchStart:CGPoint = CGPoint();
	var wasInterXPoint:Bool = false;
	var smallistDist:CGFloat = 0.0;

	var curvePts:[CGPoint] = [CGPoint]();
	var dotsArray:[dotsGroup] = [dotsGroup]();

	var curvePath:UIBezierPath?;
    var touchOffset:CGPoint = CGPoint();

	var pullBarCenterX:CGFloat = 0.0;
	var pullBarMax:CGFloat = 0.0;
	var pullBarMin:CGFloat = 0.0;
	var pingBarMax:CGFloat = 0.0;
	var pingBarMin:CGFloat = 0.0;
	var layrBarMax:CGFloat = 0.0;
	var layrBarMin:CGFloat = 0.0;

	var pullBarCurrentOffset:CGFloat = 0.0;
	var pingBarCurrentOffset:CGFloat = 0.0;
	var layrBarCurrentOffset:CGFloat = 0.0;

	var wasPullDragging:Bool = false;
	var wasStart:Bool = false;

	

	// MARK: - gesture
	//----------------------------------------------------------------------------------------------------------
	func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool
	{
		self.delaysContentTouches = true;
		return true;
	}

	//----------------------------------------------------------------------------------------------------------
	override func layoutSubviews()
	{
//print(" ");
//print(" ");
//print("layout Subviews");
		super.layoutSubviews();

		if (viewTransition)
		{
			self.setMaxMinZoomScalesForCurrentBounds()
			if (self.zoomScale < self.minimumZoomScale)
			{
				self.zoomScale = self.minimumZoomScale;
				self.contentOffset = CGPoint.init(x:0.0, y:0.0)
				self.setContentOffset(self.contentOffset, animated:false)
			}
		}
//print("layout Subviews  self.contentOffset:",self.contentOffset);

		currentPaintImageController.galleryButton.frame.origin.x = 10.0
		currentPaintImageController.proSwitch.frame.origin.y = currentPaintImageController.view.frame.height - 33.0;
		currentPaintImageController.proLabel.frame.origin.y = currentPaintImageController.view.frame.height - 28.0;

		if ( !phonePortrait)
		{
			currentPaintImageController.galleryButton.frame.origin.x = 32.0
			currentPaintImageController.proSwitch.frame.origin.y = currentPaintImageController.view.frame.height - 55.0;
			currentPaintImageController.proLabel.frame.origin.y = currentPaintImageController.view.frame.height - 50.0;
		}

		//------------------------------ image frame --------------------------------------
		var frameToCenter:CGRect = self.containerView.frame;
//print("layout Subviews self.contain erView.frame",self.containerView.frame)

		if (frameToCenter.size.width < self.bounds.size.width)
		{
			frameToCenter.origin.x = (self.bounds.size.width - frameToCenter.size.width) / 2.0;

			if ( !phonePortrait)
			{
				frameToCenter.origin.x = 0.0;
			}

		} else {
			frameToCenter.origin.x = 0.0;
		}
		frameToCenter.origin.y = 0.0;

		maxOffsetX = ((self.contentSize.width / self.cutOffX) * origFrameWidth) - 71.0
		maxOffsetY = ((self.contentSize.height / self.cutOffY) - 125.0)

//print("2 layoutSubviews containWidthImage,containHeightImage:",containWidthImage,containHeightImage);
//print("2 layoutSubviews maxOffsetX,maxOffsetY:",maxOffsetX,maxOffsetY);

		if (origFrameWidth > 4.0)
		{
			self.contentOffset = CGPoint.init(x:min(self.contentOffset.x,maxOffsetX),
											  y:min(self.contentOffset.y,maxOffsetY))

			if UIDevice.current.hasNotch
			{
				if (phonePortrait)
				{
					if (self.firstIn || (self.zoomScale < (self.minimumZoomScale * 1.02) && self.contentOffset.y >= 0.0))
					{
						self.contentOffset = CGPoint.init(x:0.0, y:0.0)  //-44.0
						self.setContentOffset(self.contentOffset, animated:false)
						self.firstIn = false;
//print("layout Subviews current.hasNotch   self.conte ntOffset:",self.contentOffset);
					}
				} else {
					if (self.firstIn || (self.zoomScale < (self.minimumZoomScale * 1.02) && self.contentOffset.x >= 0.0))
					{
						self.contentOffset = CGPoint.init(x:0.0, y:0.0) //-44.0
						self.setContentOffset(self.contentOffset, animated:false)

						self.firstIn = false;
					}
				}
			}
		}

//print("  ");
//print(" self.contentOf fset:",self.contentOffset);

		self.containerView.frame = frameToCenter;
//print("3 layoutSubviews self.contai nerView.frame:",self.containerView.frame);
//print("3 layoutSubviews self.self.contentOffset:",self.contentOffset);
//print("3 layoutSubviews self.self.cont entSize:",self.contentSize);

		self.imageContainView.frame.origin = CGPoint.zero;
		self.imageView.frame.origin = CGPoint.zero;

		self.paintImageOverlay0.frame.origin = CGPoint.zero;
		self.paintImageOverlay1.frame.origin = CGPoint.zero;
		self.paintImageOverlay2.frame.origin = CGPoint.zero;
		self.paintImageOverlay3.frame.origin = CGPoint.zero;
		self.paintImageOverlay4.frame.origin = CGPoint.zero;

        self.garageImageView.frame.origin = CGPoint.zero;
        self.garageMaskView.frame.origin = CGPoint.zero;
        self.frontDrMaskView.frame.origin = CGPoint.zero;

		self.imageViewOverlay.frame.origin = CGPoint.zero;

		viewTransition = false;

		//-------------- for editing Doors and paint ---------------------
		reverseZoomFactor = 1.0 / self.zoomScale;
		lineWidthDistance = max(reverseZoomFactor * dimLineFactor,0.375);

		//-------------------- for eraser and pen tool -------------------------------
		if ( !currentPaintImageController.d4Button.isHidden)
		{
			let toolSizeAdjustment:CGFloat = max(pow(CGFloat(scalingOldToolSize), 3.0),0.375);
			let theLineWidth:CGFloat = min(lineWidthDistance * toolSizeAdjustment * 3.0,768.0);

			let adjustedD4Size0 = scalingLineWidth * self.zoomScale * 1.333;
			let adjustedD4Size = max(min(scalingLineWidth * self.zoomScale * 1.333,130.0),2.0);

			var adjustedPenSize:CGFloat = scalingOldToolSize;

			if ((scalingOldToolSize >= 0.01) && (scalingOldToolSize <= 2.0))
			{
				let theLineWidth0 = theLineWidth * self.zoomScale * 1.333;
				let adjustedPenSize0:CGFloat = adjustedD4Size0 / theLineWidth0;
				let adjustedPenSize1:CGFloat = adjustedPenSize0 * scalingOldToolSize;
					adjustedPenSize = max(min(pow(CGFloat(adjustedPenSize1), 0.33333),2.0),0.01);
			}

			//-------------------- synchronize -------------------------------
			if (currentPaintImageController.eraserButton.isSelected)
			{
				UserDefaults.standard.set(adjustedPenSize, forKey: "eraserToolSizeAdjustment")
			} else if (currentPaintImageController.penButton.isSelected) {
				UserDefaults.standard.set(adjustedPenSize, forKey: "toolSizeAdjustment")
			}
			UserDefaults.standard.synchronize();

			//-------------------- display corrected sizes -------------------------------
			currentPaintImageController.toolSizeSlider.value = Float(adjustedPenSize)
			let d4ButtonSize:CGSize = CGSize(width:adjustedD4Size, height:adjustedD4Size);
			currentPaintImageController.d4Button.bounds = CGRect(origin:CGPoint.zero, size: d4ButtonSize);
		}


		//-------------------- for paint cut xacto and rectangle -------------------------------
		let lineWidth = 10.0 * min(theImageScale*2.0,1.0);
		let lineScale = min(theImageScale*2.0,1.0);

		if (currentPaintImageController.editPaintButton.isSelected) ||
		   (currentPaintImageController.cutPaintButton.isSelected) ||
		   (currentPaintImageController.copyPaintButton.isSelected)
		{
			if (reverseZoomFactor > 5.5)
			{
				currentPaintImageController.paintLayer.lineWidth = min(lineWidth * reverseZoomFactor,14.0*lineScale); // 28.0
			} else if (reverseZoomFactor > 3.75) {
				currentPaintImageController.paintLayer.lineWidth = min(lineWidth * reverseZoomFactor,10.0*lineScale); // 20.0
			} else if (reverseZoomFactor > 2.5) {
				currentPaintImageController.paintLayer.lineWidth = min(lineWidth * reverseZoomFactor,8.0*lineScale); // 15.0
			} else if (reverseZoomFactor > 1.0) {
				currentPaintImageController.paintLayer.lineWidth = min(lineWidth * reverseZoomFactor,5.0*lineScale); // 15.0
			} else if (reverseZoomFactor > 0.5) {
				currentPaintImageController.paintLayer.lineWidth = min(lineWidth * reverseZoomFactor,3.0*lineScale); // 10.0
			} else {
				currentPaintImageController.paintLayer.lineWidth = min(lineWidth * reverseZoomFactor,1.5*lineScale); // 6.0
			}
		}
		//-------------------- for paint cut xacto and rectangle end -------------------------------


		let markerButtonWidth: CGFloat = cropButtonSize / self.zoomScale
		let markerButtonSize:CGSize = CGSize(width: CGFloat(markerButtonWidth), height: CGFloat(markerButtonWidth))
		let sufaceButtonWidth:CGFloat = 50.0 * reverseZoomFactor
		let magnifyWidth:CGFloat = 100.0 * reverseZoomFactor
		let magnifyOff:CGFloat = 75.0 * reverseZoomFactor
//		maskTarget.lineWidth = 1.2 * reverseZoomFactor

		let surfaceButtonSize = CGSize(width: CGFloat(sufaceButtonWidth), height: CGFloat(sufaceButtonWidth))
		let magnifySize:CGSize = CGSize(width: magnifyWidth, height: magnifyWidth);
		magnifyOffset = CGPoint(x:magnifyOff, y:magnifyOff)

		self.deleteDimButton.bounds = CGRect(origin:CGPoint.zero, size: markerButtonSize)
		self.editDimButton.bounds = CGRect(origin:CGPoint.zero, size: markerButtonSize)

		self.d1Button.bounds = CGRect(origin:CGPoint.zero, size: surfaceButtonSize)
		self.d2Button.bounds = CGRect(origin:CGPoint.zero, size: surfaceButtonSize)

//		maskRect = CGRect(origin:CGPoint.zero, size: magnifySize)

		maxDistance = reverseZoomFactor * distScaleFactor;

		if (currentPaintImageController.editPaintButton.isSelected) || (currentToolIndex == 11)
		{
			if let frstButton = currentPaintImageController.theButtonV0
			{
				self.deleteDimButton.center = CGPoint(x: frstButton.center.x - (maxDistance * 1.75),
													  y: frstButton.center.y - (maxDistance * 1.75))
			}

			if let lastButton = self.buttonV0
			{
				self.editDimButton.center   = CGPoint(x: lastButton.center.x + (maxDistance * 1.75),
													  y: lastButton.center.y - (maxDistance * 1.75))
			}
		} else {
			self.deleteDimButton.center = CGPoint(x: self.d1Button.center.x - (maxDistance * 1.75),
												  y: self.d1Button.center.y - (maxDistance * 1.75))
			self.editDimButton.center   = CGPoint(x: self.d2Button.center.x + (maxDistance * 1.75),
												  y: self.d2Button.center.y - (maxDistance * 1.75))
		}


		magnifyTarget.lineWidth = 1.2 * reverseZoomFactor;
		magnifyRect = CGRect(x:0.0, y:0.0, width:magnifySize.width, height:magnifySize.height);

		self.drawPaintCorners();
	}

	//----------------------------------------------------------------------------------------------------------
	@IBAction func dblTapAction(_ recognizer: UITapGestureRecognizer)
	{

	}
//	{
////print("dblTapAction recognizer",recognizer)
//		if ( !isThisForPaint) {return;}
//
//        switch (recognizer.state)
//        {
//            case UIGestureRecognizer.State.began:
////print("dblTapAction State.began recognizer",recognizer)
//            break;
//
//            case UIGestureRecognizer.State.changed:
//            break;
//
//            case UIGestureRecognizer.State.ended:
//
//                    var bestDist:CGFloat = 999999.0;
//                    var pt0:Ppoint?;
//                    var respItem0:Response?;
//                    let recPt:CGPoint = recognizer.location(in:self.imageVi ewOverlay);
//
//				//	var sizeButton:PECButton?
//					var lockButton:PECButton?
//
//
//                    if let ho = hou seObject
//                    {
//                        for resp in ho.response
//                        {
//                            for respItem in resp
//                            {
//                            	lockButton = nil;
//								if let button = respItem.p1.button
//								{
//									if let abutton = button.lockLinkFirst
//									{
//										lockButton = abutton;
//									}
//								}
//
//                                if (respItem.wasPoly) {break;}
//                                if (respItem.wasArch)
//                                {
//                                    let pt1:CGPoint = respItem.p1.pt;
//                                    let pt2:CGPoint = respItem.p1.nx.pt;
//
//                                    var dist0:CGFloat = findDistance2D(pt1,pt2)
//                                    var dist1:CGFloat = findDistance2D(pt1,recPt)
//                                    var dist2:CGFloat = findDistance2D(pt2,recPt)
//
//                                    if ((dist1 < sufaceButtonRadius) || (dist2 < sufaceButtonRadius))  {break;}
//
//                                    var distT:CGFloat = dist1+dist2;
//
//                                    if (distT < dist0*1.12)
//                                    {
//                                        if (distT < bestDist)
//                                        {
//                                            bestDist = distT;
//                                            pt0 = respItem.p1;
//                                            respItem0 = respItem;
//                                        }
//                                    }
//
//                                    let pt3:CGPoint = respItem.p1.nx.nx.pt;
//
//                                    dist0 = findDistance2D(pt2,pt3)
//                                    dist1 = findDistance2D(pt2,recPt)
//                                    dist2 = findDistance2D(pt3,recPt)
//
//                                    if ((dist1 < sufaceButtonRadius) || (dist2 < sufaceButtonRadius))  {break;}
//
//                                    distT = dist1+dist2;
//
//                                    if (distT < dist0*1.12)
//                                    {
//                                        if (distT < bestDist)
//                                        {
//                                            bestDist = distT;
//                                            pt0 = respItem.p1.nx;
//                                            respItem0 = respItem;
//                                        }
//                                    }
//                                } else {
//                                    let pt1:CGPoint = respItem.p1.pt;
//                                    let pt2:CGPoint = respItem.p1.nx.pt;
//
//                                    let dist0:CGFloat = findDistance2D(pt1,pt2)
//                                    let dist1:CGFloat = findDistance2D(pt1,recPt)
//                                    let dist2:CGFloat = findDistance2D(pt2,recPt)
//
//                                    if ((dist1 < sufaceButtonRadius) || (dist2 < sufaceButtonRadius))  {break;}
////print("  ")
////print("dist1,dist2",dist1,dist2)
//
//                                    let distT:CGFloat = dist1+dist2;
//
//                                    if (distT > dist0*1.12) {break;}
//
//                                    if (distT < bestDist)
//                                    {
//                                        bestDist = distT;
//                                        pt0 = respItem.p1;
//                                        respItem0 = respItem;
//                                    }
//                                }
//                            }
//                        }
//                    }
//
//
//					//-------- there was a new point --------------
//                    if let respItem00 = respItem0
//                    {
//                        if let pt00 = pt0
//                        {
//                            if (respItem00.wasArch)
//                            {
//                                respItem00.wasPoly = true;
//                                respItem00.wasArch = false;
//                            } else  {
//                                respItem00.wasArch = true;
//                            }
//
//                            let nxp:Ppoint! = pt00.nx;
//
//                            let theButtonV2:PECButton = currentPaintImageController.cornerButton.pecDuplicate()!
//                            self.imageVi ewOverlay.addSubview(theButtonV2)
//
//							if let abutton = lockButton   //*************** change 0.0.15 add if block ***
//							{
//								self.imageVie wOverlay.addSubview(abutton)
//							}
//
//
//                            theButtonV2.center = recPt;
//                            theButtonV2.ppt = Ppoint.init();
//                            theButtonV2.ppt.button = theButtonV2;
//                            theButtonV2.deleteCornerRecog.isEnabled = true;
//                            theButtonV2.respItem = respItem00;
//
//                            theButtonV2.ppt.nx = nxp;
//                            theButtonV2.ppt.lt = pt00;
//                            pt00.nx = theButtonV2.ppt;
//                            nxp.lt  = theButtonV2.ppt;
//
//                            theButtonV2.ppt.pt = recPt;
//
//							respItem00.p6 = respItem00.p5
//							respItem00.p5 = respItem00.p4
//							respItem00.p4 = respItem00.p3
//
//							if (respItem00.wasArch)
//							{
//								respItem00.p3 = respItem00.p2
//								respItem00.p2 = theButtonV2.ppt
//							} else {
//								if (pt00 == respItem00.p2)
//								{
//									respItem00.p3 = theButtonV2.ppt
//								} else {
//									respItem00.p3 = respItem00.p2
//									respItem00.p2 = theButtonV2.ppt
//								}
//							}
//
//							if let mainEditDr = currentMainImageController
//							{
//								mainEditDr.drawDoorObjectAt()
//								currentPaintImageController.scrollView.drawPaintCorners();
//							}
//                        }
//                    }
//
//           break;
//
//            case UIGestureRecognizer.State.cancelled:
//            break;
//
//            case UIGestureRecognizer.State.failed:
//            break;
//
//               case UIGestureRecognizer.State.possible:
//            break;
//
//            default:
//            break;
//        }
//    }

	//----------------------------------------------------------------------------------------------------------
	func drawPaintCorners()
	{
		let cornerWidth = min(theImageScale*1.95,1.0);
		let width:CGFloat = ((1.0084135710597*(reverseZoomFactor * reverseZoomFactor)) - (18.012331804514*reverseZoomFactor) + 130.0) * cornerWidth;
		let surfaceButtonWidth:CGFloat = max((width * 0.6) * reverseZoomFactor,(50.0 * cornerWidth)); //1.2 75.0
		sufaceButtonRadius = surfaceButtonWidth * 0.5;

		let surfaceButtonWidth2 = max(cornerButtonSize / self.zoomScale, (50.0 * cornerWidth))
		let surfaceButtonSize:CGSize = CGSize(width: CGFloat(surfaceButtonWidth2), height: CGFloat(surfaceButtonWidth2))

		sufaceButtonRadius = surfaceButtonWidth2 * 0.5;

		let markerButtonWidth: CGFloat = surfaceButtonWidth2 * 0.5
		let markerButtonSize:CGSize = CGSize(width: CGFloat(markerButtonWidth), height: CGFloat(markerButtonWidth))

		for subView in self.imageViewOverlay.subviews
		{
			if let button:PECButton = subView as? PECButton
			{
				if (button.isLock)
				{
					button.bounds = CGRect(origin:CGPoint.zero, size: surfaceButtonSize);
				} else if (button.isForSizing) {
					button.bounds = CGRect(origin:CGPoint.zero, size: surfaceButtonSize);

					if let buttonV3 = button.showhideLt
					{
						button.center = buttonV3.center;
						button.center.x -= sufaceButtonRadius*0.85;
						button.center.y -= sufaceButtonRadius*0.85;
					}

				} else {
					button.bounds = CGRect(origin:CGPoint.zero, size: surfaceButtonSize);
				}
			} else if let button:UIButton = subView as? UIButton {
				button.bounds = CGRect(origin:CGPoint.zero, size: markerButtonSize)
			}
		}
	}

	// MARK: - math
	//---------------------------------------------------------------------
	func findDistance2D(_ pt1:CGPoint, _ pt2:CGPoint) -> CGFloat
	{
		return sqrt(((pt2.x - pt1.x) * (pt2.x - pt1.x)) + ((pt2.y - pt1.y) * (pt2.y - pt1.y)));
	}

	//----------------------------------------------------------------------------------------------------------
	func findPerpPt_2D(_ pt: CGPoint, _ line:Bline) -> (wasGood:Bool, pt1:CGPoint)
	{
		var ax, bx, ay, by, above, below, td, dx, dy:CGFloat

		ax = line.epoint.x - line.spoint.x;
		bx = pt.x - line.spoint.x;

		ay = line.epoint.y - line.spoint.y;
		by = pt.y - line.spoint.y;

		above = bx * ax + by * ay;
		below = ax * ax + ay * ay;

		if (abs(below) < CGFloat.ulpOfOne) {return (false, CGPoint(x:9.9, y:9.9))}
		else {td = above / below;}

		if ((td < 0.0) || (td > 1.0))  {return (false, CGPoint(x:9.9, y:9.9))}

		dx = td * ax;
		dy = td * ay;

		return (true, CGPoint(x:line.spoint.x + dx, y:line.spoint.y + dy))
	}


	// MARK:  - touches actions
	//----------------------------------------------------------------------------------------------------------
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
	{
//print("touches Began currentToolIndex:",currentToolIndex);
		super.touchesBegan(touches, with:event);
//		isActivateSurfaceTap = false;

		touchStart = touches.first?.location(in:self.imageView) ?? CGPoint.zero
//		startPoint = touchStart;
//		draggingPt = touchStart;

		touchOffset = CGPoint.zero;
//		pushDownTriggered = false;
		curvePath = nil;
//		currentPaintImageController.galleryView.isHidden = true;
		reverseZoomFactor = 1.0 / self.zoomScale;

		switch (currentToolIndex)
		{
			case -1:
				if ( !twasForModifai)
				{
					currentPaintImageController.touchesOnColor(touchStart);
				}
			break;

			case 88:
					currentToolIndex = -1;
				break;

			case 8668:
					currentPaintImageController.touchesDealWithThePush(touchStart);
				break;

			case 8669:
					currentPaintImageController.touchesDealWithClearPush(touchStart);
				break;

			case 26:
				break;

			case 25:
				break;

			case 24:
				break;

			case 23:
				break;

			case 22:
				break;

			case 21:
				break;

			case 20:
				break;

			case 1:
				break;

			case 11:
					if (self.wasStart)
					{
						firstPoint0 = touchStart;
						self.wasInterXPoint = false

						let theButtonV1:PECButton = currentPaintImageController.cornerButton.pecPaintDuplicate()!
						theButtonV1.center = touchStart;
						self.imageViewOverlay.addSubview(theButtonV1)
						theButtonV1.tag = 10;
						theButtonV1.isForPaint = true;
						theButtonV1.ppt = Ppoint.init(pt:theButtonV1.center);
						theButtonV1.ppt.button = theButtonV1;

						let theButtonV2:PECButton = currentPaintImageController.cornerButton.pecPaintDuplicate()!
						theButtonV2.center = touchStart;
						self.imageViewOverlay.addSubview(theButtonV2)
						theButtonV2.tag = 10;
						theButtonV2.isForPaint = true;
						theButtonV2.ppt = Ppoint.init(pt:theButtonV2.center);
						theButtonV2.ppt.button = theButtonV2;

						theButtonV1.ppt.lt = theButtonV2.ppt;
						theButtonV1.ppt.nx = theButtonV2.ppt;
						theButtonV2.ppt.lt = theButtonV1.ppt;
						theButtonV2.ppt.nx = theButtonV1.ppt;

	  					currentPaintImageController.theButtonV0 = theButtonV1;
						self.buttonV0 = theButtonV2


						self.deleteDimButton.isHidden =  false;
						self.editDimButton.isHidden =  false;

						self.deleteDimButton.center = CGPoint(x: touchStart.x - (maxDistance * 1.75),
															  y: touchStart.y - (maxDistance * 1.75))
						self.editDimButton.center   = CGPoint(x: touchStart.x + (maxDistance * 1.75),
															  y: touchStart.y - (maxDistance * 1.75))

						self.drawPaintCorners();
					} else {

						let theButtonV1:PECButton = currentPaintImageController.cornerButton.pecPaintDuplicate()!
						theButtonV1.center = touchStart;
						self.imageViewOverlay.addSubview(theButtonV1)
						if ( !dragSquare) {theButtonV1.tag = 10;}
						theButtonV1.isForPaint = true;
						theButtonV1.ppt = Ppoint.init(pt:theButtonV1.center);
						theButtonV1.ppt.button = theButtonV1;

						var pt0:Ppoint?;
						self.wasInterXPoint = false

						if let frstButton = currentPaintImageController.theButtonV0
						{
							let ppft:Ppoint = frstButton.ppt;
							var bestDist:CGFloat = 999999.0;

							if var curPt = ppft.nx
							{
								while curPt != ppft
								{
									let pt1:CGPoint = curPt.lt.pt;
									let pt2:CGPoint = curPt.pt;
									let bline:Bline = Bline(pt1,pt2);
									let bundlePt = findPerpPt_2D(touchStart, bline)

									if (bundlePt.wasGood)
									{
										let dist00:CGFloat = ccpDistance(touchStart,bundlePt.pt1)

										let dist0:CGFloat = ccpDistance(pt1,pt2)
										let dist1:CGFloat = ccpDistance(pt1,touchStart)
										let dist2:CGFloat = ccpDistance(pt2,touchStart)


										let distT:CGFloat = dist1+dist2;

										if ((distT < dist0*1.01) && (dist00 < bestDist))
										{
											bestDist = dist00;
											pt0 = curPt.lt;
										}
									}

									curPt = curPt.nx;
								}

							}
						}

						if let pt00 = pt0
						{
							let nxp:Ppoint! = pt00.nx;

							pt00.nx = theButtonV1.ppt;
							theButtonV1.ppt.lt = pt00;
							theButtonV1.ppt.nx = nxp;
							nxp.lt = theButtonV1.ppt

							self.wasInterXPoint = true;
							self.interXButton = theButtonV1;

						} else {

								self.buttonV0?.ppt.nx = theButtonV1.ppt;
								theButtonV1.ppt.lt = self.buttonV0?.ppt;
								theButtonV1.ppt.nx = currentPaintImageController.theButtonV0?.ppt;
								currentPaintImageController.theButtonV0?.ppt.lt = theButtonV1.ppt

								self.buttonV0 = theButtonV1

								self.editDimButton.center   = CGPoint(x: touchStart.x + (maxDistance * 1.75),
																	  y: touchStart.y - (maxDistance * 1.75))
						}
					}
				break;

			case 2:
					self.d1Button.isHidden = false;
					self.d2Button.isHidden = false;
					self.d1Button.center = touchStart
					self.d2Button.center = touchStart
					self.d1Button.touchOffset = CGPoint.zero
					self.d2Button.touchOffset = CGPoint.zero

					self.deleteDimButton.isHidden =  false;
					self.editDimButton.isHidden =  false;

					self.deleteDimButton.center = CGPoint(x: touchStart.x - (maxDistance * 1.75),
														  y: touchStart.y - (maxDistance * 1.75))
					self.editDimButton.center   = CGPoint(x: touchStart.x + (maxDistance * 1.75),
														  y: touchStart.y - (maxDistance * 1.75))

					self.curvePts.append(touchStart)
					lastPoint02 = touchStart;
				break;

			case 3:
					self.d1Button.isHidden = false;
					self.d2Button.isHidden = false;
					self.d1Button.center = touchStart
					self.d2Button.center = touchStart
					self.d1Button.touchOffset = CGPoint.zero
					self.d2Button.touchOffset = CGPoint.zero

					self.deleteDimButton.isHidden =  false;
					self.editDimButton.isHidden =  false;

					self.deleteDimButton.center = CGPoint(x: touchStart.x - (maxDistance * 1.75),
														  y: touchStart.y - (maxDistance * 1.75))
					self.editDimButton.center   = CGPoint(x: touchStart.x + (maxDistance * 1.75),
														  y: touchStart.y - (maxDistance * 1.75))

					self.curvePts.append(touchStart)
					lastPoint02 = touchStart;
				break;

			case 4:
					self.d1Button.isHidden = false;
					self.d2Button.isHidden = false;
					self.d1Button.center = touchStart
					self.d2Button.center = touchStart
					self.d1Button.touchOffset = CGPoint.zero
					self.d2Button.touchOffset = CGPoint.zero

					self.deleteDimButton.isHidden =  false;
					self.editDimButton.isHidden =  false;

					self.deleteDimButton.center = CGPoint(x: touchStart.x - (maxDistance * 1.75),
														  y: touchStart.y - (maxDistance * 1.75))
					self.editDimButton.center   = CGPoint(x: touchStart.x + (maxDistance * 1.75),
														  y: touchStart.y - (maxDistance * 1.75))

					self.curvePts.append(touchStart)
					lastPoint02 = touchStart;
				break;

			case 5:

//print("  ")
//print("  ")
//print("00 touchesBegan self.wasS tart:",self.wasStart)

					if (self.wasStart)
					{
						firstPoint0 = touchStart;
						self.wasInterXPoint = false

						let theButtonV1:PECButton = currentPaintImageController.cornerButton.pecPaintDuplicate()!
						theButtonV1.center = touchStart;
						self.imageViewOverlay.addSubview(theButtonV1)
						theButtonV1.tag = 10;
						theButtonV1.isForPaint = true;
						theButtonV1.ppt = Ppoint.init(pt:theButtonV1.center);
						theButtonV1.ppt.button = theButtonV1;

						let theButtonV2:PECButton = currentPaintImageController.cornerButton.pecPaintDuplicate()!
						theButtonV2.center = touchStart;
						self.imageViewOverlay.addSubview(theButtonV2)
						theButtonV2.tag = 10;
						theButtonV2.isForPaint = true;
						theButtonV2.ppt = Ppoint.init(pt:theButtonV2.center);
						theButtonV2.ppt.button = theButtonV2;

						theButtonV1.ppt.lt = theButtonV2.ppt;
						theButtonV1.ppt.nx = theButtonV2.ppt;
						theButtonV2.ppt.lt = theButtonV1.ppt;
						theButtonV2.ppt.nx = theButtonV1.ppt;

	  					currentPaintImageController.theButtonV0 = theButtonV1;
						self.buttonV0 = theButtonV2

//print("00 touchesBegan currentPaintImageController.theButtonV0:",currentPaintImageController.theButtonV0 as Any)
//print("00 touchesBegan self.buttonV0:",self.buttonV0 as Any)
//print("00 touchesBegan self.wasInterXPoint:",self.wasInterXPoint as Any)
//print("00 touchesBegan firstPoint0:",firstPoint0 as Any)


						self.deleteDimButton.isHidden =  true;
						self.editDimButton.isHidden =  true;

						self.deleteDimButton.center = CGPoint(x: touchStart.x - (maxDistance * 1.75),
															  y: touchStart.y - (maxDistance * 1.75))
						self.editDimButton.center   = CGPoint(x: touchStart.x + (maxDistance * 1.75),
															  y: touchStart.y - (maxDistance * 1.75))

						self.drawPaintCorners();
					} else {

						let theButtonV1:PECButton = currentPaintImageController.cornerButton.pecPaintDuplicate()!
						theButtonV1.center = touchStart;
						self.imageViewOverlay.addSubview(theButtonV1)
						if ( !dragSquare) {theButtonV1.tag = 10;}
						theButtonV1.isForPaint = true;
						theButtonV1.ppt = Ppoint.init(pt:theButtonV1.center);
						theButtonV1.ppt.button = theButtonV1;

						var pt0:Ppoint?;
						self.wasInterXPoint = false
						var lastWasLarger:Bool = false;

						var sizeButton:PECButton?
						var lockButton:PECButton?
						var cutButton:UIButton?
						var fillButton:UIButton?

						if let frstButton = currentPaintImageController.theButtonV0
						{
							let ppft:Ppoint = frstButton.ppt;
							var bestDist:CGFloat = 999999.0;

							if (dragSquare)
							{
								if let abutton = frstButton.showhideLt
								{
									sizeButton = abutton;
								}

								if let abutton = frstButton.lockLinkFirst
								{
									lockButton = abutton;
								}

								if let abutton = frstButton.delButtonNx
								{
									fillButton = abutton;
								}
							}

							if var curPt = ppft.nx
							{
								while curPt != ppft
								{
//print("  ");
//print("curPt",curPt as Any);
									let pt1:CGPoint = curPt.lt.pt;
									let pt2:CGPoint = curPt.pt;
									let bline:Bline = Bline(pt1,pt2);
									let bundlePt = findPerpPt_2D(touchStart, bline)

									if (bundlePt.wasGood)
									{
										let dist00:CGFloat = ccpDistance(touchStart,bundlePt.pt1)

										let dist0:CGFloat = ccpDistance(pt1,pt2)
										let dist1:CGFloat = ccpDistance(pt1,touchStart)
										let dist2:CGFloat = ccpDistance(pt2,touchStart)

										if let abutton = curPt.button.showhideLt
										{
											sizeButton = abutton;
										}

										if let abutton = curPt.button.delButtonNx
										{
											if (fillButton != nil)
											{
												cutButton = abutton;
											} else {
												fillButton = abutton;
											}
										}

										let distT:CGFloat = dist1+dist2;

//print("dist00 < bestDist",dist00,bestDist);
										if ((distT < dist0*1.01) && (dist00 < bestDist))
										{
											bestDist = dist00;
											pt0 = curPt.lt;
											lastWasLarger = false;
											if (dist1 > dist2) {lastWasLarger = true;}
										}
									}

									curPt = curPt.nx;
								}

								if (dragSquare)
								{
									let pt1:CGPoint = curPt.lt.pt;
									let pt2:CGPoint = curPt.pt;
									let bline:Bline = Bline(pt1,pt2);
									let bundlePt = findPerpPt_2D(touchStart, bline)

									if (bundlePt.wasGood)
									{
										let dist00:CGFloat = ccpDistance(touchStart,bundlePt.pt1)

										let dist0:CGFloat = ccpDistance(pt1,pt2)
										let dist1:CGFloat = ccpDistance(pt1,touchStart)
										let dist2:CGFloat = ccpDistance(pt2,touchStart)

										let distT:CGFloat = dist1+dist2;

										if ((distT < dist0*1.01) && (dist00 < bestDist))
										{
											bestDist = dist00;
											pt0 = curPt.lt;
											self.buttonV0 = theButtonV1
											lastWasLarger = false;
											if (dist1 > dist2) {lastWasLarger = true;}
										}
									}
								}
							}
						}

						if let pt00 = pt0
						{
							let nxp:Ppoint! = pt00.nx;

							pt00.nx = theButtonV1.ppt;
							theButtonV1.ppt.lt = pt00;
							theButtonV1.ppt.nx = nxp;
							nxp.lt = theButtonV1.ppt

							self.wasInterXPoint = true;
							self.interXButton = theButtonV1;

							if (dragSquare)
							{
								if let abutton = sizeButton
								{
									self.imageViewOverlay.addSubview(abutton)
								}
								if let abutton = lockButton
								{
									self.imageViewOverlay.addSubview(abutton)
								}
								if let abutton = cutButton
								{
							//		abutton.bringSubviewToFront(self.imageVie wOverlay)
									self.imageViewOverlay.addSubview(abutton)
								}
								if let abutton = fillButton
								{
							//		abutton.bringSubviewToFront(self.imageVie wOverlay)
									self.imageViewOverlay.addSubview(abutton)
								}

								if (lastWasLarger)
								{
									theButtonV1.delButtonLt = nxp.button.delButtonLt;
									theButtonV1.delButtonLt?.center = theButtonV1.findMidPtBut2D(theButtonV1.ppt.pt,theButtonV1.ppt.lt.pt);
									nxp.button.delButtonLt = nil;
								} else {
									theButtonV1.delButtonNx = pt00.button.delButtonNx;
									theButtonV1.delButtonNx?.center = theButtonV1.findMidPtBut2D(theButtonV1.ppt.pt,theButtonV1.ppt.nx.pt);
									pt00.button.delButtonNx = nil;
								}
							}

						} else {

							if (dragSquare)
							{
								theButtonV1.removeFromSuperview()
							} else {
								self.buttonV0?.ppt.nx = theButtonV1.ppt;
								theButtonV1.ppt.lt = self.buttonV0?.ppt;
								theButtonV1.ppt.nx = currentPaintImageController.theButtonV0?.ppt;
								currentPaintImageController.theButtonV0?.ppt.lt = theButtonV1.ppt

								self.buttonV0 = theButtonV1

								self.editDimButton.center   = CGPoint(x: touchStart.x + (maxDistance * 1.75),
																	  y: touchStart.y - (maxDistance * 1.75))
							}
						}
					}
				break;

			case 6:
					self.wasInterXPoint = false
					var bestDist:CGFloat = 999999.0;
					var pt0:Ppoint?;
					var lastWasLarger:Bool = false;
					var sizeButton:PECButton?
					var lockButton:PECButton?
					var cutButton:UIButton?
					var fillButton:UIButton?

					if let button = currentPaintImageController.theButtonV0
					{
						let ppft:Ppoint = button.ppt;

//print("0 button.sho whideLt",button.sho whideLt as Any)
						if let abutton = button.showhideLt
						{
							sizeButton = abutton;
						}

						if let abutton = button.lockLinkFirst
						{
							lockButton = abutton;
						}

						if let abutton = button.delButtonNx
						{
							fillButton = abutton;
						}

						let pt1:CGPoint = ppft.lt.pt;
						let pt2:CGPoint = ppft.pt;

						let dist0:CGFloat = self.findDistance2D(pt1,pt2)
						let dist1:CGFloat = self.findDistance2D(pt1,touchStart)
						let dist2:CGFloat = self.findDistance2D(pt2,touchStart)
						let distT:CGFloat = dist1+dist2;

						if (distT < dist0*1.01)
						{
							bestDist = distT;
							pt0 = ppft.lt;
							lastWasLarger = false;
							if (dist1 > dist2) {lastWasLarger = true;}
						}

						if var curPt = ppft.nx
						{
							while curPt != ppft
							{
								let pt1:CGPoint = curPt.lt.pt;
								let pt2:CGPoint = curPt.pt;

								let dist0:CGFloat = self.findDistance2D(pt1,pt2)
								let dist1:CGFloat = self.findDistance2D(pt1,touchStart)
								let dist2:CGFloat = self.findDistance2D(pt2,touchStart)

								if let abutton = curPt.button.showhideLt
								{
									sizeButton = abutton;
								}

								if let abutton = curPt.button.delButtonNx
								{
									if (fillButton != nil)
									{
										cutButton = abutton;
									} else {
										fillButton = abutton;
									}
								}

						//		if ((dist1 < sufaceButtonRadius) || (dist2 < sufaceButtonRadius))  {pt0 = nil; break;}

								let distT:CGFloat = dist1+dist2;

								if (distT > dist0*1.01) {curPt = curPt.nx; continue;}

								if (distT < bestDist)
								{
									bestDist = distT;
									pt0 = curPt.lt;
									lastWasLarger = false;
									if (dist1 > dist2) {lastWasLarger = true;}
								}

								curPt = curPt.nx;
							}
						}
					}

					//-------- there was a new point --------------
					if let pt00 = pt0
					{
						let nxp:Ppoint! = pt00.nx;

						let theButtonV2:PECButton = currentPaintImageController.cornerButton.pecDuplicate()!
						self.imageViewOverlay.addSubview(theButtonV2)

						if let abutton = cutButton
						{
					//		abutton.bringSubv iewToFront(self.imageViewOverlay)
							self.imageViewOverlay.bringSubviewToFront(abutton)
						}
						if let abutton = fillButton
						{
					//		abutton.bringSubv iewToFront(self.imageViewOverlay)
							self.imageViewOverlay.bringSubviewToFront(abutton)
						}
						if let abutton = sizeButton
						{
							self.imageViewOverlay.addSubview(abutton)
						}
						if let abutton = lockButton
						{
							self.imageViewOverlay.addSubview(abutton)
						}

						theButtonV2.center = touchStart;
						theButtonV2.ppt = Ppoint.init(pt:touchStart);
						theButtonV2.ppt.button = theButtonV2;
						theButtonV2.isSelected = pt00.button.isSelected;
						theButtonV2.deleteCornerRecog.isEnabled = true;
						theButtonV2.isForPaint = true;
						theButtonV2.tag = 11;

						theButtonV2.ppt.nx = nxp;
						theButtonV2.ppt.lt = pt00;
						pt00.nx = theButtonV2.ppt;
						nxp.lt  = theButtonV2.ppt;

					//	theButtonV2.ppt.pt = touchStart;

						if (lastWasLarger)
						{
							theButtonV2.delButtonLt = nxp.button.delButtonLt;
							theButtonV2.delButtonLt?.center = theButtonV2.findMidPtBut2D(theButtonV2.ppt.pt,theButtonV2.ppt.lt.pt);
							nxp.button.delButtonLt = nil;
						} else {
							theButtonV2.delButtonNx = pt00.button.delButtonNx;
							theButtonV2.delButtonNx?.center = theButtonV2.findMidPtBut2D(theButtonV2.ppt.pt,theButtonV2.ppt.nx.pt);
							pt00.button.delButtonNx = nil;
						}

						self.wasInterXPoint = true;
						self.interXButton = theButtonV2;

						currentPaintImageController.drawPaintLineAt();
						self.drawPaintCorners();
					}
				break;

			case 9:
					smallistDist = 9999999.0
				//	let limitToSurface30 = 30.0 / self.zoomScale
					let limitToSurface100 = 150.0 / self.zoomScale

					dotsArray.removeAll()

					var i:Int = -1;

					for thePt in self.curvePts
					{
						i += 1;
						let length:CGFloat = ccpDistance(thePt, touchStart);

						if length < limitToSurface100
						{
							if length < smallistDist
							{
								smallistDist = length
							}

							dotsArray.append(dotsGroup(i,length))
						}
					}
				break;

			default:
				break;
		}
	}

	//----------------------------------------------------------------------------------------------------------
	override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
	{
//print("touches Moved");
		super.touchesMoved(touches, with:event);

		switch (currentToolIndex)
		{
			case -1:
					touchStart = touches.first?.location(in:self.imageView) ?? CGPoint.zero
				break;

			case 21:
				break;

			case 1:
				break;

			case 11:
					let point:CGPoint = event?.allTouches?.first?.location(in:self.imageViewOverlay) ?? CGPoint.zero;

					if (self.wasInterXPoint)
					{
						if let button = self.interXButton
						{
							button.center = point;
							button.ppt.pt = point;
						}

					} else {
						self.editDimButton.center = CGPoint(x:point.x + (maxDistance * 1.75),
															y:point.y - (maxDistance * 1.75));

						self.buttonV0?.center = point;
						self.buttonV0?.ppt.pt = point;
					}

					currentPaintImageController.drawPaintLineAt()
					self.drawPaintCorners();

				break;

			case 2:
					let point:CGPoint = event?.allTouches?.first?.location(in:self.imageViewOverlay) ?? CGPoint.zero;
				//	let pointN = CGPoint(x:point.x - self.d2Button.touchOffset.x, y:point.y - self.d2Button.touchOffset.y);
					self.d2Button.center = point;

					self.editDimButton.center = CGPoint(x:self.d2Button.center.x + (maxDistance * 1.75),
														y:self.d2Button.center.y - (maxDistance * 1.75));

					if (self.farEnough2())
					{
						self.curvePts.append(point)
						lastPoint02 = point;
						self.drawPaintEraseAt()
					}

				break;

			case 3:
					let point:CGPoint = event?.allTouches?.first?.location(in:self.imageViewOverlay) ?? CGPoint.zero;
				//	let pointN = CGPoint(x:point.x - self.d2Button.touchOffset.x, y:point.y - self.d2Button.touchOffset.y);
					self.d2Button.center = point;

					self.editDimButton.center = CGPoint(x:self.d2Button.center.x + (maxDistance * 1.75),
														y:self.d2Button.center.y - (maxDistance * 1.75));

					if (self.farEnough2())
					{
						self.curvePts.append(point)
						lastPoint02 = point;
						self.drawPaintEraseAt()
					}

				break;

			case 4:
					let point:CGPoint = event?.allTouches?.first?.location(in:self.imageViewOverlay) ?? CGPoint.zero;
				//	let pointN = CGPoint(x:point.x - self.d2Button.touchOffset.x, y:point.y - self.d2Button.touchOffset.y);
					self.d2Button.center = point;

					self.editDimButton.center = CGPoint(x:self.d2Button.center.x + (maxDistance * 1.75),
														y:self.d2Button.center.y - (maxDistance * 1.75));

					if (self.farEnough2())
					{
						self.curvePts.append(point)
						lastPoint02 = point;
						self.drawPaintEraseAt()
					}

				break;

			case 5:
					let point:CGPoint = event?.allTouches?.first?.location(in:self.imageViewOverlay) ?? CGPoint.zero;


//print("00 touchesmoved self.imageViewOverlay:",self.imageViewOverlay as Any)
//print("00 touchesmoved point:",point as Any)

					if (self.wasInterXPoint)
					{
						if let button = self.interXButton
						{
							button.center = point;
							button.ppt.pt = point;
						}

					} else {
						self.editDimButton.center = CGPoint(x:point.x + (maxDistance * 1.75),
															y:point.y - (maxDistance * 1.75));

						self.buttonV0?.center = point;
						self.buttonV0?.ppt.pt = point;
					}
//print("00 touchesmoved self.buttonV0:",self.buttonV0 as Any)

					currentPaintImageController.drawPaintLineAt()
					self.drawPaintCorners();
				break;

			case 6:
					let point:CGPoint = event?.allTouches?.first?.location(in:self.imageViewOverlay) ?? CGPoint.zero;

					if (self.wasInterXPoint)
					{
						if let button = self.interXButton
						{
							button.center = point;
							button.ppt.pt = point;
						}
					}

					currentPaintImageController.drawPaintLineAt()
					self.drawPaintCorners();
				break;

			case 9:
					let touch:CGPoint = event?.allTouches?.first?.location(in:self.imageViewOverlay) ?? CGPoint.zero;
					let pushLengthX:CGFloat = (touch.x - touchStart.x) * 0.5;
					let pushLengthY:CGFloat = (touch.y - touchStart.y) * 0.5;

					touchStart = touch;

					for dots in self.dotsArray
					{
						let amount0 = smallistDist / dots.length
						let amount = amount0 * amount0

						self.curvePts[dots.index].x += (amount * pushLengthX)
						self.curvePts[dots.index].y += (amount * pushLengthY)
					}

					if (self.dotsArray.count > 0)
					{
						let dot0 = self.dotsArray[0]
//print("dot0.index",dot0.index)

						if (dot0.index == 0)
						{
							let amount0 = smallistDist / dot0.length
							let amount = amount0 * amount0

							self.d1Button.center.x += (amount * pushLengthX);
							self.d1Button.center.y += (amount * pushLengthY);
							self.deleteDimButton.center = CGPoint(x:self.d1Button.center.x + (maxDistance * 1.75),
																  y:self.d1Button.center.y - (maxDistance * 1.75));
						}

						if let dotLast = self.dotsArray.last
						{
//print("dotLast.index self.curv ePts.count-1",dotLast.index, self.cur vePts.count-1)

							if (dotLast.index == self.curvePts.count-1)
							{
								let amount0 = smallistDist / dotLast.length
								let amount = amount0 * amount0

								self.d2Button.center.x += (amount * pushLengthX);
								self.d2Button.center.y += (amount * pushLengthY);
								self.editDimButton.center = CGPoint(x:self.d2Button.center.x + (maxDistance * 1.75),
																	y:self.d2Button.center.y - (maxDistance * 1.75));
							}
						}
					}

					self.drawPaintEraseAt()
				break;

			default:
				break;
		}
	}

	//----------------------------------------------------------------------------------------------------------
	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
	{
//print("touches Ended currentToolIndex:",currentToolIndex);
		super.touchesEnded(touches, with:event);
		self.wasStart = false;
//print("touches Ended self.wasS tart:",self.wasStart);
//print("touches Ended currentToolIndex:",currentToolIndex);

		switch (currentToolIndex)
		{
			case -1:
				break;

			case 21:
				break;

			case 22:
				break;

			case 8669:
				self.wasStart = true;
				currentToolIndex = 5;
//print("86 69 touches Ended self.wasS tart:",self.wasStart);
				break;

			case 1:
				break;

			case 11:
					currentPaintImageController.drawPaintLineAt()
					self.drawPaintCorners();

					self.wasStart = false;
//print("11 touches Ended self.wasS tart:",self.wasStart);
				break;

			case 2:
					//-------  finish up path ---------
					currentToolIndex = 9;
				//	let point:CGPoint = event?.allTouches?.first?.location(in:self.imageViewOverlay) ?? CGPoint.zero;

					let touchedPt:CGPoint = self.d2Button.center;
					if let lastPt:CGPoint = self.curvePts.last
					{
						if ( !closeToXY(lastPt, touchedPt, 2.0))
						{
							self.curvePts.append(touchedPt);
						}

						self.drawPaintEraseAt()
					}
				break;

			case 3:
					//-------  finish up path ---------
					currentToolIndex = 9;

					let touchedPt:CGPoint = self.d2Button.center;
					if let lastPt:CGPoint = self.curvePts.last
					{
						if ( !closeToXY(lastPt, touchedPt, 2.0))
						{
							self.curvePts.append(touchedPt);
						}

						self.drawPaintEraseAt()
					}
				break;


			case 4:
					//-------  finish up path ---------
					currentToolIndex = 9;

					let touchedPt:CGPoint = self.d2Button.center;
					if let lastPt:CGPoint = self.curvePts.last
					{
						if ( !closeToXY(lastPt, touchedPt, 2.0))
						{
							self.curvePts.append(touchedPt);
						}

						self.drawPaintEraseAt()
					}
				break;

			case 5:
					//-------  finish up path ---------
					if let lastButton = self.buttonV0
					{
						if let firstButton = lastButton.ppt.lt.button
						{
							if (closeToXY(lastButton.center, firstButton.center, 5.0)) // 15
							{
								let ltp:Ppoint! = lastButton.ppt.lt;
								let nxp:Ppoint! = lastButton.ppt.nx;

								lastButton.removeFromSuperview()
								self.buttonV0 = firstButton

								ltp.nx = nxp;
								nxp.lt = ltp;
							}
						}
					}

					currentPaintImageController.drawPaintLineAt()
					self.drawPaintCorners();

// 					//****** change 0.0.22 no didn't need this but keep for possible useful later ***
// 					if let lastButton = self.but tonV0
//					{
//						let pointN = lastButton.center;
//
//						var needsEscape = false;
//						var moveBack:CGPoint = CGPoint.zero;
//
//						if (pointN.x < 0.0) {needsEscape = true; moveBack.x = 20.0-pointN.x;}
//						if (pointN.y < 0.0) {needsEscape = true; moveBack.y = 20.0-pointN.y;}
//						if (pointN.x > currentPaintImageController.scrollView.imageViewOverlay.frame.size.width)
//							{needsEscape = true; moveBack.x = -20.0 - (pointN.x-currentPaintImageController.scrollView.imageViewOverlay.frame.size.width);}
//						if (pointN.y > currentPaintImageController.scrollView.imageViewOverlay.frame.size.height)
//							{needsEscape = true; moveBack.y = -20.0 - (pointN.y-currentPaintImageController.scrollView.imageViewOverlay.frame.size.height);}
//
//						if (needsEscape)
//						{
//							lastButton.center.x += moveBack.x;
//							lastButton.center.y += moveBack.y;
//							lastButton.ppt.pt = lastButton.center;
//
//							self.editDimButton.center = CGPoint(x:lastButton.center.x + (maxDistance * 1.75),
//																y:lastButton.center.y - (maxDistance * 1.75));
//
//							currentPaintImageController.drawPaintLineAt()
//							self.drawPain tCorners();
//						}
//					}

					self.wasStart = false;
//print("5 touches Ended self.wasS tart:",self.wasStart);
				break;

			case 6:
				break;

			case 9:
				break;

			default:
				break;
		}
	}

	//----------------------------------------------------------------------------------------------------------
	override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?)
	{
//print("touches Cancelled");
		super.touchesCancelled(touches, with:event);

		if (currentToolIndex == 5)
		{
			if let lastButton = self.buttonV0
			{
				if let firstButton = lastButton.ppt.lt.button
				{
					let ltp:Ppoint! = lastButton.ppt.lt;
					let nxp:Ppoint! = lastButton.ppt.nx;

					lastButton.removeFromSuperview()
					self.buttonV0 = firstButton

					ltp.nx = nxp;
					nxp.lt = ltp;

					currentPaintImageController.drawPaintLineAt()
				}
			}
		}

		self.wasStart = false;
//print(" touches canceled self.wasS tart:",self.wasStart);
	}


	// MARK: - deActivate
	//-------------------------------------------------------------
	func farEnough2() -> Bool
	{
		let topPart:CGFloat = (self.d2Button.center.x - lastPoint02.x) * (self.d2Button.center.x - lastPoint02.x)
		let rgtPart:CGFloat = (self.d2Button.center.y - lastPoint02.y) * (self.d2Button.center.y - lastPoint02.y)
		let quadDistance:CGFloat = sqrt(topPart + rgtPart)
		return quadDistance > (maxDistance * maxDistExtra) ? true : false
	}

	//-------------------------------------------------------------
	func deActivateAllScrollView()
	{
		self.wasStart = false;
//print(" deActivateAllScrollView self.wasS tart:",self.wasStart);

		touchOffset = CGPoint.zero
		currentToolIndex = -1

		self.delaysContentTouches = true
	}

	//----------------------------------------------------------------------------------------------------------
	func deActivateAllall()
	{
		if (currentToolIndex != 8668)
		{
			currentToolIndex = -1;
		}
	}

	// MARK: - PECDimension
	//---------------------------------------------------------------------
	func drawPaintEraseAt()
	{
		if (self.curvePts.count < 2)
		{
			return
		}

		let tempArrayPath: UIBezierPath? = UIBezierPath.interpolateCGPointsWithHermite(self.curvePts, closed: false)

		if let tempArrayPath0 = tempArrayPath
		{
			currentPaintImageController.paintLayer.path = nil;
			currentPaintImageController.paintLayer.path = tempArrayPath0.cgPath;
		}
	}

	//-------------------------------------------------------------
	@IBAction func chooseToolSize(_ sender: Any)
	{
		var toolSizeAdjustment: CGFloat = CGFloat(currentPaintImageController.toolSizeSlider.value)

		if (currentPaintImageController.eraserButton.isSelected)
		{
			UserDefaults.standard.set(toolSizeAdjustment, forKey: "eraserToolSizeAdjustment")
		} else if (currentPaintImageController.penButton.isSelected) {
			UserDefaults.standard.set(toolSizeAdjustment, forKey: "toolSizeAdjustment")
		}
		UserDefaults.standard.synchronize();

		scalingOldToolSize = toolSizeAdjustment;
		toolSizeAdjustment = CGFloat(max(pow(toolSizeAdjustment, 3.0), 0.375))

		var theLineWidth = CGFloat(min(lineWidthDistance * toolSizeAdjustment * 3.0, 768.0))
		scalingLineWidth = theLineWidth;
		currentPaintImageController.paintLayer.lineWidth = theLineWidth

		theLineWidth = CGFloat(theLineWidth) * self.zoomScale * 1.333

		let buttonSize = CGSize(width: CGFloat(theLineWidth), height: CGFloat(theLineWidth))
		currentPaintImageController.d4Button.bounds = CGRect(origin:CGPoint.zero, size: buttonSize)
	}

	//-------------------------------------------------------------
	func reUpSlider()
	{
		currentPaintImageController.toolSizeSlider.isHidden = false
		currentPaintImageController.d4Button.isHidden = false

		var toolSizeAdjustment: CGFloat = 1.0

		if (currentPaintImageController.eraserButton.isSelected)
		{
			toolSizeAdjustment = CGFloat(UserDefaults.standard.float(forKey:"eraserToolSizeAdjustment"))
		} else if (currentPaintImageController.penButton.isSelected) {
			toolSizeAdjustment = CGFloat(UserDefaults.standard.float(forKey:"toolSizeAdjustment"))
		}

		scalingOldToolSize = toolSizeAdjustment;

		currentPaintImageController.toolSizeSlider.value = Float(toolSizeAdjustment)
		toolSizeAdjustment = CGFloat(max(pow(toolSizeAdjustment, 3.0), 0.375))

		var theLineWidth:CGFloat = CGFloat(min(lineWidthDistance * toolSizeAdjustment * 3.0, 768.0))
		scalingLineWidth = theLineWidth;
		currentPaintImageController.paintLayer.lineWidth = theLineWidth;

		theLineWidth = theLineWidth * self.zoomScale * 1.333

		let buttonSize = CGSize(width: CGFloat(theLineWidth), height: CGFloat(theLineWidth))
		currentPaintImageController.d4Button.bounds = CGRect(origin:CGPoint.zero, size: buttonSize)
	}


	// MARK: - layr bar
	//-------------------------------------------------------------
	@IBAction func layrDragExit(_ sender: Any, with event:UIEvent?)
	{
		UserDefaults.standard.set(layrBarCurrentOffset, forKey:"layrBarCurrentOffset");
		UserDefaults.standard.synchronize();
	}

	//-------------------------------------------------------------
	@IBAction func layrDrag(_ sender: Any, with event:UIEvent?)
	{
		var point:CGPoint = event?.allTouches?.first?.location(in:self) ?? CGPoint.zero;

		point = CGPoint(x: point.x - touchOffset.x, y: point.y - touchOffset.y)
		point.x = min(point.x, layrBarMax)
		point.x = max(point.x, layrBarMin)
		layrBarCurrentOffset = CGFloat(self.frame.size.width - point.x)
		self.layrBarButton.center = CGPoint(x: point.x, y: self.layrBarButton.center.y)
	//	self.layrPadView.center = CGPoint(x: (self.layrBarBut ton.center.x) + 111.0, y: self.layrPadView.center.y)

	//	let controlOffsetX = CGFloat(layrBarCurrentOffset - 16.0) * 1.4736842
		let controlOffsetX = CGFloat(layrBarCurrentOffset - 12.0) //18.0
	//	self.layrContr olView.center = CGPoint(x: (self.frame.size.width - CGFloat(controlOffsetX)) + 25.0, y: 122.0)
		self.layrControlView.center = CGPoint(x: (self.frame.size.width - CGFloat(controlOffsetX)) + 20.0, y: self.layrControlView.center.y) //25.0
	}

	// MARK: - pull bar
	//--------------------------------------------------------------------------------------------------------
	@IBAction func pullTouchDown(_ sender: Any, with event:UIEvent?)
	{
		let point:CGPoint = event?.allTouches?.first?.location(in:self) ?? CGPoint.zero;

		pingBarMax = self.frame.size.height - 18.0;
		pingBarMin = self.frame.size.height - 294.0;

		layrBarMax = self.frame.size.width - 4.0; // -16
		layrBarMin = self.frame.size.width - 58.0; // -70

		touchOffset = CGPoint.zero;

		if let control = sender as? UIButton
		{
			touchOffset = CGPoint(x:(point.x - control.center.x), y:(point.y - control.center.y));
		}
		wasPullDragging = false;
	}

	//--------------------------------------------------------------------------------------------------------
	@IBAction func pullUpInside(_ sender: Any, with event:UIEvent?)
	{
		let defaults = UserDefaults.standard
		if (wasPullDragging)
		{
			defaults.set(pullBarCurrentOffset, forKey:"pullBarCurrentOffset");
			defaults.synchronize();
			return;
		}

		var pullBarXPosition:CGFloat = 0.0;
		var xPosition:CGFloat = 0.0;
		var pullDirection:CGFloat = 0.0;

			 if (self.pullBarButton.center.x >= (pullBarCenterX + 40.0)) {xPosition =  pullBarCenterX;}
		else if (self.pullBarButton.center.x <= (pullBarCenterX - 88.0)) {xPosition = (pullBarCenterX - 50.0);}
		else if (touchOffset.x > 0.0) {pullDirection = 50.0;}
		else {pullDirection = -50.0;}

		if (pullDirection == 50.0)
			{xPosition = self.pullBarButton.center.x + 50.0;}
		else if (pullDirection == -50.0)
			{xPosition = self.pullBarButton.center.x - 50.0;}

			 if  (xPosition <= ((pullBarCenterX + (-2 * 50)) + 25.0)) {pullBarXPosition = (pullBarCenterX + (-2 * 50));}
		else if ((xPosition >  ((pullBarCenterX + (-1 * 50)) - 25.0)) && (xPosition <=  ((pullBarCenterX + (-1 * 50)) + 25.0))) {pullBarXPosition = (pullBarCenterX + (-1 * 50));}
		else if ((xPosition >  ((pullBarCenterX + ( 0 * 50)) - 25.0)) && (xPosition <=  ((pullBarCenterX + ( 0 * 50)) + 25.0))) {pullBarXPosition = (pullBarCenterX + ( 0 * 50));}
		else if  (xPosition >  ((pullBarCenterX + ( 1 * 50)) - 25.0)) {pullBarXPosition = (pullBarCenterX + ( 1 * 50));}

		pullBarCurrentOffset = pullBarXPosition - pullBarCenterX;
		defaults.set(pullBarCurrentOffset, forKey:"pullBarCurrentOffset");
		defaults.synchronize();

		self.pullBarButton.center	= CGPoint(x:pullBarXPosition					, y:self.pullBarButton.center.y);
		self.touchPadView.center	= CGPoint(x:self.pullBarButton.center.x - touchPadCenterX	, y:self.touchPadView.center.y);
	}

	//--------------------------------------------------------------------------------------------------------
	@IBAction func pullDragInside(_ sender: Any, with event:UIEvent?)
	{
		var point:CGPoint = event?.allTouches?.first?.location(in:self) ?? CGPoint.zero;
		point = CGPoint(x:(point.x - touchOffset.x), y:(point.y - touchOffset.y));

		point.x = min(point.x, pullBarMax);
		point.x = max(point.x, pullBarMin);
		wasPullDragging = true;
		pullBarCurrentOffset = point.x - pullBarCenterX;

		self.pullBarButton.center = CGPoint(x:point.x, y:self.pullBarButton.center.y);
		self.touchPadView.center  = CGPoint(x:self.pullBarButton.center.x - touchPadCenterX, y:self.touchPadView.center.y);
	}

	//--------------------------------------------------------------------------------------------------------
	@IBAction func pullDragOutside(_ sender: Any, with event:UIEvent?)
	{
		var point:CGPoint = event?.allTouches?.first?.location(in:self) ?? CGPoint.zero;
		point = CGPoint(x:(point.x - touchOffset.x), y:(point.y - touchOffset.y));

		point.x = min(point.x, pullBarMax);
		point.x = max(point.x, pullBarMin);
		wasPullDragging = true;
		pullBarCurrentOffset = point.x - pullBarCenterX;

		self.pullBarButton.center = CGPoint(x:point.x, y:self.pullBarButton.center.y);
		self.touchPadView.center  = CGPoint(x:self.pullBarButton.center.x - touchPadCenterX, y:self.touchPadView.center.y);
	}

	//--------------------------------------------------------------------------------------------------------
	@IBAction func pullUpOutside(_ sender: Any, with event:UIEvent?)
	{
		if (wasPullDragging)
		{
			let defaults = UserDefaults.standard
			defaults.set(pullBarCurrentOffset, forKey:"pullBarCurrentOffset");
			defaults.synchronize();
		}
	}


	// MARK: - UIScrollViewDelegate
	//----------------------------------------------------------------------------------------------------------
	override func touchesShouldCancel(in view: UIView) -> Bool
	{
		return true;
	}

	//----------------------------------------------------------------------------------------------------------
	func viewForZooming(in scrollView: UIScrollView) -> UIView?
	{
		return self.containerView;
	}

	//----------------------------------------------------------------------------------------------------------
	func configureForImageSize()
	{
//print("1 configureForImageSize  self.contentOffset:",self.contentOffset);
//print("1 configureForImageSize  skipCon figure:",skipConfigure);
		if (skipConfigure)
		{
			self.zoomScale = curZoom;
			self.contentSize = curSize;
			self.contentOffset = curOffset;
			self.containerView.frame = CGRect(x:0.0, y:0.0, width:curSize.width, height:curSize.height)
			skipConfigure = false;
		} else {
			self.setMaxMinZoomScalesForCurrentBounds();
			self.zoomScale = self.minimumZoomScale;
			self.firstIn = true;
		}
//print("   ");
//print("configureForImageSize self.imageContainView.frame:",self.imageContainView.frame);
//print("configureForImageSize self.containe rView.frame:",self.containerView.frame);
//print("configureForImageSize self.conten tSize:",self.contentSize);
	}

	//----------------------------------------------------------------------------------------------------------
	func setMaxMinZoomScalesForCurrentBounds()
	{
		let boundsSize:CGSize = self.bounds.size;
		self.contentSize = CGSize(width:containWidthImage, height:containHeightImage); 
		self.imageView.frame = CGRect(x:0.0, y:0.0, width:self.imageView.frame.size.width, height:self.imageView.frame.size.height);
		self.imageContainView.frame = self.imageView.frame;
//print("   ");
//print("   ");
//print("setMaxMinZo omScalesForCurrentBounds self.imageContainView.frame:",self.imageContainView.frame);
//print("setMaxMinZo omScalesForCurrentBounds self.contai nerView.frame:",self.containerView.frame);
//print("setMaxMinZo omScalesForCurrentBounds self.conte ntSize:",self.contentSize);

		self.paintImageOverlay0.frame = self.imageView.frame;
		self.paintImageOverlay1.frame = self.imageView.frame;
		self.paintImageOverlay2.frame = self.imageView.frame;
		self.paintImageOverlay3.frame = self.imageView.frame;
		self.paintImageOverlay4.frame = self.imageView.frame;

        self.garageImageView.frame = self.imageView.frame;
        self.garageMaskView.frame = self.imageView.frame;
        self.frontDrMaskView.frame = self.imageView.frame;

		self.imageViewOverlay.frame = self.imageView.frame;

		let xScale:CGFloat = boundsSize.width  / containWidthImage;    // the scale needed to perfectly fit the image width-wise
		let yScale:CGFloat = boundsSize.height / containHeightImage;   // the scale needed to perfectly fit the image height-wise
//print(" containWidthImage,containHeightImage:",containWidthImage,containHeightImage);

		var minScale:CGFloat = min(xScale, yScale);

		// on high resolution screens we have double or triple the pixel density,
		// so we will be seeing every pixel if we limit the maximum zoom scale to 0.5 or 0.333.
		var maxScale:CGFloat = 1.0 / UIScreen.main.scale;

		// don't let minScale exceed maxScale. (If the image is smaller than the screen,
		// we don't want to force it to be zoomed.)
		if (minScale > maxScale)
		{
			minScale = maxScale;
		}
		else if (minScale < 0.001)
		{
			minScale = maxScale;
		}


		maxScale = minScale * 40.0;
//print(" maxScale,minScale:",maxScale,minScale);

		self.maximumZoomScale = maxScale; //8.0 0.25
		self.minimumZoomScale = minScale;
		self.cutOffX = (0.0125 * containWidthImage)
//print("1 self.cutOffX,self.cutOffY:",self.cutOffX,self.cutOffY);
		if ( !phonePortrait)
		{
			self.cutOffX = (0.0125 * containHeightImage)
		}
//print("2 self.cutOffX,self.cutOffY:",self.cutOffX,self.cutOffY);


		let ratio =  self.frame.size.height / self.frame.size.width;

		if (origFrameWidth < 4.0)
		{
			origFrameWidth = self.frame.size.width;

			if (ratio < 1.0)
			{
				origFrameWidth = self.frame.size.height;
			}
		}

		self.cutOffY = self.cutOffX / self.frame.size.width

		if (ratio < 1.0)
		{
			self.cutOffY = self.cutOffX / self.frame.size.height
		}
//print("3 self.cutOffX,self.cutOffY:",self.cutOffX,self.cutOffY);
	}

	// MARK: - set up
	//----------------------------------------------------------------------------------------------------------
	func configureButtonSizes()
	{
		reverseZoomFactor = 1.0 / self.zoomScale
		maxDistance = reverseZoomFactor * distScaleFactor
		lineWidthDistance = CGFloat(max(reverseZoomFactor * dimLineFactor, 0.375))

		let sufaceButtonWidth = 50.0 * reverseZoomFactor
		let markerButtonWidth = 40.0 * reverseZoomFactor
		let magnifyOff = 75.0 * reverseZoomFactor

		let surfaceButtonSize = CGSize(width: CGFloat(sufaceButtonWidth), height: CGFloat(sufaceButtonWidth))
		let markerButtonSize = CGSize(width: CGFloat(markerButtonWidth), height: CGFloat(markerButtonWidth))
//		let magnifySize = CGSize(width: CGFloat(magnifyWidth), height: CGFloat(magnifyWidth))

		magnifyOffset = CGPoint(x: magnifyOff, y:magnifyOff);

		self.d1Button.bounds = CGRect(origin:CGPoint.zero, size: surfaceButtonSize)
		self.d2Button.bounds = CGRect(origin:CGPoint.zero, size: surfaceButtonSize)
//		self.d3Button.bounds = CGRect(origin:CGPoint.zero, size: surfaceButtonSize)

		self.deleteDimButton.bounds = CGRect(origin:CGPoint.zero, size: markerButtonSize)
		self.editDimButton.bounds = CGRect(origin:CGPoint.zero, size: markerButtonSize)
	}

	//----------------------------------------------------------------------
	func setUpColorWheelImage()
	{
//print("setUp Color Wheel mage")
		var a:CGFloat =  1.0
		var angle:CGFloat =  0.0
		if (defaultLineColor.getHue(&hueNormal00, saturation:&sat00, brightness:&lum00, alpha:&a))
		{
        	self.touchPadView.sat = sat00;
			self.touchPadView.lum = lum00;

        	angle = (mpi2 * hueNormal00);
        	self.touchPadView.hue = angle;
		}

		currentPaintImageController.gradientViewSaturation.satFrameOnly(sat00);
		currentPaintImageController.gradientViewLuminosity.lumFrameOnly(lum00);

		self.touchPadView.startColor = origStartColor.copy() as? UIColor

		var origLum:CGFloat = lum00;
		var origSat:CGFloat = sat00;

		var hs, ss, ls:CGFloat
			ss = 1.0;
			ls = 0.5;
			hs = 0.15;
			a = 1.0;

        self.touchPadView.startColor.getHue(&hs, saturation:&ss, brightness:&ls, alpha:&a)

		self.touchPadView.topLum = maxLum;
		self.touchPadView.lumStep = 0.10;

		self.touchPadView.topSat = maxSat;
		self.touchPadView.satStep = 0.20;
		self.touchPadView.satBigStep = 2.0;

		var adjLum:CGFloat = 0.0;
		var flagLum:Int = 0;

		if (ls < 0.58)
		{
			let lumScale = CGFloat(ls / self.touchPadView.topLum)
			let brightness:CGFloat = 1.0 - lumScale;

			if (brightness > 0.001)
			{
				adjLum = brightness;

				if (brightness >= 0.5) {adjLum = 0.5 + ((brightness - 0.5) * 1.15);}

				let adjSat = 1.0 - (adjLum * 0.25);
				adjLum = 1.0 - (adjLum * 0.5);

				self.touchPadView.topLum *= adjLum
				self.touchPadView.lumStep =  ((self.touchPadView.topLum / maxLum) * 0.10) * 0.75;

				self.touchPadView.topSat *= adjSat
				self.touchPadView.satStep =  (self.touchPadView.topSat / maxSat) * 0.20;
				self.touchPadView.satBigStep = 1.25;

				let orgAdjLum = (maxLum / self.touchPadView.topLum);
				origLum *= orgAdjLum;

				let orgAdjSat = (maxSat / self.touchPadView.topSat);
				origSat *= orgAdjSat;
				flagLum = 1
			}
		}

		if (angle >  CGFloat.pi) {angle = -CGFloat.pi + (angle-CGFloat.pi);}
		if (angle < -CGFloat.pi) {angle =  CGFloat.pi + (angle+CGFloat.pi);}

		self.touchPadView.currentAngle = angle;
		self.touchPadView.lastAngle = angle;

        self.touchPadView.firstRotateToIndexUsingAngle(angle);
		currentPaintImageController.valuesChanged();
		self.touchPadView.updateData()

		var satSpot:Int = -2;
		var satDist:CGFloat = abs(origSat - 1.00);

		if abs(origSat - 0.60) < satDist
		{
			satSpot = 1;
			satDist = abs(origSat - 0.60)
		}

		if abs(origSat - 0.40) < satDist
		{
			satSpot = 0;
		}


		var lumSpot:Int = 6;
		var lumDist:CGFloat = abs(origLum - 0.70);

		if abs(origLum - 0.60) < lumDist
		{
			lumSpot = 5;
			lumDist = abs(origLum - 0.60);
		}

		if abs(origLum - 0.50) < lumDist
		{
			lumSpot = 4;
			lumDist = abs(origLum - 0.50);
		}

		if abs(origLum - 0.40) < lumDist
		{
			lumSpot = 3;
			lumDist = abs(origLum - 0.40);
		}

		if abs(origLum - 0.30) < lumDist
		{
			lumSpot = 2;
		}

		// ---------- final spot ------
		lumSpot += satSpot;
		if (lumSpot < 5) {lumSpot -= flagLum;}

		lumSpot = min(max(lumSpot,1),7)

		let distSpotf:CGFloat =  ((69.65776 + (CGFloat(lumSpot+1) * 33.79278)) - 16.89639) + 340.0;

		self.touchPadView.indicator2.center = CGPoint(x:distSpotf,y:340.0);
		self.touchPadView.indicator2SpinnerView.layer.transform = CATransform3DMakeRotation(-self.touchPadView.currentAngle-mpiDiv6, 0, 0, 1);
	}

	//----------------------------------------------------------------------------------------------------------
	func displayImageWithSize()
	{
//		hidingViewWasPreferenceContoller = false;
		self.configureForImageSize();
		self.wasStart = false;
//print(" displayImageWithSize self.wasS tart:",self.wasStart);
		let defaults = UserDefaults.standard
		self.configureButtonSizes();

		pullBarMin = 20.0;

//		if ( !hidingViewWasPrefe renceContoller)
//		{
			//------------- pad views setup ---------------
			pullBarCenterX = self.pullBarButton.center.x;

			pingBarMax = self.frame.size.height - 18.0;
			pingBarMin = self.frame.size.height - 294.0;

			layrBarMax = self.frame.size.width - 4.0; //12
			layrBarMin = self.frame.size.width - 58.0; //72


			pullBarCurrentOffset = CGFloat(defaults.float(forKey: "pullBarCurrentOffset"));
			let pullBarLimit = max(min(pullBarCenterX + pullBarCurrentOffset, pullBarMax),pullBarMin);
			pullBarCurrentOffset = pullBarLimit - pullBarCenterX;

			self.pullBarButton.center	= CGPoint(x:pullBarCenterX + pullBarCurrentOffset , y:currentPaintImageController.mainBaseView.frame.height - pullBarDist);
			self.touchPadView.center	= CGPoint(x:self.pullBarButton.center.x - touchPadCenterX	, y:self.touchPadView.center.y);

			pingBarCurrentOffset = CGFloat(defaults.float(forKey: "pingBarCurrentOffset"));
			layrBarCurrentOffset = CGFloat(defaults.float(forKey: "layrBarCurrentOffset"));

			self.deActivateAllall();
//		} else {
//			self.deActivateAllall();
//		}

		var currentLayrOffset:CGFloat = self.frame.size.width - layrBarCurrentOffset;
		currentLayrOffset = min(currentLayrOffset, layrBarMax);
		currentLayrOffset = max(currentLayrOffset, layrBarMin);

		layrBarCurrentOffset = self.frame.size.width - currentLayrOffset;

		self.layrBarButton.center = CGPoint(x:currentLayrOffset, y:self.layrBarButton.center.y);
		let controlOffsetX:CGFloat = (layrBarCurrentOffset - 12.0); //18

		self.layrControlView.center = CGPoint(x:(self.frame.size.width - controlOffsetX) + 20.0, y:self.layrControlView.center.y); //25

//		hidingViewWasPrefe renceContoller = false;
	}
}
