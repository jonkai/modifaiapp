import Foundation
import UIKit
import PECimage


//--------------------------------------------------------------------------------------
class ImageScrollView: UIScrollView, UIScrollViewDelegate, UIGestureRecognizerDelegate
{
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageContainView: UIView!
    @IBOutlet weak var containerView: UIView!

    @IBOutlet weak var paintImageView: UIImageView!
    @IBOutlet weak var paintImageView1: UIImageView!
    @IBOutlet weak var paintImageView2: UIImageView! //*************** change 0.0.40 add *** (storyboard must also include)

    @IBOutlet weak var garageImageView: UIImageView!
//    @IBOutlet weak var garageMa skView: UIImageView!
//    @IBOutlet weak var frontDrMa skView: UIImageView!
    @IBOutlet weak var imageViewOverlay: UIImageView!

    @IBOutlet var dblTapLineRecog: UITapGestureRecognizer!

    var cutOffX:CGFloat = 757.0;
    var cutOffY:CGFloat = 568.2;
    var maxOffsetX:CGFloat = 343.0
    var maxOffsetY:CGFloat = 1470.0

    var origFrameWidth:CGFloat = 2.0
    var wasLandscape:Bool = false;
    var firstIn:Bool = false;

    var reverseZoomFactor:CGFloat = 1.0;
    var maxDistance:CGFloat = 1.0;
    var lineWidthDistance:CGFloat = 1.0;
    let distScaleFactor:CGFloat = 19.0;
    let dimLineFactor:CGFloat = 3.0;

    var magnifyOffset:CGPoint = CGPoint();

    var magnifyMask:CAShapeLayer = CAShapeLayer();
    var magnifyTarget:CAShapeLayer = CAShapeLayer();
    var magnifyRect:CGRect = CGRect();
    var magnifyOriginalFrame:CGRect = CGRect();

//Dutch corners

    // MARK: - gesture
    //-----------------------------------------------------------------------
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool
    {
//print("UIScrollViewDelegate gestureRecognizer ")
        self.delaysContentTouches = true;
        return true;
    }

    //----------------------------------------------------------------------------
    override func layoutSubviews()
    {
//print("6 self.imageV iew.frame    : \(self.imageView.frame)")
        super.layoutSubviews();

        if (viewTransition)
        {
            self.setMaxMinZoomScalesForCurrentBounds()
            if (self.zoomScale < self.minimumZoomScale)
            {
                self.zoomScale = self.minimumZoomScale;
                self.contentOffset = CGPoint.init(x:0.0, y:0.0)
                self.setContentOffset(self.contentOffset, animated:false)
            }
        }

        //-------------------- image frame -----------------------------
        var frameToCenter:CGRect = self.containerView.frame;
//print("0 frameToCenter",frameToCenter)

        if (frameToCenter.size.width < self.bounds.size.width)
        {
            frameToCenter.origin.x = (self.bounds.size.width - frameToCenter.size.width) / 2.0;

//print("  ")
//print("  ")
//print("0b phonePortrait",phonePortrait)
//print("0b frameToCenter",frameToCenter)
            if ( !phonePortrait)
            {
                frameToCenter.origin.x = 0.0;
            }

        } else {
            frameToCenter.origin.x = 0.0;
        }
//print("1 frameToCenter",frameToCenter)
        frameToCenter.origin.y = 0.0;

//print(" ")
//print("2 self.contentOffset      : \(self.contentOffset) phonePortrait:\(phonePortrait)")

        maxOffsetX = ((self.contentSize.width / self.cutOffX) * origFrameWidth) - 71.0
//print(" ")
//print("2 cutOffX", self.cutOffX);
//print("2 maxOffsetX",maxOffsetX)
//print("2 origFrameWidth",origFrameWidth)

        maxOffsetY = ((self.contentSize.height / self.cutOffY) - 125.0)
//print(" ")
//print("2 cutOffY", self.cutOffY);
//print("2 maxOffsetY",maxOffsetY)

//print("10 self.contentOffset",self.contentOffset)

        if (origFrameWidth > 4.0)
        {
            self.contentOffset = CGPoint.init(x:min(self.contentOffset.x,maxOffsetX),
                                              y:min(self.contentOffset.y,maxOffsetY))

//print("11 self.contentOffset",self.contentOffset)
//print("11 firstIn",firstIn)
//print("11 UIDevice.current.hasNotch",UIDevice.current.hasNotch)

            if UIDevice.current.hasNotch
            {
                if (phonePortrait)
                {
                    if (firstIn || (self.zoomScale < (self.minimumZoomScale * 1.02) && self.contentOffset.y >= 0.0))
                    {
//print("11 phonePortrait doing self.contentOffset")
                        self.contentOffset = CGPoint.init(x:0.0, y:0.0)  //-44.0
                        self.setContentOffset(self.contentOffset, animated:false)
                        firstIn = false;
                    }
                } else {
                    if (firstIn || (self.zoomScale < (self.minimumZoomScale * 1.02) && self.contentOffset.x >= 0.0))
                    {
//print("11 landscape doing self.contentOffset")
                        self.contentOffset = CGPoint.init(x:-44.0, y:0.0)
                        self.setContentOffset(self.contentOffset, animated:false)

                        firstIn = false;
                    }
                }
            }
        }

//print(" ")
//print(" ")
//print("2 frameToCenter",frameToCenter)
//print("3 self.frame              : \(self.frame)")
//print("3 self.containerView.frame: \(self.containerView.frame)")
//print("3 self.imageV iew.frame    : \(self.imageV iew.frame)")
//print("3 self.conten tSize        : \(self.conten tSize)")
//print("3 self.contentOffset      : \(self.contentOffset)")
//print(" ")
//print("3 self.zoomScale          : \(self.zoomScale)")
//print("3 self.maximumZoomScale   : \(self.maximumZoomScale)")
//print("3 self.minimu mZoomScale   : \(self.minim umZoomScale)")
//print(" ")
//print(" ")
//print(" ")
//print(" ")

        self.containerView.frame = frameToCenter;
//print("4 self.contai nerView.frame",self.container View.frame)

        self.imageContainView.frame.origin = CGPoint.zero;
    //    self.imageCon tainView.frame.origin.y = 0.0;

        self.imageView.frame.origin = CGPoint.zero;
		self.paintImageView.frame.origin = CGPoint.zero;
		self.paintImageView1.frame.origin = CGPoint.zero;
		self.paintImageView2.frame.origin = CGPoint.zero;  //***************** change 0.0.40 add ***
        self.imageViewOverlay.frame.origin = CGPoint.zero;


        if (hasOnlyGarageDoors) //********** change 1.0.0 add if block ***
        {
            self.paintImageView.isHidden = true
            self.paintImageView1.isHidden = true
            self.paintImageView2.isHidden = true
        }


        viewTransition = false;
//print("4 layoutSubviews viewTra nsition:  dontR otate: ",viewTra nsition,dontRotate)

        //-------------- for editing Doors ---------------------
        reverseZoomFactor = 1.0 / self.zoomScale;

        maxDistance = reverseZoomFactor * distScaleFactor;
////BLog(@"reverseZoomFactor: %f",reverseZoomFactor);
////BLog(@"max Distance: %f",max Distance);
        lineWidthDistance = max(reverseZoomFactor * dimLineFactor,0.375);

        //*********** change 1.4.5 added theImageScale scale factor below in several places ***
        let lineWidth = 10.0 * min(theImageScale*1.25,1.0);

        if (reverseZoomFactor > 5.5)
        {
            polyLayer.lineWidth = min(lineWidth * reverseZoomFactor,28.0*theImageScale);
        } else if (reverseZoomFactor > 3.75) {
            polyLayer.lineWidth = min(lineWidth * reverseZoomFactor,20.0*theImageScale);
        } else if (reverseZoomFactor > 2.5) {
            polyLayer.lineWidth = min(lineWidth * reverseZoomFactor,15.0*theImageScale);
        } else if (reverseZoomFactor > 1.0) {
            polyLayer.lineWidth = min(lineWidth * reverseZoomFactor,10.0*theImageScale);
        } else {
            polyLayer.lineWidth = min(lineWidth * reverseZoomFactor,6.0*theImageScale);
        }
//print("reverseZo omFactor polyLa yer.lineWidth:",reverseZo omFactor, polyLa yer.lineWidth)

        let magnifyWidth:CGFloat = 100.0 * reverseZoomFactor;
        let magnifyOff:CGFloat = 75.0 * reverseZoomFactor;
        magnifyTarget.lineWidth = 1.2 * reverseZoomFactor;

        let magnifySize:CGSize = CGSize(width: magnifyWidth, height: magnifyWidth);
        magnifyOffset = CGPoint(x:magnifyOff, y: magnifyOff);

        magnifyRect = CGRect(x:0.0, y:0.0, width:magnifySize.width, height:magnifySize.height);

        self.drawPolyCorners();
    }

    //-----------------------------------------------------------------------------------
    @IBAction func dblTapAction(_ recognizer: UITapGestureRecognizer)
    {
//print("dblTapAction recognizer",recognizer)
        switch (recognizer.state)
        {
            case UIGestureRecognizer.State.began:
//print("dblTapAction State.began recognizer",recognizer)
            break;

            case UIGestureRecognizer.State.changed:
            break;

            case UIGestureRecognizer.State.ended:

                    var bestDist:CGFloat = 999999.0;
                    var pt0:Ppoint?;
                    var respItem0:Response?;
                    let recPt:CGPoint = recognizer.location(in:self.imageViewOverlay);

				//	var sizeButton:PECButton?  //*************** change 0.0.15 add ***
					var lockButton:PECButton?  //*************** change 0.0.15 add ***


                    if let ho = houseObject
                    {
                        for resp in ho.response
                        {
                            for respItem in resp
                            {
                            	lockButton = nil;  //*************** change 0.0.15 add ***
								if let button = respItem.p1.button  //*************** change 0.0.15 add if block ***
								{
									if let abutton = button.lockLinkFirst
									{
										lockButton = abutton;
									}
								}

                                if (respItem.wasPoly) {break;}
                                if (respItem.p1.nx == nil){break;} //*************** change 2.1.8 add ***
                           //     if (respItem.wasLocked) {break;}  //*************** change 0.0.15 remove ***
                                if (respItem.wasArch)
                                {
                                    let pt1:CGPoint = respItem.p1.pt;
                                    let pt2:CGPoint = respItem.p1.nx.pt;

                                    var dist0:CGFloat = findDistance2D(pt1,pt2)
                                    var dist1:CGFloat = findDistance2D(pt1,recPt)
                                    var dist2:CGFloat = findDistance2D(pt2,recPt)

                                    if ((dist1 < sufaceButtonRadius) || (dist2 < sufaceButtonRadius))  {break;}

                                    var distT:CGFloat = dist1+dist2;

                                    if (distT < dist0*1.12)
                                    {
                                        if (distT < bestDist)
                                        {
                                            bestDist = distT;
                                            pt0 = respItem.p1;
                                            respItem0 = respItem;
                                        }
                                    }

                                    let pt3:CGPoint = respItem.p1.nx.nx.pt;

                                    dist0 = findDistance2D(pt2,pt3)
                                    dist1 = findDistance2D(pt2,recPt)
                                    dist2 = findDistance2D(pt3,recPt)

                                    if ((dist1 < sufaceButtonRadius) || (dist2 < sufaceButtonRadius))  {break;}

                                    distT = dist1+dist2;

                                    if (distT < dist0*1.12)
                                    {
                                        if (distT < bestDist)
                                        {
                                            bestDist = distT;
                                            pt0 = respItem.p1.nx;
                                            respItem0 = respItem;
                                        }
                                    }
                                } else {
                                    let pt1:CGPoint = respItem.p1.pt;
                                    let pt2:CGPoint = respItem.p1.nx.pt;

                                    let dist0:CGFloat = findDistance2D(pt1,pt2)
                                    let dist1:CGFloat = findDistance2D(pt1,recPt)
                                    let dist2:CGFloat = findDistance2D(pt2,recPt)

                                    if ((dist1 < sufaceButtonRadius) || (dist2 < sufaceButtonRadius))  {break;}
//print("  ")
//print("dist1,dist2",dist1,dist2)

                                    let distT:CGFloat = dist1+dist2;

                                    if (distT > dist0*1.12) {break;}

                                    if (distT < bestDist)
                                    {
                                        bestDist = distT;
                                        pt0 = respItem.p1;
                                        respItem0 = respItem;
                                    }
                                }
                            }
                        }
                    }


					//-------- there was a new point --------------
                    if let respItem00 = respItem0
                    {
                        if let pt00 = pt0
                        {
                        	shouldSave = true;  //********** change 0.0.40 add ***

                    //        currentMainImageController.kountDrs = 0;   //*************** change 0.0.15 remove lines ***
//                            currentMainImageController.polyPath.removeAllPoints();
//                            polyLayer.path = nil;

                            if (respItem00.wasArch)
                            {
                                respItem00.wasPoly = true;
                                respItem00.wasArch = false;
                            } else  {
                                respItem00.wasArch = true;
                            }

                            let nxp:Ppoint! = pt00.nx;

                            let theButtonV2:PECButton = currentMainImageController.cornerButton.pecDuplicate()!
                            self.imageViewOverlay.addSubview(theButtonV2)

							if let abutton = lockButton   //*************** change 0.0.15 add if block ***
							{
								self.imageViewOverlay.addSubview(abutton)
							}


                            theButtonV2.center = recPt;
                            theButtonV2.ppt = Ppoint.init();
                            theButtonV2.ppt.button = theButtonV2;
                            theButtonV2.deleteCornerRecog.isEnabled = true;
                            theButtonV2.respItem = respItem00;

                            theButtonV2.ppt.nx = nxp;
                            theButtonV2.ppt.lt = pt00;
                            pt00.nx = theButtonV2.ppt;
                            nxp.lt  = theButtonV2.ppt;

                            theButtonV2.ppt.pt = recPt;

							respItem00.p6 = respItem00.p5    //*************** change 0.0.15 add 17 lines below ***
							respItem00.p5 = respItem00.p4
							respItem00.p4 = respItem00.p3

							if (respItem00.wasArch)
							{
								respItem00.p3 = respItem00.p2
								respItem00.p2 = theButtonV2.ppt
							} else {
								if (pt00 == respItem00.p2)
								{
									respItem00.p3 = theButtonV2.ppt
								} else {
									respItem00.p3 = respItem00.p2
									respItem00.p2 = theButtonV2.ppt
								}
							}

							if let mainCont = currentMainImageController
							{
								mainCont.drawDoorObjectAt()
								self.drawPolyCorners();
							}
                        }
                    }

           break;

            case UIGestureRecognizer.State.cancelled:
            break;

            case UIGestureRecognizer.State.failed:
            break;

               case UIGestureRecognizer.State.possible:
            break;

            default:
            break;
        }
    }

    //----------------------------------------------------------------------------------------------------------
    func drawPolyCorners()
    {
        let cornerWidth = min(theImageScale*1.75,1.0); //1.95
        let width:CGFloat = ((1.0084135710597*(reverseZoomFactor * reverseZoomFactor)) - (18.012331804514*reverseZoomFactor) + 130.0) * cornerWidth;
        let surfaceButtonWidth:CGFloat = max((width * 1.2) * reverseZoomFactor,(75.0 * cornerWidth));
        let deleteButtonWidth:CGFloat = surfaceButtonWidth * 0.33;
		let lockButtonWidth:CGFloat = surfaceButtonWidth * 0.75;  //*************** change 0.0.15 add/ correct name ***
        sufaceButtonRadius = surfaceButtonWidth * 0.5;

//print("  ")
//print("theImageScale",theImageScale)
//print("sufaceButtonWidth",sufaceButtonWidth)
//print("self.zoomScale:,width",self.zoomScale,width)
//print("reverseZoomFactor sufaceButtonWidth:",reverseZoo mFactor, sufaceButtonWidth)

        for subView in self.imageViewOverlay.subviews
        {
            if let button:PECButton = subView as? PECButton
            {
				if (button.isLock)      //*************** change 0.0.15 add if block  ***
				{
					button.bounds = CGRect(x:0.0, y:0.0, width:lockButtonWidth, height:lockButtonWidth);
				} else if (button.isForSizing) {
					button.bounds = CGRect(x:0.0, y:0.0, width:lockButtonWidth, height:lockButtonWidth);

					if let buttonV3 = button.showhideLt
					{
						button.center = buttonV3.center;
						button.center.x -= sufaceButtonRadius*0.75;
						button.center.y -= sufaceButtonRadius*0.75;
					}

				} else {
					button.bounds = CGRect(x:0.0, y:0.0, width:surfaceButtonWidth, height:surfaceButtonWidth);
                	button.isHidden = false;
				}
            } else if let button:UIButton = subView as? UIButton {
                button.bounds = CGRect(x:0.0, y:0.0, width:deleteButtonWidth, height:deleteButtonWidth);
				button.setBackgroundImage(UIImage(named: "delete.png"), for: .normal);
                button.isHidden = false;
            }
        }
    }


    // MARK: - math
    //----------------------------------------------------------------------------------------------------------
    func findDistance2D(_ pt1:CGPoint, _ pt2:CGPoint) -> CGFloat
    {
        return sqrt(((pt2.x - pt1.x) * (pt2.x - pt1.x)) + ((pt2.y - pt1.y) * (pt2.y - pt1.y)));
    }


//    // MARK:  - touches actions
//    //---------------------------------------------------------------------
//    override func touch esBegan(_ touches: Set<UITouch>, with event: UIEvent?)
//    {
//print("  touch esBegan")
//        if (currentMainImageController.editButton.isSelected)
//        {
//print("  currentMainImageController.editButton.isSelected")
//
//            touchSt art = touches.first?.location(in:self.imag eView) ?? CGPoint.zero;
//print("  touchSt art",touchSt art)
//        //    touchOf fset = CGPoint.zero;
//
//        }
//    }
//
//    //--------------------------------------------------------------------------------
//    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
//    {
//print("  touchesMoved")
//
//    }
//
//    //------------------------------------------------------------------------------------
//    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
//    {
//print("  touchesEnded")
//
//    }
//
//    //---------------------------------------------------------------------------------
//    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?)
//    {
//print("  touchesCancelled")
//
//    }

    // MARK: - UIScrollViewDelegate
    //-------------------------------------------------------------------------------
    override func touchesShouldCancel(in view: UIView) -> Bool
    {
//print(" touchesShouldCancel ")
        return true;
    }

    //---------------------------------------------------------------------------------
    func viewForZooming(in scrollView: UIScrollView) -> UIView?
    {
//print("viewForZooming self.containerView.frame",self.containerView.frame)
//print("8 self.imageVi ew.frame    : \(self.imageVi ew.frame)")
        return self.containerView;
    }

    //-------------------------------------------------------------------------------------
    func configureForImageSize()
    {
//print("  configureFor ImageSize  skipConfigure:",skipConfigure)
        if (skipConfigure)
        {
//print("22 curZoom",curZoom)
//print("22 curOffset",curOffset)
//print("22 curSize",curSize)
            self.zoomScale = curZoom;
            self.contentSize = curSize;
            self.contentOffset = curOffset;
            self.containerView.frame = CGRect(x:0.0, y:0.0, width:curSize.width, height:curSize.height)
            skipConfigure = false;
        } else {
            self.setMaxMinZoomScalesForCurrentBounds();
//            self.zo omScale = self.minimumZoo mScale * 1.19999;
//            self.cutOffScale = self.minimumZo omScale * 1.20
            self.zoomScale = self.minimumZoomScale;
//            self.cutOffScale = self.minimumZo omScale * 1.0001;
            firstIn = true;
        }
//print("9 self.imageVi ew.frame    : \(self.imageVie w.frame)")
    }

    //--------------------------------------------------------------------------------------
    func setMaxMinZoomScalesForCurrentBounds()
    {
//print("0 self.containerView.frame",self.containerView.frame)

        let boundsSize:CGSize = self.bounds.size;
        self.contentSize = CGSize(width:containWidthImage, height:containHeightImage);  //self.containerView.frame.size;
        self.imageView.frame = CGRect(x:0.0, y:0.0, width:self.imageView.frame.size.width, height:self.imageView.frame.size.height);
		self.paintImageView.frame = self.imageView.frame;
		self.paintImageView1.frame = self.imageView.frame;
		self.paintImageView2.frame = self.imageView.frame;  //***************** change 0.0.40 add ***
        self.imageViewOverlay.frame = self.imageView.frame;
        self.imageContainView.frame = self.imageView.frame;
//print("  ");
//print("  ");
//print(" setMaxMinZoom ScalesForCurrentBounds ");
//print("self.conten tSize ", self.cont entSize);
//print("1 self.containerView.frame",self.containerView.frame)

        let xScale:CGFloat = boundsSize.width  / containWidthImage;    // the scale needed to perfectly fit the image width-wise
        let yScale:CGFloat = boundsSize.height / containHeightImage;   // the scale needed to perfectly fit the image height-wise
//print("xScale %f", xScale);
//print("yScale %f", yScale);
//print("boundsSize.width  ", boundsSize.width);
//print("boundsSize.height ", boundsSize.height);
//print("containWidthImage ", containWidthImage);
//print("containHeightImage", containHeightImage);

        var minScale:CGFloat = min(xScale, yScale);
//print("minScale ", minScale);

        // on high resolution screens we have double or triple the pixel density,
        // so we will be seeing every pixel if we limit the maximum zoom scale to 0.5 or 0.333.
        var maxScale:CGFloat = 1.0 / UIScreen.main.scale;
//print("UIScreen.main.scale ", UIScreen.main.scale);
//print("maxScale ", maxScale);

        // don't let minScale exceed maxScale. (If the image is smaller than the screen,
        // we don't want to force it to be zoomed.)
        if (minScale > maxScale)
        {
            minScale = maxScale;
        }
        else if (minScale < 0.001)
        {
            minScale = maxScale;
        }

//print("minScale ", minScale);

        maxScale = minScale * 40.0;  //5.0 +++++++++++ put back +++
//print("maxScale ", maxScale);
//        maxScale = 6.0;
//print("maxScale ", maxScale);

        self.maximumZoomScale = maxScale; //8.0 0.25
        self.minimumZoomScale = minScale;
        self.cutOffX = (0.0125 * containWidthImage)
//print("containWidthImage    : \(containWidthImage)")
//print("self.cutOffX    : \(self.cutOffX)")
//print("10 self.imageV iew.frame    : \(self.imageV iew.frame)")

        let ratio =  self.frame.size.height / self.frame.size.width;
//print("1 origFrameWidth ", origFrameWidth);
//print("1 wasLandscape ", wasLandscape);

        if (origFrameWidth < 4.0)
        {
            origFrameWidth = self.frame.size.width;
//print("2 origFrameWidth ", origFrameWidth);

            if (ratio < 1.0)
            {
                origFrameWidth = self.frame.size.height;
            }

            if ( !phonePortrait)
            {
                wasLandscape = true;
            }
        }

//print("2 phonePortrait ", phonePortrait);
//print("2 wasLandscape ", wasLandscape);

        if (wasLandscape)
        {
            self.cutOffX = (0.0125 * containHeightImage)
        }

        self.cutOffY = self.cutOffX / self.frame.size.width

        if (ratio < 1.0)
        {
            self.cutOffY = self.cutOffX / self.frame.size.height
        }

//print("  ");
//print("self.cutOffX ", self.cutOffX);
//print("ratio        ", ratio);
//print("self.cutOffY ", self.cutOffY);
//print("  ");
//print("2 self.containerView.frame",self.containerView.frame)
//print("11 self.imageV iew.frame    : \(self.imag eView.frame)")
    }
}

