import Foundation
import UIKit

class ReferenceItem   //*************** change 0.0.31 changes ***
{
    var name: String
    var isOneCar: Bool?
    var image: UIImage?
    var jsonFileName: String?
    var replacementGarageNames: [String]?
//    var maskNames: [String]?
//    var maskColors: [UIColor]?
//    var isPaintables: [Int]?
    var houseObject: HouseObject?

    //-----------------------------------------
    convenience init(name: String, isOneCar: Bool, image: UIImage, jsonFileName: String, garages: [String])
    {
        self.init(name: name)
        self.isOneCar = isOneCar
        self.image = image
        self.jsonFileName = jsonFileName
        self.replacementGarageNames = garages
        self.houseObject = nil
//        self.maskNames = nil
//        self.maskColors = nil
//        self.isPaintables = nil
    }
    
    //-----------------------------------------
    required init(name: String)
    {
        self.name = name
    }
}
