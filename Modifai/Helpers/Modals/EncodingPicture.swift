//
//  EncodingPicture.swift
//  Modifai


import Foundation
import UIKit
import Photos




//--------------------------------------------------------------------------------------
//--------------------------------------------------------
extension Array
{
    mutating func removeIndexes(_ indexes : IndexSet)
    {
        guard var i = indexes.first, i < count else { return }
        var j = index(after: i)
        var k = indexes.integerGreaterThan(i) ?? endIndex
        while j != endIndex
        {
            if k != j { swapAt(i, j); formIndex(after: &i) }
            else { k = indexes.integerGreaterThan(k) ?? endIndex }
            formIndex(after: &j)
        }
        removeSubrange(i...)
    }
}

//--------------------------------------------------------
extension UIDevice
{
    var hasNotch: Bool
    {
       if let keyWindow = UIWindow.key {
       let bottom = keyWindow.safeAreaInsets.bottom
       return bottom > 0
     }
        return false
    }
}
