import Foundation
import UIKit
import Alamofire
import CoreLocation
import CoreMotion

//Production   //*************** change 3.0.5 remove put in header file ***
//let baseUrl = "https://modifaiprod-dot-homes-196619.appspot.com"
//let applytransBase = "http://ec2-44-238-132-205.us-west-2.compute.amazonaws.com:5083/applyTrans"
//let jotFormLink = "https://form.jotform.com/201259212801040"

//Development   //*************** change 3.0.5 remove put in header file ***
//let baseUrl = "https://homes-196619.appspot.com"
//let applytransBase = "http://35.203.65.160:5083/applyTrans"
//let applytransBase = "http://ec2-44-238-155-221.us-west-2.compute.amazonaws.com:5083/applyTrans"
//let jotFormLink = "https://form.jotform.com/201016711662040"

let connectionError = "we are having difficulty processing favorites, this may be due to network connectivity, please try again later"
let noNetwork = "Please check network connectivity"
let applytransError = "We are sorry there seems to be connectivity issues. Please try again later"
//class Connectivity {
//    class var isConnectedToInternet:Bool {
//        return NetworkReachabilityManager()!.isReachable
//    }
//}

  //*************** change 3.0.5 remove put in header file ***
//let paintUrl:URL = URL(string: "http://ec2-44-236-52-217.us-west-2.compute.amazonaws.com:5083/applyTrans")!

var manufactureZipcodes:[String:Any] = [String:Any]()
let compressionQuality:CGFloat = 0.00001

class DataManager
{
    static func getHomeCatelogs(completion: @escaping (Error?, Data?) -> Void)
    {
        
        //  Alamofire.SessionManager.default.session.configuration.timeoutIntervalForRequest = 30
        
        
        LoadingIndicatorView.show("Please wait")
        var data:Data!
        AFManager.request("\(baseUrl)/GetCatalog/all_home_objects", method: .get, encoding: JSONEncoding.default).responseJSON
        { response in
                LoadingIndicatorView.hide()
                
                if (response.result.error != nil)
                {
                    self.checkErrorType(errorcode: response.error!._code)
                    // self.showError(errorMessage:response.result.error!.localizedDescription)
//print("00 response.result.error:",response.result.error as Any)
                    return
                }
                if let json = response.result.value as? [String:Any]
                {
                    
                    if (json["result"] != nil)
                    {
                        if json["result"]as! String == "failure"
                        {
                            self.showError(errorMessage: json["message"]as! String)
                            return
                        }
                    }
                    LoadingIndicatorView.hide()
//debugPrint("00 response:",response)
                    data = response.data!
                    
                    completion(nil, data)
                }
        }
    }

    static func getDeletedAgentHomes(range:String,limit:String,completion: @escaping (Error?, NSDictionary?) -> Void)
    {
       // LoadingIndicatorView.show("Please wait")
        let parametersDic: [String:Any] = ["range":range,"limit":limit]

        AFManager.request("\(baseUrl)/GetDeletedAgentHomes" ,method: .post, parameters: parametersDic,encoding: JSONEncoding.default, headers: nil).responseJSON { response in
               // LoadingIndicatorView.hide()
               
                if let json = response.result.value as? NSDictionary
                {
                    
                  //  LoadingIndicatorView.hide()
                  // debugPrint("01 response:",response)
                    completion(nil, json)
                }else
                {
                    completion(nil, nil)
                 }
        }
    }
    
    //--------------------------------------
    static func getCompletedAgentHomes(range:String,limit:String,completion: @escaping (Error?, NSDictionary?) -> Void)
    {
        let parametersDic: [String:Any] = ["range":range,"limit":limit]

        AFManager.request("\(baseUrl)/GetCompletedAgentHomes" ,method: .post, parameters: parametersDic,encoding: JSONEncoding.default, headers: nil).responseJSON
        { response in
//print("0 getCompletedAgentHomes response",response)

                if let json = response.result.value as? NSDictionary
                {
//debugPrint("01 response:",response)
                    completion(nil, json)
                } else {
                    completion(nil, nil)

                 }
        }
    }

    static func getAllAgentHomes(range:String,limit:String,completion: @escaping (Error?, NSDictionary?) -> Void)
        {
                        
           // LoadingIndicatorView.show("Please wait")
            let parametersDic: [String:Any] = ["range":range,"limit":limit]

            AFManager.request("\(baseUrl)/GetAgentHomes" ,method: .post, parameters: parametersDic,encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                   // LoadingIndicatorView.hide()
                   
                    if let json = response.result.value as? NSDictionary
                    {
                        
                       // LoadingIndicatorView.hide()
                     //  debugPrint("00 response:",response)
                        completion(nil, json)
                    }else
                    {
                        completion(nil, nil)

                     }
            }
        }
    static func getAllAgentHomeStatus(agentID:String,completion: @escaping (Error?, Bool?) -> Void)
           {
                           
               LoadingIndicatorView.show("Please wait")
               let parametersDic: [String:Any] = ["agentID":agentID]

               AFManager.request("\(baseUrl)/GetAgentHomeStatus" ,method: .post, parameters: parametersDic,encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                       LoadingIndicatorView.hide()
                      
                if let json = response.result.value as? [String:Any]
                       {
                           
                           LoadingIndicatorView.hide()
                          let status = json["status"] as? Bool ?? false
                           completion(nil, status)
                        
                       }else
                       {
                           completion(nil, false)

                        }
               }
           }
    static func GetPaintColorsForAgentHomes(orderNumber:String,completion: @escaping (Error?, Bool?) -> Void)
    {
        
        let parametersDic: [String:Any] = ["orderNumber":orderNumber]
        AFManager.request("\(baseUrl)/GetAgentOrderPaintColor", method: .post, parameters: parametersDic,encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            //debugPrint(response)

            if (response.result.error != nil)
            {
                 completion(nil, false)
              //  self.checkErrorType(errorcode: response.error!._code)
                return
            }
            if let JSON  = response.result.value             {
               
                if let checkoutlist = JSON as? NSArray
                               {
                                if checkoutlist.count > 0
                                {
                                    self.downloadFavSceneFiles(fromArray:checkoutlist)
                                    

                               }
                               completion(nil, true)
                                
                }
            }else{
               completion(nil, false)

            }
        }
        
    }
    static func getAgentHouseList(orderNumber:String,completion: @escaping (Error?, NSDictionary?) -> Void) 
       {
        
        
        let parametersDic: [String:Any] = ["orderNumber":orderNumber]
        
        AFManager.request("\(baseUrl)/GetAgentOrderFullSizeImage", method: .post, parameters: parametersDic,encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            //debugPrint(response)
            
            if (response.result.error != nil)
            {
                completion(nil,nil)
                LoadingIndicatorView.hide() //************* change 1.2.0
                return
            }
            if let data = response.result.value as? NSDictionary
            {
                if (data["result"] != nil)
                {
                    if data["result"]as! String == "failure"
                    {
                        UserDefaults.standard.set(true, forKey: "OrderDownload")
                        completion(nil,nil)
                        self.showError(errorMessage: data["message"]as! String)
                        return
                    }
                }
                UserDefaults.standard.set(true, forKey: "OrderDownload")
                
                
                let tempDic = data as? [String:Any]
                if tempDic != nil && tempDic!.count > 0
                {
                    let imgurl = tempDic!["image_url"]
                    UserDefaults.standard.set(imgurl, forKey:"Download_Last_Image_url")
                    UserDefaults.standard.set(imgurl, forKey:"Last_Image_url")

                    debugPrint("downloadedImage:",imgurl!)
                    var imgurlName = (imgurl! as! NSString).lastPathComponent
                    let imgarray = imgurlName.components(separatedBy:"--")
                    // let tempfilename2 = imgarray[0]
                    imgurlName = imgarray[0] + ".jpg"
                    let fileurl2:String = (tempDic!["editedrois_file_url"] as? String) ?? ""
                    
                    if !fileurl2.isEmpty{
                        
                        var fileurlName2 = (fileurl2 as NSString).lastPathComponent
                        let filearray2 = fileurlName2.components(separatedBy:"--")
                        fileurlName2 = filearray2[0] + ".txt"
                        if !fileurlName2.contains("_houseObject")
                        {
                            fileurlName2 = filearray2[0] + "_houseObject" + ".txt"
                        }
                        AppDelegate.downloadUsingAlamofire(url: URL(string: fileurl2)!, fileName: fileurlName2)
                    }
                    
                    AppDelegate.downloadUsingAlamofire(url: URL(string: imgurl! as! String)!, fileName: imgurlName)
                    
                  //  UserDefaults.standard.set(imgurl, forKey: "Last_Image_url")
                    
                    
                    let fileurl:String = (tempDic!["file_url"] as? String) ?? ""
                    
                    if !fileurl.isEmpty{
                        
                        var fileurlName = (fileurl as NSString).lastPathComponent
                        let filearray = fileurlName.components(separatedBy:"--")
                        fileurlName = filearray[0] + ".txt"
                        if !fileurlName.contains("_doc")
                        {
                            fileurlName = filearray[0] + "_doc" + ".txt"
                            
                        }
                        AppDelegate.downloadUsingAlamofire(url: URL(string: fileurl)!, fileName: fileurlName)
                    }
        
                }
                completion(nil,data)
            }else
            {
                UserDefaults.standard.set(true, forKey: "OrderDownload")
                
                completion(nil,nil)
            }
        }
        
    }
    static func getPreviousHouseList(completion: @escaping (Error?, NSArray?) -> Void)
    {
        
        guard let userid = UserDefaults.standard.value(forKey:"UserID") else{return}
        LoadingIndicatorView.show("Please wait")
        let parametersDic: [String:Any] = ["userID":userid as! String]
        
        AFManager.request("\(baseUrl)/ImagesAndFiles", method: .post, parameters: parametersDic,encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            //debugPrint(response)
            LoadingIndicatorView.hide()
            if (response.result.error != nil)
            {
                //self.checkErrorType(errorcode: response.error!._code)
                // self.showError(errorMessage:response.result.error!.localizedDescription)
                return
            }
            if let json = response.result.value as? NSDictionary
            {
                if (json["result"] != nil)
                {
                    if json["result"]as! String == "failure"
                    {
                        self.showError(errorMessage: json["message"]as! String)
                        return
                    }
                }
                //print("JSON: \(json)") // Works!
                let data = json["\(userid)"] as! NSArray
                //print("Data: \(data)")
                completion(nil,data)
            }else
            {
                completion(nil,nil)
            }
        }
        
    }
    static func showError(errorMessage:String)
    {
        AlertViewManager.shared.ShowOkAlert(title:"Error", message: errorMessage, handler: nil)
    }
    static func checkErrorType(errorcode:Int)
    {
        switch (errorcode){
            /*case NSURLErrorTimedOut:
             //Manager your time out error
             self.showError(errorMessage:connectionError)
             break*/
        case NSURLErrorNotConnectedToInternet:
            //Manager your not connected to internet error
            self.showError(errorMessage:noNetwork)
            
            break
        default:
            self.showError(errorMessage:"Could not connect to the server, please try again.")
        }
    }

    static func getHomeCatalogsAfterFilter(filterDic: [String : String], completion: @escaping (Error?, Data?) -> Void)
    {
        //https://homes-196619.appspot.com/GetCatalog/all_home_objects?style='Traditional'&price=7900
        
        LoadingIndicatorView.show("Please wait")
        let brand = filterDic["brand"]?.lowercased() ?? ""
        let style = filterDic["style"]?.lowercased() ?? ""
        let price = filterDic["price"]?.lowercased() ?? ""
        
        var urlString = "\(baseUrl)/GetCatalog/all_home_objects"
        //style='\(style)'&bra nd='\(bra nd)'&price='\(price)'
        
        if style.count>0
        {
            urlString = urlString + "?style='\(style)'"
        }
        if brand.count>0
        {
            if urlString.contains("?")
            {
                urlString = urlString + "&brand='\(brand)'"
            } else {
                urlString = urlString + "?brand='\(brand)'"
            }
        }

        if price.count>0
        {
            if urlString.contains("?")
            {
                urlString = urlString + "&price='\(price)'"
            }else
            {
                urlString = urlString + "?price='\(price)'"
            }
        }

        //print("urlString===\(urlString)")
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        //print("urlString2===\(urlString)")
        var data:Data!
        AFManager.request(urlString, method: .get, encoding: JSONEncoding.default)
            .responseJSON { response in
                if (response.result.error != nil)
                {
                    self.checkErrorType(errorcode: response.error!._code)
                    // self.showError(errorMessage:response.result.error!.localizedDescription)
                    return
                }
                LoadingIndicatorView.hide()
                //debugPrint(response)
                data = response.data!
                completion(nil, data)
        }
        
    }
    static func getUserDetails(completion: @escaping (Error?, NSDictionary?) -> Void)
    {
        
        guard let userid = UserDefaults.standard.value(forKey:"UserID") else{return}
        LoadingIndicatorView.show("Please wait")
        let parametersDic: [String:Any] = ["userID":userid as! String]
        
        AFManager.request("\(baseUrl)/GetRequest", method: .post, parameters: parametersDic,encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            //debugPrint(response)
            LoadingIndicatorView.hide()
            //     if (response.response?.statusCode != nil)
            //     {
            //         self.showError(errorMessage: "Could not connect to the server, please try again.")
            //         return
            //     }
            if (response.result.error != nil)
            {
                self.checkErrorType(errorcode: response.error!._code)
                // self.showError(errorMessage:response.result.error!.localizedDescription)
                return
            }
            if let json = response.result.value as? NSDictionary
            {
                if (json["result"] != nil)
                {
                    if json["result"]as! String == "failure"
                    {
                        self.showError(errorMessage: json["message"]as! String)
                        return
                    }
                }
                //print("JSON: \(json)") // Works!
                //      let data = json["userID"] as! String
                //      //print("Data: \(data)")
                completion(nil,json)
            } else {
                completion(nil,nil)
            }
        }
        
    }
    
    //------------------------------------------
    static func registerUser(emailId:String,tokenId:String,completion: @escaping (Error?,String?) -> Void)
    {
        LoadingIndicatorView.show("Please wait")
        let appName = Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String ?? ""

        
        let parametersDic: [String:Any] = ["username":emailId,"tokenId":tokenId,"devicename":UIDevice.modelName,"appname":appName]
        //print("emailId: \(emailId)")
        //print("tokenId: \(tokenId)")
        //print("devicename:",UIDevice.modelName)
        let headers: HTTPHeaders = [
            "Content-Type":"application/json",
            "Accept": "application/json"
        ]
        AFManager.request("\(baseUrl)/CreateUser", method: .post, parameters: parametersDic,encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            //debugPrint(response)
            LoadingIndicatorView.hide()
            if (response.result.error != nil)
            {
              //  self.checkErrorType(errorcode: response.error!._code)
                // self.showError(errorMessage:response.result.error!.localizedDescription)
                return
            }
            if let json = response.result.value as? [String:Any]
            {
                if (json["result"] != nil)
                {
                    if json["result"]as! String == "failure"
                    {
                       // self.showError(errorMessage: json["message"]as! String)
                        return
                    }
                }
                //print("JSON: \(json)") // Works!
                let data = json["userID"] as! String
                //print("Data: \(data)")
                UserDefaults.standard.set(data, forKey:"UserID")
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.saveValues()
                completion(nil,data)
            }
        }
    }
    
    //------------------------------------------
    static func updateUserDetails(userDetails:NSDictionary,completion: @escaping (Error?, NSString?) -> Void)
    {
        guard let userid = UserDefaults.standard.value(forKey:"UserID") else{return}
        guard let username = UserDefaults.standard.value(forKey:"UserEmail") else{return}
        let zip:Int = Int(userDetails.value(forKey:"zipcode") as! String)!
        //let phone:Int = Int(userDetails.value(forKey:"PhoneNumber") as! String)!
        let phone = userDetails.value(forKey:"PhoneNumber") as! String
        let parametersDic: [String:Any] = ["userID":userid as! String,"username":username as! String,"firstname":userDetails.value(forKey:"firstname") as! String,  "lastname":userDetails.value(forKey:"lastname") as! String,"streetname1":userDetails.value(forKey:"streetname1") as! String,"streetname2":userDetails.value(forKey:"streetname2") as! String,"PhoneNumber":phone,"city":userDetails.value(forKey:"city") as! String,"state":userDetails.value(forKey:"state") as! String, "zipcode":zip,"Email":userDetails.value(forKey:"Email") as! String,"set_update":0]
        AFManager.request("\(baseUrl)/CreateRequest", method: .post, parameters: parametersDic,encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            //debugPrint(response)
            //            if (response.result.error != nil)
            //            {
            //                self.showError(errorMessage: "Could not connect to the server, please try again.")
            //                return
            //            }
            if let JSON  = response.result.value as? [String:Any]
            {
                //                if (JSON["result"] != nil)
                //                {
                //                    if JSON["result"]as! String == "failure"
                //                    {
                //                        self.showError(errorMessage: JSON["message"]as! String)
                //                        return
                //                    }
                //                }
                print("JSON: \(JSON)") // Works!
                //                let data  = JSON as! NSDictionary
                completion(nil,"success")
            } else {
                completion(nil,nil)
            }
        }
    }
    
    //------------------------------------------
    static func getImage(fileName:String,completion: @escaping (Error?, String?) -> Void)
    {
        let txtfileName = fileName.replacingOccurrences(of:".jpg", with: "_doc.txt")
        let txtfileName2:String = fileName.replacingOccurrences(of:".jpg", with: "_doc")
        //print(" ")
        //print("get Image got txtfileName:",txtfileName as Any)
        //print(" ")
        //print("get Image got txtfileName2:",txtfileName2 as Any)
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        {
            let fileURL = dir.appendingPathComponent(txtfileName)
            
            do
            {
                let contents = try String(contentsOf: fileURL, encoding: .utf8)
                let data :Data = contents.data(using: String.Encoding.utf8)! as Data
                let convertedString = String(data: data as Data , encoding: String.Encoding.utf8)
                //                let output = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments) as? String
                //                let tempArray:NSArray = [output!]
                //                completion(nil,tempArray)
                completion(nil,convertedString)
                //print(contents)
                //print(" ")
                //print(" ")
                //print("get Image got convertedString:",convertedString as Any)
                return
            } catch {
                //print(" ")
                //print(" ")
                //print("get Image didn't load example")
                // contents could not be loaded
            }
        }
        
        if let filepath = Bundle.main.path(forResource: txtfileName2, ofType: "txt")
        {
            do
            {
                let contents = try String(contentsOfFile: filepath)
                //print(contents)
                let data :Data = contents.data(using: String.Encoding.utf8)! as Data
                let convertedString = String(data: data as Data , encoding: String.Encoding.utf8)
                
                //print(" ")
                //print(" ")
                //print("get Image got convertedString2 ???:",convertedString as Any)
                
                
                let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
                let filename =  paths[0].appendingPathComponent(txtfileName)
                if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                {
                    let fileURL = dir.appendingPathComponent(txtfileName)
                    
                    if FileManager.default.fileExists(atPath:fileURL.path)
                    {
                        //print("FILE AVAILABLE")
                        //print(" ")
                        //print(" ")
                        //print("get Image got FILE AVAILABLE")
                        
                    } else {
                        //print("FILE NOT AVAILABLE")
                        do
                        {
                            try convertedString!.write(to: filename, atomically: true, encoding: String.Encoding.utf8)
                            //print("Done Converting")
                            
                        } catch {
                            //print("Error Converting")
                        }
                    }
                    // self.uploadFilesToCloud()
                    
                }
                completion(nil,convertedString)
                //print("get Image got FILE AVAILABLE completion")
                return
            } catch {
                // contents could not be loaded
            }
        } else {
            // example.txt not found!
        }
        completion(nil,nil)
    }
    
    //-------------------------------------
    static func getExistingUserId(emailId:String,completion: @escaping (Error?, String?) -> Void)
    {
        let parametersDic: [String:Any] = ["username":emailId]
        AFManager.request("\(baseUrl)/GetUser", method: .post, parameters: parametersDic,encoding: JSONEncoding.default, headers: nil).responseJSON
            { response in
                //debugPrint(response)
                
                if (response.result.error != nil)
                {
                    self.checkErrorType(errorcode: response.error!._code)
                    // self.showError(errorMessage:response.result.error!.localizedDescription)
                    return
                }
                
                if let JSON  = response.result.value as? [String:Any]
                {
                    //print("JSON: \(JSON)") // Works!
                    if (JSON["result"] != nil)
                    {
                        if JSON["result"]as! String == "failure"
                        {
                            self.showError(errorMessage: JSON["message"]as! String)
                            return
                        }
                    }
                    let data  = JSON as! NSMutableDictionary
                    let userIdArray = data.value(forKey:"UserIds") as! NSMutableArray
                    //print("userIdArray: \(userIdArray[0])")
                    let dict:NSDictionary = userIdArray[0] as! NSDictionary
                    completion(nil,(dict.value(forKey: "userID") as! String))
                }
        }
        
    }
    //**************** Not using this method Lakshmi - 2.0.1
//    static func getFavoriteHouses(completion: @escaping (Error?, NSArray) -> Void)

  //  static func getFavoriteStatus(catalogID:Array<NSDictionary>,imageID:String,completion: @escaping (Error?, Bool?,String) -> Void)  //not using change 2.0.6
    
    
  //  static func updateCheckout(favoriteID:String,checkout:Bool,completion: @escaping (Error?, String?) -> Void) // not using change 2.0.6
    
    
    //----------------------------------------------------------
  //  static func getDesignList(imageUrl:String,completion: @escaping (Error?, NSMutableDictionary?) -> Void) // not using change 2.0.6

    

 //   static func GetFavoriteItems(imageUrl:String,completion: @escaping (Error?, NSArray?) -> Void) // not using change 2.0.6
    
    
   // static func getCheckoutItems(completion: @escaping (Error?, NSArray?) -> Void) // not using change 2.0.6
    
    
    //--------------------------------------------------------------------
    static func createFavoriteScene(catalogArray:Array<NSDictionary>, mainFolderName:String, catFolderName:String, filenamesArray:[URL],completion:@escaping(Error?, String?) -> Void) //**************** replace Method Lakshmi - 2.0.1
    {
        
        //print("create Favorite Scene")
        guard let userid = UserDefaults.standard.value(forKey:"UserID") else {return}
        
        let headers: HTTPHeaders =
            [
                "Content-Type":"application/json",
                "Accept": "application/json"
        ]
        
        guard let lastImageUrl = UserDefaults.standard.value(forKey: "Last_Image_url") else {return}
        let jsonData = try! JSONSerialization.data(withJSONObject: catalogArray, options: [])
        let decoded = String(data: jsonData, encoding: .utf8)!
        
        let parametersDic: [String:Any] = ["userID": userid as! String,"catalogID":decoded,"image_url":lastImageUrl as! String,"mainFolderName":mainFolderName,"catFolderName":catFolderName]
        
        AFManager.upload(multipartFormData:{  multipartFormData in
            
            for itemURL in filenamesArray
            {
                let fileURL:URL = itemURL
                let filenamefromURL = fileURL.lastPathComponent

                if fileURL.absoluteString.contains(".txt")
                {
                do {
                    let contents = try String(contentsOf: fileURL, encoding: .utf8)
                    let convertData = contents.data(using: String.Encoding.utf8)! as Data
                    var imageWithName = "garagerName"
                    if fileURL.absoluteString.contains("doordetails")
                    {
                        imageWithName = "doorName"
                    }
                    else if fileURL.absoluteString.contains("cardetails")
                    {
                        imageWithName = "garagerName"
                    }
                    
                    multipartFormData.append(convertData, withName: imageWithName, fileName: filenamefromURL, mimeType: "text/plain")

                } catch {
                    completion(nil,nil)
                }
                }else
                {
                    do {
                        if let image = UIImage(contentsOfFile: fileURL.path)
                        {
                        let imageData = image.jpegData(compressionQuality:1.0)
                      
                        var imageWithName = "image"
                        if fileURL.absoluteString.contains("_favorite.jpg")
                        {
                            imageWithName = "favFullImage"
                        }else if fileURL.absoluteString.contains("car.jpg")
                        {
                            imageWithName = "favGarage"
                        }else if fileURL.absoluteString.contains("door.jpg")
                        {
                            imageWithName = "favDoor"
                        }
                        
                        multipartFormData.append(imageData!, withName: imageWithName, fileName: filenamefromURL, mimeType: "image/jpeg")
                        }

                    }
                }
            }
            for (key, value) in parametersDic {
                //print(value)
                multipartFormData.append(((value as AnyObject).data(using: String.Encoding.utf8.rawValue))!, withName: key)
            }
        },
                         usingThreshold:UInt64.init(),
                         to:"\(baseUrl)/CreateSceneFavorite",
            method:.post,
            headers:headers,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload,_,_ ):
                    
                    upload.responseJSON { response in
                        //print(response)
//debugPrint("createAPI response:",response)
                        //
                        if (response.result.error != nil)
                        {
                           // self.checkErrorType(errorcode: response.error!._code)
                            completion(nil,"")
                            return
                        }
                        if let json = response.result.value as? [String:Any]
                        {
                            
                            if (json["result"] != nil)
                            {
                                if json["result"]as! String == "failure"
                                {
                                    completion(nil,"")
                                    return
                                }
                            }
                            
                            
                            completion(nil,"")
                        }else
                        {
                            completion(nil,"")
                        }
                    }
                    
                    
                case .failure(let encodingError):
                    print("encodingError:",encodingError)
                    LoadingIndicatorView.hide()
                    completion(nil,"")
                }
        })
        
    }
    static func removeFavoriteSceneNew(mainFolderName:String, catFolderName:String)
    {
        guard let userid = UserDefaults.standard.value(forKey:"UserID") else
              {
                  return
              }
             
              let parametersDic: [String:Any] = ["mainFolderName":mainFolderName,"userID":userid,"catFolderName":catFolderName]
              AFManager.request("\(baseUrl)/RemoveFavoriteScene", method: .post, parameters: parametersDic,encoding: JSONEncoding.default, headers: nil).responseJSON
                  { response in
                   // debugPrint("remove API:",response)
                   
                      if (response.result.error != nil)
                      {
                         
                          return
                      }
                      
                      if let JSON  = response.result.value as? [String:Any]
                      {
                          //print("JSON: \(JSON)") // Works!
                          if (JSON["result"] != nil)
                          {
                              if JSON["result"]as! String == "failure"
                              {
                                
                                  return
                              }
                          }
                      }
              }
    }
    static func removeAgentHomeFromServer(agentID:String)
     {
//         guard let userid = UserDefaults.standard.value(forKey:"UserID") else
//               {
//                   return
//               }
//
        if hasInternalApp
        {
               let parametersDic: [String:Any] = ["agentID":agentID]
               AFManager.request("\(baseUrl)/DeleteAgentOrder", method: .post, parameters: parametersDic,encoding: JSONEncoding.default, headers: nil).responseJSON
                   { response in
                     debugPrint("remove API:",response)
                    
                       if (response.result.error != nil)
                       {
                          
                           return
                       }
                       
                       if let JSON  = response.result.value as? [String:Any]
                       {
                         //  print("JSON: \(JSON)") // Works!
                           if (JSON["result"] != nil)
                           {
                               if JSON["result"]as! String == "failure"
                               {
                                 
                                   return
                               }
                           }
                       }
               }
        }
     }
       
       //--------------------------------------------------------------------
  static func removePaintImageFromServer(filename:String, paintURL:String)
  {
//      guard let userid = UserDefaults.standard.value(forKey:"UserID") else
//            {
//                return
//            }
           
            let parametersDic: [String:Any] = ["filename":filename]  //,"userID":userid,"painturl":paintURL]
            AFManager.request("\(baseUrl)/UpdateDeletedPaintColor", method: .post, parameters: parametersDic,encoding: JSONEncoding.default, headers: nil).responseJSON
                { response in
                  debugPrint("remove API:",response)
                 
                    if (response.result.error != nil)
                    {
                       
                        return
                    }
                    
                    if let JSON  = response.result.value as? [String:Any]
                    {
                    //debugPrint("JSON: \(JSON)") // Works!
                        if (JSON["result"] != nil)
                        {
                            if JSON["result"]as! String == "failure"
                            {
                              
                                return
                            }
                        }
                    }
            }
  }
    
    //--------------------------------------------------------------------
    static func uploadModifiedImage(imgLocation: CLLocation, fileName: String, image: UIImage, completion: @escaping (Error?, String?,String?) -> Void)
    {
        
        guard let imageData = image.jpegData(compressionQuality: 1.0) else {
            let error = NSError(domain:"com.modifai", code:401, userInfo:[ NSLocalizedDescriptionKey: "Invalid Image"])
            completion(error, nil,nil)
            return
        }
        guard let accessToken = UserDefaults.standard.object(forKey: "accessTokenValue")
            else {return}
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \( accessToken as! String)",
            //"Authorization": "23423424asdfcasdf",
            "Content-Type":"application/json",
            "Accept": "application/json"
        ]
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let imgTimeString = formatter.string(from: imgLocation.timestamp)
        guard let userid = UserDefaults.standard.value(forKey:"UserID") else{return}
        
        guard let lastImageUrl = UserDefaults.standard.value(forKey: "Last_Image_url") else {return}
        
        let parametersDic: [String:String] = ["lat":String(imgLocation.coordinate.latitude),"long": String(imgLocation.coordinate.longitude),"capture":imgTimeString,"userID": userid as! String,"image_url":lastImageUrl as! String]
        
        
        
        LoadingIndicatorView.show("Please wait")
        AFManager.upload(multipartFormData:{  multipartFormData in
            for (key, value) in parametersDic {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
            }
            multipartFormData.append(imageData, withName: "image", fileName: fileName, mimeType: "image/jpeg")
        },
                         usingThreshold:UInt64.init(),
                         to:"\(baseUrl)/CreateReplacedImage",
            method:.post,
            headers:headers,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload,_,_ ):
                    
                    upload.responseJSON { response in
                        //print(response)
                        //debugPrint(response)
                        LoadingIndicatorView.hide()
                        if (response.result.error != nil)
                        {
                            self.checkErrorType(errorcode: response.error!._code)
                            return
                        }
                        if let json = response.result.value as? [String:Any]
                        {
                            
                            //print("JSON: \(json)") // Works!
                            let data = json["replacedImageID"] as! String
                            let replaceImg_Url = json["replaced_image_url"] as! String
                            //print("Data: \(data)")
                            //print("replaceImg_Url: \(replaceImg_Url)")
                            completion(nil,data,replaceImg_Url)
                        }
                    }
                    //  upload.responseString(completionHandler: { response in
                    //    //print("success", response.result.value as Any)
                    
                //  })
                case .failure(let encodingError):
                    print(encodingError)
                    break;
                }
        })
        
    }
    
  

    //-----------------------------------------------------------
    static func uploadImageAndTextFile(imageUrl: String,agentID:String,image:UIImage, txtUrl: String, completion: @escaping (Error?, String?) -> Void)
    {
        var nameTemp1 = txtUrl.replacingOccurrences(of:".jpg", with:"")
        let imgarray1 = nameTemp1.components(separatedBy:"--")
        if imgarray1.count>0
        {
         nameTemp1 = imgarray1[0]
        }
         
        let fullImgID = UserDefaults.standard.value(forKey:String(format:"%@_fullID",nameTemp1)) as? String ?? ""
        if fullImgID.count > 0
        {
        return
        }
    
        let headers: HTTPHeaders = [
            "Content-Type":"application/json",
            "Accept": "application/json"
        ]
        
        let userid = UserDefaults.standard.value(forKey:"UserID") ?? ""
        let txtfileName = txtUrl.replacingOccurrences(of:".jpg", with: "_doc.txt")
        let persTextfileName = txtUrl.replacingOccurrences(of:".jpg", with: "_perspdoc.txt")
        
        var textfileConvString:Data? = nil
        var persptextfileConvString:Data? = nil
        
        guard let imageData = image.jpegData(compressionQuality: 1.0) else
        {
            let error = NSError(domain:"com.modifai", code:401, userInfo:[ NSLocalizedDescriptionKey: "Invalid Image"])
            completion(error,nil)
            return
        }
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        {
            let fileURL = dir.appendingPathComponent(txtfileName)
            
            do {
                let contents = try String(contentsOf: fileURL, encoding: .utf8)
                textfileConvString = contents.data(using: String.Encoding.utf8)! as Data
                //    textfileConvString = String(data: data as Data , encoding: String.Encoding.utf8)!
            } catch {
                // contents could not be loaded
            }
            
        }
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = dir.appendingPathComponent(persTextfileName)
            
            do {
                let contents = try String(contentsOf: fileURL, encoding: .utf8)
                persptextfileConvString = contents.data(using: String.Encoding.utf8)! as Data
                //    textfileConvString = String(data: data as Data , encoding: String.Encoding.utf8)!
            } catch {
                // contents could not be loaded
               
            }
            
        }
        
        let parameters = [
            "userID": userid,"agentID":agentID
        ]
//debugPrint("UploadFullSizedImage param\(parameters)")
   
        AFManager.upload(multipartFormData:{  multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
            }

            multipartFormData.append(imageData, withName: "image", fileName: imageUrl, mimeType: "image/jpeg")

            if textfileConvString != nil
            {
                multipartFormData.append(textfileConvString!, withName: "file_url", fileName: txtfileName, mimeType: "text/plain")
            }
            if persptextfileConvString != nil
            {
                multipartFormData.append(persptextfileConvString!, withName: "perspectiverois_file_url", fileName: persTextfileName, mimeType: "text/plain")
            }
        },
                         usingThreshold:UInt64.init(),
                         to:"\(baseUrl)/UploadFullSizedImage",
            method:.post,
            headers:headers){ (result) in
            switch result{
            case .success(let upload, _, _):

                    upload.uploadProgress(closure: { (Progress) in
//print("Upload Progress: \(Progress.fractionCompleted)")
                                   }) // add change 2.0.6

                    upload.responseJSON { response in
                        //print(response)
                        //debugPrint("UploadFullSizedImage status\(response.response?.statusCode)")
//                        debugPrint("UploadFullSizedImage Name:\(txtUrl),URL:\(baseUrl)/UploadFullSizedImage,params:\(parameters)")
//debugPrint("UploadFullSizedImage response:\(response)")
                        LoadingIndicatorView.hide()

                        if let json = response.result.value as? NSDictionary
                        {
                            if (json["result"] != nil)
                            {
                                if json["result"]as! String == "failure"
                                {
                                    self.showError(errorMessage: json["message"]as! String)
                                    return
                                }
                            }
                            let fullSizeimageID = (json["imageID"] as? String)!
                            var nameTemp = txtUrl.replacingOccurrences(of:".jpg", with:"")
                            let imgarray = nameTemp.components(separatedBy:"--")
                            if imgarray.count>0
                            {
                             nameTemp = imgarray[0]
                            }

                            UserDefaults.standard.set(fullSizeimageID, forKey:String(format:"%@_fullID",nameTemp))
                            if json["userID"] is NSNull
                            {
                                self.uploadImageIDIntoFile(fullsizeImageID:fullSizeimageID)
                            }

                        }
                            completion(nil,nil)

                    }
                  //  completion(nil,nil)
                    //  upload.responseString(completionHandler: { response in
                    //    //print("success", response.result.value as Any)

                //  })
                case .failure(let encodingError):
                    print("encodingError:",encodingError)
                    completion(nil,nil)
                }
        }
        
        
    }
    
  
    static func getObjectJsonNew(paramName: String,agentID:String, fileName: String, image: UIImage,imageFromGalleryName:String, completion: @escaping (Error?, String?) -> Void)
    {
        
        guard let imageData = image.jpegData(compressionQuality: 1.0) else {
            let error = NSError(domain:"com.modifai", code:401, userInfo:[ NSLocalizedDescriptionKey: "Invalid Image"])
            completion(error,nil)
            return
        }
        
        let headers: HTTPHeaders = [
            
            "Content-Type":"application/json",
            "Accept": "application/json"
        ]
        
        let url = URL(string: applytransBase)
        
        // generate boundary string using a unique per-app string
        let boundary = UUID().uuidString
        
        //     let session = URLSession.shared
        
        // Set the URLRequest to POST and to the specified URL
        var urlRequest = URLRequest(url: url!)
        urlRequest.httpMethod = "POST"
        urlRequest.timeoutInterval = 20.0
        
        // Set Content-Type Header to multipart/form-data, this is equivalent to submitting form data with file upload in a web browser
        // And the boundary is also set here
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        var data = Data()
        guard let deviceID = UserDefaults.standard.value(forKey: "deviceID") else {return}
        
        // Add the image data to the raw http request data
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"\(paramName)\"; filename=\"\(fileName)\"\r\n".data(using: .utf8)!)
        data.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
        data.append(imageData)
        
        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
        let parametersDic: [String:String] = ["deviceID": deviceID as! String,"agentID":agentID]
        //      LoadingIndicatorView.show("Please wait")
        Alamofire.SessionManager.default.session.configuration.timeoutIntervalForRequest = 30
        Alamofire.upload(multipartFormData:{  multipartFormData in
            for (key, value) in parametersDic {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
            }
            multipartFormData.append(imageData, withName: "image", fileName: fileName, mimeType: "image/jpeg")
            
        },
                         usingThreshold:UInt64.init(),
                         to:applytransBase,
                         method:.post,
                         headers:headers,
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload,_,_ ):
                                upload.responseJSON
                                    { response in
                                        //print(response)
                                      //  debugPrint("applytrans res \(response)")
                                        LoadingIndicatorView.hide()
                                        if (response.result.error != nil)
                                        {
                                            //   self.showError(errorMessage:response.result.error!.localizedDescription)
                                            switch (response.error!._code){
                                            case NSURLErrorTimedOut:
                                                //Manager your time out error
                                                self.showError(errorMessage:applytransError)
                                                break
                                            case NSURLErrorNotConnectedToInternet:
                                                //Manager your not connected to internet error
                                                self.showError(errorMessage:noNetwork)
                                                
                                                break
                                            default:
                                                self.showError(errorMessage:"Sorry we couldn’t process that image. Could you please retake the picture")
                                            }
                                            //                                        self.showError(errorMessage: "Sorry we couldn’t process that image. Could you please retake the picture")
                                            completion(nil,nil)
                                            return
                                        }
                                        if let json = response.result.value as? NSDictionary
                                        {
                                            if (json["result"] != nil)
                                            {
                                                if json["result"]as! String == "failure"
                                                {
                                                    self.showError(errorMessage: json["message"]as! String)
                                                    completion(nil,nil)
                                                    return
                                                }
                                            }
                                            
                                            let convertedString = String(data: response.data! , encoding: String.Encoding.utf8) // the data will be converted to the string
                                            let txtfileName = imageFromGalleryName.replacingOccurrences(of:".jpg", with: "_doc.txt")
                                            let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
                                            let filename =  paths[0].appendingPathComponent(txtfileName)
                                            
                                            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                                            {
                                                let fileURL = dir.appendingPathComponent(txtfileName)
                                                
                                                if FileManager.default.fileExists(atPath:fileURL.path)
                                                {
                                                    //print("FILE AVAILABLE")
                                                    
                                                } else {
                                                    //print("FILE NOT AVAILABLE")
                                                    do
                                                    {
                                                        try convertedString!.write(to: filename, atomically: true, encoding: String.Encoding.utf8)
                                                        //print("Done Converting")
                                                    } catch {
                                                        //print("Error Converting")
                                                    }
                                                }
                                                // self.uploadFilesToCloud()
                                                
                                            }
                                            
                                            //    let tempArray:NSArray = [json]
                                            //print("JSON: \(convertedString)") // Works!
                                            completion(nil,convertedString)
                                        } else {
                                            completion(nil,nil)
                                        }
                                }
                                //  upload.responseString(completionHandler: { response in
                                //    //print("success", response.result.value as Any)
                                
                            //  })
                            case .failure(let encodingError):
                                print("encodingError",encodingError)
                            }
        })
        
    }
    
    //----------------------------------------
    func getDocumentsDirectory() -> URL
    {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
//    //----------------------------------------
//    static func UpdateUserZip(ZipCode:String,completion: @escaping (Error?, String?) -> Void)
//    {
//        let userid = UserDefaults.standard.value(forKey:"UserID") as? String ?? ""
//        let parametersDic: [String:Any] = ["zipcode":ZipCode,"userID":userid]
//
//        AFManager.request("\(baseUrl)/CreateUserZip", method: .post, parameters: parametersDic,encoding: JSONEncoding.default, headers: nil).responseJSON {
//            responseresult in
//            if let resjson = responseresult.result.value as? [String:Any]
//            {
//                //debugPrint(responseresult)
//                let shopping =  resjson["shopping"] as? Int ?? 0
//
//                shoppingVal = String(shopping)
//                self.getAllManufacturerZipCodes()
//                completion(nil,String(shopping))
//
//            }else
//            {
//
//                completion(nil,"0")
//                shoppingVal = "0"
//                self.getAllManufacturerZipCodes()
//                completion(nil,String(shoppingVal))
//            }
//        }
//
//    }

	//----------------------------------------
	static func UpdateUserZip(ZipCode:String,completion: @escaping (Error?, String?) -> Void)
	{
        let userid = UserDefaults.standard.value(forKey:"UserID") as? String ?? ""
        let parametersDic: [String:Any] = ["zipcode":ZipCode,"userID":userid]
        
		  AFManager.request("\(baseUrl)/CreateUserZip", method: .post, parameters: parametersDic,encoding: JSONEncoding.default, headers: nil).responseJSON
		  {
				responseresult in
				if let resjson = responseresult.result.value as? [String:Any]
				{
//debugPrint(responseresult)
					if isGarageApp
					{
						shoppingVal = "1"
					} else {
						let shopping = resjson["shopping"] as? Int ?? 0
						shoppingVal = String(shopping)
					}

					 self.getAllManufacturerZipCodes()
					 completion(nil,shoppingVal)

				} else {
					 shoppingVal = "0"
					 self.getAllManufacturerZipCodes()
					 completion(nil,shoppingVal)
				}
		  }
    }

    //----------------------------------------
    static func getAllManufacturerZipCodes()
    {
        AFManager.request("\(baseUrl)/GetAllManufacturerZipCodes", method: .get, parameters: nil,encoding: JSONEncoding.default, headers: nil).responseJSON
        {
            responseresult in
            if let resjson = responseresult.result.value as? [[String:Any]]
            {
                manufactureZipcodes = [String:Any]()
                //debugPrint(responseresult)
                for dic in resjson
                {
                   
                    manufactureZipcodes = manufactureZipcodes.mergedWith(otherDictionary: dic)
                }
                if manufactureZipcodes.count>0
                {
                let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
                let filename = "ManufacturerZipCodes.txt"
                let filenameTemppath =  paths[0].appendingPathComponent(filename)
               
                do {
                    
                    let jsonData: Data? = try? JSONSerialization.data(withJSONObject: manufactureZipcodes)
                    try jsonData!.write(to: filenameTemppath)
                } catch {
                    //print("Couldn't write file")
                }
                }

            }else
            {
            }
        }
    }
    
    static func getTwoCarLocalData() -> [String] {
        return ["1_s1.jpg", "1_s2.jpg", "1_s3.jpg"]
    }
    
    static func getOneCarLocalData() -> [String] {
        return ["1_s8.jpg", "1_s10.jpg", "1_s12.jpg"]
    }
    static func updateFullsizeImageWithUserID(fullsizeImageID:String)
    {
        guard let userid = UserDefaults.standard.value(forKey:"UserID") as? String else{
            return
        }
        
        let parametersDic: [String:Any] =  ["userID":userid,"fullsizeimageID":fullsizeImageID]
        
        AFManager.request("\(baseUrl)/UpdateFullSizeImage", method: .post, parameters: parametersDic,encoding: JSONEncoding.default, headers: nil).responseJSON {
            responseresult in
            if let resjson = responseresult.result.value as? [String:Any]
            {
                //debugPrint(responseresult)
                //debugPrint(resjson)
                let fullsizeimageID = (resjson["fullsizeimageID"] as? String) ?? ""
                if fullsizeimageID.count>0
                {
                    DataManager.removeImageIDFromFile(fullsizeImageID:fullsizeimageID)
                }
            }else
            {
                
            }
        }
    }
       
    static func loadManufacturerZipCodes() -> [String:Any]
    {
        var manfZips:[String:Any] = [String:Any]()
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = dir.appendingPathComponent("ManufacturerZipCodes.txt")
            
            if FileManager.default.fileExists(atPath:fileURL.path) {
                
                //print("FILE AVAILABLE")
                
                do {
                    let contents = try String(contentsOf: fileURL, encoding: .utf8)
                    //print(contents)
                    let data :Data = contents.data(using: String.Encoding.utf8)! as Data
                    
                    if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String:Any]
                    {
                        //print(jsonArray) // use the json here
                        manfZips = jsonArray
                        return manfZips
                    } else {
                        //print("bad json")
                        return manfZips

                    }
                    
                } catch {
                    //print("Couldn't read file.")
                   return manfZips

                }
                
            }else
            {
                return manfZips
            }
        }else
        {
            return manfZips
        }
       
    }
    static func loadPaintTextFile(fileName:String) -> [Any] //change add 2.0.1
    {
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = dir.appendingPathComponent(fileName)
            
            if FileManager.default.fileExists(atPath:fileURL.path) {
                
                //print("FILE AVAILABLE")
                
                do {
                    let contents = try String(contentsOf: fileURL, encoding: .utf8)
                    //print(contents)
                    let data :Data = contents.data(using: String.Encoding.utf8)! as Data
                    
                    if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Any]
                    {
                        //print(jsonArray) // use the json here
                        
                        return jsonArray
                    } else {
                        //print("bad json")
                        
                    }
                    
                } catch {
                    //print("Couldn't read file.")
                    return []
                }
                
            }
        }
        return []
    }
    static func loadFullSizeTextFile(fileName:String) -> Array<String>
    {
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = dir.appendingPathComponent(fileName)
            
            if FileManager.default.fileExists(atPath:fileURL.path) {
                
                //print("FILE AVAILABLE")
                
                do {
                    let contents = try String(contentsOf: fileURL, encoding: .utf8)
                    //print(contents)
                    let data :Data = contents.data(using: String.Encoding.utf8)! as Data
                    
                    if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String]
                    {
                        //print(jsonArray) // use the json here
                        
                        return jsonArray
                    } else {
                        //print("bad json")
                        
                    }
                    
                } catch {
                    //print("Couldn't read file.")
                    return []
                }
                
            }
        }
        return []
    }
    
    static func removeImageIDFromFile(fullsizeImageID:String)
    {
        var tempArray = [String]()
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let filename = "FullSizedImageIDs.txt"
        let filenameTemppath =  paths[0].appendingPathComponent(filename)
        let fullsizedArray = loadFullSizeTextFile(fileName:filename)
        if fullsizedArray.count > 0
        {
            tempArray = fullsizedArray
            if let index = tempArray.firstIndex(where: {$0 == fullsizeImageID}) {
                tempArray.remove(at: index)
            }
            
        }
        do {
            
            let jsonData: Data? = try? JSONSerialization.data(withJSONObject: tempArray)
            
            try jsonData!.write(to: filenameTemppath)
        } catch {
            //print("Couldn't write file")
        }
    }
    static func updateUserIDforOldImageIDs()
    {
        let fullSizedImageIDSArray = DataManager.loadFullSizeTextFile(fileName:"FullSizedImageIDs.txt")
        if fullSizedImageIDSArray.count > 0
        {
            for fullSizeImgID in fullSizedImageIDSArray
            {
                DataManager.updateFullsizeImageWithUserID(fullsizeImageID:fullSizeImgID)
            }
        }
    }
    static func uploadImageIDIntoFile(fullsizeImageID:String)
    {
        var tempArray = [fullsizeImageID]
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let filename = "FullSizedImageIDs.txt"
        let filenameTemppath =  paths[0].appendingPathComponent(filename)
        let fullsizedArray = loadFullSizeTextFile(fileName:filename)
        if fullsizedArray.count > 0
        {
            tempArray = fullsizedArray
            
            if !tempArray.contains(fullsizeImageID)
            {
                tempArray.append(fullsizeImageID)
            }
            
        }
        
        do {
            
            let jsonData: Data? = try? JSONSerialization.data(withJSONObject: tempArray)
            
            try jsonData!.write(to: filenameTemppath)
        } catch {
            //print("Couldn't write file")
        }
    }
    

    //******* change 0.444.24 API for delete Image
   /* static func deleteImageFromServerAPI(fileName:String, completion: @escaping (Error?, String?) -> Void)
    {
        let headers: HTTPHeaders = [
            
            "Content-Type":"application/json",
            "Accept": "application/json"
        ]
     
        let userid = UserDefaults.standard.value(forKey:"UserID") ?? ""
        let newFileName = fileName.replacingOccurrences(of:".jpg", with: "")

        let parameters = ["userID": userid as! String,"filename":newFileName]
            LoadingIndicatorView.show("Please wait")
      AFManager.request("\(baseUrl)/DeleteProjectForUser", method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: headers).responseJSON
      { response in
          //debugPrint(response)
          LoadingIndicatorView.hide()
         if (response.result.error != nil)
            {
                self.checkErrorType(errorcode: response.error!._code)
              completion(nil,nil)
              return
          }

          if let JSON  = response.result.value as? [String:Any]
          {
              print("JSON: \(JSON)") // Works!
              if (JSON["result"] != nil)
              {
                  if JSON["result"]as! String == "failure"
                  {
                      self.showError(errorMessage: JSON["message"]as! String)
                       completion(nil,nil)
                      return
                  }
              }
              
              completion(nil,nil)
          }
      }
        
    }*/
    static func uploadEditedROIS(fullSizeImageID: String, txtUrl: String, completion: @escaping (Error?, String?) -> Void)
    {
        let headers: HTTPHeaders = [
            
            "Content-Type":"application/json",
            "Accept": "application/json"
        ]
        
        //let txtfileName = txtUrl.replacingOccurrences(of:".jpg", with: "_houseObject.txt")
       let pathFileName = txtUrl.replacingOccurrences(of:".jpg", with: "_houseObject.txt")
        
        var textfileConvString:Data? = nil
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        {
            let fileURL = dir.appendingPathComponent(pathFileName)
            
            do
            {
                //print("upload Edited ROIS contents:",contents as Any)
                textfileConvString = try Data(contentsOf: fileURL)

                //print("upload Edited ROIS textfileConvString:",textfileConvString as Any)
            } catch {
                // contents could not be loaded
                completion(nil,nil)
            }
            
        }else
        {
            completion(nil,nil)
            return
        }
        
        let parameters =
            [
                "fullsizeimageID": fullSizeImageID
        ]
        
        AFManager.upload(multipartFormData:
            {  multipartFormData in
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                }
                
                
                if textfileConvString != nil
                {
                    multipartFormData.append(textfileConvString!, withName: "editedrois_file", fileName: pathFileName, mimeType: "text/plain")
                }
                
        },
                         usingThreshold:UInt64.init(),
                         to:"\(baseUrl)/UpdateEditedFullSizeImage",
            method:.post,
            headers:headers,
            encodingCompletion:
            { encodingResult in
                switch encodingResult
                {
                case .success(let upload,_,_ ):
                    upload.responseJSON
                        { response in
                            
                        print("upload Edited ROIS response:",response as Any)
                            //debugPrint(response)
                            LoadingIndicatorView.hide()
                            
                            if (response.result.value as? NSDictionary) != nil
                            {
                                completion(nil,nil)
                            } else {
                                completion(nil,nil)
                            }
                    }
                    completion(nil,nil)
                    //  upload.responseString(completionHandler: { response in
                    //    //print("success", response.result.value as Any)
                    
                //  })
                case .failure(let encodingError):
                    print(encodingError)
                    completion(nil,nil)
                }
        })
        
        
    }
    static func UpdateFavoriteScenes(dataToConve:Data,filename:String,typeName:String)
    {
        
        guard let userid = UserDefaults.standard.value(forKey:"UserID") else{return}
        let fileArray = filename.components(separatedBy:"__")
        let filenameTemp = fileArray[1]
        let folderName = fileArray[0].components(separatedBy:"/")
        
        let parametersDic: [String:Any] = ["userID":userid as! String,"mainFolderName":folderName[0],"catFolderName": folderName[1] ]
        
        
        AFManager.upload(multipartFormData:{  multipartFormData in
            for (key, value) in parametersDic {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
            }
            multipartFormData.append(dataToConve, withName: typeName, fileName: filenameTemp, mimeType: "text/plain")
        },
                         usingThreshold:UInt64.init(),
                         to:"\(baseUrl)/UpdateSceneFavItem",
            method:.post,
            headers:nil,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload,_,_ ):
                    
                    upload.responseJSON { response in
                        //print(response)
// debugPrint(response)
                        if (response.result.error != nil)
                        {
                           
                            return
                        }
                       
                        if let json = response.result.value as? [String:Any]
                        {
                        debugPrint(json)
                         return
                        }
                    }
                  
                case .failure(let encodingError):
                    print(encodingError)
                    break;
                }
        })
       
        
    }
    //----------------
    static func openCompleteProject(agentID:String,completion: @escaping (Error?,Bool) -> Void)
    {
        //  LoadingIndicatorView.show("Please wait")
        let parametersDic: [String:Any] = ["agentID":agentID]
        
          AFManager.request("\(baseUrl)/ReopenDeletedAgentOrder", method: .post, parameters: parametersDic,encoding: JSONEncoding.default, headers: nil).responseJSON { response in
               debugPrint(response)
             // LoadingIndicatorView.hide()
              if (response.result.error != nil)
              {
                  completion(nil,false)
                //  self.checkErrorType(errorcode: response.error!._code)
                  return
              }
              if let JSON  = response.result.value             {
                debugPrint(JSON)
                completion(nil,true)
                 
              }else{
                completion(nil,false)
              }
          }
    }
    static func completeProject(agentID:String,status:String,completion: @escaping (Error?,Bool) -> Void)
    {
        //  LoadingIndicatorView.show("Please wait")
        let parametersDic: [String:Any] = ["agentID":agentID,"status":status]
        
          AFManager.request("\(baseUrl)/UpdateAgentHomeStatus", method: .post, parameters: parametersDic,encoding: JSONEncoding.default, headers: nil).responseJSON { response in
               debugPrint(response)
             // LoadingIndicatorView.hide()
              if (response.result.error != nil)
              {
                  completion(nil,false)
                //  self.checkErrorType(errorcode: response.error!._code)
                  return
              }
              if let JSON  = response.result.value             {
                  debugPrint(JSON)
                completion(nil,true)
                 
              }else{
                completion(nil,false)
              }
          }
    }
    static func GetFavoriteScenes()
    {
        
        guard let userid = UserDefaults.standard.value(forKey:"UserID") else{return}
      //  LoadingIndicatorView.show("Please wait")
        let parametersDic: [String:Any] = ["userID":userid as! String]
        AFManager.request("\(baseUrl)/GetFavoriteScenes", method: .post, parameters: parametersDic,encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            //debugPrint(response)
           // LoadingIndicatorView.hide()
            if (response.result.error != nil)
            {
              //  self.checkErrorType(errorcode: response.error!._code)
                return
            }
            if let JSON  = response.result.value             {
                //   LoadingIndicatorView.hide()
                if let checkoutlist = JSON as? NSArray
                {
                    self.downloadFavSceneFiles(fromArray:checkoutlist)
                }
                
               
            }else{
               
            }
        }
        
    }
    static func GetPaintColors()
    {
        
        guard let userid = UserDefaults.standard.value(forKey:"UserID") else{return}
     //   LoadingIndicatorView.show("Please wait")
        let parametersDic: [String:Any] = ["userID":userid as! String]
        AFManager.request("\(baseUrl)/UserPaintColors", method: .post, parameters: parametersDic,encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            //debugPrint(response)
          //  LoadingIndicatorView.hide()
            if (response.result.error != nil)
            {
              //  self.checkErrorType(errorcode: response.error!._code)
                return
            }
            if let JSON  = response.result.value             {
                //   LoadingIndicatorView.hide()
               
                if let checkoutlist = JSON as? NSDictionary
                               {
                                if checkoutlist[userid as! String] != nil
                                               {
                                                let arrayTemp = checkoutlist[userid as! String] as? NSArray ?? []
                                   self.downloadFavSceneFiles(fromArray:arrayTemp)
                               }
                }
            }else{
               
            }
        }
        
    }
    static func downloadFavSceneFiles(fromArray:NSArray)
    {
                  
        for dic in fromArray
        {
            let tempDic = dic as? [String:Any]
            if tempDic != nil && tempDic!.count > 0
            {
                for (key, value) in tempDic! {
                    if (key == "favFullImage" || key == "favDoor" || key == "favGarage")
                    {
                    if let fileurl1 = value as? String
                    {
                    var fileurlName1 = (fileurl1 as NSString).lastPathComponent
                    let filearray1 = fileurlName1.components(separatedBy:"--")
                    fileurlName1 = filearray1[0] + ".jpg"
                        self.downloadScenesUsingAlamofire(url: URL(string: fileurl1 )!, fileName: fileurlName1, folderName: tempDic!["mainFolderName"] as! String, catFoldername:tempDic!["catFolderName"] as! String)

                    }
                    }else if (key == "doorName" || key == "garagerName" || key == "doorDesc" || key == "garagerDesc")
                    {
                    if let fileurl1 = value as? String
                    {
                    var fileurlName1 = (fileurl1 as NSString).lastPathComponent
                    let filearray1 = fileurlName1.components(separatedBy:"--")
                    fileurlName1 = filearray1[0] + ".txt"
                        self.downloadScenesUsingAlamofire(url: URL(string: fileurl1 )!, fileName: fileurlName1, folderName: tempDic!["mainFolderName"] as! String, catFoldername:tempDic!["catFolderName"] as! String)

                    }
                    }else if (key == "paint_url" || key == "paintmask_url")
                    {
                    if let fileurl1 = value as? String
                    {
                        var fileurlName1 = (fileurl1 as NSString).lastPathComponent
                        let filearray1 = fileurlName1.components(separatedBy:"--")
                        var addition = ""
                        if filearray1.count == 3
                        {
                            let ext = filearray1[1].components(separatedBy:"_")
                            if ext.count>2
                            {
                               addition = "_" + ext[1] + "_" + ext [2]
                            }else
                            {
                                addition = "_" + ext[1]
                            }
                        }
                        
                            fileurlName1 = filearray1[0] + addition + ".png"
                            
                       
                        AppDelegate.downloadUsingAlamofire(url: URL(string: fileurl1 )!, fileName: fileurlName1)
                    }
                    }else if (key == "selected_url" || key == "defaultcolor_url" || key == "startcolor_url" || key == "paintthumb_url")
                    {
                    if let fileurl1 = value as? String
                    {
                        var fileurlName1 = (fileurl1 as NSString).lastPathComponent
                        let filearray1 = fileurlName1.components(separatedBy:"--")
                        var addition = ""
                        if filearray1.count == 3
                        {
                            let ext = filearray1[1].components(separatedBy:"_")
                            if ext.count>2
                            {
                                addition = "_" + ext[1] + "_" + ext [2]
                            }else
                            {
                                addition = "_" + ext[1]
                            }
                        }
                        
                        fileurlName1 = filearray1[0] + addition + ".txt"
                        
                        AppDelegate.downloadUsingAlamofire(url: URL(string: fileurl1 )!, fileName: fileurlName1)
                        
                        }
                    }
                      
                }
            
        }
    }
    }
    static func addSceneIDInFolder(catArray:Array<NSDictionary>,sceneID: String,folderName:String,catFoldername:String)
       {
           
                           let DocumentDirectory = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])

                           if let filePath = DocumentDirectory.appendingPathComponent(folderName)
                           {
                               
           
                               let filePath00 = filePath.appendingPathComponent(catFoldername)

                            let filenameScene = sceneID + ".txt"
                            let fileURL = filePath00.appendingPathComponent(filenameScene)
                            try? sceneID.write(to: fileURL, atomically: true, encoding: String.Encoding.utf8)

           }

       }
   static func downloadScenesUsingAlamofire(url: URL,fileName: String,folderName:String,catFoldername:String)
   {
    
    
    let fileManager = FileManager.default
    let DocumentDirectory = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
    
    if let filePath = DocumentDirectory.appendingPathComponent(folderName)
    {
        if !fileManager.fileExists(atPath: filePath.path)
        {
            //print("didn't exist")
            do
            {
                try fileManager.createDirectory(atPath: filePath.path, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                print("Unable to create directory \(error.debugDescription)")
            }
        }
        
        let filePath00 = filePath.appendingPathComponent(catFoldername)
        
        if !fileManager.fileExists(atPath: filePath00.path)
        {
            //print("didn't exist 2 ")
            do
            {
                try fileManager.createDirectory(atPath: filePath00.path, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                print("catId folder Unable to create directory \(error.debugDescription)")
            }
        }
        let fileURL = filePath00.appendingPathComponent(fileName)
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            
            return (fileURL, [.removePreviousFile])
        }
        
        Alamofire.download(url, to: destination).response
            { response in
                
                if (response.destinationURL?.path) != nil
                {
//debugPrint("downloaded - \(url.absoluteString) ")
                } else {
debugPrint("failed downloading - \(url.absoluteString) ")
                }
        }
    }
    
    
    
    
    }
    
    static func uploadPaintImageAndColors(fullImgID:String,fileName:String,wellSel:String,agentID:String,project:Project, completion: @escaping (Error?, String?) -> Void)

        {
            let headers: HTTPHeaders = [
                
                "Content-Type":"application/json",
                "Accept": "application/json"
            ]
         guard let userid = UserDefaults.standard.value(forKey:"UserID") as? String else{
             return
         }
           let paintChoiceName = fileName + "_projectChoices"+"_\(wellSel)"+".txt"

                  var paintThumbfileConvString:Data? = nil
                         
                             do
                             {
                                NSKeyedArchiver.setClassName("Project", for: Project.self)

                                 paintThumbfileConvString = try NSKeyedArchiver.archivedData(withRootObject: project, requiringSecureCoding: false)

                             } catch {
                                 // contents could not be loaded
                                 completion(nil,nil)
                             }
                             
                       
                         

            let parameters = [
                "userID": userid,"filename":paintChoiceName,"agentID":agentID
            ]
            AFManager.upload(multipartFormData:{  multipartFormData in
                for (key, value) in parameters {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                }
                
                if paintThumbfileConvString != nil
                {
                    multipartFormData.append(paintThumbfileConvString!, withName: "paintthumb_url", fileName: paintChoiceName, mimeType: "text/plain")
                }
                
            },
                                 usingThreshold:UInt64.init(),
                                 to:"\(baseUrl)/UpdatePaintColor",
                                 method:.post,
                                 headers:headers,
                                 encodingCompletion: { encodingResult in
                                    switch encodingResult {
                                    case .success(let upload,_,_ ):
                                        upload.responseJSON { response in
                                     //   print(response)
//debugPrint("response: ",response)
                                                  LoadingIndicatorView.hide()
                                          
                                            if let json = response.result.value as? NSDictionary
                                            {
                                                if (json["result"] != nil)
                                                {
                                                    if json["result"]as! String == "failure"
                                                    {
                                                       // self.showError(errorMessage: json["message"]as! String)
                                                      //  return
                                                    }
                                                }
                                               
                                             // Works!
                                                completion(nil,nil)
                                            }else
                                            {
                                                completion(nil,nil)
                                            }
                                        }
    //                                     completion(nil,nil)
                                        //  upload.responseString(completionHandler: { response in
                                        //    //print("success", response.result.value as Any)
                                        
                                    //  })
                                    case .failure(let encodingError):
                                        print(encodingError)
                                         completion(nil,nil)
                                    }
                })
          
            
        }
    //paint API start******************20/02 changes
   /* static func uploadPaintImageAndColors(fullImgID:String,paintimage:UIImage,paintImageMask:UIImage,fileName:String,type:String,wellSel:String,agentID:String,curProj:Project, completion: @escaping (Error?, String?) -> Void)

    {
        let headers: HTTPHeaders = [
            
            "Content-Type":"application/json",
            "Accept": "application/json"
        ]
     guard let userid = UserDefaults.standard.value(forKey:"UserID") as? String else{
         return
     }
       
        let startColorFilename = fileName + "_startColor"+type+"_\(wellSel)"+".txt"
        let defaultColorFilename = fileName + "_defaultColor"+type+"_\(wellSel)"+".txt"
        let paintmaskName = fileName + "_paintMask"+type+"_\(wellSel)"+".png"
        let paintName = fileName + "_paint"+type+"_\(wellSel)"+".png"
        let wasWell0Sel = fileName + "_wasWell0Sel"+"_\(wellSel)"+".txt"
        


        var strtclrConvString:Data? = nil
        var defClrConvString:Data? = nil
      var wasWellConvString:Data? = nil
        
       if maskedHouseImage0 != nil
        {
            currentButtonToDisplay = 0
        }
        if maskedHouseImage1 != nil
               {
                   currentButtonToDisplay = 1
               }
        if maskedHouseImage2 != nil
               {
                   currentButtonToDisplay = 2
               }
        let dic:[String:Any] = ["wasWell0Sel" : curProj.well0 ? 1 : 0,"wasWell1Sel" : curProj.well1 ? 1 : 0 ,"wasWell2Sel" : curProj.well2 ? 1 : 0,"wellNum":currentWellNum ,"wellNumTot":currentButtonToDisplay]
        wasWellConvString = try? JSONSerialization.data(withJSONObject: dic)

        var defColor = defaultL ineColor0
        var startColor = origSt artColor0

        switch type {
        case "0":
           defColor = defaultLi neColor0
            startColor = origSt artColor0
           
        case "1":
            defColor = defaultL ineColor1
            startColor = origSta rtColor1
           
            case "2":
             defColor = defaultL ineColor2
             startColor = origSta rtColor2
        default:
           defCo lor = defaultD oorColor0
          startColor = origSta rtColor0
        }
                  do {
                             
                       defClrConvString = try NSKeyedArchiver.archivedData(withRootObject: defColor, requiringSecureCoding: false)
                       strtclrConvString = try NSKeyedArchiver.archivedData(withRootObject: startColor
                                      , requiringSecureCoding: false)
                              }catch
                              {
                                  
                              }

        guard let paint_imageData = paintimage.pngData() else {
            let error = NSError(domain:"com.modifai", code:401, userInfo:[ NSLocalizedDescriptionKey: "Invalid Image"])
            completion(error,nil)
            return
        }

        let parameters = [
            "userID": userid,"filename":paintName,"agentID":agentID
        ]
            AFManager.upload(multipartFormData:{  multipartFormData in
                for (key, value) in parameters {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                }
               
                                           
                multipartFormData.append(paint_imageData, withName: "paint", fileName: paintName, mimeType: "image/jpeg")
               
                      guard let paintmask_imageData = paintImageMask.pngData() else {
                          let error = NSError(domain:"com.modifai", code:401, userInfo:[ NSLocalizedDescriptionKey: "Invalid Image"])
                          completion(error,nil)
                          return
                      }
//
                    multipartFormData.append(paintmask_imageData, withName: "paintmask", fileName: paintmaskName, mimeType: "image/jpeg")
                

                if strtclrConvString != nil
                {

                multipartFormData.append(strtclrConvString!, withName: "startcolor", fileName: startColorFilename, mimeType: "text/plain")
                }
                if defClrConvString != nil
                {
                   multipartFormData.append(defClrConvString!, withName: "defaultcolor", fileName: defaultColorFilename, mimeType: "text/plain")
                }
                if wasWellConvString != nil
                {
                    multipartFormData.append(wasWellConvString!, withName: "_wasWell0Sel", fileName: wasWell0Sel, mimeType: "text/plain")
                }
            },
                             usingThreshold:UInt64.init(),
                             to:"\(baseUrl)/UpdatePaintColor",
                             method:.post,
                             headers:headers,
                             encodingCompletion: { encodingResult in
                                switch encodingResult {
                                case .success(let upload,_,_ ):
                                    upload.responseJSON { response in
                                 //   print(response)
debugPr int("response: ",response)
                                              LoadingIndicatorView.hide()
                                      
                                        if let json = response.result.value as? NSDictionary
                                        {
                                            if (json["result"] != nil)
                                            {
                                                if json["result"]as! String == "failure"
                                                {
                                                   // self.showError(errorMessage: json["message"]as! String)
                                                  //  return
                                                }
                                            }
                                           
                                         // Works!
                                            completion(nil,nil)
                                        }else
                                        {
                                            completion(nil,nil)
                                        }
                                    }
//                                     completion(nil,nil)
                                    //  upload.responseString(completionHandler: { response in
                                    //    //print("success", response.result.value as Any)
                                    
                                //  })
                                case .failure(let encodingError):
                                    print(encodingError)
                                     completion(nil,nil)
                                }
            })
      
        
    }*/
    //paint API end******************20/02 changes
    //MARK - Agent Document changes
    static func loadImagesFromAgentDocumentDirectory() -> [URL]
       {
               //print("loadImage From Document Directory")
        let folderName = "Agent_Homes"
        let fileManager = FileManager.default
        let DocumentDirectory = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        if let filePath = DocumentDirectory.appendingPathComponent(folderName)
        {
            if (fileManager.fileExists(atPath: filePath.path))
            {
                do
                {
                    let folderURLs = try fileManager.contentsOfDirectory(at: filePath, includingPropertiesForKeys: nil, options: .skipsHiddenFiles )
                    return folderURLs
                    } catch {
                                   
                        return []
                        //print("no folders")
                    }
            }else
            {
                return []
            }
            
        }else{
            return []
        }
    }
           
    static func getAgentDetailsFromAgentID(agentID:String) -> AgentList?
          {
              let fullsizedArray = loadPaintTextFile(fileName:"AllAgentHomesData.txt")
              for agentDic in fullsizedArray
              {
                  let agentItem:AgentList = AgentList((agentDic as? NSDictionary)!)
                  
                 
                if agentItem.agentID == agentID
                  {
                      return agentItem
                  }
                  
              }
              let fullsizedArray1 = loadPaintTextFile(fileName:"CompletedHomesData.txt")
                           for agentDic in fullsizedArray1
                           {
                               let agentItem:AgentList = AgentList((agentDic as? NSDictionary)!)
                               
                              
                             if agentItem.agentID == agentID
                               {
                                   return agentItem
                               }
                               
                           }
            let fullsizedArray2 = loadPaintTextFile(fileName:"DeletedHomesData.txt")
            for agentDic in fullsizedArray2
            {
                let agentItem:AgentList = AgentList((agentDic as? NSDictionary)!)
                
               
              if agentItem.agentID == agentID
                {
                    return agentItem
                }
                
            }
            
              return nil
          }
       static func checkAgentFileUrlWithName(imageName:String) -> String //add change 2.0.6
       {
           let fullsizedArray = loadPaintTextFile(fileName:"AllAgentHomesData.txt")
           for agentDic in fullsizedArray
           {
               let agentItem:AgentList = AgentList((agentDic as? NSDictionary)!)
               
               let url = URL(string:agentItem.image_url)
               var imageLastname = url?.lastPathComponent ?? ""
            let nameArray = imageLastname.components(separatedBy:"--")
            
            if nameArray.count > 1
            {
               imageLastname = nameArray[0] + ".jpg"
            }else
            {
              imageLastname = imageLastname.changeExtensions(name:imageLastname)

            }
               if imageLastname == imageName
               {
                   return agentItem.agentID
               }
               
           }
           let fullsizedArray1 = loadPaintTextFile(fileName:"CompletedHomesData.txt")
           for agentDic in fullsizedArray1
           {
               let agentItem:AgentList = AgentList((agentDic as? NSDictionary)!)
               
               let url = URL(string:agentItem.image_url)
               var imageLastname = url?.lastPathComponent ?? ""
            let nameArray = imageLastname.components(separatedBy:"--")
            
            if nameArray.count > 1
            {
               imageLastname = nameArray[0] + ".jpg"
            }else
            {
              imageLastname = imageLastname.changeExtensions(name:imageLastname)

            }
               if imageLastname == imageName
               {
                   return agentItem.agentID
               }
               
           }
        let fullsizedArray2 = loadPaintTextFile(fileName:"DeletedHomesData.txt")
                  for agentDic in fullsizedArray2
                  {
                      let agentItem:AgentList = AgentList((agentDic as? NSDictionary)!)
                      
                      let url = URL(string:agentItem.image_url)
                    var imageLastname = url?.lastPathComponent ?? ""
                 let nameArray = imageLastname.components(separatedBy:"--")
                 
                 if nameArray.count > 1
                 {
                    imageLastname = nameArray[0] + ".jpg"
                 }else
                 {
                   imageLastname = imageLastname.changeExtensions(name:imageLastname)

                 }
                    if imageLastname == imageName
                    {
                          return agentItem.agentID
                      }
                      
                  }
           return ""
       }
    static func replaceURLInAgent(urlString:String,replaceStr:String) //************* change add 2.0.6 adding newname
    {
        var fullsizedArray = loadPaintTextFile(fileName:"AllAgentHomesData.txt")
        for (index, agentDic) in fullsizedArray.enumerated()
        {
            
            if let dic = (agentDic as? NSDictionary)?.mutableCopy() as? NSMutableDictionary
            {
            let imageLastname = dic["image_url"] as? String ?? ""
          
            if imageLastname == urlString
            {
                dic["image_url"] = replaceStr
                fullsizedArray.append(dic.copy() as? NSDictionary ?? [:])
                fullsizedArray.remove(at: index)
                break
            }
            }
        }
       
        if fullsizedArray.count > 0
        {
            
            let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            let filename = "AllAgentHomesData.txt"
            let filenameTemppath =  paths[0].appendingPathComponent(filename)
            
            
            do {
                
                let jsonData: Data? = try? JSONSerialization.data(withJSONObject: fullsizedArray)
                try jsonData!.write(to: filenameTemppath)
            } catch {
                //print("Couldn't write file")
            }
            
        }
       
    }
    //MARK:- Agent Feed Update
    static func agentHouseFeedUpdate(agentID:String,status:String)
    {
        let parametersDic: [String:Any] = ["agentID":agentID,"feed":status]
        
          AFManager.request("\(baseUrl)/UpdateFeedStatus", method: .post, parameters: parametersDic,encoding: JSONEncoding.default, headers: nil).responseJSON { response in
               debugPrint(response)
           
              if let JSON  = response.result.value
              {
                  debugPrint(JSON)
                 
              }else{
               
              }
          }
    }
}

extension Dictionary {
    func mergedWith(otherDictionary: [Key: Value]) -> [Key: Value] {
        var mergedDict: [Key: Value] = [:]
        [self, otherDictionary].forEach { dict in
            for (key, value) in dict {
                mergedDict[key] = value
            }
        }
        return mergedDict
    }
}

