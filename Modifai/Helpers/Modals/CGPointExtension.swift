import UIKit

//------------------------------------------
func ccpLength(_ v: CGPoint) -> CGFloat
{
	return sqrt(ccpLengthSQ(v))
}

//------------------------------------------
func ccpDistance(_ p1: CGPoint, _ p2: CGPoint) -> CGFloat
{
	return ccpLength(ccpSub(p1, p2))
}

//------------------------------------------
func ccpNormalize(_ v: CGPoint) -> CGPoint
{
	return ccpMult(v, 1.0 / ccpLength(v))
}

//------------------------------------------
func ccpForAngle(_ a: CGFloat) -> CGPoint
{
	return ccp(CGFloat(cos(CGFloat(a))), CGFloat(sin(CGFloat(a))))
}

//------------------------------------------
func ccpToAngle(_ v: CGPoint) -> CGFloat
{
	return CGFloat(atan2(CGFloat(v.y), CGFloat(v.x)))
}

//------------------------------------------
func ccpLerp(_ a: CGPoint, _ b: CGPoint, _ alpha: CGFloat) -> CGPoint
{
	return ccpAdd(ccpMult(a, 1.0 - alpha), ccpMult(b, CGFloat(alpha)))
}

//------------------------------------------
func clampf(_ value: CGFloat, _ min_inclusive0: CGFloat, _ max_inclusive0: CGFloat) -> CGFloat
{
	var min_inclusive = min_inclusive0;
	var max_inclusive = max_inclusive0;

	if min_inclusive > max_inclusive
	{
		let tmp = min_inclusive
		min_inclusive = max_inclusive
		max_inclusive = tmp
	}

	return value < min_inclusive ? min_inclusive : value < max_inclusive ? value : max_inclusive
}

//------------------------------------------
func ccpClamp(_ p: CGPoint, _ min_inclusive: CGPoint, _ max_inclusive: CGPoint) -> CGPoint
{
	return ccp(CGFloat(clampf(CGFloat(p.x), CGFloat(min_inclusive.x), CGFloat(max_inclusive.x))),
			   CGFloat(clampf(CGFloat(p.y), CGFloat(min_inclusive.y), CGFloat(max_inclusive.y))))
}

//------------------------------------------
func ccpFromSize(_ s: CGSize) -> CGPoint
{
	return ccp(s.width, s.height)
}

//------------------------------------------
func ccpCompOp(_ p: CGPoint, _ opfunc: (@convention(c) (CGFloat) -> CGFloat)!) -> CGPoint
{
	return ccp(CGFloat(opfunc(CGFloat(p.x))), CGFloat(opfunc(CGFloat(p.y))))
}

//------------------------------------------
func ccpFuzzyEqual(_ a: CGPoint, _ b: CGPoint, _ parit: CGFloat) -> Bool
{
	if a.x - CGFloat(parit) <= b.x && b.x <= a.x + CGFloat(parit)
	{
		if a.y - CGFloat(parit) <= b.y && b.y <= a.y + CGFloat(parit)
		{
			return true
		}
	}

	return false
}

//------------------------------------------
func ccpCompMult(_ a: CGPoint, _ b: CGPoint) -> CGPoint
{
	return ccp(a.x * b.x, a.y * b.y)
}

//------------------------------------------
func ccpAngleSigned(_ a: CGPoint, _ b: CGPoint) -> CGFloat
{
	let a2 = ccpNormalize(a)
	let b2 = ccpNormalize(b)
	let angle = atan2(CGFloat(a2.x * b2.y - a2.y * b2.x), ccpDot(a2, b2))

	if abs(angle) < CGFloat.ulpOfOne
	{
		return 0.0
	}

	return angle
}

//------------------------------------------
func ccpRotateByAngle(_ v: CGPoint, _ pivot: CGPoint, _ angle: CGFloat) -> CGPoint
{
	var r:CGPoint = ccpSub(v, pivot)
	let cosa:CGFloat = cos(angle)
	let sina:CGFloat = sin(angle)
	let t:CGFloat = r.x

	r.x = t * cosa - r.y * sina + pivot.x
	r.y = t * sina + r.y * cosa + pivot.y

	return r
}

//------------------------------------------
func ccpSegmentIntersect(_ A: CGPoint, _ B: CGPoint, _ C: CGPoint, _ D: CGPoint) -> Bool
{
	let bundlePt = ccpLineIntersect(A, B, C, D)

	if (bundlePt.wasGood && (bundlePt.s >= 0.0 && bundlePt.s <= 1.0 && bundlePt.t >= 0.0 && bundlePt.t <= 1.0))
	{
		return true
	}

	return false
}

//------------------------------------------
func ccpIntersectPoint(_ A: CGPoint, _ B: CGPoint, _ C: CGPoint, _ D: CGPoint) -> CGPoint
{
	let bundlePt = ccpLineIntersect(A, B, C, D)

	if (bundlePt.wasGood)
	{
		var P: CGPoint = CGPoint()
		P.x = A.x + CGFloat(bundlePt.s) * (B.x - A.x)
		P.y = A.y + CGFloat(bundlePt.s) * (B.y - A.y)

		return P
	}

	return CGPoint.zero
}

//------------------------------------------
func ccpLineIntersect(_ A: CGPoint, _ B: CGPoint, _ C: CGPoint, _ D: CGPoint) -> (wasGood:Bool, s:CGFloat, t:CGFloat)
{
	var s:CGFloat = 0.0
	var t:CGFloat = 0.0

	if (A.x == B.x && A.y == B.y) || (C.x == D.x && C.y == D.y)
	{
		return (false,s,t)
	}

	let BAx = CGFloat(B.x - A.x)
	let BAy = CGFloat(B.y - A.y)
	let DCx = CGFloat(D.x - C.x)
	let DCy = CGFloat(D.y - C.y)
	let ACx = CGFloat(A.x - C.x)
	let ACy = CGFloat(A.y - C.y)
	let denom = DCy * BAx - DCx * BAy

	s = DCx * ACy - DCy * ACx
	t = BAx * ACy - BAy * ACx

	if (denom < CGFloat.ulpOfOne)
	{
		if (s == 0.0 || t == 0.0)
		{
			return (true,s,t)
		}

		return (false,s,t)
	}

	s = s / denom
	t = t / denom

	return (true,s,t)
}

//------------------------------------------
func ccpAngle(_ a: CGPoint, _ b: CGPoint) -> CGFloat
{
	let angle:CGFloat = acos(ccpDot(ccpNormalize(a), ccpNormalize(b)))

	if abs(angle) < CGFloat.ulpOfOne
	{
		return 0.0
	}

	return angle
}

//------------------------------------------
func ccp(_ x: CGFloat, _ y: CGFloat) -> CGPoint
{
	return CGPoint(x: x, y: y)
}

//------------------------------------------
func ccpNeg(_ v: CGPoint) -> CGPoint
{
	return ccp(-v.x, -v.y)
}

//------------------------------------------
func ccpAdd(_ p1: CGPoint, _ p2: CGPoint) -> CGPoint
{
	return ccp(p1.x + p2.x, p1.y + p2.y)
}

//------------------------------------------
func ccpSub(_ p1: CGPoint, _ p2: CGPoint) -> CGPoint
{
	return ccp(p1.x - p2.x, p1.y - p2.y)
}

//------------------------------------------
func ccpMult(_ v: CGPoint, _ s: CGFloat) -> CGPoint
{
	return ccp(v.x * s, v.y * s)
}

//------------------------------------------
func ccpMidpoint(_ p1: CGPoint, _ p2: CGPoint) -> CGPoint
{
	return ccpMult(ccpAdd(p1, p2), 0.5)
}

//------------------------------------------
func ccpDot(_ p1: CGPoint, _ p2: CGPoint) -> CGFloat
{
	return p1.x * p2.x + p1.y * p2.y
}

//------------------------------------------
func ccpCross(_ p1: CGPoint, _ p2: CGPoint) -> CGFloat
{
	return p1.x * p2.y - p1.y * p2.x
}

//------------------------------------------
func ccpPerp(_ v: CGPoint) -> CGPoint
{
	return ccp(-v.y, v.x)
}

//------------------------------------------
func ccpRPerp(_ v: CGPoint) -> CGPoint
{
	return ccp(v.y, -v.x)
}

//------------------------------------------
func ccpProject(_ p1: CGPoint, _ p2: CGPoint) -> CGPoint
{
	return ccpMult(p2, ccpDot(p1, p2) / ccpDot(p2, p2))
}

//------------------------------------------
func ccpRotate(_ p1: CGPoint, _ p2: CGPoint) -> CGPoint
{
	return ccp(p1.x * p2.x - p1.y * p2.y, p1.x * p2.y + p1.y * p2.x)
}

//------------------------------------------
func ccpUnrotate(_ p1: CGPoint, _ p2: CGPoint) -> CGPoint
{
	return ccp(p1.x * p2.x + p1.y * p2.y, p1.y * p2.x - p1.x * p2.y)
}

//------------------------------------------
func ccpLengthSQ(_ v: CGPoint) -> CGFloat
{
	return ccpDot(v, v)
}

//------------------------------------------
func ccpDistanceSQ(_ p1: CGPoint, _ p2: CGPoint) -> CGFloat
{
	return ccpLengthSQ(ccpSub(p1, p2))
}

//-----------------------------------------------------
func closeToXY(_ p1:CGPoint, _ p2:CGPoint, _ tol:CGFloat) -> Bool
{
	let x:CGFloat = abs(p1.x - p2.x)
	let y:CGFloat = abs(p1.y - p2.y)
//print("closeToXY x,y:",x,y);

	if (x < tol && y < tol) {return true;}
	return false;
}
