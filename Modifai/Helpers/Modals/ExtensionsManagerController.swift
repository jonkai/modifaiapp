import Foundation
import Photos
import AVKit
//var destIma geWidth:CGFloat  = 2048.0;
//var destIma geHeight:CGFloat = 1536.0;
var vertCellWidth:CGFloat = 84.0;

var currentOneCarGar = 0;
var currentTwoCarGar = 0;
var currentSinglFrntDoor = 0;
var currentDoublFrntDoor = 0;
var currentCombinedFrntDoor = 0;  //********** change 0.0.40 add ***


var doorCellWidth: CGFloat = 100.0;
var tempCellWidth: CGFloat = 100.0;

//var containWi dthImage:CGFloat  = 1.0;  //********** change 0.0.40 placed in different swift file ***
//var containHeightImage:CGFloat = 1.0;
//var viewTra nsition = false;
//var phonePortrait:Bool = true;
//var lastPhonePortrait:Bool = true;
//var skipConfigure:Bool = false;
//var curZoom:CGFloat = 1.0;
//var curOffset:CGPoint = CGPoint();
//var curSize:CGSize = CGSize();


//--------------------------------------------
extension UIImageView
{
    //--------------------------------------------
    public func fromGif(resourceName: String)
    {
        guard let path = Bundle.main.path(forResource: resourceName, ofType: "gif") else
        {
  print("Gif does not exist at that path")
            return
        }
        let url = URL(fileURLWithPath: path)
        guard let gifData = try? Data(contentsOf: url),
            let source =  CGImageSourceCreateWithData(gifData as CFData, nil) else { return }
        var images = [UIImage]()
        let imageCount = CGImageSourceGetCount(source)

        for i in 0 ..< imageCount
        {
            if let image = CGImageSourceCreateImageAtIndex(source, i, nil)
            {
                images.append(UIImage(cgImage: image))
            }
        }
    //    let gifImageView = UIImageView(frame: frame)
        self.animationImages = images
//        return gifImageView
    }


    override open var intrinsicContentSize: CGSize
    {
        if let myImage = self.image
        {
            let myImageWidth = myImage.size.width
            let myImageHeight = myImage.size.height
            let myViewWidth = self.frame.size.width
            let ratio = myViewWidth/myImageWidth
            var scaledHeight =  myImageHeight * ratio
            
            if myImageWidth < myImageHeight && scaledHeight > self.frame.height
            {
                self.contentMode = .scaleAspectFit
                scaledHeight = self.frame.height
                
            }
            return CGSize(width: myViewWidth, height: scaledHeight)
        }
        
        return CGSize(width: -1.0, height: -1.0)
    }
    
    var addOrientationChangeHeightForPortrait: CGSize {
        if let myImage = self.image {
            let myImageWidth = myImage.size.width
            let myImageHeight = myImage.size.height
            let myViewWidth = self.frame.size.width
            
            let ratio = myViewWidth/myImageWidth
            var scaledHeight = myImageHeight * ratio
            if myImageWidth < myImageHeight && scaledHeight > self.frame.height
            {
                self.contentMode = .scaleAspectFit
                scaledHeight = self.frame.height
            }
            return CGSize(width: myViewWidth, height: scaledHeight)
        }
        
        return CGSize(width: -1.0, height: -1.0)
    }
}

//----------------------------------------------------------
extension UIImage
{
    //------------------------------------------------------------------------------------
    public class func gifImageWithName(_ name: String) -> UIImage?
    {
        guard let bundleURL = Bundle.main
            .url(forResource: name, withExtension: "gif") else
        {
                print("SwiftGif: This image named \"\(name)\" does not exist")
                return nil
        }

        guard let imageData = try? Data(contentsOf: bundleURL) else
        {
            print("SwiftGif: Cannot turn image named \"\(name)\" into NSData")
            return nil
        }

        return gifImageWithData(imageData)
    }

    //------------------------------------------------------------------------------------
    public class func gifImageWithData(_ data: Data) -> UIImage?
    {
        guard let source = CGImageSourceCreateWithData(data as CFData, nil) else
        {
//print("image data didn't create gif")
            return nil
        }

        return UIImage.animatedImageWithSource(source)
    }

    //----------------------------------------------------------
    class func animatedImageWithSource(_ source: CGImageSource) -> UIImage?
    {
        let count = CGImageSourceGetCount(source)
        var images = [CGImage]()
        var delays = [Int]()

        for i in 0..<count
        {
            if let image = CGImageSourceCreateImageAtIndex(source, i, nil)
            {
                images.append(image)
            }

            let delaySeconds = UIImage.delayForImageAtIndex(Int(i), source: source)
            delays.append(Int(delaySeconds * 1000.0))
        }

        let duration: Int =
        {
            var sum = 0

            for val: Int in delays
            {
                sum += val
            }

            return sum
        }()

        let gcd = gcdForArray(delays)
        var frames = [UIImage]()

        var frame: UIImage
        var frameCount: Int
        for i in 0..<count
        {
            frame = UIImage(cgImage: images[Int(i)])
            frameCount = Int(delays[Int(i)] / gcd)

            for _ in 0..<frameCount
            {
                frames.append(frame)
            }
        }

        let animation = UIImage.animatedImage(with: frames,
                            duration: Double(duration) / 1000.0)

        return animation
    }

    //----------------------------------------------------------
    class func delayForImageAtIndex(_ index: Int, source: CGImageSource!) -> Double
    {
        var delay = 0.1

        let cfProperties = CGImageSourceCopyPropertiesAtIndex(source, index, nil)
        let gifProperties: CFDictionary = unsafeBitCast(
            CFDictionaryGetValue(cfProperties,
                Unmanaged.passUnretained(kCGImagePropertyGIFDictionary).toOpaque()),
            to: CFDictionary.self)

        var delayObject: AnyObject = unsafeBitCast(
            CFDictionaryGetValue(gifProperties,
                Unmanaged.passUnretained(kCGImagePropertyGIFUnclampedDelayTime).toOpaque()),
            to: AnyObject.self)
        if delayObject.doubleValue == 0 {
            delayObject = unsafeBitCast(CFDictionaryGetValue(gifProperties,
                Unmanaged.passUnretained(kCGImagePropertyGIFDelayTime).toOpaque()), to: AnyObject.self)
        }

        delay = delayObject as! Double

        if delay < 0.1 {
            delay = 0.1
        }

        return delay
    }

    //----------------------------------------------------------
    class func gcdForArray(_ array: Array<Int>) -> Int
    {
        if array.isEmpty
        {
            return 1
        }

        var gcd = array[0]

        for val in array
        {
            gcd = UIImage.gcdForPair(val, gcd)
        }

        return gcd
    }

    //----------------------------------------------------------
    class func gcdForPair(_ aa: Int?, _ bb: Int?) -> Int
    {

        if bb == nil || aa == nil
        {
            if bb != nil
            {
                return bb!
            } else if aa != nil {
                return aa!
            } else {
                return 0
            }
        }

        guard var a = aa else {return 0}
        guard var b = bb else {return 0}

        if a < b
        {
            let c = a
            a = b
            b = c
        }

        var rest: Int
        while true
        {
            rest = a % b

            if rest == 0
            {
                return b
            } else {
                a = b
                b = rest
            }
        }
    }



    //----------------------------------------------------------
    func fixedOrientation() -> UIImage?
    {
//print("fixed Orientation.")
       guard imageOrientation != UIImage.Orientation.up else
        {
//print("fixed Orientation. was original")
            return self.copy() as? UIImage
        }
        
        guard let cgImage = self.cgImage else
        {
            return nil
        }

//print("fixed Orientation. size:",size)

        guard let colorSpace = cgImage.colorSpace, let ctx = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) else
        {
            return nil // Not able to create CGContext
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat.pi)
            break
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat.pi / 2.0)
            break
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat.pi / -2.0)
            break
        case .up, .upMirrored:
            break
        @unknown default:
            break
        }
        
        //Flip image one more time if needed to, this is to prevent flipped image
        switch imageOrientation
        {
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
            break
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: size.height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        case .up, .down, .left, .right:
            break
        @unknown default:
            break
        }
        
        ctx.concatenate(transform)
        
        switch imageOrientation
        {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        default:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }
        
        guard let newCGImage = ctx.makeImage() else { return nil }
        return UIImage.init(cgImage: newCGImage, scale: 1, orientation: .up)
    }
}

extension PHAsset
{
    var originalFilename: String?
    {
        var fname:String?
        
        let resources = PHAssetResource.assetResources(for: self)
        if let resource = resources.first
        {
            fname = resource.originalFilename
        }
        
        if fname == nil
        {
            fname = self.value(forKey: "filename") as? String
        }
        
        return fname
    }
}

extension UITableView
{
    
    //---------------------------------------------------------
    public func reloadData(_ completion: @escaping ()->())
    {
        UIView.animate(withDuration: 0, animations:
        {
            self.reloadData()
        }, completion:{ _ in
            completion()
        })
    }
    
    //---------------------------------------------------------
    func scroll(to: scrollsTo, animated: Bool)
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(10))
        {
            let numberOfSections = self.numberOfSections
            if numberOfSections > 0
            {
            let numberOfRows = self.numberOfRows(inSection: numberOfSections-1)

            switch to
            {
            case .top:
                if numberOfRows > 0
                {
                    let indexPath = IndexPath(row: 0, section: 0)
                    self.scrollToRow(at: indexPath, at: .top, animated: animated)
                }
                break
            case .bottom:
                if numberOfRows > 0
                {
                    let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                    self.scrollToRow(at: indexPath, at: .bottom, animated: animated)
                }
                break
            }
        }
        }
    }
    
    //---------------------------------------------------------
    enum scrollsTo
    {
        case top,bottom
    }
}

//---------------------------------------------------------
extension UIView
{
      //*************** change 0.0.32 add ***
    //-------------------------------------------------
    func asImage() -> UIImage
    {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image
        { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }

    // Export pdf from Save pdf in drectory and return pdf file path
    func exportAsPdfFromView() -> String
    {
        
        let pdfPageFrame = self.bounds
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageFrame, nil)
        UIGraphicsBeginPDFPageWithInfo(pdfPageFrame, nil)
        guard let pdfContext = UIGraphicsGetCurrentContext() else { return "" }
        self.layer.render(in: pdfContext)
        UIGraphicsEndPDFContext()
        return self.saveViewPdf(data: pdfData)
        
    }
    
    // Save pdf file in document directory
    func saveViewPdf(data: NSMutableData) -> String
    {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docDirectoryPath = paths[0]
        let pdfPath = docDirectoryPath.appendingPathComponent("ModifaiDetails.pdf")
        if data.write(to: pdfPath, atomically: true)
        {
            return pdfPath.path
        } else {
            return ""
        }
    }
 
    // 03/19 start
    @IBInspectable
    var cornerRadius1: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    // end
}
extension UIViewController
{
    func getImageOfScrollView(scrollView:UIScrollView) -> UIImage
    {
        var image = UIImage()

        UIGraphicsBeginImageContextWithOptions(scrollView.contentSize, false, UIScreen.main.scale)

        // save initial values
        let savedContentOffset = scrollView.contentOffset
        let savedFrame = scrollView.frame
        let savedBackgroundColor = scrollView.backgroundColor

        // reset offset to top left point
       // scrollView.contentOffset = CGPoint.zero
        // set frame to content size
//        scrollView.frame = CGRect(x:0, y:0, width:scrollView.contentSize.width, height:scrollView.contentSize.height)
        // remove background
        scrollView.backgroundColor = UIColor.clear

        // make temp view with scroll view content size
        // a workaround for issue when image on ipad was drawn incorrectly
       // let tempView = UIView(frame:scrollView.frame)

        // save superview
//        let tempSuperView = scrollView.superview
//        // remove scrollView from old superview
//        scrollView.removeFromSuperview()
//        // and add to tempView
//        tempView.addSubview(scrollView)

        // render view
        // drawViewHierarchyInRect not working correctly
        scrollView.layer.render(in: UIGraphicsGetCurrentContext()!)
        // and get image
        image = UIGraphicsGetImageFromCurrentImageContext()!

        // and return everything back
       // tempView.subviews[0].removeFromSuperview()
       // tempSuperView?.addSubview(scrollView)

        // restore saved settings
        scrollView.contentOffset = savedContentOffset
        scrollView.frame = savedFrame
        scrollView.backgroundColor = savedBackgroundColor

        UIGraphicsEndImageContext()

        return image
    }
}
extension UIImageView
{
    func load(url: URL)
    {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url)
            {
                if let image = UIImage(data: data)
                {
                    DispatchQueue.main.async
                    {
                        self?.image = image
                    }
                }
            }
        }
    }
}

//------------------------------------
extension UIViewController
{
    //-------------------------------------------------------------------
     func verifyUrl (urlString: String?) -> Bool {
         if let urlString = urlString {
             if let url = NSURL(string: urlString) {
                 return UIApplication.shared.canOpenURL(url as URL)
             }
         }
         return false
     }
    //------------------------------------
    func checkPaintImageExist(filename:String,changeImage:UIImageView) -> Bool
    {
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath = paths.first{
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(filename)
            let image    = UIImage(contentsOfFile: imageURL.path)
            if image != nil
            {
                changeImage.image = image
                return true
            }
        }
        return false
    }

    //------------------------------------
    func checkPaintMaskImageExist(filename:String) -> UIImage?
    {
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(filename)
            if let image = UIImage(contentsOfFile: imageURL.path)
            {
                return image
            }
        }
        return nil
    }

    //------------------------------------
    func checkUIColorExist(filename:String) -> UIColor?
    {
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
//print("checkUIColorExist paths:",paths)

        if let dirPath = paths.first
        {
            do
            {
                let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(filename)
//print("checkUIColorExist imageURL:",imageURL)
                let rawdata = try Data(contentsOf: imageURL)
                if let color = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? UIColor
                {
//print("checkUIColorExist color:",color)
                    return color
                }
            } catch {
//print("Couldn't read UIColor")
                return nil
            }
        }
        return nil
    }

    // ********** change 0.0.17 add ***
    //------------------------------------
    //Start cchanges******************20/02 changes
    func checkIfWellNumExists(filename:String) -> [String:Any]
    {
        // ****************MultiColorCHange

 

        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = dir.appendingPathComponent(filename)
            
            if FileManager.default.fileExists(atPath:fileURL.path) {
                
                //print("FILE AVAILABLE")
                
                do {
                    let contents = try String(contentsOf: fileURL, encoding: .utf8)
                    //print(contents)
                    let data :Data = contents.data(using: String.Encoding.utf8)! as Data
                    
                    if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String:Any]
                    {
                        //print(jsonArray) // use the json here
                        
                        return jsonArray
                    } else {
                        //print("bad json")
                        
                    }
                    
                } catch {
                    //print("Couldn't read file.")
                    return [:]
                }
                
            }else
            {
                let temp = filename.replacingOccurrences(of:".txt", with: "")
                if let filePath = Bundle.main.path(forResource:temp, ofType: "txt")
                {
                    if FileManager.default.fileExists(atPath:filePath) {
                        
                        //print("FILE AVAILABLE")
                        
                        do {
                            let contents = try String(contentsOf: filePath.url, encoding: .utf8)
                            //print(contents)
                            let data :Data = contents.data(using: String.Encoding.utf8)! as Data
                            
                            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String:Any]
                            {
                                //print(jsonArray) // use the json here
                                
                                return jsonArray
                            } else {
                                //print("bad json")
                                
                            }
                            
                        } catch {
                            //print("Couldn't read file.")
                            return [:]
                        }
                        
                    }
                }
            }
        
        }
        return [:]
    }
    //end cchanges******************20/02 changes
}

//--------------------------------------------------------
extension String
{
    //------------------------------------ //************* change add 2.0.6
    func randomString() -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var s = ""
        for _ in 0 ..< 10 {
            s.append(letters.randomElement()!)
        }
        return "IMG_\(s).jpg"
    }
    //--------------------------------------------------------
    func changeExtensions(name:String)->String
    {
        let tempArray = name.components(separatedBy:".")
        var tempfirstName = tempArray[0] as String
        tempfirstName = tempfirstName + ".jpg"
        return tempfirstName
    }

    //--------------------------------------------------------
    func convertToDictionary() -> [String: Any]?
    {
        if let data = self.data(using: .utf8)
        {
            do
            {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
//print("convert To Dictionary error.localizedDescription",error.localizedDescription)
            }
        }
        return nil
    }

    func dateFromString() -> Date?
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.date(from:self)
    }
    func dateFromWeekString() -> Date
       {
           let dateStr = self.components(separatedBy:" - ")
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.date(from:dateStr[0]) ?? Date()
       }
    
}

extension Date {
    func stringFromDate() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: self)
    }
    var startOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 0, to: sunday)
    }

    var endOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        
        return gregorian.date(byAdding: .day, value: 6, to: sunday)
    }
}
extension UIView {
    class func initFromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: self), owner: nil, options: nil)?[0] as! T
    }
}


extension UIWindow {
    static var key: UIWindow? {
        if #available(iOS 13, *) {
            return UIApplication.shared.windows.first { $0.isKeyWindow }
        } else {
            return UIApplication.shared.keyWindow
        }
    }
}


