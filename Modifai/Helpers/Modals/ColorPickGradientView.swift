import UIKit

//--------------------------------------------------------------------------------------
class ColorPickGradientView: UIView
{
	@IBOutlet weak var satSelectorView: UIImageView!
	@IBOutlet weak var lumSelectorView: UIImageView!

	var color1:UIColor = UIColor();
	var color2:UIColor = UIColor();

	var colorSpace:CGColorSpace!

    var gradient:CGGradient!
	var satValue:CGFloat = 0.0;
	var lumValue:CGFloat = 0.0;

	//--------------------------------------------------------------------------------------------------
	override func awakeFromNib()
	{
		super.awakeFromNib()
		let gestureRecognizer:UIPanGestureRecognizer = UIPanGestureRecognizer(target:self,  action: #selector(panOrTapValue(_:)));
		self.addGestureRecognizer(gestureRecognizer);

		let tapGestureRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target:self,  action: #selector(panOrTapValue(_:)));
		self.addGestureRecognizer(tapGestureRecognizer);

		self.colorSpace = CGColorSpaceCreateDeviceRGB()
	}

//	//----------------------------------------------------------------------------------------------------------
//	override func point(inside point:CGPoint, with event:UIEvent?) ->Bool
//	{
//		return false;
//	}

	//--------------------------------------------------------------------------------------------------
	func reloadGradient()
	{
		guard let c1 = self.color1.cgColor.components else {return;}
		guard let c2 = self.color2.cgColor.components else {return;}

		let comps:[CGFloat] =
		[
			c1[0], c1[1], c1[2], c1[3],
			c2[0], c2[1], c2[2], c2[3]
		]

		let componentCount:Int = sizeof(comps)/(sizeof(comps[0])*4);

		self.gradient = CGGradient(colorSpace: self.colorSpace, colorComponents: comps, locations: nil, count: componentCount)
	}

	//----------------------------------------------
	func sizeof <T> (_ : T.Type) -> Int
	{
		return (MemoryLayout<T>.stride)
	}

	//----------------------------------------------
	func sizeof <T> (_ : T) -> Int
	{
		return (MemoryLayout<T>.stride)
	}

	//----------------------------------------------
	func sizeof <T> (_ value : [T]) -> Int
	{
		return (MemoryLayout<T>.stride * value.count)
	}

//	//--------------------------------------------------------------------------------------------------
//	func sizeof<T:FixedWidthInteger>(_ int:T) -> Int
//	{
//		return int.bitWidth/UInt8.bitWidth
//	}
//
//	//--------------------------------------------------------------------------------------------------
//	func sizeof<T:FixedWidthInteger>(_ intType:T.Type) -> Int
//	{
//		return intType.bitWidth/UInt8.bitWidth
//	}

	//--------------------------------------------------------------------------------------------------
	override func draw(_ rect:CGRect)
	{
		if let context = UIGraphicsGetCurrentContext()
		{
			if let gradient = self.gradient
			{
				context.drawLinearGradient(gradient, start: CGPoint(x:0.0, y:0.0), end: CGPoint(x:rect.size.width, y:0.0), options: CGGradientDrawingOptions(rawValue: 0));
			}
		}
	}

	//--------------------------------------------------------------------------------------------------
	func satSetValue(_ value0:CGFloat)
	{
		self.satValue = value0;
//print("sat Set Value  self.satValue:",self.satValue)
		shouldSave = true;  //********** change 0.0.40 add ***

		var frame:CGRect = self.satSelectorView.frame;
		frame.origin.y = 0.0;
		frame.origin.x = self.satValue * (self.frame.size.width - 4) + self.frame.origin.x - 7 + 2;
		self.satSelectorView.frame = frame;
		currentPaintImageController.scrollView.touchPadView.sat = self.satValue;
		currentPaintImageController.scrollView.touchPadView.setDefaultWellColor()
	}

	//--------------------------------------------------------------------------------------------------
	func lumSetValue(_ value0:CGFloat)
	{
		self.lumValue = value0;
//print("lum Set Value  self.lumValue:",self.lumValue)
		shouldSave = true;  //********** change 0.0.40 add ***

		var frame:CGRect = self.lumSelectorView.frame;
		frame.origin.y = 0.0;
		frame.origin.x = self.lumValue * (self.frame.size.width - 4) + self.frame.origin.x - 7 + 2;
		self.lumSelectorView.frame = frame;
		currentPaintImageController.scrollView.touchPadView.lum = self.lumValue;
		currentPaintImageController.scrollView.touchPadView.setDefaultWellColor()
	}

	//--------------------------------------------------------------------------------------------------
	func satFrameOnly(_ value0:CGFloat)
	{
		self.satValue = value0;
//print("sat Frame Only  self.satValue:",self.satValue)

		var frame:CGRect = self.satSelectorView.frame;
		frame.origin.y = 0.0;
		frame.origin.x = self.satValue * (self.frame.size.width - 4) + self.frame.origin.x - 7 + 2;
		self.satSelectorView.frame = frame;
	}

	//--------------------------------------------------------------------------------------------------
	func lumFrameOnly(_ value0:CGFloat)
	{
		self.lumValue = value0;
//print("lum Frame Only  self.lumValue:",self.lumValue)

		var frame:CGRect = self.lumSelectorView.frame;
		frame.origin.y = 0.0;
		frame.origin.x = self.lumValue * (self.frame.size.width - 4) + self.frame.origin.x - 7 + 2;
		self.lumSelectorView.frame = frame;
	}

	//--------------------------------------------------------------------------------------------------
	@objc func panOrTapValue(_ recognizer:UIGestureRecognizer)
	{
		switch (recognizer.state)
		{
			case UIGestureRecognizer.State.possible:
				let point:CGPoint = recognizer.location(in:self);
				if (self.bounds.contains(point))
				{
					let val:CGFloat = (point.x / self.frame.size.width);

					if (self == currentPaintImageController.gradientViewSaturation)
					{
						self.satSetValue(val);
					} else {
						self.lumSetValue(val);
					}
				}
			break;
			case UIGestureRecognizer.State.began:
				let point:CGPoint = recognizer.location(in:self);
				if (self.bounds.contains(point))
				{
					let val:CGFloat = (point.x / self.frame.size.width);

					if (self == currentPaintImageController.gradientViewSaturation)
					{
						self.satSetValue(val);
					} else {
						self.lumSetValue(val);
					}
				}
			break;
			case UIGestureRecognizer.State.changed:
				let point:CGPoint = recognizer.location(in:self);
				if (self.bounds.contains(point))
				{
					let val:CGFloat = (point.x / self.frame.size.width);

					if (self == currentPaintImageController.gradientViewSaturation)
					{
						self.satSetValue(val);
					} else {
						self.lumSetValue(val);
					}
				}
			break;
			case UIGestureRecognizer.State.ended:
				isBump = true;
				let point:CGPoint = recognizer.location(in:self);
				if (self.bounds.contains(point))
				{
					let val:CGFloat = (point.x / self.frame.size.width);
					if (self == currentPaintImageController.gradientViewSaturation)
					{
						self.satSetValue(val);
					} else {
						self.lumSetValue(val);
					}
				} else {  //********** change 0.0.40 add ***
					if (self == currentPaintImageController.gradientViewSaturation)
					{
						self.satSetValue(self.satValue);
					} else {
						self.lumSetValue(self.lumValue);
					}
				}
			break;
			case UIGestureRecognizer.State.failed:
			break;
			case UIGestureRecognizer.State.cancelled:
			break;
			default:
			break;
		}
	}
}
