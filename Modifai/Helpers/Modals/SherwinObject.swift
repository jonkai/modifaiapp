import UIKit      //************** change 3.0.0 add whole file ***

//---------------------------------------------------------------------
class SherwinObject:NSObject, Codable, NSCoding, NSCopying
{
//	var iD:String? = nil;

    public var code:Int = 0
    public var name:String = "0"
    public var red:Int = 0
    public var grn:Int = 0
    public var blu:Int = 0

    public var redf0:CGFloat = 0.0
    public var grnf0:CGFloat = 0.0
    public var bluf0:CGFloat = 0.0

    public var hue:CGFloat = 0.0
    public var sat:CGFloat = 0.0
    public var lum:CGFloat = 0.0

    public var hueSatLumKey:Int = 0
    public var hueSatLumKeyE:Int = 0

//    public var redGrnKey:Int = 0
//    public var redBluKey:Int = 0
//
//    public var grnBluKey:Int = 0
//	public var grnRedKey:Int = 0
//
//    public var bluRedKey:Int = 0
//    public var bluGrnKey:Int = 0

	//-----------------------------------
	override var description: String
    {
		return """
			 ------ key:\(hueSatLumKey)		code:\(code) name:\(name)		hue:\(hue) sat:\(sat) lum:\(lum)	red:\(red) grn:\(grn) blu:\(blu)
			"""
    }
//   redGrnKey:\(redGrnKey) grnBluKey:\(grnBluKey) bluRedKey:\(bluRedKey)
	//---------------------------------------------------------------------
    public enum CodingKeys: String, CodingKey
    {
//		case iD

        case code
        case name
        case red
        case grn
        case blu

        case hue
        case sat
        case lum

        case hueSatLumKey
        case hueSatLumKeyE

//        case redGrnKey
//        case redBluKey
//
//        case grnBluKey
//        case grnRedKey
//
//        case bluRedKey
//        case bluGrnKey
    }

	//---------------------------------------------------------------------
	public init(other:SherwinObject)
	{
        super.init()

 		self.code = other.code;
		self.name = other.name;
		self.red = other.red;
		self.grn = other.grn;
		self.blu = other.blu;

        self.redf0 = other.redf0;
        self.grnf0 = other.grnf0;
        self.bluf0 = other.bluf0;

        self.hue = other.hue;
        self.sat = other.sat;
        self.lum = other.lum;

        self.hueSatLumKey = other.hueSatLumKey;
        self.hueSatLumKeyE = other.hueSatLumKeyE;

//        self.grnRedKey = other.grnRedKey;
   }

	//---------------------------------------------------------------------
	public init(code:Int,name:String,red:Int,grn:Int,blu:Int)
	{
        super.init()

//		self.iD = UUID().uuidString

		self.code = code;
		self.name = name;
		self.red = red;
		self.grn = grn;
		self.blu = blu;


        self.redf0 = CGFloat(self.red)/255.0;
        self.grnf0 = CGFloat(self.grn)/255.0;
        self.bluf0 = CGFloat(self.blu)/255.0;

//print("  ");
//print("redf0, greenf0, bluef0:",redf0, greenf0, bluef0);

        let tmax:CGFloat = max(max(self.redf0, self.grnf0), self.bluf0);
        let tmin:CGFloat = min(min(self.redf0, self.grnf0), self.bluf0);

        let delta:CGFloat = tmax - tmin;

        var sat:CGFloat = 0.0;
        var hue:CGFloat = 0.0;

        if (tmax > 0.0)
        {
            sat = delta / tmax;
        }

        if (delta > 0.0)
        {
            var divisor = 6.0

            if ((self.redf0 > self.grnf0) && (self.redf0 > self.bluf0))
            {
                hue = CGFloat(modf(Double(self.grnf0 - self.bluf0) / Double(delta), &divisor)) * 0.6;
            } else if ((self.grnf0 >= self.redf0) && (self.grnf0 > self.bluf0)) {
                hue = (((self.bluf0 - self.redf0) / delta) + 2.0) * 0.6;
            } else {
                hue = (((self.redf0 - self.grnf0) / delta) + 4.0) * 0.6;
            }

            if (hue < 0.0)
            {
                hue = 3.6 + hue;
            }
        }

        let hue10 = hue / 3.6;

		self.hue = hue10
		self.sat = sat
		self.lum = tmax


//d = sqrt(((r2-r1)*0.3)^2 + ((g2-g1)*0.59)^2 + ((b2-b1)*0.11)^2)
//constant float3 luminanceWeight = float3(0.1125, 0.3154, 0.1121);
//constant float3 luminanceWeight = float3(0.2125, 0.7154, 0.0721);  //0.1121


        let hue11 = Int(round(hue10*1000.0));
        var sat11 = Int(round(sat*1000.0));
        var lum11 = Int(round(tmax*1000.0));

        if (sat11 == 1000) {sat11 = 999}
        if (lum11 == 1000) {lum11 = 999}

		self.hueSatLumKey = (hue11*1000000)+(sat11*1000)+(lum11);

		if (hue10 < 0.02)
		{
        	let hue12 = Int((1.0+hue10)*1000.0);
			self.hueSatLumKeyE = (hue12*1000000)+(sat11*1000)+(lum11)
		} else if (hue10 > 0.98) {
        	let hue12 = Int((hue10-1.0)*1000.0);
			self.hueSatLumKeyE = (hue12*1000000)-(sat11*1000)-(lum11)
		}



//		self.redGrnKey = (red*1000000)+(grn*1000)+(blu);
//		self.redBluKey = (red*1000000)+(blu*1000)+(grn);
//
//		self.grnBluKey = (grn*1000000)+(blu*1000)+(red);

//        let grn11 = Int(round((self.grnf0*0.59)*1000.0));
//        let red11 = Int(round((self.redf0*0.3)*1000.0));
//        let blu11 = Int(round((self.bluf0*0.1121)*1000.0));
//
//		self.grnRedKey = (grn11*1000000)+(red11*1000)+blu11;
//
//		self.bluRedKey = (blu*1000000)+(red*1000)+(grn);
//		self.bluGrnKey = (blu*1000000)+(grn*1000)+(red);

	}

	//---------------------------------------------------------------------
    func copy(with zone: NSZone? = nil) -> Any
    {
        let copy = SherwinObject.init(other:self)
		return copy
    }

	//---------------------------------------------------------------------
	required init?(coder decoder: NSCoder)
	{
//		self.iD = decoder.decodeObject(forKey: "iD") as? String
		self.code = decoder.decodeInteger(forKey: "code")
		self.name = decoder.decodeObject(forKey: "name") as! String
		self.red = decoder.decodeInteger(forKey: "red")
		self.grn = decoder.decodeInteger(forKey: "grn")
		self.blu = decoder.decodeInteger(forKey: "blu")

		self.hue = CGFloat(decoder.decodeFloat(forKey: "hue"))
		self.sat = CGFloat(decoder.decodeFloat(forKey: "sat"))
		self.lum = CGFloat(decoder.decodeFloat(forKey: "lum"))

		self.hueSatLumKey  = decoder.decodeInteger(forKey: "hueSatLumKey")
		self.hueSatLumKeyE = decoder.decodeInteger(forKey: "hueSatLumKeyE")

//		self.redGrnKey = decoder.decodeInteger(forKey: "redGrnKey")
//		self.redBluKey = decoder.decodeInteger(forKey: "redBluKey")
//
//		self.grnBluKey = decoder.decodeInteger(forKey: "grnBluKey")
//		self.grnRedKey = decoder.decodeInteger(forKey: "grnRedKey")
//
//		self.bluRedKey = decoder.decodeInteger(forKey: "bluRedKey")
//		self.bluGrnKey = decoder.decodeInteger(forKey: "bluGrnKey")

        super.init()
	}

	//---------------------------------------------------------------------
    func encode(with aCoder: NSCoder)
	{
//		aCoder.encode(self.iD, forKey: "iD")
		aCoder.encode(self.code, forKey: "code")
		aCoder.encode(self.name, forKey: "name")

		aCoder.encode(self.red, forKey: "red")
		aCoder.encode(self.grn, forKey: "grn")
		aCoder.encode(self.blu, forKey: "blu")

		aCoder.encode(self.hue, forKey: "hue")
		aCoder.encode(self.sat, forKey: "sat")
		aCoder.encode(self.lum, forKey: "lum")

		aCoder.encode(self.hueSatLumKey,  forKey: "hueSatLumKey")
		aCoder.encode(self.hueSatLumKeyE, forKey: "hueSatLumKeyE")

//		aCoder.encode(self.redGrnKey, forKey: "redGrnKey")
//		aCoder.encode(self.redBluKey, forKey: "redBluKey")
//
//		aCoder.encode(self.grnBluKey, forKey: "grnBluKey")
//		aCoder.encode(self.grnRedKey, forKey: "grnRedKey")
//
//		aCoder.encode(self.bluRedKey, forKey: "bluRedKey")
//		aCoder.encode(self.bluGrnKey, forKey: "bluGrnKey")
	}
}


//---------------------------------------------------------------------
extension Array where Element: NSCopying
{
	func copy() -> [Element]
	{
		return self.map { $0.copy() as! Element }
	}
}
