import Foundation
import UIKit

//*************** change 1.1.0 add new value 'gallIndex' through out ***
//---------------------------------------------------------------------
class Project:NSObject, Codable, NSCoding
{
    var projectID:String? = nil;
    var thumbnailData:Data? = nil;
    var imageData:Data? = nil;
	var imageName:String? = nil;
	var wasProcessed:Bool = false;

	var defaultLineColor0d:Data? = nil;
	var origStartColor0d:Data? = nil;

	var defaultLineColor1d:Data? = nil;
	var origStartColor1d:Data? = nil;

	var defaultLineColor2d:Data? = nil;
	var origStartColor2d:Data? = nil;

	var defaultLineColor3d:Data? = nil;
	var origStartColor3d:Data? = nil;

	var defaultLineColor4d:Data? = nil;
	var origStartColor4d:Data? = nil;


	var defaultLineGaraged:Data? = nil;
	var origStartGaraged:Data? = nil;

	var defaultLineFrontDrd:Data? = nil;
	var origStartFrontDrd:Data? = nil;

    var house0:Data? = nil;

    var mask0:Data? = nil;
    var mask1:Data? = nil;
    var mask2:Data? = nil;
    var mask3:Data? = nil;
    var mask4:Data? = nil;

    var garageMask:Data? = nil;
    var garageSMask:Data? = nil;
    var frontDrMask:Data? = nil;

    var houseMask0:Data? = nil;
    var siding0:Data? = nil;
    var roof0:Data? = nil;

    var currentWellNum:Int = 0;
    var currentButtonToDisplay:Int = 0;
    var gallIndex:Int = 0;

    var well0:Bool = false;
    var well1:Bool = false;
    var well2:Bool = false;
    var well3:Bool = false;
    var well4:Bool = false;

	//---------------------------------------------------------------------
    private enum CodingKeys: String, CodingKey
    {
		case projectID
        case thumbnailData
        case imageData
        case imageName
        case wasProcessed

		case defaultLineColor0d
		case origStartColor0d

		case defaultLineColor1d
		case origStartColor1d

		case defaultLineColor2d
		case origStartColor2d

		case defaultLineColor3d
		case origStartColor3d

		case defaultLineColor4d
		case origStartColor4d


		case defaultLineGaraged
		case origStartGaraged

		case defaultLineFrontDrd
		case origStartFrontDrd

		case house0

		case mask0
		case mask1
		case mask2
		case mask3
		case mask4

		case garageMask
		case garageSMask
		case frontDrMask

		case houseMask0
		case siding0
		case roof0

		case currentWellNum
		case currentButtonToDisplay
		case gallIndex

		case well0
		case well1
		case well2
		case well3
		case well4
    }

	//---------------------------------------------------------------------
	override init()
	{
        super.init()
	}

	//---------------------------------------------------------------------
	required init?(coder decoder: NSCoder)
	{
		self.projectID = decoder.decodeObject(forKey: "projectID") as? String
		self.thumbnailData = decoder.decodeObject(forKey: "thumbnailData") as? Data
		self.imageData = decoder.decodeObject(forKey: "imageData") as? Data
		self.imageName = decoder.decodeObject(forKey: "imageName") as? String
		self.wasProcessed = decoder.decodeBool(forKey: "wasProcessed")

		self.defaultLineColor0d = decoder.decodeObject(forKey: "defaultLineColor0d") as? Data
		self.origStartColor0d = decoder.decodeObject(forKey: "origStartColor0d") as? Data

		self.defaultLineColor1d = decoder.decodeObject(forKey: "defaultLineColor1d") as? Data
		self.origStartColor1d = decoder.decodeObject(forKey: "origStartColor1d") as? Data

		self.defaultLineColor2d = decoder.decodeObject(forKey: "defaultLineColor2d") as? Data
		self.origStartColor2d = decoder.decodeObject(forKey: "origStartColor2d") as? Data

		self.defaultLineColor3d = decoder.decodeObject(forKey: "defaultLineColor3d") as? Data
		self.origStartColor3d = decoder.decodeObject(forKey: "origStartColor3d") as? Data

		self.defaultLineColor4d = decoder.decodeObject(forKey: "defaultLineColor4d") as? Data
		self.origStartColor4d = decoder.decodeObject(forKey: "origStartColor4d") as? Data


		self.defaultLineGaraged = decoder.decodeObject(forKey: "defaultLineGaraged") as? Data
		self.origStartGaraged = decoder.decodeObject(forKey: "origStartGaraged") as? Data

		self.defaultLineFrontDrd = decoder.decodeObject(forKey: "defaultLineFrontDrd") as? Data
		self.origStartFrontDrd = decoder.decodeObject(forKey: "origStartFrontDrd") as? Data

		self.house0 = decoder.decodeObject(forKey: "house0") as? Data

		self.mask0 = decoder.decodeObject(forKey: "mask0") as? Data
		self.mask1 = decoder.decodeObject(forKey: "mask1") as? Data
		self.mask2 = decoder.decodeObject(forKey: "mask2") as? Data
		self.mask3 = decoder.decodeObject(forKey: "mask3") as? Data
		self.mask4 = decoder.decodeObject(forKey: "mask4") as? Data

		self.garageMask = decoder.decodeObject(forKey: "garageMask") as? Data
		self.garageSMask = decoder.decodeObject(forKey: "garageSMask") as? Data
		self.frontDrMask = decoder.decodeObject(forKey: "frontDrMask") as? Data

		self.houseMask0 = decoder.decodeObject(forKey: "houseMask0") as? Data
		self.siding0 = decoder.decodeObject(forKey: "siding0") as? Data
		self.roof0 = decoder.decodeObject(forKey: "roof0") as? Data

		self.currentWellNum = decoder.decodeInteger(forKey: "currentWellNum")
		self.currentButtonToDisplay = decoder.decodeInteger(forKey: "currentButtonToDisplay")
		self.gallIndex = decoder.decodeInteger(forKey: "gallIndex")

		self.well0 = decoder.decodeBool(forKey: "well0")
		self.well1 = decoder.decodeBool(forKey: "well1")
		self.well2 = decoder.decodeBool(forKey: "well2")
		self.well3 = decoder.decodeBool(forKey: "well3")
		self.well4 = decoder.decodeBool(forKey: "well4")

        super.init()
	}

	//---------------------------------------------------------------------
    func encode(with aCoder: NSCoder)
	{
		aCoder.encode(self.projectID, forKey: "projectID")
		aCoder.encode(self.thumbnailData, forKey: "thumbnailData")
		aCoder.encode(self.imageData, forKey: "imageData")
		aCoder.encode(self.imageName, forKey: "imageName")
		aCoder.encode(self.wasProcessed, forKey: "wasProcessed")

		aCoder.encode(self.defaultLineColor0d, forKey: "defaultLineColor0d")
		aCoder.encode(self.origStartColor0d, forKey: "origStartColor0d")

		aCoder.encode(self.defaultLineColor1d, forKey: "defaultLineColor1d")
		aCoder.encode(self.origStartColor1d, forKey: "origStartColor1d")

		aCoder.encode(self.defaultLineColor2d, forKey: "defaultLineColor2d")
		aCoder.encode(self.origStartColor2d, forKey: "origStartColor2d")

		aCoder.encode(self.defaultLineColor3d, forKey: "defaultLineColor3d")
		aCoder.encode(self.origStartColor3d, forKey: "origStartColor3d")

		aCoder.encode(self.defaultLineColor4d, forKey: "defaultLineColor4d")
		aCoder.encode(self.origStartColor4d, forKey: "origStartColor4d")


		aCoder.encode(self.defaultLineGaraged, forKey: "defaultLineGaraged")
		aCoder.encode(self.origStartGaraged, forKey: "origStartGaraged")

		aCoder.encode(self.defaultLineFrontDrd, forKey: "defaultLineFrontDrd")
		aCoder.encode(self.origStartFrontDrd, forKey: "origStartFrontDrd")

		aCoder.encode(self.house0, forKey: "house0")

		aCoder.encode(self.mask0, forKey: "mask0")
		aCoder.encode(self.mask1, forKey: "mask1")
		aCoder.encode(self.mask2, forKey: "mask2")
		aCoder.encode(self.mask3, forKey: "mask3")
		aCoder.encode(self.mask4, forKey: "mask4")

		aCoder.encode(self.garageMask, forKey: "garageMask")
		aCoder.encode(self.garageSMask, forKey: "garageSMask")
		aCoder.encode(self.frontDrMask, forKey: "frontDrMask")

		aCoder.encode(self.houseMask0, forKey: "houseMask0")
		aCoder.encode(self.siding0, forKey: "siding0")
		aCoder.encode(self.roof0, forKey: "roof0")

		aCoder.encode(self.currentWellNum, forKey: "currentWellNum")
		aCoder.encode(self.currentButtonToDisplay, forKey: "currentButtonToDisplay")
		aCoder.encode(self.gallIndex, forKey: "gallIndex")

		aCoder.encode(self.well0, forKey: "well0")
		aCoder.encode(self.well1, forKey: "well1")
		aCoder.encode(self.well2, forKey: "well2")
		aCoder.encode(self.well3, forKey: "well3")
		aCoder.encode(self.well4, forKey: "well4")
	}
}

//-------------------------------------------
public extension UIColor
{
	//-------------------------------------------
	class func color(data:Data?) -> UIColor!
	{
		guard let data0 = data else {return UIColor.clear}

		do {
			return try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data0) as? UIColor
		} catch {
			return UIColor.clear
		}
	}

	//-------------------------------------------
	func encode() -> Data?
	{
        NSKeyedArchiver.setClassName("Project", for: Project.self)
		return try? NSKeyedArchiver.archivedData(withRootObject: self, requiringSecureCoding: false)
	}
}
