import UIKit

//----------------------------------------------------------------------------------------
extension UIBezierPath
{
	//----------------------------------------------------------------------------------------
	class func interpolateCGPointsWithCatmullRom(_ pointsInArray0: [CGPoint]?, closed: Bool, alpha0: CGFloat) -> UIBezierPath?
	{
		guard let pointsInArray = pointsInArray0 else {return nil}

		if pointsInArray.count < 4
		{
			return nil
		}

		var alpha: CGFloat = alpha0;

		let endIndex = closed ? pointsInArray.count : pointsInArray.count - 2

		if alpha < 0.0
		{
			alpha = 0.0
		}

		if alpha > 1.0
		{
			alpha = 1.0
		}

		let path: UIBezierPath! = UIBezierPath.init()
		path.lineJoinStyle = CGLineJoin.round
		path.lineCapStyle = CGLineCap.round

		let startIndex = closed ? 0 : 1

		for ii in startIndex..<endIndex
		{
//			var p0: CGPoint = CGPoint()
//			var p1: CGPoint = CGPoint()
//			var p2: CGPoint = CGPoint()
//			var p3: CGPoint = CGPoint()

			let nextii = (ii + 1) % pointsInArray.count
			let nextnextii = (nextii + 1) % pointsInArray.count
			let previi = ii - 1 < 0 ? pointsInArray.count - 1 : ii - 1

			let p1: CGPoint = pointsInArray[ii]
			let p0: CGPoint = pointsInArray[previi]
			let p2: CGPoint = pointsInArray[nextii]
			let p3: CGPoint = pointsInArray[nextnextii]

//			p0 = pecP0.convertToCG()
//			p1 = pecP1.convertToCG()
//			p2 = pecP2.convertToCG()
//			p3 = pecP3.convertToCG()

			let d1 = ccpLength(ccpSub(p1, p0))
			let d2 = ccpLength(ccpSub(p2, p1))
			let d3 = ccpLength(ccpSub(p3, p2))

			var b1: CGPoint = CGPoint()
			var b2: CGPoint = CGPoint()

			if abs(d1) < CGFloat.ulpOfOne
			{
				b1 = p1
			} else {
				b1 = ccpMult(p2, pow(d1, 2.0 * alpha))
				b1 = ccpSub(b1, ccpMult(p0, pow(d2, 2.0 * alpha)))
				b1 = ccpAdd(b1, ccpMult(p1, 2.0 * pow(d1, 2.0 * alpha) + 3.0 * pow(d1, alpha) * pow(d2, alpha) + pow(d2, 2.0 * alpha)))
				b1 = ccpMult(b1, 1.0 / (3.0 * pow(d1, alpha) * (pow(d1, alpha) + pow(d2, alpha))))
			}

			if abs(d3) < CGFloat.ulpOfOne
			{
				b2 = p2
			} else {
				b2 = ccpMult(p1, pow(d3, 2 * alpha))
				b2 = ccpSub(b2, ccpMult(p3, pow(d2, 2 * alpha)))
				b2 = ccpAdd(b2, ccpMult(p2, 2 * pow(d3, 2 * alpha) + 3 * pow(d3, alpha) * pow(d2, alpha) + pow(d2, 2 * alpha)))
				b2 = ccpMult(b2, 1.0 / (3 * pow(d3, alpha) * (pow(d3, alpha) + pow(d2, alpha))))
			}

			if ii == startIndex
			{
				path.move(to: p1)
			}

			path.addCurve(to: p2, controlPoint1: b1, controlPoint2: b2)
		}

		if closed
		{
			path.close()
		}

		return path
	}

	//----------------------------------------------------------------------------------------
	class func interpolateCGPointsWithHermite(_ pointsInArray0: [CGPoint]?, closed: Bool) -> UIBezierPath?
	{
		guard let pointsInArray = pointsInArray0 else {return nil}

		if pointsInArray.count == 0
		{
			return nil
		}

		if pointsInArray.count <= 3
		{
			let tempPath: UIBezierPath! = UIBezierPath.init()
			let pv0:CGPoint = pointsInArray.first ?? CGPoint()
			tempPath.move(to: pv0)

			var firstin = true

			for pv in pointsInArray
			{
				if firstin
				{
					firstin = false
					continue
				}

				tempPath.addLine(to: pv)
			}

			return tempPath
		}

		let nCurves = closed ? pointsInArray.count : pointsInArray.count - 1
		let path: UIBezierPath! = UIBezierPath.init()
		path.lineJoinStyle = CGLineJoin.round
		path.lineCapStyle = CGLineCap.round

		for ii in 0..<nCurves
		{
			var curPt = pointsInArray[ii]

			if ii == 0
			{
				path.move(to: curPt)
			}

			var nextii = (ii + 1) % pointsInArray.count
			var previi = ii - 1 < 0 ? pointsInArray.count - 1 : ii - 1
			var prevPt = pointsInArray[previi]
			var nextPt = pointsInArray[nextii]
			let endPt = CGPoint(x: nextPt.x, y: nextPt.y)
			var mx: CGFloat = 0.0
			var my: CGFloat = 0.0

			if closed || ii > 0
			{
				mx = (nextPt.x - curPt.x) * 0.5 + (curPt.x - prevPt.x) * 0.5
				my = (nextPt.y - curPt.y) * 0.5 + (curPt.y - prevPt.y) * 0.5
			} else {
				mx = (nextPt.x - curPt.x) * 0.5
				my = (nextPt.y - curPt.y) * 0.5
			}

			let ctrlPt1: CGPoint = CGPoint(x:curPt.x + mx / 3.0, y:curPt.y + my / 3.0)

			curPt = pointsInArray[nextii]
			nextii = (nextii + 1) % pointsInArray.count
			previi = ii
			prevPt = pointsInArray[previi]
			nextPt = pointsInArray[nextii]

			if closed || ii < nCurves - 1
			{
				mx = (nextPt.x - curPt.x) * 0.5 + (curPt.x - prevPt.x) * 0.5
				my = (nextPt.y - curPt.y) * 0.5 + (curPt.y - prevPt.y) * 0.5
			} else {
				mx = (curPt.x - prevPt.x) * 0.5
				my = (curPt.y - prevPt.y) * 0.5
			}

			let ctrlPt2: CGPoint = CGPoint(x:curPt.x - mx / 3.0, y:curPt.y - my / 3.0)
			path.addCurve(to: endPt, controlPoint1: ctrlPt1, controlPoint2: ctrlPt2)
		}

		if closed
		{
			path.close()
		}

		return path
	}
}
