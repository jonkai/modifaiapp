import UIKit
import PECimage

//--------------------------------------------------------------------------------------
class IndicatorImageView: UIImageView
{
	@IBOutlet weak var indicator2SpinnerView:UIView!
    var touchOffset:CGPoint = CGPoint.init();
    var shouldMove:Bool = false;
    var adjustedAngle00:CGFloat = 0.0;
    var sat:CGFloat = 0.998;
    var lum:CGFloat = 0.998;
    var origSat:CGFloat = 0.998;
    var origLum:CGFloat = 0.998;
	var beginDragTimeStamp:TimeInterval = TimeInterval();
	var origCenter:CGPoint = CGPoint.zero;

	// MARK: - touches actions
	//----------------------------------------------------------------------------------------------------------
	override func point(inside point:CGPoint, with event:UIEvent?) -> Bool
	{
		var shouldTouchStayOnThisLevel:Bool = false;
		let currentStart = CGPoint(x:point.x - 21.0, y:-(point.y - 21.0));
//print("point,currentStart:",point,currentStart)

		let lengthToCenter =  sqrt((currentStart.x * currentStart.x) + (currentStart.y * currentStart.y));
		if (lengthToCenter <= 18.0) {shouldTouchStayOnThisLevel = true;}

//print("IndicatorView lengthToCenter, shouldTouchStayOnThisLevel: ",lengthToCenter,shouldTouchStayOnThisLevel);

		return shouldTouchStayOnThisLevel;
	}

	//----------------------------------------------------------------------------------------------------------
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
	{
//print("  ")
//print("  ")
//print("IndicatorImageView touchesBegan")
//print("IndicatorImageView touchesBegan  event?.allTouches",event?.allTouches as Any)
        var point:CGPoint = CGPoint.zero;
		self.shouldMove = false;
	//	othe rHalf = false;
		self.touchOffset = CGPoint.zero

		if let event0 = event
		{
			beginDragTimeStamp = event0.timestamp;

			point = event0.allTouches?.first?.location(in:self.indicator2SpinnerView) ?? CGPoint.zero;
			self.origCenter = point;

			self.touchOffset = CGPoint(x:point.x - self.center.x, y:point.y - self.center.y);
//print("IndicatorImageView touchesBegan  self.touchOffset",self.touchOffset)
		}

		currentPaintImageController.scrollView.touchPadView.touchesBegan(touches, with:event);
		currentPaintImageController.scrollView.touchPadView.touchesMoved(touches, with:event);
		currentPaintImageController.scrollView.touchPadView.touchesEnded(touches, with:event);
	}

	//----------------------------------------------------------------------------------------------------------
	override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
	{
//print("IndicatorImageView touchesMoved")
//        super.touchesMoved(touches, with:event);

		let point = event?.allTouches?.first?.location(in:currentPaintImageController.scrollView.touchPadView) ?? CGPoint.zero;
		let indicatorPt = CGPoint(x:point.x - self.touchOffset.x, y:point.y - self.touchOffset.y);
	//	let indicatorPt = point;
//print("IndicatorImageView touchesMoved  indicatorPt",indicatorPt)

		let currentSpot = CGPoint(x:indicatorPt.x - 340.0, y:-(indicatorPt.y - 340.0));
		self.shouldMove = false;
//		var otherHalf:Bool = false;
//print("point,currentSpot:",point,currentSpot)

		let dist =  sqrt((currentSpot.x * currentSpot.x) + (currentSpot.y * currentSpot.y));
		if (dist > 86.0) && (dist <= touchPadLimit)
		{
			self.center = indicatorPt;
			self.shouldMove = true;

			let spotAngle:CGFloat = self.indicatorRotationAngleOfPoint(currentSpot);
			var spotAngle0:CGFloat = spotAngle;

			if (spotAngle < -mpi2)
			{
				spotAngle0 = -CGFloat.pi - (-mpi3 - spotAngle)
			}

//			let adjustedAngle0:CGFloat = currentProtr actorAngle + spotAngle - angleO fStart;
//			adjustedAngle00 = adjustedAngle0 - (spotAngle0 + pip30);

	//		let adjustedAngle0:CGFloat = currentProtra ctorAngle + spotAngle - angle OfStart;
			adjustedAngle00 = currentPaintImageController.scrollView.touchPadView.currentProtractorAngle - (spotAngle0 + pip30);

//print("IndicatorImageView currentProtractorAngle,spotAngle:",currentPaintImageController.scrollView.touchPadView.currentProtractorAngle,spotAngle)
//print("IndicatorImageView indicatorPt,currentSpot,spotAngle,spotAngle0,adjustedAngle00:",indicatorPt,currentSpot,spotAngle,spotAngle0,adjustedAngle00)

//print("  ")
//print("IndicatorImageView touchPadView.indica tor2SpinnerView.layer.transform:",currentPaintImageController.scrollView.touchPadView.indica tor2SpinnerView.layer.transform)


//			let dividerAngle:Float = Float(adjustedAngle00-mpiDiv24Half) / Float(mpiDiv24);
//			let dividerAngle0:Float = roundf(dividerAngle);

//			var whichHalfSpokes = false;
//
//			if (dividerAngle > dividerAngle0)
//			{
//				whichHalfSpokes = true;
//			}

			let distSpotf:Float =  (Float(dist) - 67.1) / 33.79278;
			let distSpot:Float =  Float(Int(distSpotf));

//			var whichHalfConcentric = false;
//
////print("dist,distSpot,distSpotf:",dist,distSpot,distSpotf)
//
//			if ((distSpotf - Float(distSpot)) < 0.62)
//			{
//				whichHalfConcentric = true;
//			}

//			if (whichHalfSpokes && whichHalfConcentric)
//			{
//				otherHalf = true;
//			} else if (whichHalfSpokes || whichHalfConcentric) {
//
//				var spokesAmount:Float = 0.0;
//				var concenAmount:Float = 0.0;
//
//				//------------- find distances to half way points and compare ------------
//				if (whichHalfSpokes)
//				{
//					spokesAmount = (dividerAngle0+0.5) - dividerAngle;
//					concenAmount = distSpotf - (distSpot+0.62);
////print("whichHalfSpokes spokesAmount, concenAmount:",spokesAmount, concenAmount)
//
//					if (spokesAmount > concenAmount)
//					{
//						otherHalf = true;
//					}
//				} else {
//					spokesAmount = dividerAngle - (dividerAngle0-0.5);
//					concenAmount = (distSpot+0.62) - distSpotf;
////print("whichHalfConcentric spokesAmount, concenAmount:",spokesAmount, concenAmount)
//
//					if (concenAmount > spokesAmount)
//					{
//						otherHalf = true;
//					}
//				}
//			}

			if let pad = currentPaintImageController.scrollView.touchPadView
			{
				sat = pad.topSat;
				lum = pad.topLum;

				if (distSpot == 0)
				{
					sat = pad.topSat + (pad.satStep * pad.satBigStep);
					lum = pad.topLum - (pad.lumStep * 3.0);

					origSat = maxSat + 0.40;
					origLum = maxLum - 0.30;
				} else if (distSpot == 1) {
					sat = pad.topSat + (pad.satStep * pad.satBigStep);
					lum = pad.topLum - (pad.lumStep * 3.0);

					origSat = maxSat + 0.40;
					origLum = maxLum - 0.30;
				} else if (distSpot == 2) {
					sat = pad.topSat - pad.satStep;
					lum = pad.topLum - (pad.lumStep * 4.0);

					origSat = maxSat - 0.20;
					origLum = maxLum - 0.40;
				} else if (distSpot == 3) {
					sat = pad.topSat - pad.satStep;
					lum = pad.topLum - (pad.lumStep * 3.0);

					origSat = maxSat - 0.20;
					origLum = maxLum - 0.30;
				} else if (distSpot == 4) {
					sat = pad.topSat - pad.satStep;
					lum = pad.topLum - (pad.lumStep * 2.0);

					origSat = maxSat - 0.20;
					origLum = maxLum - 0.20;
				} else if (distSpot == 5) {
					sat = pad.topSat;
					lum = pad.topLum - (pad.lumStep * 2.0);

					origSat = maxSat;
					origLum = maxLum - 0.20;
				} else if (distSpot == 6) {
					sat = pad.topSat;
					lum = pad.topLum - pad.lumStep;

					origSat = maxSat;
					origLum = maxLum - 0.10;
				} else if (distSpot == 7) {
					sat = pad.topSat;
					lum = pad.topLum;

					origSat = maxSat;
					origLum = maxLum;
				}
			}





//			sat = 0.60;  //0.998
//			lum = 0.70; //0.998
//
//
//			if (distSpot == 0)
//			{
//				sat = 0.998;
//				lum = 0.40;
//			} else if (distSpot == 1) {
//				sat = 0.998;
//				lum = 0.40;
//			} else if (distSpot == 2) {
//				sat = 0.40;
//				lum = 0.30;
//			} else if (distSpot == 3) {
//				sat = 0.40;
//				lum = 0.40;
//			} else if (distSpot == 4) {
//				sat = 0.40;
//				lum = 0.50;
//			} else if (distSpot == 5) {
//				sat = 0.60;
//				lum = 0.50;
//			} else if (distSpot == 6) {
//				sat = 0.60;
//				lum = 0.60;
//			} else if (distSpot == 7) {
//				sat = 0.60;
//				lum = 0.70;
//			}






//			sat = 0.998;
//			lum = 0.998;
//
//			if (distSpot == 0)
//			{
//				sat = 0.998;
//				lum = 0.40
//			} else if (distSpot == 1) {
//				sat = 0.998;
//				lum = 0.40;
//			} else if (distSpot == 2) {
//				sat = 0.10;
//				lum = 0.50
//			} else {
//				sat = CGFloat(distSpot-2)*0.20;
//				lum = CGFloat(distSpot-2)*0.20;
//			}
//
////print("1 sat,lum:",sat,lum)
//
//			if (distSpot == 3)
//			{
//				lum = 0.50
//			}
//
////print("2 sat,lum:",sat,lum)
////othe rHalf = true;
//
//			if (otherHalf)
//			{
//				if (distSpot == 1)
//				{
//					sat = 0.998;
//					lum = 0.40;
//				} else if (distSpot == 2) {
//					sat = 0.90;
//					lum = 0.60;
//				} else if (distSpot == 3) {
//					sat = 0.998;
//					lum = 0.80;
//				} else if (distSpot == 4) {
//					sat = 0.998;
//					lum = 0.90;
//				} else if (distSpot == 5) {
//					sat = 0.60;
//					lum = 0.80;
//				} else if (distSpot == 6) {
//					sat = 0.50;
//					lum = 0.80;
//				} else if (distSpot == 7) {
//					sat = 0.30;
//					lum = 0.80;
//				}
//			}

			self.indicatorDefaultWellColorFromChip();
		}
	}

	//----------------------------------------------------------------------------------------------------------
	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
	{
//        super.touchesEnded(touches, with:event);
//print("IndicatorImageView touchesEnded")
//print("  ")
		if let event0 = event
		{
			if (event0.timestamp - beginDragTimeStamp < 1.0)
			{
				self.center = self.origCenter;
				currentPaintImageController.scrollView.touchPadView.touchesEnded(touches, with:event);
			} else {

				if (self.shouldMove)
				{
					isBump = true;
					self.indicatorDefaultWellColorFromChip();
				}
			}
		}
	}

	//----------------------------------------------------------------------------------------------------------
	override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?)
	{
//        super.touchesCancelled(touches, with:event);
	}

	//----------------------------------------------------------------
	func indicatorRotationAngleOfPoint(_ cpoint:CGPoint) -> CGFloat
	{
		return (atan2(cpoint.y, -cpoint.x) - mpi2);
	}

	//----------------------------------------------------------------
	func indicatorDefaultWellColorFromChip()
	{
//print("indicatorDefaultWel lColorFromChip")
		if let currentWell = currentPaintImageController.currentWell
		{
			var angleAdj:CGFloat = self.adjustedAngle00;
			currentPaintImageController.scrollView.touchPadView.hue = angleAdj;

//print("indicatorDefaultWel lColorFromChip angleAdj:",angleAdj)

			if (self.adjustedAngle00 < 0.0)
			{
				angleAdj = CGFloat.pi + (angleAdj + CGFloat.pi);
			}

			currentPaintImageController.scrollView.touchPadView.sat = self.sat;
			currentPaintImageController.scrollView.touchPadView.lum = self.lum;
			currentPaintImageController.gradientViewSaturation.satFrameOnly(self.sat);
			currentPaintImageController.gradientViewLuminosity.lumFrameOnly(self.lum);

			let hueNormal:CGFloat = angleAdj / mpi2;

			defaultLineColor = UIColor(hue:hueNormal, saturation:self.sat, brightness:self.lum, alpha:1.0);

			let lineColor:UIColor = UIColor(hue:hueNormal, saturation:self.origSat, brightness:self.origLum, alpha:1.0);
//print("indicatorDefaultWel lColorFromChip  lineColor:",lineColor)
			currentWell.backgroundColor = lineColor;

			currentPaintImageController.valuesChanged();
			currentPaintImageController.scrollView.touchPadView.runThroughHueFilters();
		}
	}
}
