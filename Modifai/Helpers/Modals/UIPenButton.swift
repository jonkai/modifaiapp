import Foundation
import UIKit

//--------------------------------------------------------------------------------------
class UIPenButton: UIButton
{
//	var deleteCornerRecog: UILongPressGestureRecognizer!
//	var lockCornerRecog: UITapGestureRecognizer!
	public var touchOffset: CGPoint = CGPoint()

	// MARK:  - touches actions
	//----------------------------------------------------------------------------------------------------------
	override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
	{
//print("UIPen Button touch esBegan");
		super.touchesBegan(touches, with:event);

		if let curPaint = currentPaintImageController
		{
			curPaint.scrollView.delaysContentTouches = false;
			let point:CGPoint = event?.allTouches?.first?.location(in:curPaint.scrollView.imageViewOverlay) ?? CGPoint.zero;

			self.touchOffset = CGPoint(x:point.x - self.center.x, y:point.y - self.center.y);
			lastPoint02 = self.center;
		}
	}

	//----------------------------------------------------------------------------------------------------------
	override public func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
	{
//print("UIPen Button touchesMoved");
		super.touchesMoved(touches, with:event);

		if let paintCont = currentPaintImageController
		{
			let point:CGPoint = event?.allTouches?.first?.location(in:paintCont.scrollView.imageViewOverlay) ?? CGPoint.zero;
			let pointN = CGPoint(x:point.x - self.touchOffset.x, y:point.y - self.touchOffset.y);
			self.center = pointN;

			if (self.tag == 1)
			{
				paintCont.scrollView.deleteDimButton.center = CGPoint(x:self.center.x - (maxDistance * 1.75), y:self.center.y - (maxDistance * 1.75));

				if (self.farEnough())
				{
					paintCont.scrollView.curvePts.insert(pointN, at: 0)
					lastPoint02 = pointN;
				}
			}

			if (self.tag == 2)
			{
				paintCont.scrollView.editDimButton.center = CGPoint(x:self.center.x + (maxDistance * 1.75), y:self.center.y - (maxDistance * 1.75));

				if (self.farEnough())
				{
					paintCont.scrollView.curvePts.append(pointN)
					lastPoint02 = pointN;
				}
			}

			paintCont.scrollView.drawPaintEraseAt()
		}
	}

	//----------------------------------------------------------------------------------------------------------
	override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
	{
//print("UIPen Button touchesEnded");
		super.touchesEnded(touches, with:event);

		if let paintCont = currentPaintImageController
		{
			paintCont.scrollView.delaysContentTouches = true;   //*************** change 0.0.20 maybe should be false ***  //............ check test ...

			let touchedPt:CGPoint = self.center;
			if let point:CGPoint = paintCont.scrollView.curvePts.last
			{

				if ( !closeToXY(point, touchedPt, 2.0))
				{
					if (self.tag == 1)
					{
						paintCont.scrollView.curvePts.insert(touchedPt, at: 0)
					} else if (self.tag == 2) {
						paintCont.scrollView.curvePts.append(touchedPt);
					}
				}
			}

			paintCont.scrollView.drawPaintEraseAt()
		}
	}

	//----------------------------------------------------------------------------------------------------------
	override public func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		super.touchesCancelled(touches, with:event);
		self.touchesEnded(touches, with:event);
	}

	// MARK: - math
	//-------------------------------------------------------------
	func farEnough() -> Bool
	{
		let quadDistance:CGFloat = sqrt(((self.center.x - lastPoint02.x) * (self.center.x - lastPoint02.x)) +
										((self.center.y - lastPoint02.y) * (self.center.y - lastPoint02.y)))
		return quadDistance > (maxDistance * maxDistExtra) ? true : false
	}
}


