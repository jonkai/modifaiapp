import Foundation
import UIKit

//-----------------------------------------
class PopCollectionViewCell: UICollectionViewCell
{
	@IBOutlet weak var imgView: UIImageView!
	@IBOutlet weak var deleteBtn: UIButton!

	var projectThumb:ProjectThumb?

	//-----------------------------------------
    required override init(frame: CGRect)
    {
        super.init(frame: frame)
    }

	//-----------------------------------------
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }

	//-----------------------------------------
    override func prepareForReuse()
    {
        super.prepareForReuse()
    }
}
