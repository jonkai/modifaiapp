import UIKit
import PECimage

let maxLum:CGFloat = 0.70;
let maxSat:CGFloat = 0.60;

//********** change 0.0.40 replace whole swift file ***
//--------------------------------------------------------------------------------------
class RoundTouchPadView: UIView
{
//	@IBOutlet weak var scrollView:BaseScrollView!;

	@IBOutlet weak var protractorImage:UIView!
	@IBOutlet weak var protractInnerImage:UIView!

	@IBOutlet weak var indicator1:UIImageView!
	@IBOutlet weak var indicator2SpinnerView:UIView!
	@IBOutlet weak var indicator2:IndicatorImageView!
	@IBOutlet weak var colorWheelChip:UIImageView!

	@IBOutlet weak var contractExpandButton: UIButton!
	@IBOutlet weak var showKeyListButton: UIButton!
	@IBOutlet weak var gridButton: UIButton!
	@IBOutlet weak var snapButton: UIButton!
	@IBOutlet weak var controlPadButton: UIButton!
	@IBOutlet weak var moveButton: UIButton!
	@IBOutlet weak var cutUpButton: UIButton!

	@IBOutlet weak var noteButton: UIButton!
	@IBOutlet weak var markUpButton: UIButton!
	@IBOutlet weak var areaButton: UIButton!
	@IBOutlet weak var directButton: UIButton!
	@IBOutlet weak var offsetButton: UIButton!
	@IBOutlet weak var arrowButton: UIButton!
	@IBOutlet weak var keyNoteButton: UIButton!

//	var startHue:CGFloat = 0.0;scrollView
//	var startSat:CGFloat = 0.0;
//	var startLum:CGFloat = 0.0;
//	var adjLum:CGFloat = 0.0;

	var topLum:CGFloat = maxLum;
	var lumStep:CGFloat = 0.10;

	var topSat:CGFloat = maxSat;
	var satStep:CGFloat = 0.20;
	var satBigStep:CGFloat = 2.0;

	var hue:CGFloat = 0.0;
	var sat:CGFloat = 0.0;
	var lum:CGFloat = 0.0;

	var currentView:UIView!; //PECLayerView
	var pecImage:UIImage?;
//	var pecImageFull:UIImage?;
	var hueAdjustFilter:CIFilter!;
	var saturationFilter:CIFilter!;

	var startColor:UIColor!;
	var currentProtractorAngle:CGFloat = 0.0;

	var angleOfStart:CGFloat = 0.0;

	var satLayer:CALayer! = CALayer();
	var lumLayer:CALayer! = CALayer();

	var adjustedAngleDiff:CGFloat = 0.0;
	var currentAngle:CGFloat = 0.0;
	var currentInnerAngle:CGFloat = 0.0;

	var doProtractor:Bool = false;
	var doProtractInner:Bool = false;
	var currentStart:CGPoint = CGPoint();
	var beginDragTimeStamp:TimeInterval = TimeInterval();
	var lastAngle:CGFloat = 0.0;

	var wasJump:Bool = false;
	var isFinal:Bool = false;
	var isThump:Bool = false;
	var roundTouchPadLengthToCenter:CGFloat = 0.0;


	// MARK: - touches actions
	//----------------------------------------------------------------------------------------------------------
	override func point(inside point:CGPoint, with event:UIEvent?) -> Bool
	{
		var shouldTouchStayOnThisLevel:Bool = false;
		self.currentStart = CGPoint(x:point.x - 340.0, y:-(point.y - 340.0));
//print("point,currentStart:",point,currentStart)

		self.roundTouchPadLengthToCenter =  sqrt((currentStart.x * currentStart.x) + (currentStart.y * currentStart.y));
		if (self.roundTouchPadLengthToCenter <= touchPadLimit) {shouldTouchStayOnThisLevel = true;}

//print("RoundTouchP adView self.roundTouchPadLengthToCenter, shouldTouchStayOnThisLevel: ",self.roundTouchPadLengthToCenter,shouldTouchStayOnThisLevel);

		return shouldTouchStayOnThisLevel;
	}

	//----------------------------------------------------------------------------------------------------------
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
	{
//print("round touchesBegan")
        super.touchesBegan(touches, with:event);
//		doProtractor = false;
		doProtractor = true;
		doProtractInner = false;
	//	self.adjustedAngleDiff = 0.0;

		if let event0 = event
		{
			beginDragTimeStamp = event0.timestamp;
		}
	//	wasMaskBump = false;
	//	otherHa lf = false;
//print("round touchesBegan self.currentAngle: ", self.currentAngle);

		isFinal = false;



//		if (self.roundTouchPadLengthToCenter >= 125.0)
//		{
//			doProtractor = true;

//print("  ")
//print("  ")
//print("round touchesBegan lastAngle:",lastAngle)
			let spotAngle:CGFloat = self.rotationAngleOfPoint(self.currentStart);
			angleOfStart = spotAngle;
			currentProtractorAngle = self.currentAngle;

//			self.indic ator2SpinnerView.layer.transform = CATransform3DInvert(self.protractorImage.layer.transform)
//print("round touchesBegan self.currentA ngle,mpiDiv6,-self.currentA ngle-mpiDiv6:",self.currentA ngle,mpiDiv6,-self.currentA ngle-mpiDiv6)
//print("round touchesBegan self.indica tor2SpinnerView.layer.transform:",self.indicat or2SpinnerView.layer.transform)

			lastAngle = currentProtractorAngle;
//print("round touchesBegan currentProtractorAngle:, angleOfStart: ", currentProtractorAngle,angleOfStart);
//		}
	}

	//----------------------------------------------------------------------------------------------------------
	override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
	{
//print("  ")
//print("  ")
//print("round touchesMoved")
        super.touchesMoved(touches, with:event);

		if let event0 = event
		{
			let currentSpot:CGPoint = self.locationFromCenterPt(event0.allTouches?.first?.location(in:self) ?? CGPoint.zero)

			let spotAngle:CGFloat = self.rotationAngleOfPoint(currentSpot);
			let adjustedAngle0:CGFloat = currentProtractorAngle + spotAngle - angleOfStart;
//print("round touchesMoved adjustedAngle0,currentProtrac torAngle,spotAngle,angleO fStart: ", adjustedAngle0,currentProtr actorAngle,spotAngle,angleOfStart);

			wasJump = false;
			var adjustedAngle:CGFloat = adjustedAngle0;

			if (adjustedAngle >  CGFloat.pi) {adjustedAngle = -CGFloat.pi + (adjustedAngle-CGFloat.pi);}
			if (adjustedAngle < -CGFloat.pi) {adjustedAngle =  CGFloat.pi + (adjustedAngle+CGFloat.pi);}
//print("round touchesMoved adjustedAngle: ", adjustedAngle);
//print("1 round touchesMoved self.currentA ngle: ", self.currentA ngle);
//print("1 round touchesMoved lastAngle,adjustedAngle: ", lastAngle,adjustedAngle);

//print("1 round touchesMoved self.currentAngle: ", self.currentAngle);

			if ((lastAngle < 3.0) && (adjustedAngle > 3.0))
			{
//print(" lastAngle < 3.0) && (adjustedAngle > 3.0 ");
				self.protractorImage.layer.transform = CATransform3DMakeRotation(adjustedAngle, 0, 0, 1);
				self.currentAngle = adjustedAngle;
				self.setDefaultWellColorFrom(adjustedAngle);
				wasJump = true;

			} else if ((lastAngle > 3.0) && (adjustedAngle < 3.0)) {
//print(" (lastAngle > 3.0) && (adjustedAngle < 3.0) ");
				self.protractorImage.layer.transform = CATransform3DMakeRotation(adjustedAngle, 0, 0, 1);
				self.currentAngle = adjustedAngle;
				self.setDefaultWellColorFrom(adjustedAngle);
				wasJump = true;
			}
//print("2 round touchesMoved self.currentAngle: ", self.currentAngle);

			lastAngle = adjustedAngle;

			if (event0.timestamp - beginDragTimeStamp < 0.025)
			{
//print("  ")
//print(" event0.timestamp - beginDrag TimeStamp < 0.025 ");
				return;
			}

//print("  ")
//print(" doProtractor wasJump",doProtractor,wasJump);
			shouldSave = true;

			if (doProtractor && !wasJump)
			{
				let animation:CABasicAnimation = CABasicAnimation(keyPath:"transform.rotation.z")
				animation.fromValue = self.currentAngle;
				animation.toValue = adjustedAngle;
				animation.duration = 0.0;
				animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)

				CATransaction.begin();
				CATransaction.setDisableActions(true);
				self.protractorImage.layer.add(animation, forKey:nil);
				self.protractorImage.layer.transform = CATransform3DMakeRotation(adjustedAngle, 0, 0, 1);

				CATransaction.commit();

				self.currentAngle = adjustedAngle;
//print("touchesMoved  self.currentAngle: ", self.currentAngle);

				self.setDefaultWellColorFrom(adjustedAngle);
			}
		}
	}

	//----------------------------------------------------------------------------------------------------------
	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
	{
//print("round  touchesEnded ")
        super.touchesEnded(touches, with:event);

		if (proOnorOff)
		{
			self.shouldBump()
		} else {

			if let event0 = event
			{
				if (event0.timestamp - beginDragTimeStamp < 1.25)
				{
					if abs(self.currentAngle - currentProtractorAngle) < 0.025
					{
//						var otherHalf:Bool = false;

						let indicatorPt:CGPoint = event0.allTouches?.first?.location(in:self) ?? CGPoint.zero
						let currentSpot:CGPoint = self.locationFromCenterPt(indicatorPt)
						let spotAngle:CGFloat = self.rotationAngleOfPoint(currentSpot);
						var spotAngle0:CGFloat = spotAngle;

						if (spotAngle < -mpi2)
						{
							spotAngle0 = -CGFloat.pi - (-mpi3 - spotAngle)
						}

						let adjustedAngle00:CGFloat = currentProtractorAngle - (spotAngle0 + pip30);

						let dist =  sqrt((currentSpot.x * currentSpot.x) + (currentSpot.y * currentSpot.y));
						let distSpotf:Float =  (Float(dist) - 67.1) / 33.79278;  //84.0
						let distSpot:Float =  Float(Int(distSpotf));

//print("otherH alf:",otherH alf)

						self.indicator2.center = indicatorPt;
						self.indicator2SpinnerView.layer.transform = CATransform3DInvert(self.protractorImage.layer.transform)

						var sat:CGFloat = self.topSat;
						var lum:CGFloat = self.topLum;

						var origSat:CGFloat = maxSat;
						var origLum:CGFloat = maxLum;

						if (distSpot == 0)
						{
							sat = self.topSat + (self.satStep * self.satBigStep);
							lum = self.topLum - (self.lumStep * 3.0);

							origSat = maxSat + 0.40;
							origLum = maxLum - 0.30;
						} else if (distSpot == 1) {
							sat = self.topSat + (self.satStep * self.satBigStep);
							lum = self.topLum - (self.lumStep * 3.0);

							origSat = maxSat + 0.40;
							origLum = maxLum - 0.30;
						} else if (distSpot == 2) {
							sat = self.topSat - self.satStep;
							lum = self.topLum - (self.lumStep * 4.0);

							origSat = maxSat - 0.20;
							origLum = maxLum - 0.40;
						} else if (distSpot == 3) {
							sat = self.topSat - self.satStep;
							lum = self.topLum - (self.lumStep * 3.0);

							origSat = maxSat - 0.20;
							origLum = maxLum - 0.30;
						} else if (distSpot == 4) {
							sat = self.topSat - self.satStep;
							lum = self.topLum - (self.lumStep * 2.0);

							origSat = maxSat - 0.20;
							origLum = maxLum - 0.20;
						} else if (distSpot == 5) {
							sat = self.topSat;
							lum = self.topLum - (self.lumStep * 2.0);

							origSat = maxSat;
							origLum = maxLum - 0.20;
						} else if (distSpot == 6) {
							sat = self.topSat;
							lum = self.topLum - self.lumStep;

							origSat = maxSat;
							origLum = maxLum - 0.10;
						} else if (distSpot == 7) {
							sat = self.topSat;
							lum = self.topLum;

							origSat = maxSat;
							origLum = maxLum;
						}



						if (currentPaintImageController.currentWell != nil)
						{
							currentPaintImageController.gradientViewSaturation.satFrameOnly(sat);
							currentPaintImageController.gradientViewLuminosity.lumFrameOnly(lum);
					//		currentPaintImageController.valuesChanged();

							isBump = true;
							self.setDefaultWellColorFromChip(adjustedAngle00, sat:sat, lum:lum);

							var angleAdj:CGFloat = adjustedAngle00;

							if (adjustedAngle00 < 0.0)
							{
								angleAdj = CGFloat.pi + (angleAdj + CGFloat.pi);
							}

							let hueNormal:CGFloat = angleAdj / mpi2;


							let lineColor:UIColor = UIColor(hue:hueNormal, saturation:origSat, brightness:origLum, alpha:1.0);
//print("round touchpad lineColor:",lineColor)

							currentPaintImageController.currentWell.backgroundColor = lineColor;
						}
					}
				}
			}
		}
	}

	//----------------------------------------------------------------------------------------------------------
	override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?)
	{
        super.touchesCancelled(touches, with:event);
	}

	//----------------------------------------------------------------------------------------------------------
	func shouldEnd()
	{
//print(" shouldEnd ");
		isFinal = true;
		self.setDefaultWellColor()
	}

	//----------------------------------------------------------------------------------------------------------
	func shouldBump()
	{
//print(" shou ldBump ");
		isBump = true;
		self.setDefaultWellColor();
	}

	//----------------------------------------------------------------------------------------------------------
	func updateData()
	{
//print("round updat eData currentO verlay:",currentPaintImageController.currentOverlay as Any)
//print("round updat eData currentO verlay.highlight edImage:",currentPaintImageController.currentOverlay?.highlightedImage as Any)
		guard let currentOverlay = currentPaintImageController.currentOverlay else {self.pecImage = nil; return;}
		guard let image = currentOverlay.highlightedImage else { self.pecImage = nil; return;}

//		self.pecImageFull = image;
		let targetSize:CGSize = CGSize(width:image.size.width*0.50, height:image.size.height*0.50);

		autoreleasepool
		{
			UIGraphicsBeginImageContextWithOptions(targetSize, false, screenScale);
				image.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
				self.pecImage = UIGraphicsGetImageFromCurrentImageContext();
			UIGraphicsEndImageContext();
		}
	}


	// MARK: - Gesture Recognizers
	//----------------------------------------------------------------------------------------------------------
	@IBAction func doubleDownTool(_ recognizer: UITapGestureRecognizer)
	{

	}

	// MARK: -  protractor rotation math:
	//----------------------------------------------------------------
	func controlIndexForAngle(_ angle:CGFloat) -> Int
	{
		let index:Int = Int(round((((mpi2 - angle) - minInner) / fifteenRad) - 0.34));
		return index;
	}

	//----------------------------------------------------------------
	func toolIndexForAngle(_ angle:CGFloat) -> Int
	{
		let index:Int = Int(round((((mpi2 - angle) - minProt) / fifteenRad) - 0.34));
		return index;
	}

	//----------------------------------------------------------------
	func controlIndexForAngleAtdefault(_ angle:CGFloat) -> Int
	{
		let index:Int = Int(round((angle-minInner)/fifteenRad));
		return index;
	}

	//----------------------------------------------------------------
	func toolIndexForAngleAtdefault(_ angle:CGFloat) -> Int
	{
		let index:Int = Int(round((angle-minProt)/fifteenRad));
		return index;
	}

	//----------------------------------------------------------------
	func locationFromCenterPt(_ cpoint:CGPoint) -> CGPoint
	{
		let cpoint0:CGPoint = CGPoint(x:cpoint.x - 340.0, y:-(cpoint.y - 340.0));
//print("cpoint,cpoint0:",cpoint,cpoint0)
		return cpoint0;
	}

	//----------------------------------------------------------------
	func rotationAngleOfPoint(_ cpoint:CGPoint) -> CGFloat
	{
		return (atan2(cpoint.y, -cpoint.x) - mpi2);
	}

	//----------------------------------------------------------------
	func nearestAngleToCurrentControl() -> CGFloat
	{
//		curWorkSheet.controlIndex = self.controlIndexForAngleAtdefault(self.currentInnerAngle);
//		return (fifteenRad*curWorkSheet.controlIndex) + minInner;
return 0.0;
	}

	//----------------------------------------------------------------
	func nearestAngleToCurrentTool() -> CGFloat
	{
//		curWorkSheet.toolIndex = self.toolIndexForAngleAtdefault(self.currentAn gle);
//		return (fifteenRad*curWorkSheet.toolIndex) + minProt;
return 0.0;
	}

	//----------------------------------------------------------------
	func snapToControlIndex(_ anIndex:Int)
	{
		let nearestControlAngle:CGFloat = (fifteenRad*CGFloat(anIndex)) + minInner;
		self.rotateToIndexUsingInnerAngle(nearestControlAngle);
	}

	//----------------------------------------------------------------
	func snapToToolIndex(_ anIndex:Int)
	{
//print(" snapToToolIndex")
		let nearestToolAngle:CGFloat = 0.0;
		self.rotateToIndexUsingAngle(nearestToolAngle);
	}

//	//----------------------------------------------------------------
//	func snapToSta rtColor()
//	{
////print("snapToSt artColor origStartC olor:",origStartC olor)
//		var h, s, l, a:CGFloat
//		var starth, starts, startl, starta:CGFloat
//			s = 1.0;
//			l = 0.5;
//			h = 0.15;
//			a = 1.0;
//
//		if (defaultLineColor.getHue(&h, saturation:&s, brightness:&l, alpha:&a))
//		{
////		} else {
////			s = 1.0;
////			l = 0.5;
////			h = 0.15;
////			a = 1.0;
//		}
//			starts = s;
//			startl = l;
//			starth = h;
//			starta = a;
//
//		if (origStartCo lor.getHue(&starth, saturation:&starts, brightness:&startl, alpha:&starta))
//		{
////		} else {
////			starts = s;
////			startl = l;
////			starth = h;
////			starta = a;
//		}
//
//		self.startC olor = origStar tColor.copy() as? UIColor
//		self.sat = s;
//		self.star tSat = starts;
//		self.lum = l;
//		self.startLum = startl;
//
//		let angle:CGFloat = (mpi2 * h);
//		self.hue = angle;
////print("snapToStar tColor angle",angle)
////print("snapToSt artColor self.startC olor:",self.startC olor as Any)
//
//        let angleStart:CGFloat = (mpi2 * starth);
//        self.star tHue = angleStart;
//
//		isB ump = true;
//		self.setDefaultWel lColor();
//
////		self.rotateToInde xUsingAngle(angle);
////		currentPaintImageController.valuesCha nged();
//	}

	//----------------------------------------------------------------
	func snapToDefaultColor()
	{
//print("round snapToDefa ultColor")
		var h, s, l, a:CGFloat
			s = 1.0;
			l = 0.5;
			h = 0.15;
			a = 1.0;

		defaultLineColor.getHue(&h, saturation:&s, brightness:&l, alpha:&a)

		self.sat = s;
		self.lum = l;

		self.startColor = origStartColor.copy() as? UIColor


		var hs, ss, ls:CGFloat
			ss = 1.0;
			ls = 1.0;
			hs = 0.15;
			a = 1.0;

		self.startColor.getHue(&hs, saturation:&ss, brightness:&ls, alpha:&a)

		self.topLum = maxLum;
		self.lumStep = 0.10;

		self.topSat = maxSat;
		self.satStep = 0.20;
		self.satBigStep = 2.0;

		var adjLum:CGFloat = 0.0;
//		var adjustedSat:CGFloat = 0.0;

		if (ls < 0.58)
		{
			let lumScale = CGFloat(ls / self.topLum)
			let brightness:CGFloat = 1.0 - lumScale;
//print("1 ls,lumScale,brightness:",ls,lumScale,brightness)

			if (brightness > 0.001)
			{
				adjLum = brightness;
//print("1 adjLum:",adjLum)
				if (brightness >= 0.5) {adjLum = 0.5 + ((brightness - 0.5) * 1.15);}
//print("2 adjLum:",adjLum)

				let adjSat = 1.0 - (adjLum * 0.25);
				adjLum = 1.0 - (adjLum * 0.5);
//				adjustedSat = 1.0 - (pow(brightness * 1.5,1.4));
//print("3 adjLum,adjustedSat:",adjLum,adjustedSat)

				self.topLum *= adjLum
				self.lumStep =  ((self.topLum / maxLum) * 0.10) * 0.75;

				self.topSat *= adjSat
				self.satStep =  (self.topSat / maxSat) * 0.20;
				self.satBigStep = 1.25;
			}
		}

//print("4 self.topLum,self.lumStep:",self.topLum,self.lumStep)
//print("4 self.topSat,self.satStep:",self.topSat,self.satStep)



		var angle:CGFloat = (mpi2 * h);
//print("   snapToDef aultColor angle, h",angle,h)
		if (angle >  CGFloat.pi) {angle = -CGFloat.pi + (angle-CGFloat.pi);}
		if (angle < -CGFloat.pi) {angle =  CGFloat.pi + (angle+CGFloat.pi);}

//print(" 2 snapToDef aultColor angle:",angle)

		self.hue = angle;
//		self.startHue = angle;

//print(" snapToDefa ultColor  angle:",angle)
		self.rotateToIndexUsingAngle(angle);
		currentPaintImageController.valuesChanged();
	}

//    //----------------------------------------------------------------
//    func setUpHueSatLum(_ h:CGFloat, s:CGFloat, l:CGFloat)
//    {
////print("round setUpHu eSatLum")
//   //     self.star tColor = defaultL ineColor.copy() as? UIColor
////print("setUpHueS atLum self.startCo lor:",self.startCo lor as Any)
//
//        self.star tSat = s;
//        self.startLum = l;
//
//        self.sat = s;
//        self.lum = l;
//
//        let angle:CGFloat = (mpi2 * h);
////print(" ")
////print("setUpHu eSatLum  angle:",angle)
////print("self.start Color:",self.sta rtColor as Any)
////print("default LineColor:",defaultLi neColor)
//
//        self.hue = angle;
//        self.star tHue = angle;
//
//        self.rotateToInde xUsingAngle(angle);
//        currentPaintImageController.valuesChanged()
//    }

    //----------------------------------------------------------------
    func snapToOriginalColor()
    {
		self.startColor = origStartColor.copy() as? UIColor

		var hs, ss, ls, a:CGFloat
			ss = 1.0;
			ls = 1.0;
			hs = 0.15;
			a = 1.0;

		self.startColor.getHue(&hs, saturation:&ss, brightness:&ls, alpha:&a)

		self.topLum = maxLum;
		self.lumStep = 0.10;

		self.topSat = maxSat;
		self.satStep = 0.20;
		self.satBigStep = 2.0;

		var adjLum:CGFloat = 0.0;
//		var adjustedSat:CGFloat = 0.0;

		if (ls < 0.58)
		{
			let lumScale = CGFloat(ls / self.topLum)
			let brightness:CGFloat = 1.0 - lumScale;
//print("1 ls,lumScale,brightness:",ls,lumScale,brightness)

			if (brightness > 0.001)
			{
				adjLum = brightness;
//print("1 adjLum:",adjLum)
				if (brightness >= 0.5) {adjLum = 0.5 + ((brightness - 0.5) * 1.15);}
//print("2 adjLum:",adjLum)

				let adjSat = 1.0 - (adjLum * 0.25);
				adjLum = 1.0 - (adjLum * 0.5);
//				adjustedSat = 1.0 - (pow(brightness * 1.5,1.4));
//print("3 adjLum,adjustedSat:",adjLum,adjustedSat)

				self.topLum *= adjLum
				self.lumStep =  ((self.topLum / maxLum) * 0.10) * 0.75;

				self.topSat *= adjSat
				self.satStep =  (self.topSat / maxSat) * 0.20;
				self.satBigStep = 1.25;
			}
		}

//print("4 self.topLum,self.lumStep:",self.topLum,self.lumStep)
//print("4 self.topSat,self.satStep:",self.topSat,self.satStep)

        self.runThroughHueFilters();
    }

	//----------------------------------------------------------------
	func setHueSatLumFromRed() //_ r:CGFloat, g:CGFloat, b:CGFloat)
	{
//print("set Hue SatLum FromRed")
		self.startColor = origStartColor.copy() as? UIColor

		self.sat = sat00;
		self.lum = lum00;

		var hs, ss, ls, a:CGFloat
			ss = 1.0;
			ls = 1.0;
			hs = 0.15;
			a = 1.0;

		self.startColor.getHue(&hs, saturation:&ss, brightness:&ls, alpha:&a)

		self.topLum = maxLum;
		self.lumStep = 0.10;

		self.topSat = maxSat;
		self.satStep = 0.20;
		self.satBigStep = 2.0;

		var adjLum:CGFloat = 0.0;
//		var adjustedSat:CGFloat = 0.0;

		if (ls < 0.58)
		{
			let lumScale = CGFloat(ls / self.topLum)
			let brightness:CGFloat = 1.0 - lumScale;
//print("1 ls,lumScale,brightness:",ls,lumScale,brightness)

			if (brightness > 0.001)
			{
				adjLum = brightness;
//print("1 adjLum:",adjLum)
				if (brightness >= 0.5) {adjLum = 0.5 + ((brightness - 0.5) * 1.15);}
//print("2 adjLum:",adjLum)

				let adjSat = 1.0 - (adjLum * 0.25);
				adjLum = 1.0 - (adjLum * 0.5);
//				adjustedSat = 1.0 - (pow(brightness * 1.5,1.4));
//print("3 adjLum,adjustedSat:",adjLum,adjustedSat)

				self.topLum *= adjLum
				self.lumStep =  ((self.topLum / maxLum) * 0.10) * 0.75;

				self.topSat *= adjSat
				self.satStep =  (self.topSat / maxSat) * 0.20;
				self.satBigStep = 1.25;
			}
		}

//print("4 self.topLum,self.lumStep:",self.topLum,self.lumStep)
//print("4 self.topSat,self.satStep:",self.topSat,self.satStep)


		var angle:CGFloat = (mpi2 * hueNormal00);
//print("setHueS atLumFromRed    angle    : ", angle );

		if (angle >  CGFloat.pi) {angle = -CGFloat.pi + (angle-CGFloat.pi);}
		if (angle < -CGFloat.pi) {angle =  CGFloat.pi + (angle+CGFloat.pi);}

		self.hue = angle;

		currentPaintImageController.gradientViewSaturation.satFrameOnly(sat00);
		currentPaintImageController.gradientViewLuminosity.lumFrameOnly(lum00);

		self.currentAngle = angle;
		self.lastAngle = angle;
//print(" 2 setHueSatL umFromRed self.currentAngle:",self.currentAngle)

        self.firstRotateToIndexUsingAngle(angle);
		currentPaintImageController.valuesChanged()

//		let satAdjust = 1.0 - self.sat;
//		let satAmount = satAdjust * 168.96389999975;





		var satSpot:Int = -2;
		var satDist:CGFloat = abs(self.sat - 1.00);

		if abs(self.sat - 0.60) < satDist
		{
			satSpot = 1;
			satDist = abs(self.sat - 0.60)
		}

		if abs(self.sat - 0.40) < satDist
		{
			satSpot = 0;
		}


		var lumSpot:Int = 6;
		var lumDist:CGFloat = abs(self.lum - 0.70);

		if abs(self.lum - 0.60) < lumDist
		{
			lumSpot = 5;
			lumDist = abs(self.lum - 0.60);
		}

		if abs(self.lum - 0.50) < lumDist
		{
			lumSpot = 4;
			lumDist = abs(self.lum - 0.50);
		}

		if abs(self.lum - 0.40) < lumDist
		{
			lumSpot = 3;
			lumDist = abs(self.lum - 0.40);
		}

		if abs(self.lum - 0.30) < lumDist
		{
			lumSpot = 2;
		}

//print(" 0 setHueSatL umFromRed lumSpot:",lumSpot)

		// ---------- final spot ------
		lumSpot += satSpot;
		lumSpot = min(max(lumSpot,1),7)

//print(" 1 setHueSatL umFromRed lumSpot:",lumSpot)


	//	let distSpotf:Float =  (Float(dist) - 67.1) / 33.79278;
//-2.55776 69.65776

		let distSpotf:CGFloat =  ((69.65776 + (CGFloat(lumSpot+1) * 33.79278)) - 16.89639) + 340.0;

//print(" 1 setHueSatL umFromRed distSpotf:",distSpotf)

		self.indicator2.center = CGPoint(x:distSpotf,y:340.0);
		self.indicator2SpinnerView.layer.transform = CATransform3DMakeRotation(-self.currentAngle-mpiDiv6, 0, 0, 1);
	}

	//----------------------------------------------------------------
	func setDefaultWellColor()
	{
//print("setDefaultW ellColor defaultLineColor0 :",defaultLineColor0)
//print("setDefaultW ellColor defaultLineColor  :",defaultLineColor)
//print("  ")
//print("setDefaultW ellColor currentPaintImageController.curr entWell :",currentPaintImageController.curr entWell as Any)
//print("setDefaultW ellColor currentPaintImageController.colorWellBut ton0 :",currentPaintImageController.colorWellBut ton0 as Any)
//print("31 setDefaultWellColor defaultLineColor :",Unmanaged.passUnretained(defaultLineColor).toOpaque())
//print("31 setDefaultWellColor defaultLineColor3:",Unmanaged.passUnretained(defaultLineColor3).toOpaque())
		if (currentPaintImageController.currentWell == nil)
		{
			if (alreadyDone)
			{
				self.pecImage = nil;
			//	self.pecImageFull = nil;

				shouldWriteToDisk = true;
				self.isHidden=true;
			}

			return;
		}

		var angleAdj:CGFloat = self.hue;
//print("setDefaultWel lColor angleAdj",angleAdj)

		if (angleAdj < 0.0)
		{
			angleAdj = CGFloat.pi + (angleAdj + CGFloat.pi);
		}

		let hueNormal:CGFloat = angleAdj / (mpi2);

		let satNormal:CGFloat = self.sat;
		let lumNormal:CGFloat = self.lum;

		defaultLineColor = UIColor(hue:hueNormal, saturation:satNormal, brightness:lumNormal, alpha:1.0);
		currentPaintImageController.currentWell.backgroundColor = defaultLineColor;

        currentPaintImageController.isPaintChanged = true
		currentPaintImageController.valuesChanged();
		self.runThroughHueFilters();
	}

	//----------------------------------------------------------------
	func setDefaultWellColorFromChip(_ angle:CGFloat, sat:CGFloat, lum:CGFloat)
	{
//print("setDefault WellColorFrom  angle:",angle)
//print("32 setDefaultWellColorFromChip defaultLineColor :",Unmanaged.passUnretained(defaultLineColor).toOpaque())
//print("32 setDefaultWellColorFromChip defaultLineColor3:",Unmanaged.passUnretained(defaultLineColor3).toOpaque())
		if (currentPaintImageController.currentWell == nil)
		{
			if (alreadyDone)
			{
				self.pecImage = nil;
			//	self.pecImageFull = nil;

				shouldWriteToDisk = true;
				self.isHidden=true;
			}

			return;
		}

		var angleAdj:CGFloat = angle;
		self.hue = angleAdj;

		if (angle < 0.0)
		{
			angleAdj = CGFloat.pi + (angleAdj + CGFloat.pi);
		}

		let hueNormal:CGFloat = angleAdj / mpi2;

		self.sat = sat;
		self.lum = lum;

		let satNormal:CGFloat = self.sat;
		let lumNormal:CGFloat = self.lum;

		defaultLineColor = UIColor(hue:hueNormal, saturation:satNormal, brightness:lumNormal, alpha:1.0);
//print(" ")
//print(" ")
//print("setDefaultWel lColorFromChip self.star tColor  :",self.star tColor as Any)
//print("setDefaultWel lColorFromChip defaultLineColor :",defaultLineColor)

//		currentPaintImageController.currentW ell.backgro undColor = defaultLineColor;
        currentPaintImageController.isPaintChanged = true
		currentPaintImageController.valuesChanged();
		self.runThroughHueFilters();
	}

	//----------------------------------------------------------------
	func setDefaultWellColorFrom(_ angle:CGFloat)
	{
//print("33 setDefaultWel lColorFrom defaultLineColor :",Unmanaged.passUnretained(defaultLineColor).toOpaque())
//print("33 setDefaultWel lColorFrom defaultLineColor3:",Unmanaged.passUnretained(defaultLineColor3).toOpaque())
//print(" ")
//print(" ")
//print("setDefaultW ellColorFrom defaultLineColor2 :",defaultLineColor2)
//print("setDefaultW ellColorFrom defaultLineColor1 :",defaultLineColor1)
//print("setDefaultW ellColorFrom defaultLineColor0 :",defaultLineColor0)
//print(" ")
//print("setDefaultW ellColorFrom origStartC olor2   :",origStartC olor2)
//print("setDefaultW ellColorFrom origStartC olor1   :",origStartC olor1)
//print("setDefaultW ellColorFrom origStartC olor0   :",origStartC olor0)
//print(" ")
//print("setDefaultW ellColorFrom defaultLineColor  :",defaultLineColor)
//print("setDefaultW ellColorFrom origStart Color    :",origStartC olor)
//print("setDefaultW ellColorFrom currentPaintImageController.curr entWell :",currentPaintImageController.curre ntWell as Any)
//print("setDefaultW ellColorFrom currentPaintImageController.colorWellBut ton0 :",currentPaintImageController.colorWellBut ton0 as Any)
//print("setDefault WellColorFrom  angle:",angle)

		if ( !proOnorOff) {return;}


		if (currentPaintImageController.currentWell == nil)
		{
			if (alreadyDone)
			{
				self.pecImage = nil;
			//	self.pecImageFull = nil;

				shouldWriteToDisk = true;
				self.isHidden=true;
			}

			return;
		}

		var angleAdj:CGFloat = angle;
		self.hue = angleAdj;

		if (angle < 0.0)
		{
			angleAdj = CGFloat.pi + (angleAdj + CGFloat.pi);
		}

		let hueNormal:CGFloat = angleAdj / mpi2;

		let satNormal:CGFloat = self.sat;
		let lumNormal:CGFloat = self.lum;


//print("  ");
//print("setDefaultW ellColorFrom  angleAdj,  hueNormal   abs(angleAdj): ",angleAdj, hueNormal,abs(angleAdj));


		defaultLineColor = UIColor(hue:hueNormal, saturation:satNormal, brightness:lumNormal, alpha:1.0);
//print("34 setDefaultWel lColorFrom defaultLineColor :",Unmanaged.passUnretained(defaultLineColor).toOpaque())

//		if abs(angleAdj - self.star tHue) > 1.0
//		{
//print("-------------------------------------- > 1.0 ---------------------------------------------------");
//
//			if let origIm age = currentPaintImageController.origi nalImage
//			{
//				let targetImageRect = CGRect(x:0.0, y:0.0, width:origImage.size.width, height:origImage.size.height);
//
//				autoreleasepool
//				{
//					if let mask = maskedHouseImage
//					{
//						UIGraphicsBeginImageContextWithOptions(targetImageRect.size, false, currentPaintImageController.screenScale);
//							origImage.draw(in: targetImageRect, blendMode: .normal, alpha: 1.0)
//							mask.draw(in: targetImageRect, blendMode: .destinationIn, alpha: 1.0)
//							let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage();
//						UIGraphicsEndImageContext();
//
//						let brightHueFilter = PECbrightH ue(self.startC olor, endColor:defaultLineColor)
//						self.pecIma geFull = image.filterWithOperation(brightHueFilter)
//					}
//
//					let targetSize:CGSize = CGSize(width:origImage.size.width*0.50, height:origImage.size.height*0.50);
//
//					autoreleasepool
//					{
//						UIGraphicsBeginImageContextWithOptions(targetSize, false, currentPaintImageController.screenScale);
//							self.pecIma geFull?.draw(in:CGRect(origin:(CGPoint(x:0, y:0)), size:targetSize));
//							self.pecI mage = UIGraphicsGetImageFromCurrentImageContext();
//						UIGraphicsEndImageContext();
//					}
//				}
//			}
//
//			self.startC olor = defaultLineColor;
//
//			self.star tSat = self.sat;
//			self.startLum = self.lum;
//			self.star tHue = angleAdj;
//		}
//
//
//		var redf1: CGFloat = 0.0;
//		var greenf1: CGFloat = 0.0;
//		var bluef1: CGFloat = 0.0;
//		var alphaf1: CGFloat = 1.0;
//
//		defaultLineColor.getRed(&redf1, green: &greenf1, blue: &bluef1, alpha: &alphaf1)
//
//print("setDefaultWel lColorFrom    redf1,greenf1,bluef1   : ", redf1,greenf1,bluef1 );



		currentPaintImageController.currentWell.backgroundColor = defaultLineColor;

//print("1 setDefaultW ellColorFrom defaultLineColor1 :",defaultLineColor1)
//print("1 setDefaultW ellColorFrom defaultLineColor0 :",defaultLineColor0)
//print("1 setDefaultW ellColorFrom defaultLineColor  :",defaultLineColor)

		currentPaintImageController.valuesChanged();
		self.runThroughHueFilters();

//print("35 setDefaultWel lColorFrom defaultLineColor :",Unmanaged.passUnretained(defaultLineColor).toOpaque())
	}

	//---------------------------------------------------------------------
	func runThroughHueFilters()
	{
        var image0 = self.pecImage
//print("image0:",image0 as Any)

//print("round runThroug hHueFilters  isBump || isFinal || isThump",isBump,isFinal, isThump)
//print("self.startC olor, defaultLineColor:",self.startC olor as Any, defaultLineColor as Any)

		if (isBump || isFinal || isThump)
		{
			isBump = false;
			isFinal = false;
			isThump = false;

			image0 = currentPaintImageController.currentOverlay.highlightedImage;
//print("full size image0:",image0 as Any)
		}

        if let image = image0
        {
			if (image.cgImage != nil)
			{
		//		let brightHueFilter = PECbrightContrast(self.star tColor, endColor:defaultLineColor)
				let brightHueFilter = PECbrightHue(self.startColor, endColor:defaultLineColor)
				currentPaintImageController.currentOverlay.image = image.filterWithOperation(brightHueFilter)
			} else {
  print("------- run error there was no cgImage on the image")
			}
        }
	}

	//----------------------------------------------------------------
	func snapToNearestControl()
	{
		let nearestControlAngle:CGFloat = self.nearestAngleToCurrentControl();
		self.rotateToIndexUsingInnerAngle(nearestControlAngle);
	}

	//----------------------------------------------------------------
	func snapToNearestTool()
	{
		let nearestToolAngle:CGFloat = self.nearestAngleToCurrentTool();
//print(" snapToNearestTool  nearestToolAngle:",nearestToolAngle)
		self.rotateToIndexUsingAngle(nearestToolAngle);
	}

	//----------------------------------------------------------------
	func rotateToIndexUsingInnerAngle(_ angle:CGFloat)
	{
		if (angle == self.currentInnerAngle) {return;}

		let animation:CABasicAnimation = CABasicAnimation(keyPath:"transform.rotation.z")
		animation.fromValue = self.currentInnerAngle;
		animation.toValue = angle;
		animation.duration = 0.0;
		animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)

		CATransaction.begin();
		CATransaction.setDisableActions(true);
		self.protractInnerImage.layer.add(animation, forKey:nil);
		self.protractInnerImage.layer.transform = CATransform3DMakeRotation(angle, 0, 0, 1);

		CATransaction.commit();

		self.currentInnerAngle = angle;
	}

	//----------------------------------------------------------------
	func rotateToIndexUsingAngle(_ angle:CGFloat)
	{
		if (angle == self.currentAngle) {return;}

		let animation:CABasicAnimation = CABasicAnimation(keyPath:"transform.rotation.z")
		animation.fromValue = self.currentAngle;
		animation.toValue = angle;
		animation.duration = 0.0;
		animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)

		CATransaction.begin();
		CATransaction.setDisableActions(true);
		self.protractorImage.layer.add(animation, forKey:nil);
		self.protractorImage.layer.transform = CATransform3DMakeRotation(angle, 0, 0, 1);

		CATransaction.commit();

		self.currentAngle = angle;
//print(" rotateToInd exUsingAngle self.curren tAngle:",self.currentAngle)
	}


	//----------------------------------------------------------------
	func firstRotateToIndexUsingAngle(_ angle:CGFloat)
	{
		let animation:CABasicAnimation = CABasicAnimation(keyPath:"transform.rotation.z")
		animation.fromValue = self.currentAngle;
		animation.toValue = angle;
		animation.duration = 0.0;
		animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)

		CATransaction.begin();
		CATransaction.setDisableActions(true);
		self.protractorImage.layer.add(animation, forKey:nil);
		self.protractorImage.layer.transform = CATransform3DMakeRotation(angle, 0, 0, 1);

		CATransaction.commit();

		self.currentAngle = angle;
//print(" firstRotateToIndexUsin gAngle self.curren tAngle:",self.currentAngle)
	}
}
