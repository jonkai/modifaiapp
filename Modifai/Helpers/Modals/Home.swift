import Foundation
import UIKit

//--------------------------------------
public class Home : NSObject  //*************** change 0.0.31 changes though out ***
{
    var garages: [Garage]
    var doors: [Door]
    var singleGarage:[SingleGarageObj]

    public init(responseData:ResponseData)
    {
        self.garages = responseData.garages
        self.doors = responseData.doors
        self.singleGarage = [SingleGarageObj]()

        for garage in responseData.singleGarage
        {
            let sGarage = SingleGarageObj(singleGarage: garage)
            self.singleGarage.append(sGarage)
        }
    }
}

//--------------------------------------
public class GarageObj : NSObject
{
    var title: String
    var subTitle: String
    var image: String
    var desc: String
    var price: String
    var catalogID: String
    var manufacturerID: Int

    var mask_url: String?
    var isPaintable: Int?
    var rValue: CGFloat?
    var gValue: CGFloat?
    var bValue: CGFloat?

    public init(garage:Garage)
    {
        self.title = garage.title
        self.subTitle = garage.subTitle
        self.image = garage.image
        self.desc = garage.desc
        self.price = garage.price
        self.catalogID = garage.catalogID
        self.manufacturerID = garage.manufacturerID

		self.mask_url = garage.mask_url ?? " "
		self.isPaintable = garage.isPaintable ?? 0
		self.rValue = garage.rValue ?? 0
		self.gValue = garage.gValue ?? 0
		self.bValue = garage.bValue ?? 0
    }
}

//--------------------------------------
public class DoorObj : NSObject
{
    var title: String
    var subTitle: String
    var image: String
    var desc: String
    var price: String
    var catalogID: String
    var manufacturerID: Int

    var mask_url: String?
    var isPaintable: Int?
    var rValue: CGFloat?
    var gValue: CGFloat?
    var bValue: CGFloat?

    public init(door:Door)
    {
        self.title = door.title
        self.subTitle = door.subTitle
        self.image = door.image
        self.desc = door.desc
        self.price = door.price
        self.catalogID = door.catalogID
        self.manufacturerID = door.manufacturerID

		self.mask_url = door.mask_url ?? " "
		self.isPaintable = door.isPaintable ?? 0
		self.rValue = door.rValue ?? 0
		self.gValue = door.gValue ?? 0
		self.bValue = door.bValue ?? 0
    }
}

//--------------------------------------
public class SingleGarageObj : NSObject
{
    var title: String
    var subTitle: String
    var image: String
    var desc: String
    var price: String
    var catalogID: String
    var manufacturerID: Int

    var mask_url: String?
    var isPaintable: Int?
    var rValue: CGFloat?
    var gValue: CGFloat?
    var bValue: CGFloat?

    public init(singleGarage:SingleGarage)
    {
        self.title = singleGarage.title
        self.subTitle = singleGarage.subTitle
        self.image = singleGarage.image
        self.desc = singleGarage.desc
        self.price = singleGarage.price
        self.catalogID = singleGarage.catalogID
        self.manufacturerID = singleGarage.manufacturerID

		self.mask_url = singleGarage.mask_url ?? " "
		self.isPaintable = singleGarage.isPaintable ?? 0
		self.rValue = singleGarage.rValue ?? 0
		self.gValue = singleGarage.gValue ?? 0
		self.bValue = singleGarage.bValue ?? 0

        super.init()
    }

    override init()
    {
        self.title = " "
        self.subTitle = " "
        self.image  = " "
        self.desc = " "
        self.price = " "
        self.catalogID = " "
        self.manufacturerID = 0

		self.mask_url = " "
		self.isPaintable = 0
		self.rValue = 0.0
		self.gValue =  0.0
		self.bValue = 0.0

        super.init()
    }
}

//--------------------------------------
public struct ResponseData: Codable
{
    var garages: [Garage]
    var doors: [Door]
    var singleGarage:[SingleGarage]
    
    public init(garages: [Garage],doors: [Door],singleGarage:[SingleGarage])
    {
        self.garages = garages
        self.doors = doors
        self.singleGarage = singleGarage
    }

    enum CodingKeys: String, CodingKey
    {
        case garages = "two_car_garage"
        case doors = "front_door"
        case singleGarage = "one_car_garage"
    }
}

//--------------------------------------
public struct Garage : Codable
{
    var title: String
    var subTitle: String
    var image: String
    var desc: String
    var price: String
    var catalogID: String
    var manufacturerID: Int

    var mask_url: String?
    var isPaintable: Int?
    var rValue: CGFloat?
    var gValue: CGFloat?
    var bValue: CGFloat?

    public init(title: String,subTitle: String,image: String,desc: String,price: String,catalogID: String,manufacturerID:Int,
    			mask_url:String?,isPaintable:Int?,rValue:CGFloat?,gValue:CGFloat?,bValue:CGFloat?)
    {
        self.title = title
        self.subTitle = subTitle
        self.image  = image
        self.desc = desc
        self.price = price
        self.catalogID = catalogID
        self.manufacturerID = manufacturerID

//print("0 mask_url:",mask_url as Any)

		self.mask_url = mask_url ?? " "
		self.isPaintable = isPaintable ?? 0
		self.rValue = rValue ?? 0
		self.gValue = gValue ?? 0
		self.bValue = bValue ?? 0
    }

    public init()
    {
        self.title = " "
        self.subTitle = " "
        self.image  = " "
        self.desc = " "
        self.price = " "
        self.catalogID = " "
        self.manufacturerID = 0

//print("init mask_url")

		self.mask_url = " "
		self.isPaintable = 0
		self.rValue = 0.0
		self.gValue =  0.0
		self.bValue = 0.0
    }

    enum CodingKeys: String, CodingKey
    {
        case title = "name"
        case subTitle = "brand"
        case image = "url_original"
        case desc = "descrip"
        case price = "price"
        case catalogID = "catalogID"
        case manufacturerID = "manufacturerID"

		case mask_url = "catalogmask_url"
		case isPaintable = "is_paintable"
		case rValue = "catalog_rvalue"
		case gValue = "catalog_gvalue"
		case bValue = "catalog_bvalue"
    }
}

//--------------------------------------
public struct SingleGarage : Codable
{
    var title: String
    var subTitle: String
    var image: String
    var desc: String
    var price: String
    var catalogID: String
    var manufacturerID: Int

    var mask_url: String?
    var isPaintable: Int?
    var rValue: CGFloat?
    var gValue: CGFloat?
    var bValue: CGFloat?

    public init(title: String,subTitle: String,image: String,desc: String,price: String,catalogID: String,manufacturerID:Int,
    			mask_url:String?,isPaintable:Int?,rValue:CGFloat?,gValue:CGFloat?,bValue:CGFloat?)
    {
        self.title = title
        self.subTitle = subTitle
        self.image  = image
        self.desc = desc
        self.price = price
        self.catalogID = catalogID
        self.manufacturerID = manufacturerID

//print("1 mask_url:",mask_url as Any)
		self.mask_url = mask_url ?? " "
		self.isPaintable = isPaintable ?? 0
		self.rValue = rValue ?? 0
		self.gValue = gValue ?? 0
		self.bValue = bValue ?? 0
    }

    public init()
    {
        self.title = " "
        self.subTitle = " "
        self.image  = " "
        self.desc = " "
        self.price = " "
        self.catalogID = " "
		self.manufacturerID = 0

		self.mask_url = " "
		self.isPaintable = 0
		self.rValue = 0.0
		self.gValue =  0.0
		self.bValue = 0.0
    }

    enum CodingKeys: String, CodingKey
    {
        case title = "name"
        case subTitle = "brand"
        case image = "url_original"
        case desc = "descrip"
        case price = "price"
        case catalogID = "catalogID"
        case manufacturerID = "manufacturerID"

		case mask_url = "catalogmask_url"
		case isPaintable = "is_paintable"
		case rValue = "catalog_rvalue"
		case gValue = "catalog_gvalue"
		case bValue = "catalog_bvalue"
    }
}

//--------------------------------------
public struct Door : Codable
{
    var title: String
    var subTitle: String
    var image: String
    var desc: String
    var price: String
    var catalogID: String
    var labelID:Int
    var typeID:Int
    var manufacturerID: Int

    var mask_url: String?
    var isPaintable: Int?
    var rValue: CGFloat?
    var gValue: CGFloat?
    var bValue: CGFloat?

    public init(title: String,subTitle: String,image: String,desc: String,price: String,
				catalogID: String,labelID:Int,typeID:Int, manufacturerID:Int,
    			mask_url:String?,isPaintable:Int?,rValue:CGFloat?,gValue:CGFloat?,bValue:CGFloat?)
    {
        self.title = title
        self.subTitle = subTitle
        self.image  = image
        self.desc = desc
        self.price = price
        self.catalogID = catalogID
        self.labelID = labelID
        self.typeID = typeID
        self.manufacturerID = manufacturerID

//print("2 mask_url:",mask_url as Any)
		self.mask_url = mask_url ?? " "
		self.isPaintable = isPaintable ?? 0
		self.rValue = rValue ?? 0
		self.gValue = gValue ?? 0
		self.bValue = bValue ?? 0
    }

    public init()
    {
        self.title = " "
        self.subTitle = " "
        self.image  = " "
        self.desc = " "
        self.price = " "
        self.catalogID = " "
        self.labelID = 0
        self.typeID = 0
        self.manufacturerID = 0

		self.mask_url = " "
		self.isPaintable = 0
		self.rValue = 0.0
		self.gValue =  0.0
		self.bValue = 0.0
    }

    enum CodingKeys: String, CodingKey
    {
        case title = "name"
        case subTitle = "brand"
        case image = "url_original"
        case desc = "descrip"
        case price = "price"
        case catalogID = "catalogID"
        case labelID = "labelID"
        case typeID = "typeID"
        case manufacturerID = "manufacturerID"

		case mask_url = "catalogmask_url"
		case isPaintable = "is_paintable"
		case rValue = "catalog_rvalue"
		case gValue = "catalog_gvalue"
		case bValue = "catalog_bvalue"
    }
}

//------------------------------------------

public class AgentList: NSObject
{
    var agentID: String
    var orderNumber: String
    var firstname: String
    var lastname: String
    var email: String
    var streetname1:String
    var streetname2:String
    var city:String
    var state:String
    var zipcode:Int
    var image_url:String
    var capture:String
    var feed:String

    init(_ dictionary: NSDictionary)
    {
        self.agentID = dictionary["agentID"] as? String ?? ""
        self.orderNumber = dictionary["orderNumber"] as? String ?? ""
        self.firstname = dictionary["firstname"] as? String ?? ""
        self.lastname = dictionary["lastname"] as? String ?? ""
        self.email = dictionary["email"] as? String ?? ""
        self.streetname1 = dictionary["streetname1"] as? String ?? ""
        self.streetname2 = dictionary["streetname2"] as? String ?? ""
        self.city = dictionary["city"] as? String ?? ""
        self.state = dictionary["state"] as? String ?? ""
        self.zipcode = dictionary["zipcode"] as? Int ?? 0
        self.image_url = dictionary["image_url"] as? String ?? ""
         let feedBool = dictionary["feed"] as? Bool ?? false
         self.feed = feedBool ? "1" : "0"

        let dateTemp = dictionary["capture"] as? String ?? ""
    
        let dateArray = dateTemp.components(separatedBy:" ")
        if dateArray.count > 0
        {
            self.capture = dateArray[0]
        }else
        {
            self.capture = ""
        }
        

       }
}
//------------------------------------------
public class DesignList: NSObject
{

//print("Design List")

    var sceneID: String
    var repImageID: String
    var sceneReplaceImageUrl: String
    var catalog_list: [CatelogList]

	//------------------------------------------
    init(_ dictionary: NSDictionary)
    {
        self.sceneID = dictionary["scene_id"] as! String
        self.repImageID = dictionary["image_url"] as! String
        self.sceneReplaceImageUrl = dictionary["image_url"] as! String
        self.catalog_list = [CatelogList]()
        
        let catArray:NSArray = dictionary["catalog_list"] as! NSArray
      
        for catDic in catArray
        {
            // filter duplicates in catelog
            let catlist =  CatelogList( catDic as! NSDictionary)
            let exists =  self.catalog_list.contains
            { element in
                return element.catlogID == catlist.catlogID
            }

            if !exists
			{
            	self.catalog_list.append(catlist)
            }
        }
       }
}

//------------------------------------------
public class CatelogList: NSObject
{
    var catlogID: String
    var img_url: String
    var image: UIImage
    var name: String
    var price: String
    var descriptions: String
    var checkout: Bool
    var favoriteID: String
    var type:String
    var file:String
    var filename:String
    var manufacturerID:Int

    var mask_url: String?
    var isPaintable: Int?
    var rValue: CGFloat?
    var gValue: CGFloat?
    var bValue: CGFloat?

    //------------------------------------------
    init(_ dictionary: NSDictionary)
    {
        self.catlogID = dictionary["catalogID"] as? String ?? ""
        self.img_url = dictionary["img_url"] as? String ?? ""
        self.image = dictionary["image"] as? UIImage ?? UIImage.init()
        self.name = dictionary["name"] as? String ?? ""
        self.price = dictionary["price"] as? String ?? ""
        self.descriptions = dictionary["description"] as? String ?? ""
        self.checkout = dictionary["checkout"] as? Bool ?? false
        self.favoriteID = dictionary["favoriteID"] as? String ?? ""
        self.type = dictionary["type"] as? String ?? ""
        self.file = dictionary["fileURL"] as? String ?? ""
        self.filename = dictionary["name"] as? String ?? ""
        self.manufacturerID = dictionary["mID"] as? Int ?? 0

		self.mask_url = dictionary["mask_url"] as? String ?? ""
		self.isPaintable = dictionary["isPaintable"] as? Int ?? 0
		self.rValue = dictionary["rValue"] as? CGFloat ?? 0.0
		self.gValue = dictionary["gValue"] as? CGFloat ?? 0.0
		self.bValue = dictionary["bValue"] as? CGFloat ?? 0.0

        super.init()
    }

    //------------------------------------------
    init(image: UIImage, name:String, descriptions:String, catlogID:String, price:String,
    	checkout:Bool,fileURL:String,fileName:String,type:String,mid:Int,
    	mask_url:String?,isPaintable:Int?,rValue:CGFloat?,gValue:CGFloat?,bValue:CGFloat?)
    {
        self.catlogID = catlogID
        self.img_url =  " "
        self.image = image
        self.name =  name
        self.price =  price
        self.descriptions =  descriptions
        self.checkout = checkout
        self.favoriteID =  " "
        self.type =  type
        self.file = fileURL
        self.filename = fileName
        self.manufacturerID = mid

		self.mask_url = mask_url ?? " "
		self.isPaintable = isPaintable ?? 0
		self.rValue = rValue ?? 0
		self.gValue = gValue ?? 0
		self.bValue = bValue ?? 0

        super.init()
    }

    //------------------------------------------
    override init()
    {
        self.catlogID = " "
        self.img_url =  " "
        self.image = UIImage.init()
        self.name =  " "
        self.price =  " "
        self.descriptions =  " "
        self.checkout = false
        self.favoriteID =  " "
        self.type =  ""
        self.file = " "
        self.filename = " "
        self.manufacturerID = 0

		self.mask_url = " "
		self.isPaintable = 0
		self.rValue = 0
		self.gValue = 0
		self.bValue = 0

        super.init()
    }
}

//------------------------------------------
public class CheckoutCatelogList: NSObject
{
    var catlogID: String
    var img_url: String
    var name: String
    var price: String
    var descriptions: String
    var checkout: Bool
    var favoriteID: String

    var mask_url: String?
    var isPaintable: Int?
    var rValue: CGFloat?
    var gValue: CGFloat?
    var bValue: CGFloat?

    init(_ dictionary: NSDictionary)
    {
        self.catlogID = dictionary["catalogID"] as! String
        self.img_url = dictionary["url_original"] as! String
        self.name = dictionary["name"] as! String
        self.price = dictionary["price"] as! String
        self.descriptions = dictionary["descrip"] as! String
        self.checkout = dictionary["checkout"] as! Bool
        self.favoriteID = dictionary["favoriteID"] as! String

        self.mask_url = dictionary["catalogmask_url"] as? String ?? " "
        self.isPaintable = dictionary["is_paintable"] as? Int ?? 0
        self.rValue = dictionary["catalog_rvalue"] as? CGFloat ?? 0
        self.gValue = dictionary["catalog_gvalue"] as? CGFloat ?? 0
        self.bValue = dictionary["catalog_bvalue"] as? CGFloat ?? 0
    }
}

//------------------------------------------
public class ProposalCatalogList: NSObject
{
    var catlogID: String
    var img_url: String
    var homeimage_url: String
    var name: String
    var price: String
    var descriptions: String
    var checkout: Bool
    var favoriteID: String

    var mask_url: String?
    var isPaintable: Int?
    var rValue: CGFloat?
    var gValue: CGFloat?
    var bValue: CGFloat?

    init(_ dictionary: NSDictionary)
    {
        self.catlogID = dictionary["catalogID"] as! String
        self.img_url = dictionary["url_original"] as! String
        self.name = dictionary["name"] as! String
        self.price = dictionary["price"] as! String
        self.descriptions = dictionary["descrip"] as! String
        self.checkout = dictionary["checkout"] as! Bool
        self.favoriteID = dictionary["favoriteID"] as! String
        self.homeimage_url = dictionary["image_url"] as! String

        self.mask_url = dictionary["catalogmask_url"] as? String ?? " "
        self.isPaintable = dictionary["is_paintable"] as? Int ?? 0
        self.rValue = dictionary["catalog_rvalue"] as? CGFloat ?? 0
        self.gValue = dictionary["catalog_gvalue"] as? CGFloat ?? 0
        self.bValue = dictionary["catalog_bvalue"] as? CGFloat ?? 0
    }
}
