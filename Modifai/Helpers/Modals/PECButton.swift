import Foundation
import UIKit
import PECimage

var dragSquare:Bool = false;
var dragSingle:Bool = false;
var outOfRange:Bool = false;
var startTimediff:TimeInterval = 1.0

//--------------------------------------------------------
extension PECButton
{

	// MARK:  - gesture actions
	//--------------------------------------------------------------------------
	@IBAction func lockCorner(_ recognizer: UITapGestureRecognizer)
	{
//print("  ")
//print("  ")
//print("PEC Button lock Corner dbl Tap Action")
		switch (recognizer.state)
		{
			case UIGestureRecognizer.State.began:
			break;

			case UIGestureRecognizer.State.changed:
			break;

			case UIGestureRecognizer.State.ended:

		   break;

			case UIGestureRecognizer.State.cancelled:
			break;

			case UIGestureRecognizer.State.failed:
			break;

			   case UIGestureRecognizer.State.possible:
			break;

			default:
			break;
		}
	}

	//----------------------------------------------------------------------------------------------------------
	@IBAction func deleteCorner(_ recognizer: UILongPressGestureRecognizer)
	{
		switch (recognizer.state)
		{
			case UIGestureRecognizer.State.began:

				  if (self.isForPaint)
				{
					if let paintCont = currentPaintImageController
					{
						if let frstButton = paintCont.theButtonV0
						{
							let ppft:Ppoint = frstButton.ppt;
							var kount:Int = 1;

							if var curPt = ppft.nx
							{
								while curPt != ppft
								{
									kount += 1;
									curPt = curPt.nx;
								}
							}
//print("kount:",kount)

							if (kount < 4) {return;}

							let ltp:Ppoint! = self.ppt.lt;
							let nxp:Ppoint! = self.ppt.nx;

							if (self.tag == 10)
							{
								if self == frstButton
								{
									if let button = nxp.button
									{
										paintCont.theButtonV0 = button;
									}
								}

								ltp.nx = nxp;
								nxp.lt = ltp;

								paintCont.scrollView.buttonV0 =  paintCont.theButtonV0?.ppt.lt.button;

								self.removeFromSuperview();
								paintCont.drawPaintLineAt()
							} else {

								if let dbutnx = self.delButtonNx
								{
									if let dbutlt = self.delButtonLt
									{
										let ltltp:Ppoint! = ltp.lt;

										ltltp.button.delButtonNx = dbutlt
										ltp.button.delButtonLt = dbutlt
										ltp.button.delButtonNx = dbutnx
									} else {
										ltp.button.delButtonNx = dbutnx
									}
								} else if let dbutlt = self.delButtonLt {
									nxp.button.delButtonLt = dbutlt
								}

								if self == frstButton
								{
									if let button = ltp.button
									{
										if let lockButton = self.lockLinkFirst
										{
											button.lockLinkFirst = lockButton;
											lockButton.lockLinkFirst = button;
										}
										paintCont.theButtonV0 = button;
									}
								}


								if let sizeButton = self.showhideLt
								{
									ltp.button.showhideLt = sizeButton;
									sizeButton.showhideLt = ltp.button;
								}

								ltp.nx = nxp;
								nxp.lt = ltp;

								self.removeFromSuperview();
								paintCont.drawPaintLineAt()
							}
						}
					}

				} else {

					currentMainImageController.polyPath.removeAllPoints()
					polyLayer.path = nil;
					shouldSave = true;  //********** change 0.0.40 add ***

					if (self.respItem.wasPoly)
					{
						self.respItem.wasPoly = false;
						self.respItem.wasArch = true;

					} else if (self.respItem.wasArch) {
						self.respItem.wasArch = false;
					}

					let ltp:Ppoint! = self.ppt.lt;
					let nxp:Ppoint! = self.ppt.nx;

					ltp.nx = nxp;
					nxp.lt = ltp;

					self.removeFromSuperview();

					if let mainCont = currentMainImageController
					{
						mainCont.drawDoorObjectAt();
					}
				}

			break;

			case UIGestureRecognizer.State.changed:
			break;

			case UIGestureRecognizer.State.ended:
			break;

			case UIGestureRecognizer.State.cancelled:
			break;

			case UIGestureRecognizer.State.failed:
			break;

			case UIGestureRecognizer.State.possible:
			break;

			default:
			break;
		}
	}

	// MARK:  - touches actions
	//----------------------------------------------------------------------------------------------------------
	override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
	{
//print("PECButton touches Began event:",event as Any);
		super.touchesBegan(touches, with:event);
		var point:CGPoint = CGPoint.zero;
//		dragSq uare = false;
		dragSingle = false;
		outOfRange = false;
		startTimediff = event?.timestamp ?? 0.0

		if (isThisForPaint)
		{
			if let curPaint = currentPaintImageController
			{
				curPaint.scrollView.delaysContentTouches = false;
				point = event?.allTouches?.first?.location(in:curPaint.scrollView.imageViewOverlay) ?? CGPoint.zero;
				firstPoint0 = point;
				lastPoint02 = self.center;

				if (curPaint.scrollView.zoomScale > 0.0)
				{
					self.deleteCornerRecog.allowableMovement = min(max(1.5*(1.0/curPaint.scrollView.zoomScale),0.5),2.0);  //5.0
				}

//print("PECButton touches Began curPaint.scrollView.zoomScale:",curPaint.scrollView.zoomScale as Any);
//print("PECButton touches Began self.deleteCornerRecog.allowableMovement:",self.deleteCornerRecog.allowableMovement);
			}

		} else {
			if let mainCont = currentMainImageController
			{
				editedGarage = true
		//		mainCont.fullsize = false;
		//		mainCont.scrollView.delaysContentTouches = false;
				point = event?.allTouches?.first?.location(in:mainCont.scrollView.imageViewOverlay) ?? CGPoint.zero

//				if ((self.respItem.classIDS.count > 0) && (self.respItem.classIDS[0] == -99))
//				{
////print("was -99")
//			//		dragSq uare = true;
//					dragSingle = true;
//					mainCont.reDrawBaseRef()
//				} else {
////print("was -88")
					dragSingle = true;
					self.respItem.classIDS[0] = -88
					mainCont.reDrawBaseRef()
//				}
			}
		}

		self.touchOffset = CGPoint(x:point.x - self.center.x, y:point.y - self.center.y);
		if (!self.isForPaint)
		{
			currentMainImageController.undoPECButton = self
			currentMainImageController.undoPoint = point
		}
	}

	//----------------------------------------------------------------------------------------------------------
	override public func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		super.touchesMoved(touches, with:event);
		if (outOfRange) { return; }

		var point:CGPoint = CGPoint.zero;
		var pointN:CGPoint = CGPoint.zero;


		if (self.isForPaint)
		{
			 if let paintCont = currentPaintImageController
			{
				var topUpLeft:CGPoint = CGPoint.zero;
				var botLwRght:CGPoint = CGPoint.zero;
				var lockButton0:PECButton?

				if let fstButton = paintCont.theButtonV0
				{
					if let lockButton = fstButton.lockLinkFirst
					{
						lockButton0 = lockButton;
						topUpLeft = lockButton.topLeft
						botLwRght = lockButton.botRght
					}
				}
				point = event?.allTouches?.first?.location(in:paintCont.scrollView.imageViewOverlay) ?? CGPoint.zero;
				pointN = CGPoint(x:point.x - self.touchOffset.x, y:point.y - self.touchOffset.y);

				var needsEscape = false;
				var moveBack:CGPoint = CGPoint.zero;

				if (pointN.x < 0.0) {needsEscape = true; moveBack.x = 20.0-pointN.x;}
				if (pointN.y < 0.0) {needsEscape = true; moveBack.y = 20.0-pointN.y;}
				if (pointN.x > paintCont.scrollView.imageViewOverlay.frame.size.width)  {needsEscape = true; moveBack.x = -20.0 - (pointN.x-paintCont.scrollView.imageViewOverlay.frame.size.width);}
				if (pointN.y > paintCont.scrollView.imageViewOverlay.frame.size.height) {needsEscape = true; moveBack.y = -20.0 - (pointN.y-paintCont.scrollView.imageViewOverlay.frame.size.height);}

				if (topUpLeft.x < 0.0) {needsEscape = true; moveBack.x = 20.0-topUpLeft.x;}
				if (topUpLeft.y < 0.0) {needsEscape = true; moveBack.y = 20.0-topUpLeft.y;}
				if (botLwRght.x > paintCont.scrollView.imageViewOverlay.frame.size.width)  {needsEscape = true; moveBack.x = -20.0 - (botLwRght.x-paintCont.scrollView.imageViewOverlay.frame.size.width);}
				if (botLwRght.y > paintCont.scrollView.imageViewOverlay.frame.size.height) {needsEscape = true; moveBack.y = -20.0 - (botLwRght.y-paintCont.scrollView.imageViewOverlay.frame.size.height);}

				if (needsEscape)
				{
					self.center.x += moveBack.x;
					self.center.y += moveBack.y;
					self.ppt.pt = self.center;

					if (self.isLock)
					{
						if let ppft = self.lockLinkFirst?.ppt
						{
							ppft.pt.x += moveBack.x;
							ppft.pt.y += moveBack.y;
							ppft.button.center = ppft.pt;

							self.topLeft.x += moveBack.x;
							self.topLeft.y += moveBack.y;
							self.botRght.x += moveBack.x;
							self.botRght.y += moveBack.y;

							if var curPt = ppft.nx
							{
								while curPt != ppft
								{
									curPt.pt.x += moveBack.x;
									curPt.pt.y += moveBack.y;
									curPt.button.center = curPt.pt;

									curPt = curPt.nx;
								}
							}
						}
					}

					if (self.isForSizing)
					{
						if let ppft = self.showhideLt?.ppt
						{
							ppft.pt.x += moveBack.x;
							ppft.pt.y += moveBack.y;
							ppft.button.center = ppft.pt;

							if let lockButton = lockButton0
							{
								lockButton.topLeft.x += moveBack.x;
								lockButton.topLeft.y += moveBack.y;
								lockButton.botRght.x += moveBack.x;
								lockButton.botRght.y += moveBack.y;
							}

							if var curPt = ppft.nx
							{
								while curPt != ppft
								{
									curPt.pt.x += moveBack.x;
									curPt.pt.y += moveBack.y;
									curPt.button.center = curPt.pt;

									curPt = curPt.nx;
								}
							}
						}
					}

					paintCont.drawPaintLineAt()
					outOfRange = true;
					return;
				}
			}
		} else {
			if let mainCont = currentMainImageController
			{
				var topUpLeft:CGPoint = CGPoint.zero;
				var botLwRght:CGPoint = CGPoint.zero;
				var lockButton0:PECButton?

				if let lockButton =  self.respItem.p1.button.lockLinkFirst
				{
//print("move not sizing got lockButton:",lockButton)
					lockButton0 = lockButton;
					topUpLeft = lockButton.topLeft
					botLwRght = lockButton.botRght
				}

				point = event?.allTouches?.first?.location(in:mainCont.scrollView.imageViewOverlay) ?? CGPoint.zero;
				pointN = CGPoint(x:point.x - self.touchOffset.x, y:point.y - self.touchOffset.y);

				var needsEscape = false;
				var moveBack:CGPoint = CGPoint.zero;

				if (pointN.x < 0.0) {needsEscape = true; moveBack.x = 20.0-pointN.x;}
				if (pointN.y < 0.0) {needsEscape = true; moveBack.y = 20.0-pointN.y;}
				if (pointN.x > mainCont.scrollView.imageViewOverlay.frame.size.width)  {needsEscape = true; moveBack.x = -20.0 - (pointN.x-mainCont.scrollView.imageViewOverlay.frame.size.width);}
				if (pointN.y > mainCont.scrollView.imageViewOverlay.frame.size.height) {needsEscape = true; moveBack.y = -20.0 - (pointN.y-mainCont.scrollView.imageViewOverlay.frame.size.height);}

				if (topUpLeft.x < 0.0) {needsEscape = true; moveBack.x = 20.0-topUpLeft.x;}
				if (topUpLeft.y < 0.0) {needsEscape = true; moveBack.y = 20.0-topUpLeft.y;}
				if (botLwRght.x > mainCont.scrollView.imageViewOverlay.frame.size.width)  {needsEscape = true; moveBack.x = -20.0 - (botLwRght.x-mainCont.scrollView.imageViewOverlay.frame.size.width);}
				if (botLwRght.y > mainCont.scrollView.imageViewOverlay.frame.size.height) {needsEscape = true; moveBack.y = -20.0 - (botLwRght.y-mainCont.scrollView.imageViewOverlay.frame.size.height);}

				if (needsEscape)
				{
					self.center.x += moveBack.x;
					self.center.y += moveBack.y;
					self.ppt.pt = self.center;

					if (self.isLock)
					{
						if let ppft = self.lockLinkFirst?.ppt
						{
							ppft.pt.x += moveBack.x;
							ppft.pt.y += moveBack.y;
							ppft.button.center = ppft.pt;

							self.topLeft.x += moveBack.x;
							self.topLeft.y += moveBack.y;
							self.botRght.x += moveBack.x;
							self.botRght.y += moveBack.y;

							if var curPt = ppft.nx
							{
								while curPt != ppft
								{
									curPt.pt.x += moveBack.x;
									curPt.pt.y += moveBack.y;
									curPt.button.center = curPt.pt;

									curPt = curPt.nx;
								}
							}
						}
					}

					if (self.isForSizing)
					{
						if let ppft = self.showhideLt?.ppt
						{
							ppft.pt.x += moveBack.x;
							ppft.pt.y += moveBack.y;
							ppft.button.center = ppft.pt;

							if let lockButton = lockButton0
							{
								lockButton.topLeft.x += moveBack.x;
								lockButton.topLeft.y += moveBack.y;
								lockButton.botRght.x += moveBack.x;
								lockButton.botRght.y += moveBack.y;
							}

							if var curPt = ppft.nx
							{
								while curPt != ppft
								{
									curPt.pt.x += moveBack.x;
									curPt.pt.y += moveBack.y;
									curPt.button.center = curPt.pt;

									curPt = curPt.nx;
								}
							}
						}
					}

					mainCont.drawDoorObjectAt()
					outOfRange = true;
					return;
				}
			}
		}

		if (self.isLock)
		{
			let oldCenter:CGPoint = self.center;
			self.center = pointN;
			self.ppt.pt = pointN;

			let moveX = self.center.x - oldCenter.x;
			let moveY = self.center.y - oldCenter.y;

			if let ppft = self.lockLinkFirst?.ppt
			{
				ppft.pt.x += moveX;
				ppft.pt.y += moveY;
				ppft.button.center = ppft.pt;

				if var curPt = ppft.nx
				{
					while curPt != ppft
					{
						curPt.pt.x += moveX;
						curPt.pt.y += moveY;
						curPt.button.center = curPt.pt;

						curPt = curPt.nx;
					}
				}
			}
		} else if (self.isForSizing) {
			if (outOfRange)
			{
				return;
			}

			var topUpLeft:CGPoint = CGPoint.zero;

			if (self.isForPaint)
			{
				if let paintCont = currentPaintImageController
				{
					if let fstButton = paintCont.theButtonV0
					{
						if let lockButton = fstButton.lockLinkFirst
						{
							topUpLeft = lockButton.topLeft
						}
					}
				}
			} else {
				if let lockButton =  self.respItem.p1.button.lockLinkFirst
				{
					topUpLeft = lockButton.topLeft
				}
			}

			let totDiffx0 = pointN.x - topUpLeft.x;
			let totDiffy0 = pointN.y - topUpLeft.y;

			var moveX:CGFloat = 0.0;
			var moveY:CGFloat = 0.0;
			let oldCenter:CGPoint = self.center;
			self.center = pointN;
			self.ppt.pt = pointN;

			if (totDiffx0 < sufaceButtonRadius*0.60)  //1.70
			{
				self.center.x = oldCenter.x
			} else {
				moveX = self.center.x - oldCenter.x;
			}

			if (totDiffy0 < sufaceButtonRadius*0.60) //1.70
			{
				self.center.y = oldCenter.y
			} else {
				moveY = self.center.y - oldCenter.y;
			}

			self.ppt.pt = self.center;

			if let ppft = self.showhideLt?.ppt
			{

				let totDiffx = (ppft.pt.x + moveX) - topUpLeft.x;
				let totDiffy = (ppft.pt.y + moveY) - topUpLeft.y;
//print("  ")
//print(" totDiffx,totDiffy",totDiffx,totDiffy)

				if ((totDiffx < CGFloat.ulpOfOne) || (totDiffy < CGFloat.ulpOfOne))
				{
					self.center = oldCenter;
					self.ppt.pt = self.center;
					outOfRange = true;
					return;
				}

				ppft.pt.x += moveX;
				ppft.pt.y += moveY;
				ppft.button.center = ppft.pt;

				if var curPt = ppft.nx
				{
					while curPt != ppft
					{
						let diffx = curPt.pt.x - topUpLeft.x;
						let diffy = curPt.pt.y - topUpLeft.y;
						let ratiox = diffx / totDiffx;
						let ratioy = diffy / totDiffy;

						curPt.pt.x += moveX * ratiox;
						curPt.pt.y += moveY * ratioy;
						curPt.button.center = curPt.pt;

						curPt = curPt.nx;
					}
				}
			}
		} else {

			if ( !self.isForPaint)
			{
				let spacingWidth:CGFloat = 30.0 * min(theImageScale*1.15,1.0);
				let smallWidth:CGFloat = 5.0 * min(theImageScale*1.15,1.0);

			//********** change 0.0.40 add if blocks ***
				if let nxPt:Ppoint = self.ppt.nx
				{
					if let ltPt:Ppoint = self.ppt.lt
					{
						if (self.topLPt)
						{
							let nxTp:Ppoint! = self.ppt.lt.lt.lt;
							if (pointN.y > ltPt.pt.y - spacingWidth) {return;}
							if (pointN.x > nxTp.pt.x - spacingWidth) {return;}
							if (pointN.x > nxPt.pt.x - smallWidth)   {return;}
						} else if (self.botLPt) {
							if (pointN.y < nxPt.pt.y + spacingWidth) {return;}
							if (pointN.x > ltPt.pt.x - spacingWidth) {return;}
						} else if (self.botRPt) {
							if (pointN.y < ltPt.pt.y + spacingWidth) {return;}
							if (pointN.x < nxPt.pt.x + spacingWidth) {return;}
						} else if (self.topRPt) {
							let btPt:Ppoint! = self.ppt.nx.nx.nx;
							if (pointN.y > nxPt.pt.y - spacingWidth) {return;}
							if (pointN.x < btPt.pt.x + spacingWidth) {return;}
							if (pointN.x < ltPt.pt.x + smallWidth)   {return;}
						} else {
							let btPt:Ppoint! = self.ppt.lt.lt.lt;
							if (pointN.y > btPt.pt.y - smallWidth) {return;}
							if (pointN.x < ltPt.pt.x + smallWidth) {return;}
							if (pointN.x > nxPt.pt.x - smallWidth) {return;}
						}
					}
				}
			}

			self.center = pointN;
			self.ppt.pt = self.center;
		}

		if (self.isForPaint)
		{
			if let paintCont = currentPaintImageController
			{
				paintCont.drawPaintLineAt()
			}
		} else {
			if let mainCont = currentMainImageController
			{
				shouldSave = true; //********** change 0.0.40 add ***
				mainCont.redoPoint = point
				mainCont.drawDoorObjectAt()
			}
		}
	}

	//----------------------------------------------------------------------------------------------------------
	override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		super.touchesEnded(touches, with:event);
//print("PECButton touches Ended event:",event as Any);

		var timediff:Double = 0.0;

		if let timed = event?.timestamp
		{
			timediff = timed - startTimediff
//print("PECButton touches timediff:",timediff as Any);
		}

		if (self.isForPaint)
		{
		   if let curPaint = currentPaintImageController
		   {
				curPaint.scrollView.delaysContentTouches = true;

				let point = event?.allTouches?.first?.location(in:curPaint.scrollView.imageViewOverlay) ?? CGPoint.zero;
//print("touchesEnded firstPoint0,point:",firstPoint0,point)
				if ((closeToXY(firstPoint0, point, 2.0)) && (timediff < 0.35))
				{
//print("touchesEnded firstPoint0,point:",firstPoint0,point)
					self.center = lastPoint02;
					self.ppt.pt = lastPoint02;

					let theButtonV1:PECButton = curPaint.cornerButton.pecPaintDuplicate()!
					theButtonV1.center = point;
					curPaint.scrollView.imageViewOverlay.addSubview(theButtonV1)
					if ( !dragSquare) {theButtonV1.tag = 10;}
					theButtonV1.isForPaint = true;
					theButtonV1.ppt = Ppoint.init(pt:theButtonV1.center);
					theButtonV1.ppt.button = theButtonV1;
					theButtonV1.touchOffset = CGPoint.zero;


					var pt0:Ppoint?;
				//	self.wasInterXPoint = false
					var lastWasLarger:Bool = false;

					var sizeButton:PECButton?
					var lockButton:PECButton?
					var cutButton:UIButton?
					var fillButton:UIButton?

					if let frstButton = curPaint.theButtonV0
					{
						let ppft:Ppoint = frstButton.ppt;
						var bestDist:CGFloat = 999999.0;

						if (dragSquare)
						{
							if let abutton = frstButton.showhideLt
							{
								sizeButton = abutton;
							}

							if let abutton = frstButton.lockLinkFirst
							{
								lockButton = abutton;
							}

							if let abutton = frstButton.delButtonNx
							{
								fillButton = abutton;
							}
						}

						if var curPt = ppft.nx
						{
							while curPt != ppft
							{
								let pt1:CGPoint = curPt.lt.pt;
								let pt2:CGPoint = curPt.pt;

								let dist0:CGFloat = ccpDistance(pt1,pt2)
								let dist1:CGFloat = ccpDistance(pt1,point)
								let dist2:CGFloat = ccpDistance(pt2,point)

								if let abutton = curPt.button.showhideLt
								{
									sizeButton = abutton;
								}

								if let abutton = curPt.button.delButtonNx
								{
									if (fillButton != nil)
									{
										cutButton = abutton;
									} else {
										fillButton = abutton;
									}
								}

								let distT:CGFloat = dist1+dist2;

								if (distT < dist0*1.01) && (distT < bestDist)
								{
									bestDist = distT;
									pt0 = curPt.lt;
									lastWasLarger = false;
									if (dist1 > dist2) {lastWasLarger = true;}
								}

								curPt = curPt.nx;
							}

							if (dragSquare)
							{
								let pt1:CGPoint = curPt.lt.pt;
								let pt2:CGPoint = curPt.pt;

								let dist0:CGFloat = ccpDistance(pt1,pt2)
								let dist1:CGFloat = ccpDistance(pt1,point)
								let dist2:CGFloat = ccpDistance(pt2,point)

								let distT:CGFloat = dist1+dist2;

								if (distT < dist0*1.01) && (distT < bestDist)
								{
									bestDist = distT;
									pt0 = curPt.lt;
									curPaint.scrollView.buttonV0 = theButtonV1
									lastWasLarger = false;
									if (dist1 > dist2) {lastWasLarger = true;}
								}
							}
						}
					}

					if let pt00 = pt0
					{
//print(" was intersection ");
						let nxp:Ppoint! = pt00.nx;

						pt00.nx = theButtonV1.ppt;
						theButtonV1.ppt.lt = pt00;
						theButtonV1.ppt.nx = nxp;
						nxp.lt = theButtonV1.ppt

						if (dragSquare)
						{
							if let abutton = sizeButton
							{
//print("was a sizeButton",sizeButton as Any)
								//abutton.bringSubviewToFront(self.imageViewOverlay)	//bringSubviewToFront not working? Apple swift bug?
								curPaint.scrollView.imageViewOverlay.addSubview(abutton)
							}
							if let abutton = lockButton
							{
								curPaint.scrollView.imageViewOverlay.addSubview(abutton)
							}
							if let abutton = cutButton
							{
						//		abutton.bringSubviewToFront(self.imageViewOverlay)
								curPaint.scrollView.imageViewOverlay.addSubview(abutton)
							}
							if let abutton = fillButton
							{
						//		abutton.bringSubviewToFront(self.imageViewOverlay)
								curPaint.scrollView.imageViewOverlay.addSubview(abutton)
							}

							if (lastWasLarger)
							{
								theButtonV1.delButtonLt = nxp.button.delButtonLt;
								theButtonV1.delButtonLt?.center = theButtonV1.findMidPtBut2D(theButtonV1.ppt.pt,theButtonV1.ppt.lt.pt);
								nxp.button.delButtonLt = nil;
							} else {
								theButtonV1.delButtonNx = pt00.button.delButtonNx;
								theButtonV1.delButtonNx?.center = theButtonV1.findMidPtBut2D(theButtonV1.ppt.pt,theButtonV1.ppt.nx.pt);
								pt00.button.delButtonNx = nil;
							}
						}

//						currentPaintImageController.scrollView.wasInterXPoint = true;
//						currentPaintImageController.scrollView.interXButton = theButtonV1;

					} else {
						if (dragSquare)
						{
							theButtonV1.removeFromSuperview()
						} else {
							curPaint.scrollView.buttonV0?.ppt.nx = theButtonV1.ppt;
							theButtonV1.ppt.lt = currentPaintImageController.scrollView.buttonV0?.ppt;
							theButtonV1.ppt.nx = currentPaintImageController.theButtonV0?.ppt;
							curPaint.theButtonV0?.ppt.lt = theButtonV1.ppt

							curPaint.scrollView.buttonV0 = theButtonV1

							curPaint.scrollView.editDimButton.center   = CGPoint(x: point.x + (maxDistance * 1.75),
																				 y: point.y - (maxDistance * 1.75))
						}
					}

					curPaint.drawPaintLineAt()
					curPaint.scrollView.drawPaintCorners();
				}
		   }
		} else {
		   if let mainCont = currentMainImageController
		   {
				mainCont.scrollView.delaysContentTouches = true;
		//		   mainCont.fullsize = true;
//print("touchesEnded dragSq uare:",dragSqu are)
//print("touchesEnded self.respItem.clas sIDS[0:",self.respItem.clas sIDS[0])
		  //		 dragSq uare = false;
				if (dragSingle) {self.respItem.classIDS[0] = 5;}
				dragSingle = false;
				mainCont.drawDoorObjectAt()
		   }
		}
	}

	//----------------------------------------------------------------------------------------------------------
	override public func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?)
	{
//print("touches Cancelled")
		super.touchesCancelled(touches, with:event);
	}


	// MARK: - for undo
	//----------------------------------------------------------------------------------------------------------
	func changePrevPosition(point:CGPoint)
	{
		let pointN:CGPoint = CGPoint(x:point.x - self.touchOffset.x, y:point.y - self.touchOffset.y);
		let oldCenter:CGPoint = self.center;

		if let mainEditDr = currentMainImageController
		{
			//----------- button was center point ----------  had been crashing for Lakshmi's undo
			if (self.isLock)
			{
				let moveX = pointN.x - oldCenter.x;
				let moveY = pointN.y - oldCenter.y;

				var moveBack:CGPoint = CGPoint.zero;
				moveBack.x = moveX;
				moveBack.y = moveY;

				if let ppft = self.lockLinkFirst?.ppt
				{
					ppft.pt.x += moveBack.x;
					ppft.pt.y += moveBack.y;
					ppft.button.center = ppft.pt;

					self.topLeft.x += moveBack.x;
					self.topLeft.y += moveBack.y;
					self.botRght.x += moveBack.x;
					self.botRght.y += moveBack.y;

					if var curPt = ppft.nx
					{
						while curPt != ppft
						{
							curPt.pt.x += moveBack.x;
							curPt.pt.y += moveBack.y;
							curPt.button.center = curPt.pt;

							curPt = curPt.nx;
						}
					}
				}

				mainEditDr.drawDoorObjectAt()
				return;
			}

			//----------- button was sizer ----------  had been crashing for Lakshmi's undo  this doesn't return the state but it doesn't crash now.
			if (self.isForSizing)
			{
				var topUpLeft:CGPoint = CGPoint.zero;

				if let lockButton =  self.respItem.p1.button.lockLinkFirst
				{
					topUpLeft = lockButton.topLeft
				}

				let moveX:CGFloat = pointN.x - oldCenter.x;
				let moveY:CGFloat = pointN.y - oldCenter.y;

				self.center = pointN;
				self.ppt.pt = pointN;

				self.ppt.pt = self.center;

				if let ppft = self.showhideLt?.ppt
				{
					let totDiffx = (ppft.pt.x + 0.0) - topUpLeft.x;
					let totDiffy = (ppft.pt.y + 0.0) - topUpLeft.y;
//print("  ")
//print(" totDiffx,totDiffy",totDiffx,totDiffy)


					if ((totDiffx < CGFloat.ulpOfOne) || (totDiffy < CGFloat.ulpOfOne))
					{
						self.center = oldCenter;
						self.ppt.pt = self.center;
						outOfRange = true;
						return;
					}

					ppft.pt.x += moveX;
					ppft.pt.y += moveY;
					ppft.button.center = ppft.pt;

					if var curPt = ppft.nx
					{
						while curPt != ppft
						{
							let diffx = curPt.pt.x - topUpLeft.x;
							let diffy = curPt.pt.y - topUpLeft.y;
							let ratiox = diffx / totDiffx;
							let ratioy = diffy / totDiffy;

							curPt.pt.x += moveX * ratiox;
							curPt.pt.y += moveY * ratioy;
							curPt.button.center = curPt.pt;

							curPt = curPt.nx;
						}
					}
				}

				mainEditDr.drawDoorObjectAt()

				return;
			}


            self.center = pointN;
            self.ppt.pt = self.center;

            if let nxPt:Ppoint = self.ppt.nx  //********** change 3.0.4 *** change to if block
			{
				if let ltPt:Ppoint = self.ppt.lt    //********** change 3.0.4 *** change to if block
				{
					if (pointN.x < 0.0) {return;}
					if (pointN.y < 0.0) {return;}
					if (pointN.x > mainEditDr.scrollView.imageViewOverlay.frame.size.width) {return;}
					if (pointN.y > mainEditDr.scrollView.imageViewOverlay.frame.size.height) {return;}

					let spacingWidth:CGFloat = 30.0 * min(theImageScale*1.15,1.0);
					let smallWidth:CGFloat = 5.0 * min(theImageScale*1.15,1.0);

					if (self.topLPt)
					{
						let nxTp:Ppoint! = self.ppt.lt.lt.lt;
						if (pointN.y > ltPt.pt.y - spacingWidth) {return;}
						if (pointN.x > nxTp.pt.x - spacingWidth) {return;}
						if (pointN.x > nxPt.pt.x - smallWidth)    {return;}
					} else if (self.botLPt) {
						if (pointN.y < nxPt.pt.y + spacingWidth) {return;}
						if (pointN.x > ltPt.pt.x - spacingWidth) {return;}
					} else if (self.botRPt) {
						if (pointN.y < ltPt.pt.y + spacingWidth) {return;}
						if (pointN.x < nxPt.pt.x + spacingWidth) {return;}
					} else if (self.topRPt) {
						let btPt:Ppoint! = self.ppt.nx.nx.nx;
						if (pointN.y > nxPt.pt.y - spacingWidth) {return;}
						if (pointN.x < btPt.pt.x + spacingWidth) {return;}
						if (pointN.x < ltPt.pt.x + smallWidth)    {return;}
					} else {
						let btPt:Ppoint! = self.ppt.lt.lt.lt;
						if (pointN.y > btPt.pt.y - smallWidth) {return;}
						if (pointN.x < ltPt.pt.x + smallWidth) {return;}
						if (pointN.x > nxPt.pt.x - smallWidth) {return;}
					}

					if (self.isSelected)
					{
						let moveX = self.center.x - oldCenter.x;
						let moveY = self.center.y - oldCenter.y;

						self.ppt.nx.pt.x += moveX;
						self.ppt.nx.pt.y += moveY;
						self.ppt.nx.button.center = self.ppt.nx.pt;

						self.ppt.nx.nx.pt.x += moveX;
						self.ppt.nx.nx.pt.y += moveY;
						self.ppt.nx.nx.button.center = self.ppt.nx.nx.pt;

						self.ppt.lt.pt.x += moveX;
						self.ppt.lt.pt.y += moveY;
						self.ppt.lt.button.center = self.ppt.lt.pt;

						if (self.respItem.wasArch)
						{
							self.ppt.lt.lt.pt.x += moveX;
							self.ppt.lt.lt.pt.y += moveY;
							self.ppt.lt.lt.button.center = self.ppt.lt.lt.pt;
						} else if (self.respItem.wasPoly) {
							self.ppt.lt.lt.pt.x += moveX;
							self.ppt.lt.lt.pt.y += moveY;
							self.ppt.lt.lt.button.center = self.ppt.lt.lt.pt;

							self.ppt.lt.lt.lt.pt.x += moveX;
							self.ppt.lt.lt.lt.pt.y += moveY;
							self.ppt.lt.lt.lt.button.center = self.ppt.lt.lt.lt.pt;
						}

						self.respItem.delButton?.center.x += moveX;
						self.respItem.delButton?.center.y += moveY;

					} else {
						if let button = self.delButtonLt
						{
							button.center = self.findMidPtBut2D(self.ppt.pt,self.ppt.lt.pt);
						} else if let button = self.delButtonNx {
							button.center = self.findMidPtBut2D(self.ppt.pt,self.ppt.nx.pt);
						}
					}
            	}
            }

			mainEditDr.drawDoorObjectAt()
		}
	}

	// MARK: - math
	//----------------------------------------------------------------------------------------------------------
	func findPerpPtButt_2D(_ pt: CGPoint, _ line:Bline) -> (wasGood:Bool, pt1:CGPoint)
	{
		var ax, bx, ay, by, above, below, td, dx, dy:CGFloat

		ax = line.epoint.x - line.spoint.x;
		bx = pt.x - line.spoint.x;

		ay = line.epoint.y - line.spoint.y;
		by = pt.y - line.spoint.y;

		above = bx * ax + by * ay;
		below = ax * ax + ay * ay;

		if (abs(below) < CGFloat.ulpOfOne) {return (false, CGPoint(x:9.9, y:9.9))}
		else {td = above / below;}

		if ((td < 0.0) || (td > 1.0))  {return (false, CGPoint(x:9.9, y:9.9))}

		dx = td * ax;
		dy = td * ay;

		return (true, CGPoint(x:line.spoint.x + dx, y:line.spoint.y + dy))
	}

	//---------------------------------------------------------------------------------------
	func findMidPtBut2D(_ spoint:CGPoint, _ epoint:CGPoint) -> CGPoint
	{
		let midpoint:CGPoint = CGPoint(x:((epoint.x - spoint.x) * 0.5) + spoint.x,
									   y:((epoint.y - spoint.y) * 0.5) + spoint.y);
		return midpoint;
	}

	//-------------------------------------------------------------------------------------------
	public func findMidPt(_ spoint:CGPoint, _ epoint:CGPoint)
	{
		let midpoint:CGPoint = CGPoint(x:((epoint.x - spoint.x) * 0.5) + spoint.x,
									   y:((epoint.y - spoint.y) * 0.5) + spoint.y);
		self.center = midpoint;
		self.ppt.pt = midpoint;
	}

	//----------------------------------------------------------------------------------------------------------
	func findDistance2Db(_ pt1:CGPoint, _ pt2:CGPoint) -> CGFloat
	{
		return sqrt(((pt2.x - pt1.x) * (pt2.x - pt1.x)) + ((pt2.y - pt1.y) * (pt2.y - pt1.y)));
	}


	//----------------------------------------------------------------------------------------------------------
	func pecDuplicate() -> PECButton?
	{
		let buttonDuplicate = PECButton.init(type:ButtonType.custom);
		buttonDuplicate.frame = self.frame;
		buttonDuplicate.setBackgroundImage(UIImage(named: "endPtbtrans.png"), for: .normal);
		buttonDuplicate.setBackgroundImage(UIImage(named: "locked.png"), for: .selected);
		buttonDuplicate.setBackgroundImage(UIImage(named: "endPtb.png"), for: .highlighted);
		buttonDuplicate.contentHorizontalAlignment = self.contentHorizontalAlignment;
		buttonDuplicate.contentVerticalAlignment =  self.contentVerticalAlignment;
		buttonDuplicate.adjustsImageWhenHighlighted =  false;
		buttonDuplicate.showsTouchWhenHighlighted =  false;
		buttonDuplicate.adjustsImageWhenDisabled =  false;
		buttonDuplicate.contentEdgeInsets = self.contentEdgeInsets;
		buttonDuplicate.isHidden = false;

		buttonDuplicate.deleteCornerRecog = UILongPressGestureRecognizer.init(target: buttonDuplicate, action: #selector(buttonDuplicate.deleteCorner(_:)));
		buttonDuplicate.deleteCornerRecog.minimumPressDuration = 1.5;
		buttonDuplicate.deleteCornerRecog.allowableMovement = 5.0;
		buttonDuplicate.deleteCornerRecog.isEnabled = false;

//		buttonDuplicate.lockCornerRecog = UITapGestureRecognizer.init(target: buttonDuplicate, action: #selector(buttonDuplicate.lockCorner(_:)));
//		buttonDuplicate.lockCornerRecog.numberOfTapsRequired = 2;
//		buttonDuplicate.lockCornerRecog.isEnabled = true;

		if let curMain = currentMainImageController
		{
			buttonDuplicate.deleteCornerRecog.delegate = curMain.scrollView;
//			buttonDuplicate.lockCornerRecog.delegate = curMain.scrollView;
		}

//		buttonDuplicate.gestureRecognizers = [buttonDuplicate.deleteCornerRecog,buttonDuplicate.lockCornerRecog];
		buttonDuplicate.gestureRecognizers = [buttonDuplicate.deleteCornerRecog];

		let eventArray:[UIControl.Event] = [UIControl.Event.touchDown,UIControl.Event.touchUpInside,UIControl.Event.touchUpOutside];

		// Copy targets and associated actions
		self.allTargets.forEach
		{ target in
			for controlEvent in eventArray
			{
				if let actions = self.actions(forTarget: target, forControlEvent: controlEvent)
				{
					for action in actions
					{
						buttonDuplicate.addTarget(target, action: Selector(action), for: controlEvent)
					}
				}
			}
		}

		return buttonDuplicate
	}

	//----------------------------------------------------------------------------------------------------------
	func pecPaintDuplicate() -> PECButton?
	{
		let buttonDuplicate = PECButton.init(type:ButtonType.custom);
		buttonDuplicate.frame = self.frame;
		buttonDuplicate.setBackgroundImage(UIImage(named: "endPtbtrans.png"), for: .normal);
		buttonDuplicate.setBackgroundImage(UIImage(named: "locked.png"), for: .selected);
		buttonDuplicate.setBackgroundImage(UIImage(named: "endPtb.png"), for: .highlighted);
		buttonDuplicate.contentHorizontalAlignment = self.contentHorizontalAlignment;
		buttonDuplicate.contentVerticalAlignment =  self.contentVerticalAlignment;
		buttonDuplicate.adjustsImageWhenHighlighted =  false;
		buttonDuplicate.showsTouchWhenHighlighted =  false;
		buttonDuplicate.adjustsImageWhenDisabled =  false;
		buttonDuplicate.contentEdgeInsets = self.contentEdgeInsets;
		buttonDuplicate.isHidden = false;

		buttonDuplicate.deleteCornerRecog = UILongPressGestureRecognizer.init(target: buttonDuplicate, action: #selector(buttonDuplicate.deleteCorner(_:)));
		buttonDuplicate.deleteCornerRecog.minimumPressDuration = 1.5;
		buttonDuplicate.deleteCornerRecog.allowableMovement = 1.0;  //5.0
		buttonDuplicate.deleteCornerRecog.isEnabled = true;

//		buttonDuplicate.lockCornerRecog = UITapGestureRecognizer.init(target: buttonDuplicate, action: #selector(buttonDuplicate.lockCorner(_:)));
//		buttonDuplicate.lockCornerRecog.numberOfTapsRequired = 2;
//		buttonDuplicate.lockCornerRecog.isEnabled = true;

		if let curPaint = currentPaintImageController
		{
			buttonDuplicate.deleteCornerRecog.delegate = curPaint.scrollView;
//			buttonDuplicate.lockCornerRecog.delegate = curPa int.scrollView;
		}

//		buttonDuplicate.gestureRecognizers = [buttonDuplicate.deleteCornerRecog,buttonDuplicate.lockCornerRecog];
		buttonDuplicate.gestureRecognizers = [buttonDuplicate.deleteCornerRecog];

		let eventArray:[UIControl.Event] = [UIControl.Event.touchDown,UIControl.Event.touchUpInside,UIControl.Event.touchUpOutside];

		// Copy targets and associated actions
		self.allTargets.forEach
		{ target in
			for controlEvent in eventArray
			{
				if let actions = self.actions(forTarget: target, forControlEvent: controlEvent)
				{
					for action in actions
					{
						buttonDuplicate.addTarget(target, action: Selector(action), for: controlEvent)
					}
				}
			}
		}

		return buttonDuplicate
	}
}



