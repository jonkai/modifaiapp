import Foundation
import UIKit

//*************** change 1.1.0 add new value 'gallIndex' through out ***
//---------------------------------------------------------------------
class ProjectThumb:NSObject, Codable, NSCoding
{
	var projectID:String? = nil;
	var thumbnailData:Data? = nil;
    var thumbnailName:String? = nil;
    var gallIndex:Int = 0;

	//---------------------------------------------------------------------
	private enum CodingKeys: String, CodingKey
	{
		case projectID
		case thumbnailData
        case thumbnailName
        case gallIndex
	}

	//---------------------------------------------------------------------
	override init()
	{
		self.projectID = UUID().uuidString
		super.init()
	}

	//---------------------------------------------------------------------
	required init?(coder decoder: NSCoder)
	{
		self.projectID = decoder.decodeObject(forKey: "projectID") as? String
		self.thumbnailData = decoder.decodeObject(forKey: "thumbnailData") as? Data
        self.thumbnailName = decoder.decodeObject(forKey: "thumbnailName") as? String
		self.gallIndex = decoder.decodeInteger(forKey: "gallIndex")
//print(" ProjectThumb  decoder self.projectID:",self.projectID as Any)

		super.init()
	}

	//---------------------------------------------------------------------
	func encode(with aCoder: NSCoder)
	{
		aCoder.encode(self.projectID, forKey: "projectID")
		aCoder.encode(self.thumbnailData, forKey: "thumbnailData")
        aCoder.encode(self.thumbnailName, forKey: "thumbnailName")
		aCoder.encode(self.gallIndex, forKey: "gallIndex")
	}
}
