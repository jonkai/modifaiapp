import UIKit

//--------------------------------------------------------------------------------------
class RoundInnerView: UIView
{
	// MARK: - touches actions
	//----------------------------------------------------------------------------------------------------------
	override func point(inside point:CGPoint, with event:UIEvent?) ->Bool
	{
		var shouldTouchStayOnThisLevel:Bool = false;
		let currentStart:CGPoint = CGPoint(x:point.x - 203.5, y:203.5 - point.y);

		let roundTouchPadLengthToCenter:CGFloat =  sqrt((currentStart.x * currentStart.x) + (currentStart.y * currentStart.y));
		if (roundTouchPadLengthToCenter < 203.5) {shouldTouchStayOnThisLevel = true;}

		return shouldTouchStayOnThisLevel;
	}
}
