//
//  ImageManager.swift
//  Modifai
//
//  Created by ReviveAI on 1/22/19.
//

import Foundation
import UIKit

var rotationalAngle: Double = 0.0

class ImageManager {
    
    ///////////////////////
    // get a path for a rois
    // Works with Rectangle (4 elements) or
    //       Polygons (6 elements)
    //
    // example: "rois": [[378, 456], [725, 456], [725, 603], [378, 603]]
    // [[x, y]]
    // if the rois contains 4 elements, it's a rectangle
    // if the rois contains 6 elements, it's a polygon
    //
    // returns a path and rect to be used to create an image cutout
    ////////////////////////
    static func getPathForRois(rois: [[Int]]) -> (path: CGPath, rect: CGRect)? {
        // return if rois.count < 4
        guard rois.count > 3 else { return  nil}
        
        // create a path from the rois array
        let path = UIBezierPath.init()
        var i = 0
        for xy in rois {
            if i == 0 {
                path.move(to: CGPoint.init(x: xy[0], y: xy[1]))
            } else {
                path.addLine(to: CGPoint.init(x: xy[0], y: xy[1]))
            }
            i = i + 1
        }
        path.close()
        
        // create a bounding rect for this path so the caller
        // can create a cutout image from it
        
        // create a core rect to return with the path
        // uses the furthest left point (x)
        // uses the highest top point (y)
        let rect = path.cgPath.boundingBox
        
        return (path.cgPath, rect)
    }
    
    ///////////////////////
    // get a path for an arched rois
    // Works with outer and inner rois
    // roisouter = arch
    // roisinner = rectangle
    //
    // example: "rois": [], "roisouter": [[1098, 874], [2075, 874]], "roisinner": [[1090, 912], [2081, 912], [2081, 1359], [1090, 1359]]
    // [[x, y]]
    // returns a path and rect to be used to create an image cutout
    ////////////////////////
    //
    static func getPathForArchRois(rois: [[Int]], archRois: [[Int]]) -> (path: CGPath, rect: CGRect)? {
        guard rois.count == 4 else { return nil }
        guard archRois.count == 2 else { return nil}
        // get a core path (inner rois)
        guard let corePathAndRect = getPathForRois(rois: rois) else { return nil }
       
        let newPath = UIBezierPath.init()
        newPath.append(UIBezierPath.init(cgPath: corePathAndRect.path))
        
        let ax = archRois[0][0]
        let ay = archRois[0][1]
        let axx = archRois[1][0]
        //let ayy = archRois[1][1]
        
        // NOTE: height is inner rect y - archRect.y * 2
        let archRect = CGRect.init(x: ax-10, y: ay,
                                   width: axx+10 - ax, height: (Int(corePathAndRect.rect.origin.y) - ay) * 2)
        let startPt = CGPoint.init(x: corePathAndRect.rect.origin.x, y: corePathAndRect.rect.origin.y)
        let ctrlPt = CGPoint.init(x: archRect.midX, y: archRect.origin.y - archRect.size.height)
        let endPt = CGPoint.init(x: corePathAndRect.rect.maxX, y: corePathAndRect.rect.origin.y)
        
        newPath.move(to: startPt)
        newPath.addQuadCurve(to: endPt, controlPoint: ctrlPt)
        newPath.close()
        
        let returnRect = newPath.cgPath.boundingBox
        return (newPath.cgPath, returnRect)
        
    }
    
    static func getPathForRois(rois: [[Int]], ratioWidth:CGFloat, ratioHeight:CGFloat) -> (path: CGPath, rect: CGRect)?
    {
        //print(" getPath ForRois ");
        guard rois.count > 3 else { return  nil}
        
        //print(" getPath ForRois ratioWidth :",ratioWidth);
        //print(" getPath ForRois ratioHeight:",ratioHeight);
        // create a path from the rois array
        let path = UIBezierPath.init()
        var i = 0
        
        for xy in rois
        {
            if i == 0
            {
                path.move(to: CGPoint.init(x: CGFloat(xy[0])*ratioWidth, y: CGFloat(xy[1])*ratioHeight))
                //         path.move(to: CGPoint.init(x: xy[0], y: xy[1]))
            } else {
                path.addLine(to: CGPoint.init(x: CGFloat(xy[0])*ratioWidth, y: CGFloat(xy[1])*ratioHeight))
                //         path.addLine(to: CGPoint.init(x: xy[0], y: xy[1]))
            }
            i = i + 1
        }
        path.close()
        let rect = path.cgPath.boundingBox
        
        //print(" getPath ForRois rect:",rect);
        //print(" getPath ForRois path:",path);
        
        return (path.cgPath, rect)
    }
    
    
    ///////////////////////
    // get a path for an arched rois
    // Works with outer and inner rois
    // roisouter = arch
    // roisinner = rectangle
    //
    // example: "rois": [], "roisouter": [[1098, 874], [2075, 874]], "roisinner": [[1090, 912], [2081, 912], [2081, 1359], [1090, 1359]]
    // [[x, y]]
    // returns a path and rect to be used to create an image cutout
    ////////////////////////
    
    //---------------------------------------------------------------------
    static func getPathForArchRois(rois: [[Int]], archRois: [[Int]], ratioWidth:CGFloat, ratioHeight:CGFloat) -> (path: CGPath, rect: CGRect)?
    {
        //print(" getPathForArchRois ");
        guard rois.count == 4 else { return nil }
        guard archRois.count == 2 else { return nil}
        
        guard let corePathAndRect = getPathForRois(rois: rois, ratioWidth:ratioWidth, ratioHeight:ratioHeight) else { return nil }
        
        let newPath = UIBezierPath.init()
        newPath.append(UIBezierPath.init(cgPath: corePathAndRect.path))
        
        let ax:CGFloat  = CGFloat(archRois[0][0])*ratioWidth;
        let ay:CGFloat  = CGFloat(archRois[0][1])*ratioHeight;
        let axx:CGFloat = CGFloat(archRois[1][0])*ratioWidth;
        
        let archRect = CGRect.init(x: ax, y: ay,
                                   width: axx - ax, height: (CGFloat(corePathAndRect.rect.origin.y) - ay) * 2)
        let startPt = CGPoint.init(x: corePathAndRect.rect.origin.x, y: corePathAndRect.rect.origin.y)
        let ctrlPt = CGPoint.init(x: archRect.midX, y: archRect.origin.y - archRect.size.height)
        let endPt = CGPoint.init(x: corePathAndRect.rect.maxX, y: corePathAndRect.rect.origin.y)
        
        newPath.move(to: startPt)
        newPath.addQuadCurve(to: endPt, controlPoint: ctrlPt)
        newPath.close()
        
        let returnRect = newPath.cgPath.boundingBox
        return (newPath.cgPath, returnRect)
    }
    static func getImageHole(path: CGPath, hostSize: CGSize) -> UIImage? {
        UIGraphicsBeginImageContext(hostSize)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        
        context.setFillColor(UIColor.red.cgColor)
        
        context.addPath(path)
        context.fillPath()
        var resultImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        resultImage = resultImage!.tinted(with: .red)

        return resultImage
    }
    
    //
    // blend hole and reference image for new image
    // this will blend any image with another image
    // specifically, it's used to blend an image mask with hole (same size as the reference image)
    // to a reference image so it masks out the garage
    //
    static func blendHoleWithRef(refImage: UIImage, hostRect: CGRect, holeImage: UIImage, alpha: CGFloat = 1.0) -> UIImage? {
        UIGraphicsBeginImageContext(CGSize.init(width: refImage.size.width, height: refImage.size.height))
        refImage.draw(in: hostRect)
        holeImage.draw(in: hostRect, blendMode: .destinationOut, alpha: alpha)
        let resultImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return resultImage
    }
    
    //
    // add an image to a reference image
    // used to replace an image with a hole in it with a garage image
    //
    static func addImageToHoleImage(refImageWithHole: UIImage, replacementImage: UIImage, holeRect: CGRect) -> UIImage? {
        // ref image size
        let rect = CGRect(x: 0, y: 0, width: refImageWithHole.size.width, height: refImageWithHole.size.height)
        let newRep = replacementImage.resizeImageWith(image: replacementImage, newSize: CGSize.init(width: holeRect.width, height: holeRect.height))
        
        UIGraphicsBeginImageContext(refImageWithHole.size)
        newRep.draw(in: holeRect, blendMode: .hardLight, alpha: 1)
        refImageWithHole.draw(in: rect, blendMode: .normal, alpha: 1)
        let resultImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return resultImage
    }
    
//    static func getGarageWithJson(withJson: String) -> HouseO bject? {
//        if let filepath = Bundle.main.path(forResource: withJson, ofType: "json") {
//            do {
//                let json = try String(contentsOfFile: filepath)
//                //print(contents)
//                let garage = try HouseO bject(json)
//                return garage
//                
//            } catch let error {
//                // contents could not be loaded
//                print("couldn't parse: \(error.localizedDescription)")
//                return nil
//            }
//        } else {
//            print("no file found")
//            return nil
//        }
//    }
    
    // uses a hous eobject from the web call
//    static func replaceObjectInReference(hous eObject: Hous eObject, referenceImage: UIImage, repGarage: UIImage,type:String) -> UIImage?
//    {
//print(" ")
//print(" ")
//print(" ")
//print("0 replaceObjectInReference")
//        var ratioWidth:CGFloat = 0.0
//        var ratioHeight:CGFloat = 0.0
//
//        if referenceImage.size.width > referenceImage.size.height {
//            ratioWidth  = referenceImage.size.width  / destIma geWidth;
//            ratioHeight = referenceImage.size.height / destIma geHeight;
//        } else {
//            ratioWidth  = referenceImage.size.width  / destIma geHeight;
//            ratioHeight = referenceImage.size.height / destIma geWidth;
//        }
////print("referenceImage.size:",referenceImage.size)
//
//        // the composite image made up of replacements
//        var newReferenceImage = referenceImage
//        let hostRect = CGRect.init(x: 0, y: 0, width: referenceImage.size.width, height: referenceImage.size.height)
////print("hostRect:",hostRect)
//        var roisPath: (path: CGPath, rect: CGRect)?
//
//        for resp in hou seObject.response
//        {
//            var index = 0
//
//            let replacement = repGarage;
//            //    let lbl = res p[0].labe ls[0]
//
//            for respI tem in resp
//            {
//                let str:String = respItem.labels.joined(separator: ",")
//                index  =  index + 1
//                var typeCompare = type
//                if type == "garage"
//                {
//
//                    typeCompare = "garage:"
//                }
//                else if type == "singleGarage"
//                {
//
//                    typeCompare = "garage:"
//                }
//
//                if str.lowercased().contains(typeCompare.lowercased()) {
//                    if respItem.roisouter.count == 0 && respItem.roisinner.count == 0
//                    {
//                        roisPath = ImageManager.getPathForRois(rois: respItem.rois, ratioWidth:ratioWidth, ratioHeight:ratioHeight)
//                    } else if respItem.rois.count == 0 {
//                        roisPath = ImageManager.getPathForArchRois(rois: respItem.roisinner, archRois: respItem.roisouter, ratioWidth:ratioWidth, ratioHeight:ratioHeight)
//                    }
//
//                    if let pr = roisPath
//                    {
//                        if let holeImage = ImageManager.getImageHole(path: pr.path, hostSize: hostRect.size)
//
//                        {
//
//                            if let blendedImage = ImageManager.blendHoleWithRef(refImage: newReferenceImage, hostRect: hostRect, holeImage: holeImage)
//                            {
//
//                                if let replacedImage = ImageManager.addImageToHoleImage(refImageWithHole: blendedImage, replacementImage: replacement, holeRect: pr.rect)
//                                {
//                                    newReferenceImage = replacedImage
//                                }
//                            }
//                        } else {
//                            return newReferenceImage
//                        }
//                    } else {
//                        return newReferenceImage
//                    }
//                }
//                else {
//  print("error with path")
//                    if index > resp.count {
//                        return newReferenceImage
//                    }
//                }
//            }
//        }
//        return newReferenceImage
//    }
        
    
       /* var newReferen ceImage = refer enceImage
        let hostRect = CGRect.init(x: 0, y: 0, width: referenceImage.size.width, height: referenceImage.size.height)
        var roisPath: (path: CGPath, rect: CGRect)?
        
        let ratioWidth  = referenceImage.size.width  / destIma geWidth;
        let ratioHeight = referenceImage.size.height / destIma geHeight;

        
        // build up new reference composite image
        // high level objects (garages, doors etc)
        for resp in hous eObject.response {
            // rois, roisinner, roisouter - coordinates of the object

            var index = 0
            /*for respI tem in resp {
// print(respItem.labe ls[0] )
                 let str:String = respItem.labels.joined(separator: ",")
                index  =  index + 1
                var typeCompare = type
                if type == "garage"
                {
                    if respItem.lab els[0] != "garage:two-car" {
                        continue
                    }
                    typeCompare = "garage:"
                }
                else if type == "singleGarage"
                {
                    if respItem.labe ls[0] != "garage:one-car" {
                        continue
                    }
                    typeCompare = "garage:"
                }
                let str = respItem.labels.joined(separator: ",")
                index  =  index + 1
                if str.lowercased().contains(typeCompare.lowercased()) {
                    if respItem.roisouter.count == 0 && respItem.roisinner.count == 0 {
                        // replace a rectangle object
                   //     //print("building rectangle")
                        roisPath = ImageManager.getPathForRois(rois: respItem.rois)
                    } else if respItem.rois.count == 0 {
                        //print("building arc/polygon")
                        roisPath = ImageManager.getPathForArchRois(rois: respItem.roisinner, archRois: respItem.roisouter)
                    }
                    if let pr = roisPath {
                        if let holeImage = ImageManager.getImageHole(path: pr.path, hostSize: hostRect.size) {
                            // load a reference image and blend with the hole
                            // this passes in the composite image we're building staring with original reference image
                            if let blendedImage = ImageManager.blendHoleWithRef(refImage: newReferenceImage, hostRect: hostRect, holeImage: holeImage) {
                                //referenceImage.image = blendedImage
                                // add a selected object image to the blendedImage
                                if let replacedImage = ImageManager.addImageToHoleImage(refImageWithHole: blendedImage, replacementImage: repGarage, holeRect: pr.rect) {
                                    newReferenceImage = replacedImage
                                }
                            }
                        } else {
                            //print("error with holeImage")
                            return nil
                        }
                    }
                }
               else {
                    //print("error with path")
                    if index > resp.count {
                        return newReferenceImage
                    }
                }
            }*/
            
            for respItem in resp
            {
                //print("respItem:",respItem)
                if respItem.roisouter.count == 0 && respItem.roisinner.count == 0
                {
                    // replace a rectangle object
                    //print("building rectangle")
                    roisPath = ImageManager.getPathForRois(rois: respItem.rois, ratioWidth:ratioWidth, ratioHeight:ratioHeight)
                } else if respItem.rois.count == 0 {
                    roisPath = ImageManager.getPathForArchRois(rois: respItem.roisinner, archRois: respItem.roisouter, ratioWidth:ratioWidth, ratioHeight:ratioHeight)
                }
                
                if let pr = roisPath
                {
                    if let holeImage = ImageManager.getImageHole(path: pr.path, hostSize: hostRect.size)
                    {
                        //        newReferenceImage = holeImage
                        if let blendedImage = ImageManager.blendHoleWithRef(refImage: newReferenceImage, hostRect: hostRect, holeImage: holeImage)
                        {
                            //            newReferenceImage = blendedImage
                            if let replacedImage = ImageManager.addImageToHoleImage(refImageWithHole: blendedImage, replacementImage: replacement, holeRect: pr.rect)
                            {
                                newReferenceImage = replacedImage
                                //print("newReferenceImage:",newReferenceImage)
                            }
                        }
                    } else {
                        //print("error with holeImage")
                        return nil
                    }
                } else {
                    //print("error with path")
                    return nil
                }
            }
            
        }
        return newReferenceImage
    }
    */

	//------------------------------------------
    static func colorObjectInReference(house0Object: HouseObject, referenceImage: UIImage, color: UIColor) -> UIImage?
    {
//print(" ")
//print(" ")
//print(" ")
//print("0 colorObjectInReference")
        // color an overlay on whatever image is passed in
        var newReferenceImage = referenceImage
        _ = CGRect.init(x: 0, y: 0, width: referenceImage.size.width, height: referenceImage.size.height)
        var roisPath: (path: CGPath, rect: CGRect)?
        
        for resp in house0Object.response
        {
            // rois, roisinner, roisouter - coordinates of the object
            for respItem in resp
            {
                if respItem.roisouter.count == 0 && respItem.roisinner.count == 0 {
                    // replace a rectangle object
//print("building rectangle")
                    roisPath = ImageManager.getPathForRois(rois: respItem.rois)
                } else if respItem.rois.count == 0 {
//print("building arc/polygon")
                    roisPath = ImageManager.getPathForArchRois(rois: respItem.roisinner, archRois: respItem.roisouter)
                }
                // original path
                if let pr = roisPath {
                    UIGraphicsBeginImageContext(referenceImage.size)
                    
                    referenceImage.draw(at: CGPoint.zero)
                    let context = UIGraphicsGetCurrentContext()!
                    context.setAlpha(0.30)
                    context.setFillColor(UIColor.clear.cgColor)
                    context.addPath(pr.path)
                    context.fillPath()
                    if let img = UIGraphicsGetImageFromCurrentImageContext() {
                        newReferenceImage = img
                    }
                    UIGraphicsEndImageContext()
                    
                } else {
  print("error with path")
                    return nil
                }
            }
            
        }
        return newReferenceImage
    }
    
    static func loadImageFromDiskWith(fileName: String) -> UIImage? {
        
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        
        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image
            
        }
        
        return nil
    }
}
extension UIImage {
    func tinted(with color: UIColor) -> UIImage? {
        return UIGraphicsImageRenderer(size: size, format: imageRendererFormat).image { _ in
            color.set()
            return withRenderingMode(.alwaysTemplate).draw(at: .zero) }
    }
}

extension UIImage
{
    
    func saveToDocuments(filename:String)
    {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileURL = documentsDirectory.appendingPathComponent(filename)
//print("file: \(fileURL)")
        if let data = self.jpegData(compressionQuality: 1.0) {
            do {
                try data.write(to: fileURL)
            } catch {
  print("error saving file to documents:", error)
            }
        }
    }
    
    func resizeImageWith(image: UIImage, newSize: CGSize) -> UIImage {
        
        let horizontalRatio = newSize.width / image.size.width
        let verticalRatio = newSize.height / image.size.height
        
        let ratio = max(horizontalRatio, verticalRatio)
        let newSize = CGSize(width: image.size.width * ratio, height: image.size.height * ratio)
        var newImage: UIImage
        
        if #available(iOS 10.0, *) {
            let renderFormat = UIGraphicsImageRendererFormat.default()
            renderFormat.opaque = false
            let renderer = UIGraphicsImageRenderer(size: CGSize(width: newSize.width, height: newSize.height), format: renderFormat)
            newImage = renderer.image {
                (context) in
                image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
            }
        } else {
            UIGraphicsBeginImageContextWithOptions(CGSize(width: newSize.width, height: newSize.height), true, 0)
            image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
            newImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
        }
        
        return newImage
    }
    
    func withColor(_ color: UIColor) -> UIImage
    {
//print("func withColor size:",size)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        guard let ctx = UIGraphicsGetCurrentContext(), let cgImage = cgImage else { return self }
        color.setFill()
        ctx.translateBy(x: 0, y: size.height)
        ctx.scaleBy(x: 1.0, y: -1.0)
        ctx.clip(to: CGRect(x: 0, y: 0, width: size.width, height: size.height), mask: cgImage)
        ctx.fill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        guard let colored = UIGraphicsGetImageFromCurrentImageContext() else { return self }
        UIGraphicsEndImageContext()
        return colored
    }
}

extension CGFloat {
    func toRadians() -> CGFloat {
        return self * CGFloat(Float.pi) / 180.0
    }
    
    var degrees: CGFloat {
        return self * CGFloat(180.0 / Float.pi)
    }
}

extension CGPoint {
    func angle(to comparisonPoint: CGPoint) -> CGFloat {
        let originX = comparisonPoint.x - self.x
        let originY = comparisonPoint.y - self.y
        let bearingRadians = atan2f(Float(originY), Float(originX))
        var bearingDegrees = CGFloat(bearingRadians).degrees
        while bearingDegrees < 0 {
            bearingDegrees += 360
        }
        return bearingDegrees
    }
}
