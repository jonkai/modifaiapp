import Foundation
import UIKit
import PECimage

public enum GarageTypes
{
    case oneCar
    case twoCar
    case onefrontDoor
    case twofrontDoor
    case window1
//    case window2
    case somethingNotInList
}
//---------------------------------------------------------------------
class HouseObject:NSObject, Codable, NSCoding
{
    public var image_url:[String]
    public var response:[[Response]]
	public var wasProcessed:Bool = false;

	//---------------------------------------------------------------------
    public enum CodingKeys: String, CodingKey
    {
        case image_url, response, wasProcessed
    }

	//---------------------------------------------------------------------
    init(image_url: [String], response: [[Response]], wasProcessed:Bool)
    {
//print(" House Object init(results");
        self.image_url = image_url
        self.response = response
        self.wasProcessed = wasProcessed

        super.init()
    }
    
	//---------------------------------------------------------------------
    static func getGarageType(label: String) -> GarageTypes?
    {
//print("  ");
//print("  ");
//print(" getGar ageType");
        let typeArr = label.split(separator: ":")
        if typeArr.count > 1
        {
			let key = typeArr[0]
			let val = typeArr[1]
			if key.lowercased() == "garage"
			{
////print(" getGar ageType  gar age");
				if val.lowercased() == "one-car"
				{
					return .oneCar
				} else if val.lowercased() == "two-car" {
					return .twoCar
				} else {
					return .oneCar
				}
			} else if (key.lowercased() == "front door") {
////print(" getGar ageType  front door");
				if val.lowercased() == "single"
				{
					return .onefrontDoor
				} else if val.lowercased() == "double" {
					return .twofrontDoor
				} else {
					return .onefrontDoor
				}
			} else if (key.lowercased() == "window") {
////print(" getGar ageType  win dow");
				if val.lowercased() == "one-pane"
				{
					return .window1
				} else if val.lowercased() == "two-pane" {
					return .window1
				} else {
					return .window1
				}
			} else {
				return .somethingNotInList
			}
		} else {
			if label.lowercased() == "garage"
			{
				return .oneCar
			} else if (label.lowercased() == "front door") {
////print(" label.lowercased  front door");
				return .onefrontDoor
			} else {
				return .somethingNotInList
			}
		}
    }

	//---------------------------------------------------------------------
	public init(jsonString:String)
	{
//print("  ");
//print("  ");
//print(" House Object init(jsonString:",jsonString);
//print("  ");
//print("  ");

		var responseTemp:[[Response]] = [[Response]]();
		var image_url0:[String] = ["unknownURL"];

        if let dic = jsonString.convertToDictionary()
        {
        	if let tempString = dic["image_url"] as? [String]
        	{
//print("  ");
//print("House Object tempString:",tempString);
				image_url0 = tempString;
			}


        	if let tempArray = dic["response"] as? Array<Any>
        	{
//print("  ");
//print("  ");
//print("House Object tempArray:",tempArray);
//print(" type(of: tempArray) ",type(of: tempArray));
//print("  ");
				for responseAA in tempArray //as [String:Any]
				{
//print("House Object responseAA:",responseAA);
//print(" type(of: responseAA) ",type(of: responseAA));
//print("  ");
					if let response0 = responseAA as? [[String:Any]]
					{
//print("House Object response0:",response0);
//print(" type(of: response0) ",type(of: response0));
						for response00 in response0 //as [String:Any]
						{
//print("  ");
//print("  ");
//print("House Object response00:",response00);
//print(" type(of: response00) ",type(of: response00));

							let respItem = Response.init(dictionary:response00)
//print(" respItem ",respItem);
							responseTemp.append([respItem]);
//print("  ")
//print("  ")
//print("init(_ json respItem.labe ls[0]:",respItem.lab els[0])
//print("init(_ json respItem.wasGoodDr:",respItem.wasGoodDr)
//print("init(_ json respItem.wasGoodDr:",respItem.wasPro cessed)
//print("init(_ json respItem.wasArch:",respItem.wasArch)
//print("init(_ json respItem.wasPoly:",respItem.wasPoly)
//print("init(_ json respItem.wasLocked:",respItem.wasLocked)
//print("init(_ json respItem.rois:",respItem.rois)
//print("init(_ json respItem.roisouter:",respItem.roisouter)
//print("init(_ json respItem.roisinner:",respItem.roisinner)
//print("init(_ json respItem.rect:",respItem.rect)
//print("  ")
//print("init(_ json respItem.v 1:",respItem.v 1)
//print("init(_ json respI tem.v 2:",respI tem.v 2)
//print("init(_ json respItem.v3:",respItem.v3)
//print("init(_ json respItem.v4:",respItem.v4)
//print("  ")
//print("init(_ json respItem.p1:",respItem.p1.pt.x,respItem.p1.pt.y)
//print("init(_ json respItem.p2:",respItem.p2.pt.x,respItem.p2.pt.y)
//print("init(_ json respItem.p3:",respItem.p3.pt.x,respItem.p3.pt.y)
//print("init(_ json respItem.p4:",respItem.p4.pt.x,respItem.p4.pt.y)
//print("init(_ json respItem.p5:",respItem.p5.pt.x,respItem.p5.pt.y)
//print("init(_ json respItem.p6:",respItem.p6.pt.x,respItem.p6.pt.y)
//print("  ")

						}
					}
				}
			}
        }

		self.image_url = image_url0;
		self.response = responseTemp;
		self.wasProcessed = false;

        super.init()
	}

	//---------------------------------------------------------------------
	required init?(coder decoder: NSCoder)
	{
		self.image_url = decoder.decodeObject(forKey: "image_url") as! [String]
		self.response = decoder.decodeObject(forKey: "response") as! [[Response]]
		self.wasProcessed = decoder.decodeBool(forKey: "wasProcessed")

        super.init()
	}

	//---------------------------------------------------------------------
 //   func encodeWithCoder(coder: NSCoder)
    func encode(with aCoder: NSCoder)
	{
		aCoder.encode(self.image_url, forKey: "image_url")
		aCoder.encode(self.response, forKey: "response")
		aCoder.encode(self.wasProcessed, forKey: "wasProcessed")
	}
}

//// MARK: Convenience initializers and mutators
////---------------------------------------------------------------------
//extension House Object
//{
//	//---------------------------------------------------------------------
//    convenience init(data: Data, json: String) throws
//    {
////print("  ");
////print(" House Object ini t(data: Data) throws");
//        let me = try newJSONDecoder().decode(House Object.self, from: data)
//		var responseTemp:[[Response]] = [[Response]]();
//
//        if let dic = json.convertToDictionary()
//        {
//        	if let tempArray = dic["response"] as? Array<Any>
//        	{
////print("  ");
////print("  ");
////print("House Object tempArray:",tempArray);
////print(" type(of: tempArray) ",type(of: tempArray));
////print("  ");
//				for responseAA in tempArray //as [String:Any]
//				{
////print("House Object responseAA:",responseAA);
////print(" type(of: responseAA) ",type(of: responseAA));
////print("  ");
//					if let response0 = responseAA as? [[String:Any]]
//					{
////print("House Object response0:",response0);
////print(" type(of: response0) ",type(of: response0));
//						for response00 in response0 //as [String:Any]
//						{
////print("  ");
////print("  ");
////print("House Object response00:",response00);
////print(" type(of: response00) ",type(of: response00));
//
//							let respItem = Response.init(dictionary:response00)
////print(" respItem ",respItem);
//							responseTemp.append([respItem]);
////print("  ")
////print("  ")
////print("init(_ json respItem.lab els[0]:",respItem.lab els[0])
////print("init(_ json respItem.wasGoodDr:",respItem.wasGoodDr)
////print("init(_ json respItem.wasGoodDr:",respItem.wasPro cessed)
////print("init(_ json respItem.wasArch:",respItem.wasArch)
////print("init(_ json respItem.wasPoly:",respItem.wasPoly)
////print("init(_ json respItem.wasLocked:",respItem.wasLocked)
////print("init(_ json respItem.rois:",respItem.rois)
////print("init(_ json respItem.roisouter:",respItem.roisouter)
////print("init(_ json respItem.roisinner:",respItem.roisinner)
////print("init(_ json respItem.rect:",respItem.rect)
////print("  ")
////print("init(_ json respItem.v 1:",respItem.v 1)
////print("init(_ json respItem.v 2:",respItem.v 2)
////print("init(_ json respItem.v3:",respItem.v3)
////print("init(_ json respItem.v4:",respItem.v4)
////print("  ")
////print("init(_ json respItem.p1:",respItem.p1.pt.x,respItem.p1.pt.y)
////print("init(_ json respItem.p2:",respItem.p2.pt.x,respItem.p2.pt.y)
////print("init(_ json respItem.p3:",respItem.p3.pt.x,respItem.p3.pt.y)
////print("init(_ json respItem.p4:",respItem.p4.pt.x,respItem.p4.pt.y)
////print("init(_ json respItem.p5:",respItem.p5.pt.x,respItem.p5.pt.y)
////print("init(_ json respItem.p6:",respItem.p6.pt.x,respItem.p6.pt.y)
////print("  ")
//
//						}
//					}
//				}
//			}
//        }
//
////print("  ")
////print("  ")
////print("  ")
////print("House Object responseTemp.count: ",responseTemp.count);
//
////		for responseA in responseTemp
////		{
//////print("  ")
//////print("  ")
//////print("House Object responseA.count: ",responseA.count);
////			for response in responseA
////			{
//////print("House Object response: ",response);
////
//////print("  ")
//////print("  ")
//////print("House Object response.labe ls[0]:",response.labe ls[0])
//////print("House Object response.wasGoodDr:",response.wasGoodDr)
//////print("House Object response.wasArch:",response.wasArch)
//////print("House Object response.wasPoly:",response.wasPoly)
//////print("House Object response.wasLocked:",response.wasLocked)
//////print("House Object response.rois:",response.rois)
//////print("House Object response.roisouter:",response.roisouter)
//////print("House Object response.roisinner:",response.roisinner)
////
//////print("House Object response.p1.lt:",response.p1.lt as Any)
//////print("House Object response.p1.nx:",response.p1.nx as Any)
////			}
////		}
//
////print("  ")
////print("  ")
//
//        self.init(imag e_url: me.imag e_url, response: responseTemp, wasProce ssed:false)
//    }
//
//	//---------------------------------------------------------------------
//    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws
//    {
////print("House Object init(_ json: String, using encoding");
//        guard let data = json.data(using: encoding) else
//        {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data, json:json)
//    }
//
////	//---------------------------------------------------------------------
////    convenience init(fromURL url: URL) throws
////    {
//////print("House Object init(fromURL url: URL) throws");
////        try self.init(data: try Data(contentsOf: url))
////    }
//
////	//---------------------------------------------------------------------
////    func with(
//////        imageID: [String]? = nil,
////        imag e_url: [String]? = nil,
//////        results: [String]? = nil,
////   //     metadata: [String]? = nil,
////        response: [[Response]]? = nil
////        ) -> House Object
////	{
//////print(" wi th  results");
////        return House Object(
//////            imageID: imageID ?? self.imageID,
////            imag e_url: imag e_url ?? self.imag e_url,
//////            results: results ?? self.results,
////      //      metadata: metadata ?? self.metadata,
////            response: response ?? self.response
////        )
////    }
//
////	//---------------------------------------------------------------------
////    func jsonData() throws -> Data
////    {
//////print("House Object jsonData() throws -> Data ");
////        return try newJSONEncoder().encode(self)
////    }
////
////	//---------------------------------------------------------------------
////    func jsonString(encoding: String.Encoding = .utf8) throws -> String?
////    {
//////print("House Object jsonString(encoding ");
////        return String(data: try self.jsonData(), encoding: encoding)
////    }
//}

////---------------------------------------------------------------------
//extension Response
//{
//	//---------------------------------------------------------------------
//    convenience init(data: Data) throws
//    {
////print("*************************** Response init(data: Data ***********************************");
//        let me = try newJSONDecoder().decode(Response.self, from: data)
//        self.init(labels: me.labels, wasGoodDr: me.wasGoodDr, wasArch: me.wasArch, wasPoly: me.wasPoly, wasLocked: me.wasLocked,
//        			rois: me.rois, roisouter: me.roisouter, roisinner: me.roisinner, classIDS: me.classIDS)
//    }
//
//	//---------------------------------------------------------------------
//    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws
//    {
////print("Response init(_ json: String ");
//        guard let data = json.data(using: encoding) else
//        {
////print("throwing");
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
////	//---------------------------------------------------------------------
////    convenience init(fromURL url: URL) throws
////    {
//////print("Response init(fromURL url ");
////        try self.init(data: try Data(contentsOf: url))
////    }
//
////	//---------------------------------------------------------------------
////    func with(
////        labels: [String]? = nil,
////        rois: [[Int]]? = nil,
////        roisouter: [[Int]]? = nil,
////        roisinner: [[Int]]? = nil,
////        classIDS: [Int]? = nil
////        ) -> Response
////	{
//////print("Response wi th ");
////        return Response(
////            labels: labels ?? self.labels,
////            rois: rois ?? self.rois,
////            roisouter: roisouter ?? self.roisouter,
////            roisinner: roisinner ?? self.roisinner,
////            classIDS: classIDS ?? self.classIDS
////        )
////    }
//
////	//---------------------------------------------------------------------
////    func jsonData() throws -> Data
////    {
//////print("Response jsonData ");
////        return try newJSONEncoder().encode(self)
////    }
////
////	//---------------------------------------------------------------------
////    func jsonString(encoding: String.Encoding = .utf8) throws -> String?
////    {
//////print("Response jsonString ");
////        return String(data: try self.jsonData(), encoding: encoding)
////    }
//}

//---------------------------------------------------------------------
func newJSONDecoder() -> JSONDecoder
{
//print(" newJSON   Decoder ");
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *)
    {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

//---------------------------------------------------------------------
func newJSONEncoder() -> JSONEncoder
{
//print("2 newJSON  Encoder ");
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *)
    {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - URLSession response handlers
////---------------------------------------------------------------------
//extension URLSession
//{
//	//---------------------------------------------------------------------
//    fileprivate func codableTask<T: Codable>(with url: URL, completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask
//    {
////print(" codableTask ");
//        return self.dataTask(with: url)
//        { data, response, error in
//            guard let data = data, error == nil else
//            {
//                completionHandler(nil, response, error)
//                return
//            }
//            completionHandler(try? newJSONDecoder().decode(T.self, from: data), response, nil)
//        }
//    }
//
//	//---------------------------------------------------------------------
//    func objectTask(with url: URL, completionHandler: @escaping (House Object?, URLResponse?, Error?) -> Void) -> URLSessionDataTask
//    {
////print(" objectTask ");
//        return self.codableTask(with: url, completionHandler: completionHandler)
//    }
//}
