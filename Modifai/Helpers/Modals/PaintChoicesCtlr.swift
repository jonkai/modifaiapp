import Foundation
import UIKit

//-----------------------------------------------
class PaintChoicesCtlr: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource
{
	@IBOutlet weak var imgChoiceCollectionView:UICollectionView!
	var projectID:String? = nil;

//	var projectCho iceList:[ProjectThumb] = [ProjectThumb]()   //********** change 1.1.4 remove ***

	//-----------------------------------------------
	override var prefersStatusBarHidden: Bool
	{
	   return true
	}

//    //----------------------------------------------------------------------
//    override var prefersHomeIndicatorAutoHidden: Bool
//    {
//        return true
//    }

	//------------------------------------------------------------------------------------
	override var preferredScreenEdgesDeferringSystemGestures: UIRectEdge
	{
		return [.all];
	}

	//-----------------------------------------------
	override func viewDidLoad()
	{
        super.viewDidLoad()
		
		self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
		self.imgChoiceCollectionView.backgroundColor = UIColor.clear
	
	}

	//-----------------------------------------------
    override func viewWillAppear(_ animated: Bool)
    {
          self.getChoiceProjects()
    }

	//-----------------------------------------------
	override func viewDidAppear(_ animated: Bool)
	{
		super.viewDidAppear(animated)
        setNeedsUpdateOfScreenEdgesDeferringSystemGestures()
        setNeedsUpdateOfHomeIndicatorAutoHidden()
	}

	//-----------------------------------------------
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
	{
		return curProjectChoiceList.count  //********** change 1.1.4 change ***
	}

	//-----------------------------------------------
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
	{
		let projectCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PopUpCell", for: indexPath) as! PopCollectionViewCell

		if (indexPath.item < curProjectChoiceList.count)  //********** change 1.1.4 change ***
		{
			let projectThumb = curProjectChoiceList[indexPath.item]  //********** change 1.1.4 change ***

			if let thumbData = projectThumb.thumbnailData
			{
				projectCell.imgView.image = UIImage(data:thumbData);
			}

			projectCell.imgView.contentMode = .scaleAspectFit

			if (indexPath.item == 0)
			{
				projectCell.deleteBtn.isHidden = true;
			} else {
				projectCell.deleteBtn.isHidden = false;
				projectCell.deleteBtn.tag = indexPath.item
				projectCell.projectThumb = projectThumb;

				projectCell.deleteBtn.addTarget(self, action: #selector(deleteBtnAction(sender:)), for: .touchUpInside)
			}
		}

		return projectCell
	}

	//-----------------------------------------------
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
	{
//print("choice didSelectItemAt indexPath:",indexPath);

		if let topMostController = currentPaintImageController
		{
//print("pop topMostController:",topMostController as Any);
			if (indexPath.item < curProjectChoiceList.count)  //********** change 1.1.4 change ***
			{
				let projectThumb:ProjectThumb = curProjectChoiceList[indexPath.item]  //********** change 1.1.4 change ***
//print("choice didSelectItemAt projectThumb:",projectThumb as Any);
//print("choice didSelectItemAt projectThumb.projectID:",projectThumb.projectID as Any);

//print("collectionView total",curProje ctChoiceList.count)
//print("collectionView index",indexPath.row)

				if (indexPath.item == 0)
				{
					if (hasOnlyGarageDoors)
					{
						defaultDoorColor0 = doorStartColor0
						defaultFrontDrColor0 = frontDrStartColor0
						defaultLineColor = defaultDoorColor0;

//print("1 collectionView defaultDo orColor0:",defaultDoorColor0 as Any)
						scrollEndDoor = true;
						choiceIsOriginal = false;
						shouldSave = true;
						topMostController.changeToPaintChoice(projectThumb);
						topMostController.turnItAllOn()
						topMostController.scrollView.setUpColorWheelImage();
						topMostController.scrollView.touchPadView.snapToDefaultColor();
					} else {
						choiceIsOriginal = true;
						shouldSave = false;
						topMostController.showColorWell(nil)
					}
				} else {
					choiceIsOriginal = false;
					shouldSave = true;
					topMostController.changeToPaintChoice(projectThumb);
					topMostController.turnItAllOn()
					topMostController.scrollView.setUpColorWheelImage();
					topMostController.scrollView.touchPadView.snapToDefaultColor();
				}
			}
		}

		self.dismiss(animated:true, completion:
		{
//print("choice dismiss:");
		})
	}

	//-----------------------------------------------
	@IBAction func backButtonAction(_ sender: Any)
	{
		self.dismiss(animated:true, completion:nil)
	}

  	//********** change 1.1.4 remove function, no longer needed,  needs to change when project is changed in home view,
  	//********** but needs to update when deleting gallery images handled already there ***
//    //------------------------------------
//    func getChoiceProjectList(_ projectID0:String?) -> [ProjectThumb]
//	{
//		guard let projectId = projectID0 else {return [ProjectThumb]()}
////print("choice getProje ctList  projectId:",projectId)
//
//		if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
//		{
//			let fileURL = dir.appendingPathComponent("projectTh umbs_"+projectId+".txt")
////print("choice getProj ectList  fileURL:",fileURL)
//
//			do
//			{
//				let rawdata = try Data(contentsOf: fileURL)
////print("choice getProj ectList  rawdata.count:",rawdata.count)
//				if let projectList = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(rawdata) as? [ProjectThumb]
//				{
////print("choice getPro jectList  projectList:",projectList)
//					return projectList
//				} else {
//					return [ProjectThumb]()
//				}
//			} catch {
//  print(" ---------------------- choice getProj ectList ProjectExist Couldn't read house ---------------------- ")
//				return [ProjectThumb]()
//			}
//		}
//
//		return [ProjectThumb]()
//	}

	//-----------------------------------------------
	func getChoiceProjects()
	{
//		self.projectCho iceList = self.getChoiceProjectList(self.projectID);  //********** change 1.1.4 remove ***
//print("getPro jects self.getChoiceP rojectList:",self.getChoicePr ojectList)

		imgChoiceCollectionView.reloadData()
	}

	//-----------------------------------------------
	@objc func deleteBtnAction(sender:UIButton)
	{
		guard let projectId = self.projectID else {return;}

		let indexValue = sender.tag
//print("del eteBtnAction indexValue:",indexValue)

		if (indexValue > 0) && (indexValue < curProjectChoiceList.count) //********** change 1.1.4 change ***
		{
			let projectThumb:ProjectThumb = curProjectChoiceList[indexValue] //********** change 1.1.4 change ***
            
           
			curProjectChoiceList.remove(at: indexValue)  //********** change 1.1.4 change ***

			do
			{
				if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
				{
					let fileURL = dir.appendingPathComponent("projectThumbs_"+projectId+".txt")
//print("del eteBtnAction fileURL:",fileURL)
					let data = try NSKeyedArchiver.archivedData(withRootObject: curProjectChoiceList, requiringSecureCoding: false)  //********** change 1.1.4 change ***
					try data.write(to: fileURL)
				}

			} catch {
  print(" ---------------------- deleteBt nAction proje ctThumbs_ error:",error)
			}

			imgChoiceCollectionView.reloadData()

			if let projectId = projectThumb.projectID
			{
				do
				{
					if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
					{
						let fileURL = dir.appendingPathComponent("projectChoices_"+projectId+".txt")

						try FileManager.default.removeItem(at: fileURL)
//print("del eteBt nAction project has Been del ete")
					}

				} catch {
  print(" ---------------------- deleteBt nAction projec tChoices_ error:",error)
				}
			}
            if hasInternalApp
            {
                let removejpg = projectThumb.thumbnailName ?? "" //.replacingOccurrences(of:".png", with: "") ?? ""
                // let removejpgtem = removejpg.replacingOccurrences(of:".txt", with: "")

//                if let projectId = projectThumb.projectID
//                {
                //let paintNametemp = removejpg.replacingOccurrences(of: "_paintMask0", with: "projectChoices_"+projectId)
                  //  let paintNamewith = "\(removejpg)--"

                DataManager.removePaintImageFromServer(filename:removejpg, paintURL:"")
               // }
                for i in 0..<3
                {
                    let paintNameser = removejpg.replacingOccurrences(of: "_paintMask0", with: "_paint\(i)")
                   // let paintNamewith = "\(paintNameser)--"
                    DataManager.removePaintImageFromServer(filename:paintNameser, paintURL:"")
                }
            }
		}
	}
}
