//
//  WrapperView.swift
//  A_J_Dot_Loading_Indicator
//
//  Created by Alex Jiang on 14/3/18.
//  Copyright © 2018 Alex Jiang. All rights reserved.
//

import UIKit

public final class AJDotLoadingView: UIView {

    // MARK: - Public API

    @IBOutlet weak var loadingLabel: UILabel!
    @IBOutlet weak var dotLoadingIndicator: UIDotLoadingIndicator!

    public func startAnimating()
    {
        dotLoadingIndicator.startAnimating()
    }

    public func stopAnimating()
    {
        dotLoadingIndicator.stopAnimating()
        dotLoadingIndicator.removeFromSuperview()
    }
}

private var aj_loadingIndicatorAssociationKey = 0x1023

extension AJDotLoadingView: NibLoadable {}

public extension UIView {
    private var aj_loadingIndicator: AJDotLoadingView?
    {
        get
        {
            return objc_getAssociatedObject(self, &aj_loadingIndicatorAssociationKey) as? AJDotLoadingView
        }
        set {
            objc_setAssociatedObject(self, &aj_loadingIndicatorAssociationKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    func aj_showDotLoadingIndicator()
    {
        let dotLoadView = AJDotLoadingView.createFromNib()
        dotLoadView.alpha = 0.90
        aj_loadingIndicator = dotLoadView
        
        safeAddSubView(subView: dotLoadView, viewTag: Int(aj_loadingIndicatorAssociationKey))

        //**************** crash fix Lakshmi - 2.0.1
        dotLoadView.loadingLabel.text = "Give us a few seconds to process your picture"
        if let shopping = UserDefaults.standard.value(forKey: "shopping") as? String
        {
            if shopping == "-1"
            {
                UserDefaults.standard.set("0" , forKey:"shopping")
                dotLoadView.loadingLabel.text = "Give us a few seconds to process your picture"

            }
        }

        dotLoadView.translatesAutoresizingMaskIntoConstraints = false
        self.topAnchor.constraint(equalTo: dotLoadView.topAnchor).isActive = true
        self.bottomAnchor.constraint(equalTo: dotLoadView.bottomAnchor).isActive = true
        self.leftAnchor.constraint(equalTo: dotLoadView.leftAnchor).isActive = true
        self.rightAnchor.constraint(equalTo: dotLoadView.rightAnchor).isActive = true

        aj_loadingIndicator?.startAnimating()
    }

    func aj_hideDotLoadingIndicator() {
        aj_loadingIndicator?.stopAnimating()
        aj_loadingIndicator?.removeFromSuperview()
        aj_loadingIndicator = nil
    }
}
