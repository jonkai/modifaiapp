//
//  LocationPopUp.swift
//  Modifai

import Foundation
import CoreLocation
import FirebaseAnalytics
import UIKit

public protocol ZipcodeSelectionDelegate: class {
    func onSelectionCompletionClick(sender: String)
}
class LocationPopUp: UIViewController,UITextFieldDelegate {
    var maxLength = 5
    @IBOutlet weak var checkBtn: UIButton!
    
    @IBOutlet weak var skipBtn: UIButton!

    @IBOutlet weak var zipcodeTF: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var popupView: UIView!
    var type: String!

    weak var delegate : ZipcodeSelectionDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        zipcodeTF.autocorrectionType = .no
        //Analytics.logEvent("Zip_screen_displayed", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
        self.view.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        zipcodeTF.attributedPlaceholder = NSAttributedString(string: zipcodeTF.placeholder!, attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        self.showAnimate()        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        checkBtn.backgroundColor = themeColor
        checkBtn.setTitleColor(titleColor, for: .normal)
           if type == "order"
                {
                    titleLabel.text = "Please enter an order number"
                    zipcodeTF.placeholder = "Order Number"
                    checkBtn.isEnabled = false
                    checkBtn.setTitle("Download Project", for: .normal)
                    zipcodeTF.keyboardType = .alphabet
        maxLength = 4
                }else
                {
                    titleLabel.text = "Enter your Zip code to see popular products in your area"
                    checkBtn.isEnabled = true

                    zipcodeTF.placeholder = "Zip Code"
                    checkBtn.setTitle("Check Availability", for: .normal)
                    zipcodeTF.keyboardType = .numberPad
        maxLength = 5

                }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        if newString.length >= maxLength
        {
            checkBtn.isEnabled = true
        }else
        {
            checkBtn.isEnabled = false

        }
        return newString.length <= maxLength
    }

    @IBAction func checkAction(_ sender: Any)
    {
        if zipcodeTF.text?.count == maxLength
        {
            // Analytics.logEvent("Zip_submit", parameters: ["userID":((UserDefaults.standard.value(forKey:"UserID") as? String) ?? "")])
            self.dismiss(animated:true, completion: nil)
            self.view.endEditing(true)
            if type == "order"
            {
                LoadingIndicatorView.show("Please Wait")
                
                DataManager.getAgentHouseList(orderNumber:zipcodeTF.text!, completion: { (error,data) in
                    if data != nil
                    {
                        DataManager.GetPaintColorsForAgentHomes(orderNumber: self.zipcodeTF.text!,completion:{
                            (error,data) in
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 4, execute: {
                                // code to execute
                                self.dismiss(animated:true, completion: nil)
                                LoadingIndicatorView.hide()
                                if data != nil{
                                    
                                    if let home = homeviewController{
                                        
                                        home.loadDownloadedImage()
                                        return
                                    }else{
                                        UserDefaults.standard.set(true, forKey: "loadedFirstTime")
                                        AppDelegate.orderDownloaded()
                                    }
                                }else
                                {
                                    
                                    // AppDelegate.orderDownloaded()
                                    
                                }
                            })
                            
                            
                        })
                        //  self.delegate.onSelectionCompletionClick(sender: "0")
                        
                    }else
                    {
                        
                        self.dismiss(animated:true, completion: nil)
                        LoadingIndicatorView.hide()
                        //  AppDelegate.orderDownloaded()   // 2.1.2 remove line
                    }
                    
                })
                
            } else
            {
                UserDefaults.standard.set(zipcodeTF.text , forKey:"zipcode")
                
                self.delegate.onSelectionCompletionClick(sender: "0")
                let bundleIdentifier = Bundle.main.bundleIdentifier
                let bundleName = String(describing:bundleIdentifier)
                let data1 = Data((zipcodeTF.text!).utf8)
                let status1 = KeyChain.save(key:"\(bundleName)agentzipcode", data: data1)
                print("\(bundleName) zipcode keychain status: ", status1)
                
                guard UserDefaults.standard.value(forKey:"UserID") != nil else{return}
                DataManager.UpdateUserZip(ZipCode: zipcodeTF.text!, completion: { (error,shopping) in
                    UserDefaults.standard.set(shopping , forKey:"shopping")
                    
                    let tabBarViewController = UIApplication.shared.windows.first!.rootViewController as!   UITabBarController
                    
                    if shopping  == "1"
                    {
                        
                        if (tabBarViewController.viewControllers?.count == 3 && hasInternalApp) || (tabBarViewController.viewControllers?.count == 4 && isHomeApp)
                        {
                            tabBarViewController.viewControllers?.remove(at: 2)
                            if isHomeApp
                            {
                                tabBarViewController.viewControllers?.remove(at: 2)
                            }
                            let storyBoard: UIStoryboard = UIStoryboard(name: "CustomTabBar", bundle: nil)
                            
                            let shopNavVC = storyBoard.instantiateViewController(withIdentifier: "ShoppingNavController") as! UINavigationController
                            let infoNavVC = storyBoard.instantiateViewController(withIdentifier: "InfoNavController") as! UINavigationController
                            let feedInfoVC = storyBoard.instantiateViewController(withIdentifier: "FeedNavController") as! UINavigationController
                            
                            tabBarViewController.viewControllers?.append(shopNavVC)
                            if isHomeApp
                            {
                                tabBarViewController.viewControllers?.append(feedInfoVC)
                                
                            }
                            tabBarViewController.viewControllers?.append(infoNavVC)
                        }
                        
                    }else
                    {
                        
                        if (tabBarViewController.viewControllers?.count == 4 && hasInternalApp) || (tabBarViewController.viewControllers?.count == 5 && isHomeApp)
                        {
                            tabBarViewController.viewControllers?.remove(at: 2)
                        }
                    }
                })
            }
        }
    }
    
    @IBAction func skipButton(_ sender: Any) {
        if let wd = UIApplication.shared.delegate?.window {
                 let vc = wd!.rootViewController
                 if(vc is UITabBarController){
                    LoadingIndicatorView.hide()
                   self.dismiss(animated:true, completion: nil)
                     return
                 }
             }
        UserDefaults.standard.set(true, forKey: "OrderDownload")
//print("skipButton orderDo wnloaded")
        AppDelegate.orderDownloaded()
    }
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
}
