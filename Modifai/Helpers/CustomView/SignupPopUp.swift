//
//  SignupPopUp.swift
//  Modifai


import Foundation
import CoreLocation
import AuthenticationServices

public protocol SignInClickDelegate: class {
    func onSignInCompletionClick(sender: String)
}
class SignupPopUp: UIViewController,ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
   
    
    
    @IBOutlet var signInWithAppleButton: ASAuthorizationAppleIDButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var popupView: UIView!
    
    weak var delegate : SignInClickDelegate!
    var fromOnboard:Bool!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = themeColor
        self.setupButton()
        self.showAnimate()
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated:true, completion: nil)
        if fromOnboard
        {
            if UserDefaults.standard.bool(forKey: "signUpDone") == false
                {
                    let bundleIdentifier = Bundle.main.bundleIdentifier
                    let bundleName = String(describing:bundleIdentifier)

                    var tokenId = ""
                           if let receivedData2 = KeyChain.load(key: "\(bundleName)newaccessTokenValue") {
                                                 tokenId = String(decoding: receivedData2, as: UTF8.self)
                                                 print(" accessTokenValue result: ", tokenId)
                                                 UserDefaults.standard.set(tokenId, forKey: "accessTokenValue")
                                          }
            DataManager.registerUser(emailId:"", tokenId: tokenId , completion: {(error, userId) in
            UserDefaults.standard.set(userId, forKey:"UserID")
            })
            }
        self.delegate.onSignInCompletionClick(sender: "0")
        }
       
    }
    private func setupButton() {
          
           
            signInWithAppleButton.addTarget(self, action: #selector(signInButtonTapped), for: .touchUpInside)
        signInWithAppleButton.translatesAutoresizingMaskIntoConstraints = false
        signInWithAppleButton.layer.borderColor = UIColor.black.cgColor
        signInWithAppleButton.layer.borderWidth = 1.0
        signInWithAppleButton.layer.cornerRadius = 5.0

    }
    @objc private func signInButtonTapped() {
        let provider = ASAuthorizationAppleIDProvider()
        let request = provider.createRequest()
     request.requestedScopes = [.fullName, .email]
        
        let controller = ASAuthorizationController(authorizationRequests: [request])
        controller.delegate = self
        controller.presentationContextProvider = self
        controller.performRequests()
    }
     //Mark :- Apple SignIN
        func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
            print(error.localizedDescription)
        }

        /// Handle successful sign ins
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        guard let credentials = authorization.credential as? ASAuthorizationAppleIDCredential else { return }
        
        // identifierLabel.text = "Identifier: " + credentials.user
        
        let statusText: String
        switch credentials.realUserStatus {
        case .likelyReal:
            statusText = "You are trusted by Apple"
        case .unsupported:
            statusText = "You're not trusted by Apple"
        case .unknown:
            statusText = "Apple does not know whether to trust you"
        @unknown default:
            statusText = "Unknown trust status"
        }
        var emailLabel = credentials.email ?? ""
        var tokenId = credentials.user
        UserDefaults.standard.set(tokenId, forKey: "accessTokenValue")
        
        //UserDefaults.standard.set(credentials.email! , forKey:"UserEmail")
        //  UserDefaults.standard.set(emailLabel, forKey:"UserEmail")
        print("status:",statusText)
        print("email:",emailLabel)
        print("accessTokenValue:",tokenId)
        let bundleIdentifier = Bundle.main.bundleIdentifier
        let bundleName = String(describing:bundleIdentifier)
        if emailLabel.count == 0
        {
            
            
            if let receivedData3 = KeyChain.load(key: "\(bundleName)newUserEmail") {
                let result = String(decoding: receivedData3, as: UTF8.self)
                print(" UserEmail result: ", result)
                UserDefaults.standard.set(result, forKey: "UserEmail")
                emailLabel = result
                
            }
        }
        
        if let receivedData2 = KeyChain.load(key: "\(bundleName)newaccessTokenValue") {
            tokenId = String(decoding: receivedData2, as: UTF8.self)
            print(" accessTokenValue result: ", tokenId)
            UserDefaults.standard.set(tokenId, forKey: "accessTokenValue")
        }
        DataManager.registerUser(emailId:emailLabel, tokenId: tokenId , completion: {(error, userId) in
            UserDefaults.standard.set(true,forKey: "signUpDone")
            LoadingIndicatorView.hide()
            UserDefaults.standard.set(emailLabel, forKey: "UserEmail")
            UserDefaults.standard.set(userId, forKey:"UserID")
            let bundleIdentifier = Bundle.main.bundleIdentifier
            let bundleName = String(describing:bundleIdentifier)
            DataManager.updateUserIDforOldImageIDs()
            if let receivedData1 = KeyChain.load(key: "\(bundleName)agentzipcode") {
                let result = String(decoding: receivedData1, as: UTF8.self)
                print(" zipcode result home: ", result)
                UserDefaults.standard.set(result , forKey:"zipcode")
                
                DataManager.UpdateUserZip(ZipCode: result, completion: { (error,shopping) in
                    UserDefaults.standard.set(shopping , forKey:"shopping")
                    
                    if let tabBarViewController = UIApplication.shared.windows.first!.rootViewController as? UITabBarController
                    {
                        if shoppingVal  == "1"
                        {
                            
                            if (tabBarViewController.viewControllers?.count == 3 && hasInternalApp) || (tabBarViewController.viewControllers?.count == 4 && isHomeApp)
                            {
                                tabBarViewController.viewControllers?.remove(at: 2)
                                if isHomeApp
                                                           {
                                                               tabBarViewController.viewControllers?.remove(at: 2)
                                                           }
                                let storyBoard: UIStoryboard = UIStoryboard(name: "CustomTabBar", bundle: nil)
                                
                                let shopNavVC = storyBoard.instantiateViewController(withIdentifier: "ShoppingNavController") as! UINavigationController
                                let infoNavVC = storyBoard.instantiateViewController(withIdentifier: "InfoNavController") as! UINavigationController
                                let feedInfoVC = storyBoard.instantiateViewController(withIdentifier: "FeedNavController") as! UINavigationController
                                
                                tabBarViewController.viewControllers?.append(shopNavVC)
                                if isHomeApp
                                {
                                    tabBarViewController.viewControllers?.append(feedInfoVC)
                                    
                                }
                                tabBarViewController.viewControllers?.append(infoNavVC)
                            }
                            
                        }else
                        {
                            
                            if (tabBarViewController.viewControllers?.count == 4 && hasInternalApp) || (tabBarViewController.viewControllers?.count == 5 && isHomeApp)
                            {
                                tabBarViewController.viewControllers?.remove(at: 2)
                            }
                        }
                    }
                    
                })
            }
            self.dismiss(animated:true, completion: nil)
            self.delegate.onSignInCompletionClick(sender: "0")
            
        })
        // }
        
    }
        
        // MARK: - ASAuthorizationControllerPresentationContextProviding
        
        func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
            return view.window ?? UIWindow()
        }
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
}
