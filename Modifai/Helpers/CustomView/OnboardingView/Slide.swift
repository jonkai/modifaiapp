import UIKit
import AuthenticationServices
class Slide: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelTitle: UITextView!
    @IBOutlet weak var labelDesc: UILabel!
    @IBOutlet weak var labelDesc2: UILabel!
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image3: UIImageView!
    @IBOutlet weak var labelDesc3: UILabel!

    @IBOutlet weak var createButton: ASAuthorizationAppleIDButton!
    
}
