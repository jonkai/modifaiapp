//
//  Images+CoreDataProperties.swift
//  
//
//  Created by Apple on 08/04/19.
//
//

import Foundation
import CoreData


extension Images {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Images> {
        return NSFetchRequest<Images>(entityName: "Images")
    }

    @NSManaged public var imgName: String?
    @NSManaged public var image: NSData?

}
