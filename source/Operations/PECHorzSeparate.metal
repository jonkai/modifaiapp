#include <metal_stdlib>
#include "OperationShaderTypes.h"
using namespace metal;

typedef struct
{
	float texelWidth1;
	float texelHeight1;

	float texelWidth2;
	float texelHeight2;

	float texelWidth3;
	float texelHeight3;

	float texelWidth4;
	float texelHeight4;

	float texelWidth5;
	float texelHeight5;

	float texelHeight6;
} HorzSeparateUniform;

fragment half4 horzSeparateFragment(SingleInputVertexIO fragmentInput [[stage_in]], texture2d<half> inputTexture [[texture(0)]],
                             constant HorzSeparateUniform& uniform [[buffer(1)]])
{
    constexpr sampler quadSampler;
	half4 colorTexture = inputTexture.sample(quadSampler, fragmentInput.textureCoordinate);

	for (int i = 0; i < 1; i++)
	{
		float	botRow1    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
		float	botRow2    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;
		float	botRow3    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;
		float	botRow4    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight4)).a;
		float	botRow5    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight5)).a;


//		if ((botRow1 > 0.5) && (botRow3+botRow4 < 0.5) && (botRow5 > 0.5) && (sideRowR5tot+sideRowL5tot > 3.5))
//					{colorTexture = half4(1.0,0.0,0.0,1.0); break;}


		if ((botRow1 > 0.5) && (botRow3+botRow4 < 0.5) && (botRow5 > 0.5))
		{

			float sideRowR15  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight5)).a;
			float sideRowR25  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight5)).a;
			float sideRowR35  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight5)).a;

			float sideRowR5str = sideRowR15+sideRowR25+sideRowR35;

			float sideRowR24d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight4)).a;
			float sideRowR34d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight4)).a;

			float sideRowR5tot = sideRowR5str+sideRowR24d+sideRowR34d; // which is up in reality


			float sideRowL15  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight5)).a;
			float sideRowL25  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight5)).a;
			float sideRowL35  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight5)).a;

			float sideRowL5str = sideRowL15+sideRowL25+sideRowL35;

			float sideRowL26d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight6)).a;
			float sideRowL36d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight6)).a;

			float sideRowL5tot = sideRowL5str+sideRowL26d+sideRowL36d;



			float sideRowR26d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight6)).a;
			float sideRowR36d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight6)).a;

			float sideRowR5totd = sideRowR5str+sideRowR26d+sideRowR36d;


			float sideRowL24d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight4)).a; // which is up in reality
			float sideRowL34d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight4)).a;

			float sideRowL5totu = sideRowL5str+sideRowL24d+sideRowL34d;


			int goingUp = 0;

			if (abs(sideRowL5totu-sideRowR5totd) > 1.5)
			{
				goingUp = 1;
			} else if (sideRowR5tot+sideRowL5tot >= sideRowR5totd+sideRowL5totu) {
				goingUp = 1;
			}



			if (goingUp == 1)
			{
				if (sideRowR5tot+sideRowL5tot > 3.5)
					{colorTexture = half4(1.0,0.0,0.0,1.0); break;}

			} else {

				if (sideRowR5totd+sideRowL5totu > 3.5)
					{colorTexture = half4(1.0,1.0,1.0,1.0); break;}
			}
		}


		if (colorTexture.a > 0.5)
		{
			if (botRow4 > 0.5)
			{
				float sideRowR14  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight4)).a;
				float sideRowR24  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight4)).a;
				float sideRowR34  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight4)).a;

				float sideRowR4str = sideRowR14+sideRowR24+sideRowR34;

				float sideRowR23d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;
				float sideRowR33d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;

				float sideRowR4tot = sideRowR4str+sideRowR23d+sideRowR33d;


				float sideRowL14  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight4)).a;
				float sideRowL24  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight4)).a;
				float sideRowL34  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight4)).a;

				float sideRowL4str = sideRowL14+sideRowL24+sideRowL34;

				float sideRowL25d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight5)).a;
				float sideRowL35d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight5)).a;

				float sideRowL4tot = sideRowL4str+sideRowL25d+sideRowL35d;

				if ((botRow2+botRow3 < 0.5) && (sideRowR4tot > 1.5) && (sideRowL4tot > 1.5))
						{colorTexture = half4(0.0,0.0,0.0,0.0); break;}



				float sideRowR25d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight5)).a;
				float sideRowR35d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight5)).a;

				float sideRowR4totd = sideRowR4str+sideRowR25d+sideRowR35d;


				float sideRowL23d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a; // which is up in reality
				float sideRowL33d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;

				float sideRowL4totu = sideRowL4str+sideRowL23d+sideRowL33d;


				if ((botRow2+botRow3 < 0.5) && (sideRowR4totd > 1.5) && (sideRowL4totu > 1.5))
						{colorTexture = half4(0.0,0.0,0.0,0.0); break;}
			}



			if (botRow3 > 0.5)
			{
				float sideRowR1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y)).a;
				float sideRowR2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y)).a;
				float sideRowR3  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y)).a;

				float sideRowR0tot = sideRowR1+sideRowR2+sideRowR3;

				float sideRowR1u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
				float sideRowR2u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
				float sideRowR3u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
//				float sideRowR4u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth4, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;

	//			float sideRowR1utot = sideRowR1u+sideRowR2u+sideRowR3u;
				float sideRowRtot   = botRow1+sideRowR0tot+sideRowR1u+sideRowR2u+sideRowR3u;


				float sideRowL1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y)).a;
				float sideRowL2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y)).a;
				float sideRowL3  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y)).a;

				float sideRowL0tot = sideRowL1+sideRowL2+sideRowL3;

				float sideRowL1d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
				float sideRowL2d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
				float sideRowL3d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
//				float sideRowL4d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth4, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

	//			float sideRowL1dtot = sideRowL1d+sideRowL2d+sideRowL3d;
				float sideRowLtot   = botRow1+sideRowL0tot+sideRowL1d+sideRowL2d+sideRowL3d;


				float	topRow1    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;


				float sideRowR1d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
				float sideRowR2d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
				float sideRowR3d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
//				float sideRowR4d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth4, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

				float sideRowRtotd  = topRow1+sideRowR0tot+sideRowR1d+sideRowR2d+sideRowR3d;


				float sideRowL1u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
				float sideRowL2u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
				float sideRowL3u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
//				float sideRowL4u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth4, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;

				float sideRowLtotu  = topRow1+sideRowL0tot+sideRowL1u+sideRowL2u+sideRowL3u;


				if ((sideRowRtot+sideRowLtot < 4.5) && (sideRowRtotd+sideRowLtotu < 4.5))
				{
					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}
				} else {

					int goingUp = 0;

			//		if (((sideRowLtotu > 4.5) && (sideRowRtotd < 4.5)) || ((sideRowLtotu < 4.5) && (sideRowRtotd > 4.5)))
					if (abs(sideRowLtotu-sideRowRtotd) > 1.5)
					{
						goingUp = 1;
					} else if (sideRowRtot+sideRowLtot >= sideRowRtotd+sideRowLtotu) {
						goingUp = 1;
					}

					if (goingUp == 1)
					{
						float sideRowR13  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;
						float sideRowR23  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;
						float sideRowR33  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;

						float sideRowR22d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;
						float sideRowR32d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;

						float sideRowR3tot = sideRowR13+sideRowR23+sideRowR33+sideRowR22d+sideRowR32d;


						float sideRowL13  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;
						float sideRowL23  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;
						float sideRowL33  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;

						float sideRowL24d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight4)).a;
						float sideRowL34d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight4)).a;

						float sideRowL3tot = sideRowL13+sideRowL23+sideRowL33+sideRowL24d+sideRowL34d;


						if (((sideRowRtot < 2.5) || (sideRowLtot < 2.5)) && (sideRowR3tot+sideRowL3tot > 3.5))
								{colorTexture = half4(0.0,0.0,0.0,0.0); break;}


					} else {

						float sideRowR13  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;
						float sideRowR23  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;
						float sideRowR33  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;


						float sideRowR24d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight4)).a;
						float sideRowR34d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight4)).a;

						float sideRowR3totd = sideRowR13+sideRowR23+sideRowR33+sideRowR24d+sideRowR34d;

						float sideRowL13  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;
						float sideRowL23  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;
						float sideRowL33  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;

						float sideRowL22d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;
						float sideRowL32d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;

						float sideRowL3totu = sideRowL13+sideRowL23+sideRowL33+sideRowL22d+sideRowL32d;


						if (((sideRowRtotd < 2.5) || (sideRowLtotu < 2.5)) && (sideRowR3totd+sideRowL3totu > 3.5))
								{colorTexture = half4(0.0,0.0,0.0,0.0); break;}
					}
				}
			}
		}
	}

    return colorTexture;
}
