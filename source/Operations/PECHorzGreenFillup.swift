public class PECHorzGreenFillup: BasicOperation
{
    public var texelWidth1:Float  = 1.0 { didSet { uniformSettings["texelWidth1"]  = texelWidth1  }}
    public var texelHeight1:Float = 1.0 { didSet { uniformSettings["texelHeight1"] = texelHeight1 }}

    public var texelWidth2:Float  = 1.0 { didSet { uniformSettings["texelWidth2"]  = texelWidth2  }}
    public var texelHeight2:Float = 1.0 { didSet { uniformSettings["texelHeight2"] = texelHeight2 }}

    public var texelWidth3:Float  = 1.0 { didSet { uniformSettings["texelWidth3"]  = texelWidth3  }}
    public var texelHeight3:Float = 1.0 { didSet { uniformSettings["texelHeight3"] = texelHeight3 }}

    public var texelWidth4:Float  = 1.0 { didSet { uniformSettings["texelWidth4"]  = texelWidth4  }}
    public var texelWidth5:Float  = 1.0 { didSet { uniformSettings["texelWidth5"]  = texelWidth5  }}
    public var texelWidth6:Float  = 1.0 { didSet { uniformSettings["texelWidth6"]  = texelWidth6  }}
    public var texelWidth7:Float  = 1.0 { didSet { uniformSettings["texelWidth7"]  = texelWidth7  }}
    public var texelWidth8:Float  = 1.0 { didSet { uniformSettings["texelWidth8"]  = texelWidth8  }}

	//--------------------------------------------------------
    public init()
    {
        super.init(fragmentFunctionName:"horzGreenFillupFragment", numberOfInputs:1)

		let texelWidth:Float  = 1.0 / Float(forcedImageSize.width);
		let texelHeight:Float = 1.0 / Float(forcedImageSize.height);

        ({texelWidth1  = texelWidth})()
        ({texelHeight1 = texelHeight})()

        ({texelWidth2  = texelWidth  * 2.0})()
        ({texelHeight2 = texelHeight * 2.0})()

        ({texelWidth3  = texelWidth  * 3.0})()
        ({texelHeight3 = texelHeight * 3.0})()

        ({texelWidth4  = texelWidth  * 4.0})()
        ({texelWidth5  = texelWidth  * 5.0})()
        ({texelWidth6  = texelWidth  * 6.0})()
        ({texelWidth7  = texelWidth  * 7.0})()
        ({texelWidth8  = texelWidth  * 8.0})()
    }
}

