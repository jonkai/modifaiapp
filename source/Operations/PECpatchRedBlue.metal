#include <metal_stdlib>
#include "OperationShaderTypes.h"
using namespace metal;

typedef struct
{
	float texelWidth1;
	float texelHeight1;

	float texelWidth2;
	float texelHeight2;

	float texelWidth3;
	float texelHeight3;

	float texelWidth4;
	float texelHeight4;

	float texelWidth5;
	float texelHeight5;

	float texelHeight6;
} PatchRedBlueUniform;

fragment half4 patchRedBlueFragment(SingleInputVertexIO fragmentInput [[stage_in]], texture2d<half> inputTexture [[texture(0)]],
                             constant PatchRedBlueUniform& uniform [[buffer(1)]])
{
    constexpr sampler quadSampler;
	half colorA = inputTexture.sample(quadSampler, fragmentInput.textureCoordinate).a;
	half4 colorTexture = half4(0.0,0.0,0.0,colorA);

		float	sideRowL1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y)).a;
		float	sideRowL2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y)).a;
		float	sideRowL3  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y)).a;
		float	sideRowL4  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth4, fragmentInput.textureCoordinate.y)).a;

		if ((colorA > 0.5) && (sideRowL1 + sideRowL2 + sideRowL3 + sideRowL4 > 0.5))
					{colorTexture = half4(0.0,0.0,0.0,0.0);}

    return colorTexture;
}
