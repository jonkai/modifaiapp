#include <metal_stdlib>
#include "OperationShaderTypes.h"
using namespace metal;

typedef struct
{
	float texelWidth1;
	float texelHeight1;

	float texelWidth2;
	float texelHeight2;

	float texelWidth3;
	float texelHeight3;

	float texelWidth4;
	float texelHeight4;

	float texelWidth5;
	float texelHeight5;

	float texelHeight6;
} HorzClearUniform;

fragment half4 horzClearFragment(SingleInputVertexIO fragmentInput [[stage_in]], texture2d<half> inputTexture [[texture(0)]],
                             constant HorzClearUniform& uniform [[buffer(1)]])
{
    constexpr sampler quadSampler;
	float colorA = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate)).a;
	half4 colorTexture = half4(0.0,0.0,0.0,colorA);

	if (colorA > 0.2)
	for (int i = 0; i < 1; i++)
	{
		float	sideRowR1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
		float	sideRowR10 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y)).a;
		float	sideRowR1d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

		float	sideRowR1tot = sideRowR1 + sideRowR10 + sideRowR1d;

		if (sideRowR1tot < 0.5)
					{colorTexture = half4(1.0,1.0,0.0,1.0); break;}

		float	sideRowL1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
		float	sideRowL10 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y)).a;
		float	sideRowL1d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

		float	sideRowL1tot = sideRowL1 + sideRowL10 + sideRowL1d;

		if (sideRowL1tot < 0.5)
					{colorTexture = half4(1.0,0.0,1.0,1.0); break;}
	}

    return colorTexture;
}
