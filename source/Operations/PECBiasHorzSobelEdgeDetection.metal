#include <metal_stdlib>
#include "TexelSamplingTypes.h"
using namespace metal;

fragment half4 biasHorzSobelEdgeDetectionFragment(NearbyTexelVertexIO fragmentInput [[stage_in]], texture2d<half> inputTexture [[texture(0)]])
{
    constexpr sampler quadSampler(coord::pixel);
    half4 color = inputTexture.sample(quadSampler, fragmentInput.textureCoordinate);

	if (color.r+color.g+color.b < 0.01)
	{
		color = half4(0.0,0.0,0.0,0.0);
	} else {

		float bottomLeftIntensity = inputTexture.sample(quadSampler, fragmentInput.bottomTextureCoordinate).r; // biased horizontal
		float bottomRightIntensity = inputTexture.sample(quadSampler, fragmentInput.bottomRightTextureCoordinate).r;
		float leftIntensity = inputTexture.sample(quadSampler, fragmentInput.leftTextureCoordinate).r;
		float rightIntensity = inputTexture.sample(quadSampler, fragmentInput.rightTextureCoordinate).r;
		float bottomIntensity = inputTexture.sample(quadSampler, fragmentInput.bottomTextureCoordinate).r;
		float topIntensity = inputTexture.sample(quadSampler, fragmentInput.topTextureCoordinate).r;
		float topRightIntensity = inputTexture.sample(quadSampler, fragmentInput.topRightTextureCoordinate).r;
		float topLeftIntensity = inputTexture.sample(quadSampler, fragmentInput.topLeftTextureCoordinate).r;

		float2 gradientDirection;
		gradientDirection.x = -bottomLeftIntensity - 2.0 * leftIntensity - topLeftIntensity + bottomRightIntensity + 2.0 * rightIntensity + topRightIntensity;
		gradientDirection.y = -topLeftIntensity - 2.0 * topIntensity - topRightIntensity + bottomLeftIntensity + 2.0 * bottomIntensity + bottomRightIntensity;

		float gradientMagnitude = length(gradientDirection);
		float2 normalizedDirection = normalize(gradientDirection);

		normalizedDirection = sign(normalizedDirection) * floor(abs(normalizedDirection) + 0.617316); // Offset by 1-sin(pi/8) to set to 0 if near axis, 1 if away
		normalizedDirection = (normalizedDirection + 1.0) * 0.5; // Place -1.0 - 1.0 within 0 - 1.0

		color = half4(gradientMagnitude, normalizedDirection.x, normalizedDirection.y, 1.0);
    }
    
    return color;
}
