public class PECDirectionalNonMaximumSuppression: BasicOperation
{
    public var texelWidth1:Float  = 1.0 { didSet { uniformSettings["texelWidth1"]  = texelWidth1  }}
    public var texelHeight1:Float = 1.0 { didSet { uniformSettings["texelHeight1"] = texelHeight1 }}

    public var texelWidth2:Float  = 1.0 { didSet { uniformSettings["texelWidth2"]  = texelWidth2  }}
    public var texelHeight2:Float = 1.0 { didSet { uniformSettings["texelHeight2"] = texelHeight2 }}

    public var upperThreshold:Float = 0.07 { didSet { uniformSettings["upperThreshold"] = upperThreshold }}
    public var lowerThreshold:Float = 0.07 { didSet { uniformSettings["lowerThreshold"] = lowerThreshold }}

	//--------------------------------------------------------
    public init()
    {
        super.init(fragmentFunctionName:"directionalNonMaximumSuppressionFragment", numberOfInputs:1)

		let texelWidth:Float  = 1.0 / Float(forcedImageSize.width);
		let texelHeight:Float = 1.0 / Float(forcedImageSize.height);

//print("texelWidth:",texelWidth)
//print("texelHeight:",texelHeight)

        ({texelWidth1  = texelWidth})()
        ({texelHeight1 = texelHeight})()

        ({texelWidth2  = texelWidth  * 2.0})()
        ({texelHeight2 = texelHeight * 2.0})()

        ({upperThreshold = 0.07})()
        ({lowerThreshold = 0.07})()
    }
}

