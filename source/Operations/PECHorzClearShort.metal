#include <metal_stdlib>
#include "OperationShaderTypes.h"
using namespace metal;

typedef struct
{
	float texelWidth1;
	float texelHeight1;

	float texelWidth2;
	float texelHeight2;

	float texelWidth3;
	float texelHeight3;

	float texelWidth4;
} HorzClearShortUniform;

fragment half4 horzClearShortFragment(SingleInputVertexIO fragmentInput [[stage_in]], texture2d<half> inputTexture [[texture(0)]],
                             constant HorzClearShortUniform& uniform [[buffer(1)]])
{
    constexpr sampler quadSampler;
	half colorA = inputTexture.sample(quadSampler, fragmentInput.textureCoordinate).a;
	half4 colorTexture = half4(0.0,0.0,0.0,colorA);

	if (colorA > 0.5)
	for (int i = 0; i < 1; i++)
	{
			float	sideRowL11u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowL0   = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y)).a;
			float	sideRowL11d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

			float	sideRowL1tot = sideRowL11u + sideRowL0 + sideRowL11d;

			float	sideRowR11u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowR0   = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y)).a;
			float	sideRowR11d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

			float	sideRowR1tot = sideRowR11u + sideRowR0 + sideRowR11d;


			float	sideRowB  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowT  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;

			float	sideBotRow1 = sideRowL11d + sideRowB + sideRowR11d;

			if ((sideBotRow1 > 2.5) && (sideRowL0 + sideRowR0 + sideRowL11u + sideRowR11u < 0.5))
					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}

//			half4	colB1u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight1));
//
//			if ((sideRowL11u + sideRowR11u + colB1u.g > 2.5) && (sideRowL0 + sideRowR0 + sideRowL11d + sideRowR11d < 0.5))
//					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}


			float	sideRowR22d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;
			float	sideRowR12d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;
			float	sideRowR02d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;
			float	sideRowL12d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;
			float	sideRowL22d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;

			float	sideRowDn2 = sideRowR22d + sideRowR12d + sideRowR02d + sideRowL12d + sideRowL22d;

			if ((sideBotRow1 + sideRowL0 + sideRowR0 < 0.5) && (sideRowDn2 > 3.5))
					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}



//			half4	colB1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight1));
//			half4	colB2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight2));
//			half4	colB3  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight3));
//
//
//			if ((colB1.r + colB2.r + colB3.r + colB1u.r > 2.5) && (sideRowL1tot + sideRowR1tot < 0.5))
//					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}
//
//			if ((colB1.b + colB2.b + colB3.b + colB1u.b  > 2.5) && (sideRowL1tot + sideRowR1tot < 0.5))
//					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}
//
//
//
//			half4	colB2u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight2));
//			half4	colB3u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight3));
//
//
//			if ((colB1u.r + colB2u.r + colB3u.r + colB1.r > 2.5) && (sideRowL1tot + sideRowR1tot < 0.5))
//					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}
//
//			if ((colB1u.b + colB2u.b + colB3u.b + colB1.b > 2.5) && (sideRowL1tot + sideRowR1tot < 0.5))
//					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}



			float	sideRowL20  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y)).a;
			float	sideRowL30  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y)).a;

			float	sideRowLtot = sideRowL0 + sideRowL20 + sideRowL30;


			float	sideRowR21u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowR20  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y)).a;
			float	sideRowR21d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

			float	sideRowR2tot = sideRowR21u + sideRowR20 + sideRowR21d;

			if ((sideRowLtot > 0.5) && (sideRowR2tot > 0.5)) break;


			float	sideRowR30 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y)).a;

			float	sideRowRtot = sideRowR0 + sideRowR20 + sideRowR30;


			float	sideRowL21u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowL21d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

			float	sideRowL2tot = sideRowL20 + sideRowL21u + sideRowL21d;

			if ((sideRowRtot > 0.5) && (sideRowL2tot > 0.5)) break;


			if (sideRowL1tot + sideRowR1tot < 0.5)
					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}




			float	sideRowR43  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth4, fragmentInput.textureCoordinate.y - uniform.texelHeight3)).a;
			float	sideRowR42  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth4, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).a;
			float	sideRowR41u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth4, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowR40  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth4, fragmentInput.textureCoordinate.y)).a;

			float	sideRowR4totDn = sideRowR43 + sideRowR42 + sideRowR41u + sideRowR40;

			if (sideRowL1tot + sideRowR4totDn < 0.5)
					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}



			float	sideRowR41d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth4, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowR42d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth4, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;
			float	sideRowR43d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth4, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;

			float	sideRowR4totUp = sideRowR43d + sideRowR42d + sideRowR41d + sideRowR40;

			if (sideRowL1tot + sideRowR4totUp < 0.5)
					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}




			float	sideRowL31u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowL31d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowL32d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;

			float	sideRowLtotUp = sideRowL11u + sideRowL21u + sideRowL31u;
			float	sideRowLtotDn = sideRowL11d + sideRowL21d + sideRowL31d;

			float	sideRowL43  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth4, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;
			float	sideRowL33  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;
			float	sideRowL23  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;
			float	sideRowL13  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;
			float	sideRowR13  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;
			float	sideRowR23  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;

			float	colT3u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight3)).a;

			float	sideRowDn3 = sideRowL43 + sideRowL33 + sideRowL23 + sideRowL13 + colT3u + sideRowR13 + sideRowR23;

			if ((sideRowL1tot + sideRowLtotUp + sideRowLtotDn < 1.5) && (sideRowDn3 + sideRowDn2 + sideRowL32d > 3.5))
					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}




			float	sideRowL40  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth4, fragmentInput.textureCoordinate.y)).a;
			float	sideRowL41d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth4, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowL42d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth4, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;
			float	sideRowL43d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth4, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;

			float	sideRowL4totUp = sideRowL40 + sideRowL41d + sideRowL42d + sideRowL43d;

			if (sideRowR1tot + sideRowL4totUp < 0.5)
					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}




			float	sideRowL42  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth4, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).a;
			float	sideRowL41u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth4, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;

			float	sideRowL4totDn = sideRowL40 + sideRowL41u + sideRowL42 + sideRowL43;

			if (sideRowR1tot + sideRowL4totDn < 0.5)
					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}



			float	sideRowR31u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowR31d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

			float sideBlock = sideRowL40+sideRowR40;
			float sideLeftBlock21 = sideRowL21d+sideRowL21u+sideRowL11d+sideRowL11u;
			float sideLeftBlock43 = sideRowL41d+sideRowL41u+sideRowL31d+sideRowL31u;
			float sideRghtBlock21 = sideRowR21d+sideRowR21u+sideRowR11d+sideRowR11u;
			float sideRghtBlock43 = sideRowR41d+sideRowR41u+sideRowR31d+sideRowR31u;

			if (sideRowLtot+sideRowRtot+sideBlock+sideLeftBlock21+sideLeftBlock43+sideRghtBlock21+sideRghtBlock43 < 1.5)
					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}




			float	surround = sideRowL0 + sideRowL11u + sideRowR0 + sideRowR11u + sideRowT;


			if ((surround < 0.5) && (sideRowR11d > 0.5) && (sideRowL4totDn < 0.5))
					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}

			if ((surround < 0.5) && (sideRowL11d > 0.5) && (sideRowR4totDn < 0.5))
					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}
	}

    return colorTexture;
}
