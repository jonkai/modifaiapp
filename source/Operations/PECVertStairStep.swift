public class PECVertStairStep: BasicOperation
{
    public var texelWidth1:Float  = 1.0 { didSet { uniformSettings["texelWidth1"]  = texelWidth1  }}
    public var texelHeight1:Float = 1.0 { didSet { uniformSettings["texelHeight1"] = texelHeight1 }}

    public var texelWidth2:Float  = 1.0 { didSet { uniformSettings["texelWidth2"]  = texelWidth2  }}
    public var texelHeight2:Float = 1.0 { didSet { uniformSettings["texelHeight2"] = texelHeight2 }}

    public var texelWidth3:Float  = 1.0 { didSet { uniformSettings["texelWidth3"]  = texelWidth3  }}
    public var texelHeight3:Float = 1.0 { didSet { uniformSettings["texelHeight3"] = texelHeight3 }}

    public var texelWidth4:Float  = 1.0 { didSet { uniformSettings["texelWidth4"]  = texelWidth4  }}
    public var texelHeight4:Float = 1.0 { didSet { uniformSettings["texelHeight4"] = texelHeight4 }}

    public var texelWidth5:Float  = 1.0 { didSet { uniformSettings["texelWidth5"]  = texelWidth5  }}
    public var texelHeight5:Float = 1.0 { didSet { uniformSettings["texelHeight5"] = texelHeight5 }}

    public var texelHeight6:Float = 1.0 { didSet { uniformSettings["texelHeight6"] = texelHeight6 }}

	//--------------------------------------------------------
    public init()
    {
        super.init(fragmentFunctionName:"vertStairStepFragment", numberOfInputs:1)

		let texelWidth:Float  = 1.0 / Float(forcedImageSize.width);
		let texelHeight:Float = 1.0 / Float(forcedImageSize.height);

        ({texelWidth1  = texelWidth})()
        ({texelHeight1 = texelHeight})()

        ({texelWidth2  = texelWidth  * 2.0})()
        ({texelHeight2 = texelHeight * 2.0})()

        ({texelWidth3  = texelWidth  * 3.0})()
        ({texelHeight3 = texelHeight * 3.0})()

        ({texelWidth4  = texelWidth  * 4.0})()
        ({texelHeight4 = texelHeight * 4.0})()

        ({texelWidth5  = texelWidth  * 5.0})()
        ({texelHeight5 = texelHeight * 5.0})()

        ({texelHeight6 = texelHeight * 6.0})()
    }
}

