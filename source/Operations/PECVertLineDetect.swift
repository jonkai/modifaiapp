import UIKit

//--------------------------------------------------------
public class PECVertLineDetect: OperationGroup
{
	//--------------------------------------------------------
    public init(green:Bool)
    {
        super.init()

		let sobelF   = PECBiasVertSobelEdgeDetection()
		let nonMaxF  = PECDirectionalNonMaximumSuppression()

		var vBlueF:BasicOperation;
		if (green)
		{
			vBlueF   = PECVerticalGreen()
		} else {
			vBlueF   = PECVerticalRed()
		}

		let vGsimpl1  = PECVertSimpleGap()
		let vGsstep1  = PECVertStairStep()

		let vGfill2  = PECVertGapShort()
		let vGfill2b = PECVertGapShort()
		let vClrS2   = PECdotKill()
		let vGfill2c = PECVertGapShort()
		let vClrS2b  = PECdotKill()
		let vClrS2c  = PECdotKill()

		let vBump    = PECVertBump()


//		let vGapUp2  = PECnibNab()
//		let vGapUp2b = PECnibNab()
//
//		let vSep     = PECVertSeparate()
//		let vAb2     = PECxsectY()
//		let vClrS2d  = PECdotKill()
//
//		let vGapUp2c = PECnibNab()
//		let vGapUp2d = PECnibNab()
//
//		let vSGapF2  = PECpatchRedBlue()
//		let vClr2    = PECclearYsect()
//		let vGPth2   = PECpurple()

		let vSrow2   = PECVertSingleRowUp()

        self.configureGroup
        {input, output in
//			input --> sobelF --> nonMaxF --> vBlueF
//				  --> vGfill2 --> vGfill2b --> vClrS2 --> vGfill2c --> vClrS2b --> vClrS2c
//				  --> vGapUp2 --> vGapUp2b --> vSep --> vAb2 --> vClrS2d --> vGapUp2c --> vGapUp2d
//				  --> vSGapF2 --> vClr2 --> vGPth2 --> vSrow2 --> output

			input --> sobelF --> nonMaxF --> vBlueF --> vGfill2 --> vGfill2b --> vClrS2 --> vGfill2c
				  --> vClrS2b --> vClrS2c --> vGsimpl1 --> vGsstep1 --> vBump --> vSrow2 --> output

	//		input --> sobelF --> nonMaxF --> vBlueF --> vGfill2 --> output
        }
    }
}
