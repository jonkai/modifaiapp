#include <metal_stdlib>
#include "OperationShaderTypes.h"
using namespace metal;

typedef struct
{
	float texelWidth1;
	float texelHeight1;

	float texelWidth2;
	float texelHeight2;

	float texelWidth3;
	float texelHeight3;

	float texelWidth4;
	float texelHeight4;

	float texelWidth5;
	float texelHeight5;

	float texelHeight6;
} NibNabUniform;

fragment half4 nibNabFragment(SingleInputVertexIO fragmentInput [[stage_in]], texture2d<half> inputTexture [[texture(0)]],
                             constant NibNabUniform& uniform [[buffer(1)]])
{
    constexpr sampler quadSampler;
	half colorA = inputTexture.sample(quadSampler, fragmentInput.textureCoordinate).a;
	half4 colorTexture = half4(0.0,0.0,0.0,colorA);

	if (colorA < 0.2)
	for (int i = 0; i < 1; i++)
	{
		float   sideColB1 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
		float   sideColB2 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;
		float   sideColB3 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;
		float   sideColB4 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight4)).a;
	//	float   sideColB5 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight5)).a;

		float	sideColBtot = sideColB1 + sideColB2 + sideColB3 + sideColB4 + sideColB4;

		float	sideColT1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
		float	sideColT2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).a;
		float	sideColT3  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight3)).a;
		float	sideColT4  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight4)).a;
		float	sideColT5  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight5)).a;

		float	sideColTtot = sideColT1 + sideColT2 + sideColT3 + sideColT4 + sideColT5;

			if ((sideColBtot > 1.5) && (sideColTtot > 1.5))
					{colorTexture = half4(0.0,1.0,0.0,1.0); break;}

		float sideRowL1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y)).a;
		float sideRowL2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y)).a;
		float sideRowR1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y)).a;

			if ((sideRowL1 > 0.5) && (sideRowR1 > 0.5))
					{colorTexture = half4(0.0,1.0,0.0,1.0); break;}

		float	sideRowLtot = sideRowL1 + sideRowL2;

		if (sideRowLtot < 0.5)
		{
			float	sideColT11u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideColT21u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).a;
			float	sideColT31u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight3)).a;
			float	sideColT41u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight4)).a;
			float	sideColT51u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight5)).a;

			float	sideRowL1Utot = sideColT11u + sideColT21u + sideColT31u + sideColT41u + sideColT51u;

				if ((sideColBtot > 1.5) && (sideRowL1Utot > 1.5))
						{colorTexture = half4(0.0,1.0,0.0,1.0); break;}

			float	sideColB11d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideColB21d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;
			float	sideColB31d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;
			float	sideColB41d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight4)).a;
			float	sideColB51d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight5)).a;

			float	sideRowR1Btot = sideColB11d + sideColB21d + sideColB31d + sideColB41d + sideColB51d;

				if ((sideRowR1 < 0.5) && (sideColTtot > 1.5) && (sideRowR1Btot > 1.5))
						{colorTexture = half4(0.0,1.0,0.0,1.0); break;}


			float	sideColT11d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideColT21d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).a;
			float	sideColT31d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight3)).a;
			float	sideColT41d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight4)).a;
			float	sideColT51d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight5)).a;

			float	sideRowR1Utot = sideColT11d + sideColT21d + sideColT31d + sideColT41d + sideColT51d;

				if ((sideRowR1 < 0.5) && (sideColBtot > 1.5) && (sideRowR1Utot > 1.5))
						{colorTexture = half4(0.0,1.0,0.0,1.0); break;}

			float	sideColB11u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideColB21u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;
			float	sideColB31u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;
			float	sideColB41u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight4)).a;
			float	sideColB51u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight5)).a;

			float	sideRowL1Btot = sideColB11u + sideColB21u + sideColB31u + sideColB41u + sideColB51u;

				if ((sideColTtot > 1.5) && (sideRowL1Btot > 1.5))
						{colorTexture = half4(0.0,1.0,0.0,1.0); break;}


			float sideRow30  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y)).a;

			if (sideRow30 < 0.5)
			{
				if ((sideColBtot < 1.5) && (sideRowL1Utot > 1.5) && (sideRowR1Btot > 1.5))
						{colorTexture = half4(1.0,1.0,0.0,1.0); break;}

				if ((sideColTtot < 1.5) && (sideRowR1Utot > 1.5) && (sideRowL1Btot > 1.5))
						{colorTexture = half4(1.0,1.0,0.0,1.0); break;}
			}
		}

		float sideRowR2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y)).a;

			if ((sideRowL1 > 0.5) && (sideRowR1 + sideRowR2 > 0.5) && (sideColB1 + sideColB2 + sideColB3 > 0.5) && (sideColT1 + sideColT2 + sideColT3 > 0.5))
					{colorTexture = half4(1.0,1.0,1.0,1.0); break;}

			if ((sideRowL1 + sideRowL2 > 0.5) && (sideRowR1 > 0.5) && (sideColB1 + sideColB2 + sideColB3 > 0.5) && (sideColT1 + sideColT2 + sideColT3 > 0.5))
					{colorTexture = half4(1.0,1.0,1.0,1.0); break;}
	}

    return colorTexture;
}
