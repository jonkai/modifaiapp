#include <metal_stdlib>
#include "OperationShaderTypes.h"
using namespace metal;

typedef struct
{
	float texelWidth1;
	float texelHeight1;

	float texelWidth2;
	float texelHeight2;

	float texelWidth3;
	float texelHeight3;

	float texelWidth4;
	float texelHeight4;

	float texelWidth5;
	float texelHeight5;

	float texelHeight6;
} HorzClearGreenPathUniform;

fragment half4 horzClearGreenPathFragment(SingleInputVertexIO fragmentInput [[stage_in]], texture2d<half> inputTexture [[texture(0)]],
                             constant HorzClearGreenPathUniform& uniform [[buffer(1)]])
{
    constexpr sampler quadSampler;
	float colorA = inputTexture.sample(quadSampler, fragmentInput.textureCoordinate).a;
	half4 colorTexture = half4(0.0,0.0,0.0,colorA);

	if (colorA < 0.5)
	for (int i = 0; i < 1; i++)
	{
		float	rowL1    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1,  fragmentInput.textureCoordinate.y)).g;
		float	rowL2    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2,  fragmentInput.textureCoordinate.y)).g;
		float	rowL3    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3,  fragmentInput.textureCoordinate.y)).g;

		float	rowR1    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1,  fragmentInput.textureCoordinate.y)).a;
		float	rowR2    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2,  fragmentInput.textureCoordinate.y)).a;
		float	rowR3    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3,  fragmentInput.textureCoordinate.y)).a;

		if ((rowL1+rowL2+rowL3 > 0.5) && (rowR1+rowR2+rowR3 > 0.5))
					{colorTexture = half4(0.0,1.0,1.0,1.0); break;}

		float	rowL1T1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).g;
		float	rowR1B1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

		if ((rowL1T1 > 0.5) && (rowR1B1 > 0.5))
					{colorTexture = half4(1.0,1.0,0.0,1.0); break;}

		float	rowL1B1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).g;
		float	rowR1T1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;

		if ((rowL1B1 > 0.5) && (rowR1T1 > 0.5))
					{colorTexture = half4(1.0,1.0,0.0,1.0); break;}



		float	rowT1    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x,  fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
		float	rowT2    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x,  fragmentInput.textureCoordinate.y - uniform.texelHeight2)).a;

		if ((rowL1B1 > 0.5) && (rowT1+rowT2 > 0.5))
					{colorTexture = half4(1.0,1.0,0.0,1.0); break;}


		float	rowL2B1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).g;

		if ((rowL2B1 > 0.5) && (rowR1T1 > 0.5))
					{colorTexture = half4(0.0,1.0,1.0,1.0); break;}


		float	rowB1    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x,  fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
		float	rowB2    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x,  fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;

		if ((rowL1T1 > 0.5) && (rowB1+rowB2 > 0.5))
					{colorTexture = half4(0.0,1.0,0.0,1.0); break;}


		float	rowL2T1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).g;

		if ((rowL2T1 > 0.5) && (rowR1B1 > 0.5))
					{colorTexture = half4(1.0,1.0,1.0,1.0); break;}


		if ((rowL1T1+rowL2T1 > 0.5) && (rowR1+rowR2 > 0.5))
					{colorTexture = half4(0.0,1.0,1.0,1.0); break;}

		if ((rowL1B1+rowL2B1 > 0.5) && (rowR1+rowR2 > 0.5))
					{colorTexture = half4(0.0,1.0,1.0,1.0); break;}

		float	rowL2T2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).g;

		if ((rowL2T2 > 0.5) && (rowR1B1 > 0.5))
					{colorTexture = half4(1.0,1.0,1.0,1.0); break;}

		float	rowL2B2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).g;

		if ((rowL2B2 > 0.5) && (rowR1T1 > 0.5))
					{colorTexture = half4(1.0,1.0,1.0,1.0); break;}
	}

    return colorTexture;
}
