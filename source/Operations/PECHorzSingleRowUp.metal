#include <metal_stdlib>
#include "OperationShaderTypes.h"
using namespace metal;

typedef struct
{
	float texelWidth1;
	float texelHeight1;

	float texelWidth2;
	float texelHeight2;

	float texelWidth3;
	float texelHeight3;

	float texelWidth4;
	float texelHeight4;

	float texelWidth5;
	float texelHeight5;

	float texelHeight6;
} HorzSingleRowUpUniform;

fragment half4 horzSingleRowUpFragment(SingleInputVertexIO fragmentInput [[stage_in]], texture2d<half> inputTexture [[texture(0)]],
                             constant HorzSingleRowUpUniform& uniform [[buffer(1)]])
{
    constexpr sampler quadSampler;
	float colorA = inputTexture.sample(quadSampler, fragmentInput.textureCoordinate).a;
	half4 colorTexture = half4(0.0,0.0,colorA,colorA);

	if (colorA > 0.2)
	for (int i = 0; i < 1; i++)
	{
		if (fragmentInput.textureCoordinate.x - uniform.texelWidth1 < 0.0) {colorTexture = half4(1.0,0.0,1.0,1.0); break;}

		float	sideRowLu1 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
		float	sideRowL0  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y)).a;
		float	sideRowL1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

		float	topRow1 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
		float	botRow1 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

		float	sideRowL1tot = sideRowL1 + sideRowL0 + sideRowLu1;
		float	totTopBot1 =  topRow1 + botRow1;

			if (sideRowL1tot+totTopBot1 < 0.2)
						{colorTexture = half4(1.0,0.0,1.0,1.0); break;}
	}

    return colorTexture;
}
