#include <metal_stdlib>
#include "OperationShaderTypes.h"
using namespace metal;

typedef struct
{
	float texelWidth1;
	float texelHeight1;

	float texelWidth2;
	float texelHeight2;

	float texelWidth3;
	float texelHeight3;

	float texelWidth4;
	float texelWidth5;
} HorzGapFillupUniform;

fragment half4 horzGapFillupFragment(SingleInputVertexIO fragmentInput [[stage_in]], texture2d<half> inputTexture [[texture(0)]],
                             constant HorzGapFillupUniform& uniform [[buffer(1)]])
{
    constexpr sampler quadSampler;
	float colorA = inputTexture.sample(quadSampler, fragmentInput.textureCoordinate).a;
	half4 colorTexture = half4(0.0,0.0,0.0,colorA);

	if (colorA < 0.2)
	for (int i = 0; i < 1; i++)
	{
		float   sideRowR1 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y)).a;
		float   sideRowR2 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y)).a;
		float   sideRowR3 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y)).a;
		float   sideRowR4 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth4, fragmentInput.textureCoordinate.y)).a;
		float   sideRowR5 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth5, fragmentInput.textureCoordinate.y)).a;

		float	sideRowRtot = sideRowR1 + sideRowR2 + sideRowR3 + sideRowR4 + sideRowR5;

		float	sideRowL1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y)).a;
		float	sideRowL2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y)).a;
		float	sideRowL3  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y)).a;
		float	sideRowL4  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth4, fragmentInput.textureCoordinate.y)).a;
		float	sideRowL5  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth5, fragmentInput.textureCoordinate.y)).a;

		float	sideRowLtot = sideRowL1 + sideRowL2 + sideRowL3 + sideRowL4 + sideRowL5;

			if ((sideRowRtot > 1.5) && (sideRowLtot > 1.5))
					{colorTexture = half4(0.0,1.0,0.0,1.0); break;}

		float topRow1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
		float topRow2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).a;
		float botRow1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

			if ((topRow1 > 0.5) && (botRow1 > 0.5))
					{colorTexture = half4(0.0,1.0,0.0,1.0); break;}

		float	coltot = topRow1 + topRow2;

		if (coltot < 0.5)
		{
			float	sideRowL11u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowL21u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowL31u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowL41u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth4, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowL51u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth5, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;

			float	sideRowLUtot = sideRowL11u + sideRowL21u + sideRowL31u + sideRowL41u + sideRowL51u;

				if ((sideRowRtot > 1.5) && (sideRowLUtot > 1.5))
						{colorTexture = half4(0.0,1.0,0.0,1.0); break;}

			float	sideRowR11d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowR21d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowR31d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowR41d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth4, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowR51d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth5, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

			float	sideRowRDtot = sideRowR11d + sideRowR21d + sideRowR31d + sideRowR41d + sideRowR51d;

				if ((botRow1 < 0.5) && (sideRowLtot > 1.5) && (sideRowRDtot > 1.5))
						{colorTexture = half4(0.0,1.0,0.0,1.0); break;}


			float	sideRowL11d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowL21d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowL31d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowL41d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth4, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowL51d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth5, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

			float	sideRowLDtot = sideRowL11d + sideRowL21d + sideRowL31d + sideRowL41d + sideRowL51d;

				if ((botRow1 < 0.5) && (sideRowRtot > 1.5) && (sideRowLDtot > 1.5))
						{colorTexture = half4(0.0,1.0,0.0,1.0); break;}

			float	sideRowR11u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowR21u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowR31u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowR41u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth4, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowR51u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth5, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;

			float	sideRowRUtot = sideRowR11u + sideRowR21u + sideRowR31u + sideRowR41u + sideRowR51u;

				if ((sideRowLtot > 1.5) && (sideRowRUtot > 1.5))
						{colorTexture = half4(0.0,1.0,0.0,1.0); break;}


			float topRow3  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight3)).a;

			if (topRow3 < 0.5)
			{
				if ((sideRowRtot < 1.5) && (sideRowLUtot > 1.5) && (sideRowRDtot > 1.5))
						{colorTexture = half4(1.0,1.0,0.0,1.0); break;}

				if ((sideRowLtot < 1.5) && (sideRowLDtot > 1.5) && (sideRowRUtot > 1.5))
						{colorTexture = half4(1.0,1.0,0.0,1.0); break;}
			}
		}

		float botRow2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;

			if ((topRow1 > 0.5) && (botRow1 + botRow2 > 0.5) && (sideRowR1 + sideRowR2 + sideRowR3 > 0.5) && (sideRowL1 + sideRowL2 + sideRowL3 > 0.5))
					{colorTexture = half4(1.0,1.0,1.0,1.0); break;}

			if ((topRow1 + topRow2 > 0.5) && (botRow1 > 0.5) && (sideRowR1 + sideRowR2 + sideRowR3 > 0.5) && (sideRowL1 + sideRowL2 + sideRowL3 > 0.5))
					{colorTexture = half4(1.0,1.0,1.0,1.0); break;}
	}

    return colorTexture;
}
