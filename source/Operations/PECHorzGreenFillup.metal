#include <metal_stdlib>
#include "OperationShaderTypes.h"
using namespace metal;

typedef struct
{
	float texelWidth1;
	float texelHeight1;

	float texelWidth2;
	float texelHeight2;

	float texelWidth3;
	float texelHeight3;

	float texelWidth4;
	float texelWidth5;
	float texelWidth6;
	float texelWidth7;
	float texelWidth8;
} HorzGreenFillupUniform;

fragment half4 horzGreenFillupFragment(SingleInputVertexIO fragmentInput [[stage_in]], texture2d<half> inputTexture [[texture(0)]],
                             constant HorzGreenFillupUniform& uniform [[buffer(1)]])
{
    constexpr sampler quadSampler;
	half colorA = inputTexture.sample(quadSampler, fragmentInput.textureCoordinate).a;
	half4 colorTexture = half4(0.0,0.0,0.0,colorA);

	if (colorA < 0.2)
	for (int i = 0; i < 1; i++)
	{
		float   sideRowR1 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y)).a;
		float   sideRowR2 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y)).a;
		float   sideRowR3 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y)).a;
		float   sideRowR4 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth4, fragmentInput.textureCoordinate.y)).a;
		float   sideRowR5 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth5, fragmentInput.textureCoordinate.y)).a;
		float   sideRowR6 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth6, fragmentInput.textureCoordinate.y)).a;
		float   sideRowR7 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth7, fragmentInput.textureCoordinate.y)).a;
		float   sideRowR8 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth8, fragmentInput.textureCoordinate.y)).a;

		float	sideRowRtot = sideRowR1 + sideRowR2 + sideRowR3;

		float	sideRowL1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y)).a;
		float	sideRowL2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y)).a;
		float	sideRowL3  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y)).a;
		float	sideRowL4  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth4, fragmentInput.textureCoordinate.y)).a;
		float	sideRowL5  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth5, fragmentInput.textureCoordinate.y)).a;
		float	sideRowL6  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth6, fragmentInput.textureCoordinate.y)).a;
		float	sideRowL7  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth7, fragmentInput.textureCoordinate.y)).a;
		float	sideRowL8  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth8, fragmentInput.textureCoordinate.y)).a;

		float	sideRowLtot = sideRowL1 + sideRowL2 + sideRowL3;

			if ((sideRowRtot > 1.5) && (sideRowLtot > 1.5))
					{colorTexture = half4(0.0,1.0,0.0,1.0); break;}

		float topRow1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
		float topRow2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).a;
		float botRow1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
		float botRow2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;

			if ((topRow1 > 0.5) && (botRow1 > 0.5))
					{colorTexture = half4(0.0,1.0,0.0,1.0); break;}

		float	coltot = topRow1 + topRow2;
		float	colbot = botRow1 + botRow2;

		float	sideR456tot  = sideRowR4 + sideRowR5 + sideRowR6 + sideRowR7 + sideRowR8;
		float	sideL456tot  = sideRowL4 + sideRowL5 + sideRowL6 + sideRowL7 + sideRowL8;

			if ((coltot+colbot < 0.5) && (sideRowRtot+sideR456tot > 2.5) && (sideRowLtot+sideL456tot > 2.5))
					{colorTexture = half4(1.0,0.0,1.0,1.0); break;}

		if (coltot < 0.5)
		{
			float	sideRowL11u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowL21u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowL31u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowL41u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth4, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowL51u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth5, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowL61u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth6, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowL71u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth7, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowL81u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth8, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;

			float	sideRowL1Utot = sideRowL11u + sideRowL21u + sideRowL31u;

				if ((sideRowRtot > 1.5) && (sideRowL1Utot > 1.5))
						{colorTexture = half4(1.0,0.0,0.0,1.0); break;}

			float	sideLu456tot  = sideRowL41u + sideRowL51u + sideRowL61u + sideRowL71u + sideRowL81u;

				if ((colbot < 0.5) && (sideRowRtot+sideR456tot > 2.5) && (sideRowL1Utot+sideLu456tot > 2.5))
						{colorTexture = half4(1.0,0.0,1.0,1.0); break;}

			float	sideRowR11d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowR21d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowR31d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowR41d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth4, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowR51d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth5, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowR61d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth6, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowR71d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth7, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowR81d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth8, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

			float	sideRowR1Btot = sideRowR11d + sideRowR21d + sideRowR31d;

				if ((botRow1 < 0.5) && (sideRowLtot > 1.5) && (sideRowR1Btot > 1.5))
						{colorTexture = half4(1.0,0.0,0.0,1.0); break;}

			float	sideRd456tot  = sideRowR41d + sideRowR51d + sideRowR61d + sideRowR71d + sideRowR81d;

			if ((colbot < 0.5) && (sideRowR1Btot+sideRd456tot > 2.5) && (sideRowLtot+sideL456tot > 2.5))
						{colorTexture = half4(1.0,0.0,1.0,1.0); break;}


			float	sideRowL11d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowL21d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowL31d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowL41d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth4, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowL51d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth5, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowL61d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth6, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowL71d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth7, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	sideRowL81d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth8, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

			float	sideRowLDtot = sideRowL11d + sideRowL21d + sideRowL31d;

				if ((botRow1 < 0.5) && (sideRowRtot > 1.5) && (sideRowLDtot > 1.5))
						{colorTexture = half4(1.0,0.0,0.0,1.0); break;}

			float	sideLd456tot  = sideRowL41d + sideRowL51d + sideRowL61d + sideRowL71d + sideRowL81d;

				if ((colbot < 0.5) && (sideRowRtot+sideR456tot > 2.5) && (sideRowLDtot+sideLd456tot > 2.5))
						{colorTexture = half4(1.0,0.0,1.0,1.0); break;}

			float	sideRowR11u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowR21u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowR31u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowR41u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth4, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowR51u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth5, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowR61u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth6, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowR71u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth7, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	sideRowR81u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth8, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;

			float	sideRowRUtot = sideRowR11u + sideRowR21u + sideRowR31u;

				if ((sideRowLtot > 1.5) && (sideRowRUtot > 1.5))
						{colorTexture = half4(0.0,1.0,1.0,1.0); break;}

				if ((sideRowRtot < 1.5) && (sideRowL1Utot > 1.5) && (sideRowR1Btot > 1.5))
						{colorTexture = half4(0.0,1.0,1.0,1.0); break;}

				if ((sideRowLtot < 1.5) && (sideRowLDtot > 1.5) && (sideRowRUtot > 1.5))
						{colorTexture = half4(0.0,1.0,1.0,1.0); break;}

			float	sideRu456tot  = sideRowR41u + sideRowR51u + sideRowR61u + sideRowR71u + sideRowR81u;

			if ((colbot < 0.5) && (sideRowRUtot+sideRu456tot > 2.5) && (sideRowLtot+sideL456tot > 2.5))
						{colorTexture = half4(1.0,0.0,1.0,1.0); break;}
		}


			if ((topRow1 > 0.5) && (botRow1 + botRow2 > 0.5) && (sideRowR1 + sideRowR2 + sideRowR3 > 0.5) && (sideRowL1 + sideRowL2 + sideRowL3 > 0.5))
					{colorTexture = half4(0.0,0.0,1.0,1.0); break;}

			if ((topRow1 + topRow2 > 0.5) && (botRow1 > 0.5) && (sideRowR1 + sideRowR2 + sideRowR3 > 0.5) && (sideRowL1 + sideRowL2 + sideRowL3 > 0.5))
					{colorTexture = half4(0.0,0.0,1.0,1.0); break;}
	}

    return colorTexture;
}
