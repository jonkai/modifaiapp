#include <metal_stdlib>
#include "OperationShaderTypes.h"
using namespace metal;

typedef struct
{
	float texelWidth1;
	float texelHeight1;

	float texelWidth2;
	float texelHeight2;

	float upperThreshold;
	float lowerThreshold;
} DirectionalNonMaximumSuppressionUniform;

fragment half4 directionalNonMaximumSuppressionFragment(SingleInputVertexIO fragmentInput [[stage_in]], texture2d<half> inputTexture [[texture(0)]],
                             constant DirectionalNonMaximumSuppressionUniform& uniform [[buffer(1)]])
{
    constexpr sampler quadSampler;

	half alphaL2 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2,fragmentInput.textureCoordinate.y)).a;
	half alphaR2 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2,fragmentInput.textureCoordinate.y)).a;
	half alphaT2 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x,fragmentInput.textureCoordinate.y - uniform.texelHeight2)).a;
	half alphaB2 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x,fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;

	half4 color;
	if (alphaL2 + alphaR2 + alphaT2 + alphaB2 < 3.5h)
	{
		color = half4(0.0,0.0,0.0,0.0);
	} else {

		half3 currentGradientAndDirection = inputTexture.sample(quadSampler, fragmentInput.textureCoordinate).rgb;
		float2 gradientDirection = ((float2(currentGradientAndDirection.gb) * 2.0) - 1.0) * float2(uniform.texelWidth1, uniform.texelHeight1);

		float firstSampledGradientMagnitude  = inputTexture.sample(quadSampler, fragmentInput.textureCoordinate + gradientDirection).r;
		float secondSampledGradientMagnitude = inputTexture.sample(quadSampler, fragmentInput.textureCoordinate - gradientDirection).r;



		float multiplier = step(float(firstSampledGradientMagnitude), float(currentGradientAndDirection.r));
		float multiplier2 = multiplier * step(float(secondSampledGradientMagnitude), float(currentGradientAndDirection.r));

		float thresholdCompliance = smoothstep(uniform.lowerThreshold, uniform.upperThreshold, float(currentGradientAndDirection.r));
		float mag = float(multiplier2 * thresholdCompliance);

//	color = half4(half(multiplier),half(multiplier2),mag, 1.0);

		float red = 0.0;
		float blu = 0.0;
		float redA = 0.0;
		float bluA = 0.0;

		if (gradientDirection.y >= gradientDirection.x) red = 1.0; else blu = 1.0;
		if (abs(gradientDirection.y) >= abs(gradientDirection.x)) redA = 1.0*red; else bluA = 1.0*blu;

		color = half4(mag*red,mag*(redA+bluA),mag*blu, mag);
    }
    
    return color;
}
