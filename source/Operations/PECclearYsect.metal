#include <metal_stdlib>
#include "OperationShaderTypes.h"
using namespace metal;

typedef struct
{
	float texelWidth1;
	float texelHeight1;

	float texelWidth2;
	float texelHeight2;

	float texelWidth3;
	float texelHeight3;

	float texelWidth4;
	float texelHeight4;

	float texelWidth5;
	float texelHeight5;

	float texelHeight6;
} ClearYsectUniform;

fragment half4 clearYsectFragment(SingleInputVertexIO fragmentInput [[stage_in]], texture2d<half> inputTexture [[texture(0)]],
                             constant ClearYsectUniform& uniform [[buffer(1)]])
{
    constexpr sampler quadSampler;
	half colorA = inputTexture.sample(quadSampler, fragmentInput.textureCoordinate).a;
	half4 colorTexture = half4(0.0,0.0,0.0,colorA);

	if (colorA > 0.2)
	for (int i = 0; i < 1; i++)
	{
		float	rowL1B1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
		float	rowB1    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x,  fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
		float	rowR1B1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

		float	totB1  = rowL1B1 + rowB1 + rowR1B1;

		if (totB1 < 0.5)
					{colorTexture = half4(1.0,1.0,0.0,1.0); break;}

		float	rowL1T1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
		float	rowT1    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
		float	rowR1T1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;

		float	totT1  = rowL1T1 + rowT1 + rowR1T1;

		if (totT1 < 0.5)
					{colorTexture = half4(1.0,0.0,1.0,1.0); break;}
	}

    return colorTexture;
}
