#include <metal_stdlib>
#include "OperationShaderTypes.h"
using namespace metal;

typedef struct
{
	float texelWidth1;
	float texelHeight1;

	float texelWidth2;
	float texelHeight2;

	float texelWidth3;
	float texelHeight3;

	float texelWidth4;
	float texelHeight4;

	float texelWidth5;
	float texelHeight5;

	float texelHeight6;
} DotKillUniform;

fragment half4 dotKillFragment(SingleInputVertexIO fragmentInput [[stage_in]], texture2d<half> inputTexture [[texture(0)]],
                             constant DotKillUniform& uniform [[buffer(1)]])
{
    constexpr sampler quadSampler;
	half colorA = inputTexture.sample(quadSampler, fragmentInput.textureCoordinate).a;
	half4 colorTexture = half4(0.0,0.0,0.0,colorA);

	if (colorA > 0.5)
	for (int i = 0; i < 1; i++)
	{
		float	rowL1T1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
		float	rowT1    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
		float	rowR1T1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;

		float	totT1  = rowL1T1 + rowT1 + rowR1T1;

		float	rowL1B1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
		float	rowB1    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x,  fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
		float	rowR1B1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

		 float	totB1  = rowL1B1 + rowB1 + rowR1B1;

		if ((totT1 < 0.5) && (totB1 < 0.5))
		{
			float	rowL2T1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	rowL2B1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
			float	rowR2T1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float	rowR2B1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

			if ((rowL2T1+rowL2B1 < 0.5) && (rowR2T1+rowR2B1 < 0.5))
					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}
		}


		float	rowL2T2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).a;
		float	rowL1T2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).a;
		float	rowT2    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).a;
		float	rowR1T2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).a;

		float	totT2  = rowL2T2 + rowL1T2 + rowT2 + rowR1T2;

		float	rowL2B2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;
		float	rowL1B2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;
		float	rowB2    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;
		float	rowR1B2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;

		float	totB2  = rowL2B2 + rowL1B2 + rowB2 + rowR1B2;

		float	rowL2B3  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;
		float	rowL1B3  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;
		float	rowB3    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;
		float	rowR1B3  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;

		float	totB3  = rowL2B3 + rowL1B3 + rowB3 + rowR1B3;


		if ((totT1+totT2 < 0.5) && (totB2+totB3 < 0.5))
				{colorTexture = half4(0.0,0.0,0.0,0.0); break;}


		float	rowL2T3  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight3)).a;
		float	rowL1T3  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight3)).a;
		float	rowT3    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight3)).a;
		float	rowR1T3  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight3)).a;

		float	totT3  = rowL2T3 + rowL1T3 + rowT3 + rowR1T3;

		if ((totT2+totT3 < 0.5) && (totB1+totB2 < 0.5))
				{colorTexture = half4(0.0,0.0,0.0,0.0); break;}



		float	rowR10  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y)).a;
		float	rowR1f = rowR1T2 + rowR1T1 + rowR10 + rowR1B1 + rowR1B2;
		float	rowL10  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y)).a;
		float	rowL1 = rowL1T1 + rowL10 + rowL1B1;

			if ((rowR1f > 3.5) && (rowT1+rowB1+rowL1 < 0.5))
						{colorTexture = half4(0.0,0.0,0.0,0.0); break;}


		float	rowR1 = rowR1T1 + rowR10 + rowR1B1;
		float	rowL1f = rowL1T2 + rowL1T1 + rowL10 + rowL1B1 + rowL1B2;

			if ((rowL1f > 3.5) && (rowT1+rowB1+rowR1 < 0.5))
						{colorTexture = half4(0.0,0.0,0.0,0.0); break;}


		float	sideColTtot = rowT1 + rowT2 + rowT3 + rowB1 + rowB2 + rowB3;
		float	sideRowL1tot = rowL1T1 + rowL1T2 + rowL1T3 + rowL10 + rowL1B1 + rowL1B2 + rowL1B3;
		float	sideRowR1Btot = rowR1B1 + rowR1B2 + rowR1B3;

			if ((sideColTtot < 0.5) && (sideRowL1tot < 0.5) && (sideRowR1Btot < 0.5))
					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}

		float	sideRowR1Ttot = rowR1T1 + rowR1T2 + rowR1T3;

			if ((sideColTtot < 0.5) && (sideRowL1tot < 0.5) && (sideRowR1Ttot < 0.5))
					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}



		float	sideRowR1tot = rowR1T1 + rowR1T2 + rowR1T3 + rowR10 + rowR1B1 + rowR1B2 + rowR1B3;
		float	sideRowL1Btot = rowL1B1 + rowL1B2 + rowL1B3;

			if ((sideColTtot < 0.5) && (sideRowR1tot < 0.5) && (sideRowL1Btot < 0.5))
					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}

		float	sideRowL1Ttot = rowL1T1 + rowL1T2 + rowL1T3;

			if ((sideColTtot < 0.5) && (sideRowR1tot < 0.5) && (sideRowL1Ttot < 0.5))
					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}
	}

    return colorTexture;
}
