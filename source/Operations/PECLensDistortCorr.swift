public class PECLensDistortCorr: BasicOperation
{
    public var texelWidth1:Float  = 1.0 { didSet { uniformSettings["texelWidth1"]  = texelWidth1  }}
    public var texelHeight1:Float = 1.0 { didSet { uniformSettings["texelHeight1"] = texelHeight1 }}

    public var unifu:Float = 1.0 { didSet { uniformSettings["fu"] = unifu }}
    public var unifv:Float = 1.0 { didSet { uniformSettings["fv"] = unifv }}

    public var uniu0:Float = 1.0 { didSet { uniformSettings["u0"] = uniu0 }}
    public var univ0:Float = 1.0 { didSet { uniformSettings["v0"] = univ0 }}

    public var unik1:Float = 1.0 { didSet { uniformSettings["k1"] = unik1 }}
    public var unik2:Float = 1.0 { didSet { uniformSettings["k2"] = unik2 }}
    public var unik3:Float = 1.0 { didSet { uniformSettings["k3"] = unik3 }}

    public var unip1:Float = 1.0 { didSet { uniformSettings["p1"] = unip1 }}
    public var unip2:Float = 1.0 { didSet { uniformSettings["p2"] = unip2 }}

	//--------------------------------------------------------------
    public init(lens:String)
    {
        super.init(fragmentFunctionName:"lensDistortCorrFragment", numberOfInputs:1)

		let texelWidth:Float  = 1.0 / Float(forcedImageSize.width);
		let texelHeight:Float = 1.0 / Float(forcedImageSize.height);

		var fullArray:[String] = []

		if (lens.count > 0)
		{
			if ( lens != "none")
			{
				for pattern in patternArray
				{
//print("pattern: ", pattern);
			//		fullArray = pattern.components(separatedBy: ",")
					let fullArray0 = pattern.components(separatedBy: ",")
//print("fullArray: ", fullArray);

					let tempLensModel:String = fullArray0.first ?? "none";
//print("tempLensModel: ", tempLensModel);

					if (lens.hasPrefix(tempLensModel)) {fullArray = fullArray0; break;}
			//		if (lens.hasPrefix(tempLensModel)) {break;}
				}
			}
		}

//print("  ");
//print("  ");
//print("fullArray: ", fullArray);
//print("  ");
//print("texelWidth : ", texelWidth);
//print("texelHeight: ", texelHeight);
//print("  ");

		var focalLengthInPixelsGPUX:Float = 3744.914307;  //3744.914307 //3393.828613  3646.495605  ipad mini2 3984.976807 3744.914307
		var focalLengthInPixelsGPUY:Float = 3744.914307;  //3744.914307 //3393.828613  3646.495605

		var fu:Float = focalLengthInPixelsGPUX * texelWidth;
		var fv:Float = focalLengthInPixelsGPUY * texelHeight;

		var k1:Float =  0.025005674965292338;   // 0.025005674965292338
		var k2:Float =  0.26744138962856390;    // 0.26744138962856390   0.286744138962856390
		var k3:Float = -0.78382273003615516;   //-0.84382273003615516   -0.78382273003615516

		var p1:Float = 0.0;  //-0.00033408418557645449
		var p2:Float = 0.0;  // 0.0010036060640932001

		let u0:Float = 0.50;
		let v0:Float = 0.50;

		if (fullArray.count >= 8)
		{
			focalLengthInPixelsGPUX = Float(fullArray[1]) ?? focalLengthInPixelsGPUX
			focalLengthInPixelsGPUY = Float(fullArray[2]) ?? focalLengthInPixelsGPUY

			fu = focalLengthInPixelsGPUX * texelWidth;
			fv = focalLengthInPixelsGPUY * texelHeight;

			k1 =  Float(fullArray[3]) ?? 0.025005674965292338
			k2 =  Float(fullArray[4]) ?? 0.26744138962856390
			k3 =  Float(fullArray[5]) ?? -0.78382273003615516

			p1 = Float(fullArray[6]) ?? 0.0
			p2 = Float(fullArray[7]) ?? 0.0
		}

//print("  ");
//print("focalLengthInPixelsGPUX: ", focalLengthInPixelsGPUX);
//print("focalLengthInPixelsGPUY: ", focalLengthInPixelsGPUY);
//print("k1: ", k1);
//print("k2: ", k2);
//print("k3: ", k3);
//print("  ");


        ({texelWidth1  = texelWidth})()
        ({texelHeight1 = texelHeight})()

        ({unifu = fu})()
        ({unifv = fv})()

        ({uniu0 = u0})()
        ({univ0 = v0})()

        ({unik1 = k1})()
        ({unik2 = k2})()
        ({unik3 = k3})()

        ({unip1 = p1})()
        ({unip2 = p2})()
    }
}
