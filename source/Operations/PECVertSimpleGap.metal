#include <metal_stdlib>
#include "OperationShaderTypes.h"
using namespace metal;

typedef struct
{
	float texelWidth1;
	float texelHeight1;

	float texelWidth2;
	float texelHeight2;

	float texelWidth3;
	float texelHeight3;

	float texelWidth4;
	float texelHeight4;

	float texelWidth5;
	float texelHeight5;

	float texelHeight6;
} VertSimpleGapUniform;

fragment half4 vertSimpleGapFragment(SingleInputVertexIO fragmentInput [[stage_in]], texture2d<half> inputTexture [[texture(0)]],
                             constant VertSimpleGapUniform& uniform [[buffer(1)]])
{
    constexpr sampler quadSampler;
	half colorA = inputTexture.sample(quadSampler, fragmentInput.textureCoordinate).a;
	half4 colorTexture = half4(0.0,0.0,0.0,colorA);

	if (colorA < 0.2)
	for (int i = 0; i < 1; i++)
	{
		float sideRowL1    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y)).a;
		float sideRowL2    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y)).a;
		float sideRowR1    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y)).a;
		float sideRowR2    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y)).a;

		float sideColB1 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
		float sideColB2 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;
		float sideColB3 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;

		float sideColB11d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
		float sideColB21d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;
		float sideColB31d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;


		float sideB0tot = sideColB1 + sideColB2 + sideColB3;
		float sideR1tot = sideColB11d + sideColB21d + sideColB31d;

		if ((sideRowL1+sideRowR2 > 1.5) && (sideB0tot+sideR1tot + sideRowR1 < 0.5))
		{
			float sideColT11u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float sideColT21u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).a;
			float sideColT31u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight3)).a;

			float	sideColT1tot = sideColT11u + sideColT21u + sideColT31u;

			float sideColT12d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float sideColT22d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).a;
			float sideColT32d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight3)).a;

			float	sideColT2tot = sideColT12d + sideColT22d + sideColT32d;

			if ((sideColT1tot > 2.5) && (sideColT2tot > 2.5))
					{colorTexture = half4(1.0,0.0,0.0,1.0); break;}
		}

		float sideColB11u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
		float sideColB21u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;
		float sideColB31u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;

		float sideL1tot = sideColB11u + sideColB21u + sideColB31u;

		if ((sideRowR1+sideRowL2 > 1.5) && (sideB0tot+sideL1tot + sideRowL1 < 0.5))
		{
			float sideColT11u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float sideColT21u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).a;
			float sideColT31u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight3)).a;

			float	sideColT1tot = sideColT11u + sideColT21u + sideColT31u;

			float sideColT12d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
			float sideColT22d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).a;
			float sideColT32d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight3)).a;

			float	sideColT2tot = sideColT12d + sideColT22d + sideColT32d;

			if ((sideColT1tot > 2.5) && (sideColT2tot > 2.5))
					{colorTexture = half4(0.0,1.0,0.0,1.0); break;}
		}
	}

    return colorTexture;
}
