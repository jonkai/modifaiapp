#include <metal_stdlib>
#include "TexelSamplingTypes.h"
using namespace metal;

fragment half4 biasVertSobelEdgeDetectionFragment(NearbyTexelVertexIO fragmentInput [[stage_in]], texture2d<half> inputTexture [[texture(0)]])
{
    constexpr sampler quadSampler(coord::pixel);
    half4 color = inputTexture.sample(quadSampler, fragmentInput.textureCoordinate);

	if (color.r+color.g+color.b < 0.01)
	{
		color = half4(0.0,0.0,0.0,0.0);
	} else {

		float bottomLeftIntensityf = inputTexture.sample(quadSampler, fragmentInput.bottomLeftTextureCoordinate).r;
		float topRightIntensityf = inputTexture.sample(quadSampler, fragmentInput.topRightTextureCoordinate).r;
		float topLeftIntensityf = inputTexture.sample(quadSampler, fragmentInput.topLeftTextureCoordinate).r; // no longer biased vertical
		float bottomRightIntensityf = inputTexture.sample(quadSampler, fragmentInput.bottomRightTextureCoordinate).r;
		float leftIntensityf = inputTexture.sample(quadSampler, fragmentInput.leftTextureCoordinate).r;
		float rightIntensityf = inputTexture.sample(quadSampler, fragmentInput.rightTextureCoordinate).r;
		float bottomIntensityf = inputTexture.sample(quadSampler, fragmentInput.bottomTextureCoordinate).r;
		float topIntensityf = inputTexture.sample(quadSampler, fragmentInput.topTextureCoordinate).r;


		float2 gradientDirection;
		gradientDirection.x = -bottomLeftIntensityf - 2.0 * leftIntensityf - topLeftIntensityf + bottomRightIntensityf + 2.0 * rightIntensityf + topRightIntensityf;
		gradientDirection.y = -topLeftIntensityf - 2.0 * topIntensityf - topRightIntensityf + bottomLeftIntensityf + 2.0 * bottomIntensityf + bottomRightIntensityf;

		float gradientMagnitude = length(gradientDirection);
		float2 normalizedDirection = normalize(gradientDirection);

		normalizedDirection = sign(normalizedDirection) * floor(abs(normalizedDirection) + 0.617316); // Offset by 1-sin(pi/8) to set to 0 if near axis, 1 if away
		normalizedDirection = (normalizedDirection + 1.0) * 0.5; // Place -1.0 - 1.0 within 0 - 1.0

		color = half4(gradientMagnitude, normalizedDirection.x, normalizedDirection.y, 1.0);
    }
    
    return color;
}
