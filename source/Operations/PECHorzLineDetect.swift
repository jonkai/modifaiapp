import UIKit

//--------------------------------------------------------
public class PECHorzLineDetect: OperationGroup
{
	//--------------------------------------------------------
    public init(yellow:Bool)
    {
        super.init()

		let sobelF   = PECBiasHorzSobelEdgeDetection()
		let nonMaxF  = PECDirectionalNonMaximumSuppression()

		var hBlueF:BasicOperation;
		if (yellow)
		{
			hBlueF   = PECHorizontalYellow()
		} else {
			hBlueF   = PECHorizontalBlue()
		}

		let hGfill2  = PECHorzGreenFillup()
		let hGfill2b = PECHorzGreenFillup()
		let hClrS2   = PECHorzClearShort()
		let hGfill2c = PECHorzGreenFillup()
		let hGfill2d = PECHorzGreenFillup()
		let hGfill2e = PECHorzGreenFillup()

		let hClrS2b  = PECHorzClearShort()
		let hClrS2c  = PECHorzClearShort()
		let hClrS2d  = PECHorzClearShort()

		let hGsimpl1  = PECHorzSimpleGap()
		let hGsstep1  = PECHorzStairStep()

		let hBump    = PECHorzBump()

//		let hGapUp2  = PECHorzGapFillup()
//		let hGapUp2b = PECHorzGapFillup()
//
//		let hSep     = PECHorzSeparate()
//
//		let hAb2     = PECHorzAboveRow()
//		let hClrS2d  = PECHorzClearShort()
//
//		let hGapUp2c = PECHorzGapFillup()
//		let hGapUp2d = PECHorzGapFillup()
//
//		let hSGapF2  = PECHorzSimpGapFill()
//		let hClr2    = PECHorzClear()
//		let hGPth2   = PECHorzClearGreenPath()
		let hSrow2   = PECHorzSingleRowUp()

        self.configureGroup
        {input, output in

//			input.addTarget(sobelF, atTargetIndex: 0)
//			input.addTarget(nonMaxF, atTargetIndex: 0)
//			input.addTarget(hBlueF, atTargetIndex: 0)
//
//			input.addTarget(hGfill2, atTargetIndex: 0)
//			input.addTarget(hGfill2b, atTargetIndex: 0)
//			input.addTarget(hClrS2, atTargetIndex: 0)
//			input.addTarget(hGfill2c, atTargetIndex: 0)
//			input.addTarget(hClrS2b, atTargetIndex: 0)
//			input.addTarget(hClrS2c, atTargetIndex: 0)
//
//			input.addTarget(hGapUp2, atTargetIndex: 0)
//			input.addTarget(hGapUp2b, atTargetIndex: 0)
//
//			input.addTarget(hSep, atTargetIndex: 0)
//
//			input.addTarget(hAb2, atTargetIndex: 0)
//			input.addTarget(hClrS2d, atTargetIndex: 0)
//
//			input.addTarget(hGapUp2c, atTargetIndex: 0)
//			input.addTarget(hGapUp2d, atTargetIndex: 0)
//
//			input.addTarget(hSGapF2, atTargetIndex: 0)
//			input.addTarget(hClr2, atTargetIndex: 0)
//			input.addTarget(hGPth2, atTargetIndex: 0)
//			input.addTarget(hSrow2, atTargetIndex: 0)
//			input.addTarget(output, atTargetIndex: 0)

//			input --> sobelF --> nonMaxF --> hBlueF
//				  --> hGfill2 --> hGfill2b --> hClrS2 --> hGfill2c --> hClrS2b --> hClrS2c
//				  --> hGapUp2 --> hGapUp2b --> hSep --> hAb2 --> hClrS2d --> hGapUp2c --> hGapUp2d
//				  --> hSGapF2 --> hClr2 --> hGPth2 --> hSrow2 --> output

//			input --> sobelF --> nonMaxF --> hBlueF --> hGfill2 --> hGfill2b --> hClrS2 --> output

			input --> sobelF --> nonMaxF --> hBlueF --> hGfill2 --> hGfill2b --> hClrS2 --> hGfill2c --> hGfill2d
				--> hClrS2b --> hClrS2c --> hGfill2e --> hClrS2d --> hGsimpl1 --> hGsstep1 --> hBump --> hSrow2 --> output


//			input --> sobelF --> nonMaxF --> hBlueF --> output
//				  --> hGfill2 --> hGfill2b --> hClrS2 --> hGfill2c --> hClrS2b --> hClrS2c
//				  --> hGapUp2 --> hGapUp2b --> hSep --> hAb2 --> hClrS2d --> hGapUp2c --> hGapUp2d
//				  --> hSGapF2 --> hClr2 --> hGPth2 --> hSrow2 --> output



//			input --> hBlueF
//				  --> hGfill2 --> hGfill2b --> hClrS2 --> hGfill2c --> hClrS2b --> hClrS2c
//				  --> hGapUp2 --> hGapUp2b --> hSep --> hAb2 --> hClrS2d --> hGapUp2c --> hGapUp2d
//				  --> hSGapF2 --> hClr2 --> hGPth2 --> hSrow2 --> output
        }
    }
}
