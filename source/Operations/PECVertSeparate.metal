#include <metal_stdlib>
#include "OperationShaderTypes.h"
using namespace metal;

typedef struct
{
	float texelWidth1;
	float texelHeight1;

	float texelWidth2;
	float texelHeight2;

	float texelWidth3;
	float texelHeight3;

	float texelWidth4;
	float texelHeight4;

	float texelWidth5;
	float texelHeight5;

	float texelHeight6;
} VertSeparateUniform;

fragment half4 vertSeparateFragment(SingleInputVertexIO fragmentInput [[stage_in]], texture2d<half> inputTexture [[texture(0)]],
                             constant VertSeparateUniform& uniform [[buffer(1)]])
{
    constexpr sampler quadSampler;
	half4 colorTexture = inputTexture.sample(quadSampler, fragmentInput.textureCoordinate);

	for (int i = 0; i < 1; i++)
	{
		float	botRow1    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y)).a;
		float	botRow2    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y)).a;
		float	botRow3    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y)).a;
		float	botRow4    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth4, fragmentInput.textureCoordinate.y)).a;
		float	botRow5    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth5, fragmentInput.textureCoordinate.y)).a;

		if ((botRow1 > 0.5) && (botRow2+botRow3+botRow4 < 0.5) && (botRow5 > 0.5))
					{colorTexture = half4(1.0,0.0,0.0,1.0); break;}

		if (colorTexture.a > 0.5)
		{
			if ((botRow1+botRow2+botRow3 < 0.5) && (botRow4 > 0.5))
					{colorTexture = half4(0.0,0.0,0.0,0.0); break;}

			if (botRow3 > 0.5)
			{
				float sideRow2 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;
				float sideRow3 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;
				float sideRow4 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight4)).a;
				float sideRow5 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight5)).a;

				float sideRow31d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
				float sideRow32d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;
//				float sideRow33d = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;

				if ((sideRow2+sideRow3+sideRow4+sideRow5 < 0.5) && (sideRow31d+sideRow32d > 0.5))
						{colorTexture = half4(0.0,0.0,0.0,0.0); break;}


				float sideRow2u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).a;
				float sideRow3u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight3)).a;
				float sideRow4u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight4)).a;
				float sideRow5u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight5)).a;

				float sideRow31u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
				float sideRow32u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).a;
//				float sideRow33u = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth3, fragmentInput.textureCoordinate.y - uniform.texelHeight3)).a;

				if ((sideRow2u+sideRow3u+sideRow4u+sideRow5u < 0.5) && (sideRow31u+sideRow32u > 0.5))
						{colorTexture = half4(0.0,0.0,0.0,0.0); break;}
			}
		}
	}
    return colorTexture;
}
