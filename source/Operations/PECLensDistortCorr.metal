#include <metal_stdlib>
#include "OperationShaderTypes.h"
using namespace metal;

typedef struct
{
    float texelWidth1;
    float texelHeight1;

    float fu;
    float fv;

    float u0;
    float v0;

    float k1;
    float k2;
    float k3;

    float p1;
    float p2;
} PECLensDistortCorrUniform;

//--------------------------------------------------------------
fragment half4 lensDistortCorrFragment(SingleInputVertexIO fragmentInput [[stage_in]], texture2d<half> inputTexture [[texture(0)]],
                             constant PECLensDistortCorrUniform& uniform [[buffer(1)]])
{
	float x = (fragmentInput.textureCoordinate.x - uniform.u0) / uniform.fu;
	float y = (fragmentInput.textureCoordinate.y - uniform.v0) / uniform.fv;

	float x2 = x * x;
	float y2 = y * y;

	float r2 = x2 + y2;
	float r4 = r2*r2;
	float r6 = r4*r2;

	float cDist = 1.0 + (uniform.k1*r2) + (uniform.k2*r4) + (uniform.k3*r6);
//	float cDist = 1.0 + (k1*r2) + (k2*r4);
	float xr = x*cDist;
	float yr = y*cDist;

	float a1 = 2.0 * x*y;
	float a2 = r2 + (2.0 * x2);
	float a3 = r2 + (2.0 * y2);

	float dx = (uniform.p1*a1) + (uniform.p2*a2);
	float dy = (uniform.p1*a3) + (uniform.p2*a1);

	float xd = xr + dx;
	float yd = yr + dy;

	float ud = ((xd*uniform.fu) + uniform.u0);// + du0;
	float vd = ((yd*uniform.fv) + uniform.v0);// + dv0;

	float2 textureCoordinateToUse = float2(ud,vd);


	half4 color = half4(0.0,0.0,0.0,0.0);

	if (((ud > uniform.texelWidth1) && (ud < 1.0-uniform.texelWidth1)) && ((vd > uniform.texelHeight1) && (vd < 1.0-uniform.texelHeight1)))
	{
    	constexpr sampler quadSampler;
    	color = inputTexture.sample(quadSampler, textureCoordinateToUse);
	}

	return color;
}

