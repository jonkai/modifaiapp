#include <metal_stdlib>
#include "OperationShaderTypes.h"
using namespace metal;

typedef struct
{
	float texelWidth1;
	float texelHeight1;

	float texelWidth2;
	float texelHeight2;

	float texelWidth3;
	float texelHeight3;

	float texelWidth4;
	float texelHeight4;

	float texelWidth5;
	float texelHeight5;

	float texelHeight6;
} XsectYUniform;

fragment half4 xsectYFragment(SingleInputVertexIO fragmentInput [[stage_in]], texture2d<half> inputTexture [[texture(0)]],
                             constant XsectYUniform& uniform [[buffer(1)]])
{
    constexpr sampler quadSampler;
	float colorA = inputTexture.sample(quadSampler, fragmentInput.textureCoordinate).a;
	half4 colorTexture = half4(0.0,0.0,0.0,colorA);

	if (colorA > 0.2)
	for (int i = 0; i < 1; i++)
	{
		float	sideRowR1   = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y)).a;

		float	sideRowL1   = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y)).a;
		float	sideRowL2   = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y)).a;
		float	sideRowL3   = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth3, fragmentInput.textureCoordinate.y)).a;

		float	sideRowL2B1 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
		float	sideRowL2T1 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;

		if ((sideRowR1 < 0.5) && (sideRowL1 + sideRowL2 + sideRowL3 > 2.5) && (sideRowL2B1 + sideRowL2T1 > 1.5)) break;

		if (sideRowL1 > 0.5)
					{colorTexture = half4(0.0,0.0,0.0,0.0);}// break;}
	}

    return colorTexture;
}
