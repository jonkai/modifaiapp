#include <metal_stdlib>
#include "OperationShaderTypes.h"
using namespace metal;

typedef struct
{
	float texelWidth1;
	float texelHeight1;

	float texelWidth2;
	float texelHeight2;

	float texelWidth3;
	float texelHeight3;

	float texelWidth4;
	float texelHeight4;

	float texelWidth5;
	float texelHeight5;

	float texelHeight6;
} HorzBumpUniform;

fragment half4 horzBumpFragment(SingleInputVertexIO fragmentInput [[stage_in]], texture2d<half> inputTexture [[texture(0)]],
                             constant HorzBumpUniform& uniform [[buffer(1)]])
{
    constexpr sampler quadSampler;
	half colorA = inputTexture.sample(quadSampler, fragmentInput.textureCoordinate).a;
	half4 colorTexture = half4(0.0,0.0,colorA,colorA);

	if (colorA < 0.2)
	for (int i = 0; i < 1; i++)
	{
		float sideRowL1 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).a;
		float sideRowL2 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).a;
		float sideRowR1 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
		float sideRowR2 = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;

		float sideColB21d  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;
		float sideColB21u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).a;

		float sideColB1    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y)).a;
		float sideColB11u  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

		float sideB1tot = sideColB1 + sideColB11u;


		float sideL1tot = sideRowL1 + sideRowL2 + sideColB21u;
		float sideR1tot = sideRowR1 + sideRowR2 + sideColB21d;

			if ((sideB1tot > 1.5) && (sideL1tot+sideR1tot < 0.5))
						{colorTexture = half4(1.0,0.0,1.0,1.0); break;}
	}

    return colorTexture;
}
