#include <metal_stdlib>
#include "OperationShaderTypes.h"
using namespace metal;

fragment half4 horizontalBlueFragment(SingleInputVertexIO fragmentInput [[stage_in]], texture2d<half> inputTexture [[texture(0)]])
{
    constexpr sampler quadSampler;
	half3 colorTexture = inputTexture.sample(quadSampler, fragmentInput.textureCoordinate).rgb;
	half4 colorOut = half4(0.0,0.0,0.0,0.0);

	if ((colorTexture.b > 0.5) && (colorTexture.r + colorTexture.g < 0.5))
			colorOut = half4(0.0,0.0,0.0,1.0);
	if ((colorTexture.r > 0.5) && (colorTexture.g + colorTexture.b < 0.5))  //++++++++++ can't yet tell if this is worth it +++++++  for now no.
			colorOut = half4(0.0,0.0,0.0,1.0);

    return colorOut;
}
