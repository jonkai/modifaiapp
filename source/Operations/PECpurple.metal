#include <metal_stdlib>
#include "OperationShaderTypes.h"
using namespace metal;

typedef struct
{
	float texelWidth1;
	float texelHeight1;

	float texelWidth2;
	float texelHeight2;

	float texelWidth3;
	float texelHeight3;

	float texelWidth4;
	float texelHeight4;

	float texelWidth5;
	float texelHeight5;

	float texelHeight6;
} PurpleUniform;

fragment half4 purpleFragment(SingleInputVertexIO fragmentInput [[stage_in]], texture2d<half> inputTexture [[texture(0)]],
                             constant PurpleUniform& uniform [[buffer(1)]])
{
    constexpr sampler quadSampler;
	half colorA = inputTexture.sample(quadSampler, fragmentInput.textureCoordinate).a;
	half4 colorTexture = half4(0.0,0.0,0.0,colorA);

	if (colorA < 0.5)
	for (int i = 0; i < 1; i++)
	{
		float	rowT1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).g;
		float	rowT2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).g;
		float	rowT3  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y - uniform.texelHeight3)).g;

		float	rowB1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;
		float	rowB2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight2)).a;
		float	rowB3  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x, fragmentInput.textureCoordinate.y + uniform.texelHeight3)).a;

		if ((rowT1+rowT2+rowT3 > 0.5) && (rowB1+rowB2+rowB3 > 0.5))
					{colorTexture = half4(0.0,1.0,1.0,1.0); break;}

		float	rowL1T1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).g;
		float	rowR1B1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

		if ((rowL1T1 > 0.5) && (rowR1B1 > 0.5))
					{colorTexture = half4(0.0,0.0,0.0,1.0); break;}

		float	rowR1T1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight1)).g;
		float	rowL1B1  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y + uniform.texelHeight1)).a;

		if ((rowR1T1 > 0.5) && (rowL1B1 > 0.5))
					{colorTexture = half4(0.0,0.0,0.0,1.0); break;}



		float	rowL1    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1,  fragmentInput.textureCoordinate.y)).a;
		float	rowL2    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2,  fragmentInput.textureCoordinate.y)).a;

		if ((rowR1T1 > 0.5) && (rowL1+rowL2 > 0.5))
					{colorTexture = half4(0.0,0.0,0.0,1.0); break;}


		float	rowR1T2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).g;

		if ((rowR1T2 > 0.5) && (rowL1B1 > 0.5))
					{colorTexture = half4(0.0,0.0,0.0,1.0); break;}


		float	rowR1    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth1,  fragmentInput.textureCoordinate.y)).a;
		float	rowR2    = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2,  fragmentInput.textureCoordinate.y)).a;

		if ((rowL1T1 > 0.5) && (rowR1+rowR2 > 0.5))
					{colorTexture = half4(0.0,0.0,0.0,1.0); break;}


		float	rowL1T2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth1, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).g;

		if ((rowL1T2 > 0.5) && (rowR1B1 > 0.5))
					{colorTexture = half4(0.0,0.0,0.0,1.0); break;}


		if ((rowL1T1+rowL1T2 > 0.5) && (rowB1+rowB2 > 0.5))
					{colorTexture = half4(0.0,1.0,1.0,1.0); break;}

		if ((rowR1T1+rowR1T2 > 0.5) && (rowB1+rowB2 > 0.5))
					{colorTexture = half4(0.0,0.0,0.0,1.0); break;}


		float	rowL2T2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x - uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).g;

		if ((rowL2T2 > 0.5) && (rowR1B1 > 0.5))
					{colorTexture = half4(1.0,1.0,1.0,1.0); break;}

		float	rowR2T2  = inputTexture.sample(quadSampler, float2(fragmentInput.textureCoordinate.x + uniform.texelWidth2, fragmentInput.textureCoordinate.y - uniform.texelHeight2)).g;

		if ((rowR2T2 > 0.5) && (rowL1B1 > 0.5))
					{colorTexture = half4(1.0,1.0,1.0,1.0); break;}
	}

    return colorTexture;
}
