//----------------------------------------------------------------
open class OperationGroup: ImageProcessingOperation
{
    let inputImageRelay = ImageRelay()
    let outputImageRelay = ImageRelay()
    
    public var sources:SourceContainer { get { return inputImageRelay.sources } }
    public var targets:TargetContainer { get { return outputImageRelay.targets } }
    public let maximumInputs:UInt = 4 //1 //+++++++++++ possible change just my own implementation for specific purpose JKC +++
    
    public init()
    {
    }
    
	//----------------------------------------------------------------
    public func newTextureAvailable(_ texture:Texture, fromSourceIndex:UInt)
    {
        inputImageRelay.newTextureAvailable(texture, fromSourceIndex:fromSourceIndex)
    }
    
	//----------------------------------------------------------------
    public func configureGroup(_ configurationOperation:(_ input:ImageRelay, _ output:ImageRelay) -> ())
    {
        configurationOperation(inputImageRelay, outputImageRelay)
    }
    
	//----------------------------------------------------------------
    public func transmitPreviousImage(to target:ImageConsumer, atIndex:UInt)
    {
        outputImageRelay.transmitPreviousImage(to:target, atIndex:atIndex)
    }
}
