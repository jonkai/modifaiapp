public struct Size
{
    public let width:Float
    public let height:Float
    
    public init(width:Float, height:Float)
    {
        self.width = width
        self.height = height
    }
}

public var forcedImageSize:Size = Size(width: 1.0, height:1.0);

public var patternArray:[String] = []
